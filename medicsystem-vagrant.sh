#!/bin/bash

set -e

#install puppet
#install puppet
if ! rpm -q puppetlabs-release
then
    if ! rpm --replacepkgs -ivh https://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm 
    then
        rpm --replacepkgs -ivg http://mirror.its.sfu.ca/mirror/PuppetLabs/yum/puppetlabs-release-el-6.noarch.rpm
    fi

fi

yum -y install puppet

puppet resource package puppet ensure=latest

service puppet restart

#install puppet support modules
puppet module install --force puppetlabs-stdlib --version 4.9.0
puppet module install --force puppetlabs-java --version 1.4.1
puppet module install --force nanliu-archive --version 0.3.0
puppet module install --force biemond-wildfly --version 0.4.2
puppet module install --force puppetlabs-firewall --version 1.6.0
puppet module install --force rodjek-logrotate --version 1.1.1
puppet module install --force puppetlabs-ntp --version 4.1.0

 gem install faraday_middleware