#!/bin/bash
set -e

#install puppet
if ! rpm -q puppetlabs-release
then
    rpm --replacepkgs -ivh https://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm
fi

yum -y install puppet

#install puppet support modules
puppet module install puppetlabs-java --version 1.4.1
puppet module install biemond-wildfly --version 0.3.6
puppet module install puppetlabs-firewall --version 1.6.0
puppet module install rodjek-logrotate --version 1.1.1
puppet module install puppetlabs-ntp --version 4.1.0