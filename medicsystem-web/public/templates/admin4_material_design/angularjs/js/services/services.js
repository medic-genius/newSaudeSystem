MedicsystemApp.factory('servicesFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {

        return {
            clientes: $resource('mediclab/clientes/:id', {id:'@id'}, {
                 update:   { method: "PUT" },
                 get:  { method: "GET"}

            }),

            clienteSave: $resource('mediclab/empresasClientes/addcliente', {}, {

                post:  {
                    method: "POST",
                    interceptor: {
                        response: function(response) {
                            var result = response.resource;
                            result.$status = response.status;
                            return result;
                        }
                    }
                }
            }),

            fotoCliente: $resource('mediclab/clientes/:id/foto/cliente', {id:'@id'}, {

            }),

            fotoDependente: $resource('mediclab/dependentes/:id/foto/dependente', {id:'@id'}, {

            }),

            contratoPdf: $resource('mediclab/contratos/contratopdf', {}, {
               get:  { method: "GET"}
            }),

            boletoPdf: $resource('mediclab/contratos/boletopdf', {}, {
               get:  { method: "GET"}
            }),

            contratoFuncionario: $resource('mediclab/contratos/contratopv', {}, {
               get:  { method: "GET", isArray:true }
            }),

            contratoFuncionariosUnidade: $resource('mediclab/contratos/contratospvfuncionarios', {}, {
               get:  { method: "GET", isArray:true }
            }),

            informativo: $resource('mediclab/sms/informativo', {}, {
                get:  { method: "GET", isArray:true }
            }),

            informativoinfo: $resource('mediclab/sms/informativoinfo', {}, {
                save: { method: "POST"}
            }),
            pontounidade: $resource('mediclab/registroponto/unidadeperiodo', {}, {
                query:  { method: "GET", isArray:true }
            }),
            pontofuncionario: $resource('mediclab/registroponto/funcionarioperiodo', {}, {
                query: {method: "GET", isArray: true}
            }),
            updateRegistroPonto: $resource('mediclab/registroponto/updateregistros', {},{
                update: {method:"PUT"}
            }),
            //Salvar Novo Registro ponto
            saveRegistroPonto: $resource('mediclab/registroponto/save',{},{
                save: { method: 'POST'}
            }),
            validarDataRegistro: $resource('mediclab/registroponto/registro', {},{
                get: {method: 'GET', isArray: true}
            }),
            faltamedico: $resource('mediclab/sms/agendamentos/faltamedico', {}, {
                save: { method: "POST",  isArray:true }
            }),

            medicos: $resource('mediclab/funcionarios/medicos/:id', {id:'@id'}, {
                 update:   { method: "PUT" }
            }),
            laudos: $resource('mediclab/laudoatendimento/paciente', {}, {
                 get:  { method: "GET", isArray:true }
            }),
            servicos: $resource('mediclab/servicos/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            servicosByTipo: $resource('mediclab/servicos/tipoServico/:inTipo', {id: '@intipo'}, {
                get:  { method: "GET", isArray:true }
            }),

            contratos: $resource('mediclab/contratos/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            contratoEmpresaCliente: $resource('mediclab/contratos/empresaCliente/:id', {id:'@id'}, {
                get:   { method: "GET" }
            }),

            empresasClienteAtivas: $resource('mediclab/empresasClientes/ativas', {}, {
                get:   { method: "GET" ,isArray: true}
            }),

            empresasClienteInativas: $resource('mediclab/empresasClientes/inativas', {}, {
                get:   { method: "GET" }
            }),

            empresagrupos: $resource('mediclab/empresagrupos/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            dependentes: $resource('mediclab/dependentes/:id', {id:'@id'}, {
                get:   { method: "GET" },
                update:  { method: "PUT" },
                save : {method:'POST'}
            }),

            depedenteSave : $resource('mediclab/dependentes', {}, {
                save : {method:'POST'}
            }),

            dependentesCliente: $resource('mediclab/dependentes/:id/dependentescliente', {id:'@id'}, {
                get : {method:'GET', isArray:true}
            }),

            observacoes: $resource('mediclab/observacoes/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            orgaos: $resource('mediclab/orgaos/:id', {}, {
                update:   { method: "PUT" }
            }),

            bancos: $resource('mediclab/bancos/:id', {}, {
                update:   { method: "PUT" }
            }),

            despesas: $resource('mediclab/despesas/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            lancamentosDespesas: $resource('mediclab/despesas/novoLancamento', {}, {
                save:   { method: "POST" }
            }),

            lancamentosDespesasAutorizado: $resource('mediclab/despesas/novoLancamentoAutorizado', {}, {
                save:   { method: "POST" }
            }),

            cidades: $resource('mediclab/cidades/:id', {}, {
                get:    { method: "GET" },
                query:  { method: "GET", isArray:true },
                save:   { method: "POST" },
                remove: { method: "DELETE" }
            }),

            agendamentos: $resource('mediclab/agendamentos/:id', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

            unidades: $resource('mediclab/unidades/:id', {}, {
                get:    { method: "GET" },
                query:  { method: "GET", isArray:true },
                save:   { method: "POST" },
                remove: { method: "DELETE" }
            }),

            contratoCliente: $resource('mediclab/contratos/:idcontrato/clientes/:idcliente', {idcontrato:'@idcontrato', idcliente:'@idcliente'}, {
                query:  { method: "GET" }
            }),

            clienteAgendamentos: $resource('mediclab/clientes/:id/agendamentos', {}, {
                get:  { method: "GET", isArray:true }
            }),

            clienteDespesas: $resource('mediclab/clientes/:id/despesas', {}, {
                get:  { method: "GET", isArray:true }
            }),

            clienteSomaValorDebito: $resource('mediclab/clientes/:id/somaValorDebito', {}, {
                get:  { method: "GET" }
            }),

            clienteArquivos: $resource('mediclab/clientes/:id/arquivos', {}, {
                get:  { method: "GET", isArray:true }
            }),
            clienteDependentes: $resource('mediclab/clientes/:id/dependentes', {}, {
                get:  { method: "GET", isArray:true }
            }),

            clienteContratos: $resource('mediclab/clientes/:id/contratos', {}, {
                get:  { method: "GET", isArray:true }
            }),

            clienteView: $resource('mediclab/clienteview/:id', {id:'@id'}, {
                 update:   { method: "PUT" }
            }),

            clienteSituacao: $resource('mediclab/clientes/:id/situacao', {}, {
                query:  { method: "GET", isArray:true }
            }),

            clienteNewProspect: $resource('mediclab/clientes/:cpf/newprospect', {}, {
                query:  { method: "GET" }
            }),

            clienteContratosDependentes: $resource('mediclab/clientes/:id/dependentes/:iddependente/contratos', {}, {
                get:  { method: "GET", isArray:true }
            }),
            clienteObservacoes: $resource('mediclab/clientes/:id/observacoes', {}, {
                get:  { method: "GET", isArray:true }
            }),
            clienteCoberturas: $resource('mediclab/clientes/:id/coberturas', {}, {
                get:  { method: "GET", isArray:true }
            }),

            clienteAgendamentosDepesas: $resource('mediclab/agendamentos/:id/despesa/editar', {}, {
                get:    { method: "GET" },
                query:  { method: "GET", isArray:true },
                save:   { method: "POST" },
                update:   { method: "PUT" },
                remove: { method: "DELETE" }
            }),

            loginGerente: $resource('mediclab/logingerente', {}, {
                auth:   { method: "POST" }
            }),

            despesaCarregarComboForma: $resource('mediclab/agendamentos/:id/despesa/combo/formapagamento', {}, {
                get:  { method: "GET", isArray:true }
            }),

            cidadeBairros: $resource('mediclab/cidades/:id/bairros', {}, {
                get:  { method: "GET", isArray:true }
            }),

            agendamentosStatusPresente: $resource('mediclab/agendamentos/:id/presente', {}, {
                update:   { method: "PUT" }
            }),

            agendamentosDespesa: $resource('mediclab/agendamentos/:id/despesa', {}, {
                get:    { method: "GET" }
            }),

            unidadesEspecialidades: $resource('mediclab/unidades/:id/especialidades', {}, {
                get:    { method: "GET", isArray: true }
            }),

            especialidades: $resource('mediclab/especialidades/:id', {}, {
                get:    { method: "GET" },
                query:  { method: "GET", isArray:true },
                save:   { method: "POST" },
                remove: { method: "DELETE" }
            }),

            especialidadesAtivas: $resource('mediclab/especialidades',{}, {
                get: { method: "GET" , isArray:true}
            }),

            subEspecialidades: $resource('mediclab/especialidades/:id/subespecialidades', {}, {
                get:  { method: "GET", isArray:true }
            }),

            especialidadeProfissionais: $resource('mediclab/especialidades/:id/profissionais', {}, {
                query:  { method: "GET", isArray:true }
            }),

            especialidadeProfissionaisDoDia: $resource('mediclab/especialidades/:id/profissionaisPorData', {}, {
                query:  { method: "GET", isArray:true }
            }),

            administracaoRenovacaoContratos: $resource('mediclab/renovacao/contratosrenovados', {}, {
                query:  { method: "GET", isArray: true }
            }),

            administracaoRenovacaoContratoMensalidade: $resource('mediclab/renovacao/mensalidades/contrato/:idcontrato', {idcontrato:'@idcontrato'}, {
                query:  { method: "GET", isArray: true }
            }),

            administracaoRenovacaoContratoImprimir: $resource('mediclab/renovacao/contrato/boletos/imprimir', {}, {
                query:  { method: "GET", isArray: true }
            }),
            /*begin relatorio renovacao  18/04/2018*/
            renovacaoFormaPagamento: $resource('mediclab/contratos/renovacao/formapagamento', {}, {
                query:{method: "GET", isArray: true}
            }),
            administracaoRelRenovacao: $resource('mediclab/contratos/relatoriorenovacao', {},{
                get: {method: "GET"}
            }),

            funcionariosPeriodoRelRenovacao: $resource('mediclab/contratos/renovacao/funcionarios', {dtInicio: '@dtInicio', dtFim: '@dtFim'}, {
                get: { method: 'GET', isArray: true }
            }),

            gerarBoletoRenovacao: $resource('mediclab/renovacao/renovacaocontrato/imprimir/boletos', {
                get:{method: "GET"}
            }),
            /*end*/
            funcionarioServicos: $resource('mediclab/funcionarios/:id/servicos', {}, {
                get:  { method: "GET", isArray:true }
            }),

            funcionarioTotem: $resource('mediclab/funcionarios/:id/boautorizacaototem', {id:'@id'}, {
                get:  { method: "GET", isArray:true },
                update:  { method: "PUT"}
            }),

            funcionarioCalendarioSemanal: $resource('mediclab/funcionarios/:id/calendarioSemanal', {}, {
                get:  { method: "GET", isArray:true }
            }),

            funcionarioHorarios: $resource('mediclab/funcionarios/:id/horarios', {}, {
                query:  { method: "GET", isArray:true }
            }),
            saveAssinaturaFuncionario:$resource('mediclab/funcionarios/:id/assinaturadigital', {id:'@id'}, {
                update:  { method: "PUT"}
            }),

            funcionariosDiaSAtendMes: $resource('mediclab/funcionarios/:id/horarios/naoAtende', {}, {
                query:  { method: "GET", isArray:true }
            }),

            bancosAgencias: $resource('mediclab/bancos/:id/agencias', {}),

            empresagruposVendedores: $resource('mediclab/empresagrupos/:id/vendedores', {}),

            empresagruposPlanos: $resource('mediclab/empresagrupos/:id/planos', null, {
                get: {method: "GET", isArray: true}
            }),
            /**/
            planosByEmpresaGrupo: $resource('mediclab/empresagrupos/planos', null, {
                 get: {method: "GET", isArray: true}
            }),
            /**/
            contratosDependentes: $resource('mediclab/contratos/:idcontrato/clientes/:idcliente/dependentes', {}, {
                query:  { method: "GET", isArray:true }
            }),

            contratosFormasPagamentoLiberados: $resource('mediclab/contratos/:id/formasPagamentoLiberados', {}, {
                get:  { method: "GET", isArray:true }
            }),

            servicosProfissionais: $resource('mediclab/servicos/:id/profissionais', {}, {
                query:  { method: "GET", isArray:true }
            }),

            servicosCredenciadas: $resource('mediclab/servicos/:id/credenciadas', {}, {
                query:  { method: "GET", isArray:true }
            }),

            servicosValores: $resource('mediclab/servicos/:id/valores', {}, {
                query:  { method: "GET", isArray:true }
            }),

            getDespesaServicoAutomaticoParaCliente: $resource('mediclab/servicos/:id/despesaServicoDisponivel', {}, {
                get:  { method: "PUT" }
            }),

            parcelasQuitar: $resource('mediclab/parcelas/:id/quitar', {}, {
                quitar:   { method: "PUT" }
            }),

            parcelasDesquitar: $resource('mediclab/parcelas/:id/desquitar', {}, {
                desquitar:   { method: "PUT" }
            }),

            getUltimoAgendamentoFaltoso: $resource('mediclab/agendamentos/:id/ultimofaltoso/:intipopaciente', {id:'@id',intipopaciente:'@intipopaciente'}, {
                query: { method: "GET", isArray:true }
            }),

            getTipoConsulta: $resource('mediclab/servicos/:id/tipoconsulta', {id:'@id'}, {
                query: { method: "GET", isArray:true }
            }),

            getUltimoAgendamentoCobertoValido: $resource('mediclab/servicos/:id/ultimoagendamentocobertovalido', {id:'@id'}, {
                query: { method: "GET" }
            }),

            situacaoFinanceira: $resource('mediclab/cobranca/situacaofinanceira/:id', {id:'@id'}, {
                contratosCliente: {method: "GET", isArray:true}
            }),

            situacaoFinanceiraTitulosAbertos: $resource('mediclab/cobranca/situacaofinanceira/titulos', {}, {
                get: {method: "GET", isArray:true}
            }),

            situacaoFinanceiraTitulosFechado: $resource('mediclab/clientes/:id/situacaofinanceira/quitada', {}, {
                get: {method: "GET", isArray:true}
            }),

            situacaoFinanceiraDependentes: $resource('mediclab/cobranca/situacaofinanceira/dependentes/:cliente/:contrato', {cliente:'@cliente',contrato:'@contrato'}, {
                get: {method: "GET", isArray:true}
            }),


            // folha de pagamento
            folhaPagamento: $resource('mediclab/folha/:id', {id:'@id'}, {
                get:   { method: "GET" },
                query: { method: "GET", isArray:true }
            }),

            folhaPagamentoImportarPorMes: $resource('mediclab/folha/widget/empresa/:idempresafinanceiro/:data', {idempresafinanceiro:'@idempresafinanceiro',data:'@data'}, {
                get:   { method: "GET" }
            }),

            folhaPagamentoGraficoMeses: $resource('mediclab/folha/widget/dozemeses/empresa/:idempresafinanceiro/:data', {idempresafinanceiro:'@idempresafinanceiro',data:'@data'}, {
                gerar:   { method: "GET", isArray:true }
            }),

            folhaPagamentoPeriodoReferencia: $resource('mediclab/folha/dadosvalidacao/empresa/:idempresafinanceiro/:data', {idempresafinanceiro:'@idempresafinanceiro',data:'@data'}, {
                get:   { method: "GET" }
            }),

            folhaPagamentoImportar: $resource('mediclab/folha/dto/:id', {id:'@id'}, {
                get:   { method: "GET" }
            }),

            folhaPagamentoReprova: $resource('mediclab/folha/:id/func/:idfunc/reprova', {id:'@id',idfunc:'@idfunc'}, {
                reprova:  { method: "PUT" }
            }),

            folhaPagamentoAprova: $resource('mediclab/folha/:id/func/:idfunc/aprova', {id:'@id',idfunc:'@idfunc'}, {
                aprova:  { method: "PUT" }
            }),

            folhaUpload: $resource('mediclab/folha/converter', {}, {
                uploadFolha:   { method: "POST" },
            }),

            folhaSave: $resource('mediclab/folha/salvar',{} , {
                save:   { method: "POST" }
            }),

            folhaGrupoFinanceiroGraficoMeses: $resource('mediclab/folha/widget/dozemeses/grupo/:idGrupoFinanceiro/:data', {idGrupoFinanceiro:'@idGrupoFinanceiro',data:'@data'}, {
                gerar:   { method: "GET", isArray:true }
            }),

            folhaAcrescimo: $resource('mediclab/folha/:id/acrescimoFolha',{} , {
                funcionarioAcrescimo:   { method: "PUT" }
            }),

            verificarCodFunc: $resource('mediclab/folha/verificar/duplicados',{} , {
                duplicados:   { method: "PUT", isArray:true }
            }),

            empresasFinanceiro: $resource('mediclab/empresafinanceiro/:id',{id:'@id'},{
                get:   { method: "GET" }
            }),

            empresasFinanceiroFolha: $resource('mediclab/empresafinanceiro/folha',{},{
                query:   { method: "GET", isArray: true }
            }),

            contasPagarCadastradaDataAtual: $resource('mediclab/contaspagar',{} , {
                get:   { method: "GET" },
                query: { method: "GET", isArray:true}
            }),

            contaDespesa: $resource('mediclab/clientes/:id', {id:'@id'}, {
                 update:   { method: "PUT" }
            }),

            agendamentosMedico: $resource('mediclab/agendamentos/:id/agendamentos/medico', {idprofissional: '@idprofissional'}, {
                query:  { method: "GET", isArray:true }
            }),

            profissionaisUnidade: $resource('mediclab/funcionarios/medicos/unidade/:id', {iunidade:'@unidade'}, {
                update:   { method: "PUT" },
                get: {method: "GET"}
            }),

            getTotalPorGrupo: $resource('mediclab/folha/grupo/:intipogrupo/:data/totais', {intipogrupo: '@intipogrupo',data: '@data'}, {
                get:   { method: "GET" }
            }),

            getFluxosFuncByPeriodo: $resource('mediclab/folha/fluxos/:idempresafinanceiro/funcionario/:codigo/:data', {idempresafinanceiro: '@idempresafinanceiro',codigo:'@codigo',data:'@data'}, {
                get:  { method: "GET", isArray:true }
            }),

            getDescontosTotais: $resource('mediclab/folha/widget/descontostotais/empresa/:idempresafinanceiro/:data', {idempresafinanceiro: '@idempresafinanceiro',data:'@data'}, {
                get:  { method: "GET", isArray:true }
            }),


            // Arquivos de funcionários
            arquivosFuncionario: $resource('mediclab/funcionarios/:idFuncionario/arquivos', {idFuncionario: '@idFuncionario'}, {
                get:   { method: "GET" }
            }),

            funcionariosAtivos: $resource('mediclab/funcionarios/ativos', {}, {
                query:  { method: "GET", isArray:true }
            }),

            funcionariosInativos: $resource('mediclab/funcionarios/inativos', {}, {
                query:  { method: "GET", isArray:true }
            }),

            getFuncionarioById: $resource('mediclab/funcionarios/funcionario', {}, {
                get:  { method: "GET"}
            }),

            // ATENDIMENTO MEDICO
            especialidadesProfissional : $resource('mediclab/funcionarios/:idProfissional/especialidade', {idProfissional: '@idProfissional'}, {
                get:   { method: "GET", isArray:true  }
            }),

            agendamentosProfissional : $resource('mediclab/agendamentos/:idProfissional/atendimento/medico', {idProfissional: '@idProfissional'}, {
                get:   { method: "GET", isArray:true  }
            }),

            profissionalAusentarPaciente: $resource('mediclab/agendamentos/:idAgendamento/ausente/:idProfissional', {idAgendamento:'@idAgendamento',idProfissional:'@idProfissional'}, {
                ausenta:  { method: "PUT" }
            }),

            pacienteHistoricoAtendimentos : $resource('mediclab/clientes/:idPaciente/agendamento/:idEspecialidade/historico', {idPaciente: '@idPaciente', idEspecialidade:'@idEspecialidade'}, {
                get:   { method: "GET", isArray:true  }
            }),

            recuperarPreAnamnese : $resource('mediclab/clientes/:idPaciente/preanamnese', {idPaciente: '@idPaciente'}, {
                get:   { method: "GET", isArray:true  }
            }),

            recuperarAnamnese : $resource('mediclab/clientes/:idPaciente/anamnese', {idPaciente: '@idPaciente'}, {
                get:   { method: "GET"  }
            }),

            recuperarQuestionarioPreAnamnese : $resource('mediclab/clientes/anamnese/questionario', {}, {
                get:   { method: "GET" , isArray:true   }
            }),

            atualizarAnamnese : $resource('mediclab/clientes/updateAnamnese', {}, {
                update:   { method: "PUT" }
            }),

            saveAnamnese : $resource('mediclab/clientes/saveanamnese', {}, {
                save:   { method: "POST" }
            }),

            saveAnamneseGeral : $resource('mediclab/atendimentos/:idAgendamento/save', {idAgendamento: '@idAgendamento'}, {
                save:   { method: "POST" }
            }),

            updaterAnamneseGeral : $resource('mediclab/atendimentos/:idAgendamento/update', {idAgendamento: '@idAgendamento'}, {
                update:   { method: "PUT" }
            }),

            recuperarAtendimento: $resource('mediclab/atendimentos/:idAgendamento/atendimento', {}, {
                get:   { method: "GET"  }
            }),
            getTemplate: $resource('mediclab/templateprofissional/medicos/:idFuncionario/especialidades/:idEspecialidade', {idEspecialidade:'@idEspecialidade', idFuncionario: '@idFuncionario'},{
                get: { method: "GET", isArray:true },
                post: {method: 'POST' }
            }),
            getLaudo: $resource('mediclab/laudoatendimento/:idAgendamento/agendamento', {idAgendamento:'@idAgendamento'},{
                get:   { method: "GET"}
            }),
            saveLaudo:  $resource('mediclab/laudoatendimento/:idAgendamento/save', {idAgendamento:'@idAgendamento'}, {
                save:   { method: "POST" }
            }),
            updateLaudo: $resource('mediclab/laudoatendimento/update', {}, {
                update:   { method: "PUT" }
            }),
            saveReceituario:  $resource('mediclab/receituarios/:idAgendamento/save/:idPaciente', {idAgendamento:'@idAgendamento' ,idPaciente: '@idPaciente'}, {
                save:   { method: "POST" }
            }),

            updateReceituario: $resource('mediclab/receituarios/update', {}, {
                update:   { method: "PUT" }
            }),

            recuperarReceituario: $resource('mediclab/receituarios/:idAgendamento/receituario', {idAgendamento:'@idAgendamento'}, {
                get:   { method: "GET"  }
            }),

            /**
            * Query parameter:
            * @nmMedicamento - nome do medicamento a ser buscado
            */
            searchMedicamentos: $resource('mediclab/receituarios/search', null, {
                get: { method: 'GET', isArray: true }
            }),

            checkFinalizacaoAtendimento: $resource('mediclab/atendimentos/agendamentos/:idAgendamento/check_finalizacao', null, {
                get: {method: 'GET'}
            }),

            getAgendamentoView: $resource('mediclab/agendamentos/:idAgendamento/view', null, {
                get: {method: 'GET'}
            }),

            recuperarProfissional: $resource('mediclab/funcionarios/:login/login', {login:'@login'}, {
                get:   { method: "GET"  }
            }),

            getFuncionarioByLogin: $resource('mediclab/funcionarios/:login/login', {login:'@login'}, {
                get:   { method: "GET"  }
            }),

            getServicosEspecialidadeToExame:  $resource('mediclab/funcionarios/:idEspecialidade/especialidade/exames', {idEspecialidade: '@idEspecialidade'}, {
                get:   { method: "GET" , isArray:true   }
            }),

            saveOrcamentoExame:  $resource('mediclab/orcamentos/:idAgendamento/save', {idAgendamento:'@idAgendamento' ,idPaciente: '@idPaciente'}, {
                save:   { method: "POST" }
            }),

            updateOrcamentoExame: $resource('mediclab/orcamentos/update', {}, {
                update:   { method: "PUT" }
            }),

            getOrcamento: $resource('mediclab/orcamentos/:idAgendamento/especialidadeorcamento/:idEspecialidade', {idAgendamento: '@idAgendamento',idEspecialidade:'@idEspecialidade'}, {
                get:   { method: "GET" , isArray:true   }
            }),

            atualizarAgendamento: $resource('mediclab/agendamentos/:idAgendamento/atendido/:idProfissional', {idAgendamento: '@idAgendamento',idProfissional:'@idProfissional'}, {
                  update:   { method: "PUT" }
            }),

            getTriagem: $resource('mediclab/atendimentos/:idPaciente/triageminfo/:inTipoPaciente', {idPaciente:'@idPaciente',inTipoPaciente:'@inTipoPaciente'}, {
                get:   { method: "GET"  }
            }),

            saveReceituarioOftal: $resource('mediclab/receituarios/:idAgendamento/saveoftal/:idPaciente', {idAgendamento:'@idAgendamento' ,idPaciente: '@idPaciente'}, {
                save:   { method: "POST" }
            }),

            updateReceituarioOftal: $resource('mediclab/receituarios/updateoftal', {}, {
                update:   { method: "PUT" }
            }),

            recuperarReceituarioOftal: $resource('mediclab/receituarios/:idAgendamento/receituariooftal', {idAgendamento:'@idAgendamento'}, {
                get:   { method: "GET"  }
            }),

            getHistoricoOftal:  $resource('mediclab/receituarios/:idPaciente/historicooftal/:inTipoPaciente', {idPaciente: '@idPaciente',inTipoPaciente:'@inTipoPaciente'}, {
                get:   { method: "GET" , isArray:true   }
            }),

            // Orçamentos em atendimento

            getOrcamentosCliente:  $resource('mediclab/orcamentos/:idPaciente/orcamento/cliente', {idPaciente: '@idPaciente'}, {
                get:   { method: "GET" , isArray:true   }
            }),

            getServicosOrcamento: $resource('mediclab/orcamentos/:idOrcamento/orcamento/servico', {idOrcamento:'@idOrcamento'}, {
                get:   { method: "GET" , isArray:true   }
            }),

            // Telemarketing
            agendaMedicosEspecialidade: $resource('mediclab/calendario/agenda/medico', {}, {
                get:   { method: "GET" , isArray:true   }
            }),

            drConsultaPaciente : $resource('http://atendimento.uebisite.com.br:3000/chamada', {}, {
                chamar:   { method: "POST",
                            transformResponse: function(data, headers,status){
                            response = {}
                            response.data = data;
                            response.headers = headers();
                            response.status = status;
                            return response;
                    }}
            }),

            registraChamada : $resource('mediclab/funcionarios/registrarchamada', {}, {
                registrar:   { method: "POST"}
            }),

            // RELATORIOS
            relatorioAtendimentoMedico: $resource('mediclab/estatistica/:idProfissional', {idProfissional:'@idProfissional'}, {
                get:   { method: "GET"  }
            }),

            relatorioAtendimentoMedicoPorUnidade: $resource('mediclab/estatistica/unidade/:idUnidade', {idUnidade:'@idUnidade'}, {
                get:   { method: "GET"  }
            }),

            relatorioSalarioMedicoDadosGerais: $resource('mediclab/funcionarios/relatoriomedicoativo', {}, {
                get:   { method: "GET", isArray: true  }
            }),

            relatorioSalarioTipoMedico: $resource ('mediclab/faturamento/profissional', {},{
                get:{method: "GET"}
            }),

            unidadeTrabalhada: $resource ('mediclab/faturamento/unidadetrabalhada', {},{
                get: {method: "GET", isArray: true}
            }),

            relatorioProdutividadeFuncionario: $resource('mediclab/funcionarios/relatorio/agendamentos/:dtInicio/:dtFim', {dtInicio:'@dtInicio',dtFim:'@dtFim'}, {
                get:   { method: "GET", isArray:true }
            }),
              /**
            *   Query parameters:
            * @idUnidade
            */
            previsaoAgendamentos: $resource('mediclab/estatistica/previsao/agendamento', {},
            {
                get: { method: 'GET', isArray: true}
            }),

            /**
            *   Query parameters:
            * @idUnidade
            * @idEspecialidade
            */
            avaliacaoEspMedico: $resource('mediclab/sms/espmedico/:idFuncionario', {},
            {
                get: { method: 'GET', isArray: true}
            }),

            // conversao
            allGrupoOrgaos: $resource('mediclab/gruposOrgaos', null, {
                get: {method: 'GET', isArray: true}
            }),

            allOrgaosPublicos: $resource('mediclab/orgaos', null, {
                get: {method: 'GET', isArray: true}
            }),

            orgaoByEsfera: $resource('mediclab/orgaos/inEsfera/:inEsfera', {id: '@inEsfera'}, {
                get: {method: 'GET', isArray: true}
            }),

            getEmpresaGrupo: $resource('mediclab/empresagrupos', null, {
                get: {method: 'GET', isArray: true}
            }),

            allBancos: $resource('mediclab/bancos', null, {
                get: {method: 'GET', isArray: true}
            }),

            allAgenciasBanco: $resource('mediclab/bancos/:idBanco/agencias', null, {
                get: {method: 'GET', isArray: true}
            }),

            /**
            * Query parameters
            * idCliente - idCliente a ser incluido no contrato
            * idsDependentes - lista de ids dos dependentes a serem incluidos no contrato
            * idAssinatura - id da assinatura recorrent na Vindi
            */
            createContrato: $resource('mediclab/contratos', null, {
                post: {method: 'POST'}
            }),

            checkCartao: $resource('mediclab/vendas/clientes/:card', null, {
                get: {method: 'GET'}
            }),

            criarAssinatura: $resource('mediclab/vendas/assinatura', null, {
                post: {method: 'POST'}
            }),

            // dashboard controle unidades
            dashboardControleUnidadesData: $resource('mediclab/dashboard/monitoramento/profissional', null, {
                get: { method: 'GET' }
            }),

            dashboardControleUnidadesAtendimentoData: $resource('mediclab/dashboard/monitoramento/atendimento', null, {
                get: { method: 'GET' }
            }),

            getAssociados: $resource('mediclab/empresasClientes/clientesconveniada', {}, {
                get: {method: "GET" }
            }),

            getAssociadoByCpf: $resource('mediclab/clientes/:cpf/clientecpf', {}, {
                get: {method: "GET"}
            }),

            inactivateAssociado: $resource('mediclab/empresasClientes/inativarcontrato/:id', {id: '@id'}, {
                put: {method: "PUT" }
            }),

            getAssociadosByIdCliente: $resource('mediclab/empresasClientes/dependentes/:idcliente', {idcliente: '@idcliente'}, {
                get: {method: "GET", isArray:true}
            }),

            getDependentesByIdContrato: $resource('mediclab/empresasClientes/dependentes/by_contrato/:id', {id: '@id'}, {
                get: {method: "GET", isArray:true}
            }),

            addNewDependente: $resource('mediclab/empresasClientes/adddependente/:idcliente', {idcliente: '@idcliente'}, {
                post: {method: "POST"}
            }),

            inactivateDependente: $resource('mediclab/empresasClientes/:iddependente/inativarcontratodependente/:idcliente', {idcliente: '@idcliente', iddependente: '@iddependente'}, {
                put: {method: "PUT"}
            }),

            reactivateDependente: $resource('mediclab/empresasClientes/adddependente/:idcliente', {idcliente: '@idcliente'}, {
                post: {method: "POST"}
            }),

            getDependente: $resource('mediclab/dependentes/:iddependente', {}, {
                get: { method: "GET"}
            }),

            updateDependente: $resource('mediclab/dependentes/:iddependente', {}, {
                put: { method: "PUT"}
            }),

            getServicosByConveniada: $resource('mediclab/empresasClientes/servicos'
            ),

            guiaSave: $resource('mediclab/guia', {}, {
                post: {
                    method: "POST",
                    interceptor: {
                        response: function(response) {
                            var result = response.resource;
                            result.$status = response.status;
                            return result;
                        }
                    }
                }
            }),

            getCompany: $resource('mediclab/unidades/2', {}, {
                get: {method: "GET"}
            }),

            countAgendamentosPaciente: $resource('mediclab/agendamentos/pacientes/:idPaciente/tipos/:tipoPaciente/quantidade', null, {
                get: {method: "GET" }
            }),

            updateLaudoRegistroAgendamento: $resource('mediclab/registroagendamento/registros', {idagendamento: '@idagendamento', dtfatlaudo: '@dtfatlaudo'}, {
                put: { method: "PUT" }
            }),

            searchMedico: $resource('mediclab/funcionarios/medicos', {nmMedico: '@nmMedico'}, {
                get: { method: "GET", isArray: true}
            }),

            especialidadesMedico: $resource('mediclab/funcionarios/medicos/:idMedico/especialidades', {idMedico: '@idMedico'}, {
                get: { method: "GET", isArray: true }
            }),

            templateLaudoMedico: $resource('mediclab/templateprofissional/:idTemplate', null, {
                put: { method: 'PUT' },
                delete: { method: 'DELETE' }
            }),

            templatesLaudoForEspProfissional: $resource('mediclab/templateprofissional/medicos/:idMedico/especialidades/:idEspecialidade', null, {
                get: { method: 'GET', isArray: true },
                post: { method: 'POST' }
            }),

            templatesReceituarioForEspProfissional: $resource('mediclab/receituarios/medicos/:idMedico/especialidades/:idEspecialidade/templates',
                {idMedico: '@idMedico', idEspecialidade: '@idEspecialidade'}, {
                    get: { method: 'GET', isArray: true },
                    post: { method: 'POST' }
                }),

            templatesReceituarioForAgendamento: $resource('mediclab/receituarios/templates/agendamentos/:idAgendamento', {idAgendamento: '@idAgendamento'}, {
                get: { method: 'GET', isArray: true }
            }),

            templateReceituario: $resource('mediclab/receituarios/templates/:idTemplate', {idTemplate: '@idTemplate'}, {
                get: { method: 'GET' },
                put: { method: 'PUT' },
                delete: { method: 'DELETE' }

            }),

            updateLaudoRegistroAgendamento: $resource('mediclab/registroagendamento/registros', {idagendamento: '@idagendamento', dtfatlaudo: '@dtfatlaudo'}, {
                put: {method: "PUT" }
            }),

            getGuiasByConveniada: $resource('mediclab/empresasClientes/guias', {}, {
                query: {method: "GET"}
            }),

            getGuia: $resource('mediclab/empresasClientes/guias/:id', {}, {
                get: {method: "GET"}
            }),

            deleteGuiaServico: $resource('mediclab/empresasClientes/guias/guia_servico/:id', {}, {
                delete: {method: "DELETE"}
            }),

            getGuias: $resource('mediclab/guia/cliente', {}, {
                query: {method: "GET"}
            }),

            getGuiaFromAdm: $resource('mediclab/guia/:id', {id:'@id'}, {
                query: {method: "GET"}
            }),

            getStatusItemOfGuia: function(params)
            {
                var lStatus = ["Criado - Em aberto", "Agendado", "Consumido", "Quitado", "Inativado", "Vencido"];
                if(params && (params.id === 0 || params.id))
                {
                    if(!(typeof lStatus[params.id] === 'undefined'))
                    {
                        return lStatus[params.id];
                    } else
                    {
                        return "Desconhecido";
                    }
                }
                return lStatus;
            },

            deleteGuia: $resource('mediclab/guia/:id', {}, {
                delete: {method: "DELETE"}
            }),

            getGuiaServicos: $resource('mediclab/guia_servico', {}, {
                query: {method: "GET"}
            }),

            searchDespesaByGuiaServico: $resource('mediclab/despesas/search_single/guia_servico/:id', {}, {
                get: {method: "GET"}
            }),

            getSummaryServicoByStatus: $resource
            ('mediclab/guia_servico/summary_servico_by_status', {}, {
                query: {method: "GET", isArray:true}
            }),

            getSummaryGuiaServicoByServico: $resource
            ('mediclab/guia_servico/summary_guia_servico_by_servico', {}, {
                query: {method: "GET", isArray:true}
            }),

            searchGuiaByIdDespesaServico: $resource('mediclab/despesa_guia_servico/search_single/despesa_servico/:id', {}, {
                get: {method: "GET"}
            }),

            getRelatorioFaturamentoConveniada: $resource('mediclab/faturamento/conveniada', null, {
                get: {method: "GET", isArray: true}
            }),

            reactivateAssociado: $resource('mediclab/empresasClientes/reativarcontrato/:id', {id: '@id'}, {
                put:{method: "PUT"}
            }),

            getHistoryConveniada: $resource('mediclab/faturamento/conveniada/mp', {}, {
                query:{method: "GET", isArray:true}
            }),

            getCoberturaPlano: $resource('mediclab/empresasClientes/coberturaplano', {}, {
                query:{method: "GET", isArray:true}
            }),

            //gerarFaturamentoConveniada
            getPeriodoConv: $resource('mediclab/faturamento/conveniada/periodofatura', {}, {
                get: {method: "GET", isArray: true}
            }),

            gerarFaturamentoCon: $resource('mediclab/despesas/gerafaturaconvenio', {}, {
                save: {method: "POST"}
            }),

            reabrirFaturamentoCon: $resource('mediclab/despesas/reabrirfaturaconvenio', {},{
                put:{method: "PUT"}
            }),

            getAddressByCep: $resource('mediclab/bairros/find_address_by_cep', {},{
                query: {method: "GET"}
            }),

            getCidadeByNm: $resource('mediclab/cidades/find_by_nm', {},{
                query: {method: "GET"}
            }),

            getBairroByNm: $resource('mediclab/bairros/find_by_nm', {},{
                query: {method: "GET"}
            }),

            reactiveGuia: $resource('mediclab/guia/reactive/:id', {id: '@id'}, {
                put: { method: "PUT"}
            }),

            getClienteByIdContrato: $resource('mediclab/clientes/:id/contrato', {id: '@id'}, {
                get: {method: "GET"}
            }),

            addContratoDependente: $resource('mediclab/empresasClientes/adddependente/by_contrato/:id', {id: '@id'}, {
                post: {method: "POST"}
            }),

            getDependentesByIdContratoNonExclude: $resource('mediclab/empresasClientes/dependentes/by_contrato/non_exclude/:id', {id: '@id'}, {
                get: {method: "GET", isArray:true}
            }),

            inactiveContratoDependente: $resource('mediclab/empresasClientes/inactive_contratodependente/:id', {id: '@id'}, {
                put: {method: "PUT"}
            }),
            getDetalhesFatura: $resource('mediclab/faturamento/conveniada/detalhes_fatura/:period/:id', {id: '@id', period: '@period'}, {
                get: {method: "GET"}
            }),
            //relatorio agendamento médico
            grupofuncionario: $resource('mediclab/funcionarios/grupofuncionario',{},{
                get: {method: "GET", isArray: true}            
            }),
            statusAgendamento: $resource('mediclab/agendamentos/statusagendamento', {}, {
                get:{method:"GET", isArray: true}
            }),
            relatorioAgendamento: $resource('mediclab/agendamentos/relatorioagendamento', {}, {
                get: {method: "GET"/*, isArray: true*/}
            }),
            /*add 24/04/2018*/
            empresaClienteByCnpj: $resource('mediclab/empresasClientes/cnpj/:cnpj', {cnpj: '@cnpj'},{
                get: {method:"GET", isArray: true}
            }),
            /**/

            registerContrachequeInfo: $resource('mediclab/agendamentos/:idAgendamento/contracheque/info', {}, {
                put: { method: "PUT" }
            })
        }
    }
]);
