MedicsystemApp.factory('CortesiaService', function($resource) {
	return {
		getAll: $resource('/mediclab/cortesias/all', {dtInicio: '@dtIniciao', dtFim: '@dtFim'}, {
			get: { method: "GET", isArray: true } 
		}),

		liberar: $resource('/mediclab/cortesias/liberarcortesia', null, {
			put: { method: "PUT" }
		}),

		averiguar: $resource('/mediclab/cortesias/:idCortesia/updatestatuscortesia/:inStatus', null, {
			put: { method: "PUT" }
		})
	}
});