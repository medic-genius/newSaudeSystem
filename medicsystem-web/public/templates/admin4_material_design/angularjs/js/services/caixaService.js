MedicsystemApp.factory('caixaFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {
        
            empresaGrupoCompartilhado: $resource('mediclab/caixa/empresas/:id', {}, {
                query: { method: "GET", isArray:true },
            }),

            unidadeEmpresaGrupoCompartilhado: $resource('mediclab/caixa/unidades', {}, {
                query: { method: "GET", isArray:true },
            }),

            operadorCaixa: $resource('mediclab/caixa/operadores', {}, {
                query: { method: "GET", isArray:true},
            }),

            caixaByEmpresaFinanceiro: $resource('mediclab/caixa', {}, {
                query: { method: "GET", isArray:true },
            }),

            movimentacaoCaixa: $resource('mediclab/caixa/movimentacoes', {}, {
                query: { method: "GET"},
            }),

            contasBancariasDinheiro: $resource('mediclab/contabancaria/empresafinanceirodinheiro/:idEmpresaFinanceiro', {idEmpresaFinanceiro:'@idEmpresaFinanceiro'}, {
                query: { method: "GET", isArray:true }
             }),

            conferirCaixa: $resource('mediclab/caixa/conferir', {}, {
                query: { method: "GET"},
            }),

            desconferirCaixa: $resource('mediclab/caixa/desconferir', {}, {
                query: { method: "GET"},
            }),

            movimentacaoEntradaCB:$resource('mediclab/movimentacaoconta/savemovimentacaocaixa', {}, {
                get: { method: "GET"},
            }), 

        }
    }
]);