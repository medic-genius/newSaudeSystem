MedicsystemApp.factory('triagemFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {

            unidades: $resource('mediclab/unidades', {}, {
                query:  { method: "GET", isArray:true },
            }),

            unidadesEspecialidades: $resource('mediclab/unidades/:id/especialidades', {}, {
                get:    { method: "GET", isArray: true }
            }),

            especialidadeProfissionais: $resource('mediclab/especialidades/:id/profissionais', {id:'id'}, {
                query:  { method: "GET", isArray:true }
            }),
            
            getFuncionarioByLogin: $resource('mediclab/funcionarios/:login/login', {login:'@login'}, {
                get:   { method: "GET"  }
            }),

            getAtendimentoEnfermagem: $resource('mediclab/agendamentos/atendimento/:idUnidade/enfermagem', {idUnidade:'@idUnidade'}, {
                get:   { method: "GET", isArray: true  }
            }),

            getProfissionaisByEsp: $resource('mediclab/especialidades/profissionais/triagem', {}, {
                get:   { method: "GET", isArray: true  }
            }),

            getDadosTriagemPaciente: $resource('mediclab/atendimentos/:id/triageminfo/:intipopaciente', {id:'@id',intipopaciente:'@intipopaciente'}, {
                get:   { method: "GET"  }
            }),

            triagem: $resource('mediclab/atendimentos/:id/updatetriagem/:idagendamento/:intipopaciente', {id:'@id',idagendamento:'@idagendamento',intipopaciente:'@intipopaciente'}, {
                update:   { method: "PUT" }
            }),

            triagemStatus: $resource('mediclab/agendamentos/:id/update/statustriagem', {id:'@id'}, {
                update:   { method: "PUT" }
            }),

        }
    }
]);