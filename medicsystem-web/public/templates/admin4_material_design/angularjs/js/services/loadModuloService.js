var MODULOS = function() {
    return {

        load_angularBusy: {
            name: 'cgBusy',
            insertBefore: '#ng_load_plugins_before',
            files: [
                'assets/global/plugins/angularjs/plugins/angular-busy/angular-busy.min.css',
                'assets/global/plugins/angularjs/plugins/angular-busy/angular-busy.min.js'
            ]   
        },

        load_angularSlider: {
          name: 'rzModule',
          insertBefore: '#ng_load_plugins_before',
          files: [
            'assets/global/plugins/angularjs/plugins/angular-slider/rzslider.min.css',
            'assets/global/plugins/angularjs/plugins/angular-slider/rzslider.min.js'
          ]     
        },

        load_fileSystem: {
            name : 'MedicsystemApp',
            insertBefore : '#ng_load_plugins_before', 
            files : [
                'assets/admin/pages/scripts/index3.js',
                'assets/admin/pages/css/tasks.css'
            ]
        },
                
        load_inputMask: {
            name : 'ui.utils.masks',
            insertBefore : '#ng_load_plugins_before', 
            files : [
                'assets/global/plugins/angularjs/plugins/angular-input-masks/angular-input-masks-standalone.js',
            ]    
        },

        load_jsonExport : {
            name : 'ngJsonExportExcel',
            insertBefore : '#ng_load_plugins_before', 
            files : [
                'assets/global/plugins/angularjs/plugins/json-export/FileSaver.min.js',
                'assets/global/plugins/angularjs/plugins/json-export/json-export-excel.min.js',
            ]  
        },

        load_ngTable : {
            name: 'ngTable',
            insertBefore: '#ng_load_plugins_before',
            files: [
                'assets/global/plugins/angularjs/plugins/ng-table/ng-table-new/ng-table.css',
                'assets/global/plugins/angularjs/plugins/ng-table/ng-table-new/ng-table.js',
            ]
        },

        load_ngValidate: {
            name: 'ngValidate',
            insertBefore: '#ng_load_plugins_before',
            files: [
                'assets/global/plugins/angularjs/plugins/ng-validate/angular-validate.min.js',
            ]   
        },

        load_toastr : {
            name : 'toastr',
            insertBefore : '#ng_load_plugins_before', 
            files : [
                'assets/global/plugins/angularjs/plugins/angular-toastr/angular-toastr.css',
                'assets/global/plugins/angularjs/plugins/angular-toastr/angular-toastr.tpls.js',
             ]  
        },

        load_uiSelect : {
            name: 'ui.select',
            insertBefore: '#ng_load_plugins_before',
            files: [
                'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                'assets/global/plugins/select2/select2.css',
                'assets/global/plugins/select2/select2-bootstrap.css'
            ]
        },

        load_uiTree : {
            name : 'ui.tree',
            insertBefore : '#ng_load_plugins_before', 
            files : [
                'assets/global/plugins/angularjs/plugins/angular-ui-tree/angular-ui-tree.css',
                'assets/global/plugins/angularjs/plugins/angular-ui-tree/angular-ui-tree.js',
            ]  
        },

    };
}();
