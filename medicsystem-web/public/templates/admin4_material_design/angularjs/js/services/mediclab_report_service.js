MedicsystemApp.factory('mlReportFactory', ['$resource',
    function($resource) {
        return {
            updatelatitudelongitude: $resource('mediclab/clientes/updatelatitudelongitude', {}, {
                update:   { method: "PUT" }
            }),
            latitudelongitude: $resource('mediclab/clientes/latitudelongitude', {}, {
                query: { method:'GET', isArray:true }
            }),
            enderecosmapa: $resource('mediclab/clientes/mapa', {}, {
                query: { method:'GET', isArray:true }
            }),
            report0001: $resource('mediclab/dashboard/reports/report0001', {}, {
                query: { method:'GET' }
            }),
            report0002: $resource('mediclab/dashboard/reports/report0002', {}, {
                query: { method:'GET' }
            }),
            report0003: $resource('mediclab/dashboard/reports/report0003', {}, {
                query: { method:'GET' }
            }),
            report0004: $resource('mediclab/dashboard/reports/report0004', {}, {
                query: { method:'GET' }
            }),
            report0005: $resource('mediclab/dashboard/reports/report0005', {}, {
                query: { method:'GET' }
            }),
            report0006: $resource('mediclab/dashboard/reports/report0006', {}, {
                query: { method:'GET', isArray: true }
            }),
            report0007: $resource('mediclab/dashboard/reports/report0007', {}, {
                query: { method:'GET' }
            }),
            report0008: $resource('mediclab/dashboard/reports/report0008/:idPlano', {idPlano: '@idPlano'}, {
                query: { method:'GET', isArray: true }
            }),
            report0009: $resource('mediclab/dashboard/reports/report0009', {}, {
                query: { method:'GET' }
            }),
            report0010: $resource('mediclab/dashboard/reports/report0010', {}, {
                query: { method:'GET' }
            }),
            report0011: $resource('mediclab/dashboard/reports/report0011', {}, {
                query: { method:'GET' }
            }),
            report0012: $resource('mediclab/dashboard/reports/report0012', {}, {
                query: { method:'GET' }
            }),
            report0013: $resource('mediclab/dashboard/reports/report0013', {}, {
                query: { method:'GET' }
            }),
            report0014: $resource('mediclab/dashboard/reports/report0014', {}, {
                query: { method:'GET' }
            }),
            report0015: $resource('mediclab/dashboard/reports/report0015', {}, {
                query: { method:'GET' }
            }),
            
            //Dental
            dentalreport0001: $resource('mediclab/dashboard/reports/dental/report0001', {}, {
                query: { method:'GET' }
            }),
            dentalreport0002: $resource('mediclab/dashboard/reports/dental/report0002', {}, {
                query: { method:'GET' }
            }),
            dentalreport0003: $resource('mediclab/dashboard/reports/dental/report0003', {}, {
                query: { method:'GET' }
            }),
            dentalreport0004: $resource('mediclab/dashboard/reports/dental/report0004', {}, {
                query: { method:'GET' }
            }),
            dentalreport0005: $resource('mediclab/dashboard/reports/dental/report0005', {}, {
                query: { method:'GET' }
            }),
            dentalreport0006: $resource('mediclab/dashboard/reports/dental/report0006', {}, {
                query: { method:'GET', isArray: true }
            }),
            dentalreport0007: $resource('mediclab/dashboard/reports/dental/report0007', {}, {
                query: { method:'GET' }
            }),
            dentalreport0008: $resource('mediclab/dashboard/reports/dental/report0008/:idPlano', {idPlano: '@idPlano'}, {
                query: { method:'GET', isArray: true }
            }),
            dentalreport0009: $resource('mediclab/dashboard/reports/dental/report0009', {}, {
                query: { method:'GET' }
            }),
            dentalreport0010: $resource('mediclab/dashboard/reports/dental/report0010', {}, {
                query: { method:'GET' }
            }),
            dentalreport0011: $resource('mediclab/dashboard/reports/dental/report0011', {}, {
                query: { method:'GET' }
            }),
            dentalreport0012: $resource('mediclab/dashboard/reports/dental/report0012', {}, {
                query: { method:'GET' }
            }),
            dentalreport0013: $resource('mediclab/dashboard/reports/dental/report0013', {}, {
                query: { method:'GET' }
            }),
            
            //Academia
            academiareport0001: $resource('mediclab/dashboard/reports/academia/report0001', {}, {
                query: { method:'GET' }
            }),
            academiareport0002: $resource('mediclab/dashboard/reports/academia/report0002', {}, {
                query: { method:'GET' }
            }),
            academiareport0003: $resource('mediclab/dashboard/reports/academia/report0003', {}, {
                query: { method:'GET' }
            }),
            academiareport0004: $resource('mediclab/dashboard/reports/academia/report0004', {}, {
                query: { method:'GET' }
            }),
            academiareport0005: $resource('mediclab/dashboard/reports/academia/report0005', {}, {
                query: { method:'GET' }
            }),
            academiareport0006: $resource('mediclab/dashboard/reports/academia/report0006', {}, {
                query: { method:'GET', isArray: true }
            }),
            academiareport0007: $resource('mediclab/dashboard/reports/academia/report0007', {}, {
                query: { method:'GET' }
            }),
            academiareport0008: $resource('mediclab/dashboard/reports/academia/report0008/:idPlano', {idPlano: '@idPlano'}, {
                query: { method:'GET', isArray: true }
            }),
            academiareport0009: $resource('mediclab/dashboard/reports/academia/report0009', {}, {
                query: { method:'GET' }
            }),
            academiareport0010: $resource('mediclab/dashboard/reports/academia/report0010', {}, {
                query: { method:'GET' }
            }),
            academiareport0012: $resource('mediclab/dashboard/reports/academia/report0012', {}, {
                query: { method:'GET' }
            }),
            academiareport0016: $resource('mediclab/dashboard/reports/academia/report0016',{},{
                query: {method:'GET'}
            }),
            academiareport0017: $resource('mediclab/dashboard/reports/academia/report0017',{},{
                query: {method: 'GET'}
            }),
            academiareport0018: $resource('mediclab/dashboard/reports/academia/report0018',{},{
                query: {method: 'GET'}
            }),
            academiareport0019: $resource('mediclab/dashboard/reports/academia/report0019',{},{
                query: {method: 'GET'}
            })
        }
    }
]);