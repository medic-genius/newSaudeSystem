MedicsystemApp.factory('cadastroFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {           
            
            createAgencia: $resource('mediclab/agencias', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            createBanco: $resource('mediclab/bancos', {}, {
                save: {method: 'POST'},
            }),

            createBairro: $resource('mediclab/bairros', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            createCidade: $resource('mediclab/cidades', {}, {
                save: {method: 'POST'},
            }),

            createOrgao: $resource('mediclab/orgaos', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            createFuncionario: $resource('mediclab/funcionarios', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            createServico: $resource('mediclab/servicos', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            createEmpresa: $resource('mediclab/empresasClientes', {}, {
                save: {method: 'POST'},
                update: {method: 'PUT'}
            }),

            ativarEmpresa: $resource('mediclab/empresasClientes/ativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),
            
            inativarEmpresa: $resource('mediclab/empresasClientes/inativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),

            dadosEmpresaCliente: $resource('mediclab/empresasClientes/clientecontratoview/:id', {id:'@id'}, {
                get: {method: 'GET', isArray:true}
            }),

            ativarFuncionario: $resource('mediclab/funcionarios/ativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),
            
            inativarFuncionario: $resource('mediclab/funcionarios/inativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),

            inativarServico: $resource('mediclab/servicos/inativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),

            inativarOrgao: $resource('mediclab/orgaos/inativar/:id', {id:'@id'}, {
                update: {method: 'PUT'}
            }),
        }
    }
]);