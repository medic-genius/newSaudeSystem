MedicsystemApp.factory('administrativoFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {           
            
            clientesContratos: $resource('mediclab/negociacao/clientescontratos', {}, {
                get: { method: "GET", isArray:true},
            }), 

            cadastrarClienteSuperLogica: $resource('mediclab/superlogica/cliente/cadastrarcliente', {}, {
                salvar: { method: "POST", isArray:true},
            }), 

            cadastrarCobrancaSuperLogica: $resource('mediclab/superlogica/cobranca/cadastarcobranca', {}, {
                salvar: { method: "POST", isArray:true},
            }), 

            imprimirBoletoSuperLogica: $resource('mediclab/superlogica/cobranca/boletospdf', {}, {
                get: { method: "POST"},
            }), 

            clientes: $resource('mediclab/clientes/:id', {id:'@id'}, {
                 update:   { method: "PUT" }
            }),

        }
    }
]);