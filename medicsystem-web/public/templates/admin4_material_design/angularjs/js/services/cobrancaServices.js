MedicsystemApp.factory('cobrancaFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {

        	clientes: $resource('mediclab/clientes/:id', {id:'@id'}, {
                 update:   { method: "PUT" },
                 get: { method: 'GET' }
            }),

            fotoCliente: $resource('mediclab/clientes/:id/foto/cliente', {id:'@id'}, {
            }),

            clienteArquivos: $resource('mediclab/clientes/:id/arquivos', {}, {
                get:  { method: "GET", isArray:true }
            }),

            cidades: $resource('mediclab/cidades/:id', {}, {
                query:  { method: "GET", isArray:true },
            }),

            cidadeBairros: $resource('mediclab/cidades/:id/bairros', {}, {
                get:  { method: "GET", isArray:true }
            }),

            empresagrupos: $resource('mediclab/empresagrupos', {}, {
                get:   { method: "GET", isArray:true }
            }),

            clientesInadimplentes: $resource('mediclab/cobranca/clientes', {}, {
                 get:   { method: "GET" }
            }),

            clientesInadimplentesXLS: $resource('mediclab/cobranca/clientes/xls', {}, {
                 get:   { method: "GET" }
            }),

            clienteHistoricoSimplificado: $resource('mediclab/cobranca/contatos/:id', {id:'@id'}, {
                 get:   { method: "GET", isArray:true }
            }),

            tiposContatoCliente: $resource('mediclab/cobranca/contatosoptions', {}, {
                get:   { method: "GET", isArray:true }
            }), 

            contatoCliente: $resource('mediclab/cobranca/contato/:id', {id:'@id'}, {
                save: { method: "POST"},
                get: {method: "GET"}
            }),

            contatoClienteAberto: $resource('mediclab/cobranca/contato/aberto/:id', {id:'@id'}, {
                get: {method: "GET"}
            }), 

            contatoClienteFinalizar: $resource('mediclab/cobranca/contato/finalizar/:id', {id:'@id'}, {
                finalizar: {method: "POST"}
            }), 

            situacaoFinanceira: $resource('mediclab/cobranca/situacaofinanceira/:id', {id:'@id'}, {
                contratosCliente: {method: "GET", isArray:true}
            }), 

            situacaoFinanceiraDependentes: $resource('mediclab/cobranca/situacaofinanceira/dependentes/:cliente/:contrato', {cliente:'@cliente',contrato:'@contrato'}, {
                get: {method: "GET", isArray:true}
            }), 

            situacaoFinanceiraTitulosAbertos: $resource('mediclab/cobranca/situacaofinanceira/titulos', {}, {
                get: {method: "GET", isArray:true}
            }), 

            totalCobrancaFuncionario : $resource('mediclab/cobranca/quantidadefuncionario', {}, {
                get: {method: "GET"}
            }), 

            // Gerar Boletos
            empresaGrupoCompartilhado: $resource('mediclab/caixa/empresas/:id', {}, {
                query: { method: "GET", isArray:true },
            }),

            clientesInadimplentesCompartilhado: $resource('mediclab/cobranca/compartilhado/inadimplentes', {}, {
                 get:   { method: "GET" }
            }),

            ccbrancaClienteAvulsa: $resource('mediclab/superlogica/cobranca/cadastarcobrancaavulsa', {}, {
                gerar:   { method: "POST", isArray:true }
            }),
            
            ccbrancaClienteAvulsaPeriodo: $resource('mediclab/superlogica/cobranca/cadastarcobrancaavulsaperiodo', {}, {
                gerar:   { method: "POST", isArray:true }
            }),

            cadastrarClienteSuperLogica: $resource('mediclab/superlogica/cliente/cadastrarcliente', {}, {
                salvar: { method: "POST", isArray:true},
            }), 

            cadastrarClienteSuperLogicaCompartilhado: $resource('mediclab/superlogica/cliente/compartilhado/cadastrarcliente', {}, {
                salvar: { method: "POST", isArray:true},
            }),

            cobrancaTitulosCliente: $resource('mediclab/superlogica/cobranca/createcobrancaavulsacliente',{}, 
            {
                post: { method: 'POST', isArray: true }
            }),
            
            mensalidadesInadimplentesCompartilhado: $resource('mediclab/cobranca/clientes/mensalidades', {}, {
                get: {method: "GET", isArray:true}
            }), 
            
            mensalidadesInadimplentesCompartilhadoPeriodo: $resource('mediclab/cobranca/clientes/mensalidadesperiodo', {}, {
                get: {method: "GET", isArray:true}
            }), 

            cobrancaMensalidadeCompartilhado: $resource('mediclab/cobranca/mensalidade/:id/cobranca', {id:'@id'}, {
                get: {method: "GET", isArray:true}
            }), 

            imprimirBoletoSuperLogica: $resource('mediclab/superlogica/cobranca/boletospdf', {}, {
                get: { method: "POST"},
            }), 
            
            ligar: $resource('http://128.201.136.141/liga.php', {numero:'@numero', ramal:'@ramal'}, {
                get: { method: "GET"},
            }), 
        }
    }
]);