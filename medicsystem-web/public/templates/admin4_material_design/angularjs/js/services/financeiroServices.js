MedicsystemApp.factory('financeiroFactory', ['$resource', '$rootScope',
    function($resource, $rootScope) {
    
        return {

        	// BEGIN FORNECEDOR
        	cidades: $resource('mediclab/cidades/:id', {}, {
                get:    { method: "GET" },
                query:  { method: "GET", isArray:true },
                save:   { method: "POST" },
                remove: { method: "DELETE" }
            }),

            cidadeBairros: $resource('mediclab/cidades/:id/bairros', {}, {
                get:  { method: "GET", isArray:true }
            }),

            fornecedor: $resource('mediclab/fornecedor/:id', {}, {
                save:  { method: "POST"},
                query: { method: "GET", isArray:true },
                edit: { method: "PUT"},
                remove: { method: "DELETE" }
            }),

            fornecedorReativar: $resource('mediclab/fornecedor/:id/reativar', {id:'@id'}, {
                reativar:  { method: "PUT" }
            }),

            // BEGIN CENTRO CUSTO
            centroCusto:  $resource('mediclab/centrocusto/:id', {}, {
                save:  { method: "POST"},
                query: { method: "GET", isArray:true },
                edit: { method: "PUT"},
                remove: { method: "DELETE" }
            }),
            centroCustoAll: $resource('mediclab/centrocusto/all', {}, {
                query: { method: "GET", isArray:true }
            }),
            
            allCentroCusto: $resource ('mediclab/centrocusto/allcentrocusto', {},{
                get: {method: "GET", isArray:true}
            }),
            buscaCentroCusto: $resource ('mediclab/centrocusto/busca', {},{
                query: {method: "GET", isArray: true}
            }),

            // BEGIN CONTA CONTABIL
            contaContabil: $resource('mediclab/contacontabil/:id', {}, {
                save:  { method: "POST"},
                query: { method: "GET", isArray:true },
                edit: { method: "PUT"},
                remove: { method: "DELETE" }
            }),
            contaContabilAll: $resource('mediclab/contacontabil/all', {}, {
                query: { method: "GET", isArray:true }
            }),
            buscaContaContabil: $resource('mediclab/contacontabil/busca', {}, {
                query: {method: "GET", isArray: true}
            }),

            allContasContabil : $resource('mediclab/contacontabil/allcontacontabil', {},{
                get: {method: "GET", isArray: true}
            }),
            
            //BEGIN DESPESA FINANCEIRO CADASTRO
            empresaFinanceiro: $resource('mediclab/empresafinanceiro/:id', {}, {
                query: { method: "GET", isArray:true },
            }),

            efContasBancaria: $resource('mediclab/contabancaria/empresafinanceiro/:idEmpresaFinanceiro', {idEmpresaFinanceiro:'@idEmpresaFinanceiro'}, {
                get: { method: "GET", isArray:true },
            }),

            contasBancariasConc: $resource('mediclab/contabancaria/empfinanceiroconc/:idEmpresaFinanceiro', {idEmpresaFinanceiro: '@idEmpresaFinanceiro'}, {
                get: {method: "GET", isArray: true},
            }),

            cadastroContasBanc: $resource('mediclab/contabancaria/create', {},{
                save: {method: "POST"},
            }),

            frequenciaOptions: $resource('mediclab/despesafinanceiro/frequencia', {}, {
                get: { method: "GET", isArray:true },
            }),

            despesaFinanceiro: $resource('mediclab/despesafinanceiro/:id', {}, {
                save:  { method: "POST"},
                query: { method: "GET", isArray:true },
                edit: { method: "PUT"},
                remove: { method: "DELETE" }
            }),

            dfByEmpresaFinanceiro: $resource('mediclab/despesafinanceiro/empresafinanceiro/:id', {}, {
                query: { method: "GET", isArray:true },
            }),

            dfByNrDocumento: $resource('mediclab/despesafinanceiro/documento/:documento', { documento:'@documento' }, {
                get: { method: "GET", isArray:true},
            }),
                /**/
            pagamentoOptions: $resource('mediclab/despesafinanceiro/formapagamento', {},{
                get: {method: "GET", isArray: true},
            }),

            despesasFin: $resource('mediclab/despesafinanceiro/salvardespesafin', {},{
                save: { method: "POST", isArray: true},
                query: { method: "GET", isArray: true}
            }),

            // visao geral
            qtdCard: $resource('mediclab/despesafinanceiro/quantidade', {}, {
                get: { method: "GET"},
            }),

            graficoByEmpresaFinanceiro: $resource('mediclab/despesafinanceiro/graficobyempresa/:id', {id:'@id'}, {
                get: { method: "GET", isArray:true},
            }),

            // upload anexos
            arquivosDF:  $resource('mediclab/despesafinanceiro/:iddespesa/arquivos', {iddespesa:'@iddespesa'}, {
                upload: { method: "GET"},
            }),

            arquivoDfDeletar: $resource('mediclab/despesafinanceiro/arquivo/:id', {id:'@id'}, {
                deletar: { method: "DELETE"},
            }),

            // relatorios
            movimentacaoPorDia: $resource('mediclab/movimentacaoconta', {}, {
                get: { method: "GET", isArray:true},
            }),

            despesasPorNrServico : $resource('mediclab/despesafinanceiro/servico/:servico', {servico:'@servico'}, {
                get: { method: "GET"},
            }),

             entradaSaldoMesAnterior : $resource('mediclab/movimentacaoconta/entrada/saldomesanterior', {}, {
                get: { method: "GET"},
            }),

            //BEGIN DESPESAS BY CC
            despesasPorContaContabil: $resource('mediclab/despesafinanceiro/despesascontacontabil', {}, {
                get:{ method: "GET", isArray: true}
            }),
            despesasPorCentroCusto: $resource('mediclab/despesafinanceiro/despesascentrocusto', {}, {
                get: {method: "GET", isArray: true}
            }),

            analiseHorizontal: $resource('mediclab/contacontabil/analisehorizontal', {}, {
                get: {method: "GET", isArray: true}
            }),

            //retiradas
            detalhesRetiradaInd: $resource('mediclab/caixa/detalhesretirada', {}, {
                get: {method:"GET", isArray: true}
            }),
            visualizarRetiradaGeral: $resource('mediclab/caixa/detalhesretiradageral', {}, {
                get: {method: "GET", isArray: true}
            }),

            //ler aquivo ofx
            lerArquivo: $resource('mediclab/conciliacaofinanceiro/ofxarquivo', {}, {
                get: {method: "GET"}
            }),

            //BANCOS
            banco: $resource('mediclab/bancos', {}, {
                get:{ method: "GET", isArray: true}
            }),

            agencias: $resource('mediclab/agencias', {}, {
                get:{method: "GET", isArray: true}
            }),

            agenciasByBanco: $resource('mediclab/agencias/banco/:idBanco', {idBanco: '@idBanco'}, {
                get:{method:"GET", isArray: true}
            }),

            //busca nmDespesa
            buscaDespesaFinanceiro: $resource('mediclab/despesafinanceiro/busca', {}, {
                get: {method:"GET", isArray: true}
            }),

            //updade Despesa
            updateDespesaFinanceiro: $resource('mediclab/despesafinanceiro/update',{},{
                edit: {method:"PUT"}
            }),

            //importar arquivo conciliacao
            importExtrato: $resource('mediclab/conciliacaofinanceiro/ofxarquivo', {}, {
                save: {method:"POST", isArray:true}
            }),

            //pegar informações extrato
            getInfoExtratos: $resource('mediclab/conciliacaofinanceiro/buscainfoextrato', {},{
                get:{method:"GET", isArray: true}
            }),

            //conciliar
            doConciliacao: $resource('mediclab/conciliacaofinanceiro/conciliar', {}, {
                save:{method: "POST", isArray:true}
            }),

            desconciliar: $resource('mediclab/conciliacaofinanceiro/desconciliar', {}, {
                edit:{method: "PUT", isArray: true}
            }),

            getSugestao: $resource('mediclab/conciliacaofinanceiro/sugestao', {}, {
                get: {method: "GET"/*, isArray: true*/}
            })
        }
    }
]);