MedicsystemApp.factory('RecorrenciaService', function($resource) {
	// var HOST_MEDICPHP = "http://vendedor.labmedonline.com.br";
	var HOST_MEDICPHP = "http://ad555c46.ngrok.io/vendas/v1/api";
	var HOST_VINDI = "https://app.vindi.com.br/api/v1";

	var auth_key = "Basic QnhTY0QxdzYtMDgzSEYwT3JwZ2hMWkFBT1U0elZXTFM6";

	return $resource('/', null, {
		setLog: {
			method: 'POST',
			url: HOST_MEDICPHP + '/log'
		},

		getClientPerCard: {
			method: 'GET',
			url: HOST_MEDICPHP + '/cliente/card/:card/:empresa'
		},

		checkClient: {
			method: 'GET',
			url: HOST_VINDI + '/customers/1923382'
		},

		insertClient: {
			method: 'POST',
			url: HOST_VINDI + '/customers',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': function() {
					return auth_key;
				}
			},
			cache: false
		},

		deleteClient: {
			method: 'DELETE',
			url: HOST_VINDI + '/customers/:id',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': function() {
					return auth_key;
				}
			},
			cache: false
		},

		sendPayment: {
			method: 'POST',
			url: HOST_VINDI + '/payment_profiles',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': function() {
					return auth_key;
				}
			},
			cache: false
		},

		createProduct: {
			method: 'POST',
			url: HOST_VINDI + '/products',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': function() {
					return auth_key;
				}
			},
			cache: false
		},

		createSubscription: {
			method: 'POST',
			url: HOST_VINDI + '/subscriptions',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': function() {
					return auth_key;
				}
			},
			cache: false
		}
	});
});