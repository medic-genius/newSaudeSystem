var MedicsystemApp = angular.module("MedicsystemApp", ["ui.router",
    "ui.bootstrap", "oc.lazyLoad", "ngSanitize", "ngRoute","ngResource",
    "webcam","angularFileUpload","ngFileUpload","me-lazyload",
    "jkuri.gallery","pdf","checklist-model","angular-timeline",
    "angular-scroll-animate","ngBootbox","duScroll","summernote","ui.calendar",
    "isteven-multi-select","daterangepicker","gridshore.c3js.chart","Routes","toastr","isteven-multi-select2"
]);

var getUserInfo = function() {
    if (window.sessionStorage["userInfo"]) return JSON.parse(window.sessionStorage["userInfo"]);
    return {};
}

var setUserInfo = function(data) {
    window.sessionStorage["userInfo"] = JSON.stringify(data);
}

var authData = {};

var keycloakAuth = new Keycloak('keycloak.json');

var logout = function() {
    var userData = getUserInfo();
    var logoutUrl = userData.logoutUrl;
    var userData = {loggedIn: false,authz: null};
    authData = userData;
    setUserInfo(userData);
    window.location = logoutUrl;
};

angular.element(document).ready(function ($http) {
    var http = location.protocol;
    var slashes = http.concat("//");
    var hostname = slashes.concat(window.location.hostname);
    if (location.port) {
        hostname = hostname.concat(":" + location.port);
    } else {
        hostname = hostname.concat("/ms");
    }

    keycloakAuth.init({ onLoad: 'login-required' }).success(function () {
        var userInfo = {
                loggedIn: true,
                authz: keycloakAuth,
                logoutUrl: keycloakAuth.authServerUrl + "/realms/" + keycloakAuth.realm + "/tokens/logout?redirect_uri=" + hostname + "/logout.html",
                isAuthorized: function(allowed) {
                    var userRoles = keycloakAuth.resourceAccess[keycloakAuth.clientId].roles;
                    if (allowed) {
                        for ( role of allowed ) {
                            if (userRoles.indexOf(role) != -1) return true;
                        }
                        return false;
                    } else {return true;}
                }
        };
        authData = userInfo;
        MedicsystemApp.factory('Auth', function() {return userInfo;});
        setUserInfo(userInfo);
        angular.bootstrap(document, ["MedicsystemApp"]);

    }).error(function () {
        window.location = hostname + "/logout.html";
        console.log("Falhou ao tentar logar no servidor de autenticacao. Solicite ajuda da equipe de TI.");
    });
});

MedicsystemApp.factory('authInterceptor', function($q, Auth) {
    return {
        request: function (config) {
            var deferred = $q.defer();
            if (Auth.authz.token) {
                Auth.authz.updateToken(5).success(function() {
                    config.headers = config.headers || {};
                    config.headers.Authorization = 'Bearer ' + Auth.authz.token;
                    deferred.resolve(config);
                }).error(function() {
                        deferred.reject({status: 401, data:'Failed to refresh token'});
                    });
            }
            return deferred.promise;
        }
    };
});

MedicsystemApp.factory('errorInterceptor', function($q) {
    return function(promise) {
        return promise.then(function(response) {
            return response;
        }, function(response) {
            if (response.status == 401) {
                console.log('session timeout?');
                logout();
            } else if (response.status == 403) {
                console.log("Forbidden");
            } else if (response.status == 404) {
                console.log("Not found");
            } else if (response.status) {
                if (response.data && response.data.errorMessage) {
                    console.log(response.data.errorMessage);
                } else {
                    console.log("An unexpected server error has occurred");
                }
            }
            return $q.reject(response);
        });
    };
});

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MedicsystemApp.config([ '$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({cssFilesInsertBefore : 'ng_load_plugins_before'});
}]);


// register the interceptor as a service
MedicsystemApp.factory('myHttpInterceptor', ['$q', '$rootScope', '$injector','toastr',
    function ($q, $rootScope, $injector, toastr) {
        return {
            'requestError': function (rejection) {
                return $q.reject(rejection);
            },
            'responseError': function (rejection) {

                if (rejection.status == 401) {
                    console.log('session timeout?');
                    logout();
                } else if (rejection.status == 403) {
                    console.log("Forbidden");
                } else if (rejection.status == 404) {
                    console.log("Not found");
                } else if (rejection.status = 400 || rejection.status == 500) {
                    console.log("rejection",rejection);
                    if (rejection.data && rejection.data.errorType) {

                        var pathTarget = (rejection.config.url).includes("mediclab/folha");
                        var pathTargetFinanceiro = (rejection.config.url).includes("mediclab/despesafinanceiro") || (rejection.config.url).includes("mediclab/caixa") ||  (rejection.config.url).includes("mediclab/fornecedor");
                        var errorType = rejection.data.errorType.toLowerCase();

                        console.log('pathTargetFinanceiro',pathTargetFinanceiro);

                        if( !pathTarget  && !pathTargetFinanceiro ) {
                            $rootScope.alerts = [];
                            $rootScope.alerts.push({
                                type: errorType,  msg: rejection.data.causedBy, timeout: 60000
                            });
                            Metronic.scrollTop();
                        } else {
                            console.log("errorType",errorType);
                            console.log("errorType == 'warning'",errorType == 'warning');
                            if( errorType == 'danger')
                                toastr.error(rejection.data.causedBy, 'Erro');
                            else if( errorType == 'warning' )
                                toastr.warning(rejection.data.causedBy, 'Aviso');
                        }
                    }
                }
                return $q.reject(rejection);
            }
        }
    }
]);

MedicsystemApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
    $httpProvider.interceptors.push('authInterceptor');
});

MedicsystemApp.config(['datepickerConfig', function(datepickerConfig) {
    datepickerConfig.showWeeks = false;
    datepickerConfig.language = 'pt-BR';
}]);

MedicsystemApp.config(['datepickerPopupConfig', function(datepickerPopupConfig) {
    datepickerPopupConfig.datepickerPopup = 'dd/MM/yyyy';
    datepickerPopupConfig.currentText = 'Hoje';
    datepickerPopupConfig.clearText = 'Limpar';
    datepickerPopupConfig.closeText = 'Fechar';
}]);

MedicsystemApp.directive('datepickerLocalDate', ['$parse', function ($parse) {
    var directive = {
        restrict: 'A',
        require: ['ngModel'],
        link: link
    };
    return directive;

    function link(scope, element, attr, ctrls) {
        var ngModelController = ctrls[0];

        ngModelController.$parsers.push(function (viewValue) {
            viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
            return viewValue.toISOString().substring(0, 10);
        });

        ngModelController.$formatters.push(function (modelValue) {
            if (!modelValue) {
                return undefined;
            }

            var dt = new Date(modelValue);
            dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());

            return dt;
        });
    }
}]);

MedicsystemApp.config(function($provide) {
    $provide.decorator('datepickerDirective', function($delegate) {
        var directive = $delegate[0],
            link = directive.link;

        angular.extend(directive.scope, { 'monthChanged': '&' });

        directive.compile = function() {
            return function(scope, element, attrs, ctrl) {
                link.apply(this, arguments);

                scope.$watch(function() {
                    return ctrl[0].activeDate.getTime();
                }, function(newVal, oldVal) {
                    if (scope.datepickerMode == 'day') {
                        oldVal = moment(oldVal).format('MM-YYYY');
                        newVal = moment(newVal).format('MM-YYYY');

                        if (oldVal !== newVal) {
                            var arr = newVal.split('-');
                            //TODO review that!
                            if (scope.$parent.monthChanged) {
                                scope.$parent.monthChanged(arr[0], arr[1]);
                            }
                        }
                    }
                });
            };
        };
        return $delegate;
    });
});

// AngularJS v1.3.x workaround for old style controller declarition in HTML
MedicsystemApp.config([ '$controllerProvider', function($controllerProvider) {
    $controllerProvider.allowGlobals();
} ]);

MedicsystemApp.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

/* Setup global settings */
MedicsystemApp.factory('settings', [ '$rootScope', function($rootScope) {
    var settings = {
        layout : { pageSidebarClosed : true, pageAutoScrollOnLoad : 1000},
        layoutImgPath : Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath : Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;
    return settings;
} ]);

/* Setup App Main Controller */
MedicsystemApp.controller('AppController',
    function($scope, $rootScope , $window, Auth) {

        if (Auth.authz.token)
            $rootScope.userName = Auth.authz.idTokenParsed.given_name;

        $rootScope.checkEditRole = function(allowed) {
            var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
            for ( role of userRoles ) {
                if (allowed.indexOf(role) != -1)
                    return false;
            }
            return true;
        };

        $rootScope.previous = function() {
            $window.history.back();
        };

        $scope.myFunction = function(href){
            $http.get(href).success(function(data) {
            });
        }

        $scope.$on('$viewContentLoaded', function() {
            Metronic.initComponents();
        });

        $rootScope.addDefaultTimeoutAlert = function(objectName, actionName, type) {
            if (type == "success") {
                $rootScope.alerts.push({
                    type: "info",
                    msg: objectName + " " + actionName + " com sucesso",
                    timeout: 10000 });
            } else if (type == "error") {
                $rootScope.alerts.push({
                    type: "danger",
                    msg: "Erro ao " + actionName + " " + objectName + " no sistema. Verifique os dados e envie novamente.",
                    timeout: 10000 });
            }
        }
});

/*******************************************************************************
 * Layout Partials. By default the partials are loaded through AngularJS
 * ng-include directive. In case they loaded in server side(e.g: PHP include
 * function) then below partial initialization can be disabled and Layout.init()
 * should be called on page load complete as explained above.
 ******************************************************************************/

/* Setup Layout Part - Header */
MedicsystemApp.controller('HeaderController',function($scope, $rootScope, servicesFactory, $http, Auth,toastr) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader();
    });

    $scope.nmGuiche= [];
    $scope.ultimoGuicheUsuario = 0;
    $scope.autorizaGuiche = false;
    $scope.autorizaGuicheGerente = false;
    $scope.guichesAutorizados = ['atendimentocliente'];
    $scope.guichesAutorizadosGerente = ['atendimentocliente','administrador','gerenteunidade', 'gerenterecepcaomedicaodonto','guichesAutorizadosGerente'];
    $scope.keyUnidadeFuncionario= null;
    var loginFunc =  Auth.authz.idTokenParsed.preferred_username;

    servicesFactory.getFuncionarioByLogin.get( {login: loginFunc},
        function( result) {
            if(result.unidade)
            {
                keyUnidade(result.unidade.id);
                if(!checkRole($scope.guichesAutorizados)){
                    $scope.autorizaGuiche = true;
                }

                if(!checkRole($scope.guichesAutorizadosGerente)){
                    $scope.autorizaGuicheGerente = true;
                }
            }
        });



    $scope.updatePainel = function() {

        var login =  Auth.authz.idTokenParsed.preferred_username;
        var userRoles = Auth.authz.resourceAccess["medicsystem-api"].roles;

        servicesFactory.getFuncionarioByLogin.get( {login: login},
        function( result ) {
            if( result ) {

                var idUnidade = result.unidade.id;


                if(login == 'admin'){
                    idUnidade = 0;
                }



            var json = {id_unidade: idUnidade,
                    id_consultorio: 0,
                    command: 'refresh_page'};

            $http({
                url: 'http://atendimento.uebisite.com.br:3000/command',
                method: "POST",
                data: json, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
            responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {

            }).error(function (data, status, headers, config) {
                console.log("Erro ao atualizar o painel.");
        });

            }
        });

    };

    function keyUnidade(idUnidade){

        if(idUnidade == null || idUnidade == 1){
            $scope.keyUnidadeFuncionario = "0f2f6426";
        }else if(idUnidade == 2){
            $scope.keyUnidadeFuncionario = "cd67f930";
        }else{
             $scope.keyUnidadeFuncionario = "bc51t734";
        }
    };


    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} );
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() );
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() );
        }
    };

    function checkRole (allowed) {

        var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;

        for ( role of userRoles ) {
            if (allowed.indexOf(role) != -1) {
                return false;
            }
        }
        return true;
    };



    $scope.nextNormal = function() {

        if($scope.nmGuinche == null || $scope.nmGuinche.length == 0){

            messageToastr('erro','Informe seu guichê','CALMA');
        }else{

             var json = {"guiche": $scope.nmGuinche,
                        "prioridade": false,
                        "command": "next",
                        "unidade" : $scope.keyUnidadeFuncionario};

            $http({
                url: 'http://atendimento.uebisite.com.br:3000/command',
                method: "POST",
                data: json, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
            responseType: 'json'
            }).success(function (data, status, headers, config) {
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);

              if(data != null){
                        if(data.prioridade){
                            $scope.ultimoGuicheUsuario = data.senha+" P";
                        }else{
                             $scope.ultimoGuicheUsuario = data.senha+" N";
                        }

            }
            }).error(function (data, status, headers, config) {
                console.log("Erro ao atualizar o painel.");
        });

        }



    };

    $scope.nextPrioridade = function() {

       if($scope.nmGuinche == null || $scope.nmGuinche.length == 0){

            messageToastr('erro','Informe seu guichê','CALMA');
        }else{

        var json = {"guiche": $scope.nmGuinche,
                        "prioridade": true,
                        "command": "next",
                        "unidade" : $scope.keyUnidadeFuncionario};

            $http({
                url: 'http://atendimento.uebisite.com.br:3000/command',
                method: "POST",
                data: json, //this is your json data string
                headers: {
                    'Content-type': 'application/json'
                },
            responseType: 'json'
            }).success(function (data, status, headers, config) {
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);

              if(data != null){
                    if(data.prioridade){
                        $scope.ultimoGuicheUsuario = data.senha+" P";
                    }else{
                         $scope.ultimoGuicheUsuario = data.senha+" N";
                    }

                }

            }).error(function (data, status, headers, config) {
                console.log("Erro ao atualizar o painel.");
        });
    }

    };

        $scope.repeat = function() {

            if($scope.nmGuinche == null || $scope.nmGuinche.length == 0){

                    messageToastr('erro','Informe seu guichê','CALMA');
                }else{

                    var json = {"guiche": $scope.nmGuinche,
                                    "prioridade": false,
                                    "command": "recall",
                                    "unidade" : $scope.keyUnidadeFuncionario};

                        $http({
                            url: 'http://atendimento.uebisite.com.br:3000/command',
                            method: "POST",
                            data: json, //this is your json data string
                            headers: {
                                'Content-type': 'application/json'
                            },
                        responseType: 'json'
                        }).success(function (data, status, headers, config, teste) {


                            console.log("data ",data);
                            console.log("status ",status);
                            console.log("headers ",headers);
                            console.log("config ",config);
                            console.log("teste ",teste);

                         if(data != null){
                                if(data.prioridade){
                                    $scope.ultimoGuicheUsuario = data.senha+" P";
                                }else{
                                     $scope.ultimoGuicheUsuario = data.senha+" N";
                                }

                            }

                        }).error(function (data, status, headers, config) {
                            console.log("Erro ao atualizar o painel.");
                    });

                }
     };
});

MedicsystemApp.controller('SidebarController', [ '$scope','servicesFactory', function($scope, servicesFactory) {
    $scope.menuCanSeeHome = [''];
    $scope.menuCanSeeCadastro = ['administrador', "faturamento"];
    $scope.menuCanSeeSubCadastro = ['administrador','faturamento','administrativo'];
    $scope.menuCanSeeAdministracao = ['administrador','administrativo'];
    $scope.menuCanSeeAtendimentoCliente = ['administrador','atendimentocliente','telemarketing'];
    $scope.menuCanSeeFuncionarios = ['administrador','diretoria','gerenteunidade','corpoclinico','rh','financeiro', 'digitadora'];
    $scope.menuCanSeeFolha = ['administrador','diretoria'];
    $scope.menuCanSeeRelatorio = ['administrador','gerenteunidade','corpoclinico', 'gerenteadm','faturamento', 'gerentetelemarketing'];
    $scope.menuCanSeeRelFaturamentoMedico = ['administrador', 'corpoclinico', 'faturamento', 'gerenteunidade'];
    $scope.menuCanSeeRelComissaoFunc = ['administrador', 'gerenteunidade', 'gerenteadm'];
    $scope.menuCanSeeRelatorioAgendamento = ['administrador', 'gerenteunidade', 'administrativo','gerentetelemarketing'];
    $scope.menuCanSeeRelatorioRenovacao = ['administrador', 'gerenteadm', 'diretoria'];
    $scope.menuCanSeeDashboard = ['administrador','gerenteunidade'];
    $scope.menuCanSeeRelDesempenhoMedico = ['administrador','gerenteunidade', 'corpoclinico'];
    $scope.menuCanSeeRelConveniada = ['administrador','faturamento'];
    $scope.menuCanSeeAtendimentoMedico = ['administrador','medico'];
    $scope.menuCanSeeEnfermagem = ['administrador','enfermagem'];
    $scope.menuCanSeeTelemarketing = ['administrador','telemarketing','atendimentocliente'];
    $scope.menuCanSeeTriagem = ['administrador','triagem','enfermagem'];
    $scope.menuCanSeeCobranca = ['administrador','cobranca'];
    $scope.menuCanSeeFinanceiro = ['administrador','financeiro'];
    $scope.menuCanSeeCortesias = ['gerenteadm', 'gerenteunidade'];
    $scope.menuCanSeeConveniada = ['administrador','conveniada'];


    function definirIcone( empresa ) {
      if( empresa.idEmpresaFinanceiro == 1 )  empresa.class = "glyphicon glyphicon-time" ;
      else if ( empresa.idEmpresaFinanceiro == 2) empresa.class = "fa fa-users" ;
      else if ( empresa.idEmpresaFinanceiro == 4 || empresa.idEmpresaFinanceiro == 7) empresa.class = "glyphicon glyphicon-edit" ;
      else if ( empresa.idEmpresaFinanceiro == 3) empresa.class = "fa fa-user-md" ;
      else if ( empresa.idEmpresaFinanceiro == 8) empresa.class = "fa fa-child" ;
      else if ( empresa.idEmpresaFinanceiro == 9) empresa.class = "fa fa-users" ;
      else if ( empresa.idEmpresaFinanceiro == 6) empresa.class = "fa fa-hospital-o" ;
      else if ( empresa.idEmpresaFinanceiro == 11) empresa.class = "fa fa-angle-right" ;
      else if ( empresa.idEmpresaFinanceiro == 5) empresa.class = "fa fa-bicycle" ;
      else if ( empresa.idEmpresaFinanceiro == 10 || empresa.idEmpresaFinanceiro == 20 ) empresa.class = "fa fa-stethoscope" ;

      return empresa;
    };

    function definirEmpresasGrupo( empresas ) {
        $scope.grupos = [ {id:0, nome:'Grupo Medic Lab', empresas: []},
        {id:1, nome:'Grupo Dental Saude',empresas: []}, {id:2, nome: 'Grupo Performance',empresas: []},
        {id:3, nome: 'Grupo Dr. Consulta',empresas: []}, {id:4, nome: 'Grupo Droga MED',empresas: []}];

        angular.forEach( empresas, function( empresa ) {
            empresa = definirIcone( empresa);
            if( empresa.inTipoGrupo == 0) $scope.grupos[0].empresas.push(empresa );
            else if ( empresa.inTipoGrupo == 1 )  $scope.grupos[1].empresas.push(empresa );
            else if ( empresa.inTipoGrupo == 2 )  $scope.grupos[2].empresas.push(empresa );
            else if ( empresa.inTipoGrupo == 3 )  $scope.grupos[3].empresas.push(empresa );
            else if ( empresa.inTipoGrupo == 4 )  $scope.grupos[4].empresas.push(empresa );
        })
    }

    function loadEmpresaFinanceiroFolha() {
        servicesFactory.empresasFinanceiroFolha.query( function( result) {
            definirEmpresasGrupo( result );
        });
    };

    function inicioSideBarController() {
        loadEmpresaFinanceiroFolha();
    }

    inicioSideBarController();


    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
} ]);

/* Setup Layout Part - Sidebar */
MedicsystemApp.controller('PageHeadController', [ '$scope', '$rootScope',
     function($scope, $rootScope) {

    $rootScope.alerts = [];

    $scope.closeAlert = function(index) {
        $rootScope.alerts.splice(index, 1);
    };

    $scope.$on('$includeContentLoaded', function() {
        Demo.init();
    });

}]);

/* Setup Layout Part - Footer */
MedicsystemApp.controller('FooterController', [ '$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
} ]);

/* Setup Rounting For All Pages */
MedicsystemApp
    .config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {

    //BEGIN LOAD MODULES
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_inputMask = MODULOS.load_inputMask;
    var load_jsonExport = MODULOS.load_jsonExport;
    var load_fileSystem = MODULOS.load_fileSystem;
    var load_ngValidate = MODULOS.load_ngValidate;
    var load_toastr = MODULOS.load_toastr;

    //BEGIN ROUTES
    var notAuthorized = {
        name: 'notauthorized',
        url : "/not_authorized.html",
        templateUrl : "not_authorized.html"
    };

    var home = {
        name: 'home',
        url : "/home.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/home.html",
        data : {pageTitle : '', pageSubTitle : '' },
        authorizedRoles: ['usuario','atendimentocliente','diretoria','medico','telemarketing','administrativo'],
        controller : "HomeController",
            resolve : {
                deps : [
                    '$ocLazyLoad',  function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            load_uiSelect,
                            load_fileSystem,
                            {
                                name : 'MedicsystemApp',
                                insertBefore : '#ng_load_plugins_before',
                                files : [
                                    'templates/admin4_material_design/angularjs/js/services/services.js',
                                    'templates/admin4_material_design/angularjs/js/controllers/HomeController.js' ]
                            }]);
                    }]
            }
    };

    var pesquisaServico =  {
        name: 'pesquisa_servico',
        url : "/pesquisa-servico.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/atendimento/atendimento-pesquisaServico.html",
        data : {pageTitle : 'Pesquisa de serviço', pageSubTitle : ''},
        authorizedRoles: ['usuario','atendimentocliente','diretoria','medico','administrativo','administrador'],
        controller : "AtendimentoPesqServicoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,  load_fileSystem,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/AtendimentoPesqServicoController.js'
                        ]
                    }
                ]);
            }]
        }
    };

    // begin folha de pagamento
    var folhaDashboard = {
        name: 'folha_dashboard',
        url : "/folha_dashboard/empresa/:idEmpresaFinanceiro",
        templateUrl : "templates/admin4_material_design/angularjs/views/folha_dashboard.html",
        data : {pageTitle : 'Folha de Pagamento',pageSubTitle : 'Dashboard'},
        authorizedRoles: ['diretoria'],
        controller : "FolhaDashboardController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                {
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                        'assets/admin/pages/scripts/index3.js',
                        'templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/controllers/FolhaDashboardController.js'
                    ]
                });
            }]
        }
    };

    var folhaValidar = {
        name: 'folha_dashboard-validar',
        url : "/folha_dashboard-validar.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/folha/folha_dashboard-validar.html",
        data : {pageTitle : 'Folha de Pagamento',pageSubTitle : 'Validação'},
        authorizedRoles: ['diretoria'],
        params: {mesAnoReferencia: null,idEmpresaFinanceiro: null,tipoGrupoFinanceiro: null,nmFantasia:null},
        controller : "FolhaValidacaoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_inputMask,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/FolhaValidacaoController.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var folhaValidarVisualizacao = {
        name: 'folhaValidar-visualizacao',
        url : "/folha-visualizacao/:idEmpresaFinanceiro/:tipoGrupoFinanceiro/:mesAnoReferencia.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/folha/folha_dashboard-validar.html",
        data : {pageTitle : 'Folha de Pagamento',pageSubTitle : 'Validação'},
        authorizedRoles: ['diretoria'],
        controller:"FolhaValidacaoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load(
                    [
                        load_ngTable,
                        load_inputMask,
                        {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/FolhaValidacaoController.js' ]
                        }
                    ]);
                } ]
        }
    };

    var folhaImportacao = {
        name: 'folha_importacao',
        url : "/folha_importacao.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/folha/folha_importacao.html",
        data: {pageTitle : 'Folha de Pagamento',pageSubTitle : 'Importação'},
        authorizedRoles: ['diretoria'],
        params: {nmEmpresaNickName: null,idEmpresaFinanceiro: null,nmFantasia: null},
        controller : "FolhaImportacaoController",
        resolve : {
            deps : [
                '$ocLazyLoad',function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        load_ngTable,
                        {
                            name : 'MedicsystemApp',
                            insertBefore : '#ng_load_plugins_before',
                            files : [
                                'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                                'templates/admin4_material_design/angularjs/js/services/services.js',
                                'templates/admin4_material_design/angularjs/js/controllers/FolhaImportacaoController.js' ]
                        }
                    ]);
            }]
        }
    };

    var folhaDashboardPorGrupo = {
        name: 'folha_dashboardGrupo',
        url : "/folha_dashboard/grupo/:idGrupoFinanceiro",
        templateUrl : "templates/admin4_material_design/angularjs/views/folha/folha_investimentoGrupo.html",
        data : {pageTitle : 'Totais folha de pagamento',pageSubTitle : 'grupo financeiro'},
        authorizedRoles: ['diretoria'],
        controller : "FolhaDashboardGrupoFinanceiroController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                {
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                        'assets/admin/pages/scripts/index3.js',
                        'assets/global/plugins/d3js/d3.min.js',
                        'assets/global/plugins/c3js/c3.min.css',
                        'assets/global/plugins/c3js/c3.min.js',
                        'templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/controllers/FolhaDashboardGrupoFinanceiroController.js'
                    ]
                });
            }]
        }
    };

    // end folha de pagamento

    // begin funcionario
    var funcionariosLista = {
        name: 'funcionario-lista',
        url: "/funcionarios-ativos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/funcionario/funcionario-lista.html",
        data : {pageTitle : 'Funcionários', pageSubTitle : 'Nossos funcionários'},
        authorizedRoles: ['diretoria','corpoclinico','gerenteunidade','rh','financeiro'],
        controller : "FuncionarioController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {name : 'MedicsystemApp',insertBefore : '#ng_load_plugins_before',
                        files :
                        ['templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/controllers/FuncionarioController.js']
                    }
                ]);
            }]
        }
    };

    var funcionarioUploadArquivos = {
        name: 'funcionario_uploadArquivo',
        url : "/funcionario/:idFuncionario/:nmFuncionario/upload",
        templateUrl : "templates/admin4_material_design/angularjs/views/funcionario/funcionario-uploadArquivo.html",
        data : {pageTitle : 'Funcionários', pageSubTitle : 'Importação de arquivos'},
        authorizedRoles: ['diretoria','corpoclinico'],
        controller : "FuncionarioUploadArquivoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                        'assets/global/plugins/custom-fileinputs/css/component.css',
                        'templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/controllers/FuncionarioUploadArquivoController.js',
                        'assets/global/plugins/angularjs/plugins/ng-gallery/src/css/ngGallery.css',
                        'templates/admin4_material_design/angularjs/js/controllers/VisualizarPdfController.js'
                    ]
                });
            }]
        }
    };

    var funcionarioVisualizarPDF = {
        name:'funcionario-visualizarArquivo',
        url: "/visualizar-pdf.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/funcionario/funcionario-visualizarPdf.html",
        data : {pageTitle : 'Funcionário',pageSubTitle : 'Visualizar arquivos'},
        authorizedRoles: ['diretoria'],
        params: {idFuncionario: null,nmFuncionario: null,nmArquivo: null,linkArquivo: null},
        controller : "VisualizarPdfController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                            'templates/admin4_material_design/angularjs/js/controllers/VisualizarPdfController.js'
                             ]
                });
            }]
        }
    };

    var funcionarioHorarioProfissional =  {
        name: 'funcionario-horarioProfissional',
        url: '/horarioProfissional.html',
        templateUrl: "templates/admin4_material_design/angularjs/views/funcionario/funcionario-horarioProfissional.html",
        data : {pageTitle : 'Profissional',pageSubTitle : 'Horários'},
        authorizedRoles: ['administrador'],
        controller : "ProfissionalHorarioController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                load_uiSelect,
                {
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                        'templates/admin4_material_design/angularjs/js/controllers/ModuloFuncionarioControllers/ProfissionalHorarioController.js',
                        'templates/admin4_material_design/angularjs/js/services/services.js',
                    ]
                }]);
            }]
        }
    };

    var cadastroProfissional =  {
        name: 'profissional-cadastro',
        url: '/cadastroProfissional.html',
        templateUrl: "templates/admin4_material_design/angularjs/views/funcionario/profissional-cadastro.html",
        data : {pageTitle : 'Profissional',pageSubTitle : 'Cadastro'},
        authorizedRoles: ['administrador'],
        controller : "ProfissionalController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                load_uiSelect,
                {
                    name : 'MedicsystemApp',
                    insertBefore : '#ng_load_plugins_before',
                    files : [
                        'templates/admin4_material_design/angularjs/js/controllers/ModuloFuncionarioControllers/ProfissionalController.js',
                        'templates/admin4_material_design/angularjs/js/services/services.js',
                    ]
                }]);
            }]
        }
    };

    // end funcionario

    // begin atendimento cliente
    var atendimentoCliente = {
        name:'atendimentoCliente',
        url : "/atendimentoCliente.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/atendimento/atendimentoCliente.html",
        data : {pageTitle : 'Atendimento Cliente',pageSubTitle : ''},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {dadosPreAgendamento: null},
        controller : "AtendimentoClienteController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_ngValidate,
                    load_toastr,
                    load_inputMask,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'assets/global/plugins/angularjs/plugins/ng-gallery_arq/src/css/ngGallery.css',
                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/AtendimentoClienteController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteContaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteAgendamentoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteArquivoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteDependenteController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteContratoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteObservacaoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteCoberturaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteDespesaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModalLoginGerenteController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteSituacaoFinanceiraController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteGuiasController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteGuiaDetailsController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloAtendimentoCliente/ClienteContratoNovoTemplateController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloAtendimentoCliente/ClienteComprovanteNovoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ClienteLaudosController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ShowPreparatoriosController.js',

                        ]
                    }
                ]);
            }]
        }
    };

    var atendimentoClienteEditarCliente = {
        name: "atendimentoCliente.cliente-conta",
        url: "/cliente-conta.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-conta-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Dados do Cliente'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteNovoCliente = {
        name: "atendimentoCliente.cliente-conta-novo",
        url: "/cliente-nova-conta.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-conta-novo.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Dados do Cliente'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteAgendamentosCliente = {
        name: "atendimentoCliente.agendamento",
        url: "/cliente-agendamentos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Agendamentos'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteNovoAgendamentoCliente = {
        name: "atendimentoCliente.cliente-agendamento-novo",
        url: "/cliente-novo-agendamento.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-novo.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Agendamento'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteEditarAgendamentoCliente = {
        name: "atendimentoCliente.cliente-agendamento-editar",
        url: "/cliente-editar-agendamento.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Agendamento'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {id: {squash: false,}
        }
    };

    var atendimentoclienteRetornoAgendamentoCliente = {
        name: "atendimentoCliente.cliente-agendamento-retorno",
        url: "/cliente-retorno-agendamento.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Retorno de Agendamento'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {id: { squash: false,}}
    };

    var atendimentoclienteDependentesCliente = {
        name: "atendimentoCliente.cliente-dependentes",
        url: "/cliente-dependentes.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-dependentes.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Dependentes'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteEditarDependenteCliente = {
        name: "atendimentoCliente.cliente-dependentes-editar",
         url: "/cliente-novo-dependente.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-dependentes-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Dependente'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {id: {  squash: false,}}
    };

    var atendimentoclienteContratosCliente = {
        name: "atendimentoCliente.cliente-contratos",
        url: "/cliente-contratos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-contratos.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Contratos'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteEditarContratoCliente = {
        name: "atendimentoCliente.cliente-contratos-editar",
         url: "/cliente-contrato-editar.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-contratos-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Contrato'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {id: null}
    };

    var atendimentoclienteCoberturaPlanoCliente = {
        name: "atendimentoCliente.cliente-cobertura",
        url: "/cliente-coberturas.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-cobertura.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Cobertura de Plano'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteArquivosCliente = {
        name: "atendimentoCliente.cliente-arquivos",
        url: "/cliente-arquivos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-arquivos.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Banco de Arquivos'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };


    var atendimentoclienteObservacoesCliente = {
        name: "atendimentoCliente.cliente-observacoes",
        url: "/cliente-observacoes.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-observacoes.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Observacoes'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

     var atendimentoClienteSituacaoFinanceira = {
        name: "atendimentoCliente.situacaoFinanceira",
        url: "/situacaoFinanceira.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-situacaoFinanceira.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Situação Financeira'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

     var atendimentoClienteLaudos = {
        name: "atendimentoCliente.laudos",
        url: "/laudos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-laudos.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Laudos'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteNovaObservacaoCliente = {
        name: "atendimentoCliente.cliente-observacoes-novo",
        url: "/cliente-observacoes-novo.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-observacoes-novo.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Nova Observacao'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoclienteEditarObservacaoCliente = {
        name: "atendimentoCliente.cliente-observacoes-editar",
        url: "/cliente-observacoes-editar.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-observacoes-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Observacao'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {id: null}
    };

    var atendimentoclienteNovaDespesaAgendamentoCliente = {
        name: "atendimentoCliente.cliente-despesas-agendamento-nova",
        url: "/cliente-despesas-agendamento-nova.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-agendamento-nova.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Lançamentos de Despesa'},
        authorizedRoles: ['atendimentocliente','telemarketing'],
        params: {idAgendamento: {squash: false,}}
    }

    var atendimentoclienteNovoDespesaCliente = {
        name: "atendimentoCliente.cliente-despesas-nova",
        url: "/cliente-despesas-nova.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-nova.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Nova Despesa'},
        authorizedRoles: ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'],
        params: {idAgendamento: {squash: false,}}
    };

    var atendimentoclienteNovaDespesaLancamentoCliente = {
        name: "atendimentoCliente.cliente-despesas-nova-lancamento",
        url: "/cliente-despesas-nova-lancamento.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-nova-lancamento.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Lançamentos de Despesa'},
        authorizedRoles: ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'],
        params: {despesa: {squash: false,},}
    };

    var atendimentoclienteDespesasCliente = {
        name: "atendimentoCliente.cliente-despesas",
         url: "/cliente-despesas.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-despesas.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Despesas'},
        authorizedRoles: ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro']
    };

    var atendimentoclienteEditarDespesaCliente = {
        name: "atendimentoCliente.cliente-despesas-editar",
        url: "/cliente-despesas-editar.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-editar.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Despesa'},
        authorizedRoles: ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'],
        params: {id: {squash: false,}}
    };

    var atendimentoConverterAssociado = {
        name: "atendimentoCliente.converter-associado",
        url: "/converter-associado.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/converter-associado.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Converter para associado'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoClienteGuias = {
        name: "atendimentoCliente.guias",
        url: "/guias.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/cliente-guias.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Guias'},
        authorizedRoles: ['atendimentocliente','telemarketing']
    };

    var atendimentoClienteGuiaDetail = {
        name: "atendimentoCliente.guiaDetail",
        url: "/guia_detail.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cliente/guia_detail.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Guia - Detalhes'},
        authorizedRoles: atendimentoClienteGuias.authorizedRoles,
        params: {id: {squash: false}}
    };
    // end atendimento cliente

    //begin administracao
    var administracaoRenovacaoContratos = {
        name: 'admin-renovacao-contrato',
        url : "/contratos-renovados.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/administracao/renovacao/contratos-renovados.html",
        data : {pageTitle : 'Administracao',pageSubTitle : 'Contratos Renovados'},
        authorizedRoles: ['administrador', 'administrativo'],
        controller : "AdministracaoRenovacaoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                                'assets/admin/pages/scripts/index3.js',
                                'assets/admin/pages/css/profile.css',
                                'assets/admin/pages/scripts/profile.js',
                                'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                                'templates/admin4_material_design/angularjs/js/services/services.js',
                                'templates/admin4_material_design/angularjs/js/controllers/AdministracaoRenovacaoController.js',
                                ]
                    }
                ]);
            } ]
        }
    };

    var administracaoRenovacaoContratosBoleto = {
        name: 'admin-renovacao-contrato-boletos',
        url: "/contrato-renovado-boletos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/administracao/renovacao/contrato-renovado-boletos.html",
        data: {pageTitle: 'Administração', pageSubTitle: 'Renovação de Contratos - Impressão de Boletos'},
        authorizedRoles: ['administrador', 'administrativo'],
        params: {id: {squash: false,}}
    };

    var administracaoCobranca = {
        name: 'admin-cobranca',
        url : "/cobranca.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/administracao/cobranca/cobranca.html",
        data : {pageTitle : 'Administracao',pageSubTitle : 'Operacoes de Cobranca'},
        authorizedRoles: ['administrador', 'administrativo'],
        controller : "AdministracaoCobrancaController",
        resolve : {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',

                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/AdministracaoCobrancaController.js',
                        ]
                    }
                ]);
            }]
        }
    };

    //end administracao

    // begin agenda medico old

    var atendimentomedico = {
        name: 'atendimentoMedico',
        url : "/atendimentoMedico.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/atendimento/atendimentoMedico.html",
        data : {pageTitle : 'Atendimento Médico',pageSubTitle : ''},
        authorizedRoles: ['administrador', 'corpoclinico', 'digitadora', 'medico'],
        controller : "AtendimentoMedicoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',

                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/AtendimentoMedicoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/MedicoContaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/TemplatesLaudoMedicoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/TemplatesReceituarioController.js'
                         ]
                    }
                ]);
            }]
        }
    };

    var atendimentomedicoCadastroMedico = {
        name: "atendimentoMedico.medico-conta",
        url: "/medico-conta.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/medico-conta.html",
        data: {pageTitle: 'Médico', pageSubTitle: 'Dados do Médico'},
        authorizedRoles: ['administrador', 'corpoclinico', 'medico']
    };

    var atendimentoMedicoTemplatesLaudo = {
        name: 'atendimentoMedico.templates-laudo',
        url : "/templates/laudo_medico",
        templateUrl : "templates/admin4_material_design/angularjs/views/medico/templates-laudo-medico.html",
        data : {
            pageTitle : 'Templates de Laudo Médico',
            pageSubTitle : ''
        },
        authorizedRoles: ['administrador', 'corpoclinico', 'digitadora', 'medico'],
        controller : "TemplatesLaudoMedicoController"
    };

    var atendimentoMedicoTemplatesReceituario = {
        name: 'atendimentoMedico.templates-receituario',
        url: '/templates/receituario',
        templateUrl: 'templates/admin4_material_design/angularjs/views/medico/templates-receituario.html',
        data: {
            pageTitle: 'Templates de Receituário Médico'
        },
        authorizedRoles: ['administrador', 'corpoclinico', 'digitadora', 'medico'],
        controller: 'TemplatesReceituarioController'
    }

    var agendamentoMedico = {
        name: 'agendamentoMedico',
        url : "/agendamentoMedico.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/atendimento/agendamentoMedico.html",
        data : {pageTitle : 'Atendimento Médico',pageSubTitle : ''},
        authorizedRoles: ['atendimentocliente','enfermagem','telemarketing'],
        controller : "MedicoAgendamentoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',

                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/MedicoAgendamentoController.js',
                        ]
                    }
                ]);
            } ]
        }
    };
    // end agenda medico old

    //begin módulo médico

    var medicoVisualizarPaciente = {
        name:   'medico-visualizarPacientes',
        url : "/profissional/pacientes.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/medico/medico-visualizarPacientes.html",
        data : {pageTitle : 'Filtrar Pacientes',pageSubTitle : ''},
        authorizedRoles: ['medico'],
        params: {idEspecialidade: null},
        controller : "MedicoVerificarPacientesController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                        'assets/admin/pages/scripts/index3.js',
                        'assets/admin/pages/css/profile.css',
                        'assets/admin/pages/scripts/profile.js',

                        'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                        'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                        'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                        'assets/global/plugins/bootstrap-bootbox/bootbox.min.js',

                        'templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/MedicoVisualizarPacientesController.js',
                         ]
                    }
                ]);
            }]
        }
    };

    var medicoAtendimentoPaciente = {
        name: 'medico-atendimentoPaciente',
        url : "/atendimento-paciente/:idEspecialidade/:tipoCliente/:idPaciente/:nmPaciente/:idAgendamento",
        templateUrl : "templates/admin4_material_design/angularjs/views/medico/medico-atendimentoPaciente.html",
        data : {pageTitle : 'Atendimento ao paciente',pageSubTitle : ''},
        authorizedRoles: ['medico'],
        controller : "MedicoAtendimentoPacienteController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/MedicoAtendimentoPacienteController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/MedicoLancamentoExamesController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteHistoricoAtendimentoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteAnamneseGeralController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteAnamneseEspecificaController.js',
                            // controller receituario nova versao (teste)
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteReceituarioController.js',
                            // old version controller receituario
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteReceituarioOldController.js',
                            // controller temporario (apenas pra fase de teste)
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteReceituarioTmpController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteReceituarioOftalmologicoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteLaudoController.js',
                            /*'templates/admin4_material_design/angularjs/js/controllers/ModuloMedicoControllers/PacienteAtestadoController.js',
                            'assets/global/plugins/icheck/skins/all.css',
                            'assets/global/plugins/icheck/icheck.min.js',*/
                        ]
                    }
                ]);
            }]
        }
    };

    var medicoAtendimentoPacienteHistoricoConsulta = {
        name: "medico-atendimentoPaciente.historico-consulta",
        url: "/historico-consultas.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-historicoAtendimento.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Atendimentos'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteLancarExames = {
        name: "medico-atendimentoPaciente.lancar-exames",
        url: "/lancar-exames.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/medico-lancarExames.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Lançamento de exames'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteAnamneseGeral = {
        name: "medico-atendimentoPaciente.anamnese-geral",
        url: "/analise.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-anamnese-geral.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Visão Geral'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteAnamneseEspecifica = {
        name: "medico-atendimentoPaciente.anamnese-especifica",
        url: "/anamnese.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-anamnese-especifica.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Anamnese'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteReceituario = {
        name: "medico-atendimentoPaciente.receituario",
        url: "/receituario.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-receituario.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Receituario'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteLaudo = {
        name: "medico-atendimentoPaciente.laudo",
        url: "/laudo.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-laudo.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Laudo'},
        authorizedRoles: ['medico']
    };

    var medicoAtendimentoPacienteReceituarioOftalmologico = {
        name: "medico-atendimentoPaciente.receituario-oftalmologico",
        url: "/receituario-oftalmologico.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-receituario-oftalmologico.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Receituario Oftalmológico'},
        authorizedRoles: ['medico']
    }

    /*var medicoAtendimentoPacienteAtestado = {
        name: "medico-atendimentoPaciente.atestado",
        url: "/atestado.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/medico/paciente-atestado.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Atestado'},
        authorizedRoles: ['medico']
    };*/

    //end módulo médico

    //begin modulo telemarketing

    var especialidadeAgendaDisponivel = {
        name: 'especialidadeAgendaDisponivel',
        url : "/agenda-especialidade.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/telemarketing/agendaEspecialidade.html",
        data : {pageTitle : 'Agenda por especialidade',pageSubTitle : ''},
        authorizedRoles: ['telemarketing','atendimentocliente','administrador'],
        controller : "AgendaEspecialidadeController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/fullcalendar/fullcalendar.min.css',
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloTelemarketingControllers/AgendaEspecialidadeController.js',
                        ]
                    }
                ]);
            } ]
        }
    };
    //end modulo telemarketing

    //begin modulo triagem
    var triagemVisualizarPaciente = {
        name:   'triagem-visualizarPacientes',
        url : "/triagem/pacientes.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/triagem/triagem-visualizarPacientes.html",
        data : {pageTitle : 'Filtrar Pacientes',pageSubTitle : ''
        },
        authorizedRoles: ['triagem','enfermagem'],
        params: {idEspecialidade: null },
        controller : "TriagemVisualizarPacientesController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                        'assets/admin/pages/scripts/index3.js',
                        'assets/admin/pages/css/profile.css',
                        'assets/admin/pages/scripts/profile.js',

                        'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                        'assets/global/plugins/bootstrap-bootbox/bootbox.min.js',

                        'templates/admin4_material_design/angularjs/js/services/services.js',
                        'templates/admin4_material_design/angularjs/js/services/triagemServices.js',
                        'templates/admin4_material_design/angularjs/js/controllers/ModuloTriagemControllers/TriagemVisualizarPacientesController.js',
                         ]
                    }
                ]);
            }]
        }
    };

    var triagemAtendimentoPaciente = {
        name: 'triagem-atendimentoPaciente',
        url : "/triagem-paciente/:idEspecialidade/:tipoCliente/:idPaciente/:nmPaciente/:idAgendamento/:boPrioridade",
        templateUrl : "templates/admin4_material_design/angularjs/views/triagem/triagem-atendimentoPaciente.html",
        data : {pageTitle : 'Atendimento ao paciente',pageSubTitle : ''},
        authorizedRoles: ['triagem','enfermagem'],
        controller : "TriagemAtendimentoPacienteController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/services/triagemServices.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloTriagemControllers/TriagemAtendimentoPacienteController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloTriagemControllers/TriagemAtendimentoPacienteTriagem.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var triagemAtendimentoPacienteInformacao = {
        name: "triagem-atendimentoPaciente.triagem",
        url: "/preencher-triagem.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/triagem/paciente-triagem.html",
        data: {pageTitle: 'Paciente', pageSubTitle: 'Triagem'},
        authorizedRoles: ['triagem','enfermagem']
    };

    //end modulo triagem

    //begin relatorio

    var relatorioInvestimentoMedico = {
        name: 'relatorio_investimentoMedico',
        url : "/investimentoMedico.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/investimentoMedico.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Investimento Médico'
        },
        authorizedRoles: ['administrador','gerenteunidade', 'corpoclinico'],
        controller : "RelatorioMedicoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioMedicoController.js' ]
                    }
                ]);
            }]
        }
    };

    var relatorioInvestimentoMedicoDadosGerais = {
        name: 'relatorio_investimentoMedicoDadosGerais',
        url : "/investimentoMedicoGeral.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/investimentoMedicoDadosGerais.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Dados Gerais'
        },
        authorizedRoles: ['administrador', 'corpoclinico', 'faturamento', 'gerenteunidade'],
        controller : "RelatorioMedicoDadosGeraisController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioMedicoDadosGeraisController.js' ]
                    }
                ]);
            }]
        }
    };

    var relatorioComissaoFuncionarioDadosGerais = {
        name: 'relatorio_comissaoFuncionarioDadosGerais',
        url : "/comissaoFuncionario.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/comissaoFuncionario.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Dados Gerais'
        },
        controller : "RelatorioComissaoFuncionarioController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioComissaoFuncionarioController.js' ]
                    }
                ]);
            }]
        }
    };

    var relatorioComissaoFuncionariosUnidades = {
        name: 'relatorio_comissaoFuncionariosUnidades',
        url : "/comissaoFuncionariosUnidades.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/comissaoFuncionariosUnidades.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Todos os funcionários'
        },
        authorizedRoles: ['administrador', 'gerenteunidade', 'gerenteadm'],
        controller : "RelatorioComissaoFuncionariosUnidadesController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioComissaoFuncionariosUnidadesController.js' ]
                    }
                ]);
            }]
        }
    };

    var relatorioProdutividadeFuncionario = {
        name: 'relatorio_produtividadeFuncionario',
        url : "/produtividade/funcionario",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/produtividadeFuncionario.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Produtividade Funcionário'
        },
        authorizedRoles: ['administrador','gerenteunidade'],
        controller : "RelatorioProdFuncionarioController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioProdFuncionarioController.js' ]
                    }
                ]);
            }]
        }
    };

    var relatorioPrevisaoAgendamentos = {
        name: 'previsaoAgendamentos',
        url : "/previsaoAgendamentos.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/previsao-agendamentos.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Previsão de agendamentos'
        },
        authorizedRoles: ['administrador','gerenteunidade','corpoclinico'],
        controller : "PrevisaoAgendamentosController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioPrevisaoAgendamentosController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/DetalhesAvaliacoesModalController.js'

                        ]
                    }
                ]);
            }]
        }
    };

    var relatorioAdmConveniada = {
        name: 'relatorio_adm_conveniada',
        url : "/relatorio_adm_conveniada.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/adm_conveniada.html",
        data : {
            pageTitle : 'Relatório Conveniada',
        },
        authorizedRoles: ['administrador', 'faturamento'],
        controller : "GuiaServicosController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/GuiaServicosController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/OrdersByServicoComponent.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var relatorioAdmConveniadaFaturamento = {
        name: 'relatorio_adm_conveniada_faturamento',
        url : "/relatorio_adm_conveniada_faturamento.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/relatorio_adm_conveniada_faturamento.html",
        data : {
            pageTitle : 'Relatório: faturamento conveniada',
        },
        authorizedRoles: ['administrador', 'administrativo', 'faturamento', 'corpoclinico'],
        controller : "ConveniadaFaturamentoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturamentoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturamentoDetailController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFinancialHistoryController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturamentoDetailVidasController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/faturamento/ConveniadaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ReportDetailVidasComponent.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturarController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturarModalController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturarComponent.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var conveniadaFaturar = {
        name: 'conveniada/faturar',
        url : "/conveniada/faturar.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/faturamento/conveniada-faturar.html",
        data : {
            pageTitle : 'Faturar conveniada',
        },
        authorizedRoles: ['administrador', 'administrativo', 'faturamento', 'corpoclinico'],
        controller : "ConveniadaFaturarController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturarController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/ConveniadaFaturarComponent.js',
                        ]
                    }
                ]);
            }]
        }
    };
    var renovacaoAutomatica = {
        name: 'renovacaoAutomatica',
        url: "/renovacao.html",
        templateUrl:"templates/admin4_material_design/angularjs/views/relatorio/renovacao/renovacao.html",
        data:{
            pageTitle: 'Renovação',
            /*pageSubTitle: ''*/
        },
        authorizedRoles:['administrador', 'diretoria', 'gerenteadm'],
        controller: "RenovacaoController",
        resolve :{
            deps: ['$ocLazyLoad', function($ocLazyLoad){
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_toastr,{
                        name: 'MedicsystemApp',
                        insertBefore: '#ng_load_plugins_before',
                        files:[
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/Renovacao/RenovacaoController.js'
                        ]
                    }
                ]);
            }]
        }
    };

    var relatorioAgendamentoMedico = {
        name: 'relatorioAgendamentoMedico',
        url: "/relatorioAgendamentoMedico.html",
        templateUrl: 
        "templates/admin4_material_design/angularjs/views/relatorio/agendamento/relatorio-agendamento-medico.html",
        data:{
            pageTitle:'Relatório',
            pageSubTitle:'Agendamento Médico'
        },
        authorizedRoles:['administrador','diretoria', 'telemarketing', 'administrativo'],
        controller: "RelatorioAgendamentoMedicoController",
        resolve:{
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.js',


                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/Agendamento/RelatorioAgendamentoMedicoController.js'
                        ]
                    }           
                ]);
            }]
        }
    };

    var relatorioControlePortaria = {
        name: 'controlePortaria',
        url : "/controlePortaria.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/relatorio/controle-portaria.html",
        data : {
            pageTitle : 'Relatório',
            pageSubTitle : 'Controle de Portaria'
        },
        authorizedRoles: ['administrador', 'diretoria', 'corpoclinico'],
        controller : "RelatorioControlePortariaController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.js',


                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",

                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloRelatorioControllers/RelatorioControlePortariaController.js'

                        ]
                    }
                ]);
            }]
        }
    };

    var atendimentoPainel  = {
        name: 'atendimento-painel',
        url : "/atendimento/painel",
        templateUrl : "templates/admin4_material_design/angularjs/views/atendimento/atendimentoPainel.html",
        data : {
            pageTitle : 'Painel',
            pageSubTitle : 'Informações'
        },
        authorizedRoles: ['administrador','atendimentocliente'],
        controller : "AtendimentoPainelController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/AtendimentoPainelController.js' ]
                    }
                ]);
            }]
        }
    };
    //end relatorio

    //END ROUTES

    $urlRouterProvider.otherwise("/home.html");
    $stateProvider
        .state(notAuthorized)
        .state(home)
        .state(pesquisaServico)
        .state(folhaDashboard)
        .state(folhaValidar)
        .state(folhaValidarVisualizacao)
        .state(folhaImportacao)
        .state(folhaDashboardPorGrupo)
        .state(funcionariosLista)
        .state(funcionarioUploadArquivos)
        .state(funcionarioVisualizarPDF)
        .state(atendimentomedico)
        .state(atendimentomedicoCadastroMedico)
        .state(atendimentoMedicoTemplatesLaudo)
        .state(atendimentoMedicoTemplatesReceituario)
        .state(agendamentoMedico)
        .state(atendimentoCliente)
        .state(atendimentoClienteEditarCliente)
        .state(atendimentoclienteNovoCliente)
        .state(atendimentoclienteAgendamentosCliente)
        .state(atendimentoclienteNovoAgendamentoCliente)
        .state(atendimentoclienteEditarAgendamentoCliente)
        .state(atendimentoclienteRetornoAgendamentoCliente)
        .state(atendimentoclienteDependentesCliente)
        .state(atendimentoclienteEditarDependenteCliente)
        .state(atendimentoclienteContratosCliente)
        .state(atendimentoclienteEditarContratoCliente)
        .state(atendimentoclienteCoberturaPlanoCliente)
        .state(atendimentoclienteArquivosCliente)
        .state(atendimentoclienteObservacoesCliente)
        .state(atendimentoclienteNovaObservacaoCliente)
        .state(atendimentoclienteEditarObservacaoCliente)
        .state(atendimentoclienteNovaDespesaAgendamentoCliente)
        .state(atendimentoclienteNovoDespesaCliente)
        .state(atendimentoclienteNovaDespesaLancamentoCliente)
        .state(atendimentoclienteDespesasCliente)
        .state(atendimentoclienteEditarDespesaCliente)
        .state(atendimentoClienteSituacaoFinanceira)
        .state(atendimentoClienteGuias)
        .state(atendimentoClienteGuiaDetail)
        .state(atendimentoClienteLaudos)
        .state(atendimentoConverterAssociado)
        .state(administracaoRenovacaoContratos)
        .state(administracaoRenovacaoContratosBoleto)
        .state(administracaoCobranca)
        .state(medicoVisualizarPaciente)
        .state(medicoAtendimentoPaciente)
        .state(medicoAtendimentoPacienteHistoricoConsulta)
        .state(medicoAtendimentoPacienteLancarExames)
        .state(medicoAtendimentoPacienteAnamneseGeral)
        .state(medicoAtendimentoPacienteAnamneseEspecifica)
        .state(medicoAtendimentoPacienteReceituario)
        .state(medicoAtendimentoPacienteLaudo)
        //.state(medicoAtendimentoPacienteAtestado)
        .state(medicoAtendimentoPacienteReceituarioOftalmologico)
        .state(especialidadeAgendaDisponivel)
        .state(triagemVisualizarPaciente)
        .state(triagemAtendimentoPaciente)
        .state(triagemAtendimentoPacienteInformacao)
        .state(relatorioInvestimentoMedico)
        .state(relatorioInvestimentoMedicoDadosGerais)
        .state(relatorioComissaoFuncionarioDadosGerais)
        .state(relatorioProdutividadeFuncionario)
        .state(relatorioPrevisaoAgendamentos)
        .state(relatorioAdmConveniada)
        .state(relatorioAdmConveniadaFaturamento)
        .state(relatorioComissaoFuncionariosUnidades)
        .state(relatorioControlePortaria)
        .state(relatorioAgendamentoMedico)
        .state(atendimentoPainel)
        .state(conveniadaFaturar)
        .state(renovacaoAutomatica)
    }]);

    MedicsystemApp.config(function(toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 3,
            newestOnTop: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: true,
            target: 'body',
            closeButton:true,
            extendedTimeOut: 5000,
            tapToDismiss: true,
            timeOut: 15000
        });
    });

    /* Init global settings and run the app */
    MedicsystemApp.run(function($rootScope, settings, $state, $http, $location, Auth) {

        $rootScope.$state = $state;

        $rootScope.$on('$stateChangeStart', function (event, next) {

            var authorizedRoles = next.authorizedRoles;
            if (!Auth.isAuthorized(authorizedRoles)) {
                event.preventDefault();
                $state.go('notauthorized');
            }
          });
        $rootScope.util =
        {
            formatPhoneNumber: function(s)
            {
                s = s.toString();
                s = s.replace(/\D/g,'');
                if(8 == s.length)
                {
                    return s.replace(/^(\d{4})(\d{4}).*/,"$1-$2");
                } else if(9 == s.length)
                {
                    return s.replace(/^(\d{1})(\d{4})(\d{4}).*/,"$1-$2-$3");
                } else if(10 == s.length)
                {
                    return s.replace(/^(\d{2})(\d{4})(\d{4}).*/,"($1) $2-$3");
                } else if(11 == s.length)
                {
                    return s.replace(/^(\d{2})(\d{1})(\d{4})(\d{4}).*/,"($1) $2-$3-$4");
                } else if(12 == s.length)
                {
                    return s.replace(/^(\d{3})(\d{1})(\d{4})(\d{4}).*/,"($1) $2-$3-$4");
                } else
                {
                    return s;
                }
            }
        };
    });

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    };




    angular.module('Routes', ['CobrancaRoutes','FinanceiroRoutes','DashboardRoutes',
        'CaixaRoutes','AdministrativoRoutes','CadastroRoutes', 'CortesiaRoutes','ConveniadaRoutes']);
