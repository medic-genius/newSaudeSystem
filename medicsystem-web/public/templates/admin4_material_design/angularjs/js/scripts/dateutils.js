var DateUtils = function() {
    return {
        getNow : function() {
            return new Date();
        },
        getYear : function() {
            return new Date().getFullYear();
        },
        getMonth : function() {
            return new Date().getMonth() + 1;
        },
        getFirstDayISO : function() {
            return new Date(DateUtils.getYear(), new Date().getMonth(), 1).toISOString().slice(0, 10);
        },
        getLastDayISO : function() {
            return new Date(DateUtils.getYear(), DateUtils.getMonth(), 0).toISOString().slice(0, 10);
        },
        getNextMonthOfYear : function(year, month) {
            //-1 + 1 a symbolic way to show that month comes into a (1,12) range
            //And to show that Javascript Date works into (0,11) range
            var next = new Date(year, month - 1 + 1);
            var nextFirstDay = new Date(next.getFullYear(), next.getMonth(), 1).toISOString().slice(0, 10);
            var nextLastDay = new Date(next.getFullYear(), next.getMonth() + 1, 0).toISOString().slice(0, 10);
            return { 
                year: next.getFullYear(), 
                month: next.getMonth() + 1, 
                firstDay: nextFirstDay,
                lastDay: nextLastDay
            };
        },
        getPreviousMonthOfYear : function(year, month) {
            var next = new Date(year, month - 2);
            var nextFirstDay = new Date(next.getFullYear(), next.getMonth(), 1).toISOString().slice(0, 10);
            var nextLastDay = new Date(next.getFullYear(), next.getMonth() + 1, 0).toISOString().slice(0, 10);
            return { 
                year: next.getFullYear(), 
                month: next.getMonth() + 1,
                firstDay: nextFirstDay,
                lastDay: nextLastDay 
            };
        },
        prettyMonth : function() {
            return {
                '1' : "Janeiro",
                '2': "Fevereiro",
                '3': "Marco",
                '4' : "Abril",
                '5': "Maio",
                '6': "Junho",
                '7': "Julho",
                '8': "Agosto",
                '9': "Setembro",
                '10': "Outubro",
                '11': "Novembro",
                '12': "Dezembro"
              };
        },
    };
}();