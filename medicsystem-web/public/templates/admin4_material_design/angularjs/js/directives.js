/***
GLobal Directives
***/

MedicsystemApp.directive('editRole', function(Auth) {
    return function(scope, element, attrs) {

    	var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
    	var rwRoles = scope.rwRoles;

    	for ( role of userRoles ) {
    		if (rwRoles.indexOf(role) != -1) {
    			return;
    		}
    	}
    	element.attr('ng-disabled', true);
    	element.attr("disabled", "disabled");

    }
});

MedicsystemApp.directive('showrole', function(Auth) {
    return function(scope, element, attrs) {
        scope.$watch(attrs.showrole, function(value, oldValue) {
        	var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
        	var rwRoles = value;

        	for ( role of userRoles ) {
        		if (rwRoles.indexOf(role) != -1) {
        			return element.show();
        		}
        	}
        	element.hide();

        }, true);
    }
})

MedicsystemApp.directive('capitalize', function() {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
           if(inputValue == undefined) inputValue = '';
           var capitalized = inputValue.toUpperCase();
           if(capitalized !== inputValue) {
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
            }
            return capitalized;
         }
         modelCtrl.$parsers.push(capitalize);
         capitalize(scope[attrs.ngModel]);  // capitalize initial value
     }
   };
});

// Route State Load Spinner(used on page or content load)
MedicsystemApp.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    //setTimeout(function () {
                    //    Metronic.scrollTop(); // scroll to the top on content load
                    //}, $rootScope.settings.layout.pageAutoScrollOnLoad);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
])

// Handle global LINK click
MedicsystemApp.directive('a',
    function() {
        return {
            restrict: 'E',
            link: function(scope, elem, attrs) {
                if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                    elem.on('click', function(e) {
                        e.preventDefault(); // prevent link click for above criteria
                    });
                }
            }
        };
    });

// Handle Dropdown Hover Plugin Integration
MedicsystemApp.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };
});

MedicsystemApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});

MedicsystemApp.directive('datePickerRefreshView',function() {
  var noop = function(){};
  var refreshDpOnNotify = function (dpCtrl) {
    return function() {
      dpCtrl.refreshView();
    };
  };
  return {
    require: 'datepicker',
    link: function(scope,elem,attrs,dpCtrl) {
      var refreshPromise = scope[attrs.datePickerRefreshView];
      refreshPromise.then(noop,noop,refreshDpOnNotify(dpCtrl));
    }
  };
});


MedicsystemApp.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);

MedicsystemApp.directive('icheck', function($timeout, $parse) {
    return {
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: $attrs.classCheckIcheck,
                    radioClass:$attrs.classRadioIcheck

                }).on('ifChanged', function(event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        },
    };
});

MedicsystemApp.directive('focusElement', function($timeout, $parse) {
  return {
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusElement);
      scope.$watch(model, function(value) {
        console.log('value=',value);
        if(value === true) {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
      element.bind('blur', function() {
        console.log('blur')
        scope.$apply(model.assign(scope, false));
      })
    }
  };
});

MedicsystemApp.directive('starRating', function() {
    return {
        restrict: 'E',
        template:
            '<i ng-repeat="star in stars" ng-class="star.class"></i>'
            ,scope: {
                ratingValue: '=rateValue',
                max: '=?', // optional (default is 5)
            },
            link: function(scope, element, attributes) {
                if (scope.max == undefined) {
                    scope.max = 5;
                }
                function getStarClass(star) {
                    var cl;
                    if(scope.ratingValue >= scope.max * 0.75) {
                        cl = 'font-green-jungle ';
                    } else if(scope.ratingValue < scope.max *0.75 && scope.ratingValue >= scope.max *0.50) {
                        cl = 'font-green-soft ';
                    } else if(scope.ratingValue < scope.max *0.50 && scope.ratingValue >= scope.max *0.35) {
                        cl = 'font-red-soft ';
                    } else {
                        cl = 'font-red-thunderbird ';
                    }
                    if(star.filled) {
                        cl += 'fa fa-star';
                    } else if(star.half) {
                        cl += 'fa fa-star-half-o';
                    } else if(star.empty) {
                        cl += 'fa fa-star-o';
                    }
                    return cl;
                }

                function updateStars() {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; ++i) {
                        var star = {};
                        if(i < scope.ratingValue) {
                            var v = scope.ratingValue - i;
                            if(v < 1.0) {
                                star.half = true;
                            } else {
                                star.filled = true;
                            }
                        } else {
                            star.empty = true;
                        }
                        var classStr = getStarClass(star);
                        star.class = classStr;

                        scope.stars.push(star);
                    }
                };

                scope.$watch('ratingValue', function(oldValue, newValue) {
                    if (newValue || newValue === 0) {
                        updateStars();
                    }
                });
            }
    }
});

MedicsystemApp.directive('numbersOnly', function ()
{
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl)
        {
            function fromUser(text)
            {
                if (text)
                {
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    if (transformedInput !== text)
                    {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

// async
MedicsystemApp.directive('cepValidate', cepValidate);
function cepValidate($http, $q)
{
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel)
        {
            ngModel.$asyncValidators.cepIsNotValid = function(modelValue, viewValue)
            {
                var deferred = $q.defer();
                if(undefined == modelValue)
                {
                    modelValue = "";
                }
                modelValue = modelValue.toString();
                modelValue = modelValue.replace(/\D/g,'');
                if(modelValue.length != 8)
                {
                    deferred.reject();
                    return deferred.promise;
                }
                $http.get('/mediclab/bairros/find_address_by_cep', {params: {cep: modelValue}})
                .then(
                    function(response)
                    {
                        if (200 == response.status)
                        {
                            deferred.resolve();
                        } else
                        {
                            deferred.reject();
                        }
                    },
                    function(data)
                    {
                        deferred.reject();
                    }
                );
                return deferred.promise;
            }
        }
    };
}
