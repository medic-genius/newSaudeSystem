angular.module('FinanceiroRoutes', ['ui.router',"oc.lazyLoad","ui.bootstrap"])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    
    // LOAD MODULOS NECESSARIOS
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_inputMask = MODULOS.load_inputMask;
    var load_toastr = MODULOS.load_toastr;
    var load_uiTree = MODULOS.load_uiTree;


    //BEGIN ROUTES
    var centralCadastro = {
        name: 'centralCadastro',
        url : "/financeiro/cadastro",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/principal.html",
        data : { pageTitle : 'Cadastro', pageSubTitle : 'Início'},
        authorizedRoles: ['financeiro'],
        controller : "CentralCadastroController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_inputMask,
                    load_toastr,
                    load_uiTree,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //DatePicker
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloCentralCadastro/CentralCadastroController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloCentralCadastro/FornecedorController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloCentralCadastro/CentroCustoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloCentralCadastro/ContaContabilController.js',
                            //Icheck jquery plugin
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };  

    var fornecedorCadastro = {
        name: "centralCadastro.fornecedorCadastro",
        url: "/fornecedor",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/fornecedores.html",
        data: {pageTitle: 'Cadastro', pageSubTitle: 'Fornecedor'},
        authorizedRoles: ['financeiro']
    };

    var centroCustoCadastro = {
        name: "centralCadastro.centroCustoCadastro",
        url: "/centro-custo",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/centroCusto.html",
        data: {pageTitle: 'Cadastro', pageSubTitle: 'Centro de Custo'},
        authorizedRoles: ['financeiro']
    };

    var contaContabilCadastro = {
        name: "centralCadastro.contaContabilCadastro",
        url: "/conta-contabil",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/contaContabil.html",
        data: {pageTitle: 'Cadastro', pageSubTitle: 'Conta Contábil'},
        authorizedRoles: ['financeiro']
    };

    var analiseFinanceira = {
        name:'analiseFinanceira',
        url:"/financeiro/analiseFinanceira",
        templateUrl: "templates/admin4_material_design/angularjs/views/financeiro/analiseFinanceira/analiseFinanceira.html",
        data: {pageTitle: 'Análise Financeiro' , pageSubTitle: ''},
        authorizedRoles: ['financeiro'],
        controller: "AnaliseFinanceiraController",
        params: {
            filtroDashboard: null,
            efReferencia: null,
            objectSearch: null,
        },
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_toastr,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/bootstrap/css/bootstrap.min.css',
                            //Commons
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            //DatePicker
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js', ///??
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloAnaliseFinanceira/AnaliseFinanceiraController.js',
                            //Icheck jquery plugin
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var contaPagarPrincipal = {
        name: 'contaPagar-principal',
        url : "/financeiro/contaPagar/inicio",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/contaPagar/contaPagar-principal.html",
        data : { pageTitle : 'Contas a pagar', pageSubTitle : 'Início'},
        authorizedRoles: ['financeiro'],
        controller : "ContaPagarPrincipalController",
        params: {
            filtroDashboard: null,
            efReferencia: null,
            objectSearch: null,
        },
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_toastr,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Commons
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            //DatePicker
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloContaPagar/ContaPagarPrincipalController.js',
                            //Icheck jquery plugin
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var contaPagarNova = {
        name: 'contaPagar-cadastro',
        url : "/financeiro/contaPagar/:nmEmpresaFinanceiro/cadastro",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/contaPagar/contaPagar-cadastro.html",
        data : { pageTitle : 'Contas a pagar', pageSubTitle : 'Nova despesa'},
        authorizedRoles: ['financeiro'],
        controller : "ContaPagarCadastroController",
        params: {
            efCommand: null,
            despesaFinanceiroSelected: null,
            objectSearch: null,
        },
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_uiTree,
                    load_toastr,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Commons
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            //DatePicker
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloContaPagar/ContaPagarCadastroController.js',
                            //Icheck jquery plugin
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            //jquery plugin
                            "assets/global/plugins/jquery-qrcode/qrcode.js",
                            "assets/global/plugins/jquery-qrcode/qrcode.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var visaoGeral = {
        name: 'conta-visaoGeral',
        url : "/financeiro/visao-geral",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/comum/visao-geral.html",
        data : { pageTitle : 'Visão geral', pageSubTitle : ''},
        authorizedRoles: ['financeiro'],
        controller : "ContaVisaoGeralController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //fix path seg
                            'assets/global/plugins/angularjs/plugins/c3-angular/pathseg.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloComum/ContaVisaoGeralController.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var movimentacaoContasEmpresa = {
        name: 'movimentacao-contasEmpresa',
        url : "/financeiro/movimentacoes",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/movimentacao/movimentacao-contasEmpresa.html",
        data : { pageTitle : 'Movimentações', pageSubTitle : ''},
        authorizedRoles: ['financeiro'],
        controller : "MovimentacaoContasEmpresaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //FuseJs
                            'assets/global/plugins/fusejs/fuse.js',
                            //StringJs
                            'assets/global/plugins/stringjs/stringjs.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloMovimentacao/MovimentacaoContasEmpresaController.js',
                        ]
                    }
                ]);
            }]
        }   
    };

    var despesaFinanceiroAgrupada = {
        name: 'despesaFinanceiro-agrupada',
        url : "/financeiro/despesas/agrupadas",
        templateUrl : "templates/admin4_material_design/angularjs/views/financeiro/movimentacao/despesaFinanceiro-agrupada.html",
        data : { pageTitle : 'Despesas', pageSubTitle : 'por numero de servico' },
        authorizedRoles: ['financeiro'],
        controller : "DespesaFinanceiroAgrupadaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloMovimentacao/DespesaFinanceiroAgrupadaController.js',
                            
                            

                        ]
                    }
                ]);
            }]
        }   
    };

    var conferenciaCaixaEmpresa = {
        name: 'conferenciaCaixa-empresa',
        url : "/caixa/conferencia/empresas",
        templateUrl : "templates/admin4_material_design/angularjs/views/caixa/conferencia/empresas.html",
        data : { pageTitle : 'Conferencia', pageSubTitle : 'Empresas' },
        authorizedRoles: ['financeiro'],
        controller : "ConferenciaCaixaEmpresaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/caixaServices.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCaixaControllers/conferencia/ConferenciaCaixaEmpresaController.js',
                        ]
                    }
                ]);
            }]
        }   
    };

    var conciliacaoBancaria ={
        name: 'conciliacaoBancaria',
        url:"/financeiro/conciliacao-bancaria",
        templateUrl: "templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/conciliacao-bancaria.html",
        data : { pageTitle : 'Conciliação Bancária', pageSubTitle : ''},
        authorizedRoles: ['financeiro'],
        controller : "ConciliacaoBancariaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_inputMask,
                    load_ngTable,
                    load_uiTree,
                    load_toastr,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/custom-fileinputs/css/component.css',
                            'assets/global/plugins/bootstrap/css/bootstrap.min.css',
                            //Commons
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/admin/pages/scripts/index3.js',
                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            //DatePicker
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/financeiroServices.js', ///??
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloFinanceiroControllers/ModuloConciliacao/ConciliacaoBancariaController.js'
                        ]
                    }
                ]);
            }]
        }
    };
    
    $stateProvider
        .state(centralCadastro)
        .state(fornecedorCadastro)
        .state(centroCustoCadastro)
        .state(contaContabilCadastro)
        .state(contaPagarPrincipal)
        .state(analiseFinanceira)
        .state(contaPagarNova)
        .state(visaoGeral)
        .state(movimentacaoContasEmpresa)
        .state(despesaFinanceiroAgrupada)
        .state(conciliacaoBancaria);

}]);


