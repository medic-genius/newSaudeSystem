angular.module('CortesiaRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, 
	$urlRouterProvider ) {
	var load_ngTable = MODULOS.load_ngTable;
	var load_inputMask = MODULOS.load_inputMask;
	
	var listaCortesias = {
		name: 'lista-cortesias',
		url : "/cortesias",
		templateUrl : "templates/admin4_material_design/angularjs/views/cortesia/lista-cortesias.html",
		data : { pageTitle : '', pageSubTitle : '' },
		authorizedRoles: ['gerenteunidade', 'gerenteadm'],
		controller : "ListaCortesiasController",
		resolve : {
			deps : ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load([
					load_ngTable,
					load_inputMask,
          {
          	name : 'MedicsystemApp',
          	insertBefore : '#ng_load_plugins_before',
          	files : [
          	'templates/admin4_material_design/angularjs/js/controllers/ModuloCortesia/ListaCortesiasController.js',
          	'templates/admin4_material_design/angularjs/js/services/CortesiaService.js',
          	// icheck plugin
          	"assets/global/plugins/icheck/skins/all.css",
          	"assets/global/plugins/icheck/icheck.min.js",
						]
					}
				]);
			}]
		}
	};

	$stateProvider.state(listaCortesias);
}
])