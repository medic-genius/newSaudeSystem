angular.module('CadastroRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    
    // LOAD MODULOS NECESSARIOS
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_angularBusy = MODULOS.load_angularBusy;
    var load_toastr = MODULOS.load_toastr;
    var load_angularSlider = MODULOS.load_angularSlider;
    var load_inputMask = MODULOS.load_inputMask;
    var load_angularBusy = MODULOS.load_angularBusy;

    //BEGIN ROUTES
    var cadastroBanco = {
        name: 'cadastroBanco',
        url : "/cadastro/banco.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroBanco.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Bancos' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "CadastroBancoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroBancoController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };

     var cadastroCidade = {
        name: 'cadastroCidade',
        url : "/cadastro/cidade.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroCidade.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Cidades' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "CadastroCidadeController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroCidadeController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };
    
    var cadastroOrgao = {
        name: 'cadastroOrgao',
        url : "/cadastro/orgao.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroOrgao.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Órgãos' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "CadastroOrgaoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroOrgaoController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };
    
    var cadastroFuncionario = {
        name: 'cadastroFuncionario',
        url : "/cadastro/funcionario.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroFuncionario.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Funcionários' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "CadastroFuncionarioController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroFuncionarioController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };

    var cadastroServico = {
        name: 'cadastroServico',
        url : "/cadastro/servico.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroServico.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Serviços' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "CadastroServicoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroServicoController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };

    var cadastroConveniada = {
        name: 'cadastroConveniada',
        url : "/cadastro/conveniada.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cadastro/cadastroConveniada.html",
        data : { pageTitle : 'Cadastrar', pageSubTitle : 'Conveniadas' },
        authorizedRoles: ['administrador','gerenteadm','administrativo', 'faturamento', 'gerenteunidade'],
        controller : "CadastroConveniadaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/cadastroService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCadastroControllers/CadastroConveniadaController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };
    $stateProvider
        .state(cadastroBanco)
        .state(cadastroCidade)
        .state(cadastroOrgao)
        .state(cadastroFuncionario)
        .state(cadastroServico)
        .state(cadastroConveniada)
}]);