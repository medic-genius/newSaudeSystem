angular.module('CaixaRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    
    // LOAD MODULOS NECESSARIOS
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_angularBusy = MODULOS.load_angularBusy;
    var load_toastr = MODULOS.load_toastr;

    //BEGIN ROUTES
    var conferenciaEmpresa = {
        name: 'conferencia-empresa',
        url : "/caixa/conferencia/empresas",
        templateUrl : "templates/admin4_material_design/angularjs/views/caixa/conferencia/empresas.html",
        data : { pageTitle : 'Conferência', pageSubTitle : 'Empresas' },
        authorizedRoles: ['financeiro'],
        controller : "ConferenciaEmpresaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/caixaService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCaixaControllers/conferencia/ConferenciaEmpresaController.js',
                        ]
                    }
                ]);
            }]
        }   
    };

    var conferenciaOperadorCaixa = {
        name: 'conferencia-operadorCaixa',
        url : "/caixa/conferencia/:idEmpresa/:nmEmpresa/caixas",
        templateUrl : "templates/admin4_material_design/angularjs/views/caixa/conferencia/operadorCaixa.html",
        data : { pageTitle : 'Conferencia', pageSubTitle : 'Caixas' },
        authorizedRoles: ['financeiro'],
        controller : "ConferenciaOperadorCaixaController",
        params: {objectSearch: null},
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/caixaService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCaixaControllers/conferencia/ConferenciaOperadorCaixaController.js',
                        ]
                    }
                ]);
            }]
        }   
    };

    var conferenciaMovimentacaoCaixa = {
        name: 'conferencia-movimentacao',
        url : "/caixa/:idCaixa/conferencia/:idEmpresa/:nmEmpresa/caixa/movimentacao",
        templateUrl : "templates/admin4_material_design/angularjs/views/caixa/conferencia/movimentacaoCaixa.html",
        data : { pageTitle : 'Conferencia', pageSubTitle : 'Movimentacao Caixa' },
        authorizedRoles: ['financeiro'],
        controller : "MovimentacaoCaixaController",
        params: {
            caixaSelected : null,
            objectSearch : null
        },
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/caixaService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCaixaControllers/conferencia/MovimentacaoCaixaController.js',
                        ]
                    }
                ]);
            }]
        }   
    };

    $stateProvider
        .state(conferenciaEmpresa)
        .state(conferenciaOperadorCaixa)
        .state(conferenciaMovimentacaoCaixa);
}]);


