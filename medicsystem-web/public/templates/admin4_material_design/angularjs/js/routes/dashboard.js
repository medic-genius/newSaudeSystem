angular.module('DashboardRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
      
    var load_uiSelect = {
        name: 'ui.select',
        insertBefore: '#ng_load_plugins_before',
        files: [
            'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
            'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
            'assets/global/plugins/select2/select2.css',
            'assets/global/plugins/select2/select2-bootstrap.css'
        ]
    };

    var load_ngTable = {
        name: 'ngTable',
        insertBefore: '#ng_load_plugins_before',
        files: [
            'assets/global/plugins/angularjs/plugins/ng-table/ng-table-new/ng-table.css',
            'assets/global/plugins/angularjs/plugins/ng-table/ng-table-new/ng-table.js',
        ]
    };

    var load_dirPagination = {
        name: 'angularUtils.directives.dirPagination',
        insertBefore: '#ng_load_plugins_before',
        files: [
            "assets/global/plugins/angularjs/plugins/dir-pagination/dirPagination.js"
        ]
    };

    var dashboardBegin = {
        name: 'dashboard',
        url : "/:idEmpresaGrupo/dashboard.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/dashboard.html",
        data : {
            pageTitle : 'Dashboard',
            pageSubTitle : ''
        },
        authorizedRoles: ['administrador','gerenteadm'],
        controller : "DashboardController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before', 
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            
                            'assets/global/plugins/d3js/d3.min.js',
                            'assets/global/plugins/d3js/d3pie.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/d3pieutils.js',
                            'templates/admin4_material_design/angularjs/js/scripts/dateutils.js',

                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            
                            'templates/admin4_material_design/angularjs/js/services/mediclab_report_service.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/DashboardController.js' 
                        ]
                    }
                ]);
            }]
        }     
    };
    
    var dashboardClientePlano = {
        name: 'db_clientePlano',
        url : "/db_clientePlano/:idPlano/:nmPlano/:vlPlano/:idEmpresaGrupo",
        templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/db_clientePlano.html",
        data : {
            pageTitle : 'Contratos de clientes do plano',
            pageSubTitle : ''
        },
        authorizedRoles: ['administrador','gerenteadm'],
        controller : "DashboardClientePlanoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            
                            'assets/global/plugins/d3js/d3.min.js',
                            'assets/global/plugins/d3js/d3pie.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/d3pieutils.js',
                            'templates/admin4_material_design/angularjs/js/scripts/dateutils.js',

                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            
                            'templates/admin4_material_design/angularjs/js/services/mediclab_report_service.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/DashboardController.js', 
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/DashboardClientePlanoController.js' 
                        ]
                    }
                ]);
            }]
        }
    };
    
     var dashboardResumoFaturamento = {
        name: 'db_resumoFaturamento',
        url : "/resumo-faturamento/dashboard",
        templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/db_resumoFaturamento.html",
        data : {
            pageTitle : 'Resumo Vendas (Faturamento)',
            pageSubTitle : ''
        },
        authorizedRoles: ['administrador','gerenteadm'],
        controller : "DashboardResumoFaturamentoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            
                            'assets/global/plugins/d3js/d3.min.js',
                            'assets/global/plugins/d3js/d3pie.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/d3pieutils.js',
                            'templates/admin4_material_design/angularjs/js/scripts/dateutils.js',

                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            
                            'templates/admin4_material_design/angularjs/js/services/mediclab_report_service.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/DashboardResumoFaturamentoController.js', 
                        ]
                    }
                ]);
            }]
        }
    };

    var dashboardControle = {
        name: 'controle-unidades',
        url : "/controle-unidades",
        templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/controle-unidades/controle-unidades.html",
        data : {
            pageTitle : 'Controle de unidades',
            pageSubTitle : ''
        },
        authorizedRoles: ['administrador','corpoclinico', 'gerenteunidade'],
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before', 
                        files : [
                            'assets/global/plugins/jquery.pulsate.min.js',
                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/ControleUnidades/DashboardControleProfissionaisController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloDashboardControllers/ControleUnidades/DashboardControleAgendamentosController.js'
                        ]
                    }
                ]);
            }]
        }
    };

    var dashboardControleViews = {
        name: 'controle-unidades.views',
        url : "/dashboard",
        parent: 'controle-unidades',
        views: {
            'profissionais': {
                templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/controle-unidades/controle-unidades-profissionais.html",
                controller : "DashboardControleProfissionaisController",
            },

            'agendamentos': {
                templateUrl : "templates/admin4_material_design/angularjs/views/dashboard/controle-unidades/controle-unidades-agendamentos.html",
                controller : "DashboardControleAgendamentosController",
            }
        }
    }

    $stateProvider
        .state(dashboardBegin)
        .state(dashboardClientePlano)
        .state(dashboardResumoFaturamento)
        .state(dashboardControle)
        .state(dashboardControleViews)
}]);


