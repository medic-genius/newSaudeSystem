angular.module('CobrancaRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
     
        // LOAD MODULOS NECESSARIOS
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_inputMask = MODULOS.load_inputMask;
    var load_angularBusy = MODULOS.load_angularBusy;

    var load_dirPagination = {
        name: 'angularUtils.directives.dirPagination',
        insertBefore: '#ng_load_plugins_before',
        files: [
            "assets/global/plugins/angularjs/plugins/dir-pagination/dirPagination.js"
        ]
    };

    var load_angularTimer = {
        name: 'timer',
        insertBefore: '#ng_load_plugins_before',
        files: [
            "assets/global/plugins/angularjs/plugins/angular-timer/angular-timer.min.js",
            /*  "assets/global/plugins/moment/moment-with-locales.min.js",*/
            "assets/global/plugins/angularjs/plugins/angular-timer/humanize-duration.js"
        ]
    };

    var cobrancaCliente = {
        name: 'cobrancaCliente',
        url : "/cobrancaCliente.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/cobranca/cobranca_clientes.html",
        data : {
            pageTitle : 'Cobranca',
            pageSubTitle : 'Filtros de busca'
        },
        authorizedRoles: ['cobranca'],
        controller : "CobrancaController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before', 
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/cobrancaServices.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js'
                        ]
                    }
                ]);
            }]
        }
    };

    var cobrancaClienteAtendimento = {
        name: 'cobranca-clienteAtendimento',
        url : "/cobranca-clienteAtendimento/:idCliente/:nmCliente",
        templateUrl : "templates/admin4_material_design/angularjs/views/cobranca/cobranca-clienteAtendimento.html",
        data : {
            pageTitle : 'Atendimento ao paciente',
            pageSubTitle : ''
        },
        authorizedRoles: ['cobranca'],
        controller : "CobrancaClienteAtendimentoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_angularTimer,
                    load_inputMask,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',

                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            'templates/admin4_material_design/angularjs/js/services/cobrancaServices.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaClienteAtendimentoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaHistoricoAtendimentoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaSituacaoFinanceiraController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaProcessoCobrancaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/CobrancaDocumentosController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var cobrancaClienteHistoricoAtendimento = {
        name: "cobranca-clienteAtendimento.historicoAtendimento",
        url: "/historicoCobranca.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cobranca/cobranca-historicoAtendimento.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Atendimentos'},
        authorizedRoles: ['cobranca']
    };

    var cobrancaClienteSituacaoFinanceira = {
        name: "cobranca-clienteAtendimento.situacaoFinanceira",
        url: "/situacaoFinanceira.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cobranca/cobranca-situacaoFinanceira.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Situação Financeira'},
        authorizedRoles: ['cobranca']
    };

    var cobrancaClienteProcessoCobranca = {
        name: "cobranca-clienteAtendimento.processoCobranca",
        url: "/processoCobranca.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cobranca/cobranca-processoCobranca.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'Cobranca'},
        authorizedRoles: ['cobranca']
    };

    var cobrancaClienteDocumentos = {
        name: "cobranca-clienteAtendimento.documentos",
        url: "/documentos.html",
        templateUrl: "templates/admin4_material_design/angularjs/views/cobranca/cobranca-documentos.html",
        data: {pageTitle: 'Cliente', pageSubTitle: 'documentos'},
        authorizedRoles: ['cobranca']
    };

    var cobrancaGerarBoletoEmpresa = {
        name: 'cobranca-gerarBoletoEmpresa',
        url : "/cobranca/gerar-boleto",
        templateUrl : "templates/admin4_material_design/angularjs/views/cobranca/gerarBoleto/cobranca-empresaFinanceiro.html",
        data : {
            pageTitle : 'Cobrança',
            pageSubTitle : 'Empresas'
        },
        authorizedRoles: ['cobranca'],
        controller : "CobrancaGerarBoletoEmpresaController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/cobrancaServices.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/gerarBoleto/CobrancaGerarBoletoEmpresaController.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var cobrancaGerarBoletoInadimplentes = {
        name: 'cobranca-gerarBoletoInadimplemntes',
        url : "/cobranca/:idEmpresaFinanceiro/inadimplentes",
        templateUrl : "templates/admin4_material_design/angularjs/views/cobranca/gerarBoleto/cobranca-inadimplentes.html",
        data : { pageTitle : 'Cobrança', pageSubTitle : 'Inadimplentes'
        },
        authorizedRoles: ['cobranca'],
        controller : "CobrancaGerarBoletoInadimplenteController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    load_angularBusy,
                     {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/services/cobrancaServices.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloCobrancaControllers/gerarBoleto/CobrancaGerarBoletoInadimplenteController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };


    $stateProvider
        .state(cobrancaCliente)
        .state(cobrancaClienteAtendimento)
        .state(cobrancaClienteHistoricoAtendimento)
        .state(cobrancaClienteSituacaoFinanceira)
        .state(cobrancaClienteProcessoCobranca) 
        .state(cobrancaClienteDocumentos)
        .state(cobrancaGerarBoletoEmpresa)
        .state(cobrancaGerarBoletoInadimplentes);  

}]);


