angular.module('AdministrativoRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    
    // LOAD MODULOS NECESSARIOS
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_angularBusy = MODULOS.load_angularBusy;
    var load_toastr = MODULOS.load_toastr;
    var load_angularSlider = MODULOS.load_angularSlider;
    var load_inputMask = MODULOS.load_inputMask;
    var load_angularBusy = MODULOS.load_angularBusy;

    //BEGIN ROUTES
    var gerarBoleto = {
        name: 'gerarBoleto',
        url : "/administrativo/gerar-boleto",
        templateUrl : "templates/admin4_material_design/angularjs/views/administrativo/gerarBoleto.html",
        data : { pageTitle : 'Geração', pageSubTitle : 'Boletos' },
        authorizedRoles: ['administrador','administrativo'],
        controller : "GerarBoletoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/administrativoService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloAdministrativoControllers/GerarBoletoController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };

     var ramal = {
        name: 'ramal',
        url : "/administrativo/telefones",
        templateUrl : "templates/admin4_material_design/angularjs/views/administrativo/ramal.html",
        data : { pageTitle : 'Telefones', pageSubTitle : '' },
        authorizedRoles: ['administrador','atendimentocliente'],
        controller : "RamalController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/administrativoService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloAdministrativoControllers/RamalController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                            //JQUERY TOOBAR
                            "assets/global/plugins/toolbarJs/jquery.toolbar.min.js", 
                            "assets/global/plugins/toolbarJs/jquery.toolbar.css",
                        ]
                    }
                ]);
            }]
        }   
    };

   /*  var pagamentoByIntegracao = {
        name: 'pagamentoByIntegracao',
        url : "/administrativo/pagamentos",
        templateUrl : "templates/admin4_material_design/angularjs/views/administrativo/pagamentoIntegracao.html",
        data : { pageTitle : 'Pagamentos', pageSubTitle : '' },
        authorizedRoles: ['administrador'],
        controller : "PagamentoIntegracaoController",
        resolve : {
            deps : ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngTable,
                    load_uiSelect,
                    load_angularSlider,
                    load_inputMask,
                    load_angularBusy,
                    load_toastr,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            //Services
                            'templates/admin4_material_design/angularjs/js/services/administrativoService.js',
                            //Controllers
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloAdministrativoControllers/PagamentoIntegracaoController.js',
                            //JQUERY ICHECK
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js", 
                        ]
                    }
                ]);
            }]
        }   
    };*/

    $stateProvider
        .state(gerarBoleto)
        .state(ramal);
  //      .state(pagamentoByIntegracao);
        
}]);


