angular.module('ConveniadaRoutes', ['ui.router',"oc.lazyLoad",])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    var load_uiSelect = MODULOS.load_uiSelect;
    var load_ngTable = MODULOS.load_ngTable;
    var load_inputMask = MODULOS.load_inputMask;
    var load_angularBusy = MODULOS.load_angularBusy;
    var load_ngValidate = MODULOS.load_ngValidate;

    // LOAD MODULOS NECESSARIOS
    var load_toastr = MODULOS.load_toastr;
    var load_angularSlider = MODULOS.load_angularSlider;

    var load_dirPagination = {
        name: 'angularUtils.directives.dirPagination',
        insertBefore: '#ng_load_plugins_before',
        files: [
            "assets/global/plugins/angularjs/plugins/dir-pagination/dirPagination.js"
        ]
    };

    var load_angularTimer = {
        name: 'timer',
        insertBefore: '#ng_load_plugins_before',
        files: [
            "assets/global/plugins/angularjs/plugins/angular-timer/angular-timer.min.js",
            "assets/global/plugins/angularjs/plugins/angular-timer/humanize-duration.js"
        ]
    };

    var conveniada = {
        name: 'conveniada',
        url : "/conveniada.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/conveniada.html",
        data : {
            pageTitle : 'Conveniada',
            pageSubTitle : 'Conveniada suttitle'
        },
        authorizedRoles: ['conveniada'],
        controller : "ConveniadaController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/ConveniadaController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var pageIndex = {
        name: 'pageIndex',
        url : "/conveniada_two.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/conveniada_two.html",
        data : {
            pageTitle : 'Conveniada two',
            pageSubTitle : 'Conveniada  two suttitle'
        },
        authorizedRoles: ['conveniada'],
        controller : "ConveniadaIndexController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/ConveniadaIndexController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js"
                        ]
                    }
                ]);
            }]
        }
    };

    var associados = {
        name: 'associados',
        url : "/conveniada/associados.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/associados.html",
        data : {
            pageTitle : 'Associados',
            pageSubTitle : 'Gerenciamento de associados'
        },
        authorizedRoles: ['conveniada'],
        controller : "CrudConveniadaController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'assets/admin/pages/scripts/index3.js',
                            'assets/admin/pages/css/profile.css',
                            'assets/admin/pages/scripts/profile.js',
                            'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            'assets/global/plugins/angularjs/plugins/ng-gallery_arq/src/css/ngGallery.css',
                            'assets/global/plugins/c3js/c3.min.css',
                            'assets/global/plugins/c3js/c3.min.js',
                            'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',
                            //ICHECK JQUERY
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/services/services.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/CrudConveniadaController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/ConveniadaCreateController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/EmitirGuiaController.js',
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/DependetesConveniadaComponent.js'
                        ]
                    }
                ]);
            }]
        }
    };


    var detailAssociado = {
        name: 'associado',
        url : "/conveniada/associado/:id/detail.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/associado.html",
        data : {
            pageTitle : 'Associado',
            pageSubTitle : 'Associado detalhe'
        },
        authorizedRoles: ['conveniada'],
        controller : "AssociadoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/AssociadoController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/DependetesConveniadaComponent.js',
                            "/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                            // "/assets/global/css/test.css",
                            //
                            // 'assets/admin/pages/css/profile.css',
                            // 'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            // 'assets/global/plugins/angularjs/plugins/ng-gallery_arq/src/css/ngGallery.css',
                            // 'assets/global/plugins/c3js/c3.min.css',
                            // "assets/global/plugins/icheck/skins/all.css",
                            // "assets/admin/layout/css/custom.css",
                        ]
                    }
                ]);
            }]
        }
    };

    var guias = {
        name: 'guias',
        url : "/conveniada/guias.html",
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/guias.html",
        data : {
            pageTitle : 'Guias',
            //pageSubTitle : 'guias'
        },
        authorizedRoles: ['conveniada'],
        controller : "GuiasController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/GuiasController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                        ]
                    }
                ]);
            }]
        }
    };

    var guia = {
        name: 'guia',
        url : '/conveniada/{guiaUrl:guia_[0-9]*}.html',
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/guia.html",
        data : {
            pageTitle : 'Guia',
            //pageSubTitle : 'guias'
        },
        authorizedRoles: ['conveniada'],
        controller : "GuiaController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/GuiaController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                        ]
                    }
                ]);
            }]
        }
    };

    var guiaServicos = {
        name: 'guiaServicos',
        url : '/conveniada/guia_servicos.html',
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/guia_servicos.html",
        data : {
            pageTitle : 'Serviços de Guias',
        },
        authorizedRoles: ['conveniada'],
        controller : "GuiaServicosController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/GuiaServicosController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/OrdersGuiaServicoController.js',
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/OrdersByServicoComponent.js',
                        ]
                    }
                ]);
            }]
        }
    };

    var coberturaPlano = {
        name: 'coberturaPlano',
        url : '/conveniada/cobertura_plano.html',
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/cobertura_plano.html",
        data : {},
        authorizedRoles: ['conveniada'],
        controller : "CoberturaPlanoController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/CoberturaPlanoController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                        ]
                    }
                ]);
            }]
        }
    };


    var testDependente = {
        name: 'test_dependente',
        url : "/conveniada/test/:id/dependente.html", // id = idContrato
        templateUrl : "templates/admin4_material_design/angularjs/views/conveniada/test/dependente.html",
        data : {
            pageTitle : 'Associado',
            // pageSubTitle : ''
        },
        authorizedRoles: ['conveniada'],
        controller : "DependenteController",
        resolve : {
            deps : ['$ocLazyLoad',function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    load_ngValidate,
                    load_inputMask,
                    load_uiSelect,
                    load_ngTable,
                    load_dirPagination,
                    {
                        name : 'MedicsystemApp',
                        insertBefore : '#ng_load_plugins_before',
                        files : [
                            'templates/admin4_material_design/angularjs/js/conveniada/test/DependenteController.js',
                            "assets/global/plugins/icheck/skins/all.css",
                            "assets/global/plugins/icheck/icheck.min.js",
                            'templates/admin4_material_design/angularjs/js/controllers/ModuloConveniadaControllers/DependetesConveniadaComponent.js',
                            "/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                        ]
                    }
                ]);
            }]
        }
    };

    $stateProvider
    .state(conveniada)
    .state(pageIndex)
    .state(associados)
    .state(detailAssociado)
    .state(guias)
    .state(guia)
    .state(guiaServicos)
    .state(coberturaPlano)
    .state(testDependente)
    ;
}]);
