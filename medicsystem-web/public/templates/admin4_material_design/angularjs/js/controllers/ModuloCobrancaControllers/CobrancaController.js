'use strict';

MedicsystemApp.controller('CobrancaController', function($scope, $rootScope, 
    $state,$stateParams,$filter,cobrancaFactory,$sce,NgTableParams,$modal, Auth,$window, $http) {

    function pushMessage( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage, 
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop(); 
    };

    function validarPesquisa( intervalo, idEmpresa, idFormaPagamento ) {

        var isPesquisaValida = true;

        if( intervalo == null ) {
            isPesquisaValida = false;
        }

        if( idEmpresa == null  ) {
            pushMessage('warning',"Por favor, escolha uma empresa",5000);
            isPesquisaValida = false;
        }

        return isPesquisaValida;

    };

    function configurarTimeline() {
        $scope.timelineSettings = {
            isAlternating: true,
            isWithArrows: false,
        };
    };

    function loadEmpresaFinanceiro() {

        var MEDICLAB_ID = 9;

        cobrancaFactory.empresagrupos.get( function( result ) {
            if ( result ) {

                for (var i = 0; i < result.length; i++) {
                    if( result[i].id == MEDICLAB_ID ) {
                       $scope.empresaGrupoSelected.empresaGrupo = result[i];      
                    }
                };

                $scope.empresaGrupoSelected.empresaGrupo = $scope.empresaGrupoSelected.empresaGrupo ? $scope.empresaGrupoSelected.empresaGrupo : result[0];
                $scope.empresaGrupoOptions = result;

                inicializarTipoDataPesquisa();
            }
        });
    };

    function loadFormasPagamento() {
        $scope.formaPagamentoOptions = [
            {id:99,nmFormaPagamento: 'TODAS'},
            {id:0,nmFormaPagamento: 'BOLETO BANCÁRIO'},
            {id:1,nmFormaPagamento: 'DÉBITO AUTOMÁTICO'},
            {id:2,nmFormaPagamento: 'CONTRA-CHEQUE'},
            {id:30,nmFormaPagamento: 'CARTÃO RECORRENTE'},
        ];
    };

    function configurarDateFiltro() {

        $scope.mostrarCalendario = true;
        $scope.mostrarCustomizado = false;

        $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };

        $scope.rangesDateRangePicker =  {
            'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
        }
    };

    function configurarDateFiltroSimplificado() {

        $scope.mostrarCalendario = false;
        $scope.mostrarCustomizado = false;
    
        $scope.rangesDateRangePicker =  {           
            'Hoje': [moment(), moment()],            
            'Últimos 30 dias': [moment().subtract(30, 'days'), moment()],
            'Últimos 60 dias': [moment().subtract(60, 'days'), moment()],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
            'Últimos 120 dias': [moment().subtract(120, 'days'), moment()],
            'Últimos 180 dias': [moment().subtract(180, 'days'), moment()],
        }
    };

    function inicializarTipoDataPesquisa() {
        $scope.optionDateSelected = { value: 1};

        $scope.rdOptionsDate = [
            {value:0, name:'Previsão pagamento'},
            {value:1, name:'Data próxima ligação'},
            {value:2, name:'Periodo inadimplência'},
            {value:3, name:'Inadimplência recente'},
        ];
    };

    function diaAtualCalendar() {
        var dataAtual = moment();
     
        $scope.dataFiltro = {};
        $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};          
    };

    function inicializarDataCalendario() {
        diaAtualCalendar();
        configurarDateFiltro();
    };

    function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };

    function inicializarPaginacao() {
        $scope.pagenumber = 1;
        $scope.total_count = 0;
        $scope.itemsPerPage = 15;
    };

    $scope.prepararModalEstatisticaOperadorCobranca = function() {
        var login =  Auth.authz.idTokenParsed.preferred_username;
        var nmFuncionario = Auth.authz.idTokenParsed.name.toUpperCase();
        var periodo = {dataInicio:moment().format("DD/MM/YYYY"), dataFim:moment().format("DD/MM/YYYY")}
        console.log("periodo", periodo );

        cobrancaFactory.totalCobrancaFuncionario
        .get( { login:login, dataInicio: periodo.dataInicio ,dataFim: periodo.dataFim}, function( result ) {
            console.log('result',result);
            if( result ) {

                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'templates/admin4_material_design/angularjs/views/cobranca/cobranca-estatisticaOperador-modal.html',
                    controller: 'ModalEstatisticaOperadorCobrancaCtrl',
                    resolve: {
                        nmFuncionario: function () {
                            return nmFuncionario;
                        },
                        totalCobranca: function () {
                            return result;
                        }
                    }
        });

        modalInstance.result.then(function ( retorno ) {
        }, function () {
               console.info('Modal dismissed at: ' + new Date());
        });
            }
        });
        
    };

    $scope.$watch('optionDateSelected.value', function() {
        
        if( $scope.optionDateSelected ) {

            if( $scope.optionDateSelected.value != 3 ) {
                
                configurarDateFiltro();
                
                var intervalo = getIntervaloDataSelecionada();
                var opcaoDataSeleciona = $scope.optionDateSelected.value;
                        
                if ( intervalo != null && opcaoDataSeleciona != null )
                     $scope.pesquisarClientesInadimplentes();    

            } else if( $scope.optionDateSelected.value == 3 ) {
                diaAtualCalendar();
                configurarDateFiltroSimplificado();
                $scope.pesquisarClientesInadimplentes();    
            }
        }
    });

    $scope.atualizarPesquisaCliente = function(text) {
        
        if (text) {

            var queryObject = {};
            $scope.clientes = null;
            text = text.toLowerCase();
            queryObject = { nmCliente: text };
         
            if(text){
                cobrancaFactory.clientes.query(queryObject, 
                    function( result ) {

                        if( result ) {
                            
                            if (result.length == 0) {
                                $scope.selectedClienteNotFound = "Sem dados disponiveis.";
                            } else {
                                $scope.clientes = result; 
                                $scope.selectedClienteNotFound = "";
                            }
                        }
                });
            }
        }
    
    };

    $scope.construirTimelineSimplificada = function( dadosContato ) {
        $scope.timelineEntries = [];
        $scope.dadosContatoCliente = [];

        dadosContato.forEach( function toTimelineElements(element, index, array) {
            
            var entradaTimeline = {};

            entradaTimeline.iconOnly = false;
            entradaTimeline.icon = {
                  isIcon: true,
                  isText: false,
                  noBorder: true,
                  value: 'fa-calendar-check-o',
                  color: '#1690ed'
            };
            entradaTimeline.content = {
                value: element.nmFuncionario,
                tipoContato: element.tipoContato
            };
            entradaTimeline.date = {
                value: element.dtContato,
                time: element.tempoDuracao
            }

            $scope.timelineEntries.push(entradaTimeline);
        });
    };

    $scope.historicoContatoSimplificado = function( idCliente ) {
        cobrancaFactory.clienteHistoricoSimplificado
        .get( {id:idCliente}, function( result ) {
            if ( result ){
                console.log("contatos", result);
                $scope.construirTimelineSimplificada(result);
            }
        });
    };

    $scope.alterarFormaPagtoClientesInadimplentes = function( formaPagto ) {
        
        var intervalo = getIntervaloDataSelecionada();
        
        if( intervalo != null) {
             $scope.pesquisarClientesInadimplentes();
        }
    };

    $scope.alterarEmpresaClientesInadimplentes = function( empresaGrupoSelecionada ) {
       
        var intervalo = getIntervaloDataSelecionada();
       
        if( intervalo != null) {
             $scope.pesquisarClientesInadimplentes();
        }
    };

    $scope.pesquisarPorCliente = function() {      
        if( $scope.canSearchCliente )
            $scope.canSearchCliente = false;
        else {
            $scope.canSearchCliente = true;
            $scope.clientesInadimplentes = [];
            $scope.somaTotalDivida = 0;
            $scope.somaTotalDividaComJuros = 0;
        }
    };

    $scope.pesquisarClienteInadimpente = function( cliente ) {
        $scope.clientesInadimplentes = [];

        $scope.somaTotalDivida = 0;
        $scope.somaTotalDividaComJuros = 0;

        inicializarPaginacao();

        if( cliente ) {
            cobrancaFactory.clientesInadimplentes
            .get( {nmCliente: cliente.nmCliente, boAdimplente:true},
                function( result ) {
                    if ( result )
                        $scope.clientesInadimplentes = result.clientes;

                            $scope.somaTotalDivida = $scope.clientesInadimplentes[0].somaTotalDivida;
                            $scope.somaTotalDividaComJuros = $scope.clientesInadimplentes[0].somaTotalDividaComJuros;
                }
            );
        }
    };
    
    $scope.ligar = function( numero, ramal) {
        var params = {
            numero: numero.replace(/[^0-9]/g,''),
            ramal: ramal            
        }
        
        cobrancaFactory.ligar.get(
            params,
            function(result) {
                console.log('sucesso');
            },
            function(err) {
                console.log('erro');
            }
        );
    }

    $scope.pesquisarClientesInadimplentesExport = function( ) {
        var intervalo = getIntervaloDataSelecionada();
        var idEmpresa =  $scope.empresaGrupoSelected.empresaGrupo != null ? $scope.empresaGrupoSelected.empresaGrupo.id : null;
        var idFormaPagamento = $scope.formaPagamentoSelected.formaPagamento != null && $scope.formaPagamentoSelected.formaPagamento.id != 99 ? $scope.formaPagamentoSelected.formaPagamento.id : null;
        var inOpcaoData = $scope.optionDateSelected.value;

      

            $http({
                    url: 'mediclab/cobranca/clientes/xls',
                    params:{nrIdEmpresa:idEmpresa, nrIdFormaPagamento:idFormaPagamento, nrOpcaoData: inOpcaoData, dataInicio:intervalo.dtInicio, dataFim: intervalo.dtFim,pagina:null,itensPagina:null,boAdimplente:false},
                    method: "GET",
                    headers: {
                       'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    console.log("headers",headers);
                    var header = headers('Content-Disposition')
                    var fileName = header.split("=")[1].replace(/\"/gi,'');
                    console.log(fileName);

                    var blob = new Blob([data], {type:'application/vnd.openxmlformats-officedocument.presentationml.presentation;charset=UTF-8'});
                    var objectUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                    var link = angular.element('<a/>');
                    link.attr({
                        href : objectUrl,
                        download : fileName
                    })[0].click();
                    //window.open(objectUrl);
                }).error(function (data, status, headers, config) {
                    //upload failed
                });

    }

    $scope.pesquisarClientesInadimplentes = function( newPagenNumber ) {

        inicializarPaginacao();   

        if (  !isNaN( newPagenNumber ) ) {
            $scope.pagenumber = newPagenNumber;
        }

        $scope.clientesInadimplentes = [];
        $scope.somaTotalDivida = 0;
        $scope.somaTotalDividaComJuros = 0;
        var intervalo = getIntervaloDataSelecionada();
        var idEmpresa =  $scope.empresaGrupoSelected.empresaGrupo != null ? $scope.empresaGrupoSelected.empresaGrupo.id : null;
        var idFormaPagamento = $scope.formaPagamentoSelected.formaPagamento != null && $scope.formaPagamentoSelected.formaPagamento.id != 99 ? $scope.formaPagamentoSelected.formaPagamento.id : null;
        var inOpcaoData = $scope.optionDateSelected.value;

        var isPesquisaValida = validarPesquisa( intervalo, idEmpresa, idFormaPagamento );
    
        if( isPesquisaValida ) {
            cobrancaFactory.clientesInadimplentes
            .get( {nrIdEmpresa:idEmpresa, nrIdFormaPagamento:idFormaPagamento, nrOpcaoData: inOpcaoData, dataInicio:intervalo.dtInicio, dataFim: intervalo.dtFim,pagina:$scope.pagenumber,itensPagina:$scope.itemsPerPage,boAdimplente:false},
                function( result ) {
                    if ( result.clientes ) {
                        console.log('result',result);
                        $scope.clientesInadimplentes = result.clientes;
                        $scope.somaTotalDivida = $scope.clientesInadimplentes[0].somaTotalDivida;
                        $scope.somaTotalDividaComJuros = $scope.clientesInadimplentes[0].somaTotalDividaComJuros;
                        $scope.total_count = result.quantidadeClientesTotal;
                        if(inOpcaoData == 2 || inOpcaoData == 3){
                            $scope.exportXLS = true;
                        }else {
                            $scope.exportXLS = false;
                        }
                        
                    }
                }
            );
          }
    };

	function inicio() {
        inicializarDataCalendario();
        loadEmpresaFinanceiro();  
        configurarTimeline();
        loadFormasPagamento();


        $scope.exportXLS = false;
        $scope.clientesInadimplentes = [];
        $scope.formaPagamentoSelected = {};
        $scope.empresaGrupoSelected = {};
        $scope.somaTotalDivida = 0;
        $scope.somaTotalDividaComJuros = 0;
	};

	inicio();

});

MedicsystemApp.controller('ModalEstatisticaOperadorCobrancaCtrl', function ($rootScope, $scope, $state, $modalInstance, $window, nmFuncionario, totalCobranca) {

    $scope.nmFuncionario = nmFuncionario;
    $scope.totalCobranca = totalCobranca;

    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

//TODO html binding
MedicsystemApp.filter('html', ['$sce', function($sce) {
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}])

MedicsystemApp.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});


