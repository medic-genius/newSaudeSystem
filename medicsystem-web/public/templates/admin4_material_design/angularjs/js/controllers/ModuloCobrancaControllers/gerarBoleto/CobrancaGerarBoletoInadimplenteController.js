'use strict';

MedicsystemApp.controller('CobrancaGerarBoletoInadimplenteController', function($scope, $rootScope, 
    $state,$stateParams,$filter,cobrancaFactory,$sce,NgTableParams,$modal, Auth,$window, $http, toastr) {
    
    var DR_CONSULTA_ID = 17;
    var DENTAL_ID = 13;
    var PERFORMANCE_ID = 15;
   


    function pushMessage( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage, 
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop(); 
    };

    function validarPesquisa( intervalo, idEmpresa, idFormaPagamento ) {

        var isPesquisaValida = true;

        if( intervalo == null ) {
            isPesquisaValida = false;
        }

        return isPesquisaValida;

    };
    
    function loadFormasPagamento() {
        $scope.formaPagamentoOptions = [];

        if( $stateParams.idEmpresaFinanceiro == PERFORMANCE_ID  )
            $scope.formaPagamentoOptions = [
                {id:50,nmFormaPagamento: 'BOLETO BANCÁRIO'},
                {id:1,nmFormaPagamento: 'DÉBITO AUTOMÁTICO'},
                {id:2,nmFormaPagamento: 'CONTRA-CHEQUE'},
                {id:3,nmFormaPagamento: 'CARTÃO RECORRENTE'},
            ];
        else
            $scope.formaPagamentoOptions = [
                {id:0,nmFormaPagamento: 'BOLETO BANCÁRIO'},
                {id:1,nmFormaPagamento: 'DÉBITO AUTOMÁTICO'},
                {id:2,nmFormaPagamento: 'CONTRA-CHEQUE'},
                {id:30,nmFormaPagamento: 'CARTÃO RECORRENTE'},
            ];

         $scope.formaPagamentoSelected.formaPagamento =  $scope.formaPagamentoOptions[1];
    };

    function configurarDateFiltro() {

        $scope.mostrarCalendario = true;
        $scope.mostrarCustomizado = false;

        $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };

        $scope.rangesDateRangePicker =  {
            'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
        }
    };

    function configurarDateFiltroSimplificado() {

        $scope.mostrarCalendario = false;
        $scope.mostrarCustomizado = false;
    
        $scope.rangesDateRangePicker =  {           
            'Hoje': [moment(), moment()],            
            'Últimos 30 dias': [moment().subtract(30, 'days'), moment()],
            'Últimos 60 dias': [moment().subtract(60, 'days'), moment()],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
            'Últimos 120 dias': [moment().subtract(120, 'days'), moment()],
            'Últimos 180 dias': [moment().subtract(180, 'days'), moment()],
        }
    };

    function inicializarTipoDataPesquisa() {
        $scope.optionDateSelected = { value: 2}; 

        $scope.rdOptionsDate = [
            {value:2, name:'Periodo inadimplência'},
            {value:3, name:'Inadimplência recente'},
        ];
    };
    
    function inicializarTipoValorPesquisa() {
        $scope.optionValorSelected = {value: 3};

        $scope.rdOptionsValor = [
            {value:3, name:'Total com juros'},
            {value:2, name:'Porcentagem (%)'},
            {value:1, name:'Valor Fixo (R$)'},            
        ];
    };

    function diaAtualCalendar() {
        var dataAtual = moment();
     
        $scope.dataFiltro = {};
        $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};          
    };

    function inicializarDataCalendario() {
        diaAtualCalendar();
        configurarDateFiltro();
    };

    function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };

    function inicializarPaginacao() {
        $scope.pagenumber = 1;
        $scope.total_count = 0;
        $scope.itemsPerPage = 10;
        $scope.totalChecked = 0;
    };

    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} ); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() ); 
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() ); 
        }
    };

    function validarEmail( email ) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if( re.test(email) )
            return email.toLowerCase();
        else
            return emailPadrao();
    };

    function emailPadrao() {
        return 'odontomed.ti@gmail.com';
    };

    function imprimirBoletos (listCobrancaDTO) {
    //var listCobrancaDTO = formatarCobrancaDTO($scope.listCobrancaImpressao);
        console.log('lista final imprimir',listCobrancaDTO);
        if( listCobrancaDTO.length > 0 ) {
            $http.post('mediclab/superlogica/cobranca/boletospdf', listCobrancaDTO,
                { responseType: 'arraybuffer'} 
            )
            .success(function (data) {
               var file = new Blob([data], {type: 'application/pdf'});
               var fileURL = URL.createObjectURL(file);
               window.open(fileURL);
               //concluirPesquisaFeedBack();
            })
            .error( function(err) {
                //concluirPesquisaFeedBack();
                messageToastr('erro','Ocorreu um erro na impressão.', 'DESCULPE :(')
            });
        }
    };
    

    $scope.prepararBoletosImpressao = function () {

        var listCobrancaDTO = formatarCobrancaDTO($scope.listCobrancaImpressao);
        imprimirBoletos(listCobrancaDTO);
    };

    function gerarCobrancaAvulsa( clientesID, intervalo ) {
        
        console.log("clientesID", clientesID);
        var boletoListDTO = [];
        
         if( $scope.optionDateSelected ) {
             console.log("opção de pesquisa", $scope.optionDateSelected);

            if( $scope.optionDateSelected.value != 3 ) {
                        
                cobrancaFactory.ccbrancaClienteAvulsaPeriodo
                .gerar( { dtInicio:intervalo.dtInicio, dtFim: intervalo.dtFim, nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro,
                          tipoValor: $scope.optionValorSelected.value, vlCobranca: $scope.vlCobranca.value}, clientesID 
                    ,function( result ) {
                        angular.forEach( result , function( item ) {
                            if ( item.status != '200' ) {
                                messageToastr( 'erro', 'Erro ao gerar cobrança do cliente: '+item.st_NomeCliente+'<br/>Causa: '+item.msg, 'ERRO' ) ;   
                            } else if ( item.status == '200' ) {
                                var boletoDTO = {};
                                boletoDTO.link = item.data.link_2via;
                                boletoDTO.nmCliente = item.st_NomeCliente;
                                boletoDTO.endereco = item.endereco;
                                boletoDTO.enderecoComplemento = item.enderecoComplemento;
                                boletoListDTO.push( boletoDTO );

                                messageToastr( 'sucesso', 'Sucesso ao gerar cobrança', 'SUCESSO' ) ;
                            };
                        });

                        if(boletoListDTO != null && boletoListDTO.length > 0)
                            imprimirBoletos(boletoListDTO);

                        atualizarPagina();
                }, function( err ) {
                    console.log("err", err);
                });
            
            }else if( $scope.optionDateSelected.value == 3 ) {
                
                cobrancaFactory.ccbrancaClienteAvulsa
                .gerar( { dtInicio:intervalo.dtInicio, dtFim: intervalo.dtFim, nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro }, clientesID 
                    ,function( result ) {
                        angular.forEach( result , function( item ) {
                            if ( item.status != '200' ) {
                                messageToastr( 'erro', 'Erro ao gerar cobrança do cliente: '+item.st_NomeCliente+'<br/>Causa: '+item.msg, 'ERRO' ) ;   
                            } else if ( item.status == '200' ) {
                                var boletoDTO = {};
                                boletoDTO.link = item.data.link_2via;
                                boletoDTO.nmCliente = item.st_NomeCliente;
                                boletoDTO.endereco = item.endereco;
                                boletoDTO.enderecoComplemento = item.enderecoComplemento;
                                boletoListDTO.push( boletoDTO );

                                messageToastr( 'sucesso', 'Sucesso ao gerar cobrança', 'SUCESSO' ) ;
                            };
                        });

                        if(boletoListDTO != null && boletoListDTO.length > 0)
                            imprimirBoletos(boletoListDTO);

                        atualizarPagina();
                }, function( err ) {
                    console.log("err", err);
                });
                
            }
         }

    };

    function gerarObjetoClienteSP ( item ) {
        var clienteSP = {};
        clienteSP.st_nome_sac = item.nmCliente;
        clienteSP.st_nomeref_sac = item.nmCliente;
        clienteSP.st_diavencimento_sac = 10; //dia;
        clienteSP.st_cgc_sac = item.nrCPF ? item.nrCPF : " ";
        clienteSP.st_rg_sac = item.nrRG ? item.nrRG : " "; // missing
        clienteSP.st_cep_sac = item.nrCEP ? item.nrCEP : null;
        clienteSP.st_endereco_sac = item.nmLogradouro ? item.nmLogradouro : null;
        clienteSP.st_numero_sac = item.nrNumero ? item.nrNumero : null;
        clienteSP.st_bairro_sac = item.nmBairro ? item.nmBairro : null;
        clienteSP.st_complemento_sac = item.nmComplemento ? item.nmComplemento : " ";
        clienteSP.st_cidade_sac = item.nmCidade ? item.nmCidade : null;
        clienteSP.st_estado_sac = item.nmUF ? item.nmUF : null; //missing
        clienteSP.fl_mesmoend_sac = 1;
        clienteSP.in_idCliente = item.idCliente;
        clienteSP.st_CodCliente = item.nrCodCliente ? item.nrCodCliente : null; //missing
        clienteSP.st_email_sac = item.nmEmail; // ? (item.nmEmail.trim().length > 0 ? validarEmail(item.nmEmail) : emailPadrao()) : emailPadrao();

        return clienteSP;
    };

    function definirEstagioClientes( lista ) {

        var estagioObject = { cadastro:[], boleto:[] };

        angular.forEach( lista, function( item ) {
            if( item.selected ) {
                if ( item.idClienteSl ) 
                    estagioObject.boleto.push( item.idCliente );
                else
                    estagioObject.cadastro.push( gerarObjetoClienteSP( item ) );
            }
        });

        return estagioObject;
    };

    function atualizarPagina() {

             configurarDateFiltro();

            var intervalo = getIntervaloDataSelecionada();
            var opcaoDataSeleciona = $scope.optionDateSelected.value;

            if ( intervalo != null && opcaoDataSeleciona != null )
                 $scope.pesquisarClientesInadimplentes();    
    };

    $scope.$watch('optionDateSelected.value', function() {
        
        if( $scope.optionDateSelected ) {

            if( $scope.optionDateSelected.value != 3 ) {
                
                configurarDateFiltro();
                
                var intervalo = getIntervaloDataSelecionada();
                var opcaoDataSeleciona = $scope.optionDateSelected.value;
                        
                if ( intervalo != null && opcaoDataSeleciona != null )
                     $scope.pesquisarClientesInadimplentes();    

            } else if( $scope.optionDateSelected.value == 3 ) {
                diaAtualCalendar();
                configurarDateFiltroSimplificado();
                $scope.pesquisarClientesInadimplentes();    
            }
        }
    });

    $scope.loadMensalidadesCliente = function( item ) {
        
        item.expanded = true;
        var intervalo = getIntervaloDataSelecionada();
        
        if( $scope.optionDateSelected ) {

            if( $scope.optionDateSelected.value != 3 ) {
                
                cobrancaFactory.mensalidadesInadimplentesCompartilhadoPeriodo.get({
                    nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro,
                    idCliente: item.idCliente,
                    dataInicio:intervalo.dtInicio, 
                    dataFim: intervalo.dtFim

                }, function(result){
                    $scope.mensalidadesCliente = result;
                    item.mensalidades = {};
                    item.mensalidades = result;

                });
                
            }else if( $scope.optionDateSelected.value == 3 ) {
                
                cobrancaFactory.mensalidadesInadimplentesCompartilhado.get({
                    nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro,
                    idCliente: item.idCliente,
                    dataInicio:intervalo.dtInicio, 
                    dataFim: intervalo.dtFim

                }, function(result){
                    $scope.mensalidadesCliente = result;
                    item.mensalidades = {};
                    item.mensalidades = result;

                });
                
            }
            
        }        
        
    };

    $scope.loadCobrancasMensalidade = function( item ) {
        item.expanded = true;
        cobrancaFactory.cobrancaMensalidadeCompartilhado.get({
            id: item.idMensalidade,
            nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro
                 
        }, function(result){
            //$scope.cobrancasMensalidade = result;
            item.cobrancas = {};
            item.cobrancas = result;
            angular.forEach(item.cobrancas, function( item ) { 
                angular.forEach($scope.listCobrancaImpressao, function( itemIn ) {
                //ar indice  = {};
                //indice = $scope.listCobrancaImpressao.indexOf(item);   
                //console.log("indice cobranca: "+indice);
                console.log("item ",item.id);
                console.log("itemIn ",itemIn.id);
                console.log("lista selecionados ",$scope.listCobrancaImpressao);
                if(item.id == itemIn.id ){
                   console.log("achou pelo for ", item.id);
                    item.selected = true;
                }
                    });
            });

        });
    };

    $scope.prepararGerarCobrancaAvulsa = function() {
        
        if($scope.optionValorSelected.value != 3 && ($scope.vlCobranca.value == null || $scope.vlCobranca.value == 0) ){
            messageToastr( 'erro', 'Valor de cobrança zero ou nulo', 'Campo Obrigatório' ) ;
        }else{   
       
            if( $scope.clientesInadimplentes.length > 0 ) {
                var estagioClienteObject = definirEstagioClientes( $scope.clientesInadimplentes  );
            } else
                return;

            console.log("estagioClienteObject",estagioClienteObject);

            var clienteIdList = [];
            var intervalo = getIntervaloDataSelecionada();

            // preparar boleto stage
            if ( estagioClienteObject.boleto.length > 0 )
                angular.forEach( estagioClienteObject.boleto, function( item ) { clienteIdList = estagioClienteObject.boleto  });

            // cadastro cliente stage
            if( estagioClienteObject.cadastro.length > 0 ) {
               var clientes = estagioClienteObject.cadastro;

                $scope.cadastrarClienteRequest = cobrancaFactory.cadastrarClienteSuperLogicaCompartilhado
                .salvar({nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro},clientes, function( result) {
                    if( result.length > 0 ) {
                        angular.forEach( result, function( item ) {
                            if ( item.status == '500') {
                                messageToastr('erro', 'Erro ao cadastrar ' + item.st_nome_sac + "<br/><small>"+ item.msg +"</small>" , 'OPS' ) ;
                            } else if ( item.status == '200' ) {
                                clienteIdList.push( item.in_idCliente );
                            }
                        })

                        gerarCobrancaAvulsa( clienteIdList, intervalo );
                    }
                }, function(err) {
                    messageToastr('erro', 'Erro ao realizar requisicao cadastro ' , 'NAO...' ) ;
                })    
            } else
                gerarCobrancaAvulsa( clienteIdList, intervalo );
        }        
    };
    
    $scope.prepararGerarEndereco = function() {
        
        if( $scope.clientesInadimplentes.length > 0 ) {
                        
            var clienteListDTO = [];
            
            angular.forEach( $scope.clientesInadimplentes, function( item ) {
                if( item.selected ) {                                        
                    var clienteCL = {};
                    clienteCL.link = null;
                    clienteCL.nmCliente = item.nmCliente;
                    clienteCL.endereco = item.nmLogradouro + ', ' + item.nrNumero + ' - ' + item.nmBairro;
                    clienteCL.enderecoComplemento = item.nrCEP + ' - ' + item.nmCidade + '-' + item.nmUF;
                    
                    console.log("boletolist",clienteCL);
                    clienteListDTO.push( clienteCL );
                }
            });
            
            if(clienteListDTO != null && clienteListDTO.length > 0)
                imprimirBoletos(clienteListDTO);
            
            atualizarPagina();
        } else
            return;
        
        
        
              
    }

    $scope.checkAll = function () {
        $scope.totalChecked = 0;

        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.clientesInadimplentes, function (item) {
            item.selected = $scope.selectedAll;

            if( $scope.selectedAll )
                $scope.totalChecked ++;
        });

    };

    function formatarCobrancaDTO ( lista ) {
        var boletoListDTO = [];
        angular.forEach( lista , function( item ) {
                        var boletoDTO = {};
                        boletoDTO.link = item.linkCobrancaBoleto_sl;
                        boletoDTO.nmCliente = item.nmCliente;
                        boletoDTO.endereco = item.nmEndereco;
                        boletoDTO.enderecoComplemento = item.nmEnderecoComplemento;
                        boletoListDTO.push(boletoDTO);

        });
        return boletoListDTO;
    };

    $scope.addRemoveListCobranca = function( item ) {
        if( item.selected){
            console.log("passou no select");
            var encontrado = false;

                angular.forEach($scope.listCobrancaImpressao, function( itemIn ) {
                console.log("add item ",item.id);
                console.log("add itemIn ",itemIn.id);
                console.log("add lista selecionados ",$scope.listCobrancaImpressao);
                    if(item.id == itemIn.id ){
                       encontrado = true;
                     }
                 });

            if(!encontrado)
                $scope.listCobrancaImpressao.push(item);
            //$scope.totalCheckedCobranca ++;
        } 
            
        else{
            console.log("passou no remove");
            var itensCobrancas = [];
            itensCobrancas = $scope.listCobrancaImpressao;
            //var indice  = $scope.listCobrancaImpressao.indexOf(item);    
            console.log("lista ",$scope.listCobrancaImpressao);
            //console.log("indice "+indice);
            //$scope.listCobrancaImpressao = itensCobrancas.splice(indice, 1);
            angular.forEach( $scope.listCobrancaImpressao , function( itemIn ) {
                if(itemIn.id == item.id);{
                    var indice  = $scope.listCobrancaImpressao.indexOf(item);
                    $scope.listCobrancaImpressao.splice(indice, 1);
                }
            });
            
            console.log("lista depois ",$scope.listCobrancaImpressao);
            
            $scope.totalCheckedCobranca --;
        }
            
    };


    $scope.recalcularTotalChecked = function( item ) {
        if( item.selected) 
            $scope.totalChecked ++;
        else
            $scope.totalChecked --;
    };

    $scope.atualizarPesquisaCliente = function(text) {
        
        if (text) {

            var queryObject = {};
            $scope.clientes = null;
            text = text.toLowerCase();
            queryObject = { nmCliente: text };
         
            if(text){
                cobrancaFactory.clientes.query(queryObject, 
                    function( result ) {

                        if( result ) {
                            
                            if (result.length == 0) {
                                $scope.selectedClienteNotFound = "Sem dados disponiveis.";
                            } else {
                                $scope.clientes = result; 
                                $scope.selectedClienteNotFound = "";
                            }
                        }
                });
            }
        }
    
    };

    $scope.alterarFormaPagtoClientesInadimplentes = function( formaPagto ) {
        
        var intervalo = getIntervaloDataSelecionada();
        
        if( intervalo != null) {
             $scope.pesquisarClientesInadimplentes();
        }
    };

    $scope.pesquisarPorCliente = function() {      
        if( $scope.canSearchCliente )
            $scope.canSearchCliente = false;
        else {
            $scope.canSearchCliente = true;
            $scope.clientesInadimplentes = [];
        }
    };

    $scope.pesquisarClienteInadimpente = function( cliente ) {
        $scope.clientesInadimplentes = [];
        inicializarPaginacao();

        if( cliente ) {
            cobrancaFactory.clientesInadimplentes
            .get( {nmCliente: cliente.nmCliente, boAdimplente:true},
                function( result ) {
                    if ( result )
                        $scope.clientesInadimplentes = result.clientes;
                }
            );
        }
    };



    $scope.pesquisarClientesInadimplentes = function( newPagenNumber ) {

        inicializarPaginacao();   

        if (  !isNaN( newPagenNumber ) ) {
            $scope.pagenumber = newPagenNumber;
        }

        $scope.clientesInadimplentes = [];
        var intervalo = getIntervaloDataSelecionada();
        var idEmpresa =  $scope.empresaGrupoSelected.empresaGrupo != null ? $scope.empresaGrupoSelected.empresaGrupo.id : null;
        var idFormaPagamento = $scope.formaPagamentoSelected.formaPagamento != null && $scope.formaPagamentoSelected.formaPagamento.id != 99 ? $scope.formaPagamentoSelected.formaPagamento.id : null;
        var inOpcaoData = $scope.optionDateSelected.value;

        var isPesquisaValida = validarPesquisa( intervalo, idEmpresa, idFormaPagamento );
    
        if( isPesquisaValida ) {
            cobrancaFactory.clientesInadimplentesCompartilhado
            .get( {nrIdEmpresaFinanceiro: $stateParams.idEmpresaFinanceiro, nrIdFormaPagamento:idFormaPagamento, nrOpcaoData: inOpcaoData, dataInicio:intervalo.dtInicio, dataFim: intervalo.dtFim,pagina:$scope.pagenumber,itensPagina:$scope.itemsPerPage,boAdimplente:false},
                function( result ) {
                    if ( result.clientes ) {
                        //result.clientes[0].idClienteSl = 6605;
                        //result.clientes[1].idClienteSl = 6605;
                        console.log('result.clientes[0].idClienteSl',result.clientes[0].idClienteSl);
                        $scope.clientesInadimplentes = result.clientes;
                        $scope.total_count = result.quantidadeClientesTotal;
                    }
                }
            );
          }
    };

    function definirEstiloTela() {
         $scope.objectStyle = {};
    
        if( $stateParams.idEmpresaFinanceiro == DR_CONSULTA_ID ) 
             $scope.objectStyle.color = 'blue';
        else if( $stateParams.idEmpresaFinanceiro == DENTAL_ID ) 
             $scope.objectStyle.color = 'red';
        else if( $stateParams.idEmpresaFinanceiro == PERFORMANCE_ID ||  $stateParams.idEmpresaFinanceiro == MEDIC_ID )
             $scope.objectStyle.color = 'green';
    };

    function inicio() {

        $scope.clientesInadimplentes = [];
        $scope.formaPagamentoSelected = {};
        $scope.empresaGrupoSelected = {};
        $scope.mensalidadesCliente = {};
        $scope.cobrancasMensalidade = {};
        $scope.listCobrancaImpressao = [];
        $scope.totalChecked = 0;
        $scope.vlCobranca = {};
        
        definirEstiloTela();
        inicializarDataCalendario();
        inicializarTipoDataPesquisa();
        inicializarTipoValorPesquisa();
        loadFormasPagamento();

    };

    inicio();

});

