'use strict';

MedicsystemApp.controller('CobrancaProcessoCobrancaController', function($rootScope,$scope, 
	$http, $timeout,$state,$stateParams,$modal,$filter, Auth,cobrancaFactory) {

    
    function pushMessage( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage, 
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop(); 
    };

    function loadTiposContatoCliente( tipoContato ) {
        cobrancaFactory.tiposContatoCliente
        .get( function( result ) {
            $scope.tipoContatoOptions = result;  
            if( tipoContato )
                $scope.clienteContatoCobrancaDTO.tipoContato = tipoContato;
            else
                $scope.clienteContatoCobrancaDTO.tipoContato = result[0].tipo;
        })
    };

    function inicializarData() {
        $scope.clienteContatoCobrancaDTO.dtProximaLigacao = {};
        $scope.clienteContatoCobrancaDTO.dtPrevistaPagamento = {};
       
        $scope.clienteContatoCobrancaDTO.dtProximaLigacao.date = moment().add(2, 'days');
        $scope.clienteContatoCobrancaDTO.dtPrevistaPagamento.date = moment();
    };

    function inicializarDadosClienteCobranca( contatoEmAberto ) {
        $scope.clienteContatoCobrancaDTO = {};
        $scope.clienteContatoCobrancaDTO.nmConversa = "";
        inicializarData();    
        
        if( contatoEmAberto != null ) {
            loadTiposContatoCliente( contatoEmAberto.tipoContato );            
            converterCobrancaRecuperada( contatoEmAberto );
        } else
            loadTiposContatoCliente();        
    };

    $scope.modalEnviarMensagem = function() {

        var modalInstance = $modal.open({
            animation:true,
            keyboard: false, 
            backdrop:'static',
            size:'sm',
            templateUrl: 'templates/admin4_material_design/angularjs/views/cobranca/cobranca-enviarMensagem.html',
            controller: 'ModalEnviarMensagemController',
            resolve: {
                parametro: function () {
                    return 1;
                }
            }
        });

        modalInstance.result.then(function (resultado) {
            if (resultado) {
                console.log("saiu modal");
            }
        }, function () {
            console.info('Modal dismissed at: ' + new Date());
        });

    };

    function configurarCalendario() {
        $scope.datePickerConfiguration = {
           "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };
    };

	function configuraEditorTexto() {
		$scope.optionsTextEditor = {
		    height: 150,
		    focus: true,
		    airMode: false,
		    toolbar: [
                ['edit',['undo','redo']],
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['fontclr', ['color']],
                ['fontface', ['fontname']],
                ['textsize', ['fontsize']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['table', ['table']],
		    ],
	  	};
	};

    function converterBeforeSubmit( cobranca ) {

        var cobrancaToSubmit = {};

        cobrancaToSubmit.tipoContato = cobranca.tipoContato; 
        
        cobrancaToSubmit.nmConversa = cobranca.nmConversa;
        cobrancaToSubmit.dtProximaLigacao = moment(cobranca.dtProximaLigacao.date).format('DD/MM/YYYY'); 
        cobrancaToSubmit.dtPrevistaPagamento = moment(cobranca.dtPrevistaPagamento.date).format('DD/MM/YYYY'); 

        if( cobranca.idSac != null )
            cobrancaToSubmit.idSac = cobranca.idSac;
        
        return  cobrancaToSubmit;
    };

    function converterCobrancaRecuperada( cobranca ) {
        $scope.clienteContatoCobrancaDTO = {};
        $scope.clienteContatoCobrancaDTO.dtProximaLigacao = {};
        $scope.clienteContatoCobrancaDTO.dtPrevistaPagamento = {};
        $scope.clienteContatoCobrancaDTO.idSac = cobranca.idSac;
        $scope.clienteContatoCobrancaDTO.tipoContato = cobranca.tipoContato;
        
        $scope.clienteContatoCobrancaDTO.dtPrevistaPagamento.date = moment(cobranca.dtPrevistaPagamento);
        $scope.clienteContatoCobrancaDTO.dtProximaLigacao.date = moment(cobranca.dtProximaLigacao);
 
        $scope.clienteContatoCobrancaDTO.nmConversa = cobranca.nmConversa;
    };

    function recuperarCobranca( sacId  ) {
        cobrancaFactory.contatoCliente
        .get({id:sacId}, function( result ) {
            if( result )
                converterCobrancaRecuperada( result );
        });
    };

    function validarContatoCobranca( contato ) {
        var isValidado = true;
        
       //validation via jquery cause datePicker has features for empty calendar values 
       if( !$('#dtProximaLigacao').val() ) {
            pushMessage("warning","Data da próxima ligação é obrigatória",5000);
            isValidado = false;
        }
        else if( !$('#dtPrevistaPagamento').val() ) {
            pushMessage("warning","Data de previsão de pagamento é obrigatória",5000);
            isValidado = false;
        }

        if( !contato.nmConversa ) {
            pushMessage("warning","A conversa é obrigatória",5000);
            isValidado = false;
        }

        return isValidado;
    };

    function salvarContatoCobranca( contatoToSubmit, tipoMsg ,msgFeedback ) {
        cobrancaFactory.contatoCliente
        .save({id:$stateParams.idCliente}, contatoToSubmit , function( result,responseHeaders ) {
            var location = responseHeaders('Location');
            if ( location ) {
                var locationArray = location.split("/");
                var sacId = locationArray[locationArray.length - 1];
                recuperarCobranca( sacId );
                pushMessage( tipoMsg, msgFeedback ,5000);
                }
            }, function( err ) {
                console.log('err',err);
            });
    };

    $scope.finalizarCobranca = function( clienteContatoCobrancaDTO ) {
        var isValidado = validarContatoCobranca( clienteContatoCobrancaDTO );

        if( isValidado ) {
            var contatoToSubmit = converterBeforeSubmit( clienteContatoCobrancaDTO );

            if ( clienteContatoCobrancaDTO.idSac ) 
                salvarContatoCobranca( contatoToSubmit, "info" , "Contato atualizado com sucesso" );
            else
                salvarContatoCobranca( contatoToSubmit, "success" ,"Contato salvo com sucesso" );
        }
    };

    function getClienteContatoCobrancaDTO() {
        $scope.clienteContatoCobrancaDTO = {};
        inicializarData();

        cobrancaFactory.contatoClienteAberto
        .get({id: $stateParams.idCliente}, function( data ) {

            if( data.idSac ) 
                inicializarDadosClienteCobranca( data );
            else
                inicializarDadosClienteCobranca( null );
       });
    };

	function inicio() {
        $scope.minDateProximaLigacao = moment().add(2, 'days').format("YYYY/MM/DD");
        $scope.minDatePrevisaoPagamento = moment().format("YYYY/MM/DD");

		configuraEditorTexto();
        configurarCalendario();
        getClienteContatoCobrancaDTO();
	};

	inicio();
			
});

// BEGIN MODAL SEND MESSAGE
MedicsystemApp.controller('ModalEnviarMensagemController', 
    function ($rootScope, $scope, $state, $modalInstance, cobrancaFactory, $modal, 
        parametro, $window, $ngBootbox) {


    $scope.ok = function () {
        $modalInstance.close(
            $state.go("atendimentoCliente.cliente-despesas-agendamento-nova", { idAgendamento: items.id})
        );
    };


     $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    $scope.prepararEnvioMensagem = function(template) {
        console.log("template",template);    
        $scope.templateMensagemTexto = template.mensagem;
        $scope.nrCelular="92 9 9122-4068";
    };

    function loadConfiguracaoEditorTexto() {
        $scope.optionsTextEditor = {
            height: 200,
            focus: false,
            airMode: false,
            toolbar: [],
        };
    }

    function loadTemplateMensagem() {

        $scope.templatesMensagem = [
            {id:1,inTipo:0,tipoDescricao:"Cobranca", mensagem:"Prezado Senhor,<br/><br/>Solicitamos o mais rápido possível o contato pelo (número) para comunicarmos assuntos do seu interesse, pois temos uma excelente proposta de acordo para o senhor.<br/>Aguardamos o seu contato (nome da empresa)<br/>Obrigado." },
            {id:1,inTipo:0,tipoDescricao:"Agradecimento",mensagem:"Prezado Senhor,<br/>Agradecemos a preferencia.<br/>Atenciosamento, (nome da empresa)."},
            {id:1,inTipo:0,tipoDescricao:"Modelo 3",mensagem:'sem mensagem brow'},
            {id:1,inTipo:0,tipoDescricao:"Modelo 4",mensagem:'sem mensagem brow'},
        ];

    }

    function inicio(){
        loadTemplateMensagem();
        loadConfiguracaoEditorTexto();
    }

    inicio();

});