'use strict';

MedicsystemApp.controller('CobrancaSituacaoFinanceiraController', function($scope,
	$state, $stateParams, $modal, cobrancaFactory) {

	$scope.loadCliente = function() {
		cobrancaFactory.clientes.get(
			{id: $stateParams.idCliente},
			function(result) {
				if(result) {
					$scope.cliente = result;
				}
			}
		);
	}
	function getContratosCliente() {
		cobrancaFactory.situacaoFinanceira
		.contratosCliente({id:$stateParams.idCliente }, function( result ) {
			if( result ) {
				$scope.contratos = [];
				$scope.contratos = result;
			}	
		})
	};

	$scope.carregarTitulosEmAberto = function(contrato) {
		contrato.titulosAbertos = [];
		cobrancaFactory.situacaoFinanceiraTitulosAbertos.get(
			{'nrContrato':contrato.nrContrato },
			function( result ) {
				if(result) {
					result = JSON.parse(JSON.stringify(result));
					contrato.titulosAbertos = result;
					$scope.configSelection(result, contrato);
				}
			}
		);
	};

	$scope.getDependentesContrato = function(contrato) {
		contrato.expanded = true;
		contrato.dependentes = [];
		cobrancaFactory.situacaoFinanceiraDependentes.get(
			{cliente:$stateParams.idCliente, contrato: contrato.idContrato},
			function(result) {
				if( result ) {
					contrato.dependentes = result;
				}
			}
		);
	};

	function inicio() {
		getContratosCliente();
		$scope.loadCliente();
	};

	inicio();

	$scope.configSelection = function(arr, contrato) {
		var selection = {
			list: [],
			selectAll: { value: false }
		};

		var sel = [];
		for(var i = 0; i < arr.length; ++i) {
			sel.push(arr[i]);
			sel[i].selected = false;
		}
		selection.list = sel;
		contrato.selection = selection;
	}

	$scope.getSelection = function(contrato) {
		var arr = [];
		var list = contrato.selection.list;
		for(var i = 0; i < list.length; ++i) {
			if(list[i].selected) {
				// var titulo = {};
				// for(var attr in $scope.contrato.titulosAbertos[i]) {
				// 	if(attr != 'selected') {
				// 		titulo[attr] = $scope.contrato.titulosAbertos[i][attr];
				// 	}
				// }
				// arr.push(titulo);
				arr.push(list[i]);
			}
		}
		return arr;
	}

	$scope.selectAllChanged = function(contrato) {
		$scope.setSelectionValue(contrato, contrato.selection.selectAll.value);
	}

	$scope.hasSelection = function(contrato) {		
		if(contrato.selection && contrato.selection.list) {
			var list = contrato.selection.list;
			for(var i = 0; i < list.length; ++i) {
				if(list[i].selected) {
					return true;
				}
			}
		}
		return false;
	}

	$scope.setSelectionValue = function(contrato, flag) {
		var value = !!flag;
		contrato.selection.selectAll.value = value;
		for(var i = 0; i < contrato.selection.list.length; ++i) {
			contrato.selection.list[i].selected = value;
		}
	}

	$scope.showPopup = function(contrato) {
		var modalInstance = $modal.open({
			animation: true,
			templateUrl: 'templates/admin4_material_design/angularjs/views/cobranca/gerar-boleto-titulos-modal.html',
			controller: 'ModalGerarBoletoTitulosCtrl',
			backdrop: 'static',
			size: 'md',
			resolve: {
				titulos: function () {
					return $scope.getSelection(contrato);
				},
				clienteData: function() {
					return $scope.cliente;
				}
			}
		});

		modalInstance.result.then(
			function(result) {
				if(result.value) {
					$scope.setSelectionValue(contrato, false);
				}
			},
			function() {
				console.info('Modal dismissed at: ' + new Date());
			}
		);
	}
});

MedicsystemApp.controller('ModalGerarBoletoTitulosCtrl', function($scope,
	$timeout, $http, $modalInstance, toastr, titulos, clienteData, cobrancaFactory) {

	$scope.ID_EMPRESA_FINANCEIRO = 17;
	$scope.cliente = clienteData;
	$scope.firstLoad = false;

	$scope.clienteContato = {
		nrCelular: undefined,
		shouldUpdate: false
	}

	$timeout(function() {
		$("#celular-cliente").inputmask('(99)99999-9999');
	}, 500);

	$scope.hasValidValue = function() {
		if($scope.totalValue) {
			var v = parseFloat($scope.totalValue);
			return v > 0;
		}
		return false;
	}

  $scope.openVencimentoPopup = function() {
  	$scope.popupVencimento.opened = true;
  }

  $scope.configDtPicker = function() {
  	var m = moment();
  	$scope.minDate = m.toDate();

		$scope.format = 'dd/MM/yyyy';
		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};
		$scope.popupVencimento = {
			opened: false
		}
  }

  	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.getTotalValue = function() {
		var sum = 0.0;
		for(var i = 0; i < titulos.length; ++i) {
			sum += titulos[i].valorComJuros;
		}
		$scope.totalValue = sum.toFixed(2);
		if(!$scope.firstLoad){
			$scope.valorInicio = $scope.totalValue;
		}
	}

	$scope.getListIdTitulos = function() {
		var list = [];
		for(var i = 0; i < titulos.length; ++i) {
			list.push(titulos[i].idTitulo);
		}
		return list;
	}

	$scope.cancel = function() {
		$modalInstance.close('dismiss');
	}

	$scope.close = function() {
		$modalInstance.close({value: true});
	}

	$scope.okButton = function() {
		//chamar query aqui
        //$scope.prepararGerarCobrancaTitulos();
		$scope.processing = true;
        $scope.gerarCobrancaAvulsa();		
	}

	$scope.usoPorcentagem = function (valor, valorPorc, valorTotal){
		$scope.showPorc = true;
		if (!valor){
			$scope.showPorc = true;
			$scope.firstLoad = true;
			if(valorPorc != undefined){
				$scope.disabledButton = true;
				var desconto1 = valorTotal*(valorPorc/100);
				var total = (valorTotal - desconto1).toFixed(2);
				$scope.totalValue = total;
			}else{
				$scope.disabledButton = false;
				$scope.totalValue = $scope.valorInicio;
			}
		} else {
			$scope.showPorc = false;
			$scope.firstLoad = false;
			$scope.totalValue = $scope.valorInicio;
		}
      
	}


	$scope.init = function() {
		$scope.configDtPicker();
		$scope.getTotalValue();

		var m = moment();

		var offsetDay = 3;
		if(m.days() >= 3 || m.days() <= 5) {
			offsetDay = 5;
		} else if(m.days == 6) {
			offsetDay = 4;
		}
		m.add(offsetDay, 'days');
		$scope.dtVencimento = m.toDate();
	}

	/**-------------------------------------------
	FORMATAR VALORES NUMERICOS **/
	$scope.scpliceStr = function(str, index, count, add) {
		var ar = str.split('');
		ar.splice(index, count, add);
		return ar.join('');
	}

	$scope.formatValue = function() {
		var v;
		if($scope.totalValue) {
			v = $scope.totalValue.replace(/\D/g, '');
		}
		if(!v) {
			v = '0.00';
		} else {
			var tmp = parseInt(v).toString();
			if(tmp.length == 1) {
				v = '0.0' + tmp;
			} else if(tmp.length == 2) {
				v = '0.' + tmp;
			} else {
				v = $scope.scpliceStr(tmp, -2, 0, '.');
			}
		}
		$scope.totalValue = v;
	}
	/**-------------------------------------------**/

	function messageToastr(type, message, title) {
		if( type == 'erro' ) {
			toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} ); 
		} else if( type == 'sucesso' ) {
			toastr.success(  message, title.toUpperCase() ); 
		} else if( type == 'informacao' ) {
			toastr.info(  message, title.toUpperCase() ); 
		}
	};

	$scope.imprimirBoletos = function(link) {
		if( link ) {
			//$http.post('mediclab/superlogica/cobranca/boletospdf', listCobrancaDTO,
			//	{ responseType: 'arraybuffer'} 
			//).success(function(data) {
				//var file = new Blob([data], {type: 'application/pdf'});
				var fileURL = link; //URL.createObjectURL(file);
				window.open(fileURL);
				//concluirPesquisaFeedBack();
			//}).error(function(err) {
				//concluirPesquisaFeedBack();
			//	messageToastr('erro','Ocorreu um erro ao reimprimir boleto', 'DESCULPA :(');
			//}).finally(function() {
				$scope.showPrintingMessage = false;
				$scope.processing = false;
				$scope.close();
			//});
		}
	};

	$scope.gerarObjetoClienteSP = function(item) {
		var clienteSP = {};
		clienteSP.st_nome_sac = item.nmCliente;
		clienteSP.st_nomeref_sac = item.nmCliente;
		clienteSP.st_diavencimento_sac = 10; //dia;
		clienteSP.st_cgc_sac = item.nrCPF ? item.nrCPF : " ";
		clienteSP.st_rg_sac = item.nrRG ? item.nrRG : " "; // missing
		clienteSP.st_cep_sac = item.nrCEP ? item.nrCEP : null;
		clienteSP.st_endereco_sac = item.nmLogradouro ? item.nmLogradouro : null;
		clienteSP.st_numero_sac = item.nrNumero ? item.nrNumero : null;
		clienteSP.st_bairro_sac = item.bairro.nmBairro ? item.bairro.nmBairro : null;
		clienteSP.st_complemento_sac = item.nmComplemento ? item.nmComplemento : " ";
		clienteSP.st_cidade_sac = item.cidade.nmCidade ? item.cidade.nmCidade : null;
		clienteSP.st_estado_sac = item.cidade.nmUF ? item.cidade.nmUF : null; //missing
		clienteSP.fl_mesmoend_sac = 1;
		clienteSP.in_idCliente = item.id;
		clienteSP.st_CodCliente = item.nrCodCliente ? item.nrCodCliente : null; //missing
		clienteSP.st_email_sac = item.nmEmail; // ? (item.nmEmail.trim().length > 0 ? validarEmail(item.nmEmail) : emailPadrao()) : emailPadrao();

		return clienteSP;
	};

	$scope.gerarCobrancaAvulsa = function() {
		var rParams = {
			idCliente: $scope.cliente.id,
			dtVencimento: moment($scope.dtVencimento).format('DD/MM/YYYY'),
			vlBoleto: parseFloat($scope.totalValue),
			nrCelular: $scope.clienteContato.nrCelular,
			updateCel: $scope.clienteContato.shouldUpdate
		}
		var rBody = $scope.getListIdTitulos();
        var link = null;
        
		cobrancaFactory.cobrancaTitulosCliente.post(
			rParams,
			rBody,
			function(result) {
				if(result && result.length > 0) {
					//var boletoListDTO = [];
					angular.forEach(result, function(item) {
						if(!item.link) {
							messageToastr( 'erro', 'Não foi possível gerar cobrança: '+item.msg, 'ERRO' );
							$scope.processing = false;
						} else {
                            link = item.link;
							messageToastr( 'sucesso', 'Sucesso ao gerar cobrança', 'SUCESSO');
                            /*
							var boletoDTO = {
								link: item.data.link_2via,
								nmCliente: item.st_NomeCliente,
								endereco: item.endereco,
								enderecoComplemento: item.enderecoComplemento
							}
							boletoListDTO.push(boletoDTO);
                            */
						};
					});

					
				    $scope.showPrintingMessage = true;
					$scope.imprimirBoletos(link);
					
				} else {
					$scope.processing = false;
				}
			}, function(err) {
				$scope.processing = false;
			}
		);
	};

	$scope.prepararGerarCobrancaTitulos = function() {
		if(!$scope.cliente.idClienteBoleto_sl) {
			var clienteSl = $scope.gerarObjetoClienteSP($scope.cliente);

			var rParams = {
				nrIdEmpresaFinanceiro: $scope.ID_EMPRESA_FINANCEIRO
			};

			var rBody = [clienteSl];

			cobrancaFactory.cadastrarClienteSuperLogicaCompartilhado.salvar(
				rParams,
				rBody,
				function(result) {
					if(result && result.length > 0) {
						angular.forEach(result, function(item) {
							if(item.status == '500') {
								messageToastr('erro', 'Erro ao cadastrar ' + item.st_nome_sac + "<br/><small>"+ item.msg +"</small>" , 'OPS' ) ;
								$scope.processing = false;
							} else if (item.status == '200') {
								var clienteIdList = [item.in_idCliente];
								$scope.gerarCobrancaAvulsa(clienteIdList);
							}
						});
					} else {
						$scope.processing = false;
					}
				},
				function(err) {
					messageToastr('erro', 'Não foi possível realizar a requisição.' , 'Erro' ) ;
					$scope.processing = false;
				}
			);
		} else {
			$scope.gerarCobrancaAvulsa();
		}
	};

	$scope.init();
});