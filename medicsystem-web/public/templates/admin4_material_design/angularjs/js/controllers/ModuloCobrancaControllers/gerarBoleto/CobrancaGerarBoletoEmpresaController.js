'use strict';

MedicsystemApp.controller('CobrancaGerarBoletoEmpresaController', 
  function($scope,$state,$stateParams,cobrancaFactory) {

  function loadEmpresas() {
    cobrancaFactory.empresaGrupoCompartilhado
    .query( function( list) {
      if( list )
        $scope.empresaOptions = list;
    });  
  };

  $scope.goToInadimplentes = function( empresaFinanceiro ) {
    $state.go('cobranca-gerarBoletoInadimplemntes',{ idEmpresaFinanceiro: empresaFinanceiro.idEmpresaFinanceiro});
  };

  function inicio (argument) {
    loadEmpresas();
  };

  inicio();

});


