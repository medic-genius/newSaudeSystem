'use strict';

MedicsystemApp.controller('CobrancaClienteAtendimentoController', function($scope,$state,$stateParams,cobrancaFactory,$window,$rootScope) {

/*	$scope.onExit = function() {
      return ('bye bye');
    };

   	$window.onbeforeunload =  $scope.onExit;

	window.onbeforeunload = function () {
   		console.log("before reload", $scope.finalTimer);
   		var teste = {hora:5, min:30};
   		localStorage.setItem('horario',JSON.stringify(teste));   		
	};*/

	function pushMessage( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage, 
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop(); 
    };

    function getImagemPerfilCliente( idCliente ) {
    	cobrancaFactory.fotoCliente.get( {id: idCliente},function(fotoCliente){
         	$scope.fotoClienteDecoded = fotoCliente.fotoDecoded;
        });
    }

	$scope.timerRunning = true;

	$scope.iniciarTimer = function (){
		$scope.$broadcast('timer-start');
	    $scope.timerRunning = true;
	};
 
    function pararTimer() {
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = true;
    };

    $scope.stopTimer = function() {
    	pararTimer();	
    }

    function continuarTimer() {
    	$scope.$broadcast('timer-resume');
    };

  	$scope.$on('timer-stopped', function (event, data){
  		$scope.finalTimer =  data;
		console.log('Timer Stopped - data = ', data);
    });

	function formatarTime( objectTime ) {

		var time = null;
		var hora,minuto,segundo;

		if( objectTime ) {
			hora = objectTime.hours <= 9 ? "0"+ objectTime.hours : objectTime.hours;
			minuto = objectTime.minutes <= 9 ? "0"+ objectTime.minutes : objectTime.minutes;
			segundo = objectTime.seconds <= 9 ? "0"+ objectTime.seconds : objectTime.seconds;
		}

		time = hora + ":" + minuto + ":" + segundo;
		console.log("time", time);

		return time;
	}

	function atualizarStatusAtendimentoCliente( idSac, timeDuration ) {
		$scope.buttonFinalizarDisabled = true;

		cobrancaFactory.contatoClienteFinalizar
        .finalizar({id: idSac, duracaoContato: timeDuration }, {} ,function( result ) {
            if( result ) {
            	$state.go('cobrancaCliente');
            }
       }, function( err) {
       		console.log("err",err);
       		$scope.buttonFinalizarDisabled = false;
       } );
	};

	$scope.cancelarFinalizarAtendimentoCobranca = function() {
		continuarTimer();
	};

	$scope.finalizarAtendimentoCobranca = function() {
		pararTimer();
		var timeDuration = formatarTime( $scope.finalTimer );
		cobrancaFactory.contatoClienteAberto
        .get({id: $scope.idCliente }, function( data ) {
            if( data.idSac ) 
            	atualizarStatusAtendimentoCliente( data.idSac, timeDuration);
            else {
            	continuarTimer();
            	pushMessage("warning","Você não realizou nenhuma forma de atendimento ao cliente. Ex: cobrança, negociação etc...", 10000);
            }

       });
	};

	function inicio( idCliente, nmCliente  ) {
		$scope.idCliente = idCliente;
		$scope.nmCliente = nmCliente;
		$scope.buttonFinalizarDisabled = false;
		getImagemPerfilCliente( $stateParams.idCliente );
	};	

	inicio( $stateParams.idCliente, $stateParams.nmCliente  );
				
});


