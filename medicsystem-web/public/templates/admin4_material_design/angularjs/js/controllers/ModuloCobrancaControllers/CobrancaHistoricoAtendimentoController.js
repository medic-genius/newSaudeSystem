'use strict';

MedicsystemApp.controller('CobrancaHistoricoAtendimentoController', function($scope,$state,$stateParams,cobrancaFactory) {

	function definirTimeLine( resultados ) {
		for ( var i = 0; i < resultados.length; i++) {

			var contatoRealizado = resultados[i];

			$scope.historicoContato.push({
				badgeClass: 'info',
				badgeIconClass: 'glyphicon-check',
				title: contatoRealizado.nmFuncionario ,
				content: 'Some awesome content.',
				when: contatoRealizado.dtContato,
				nmObservacao: contatoRealizado.nmConversa,
				tipoContato: contatoRealizado.tipoContato,
				dtProximaLigacao:contatoRealizado.dtProximaLigacao,
				dtPrevisaoPagamento:contatoRealizado.dtPrevistaPagamento
			});
		} 
	};

	function historicoContato( idCliente ) {
        cobrancaFactory.clienteHistoricoSimplificado
        .get( {id:$stateParams.idCliente}, function( result ) {
            if ( result ) {
                console.log("result",result);
                definirTimeLine( result );
            }
        });
    };

	function inicio() {
		$scope.historicoContato = [];
		historicoContato();
	}

	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	inicio();
			
});


