'use strict';

MedicsystemApp.controller('CobrancaDocumentosController', function($scope, $rootScope, 
    $state,$stateParams,$filter,cobrancaFactory,$sce ) {

    function loadDadosCliente( idCliente ) {
        $scope.cidadeOptions = cobrancaFactory.cidades.query();
        cobrancaFactory.clientes.get({id: idCliente},
        function( result ){
            if( result ) {
                $scope.selectedCliente = result;
                $scope.selectedCliente.dtNascimento = moment($scope.selectedCliente.dtNascimento).format("DD/MM/YYYY");
                if ( $scope.selectedCliente.cidade ) {
                    $scope.bairrosOptions = cobrancaFactory.cidadeBairros.get({ id:$scope.selectedCliente.cidade.id });
                }
            }
        });
    };

    $scope.loadImagensCliente = function() {
        if ( $stateParams.idCliente )
            $scope.arquivos = cobrancaFactory.clienteArquivos.get({id: $stateParams.idCliente });
    };

    function inicio() {
        loadDadosCliente( $stateParams.idCliente );
    }


    inicio();

});


