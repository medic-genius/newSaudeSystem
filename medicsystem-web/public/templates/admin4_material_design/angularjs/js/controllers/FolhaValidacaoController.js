'use strict';

MedicsystemApp.controller('FolhaValidacaoController', function($rootScope, $scope, $timeout,
	$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, $ngBootbox ) {
	
	function formatarData ( dataFormatar) {
        dataFormatar = dataFormatar.split("-");
        var mes = dataFormatar[0];
        var ano = dataFormatar[1];
        return ( mes + '/' + ano);
    } ;  

	function removerMesData ( mesAnoReferencia, qtdMesRemover ) {
    	
   		var periodoAtual = mesAnoReferencia;
    	var mesAnoReferenciaTemp = periodoAtual.split("/");

    	var mes = mesAnoReferenciaTemp[0] ;
    	var ano = mesAnoReferenciaTemp[1];
    	var dia = '01';

    	var dataFormatada = mes+"/"+dia+"/"+ano;
    
    	dataFormatada = new Date(dataFormatada);
    	dataFormatada.setMonth( dataFormatada.getMonth() - qtdMesRemover );

    	mes = (dataFormatada.getMonth() + 1) > 9 ? (dataFormatada.getMonth() + 1) : ('0'+ (dataFormatada.getMonth() + 1));
		ano = dataFormatada.getFullYear();

 		mesAnoReferencia = mes + "/" + ano;

 		return mesAnoReferencia;
		
    };

	function getTotaisGrupoFinanceiro ( tipoGrupoFinanceiro, mesanoreferencia ) {
    	servicesFactory.getTotalPorGrupo
    	.get( {intipogrupo: tipoGrupoFinanceiro,data: mesanoreferencia}, 
    		function( result ){
    			if( result ) {
    				$scope.totaisPorGrupoFinanceiro = result;
    			}
    	});
    };

    function getDescontosTotais ( idEmpresaFinanceiro, mesAnoReferencia ) {
    	servicesFactory.getDescontosTotais
    	.get( {idempresafinanceiro: idEmpresaFinanceiro,data: mesAnoReferencia}, 
    		function( result ){
    			if( result ) {
    				$scope.descontosTotaisWidget = result;
    			}
    	});
    };

    function inicializarView ( canEditFolha, canFormatData ) {
    	$scope.mesanoreferencia = canFormatData ? formatarData( $stateParams.mesAnoReferencia ) : $stateParams.mesAnoReferencia;
		$scope.idEmpresaFinanceiro = $stateParams.idEmpresaFinanceiro;
		$scope.sortType     = 'nome';
		$scope.sortReverse  = false;
		$scope.filterStatus = '';
		$scope.refreshFuncionarios();
		$scope.canEditFolha = canEditFolha;
		$scope.tipoGrupoFinanceiro = $stateParams.tipoGrupoFinanceiro;
		$scope.nmFantasia =  $stateParams.nmFantasia;

		getTotaisGrupoFinanceiro( $scope.tipoGrupoFinanceiro, $scope.mesanoreferencia );
		getDescontosTotais( $scope.idEmpresaFinanceiro, $scope.mesanoreferencia );
    };


	$scope.atualizarAcrescimoSalario = function( data ) {
		
		var funcionariosToUpdate = [];

		data.funcs.forEach( function(element,index) {
			if ( element.vlAcrescimoSalario != null ) {
				var dadosAtualizarProfissional = {};
				dadosAtualizarProfissional.id = parseInt(element.id);
				dadosAtualizarProfissional.description = element.vlAcrescimoSalario;
				funcionariosToUpdate.push( dadosAtualizarProfissional );
			}
		}) ;

		if ( funcionariosToUpdate.length > 0  ) {
			servicesFactory.folhaAcrescimo
			.funcionarioAcrescimo( {id: data.oif.id},funcionariosToUpdate,function( result) {
				if( result ) {
					data.oif.somavalorapagar = result.somavalorapagar;
					getTotaisGrupoFinanceiro( $scope.tipoGrupoFinanceiro, $scope.mesanoreferencia );
					servicesFactory.folhaPagamentoPeriodoReferencia.get( { idempresafinanceiro: $scope.idEmpresaFinanceiro  ,data:  $scope.mesanoreferencia}, function( oifAtualizado ) {
						$scope.folhaPagamentoImportadaSelected.somavalorapagar =  oifAtualizado.somavalorapagar;
					});
					$ngBootbox.alert(' Acréscimo realizado com sucesso. ');
				}
			} )	
		}
	};

	$scope.openReprovarModal = function( funcionario, idFolha, mesanoreferencia ) {
		$scope.mesanoreferencia = mesanoreferencia;
		var modalInstance = $modal.open({
	    	animation: true,
	    	templateUrl: 'templates/admin4_material_design/angularjs/views/folha/folha-funcionario-reprovar_folha-modal.html',
	    	controller: 'ModalFuncionarioReprovarFolhaCtrl',
	    	resolve: {
	    		funcionario: function () {
		          return funcionario;
		        },
		        idFolha: function() {
		          return idFolha;
		        },
		        mesanoreferencia: function(){
		          return mesanoreferencia;
		        },		        
	      	}
	    });
		
		modalInstance.result.then(function (deleted) {
			if(deleted) { funcionario.status=2;}
		});
	};
	
	$scope.aprovarFolhaFuncionario = function( funcionario, idFolha, mesanoreferencia ) {
		 $scope.idFolha = idFolha;
		 $scope.funcionarioSelected = funcionario;
		 $scope.mesanoreferencia = mesanoreferencia;
		 servicesFactory.folhaPagamentoAprova.aprova({ id: $scope.idFolha, idfunc:  $scope.funcionarioSelected.id }, function(result) { 
				if(result) {
					funcionario.status=1;
				}
			});	
	};
    
	$scope.$on('$viewContentLoaded', function() {
		Metronic.initAjax();
	});

    $scope.tabIndex = 0;
    $scope.tabledata = [];

    $scope.refreshFuncionarios = function() {	
		if( $scope.mesanoreferencia )
			$scope.folhaPagamentoImportadaSelected = servicesFactory.folhaPagamentoPeriodoReferencia.get( { idempresafinanceiro: $scope.idEmpresaFinanceiro  ,data:  $scope.mesanoreferencia}, function( ) {});
    };

    function folhaFuncTable(i) {
    	
    	var initialParams = { count:100 };

		var initialSettings = {
			counts:[], 
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: $scope.folhaPagamentoImportadaSelected.folhas[$scope.tabIndex].funcs
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  

    	$scope.tabledata[i] = {};
		$scope.tabledata[i].tableParams = new NgTableParams(initialParams,initialSettings);	
	}

    $scope.active = function() {
		if ($scope.folhaPagamentoImportadaSelected && $scope.folhaPagamentoImportadaSelected.folhas ) {
		    var i;
		    for (i=0;i<$scope.folhaPagamentoImportadaSelected.folhas.length;i++) {
		      if ($scope.folhaPagamentoImportadaSelected.folhas[i].active) {
		          $scope.teste = i;
		          return i;
		      }            
		    }
		}
	};

	$scope.$watch('active()', function(paneIndex) {
	    if (paneIndex !== undefined) {
	    	$scope.tabIndex = paneIndex;
	    	folhaFuncTable(paneIndex);
	      
	    }
	});
    
    $scope.getFluxoFuncPeriodoAnterior = function( funcionario ) {
    	var periodoAnterior = removerMesData($scope.mesanoreferencia, 1 );
 	    servicesFactory.getFluxosFuncByPeriodo
    	.get( {idempresafinanceiro: funcionario.empresafinanceiro.idEmpresaFinanceiro,
    		   codigo:funcionario.codigo,
    		   data: periodoAnterior,
    		   isFolhaAvulsa: funcionario.tbimportafolha.isFolhaAvulsa ? funcionario.tbimportafolha.isFolhaAvulsa : false}, 
    		function( result ){
    			if( result ) {
    				funcionario.fluxosMesAnterior = {};
    				funcionario.fluxosMesAnterior = result;
    			}
    	});

	  	funcionario.expanded = true;
    };

	if ($state.is('folhaValidar-visualizacao')) {
		if ($stateParams.mesAnoReferencia && $stateParams.idEmpresaFinanceiro)		
			inicializarView( false, true );
    };

	if ($state.is('folha_dashboard-validar')) {
		if ($stateParams.mesAnoReferencia && $stateParams.idEmpresaFinanceiro)
			inicializarView( true, false );
    };
});


MedicsystemApp.controller('ModalFuncionarioReprovarFolhaCtrl', function ($rootScope, $scope, $modalInstance, servicesFactory, funcionario, idFolha) {

	$scope.funcionario = funcionario;
	$scope.idFolha = idFolha;
	$scope.motivoExclusao = null;
	
	$scope.ok = function () {
		if ($scope.motivoExclusao == undefined || $scope.motivoExclusao.length < 5) {
			$rootScope.alerts.push({
                type: "danger", 
                msg: "Motivo de reprovação de funcionário é obrigatório e deve conter pelo menos 5 caracteres.",
                timeout: 5000 });
		} else {
			servicesFactory.folhaPagamentoReprova.reprova({ id: $scope.idFolha, idfunc: $scope.funcionario.id },$scope.motivoExclusao , function() {
				$modalInstance.close(true);
			});	
		}
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});