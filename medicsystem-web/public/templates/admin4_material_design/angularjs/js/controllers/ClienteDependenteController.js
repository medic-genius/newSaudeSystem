'use strict';

MedicsystemApp.controller('ClienteDependenteController', function($rootScope, $scope, $http, $timeout, $state, $location, $modal ,$stateParams, servicesFactory, toastr ) {
    var Dependente = servicesFactory.dependentes;
    var Informativo = servicesFactory.informativo;
    var InformativoInfo = servicesFactory.informativoinfo;
    $scope.dependenteSelected = [];
    $scope.isOpen  = true;
    $scope.qtdcheck = 0;
    $scope.botaoEnviar = false;



    $scope.parentescoOptions = [
         { id: 0,    description: 'Esposo(a)' },
         { id: 1,    description: 'Companheiro(a)' },
         { id: 2,    description: 'Filho(a)' },
         { id: 3,    description: 'Filho(a) Adotivo(a)' },
         { id: 4,    description: 'Pai' },
         { id: 5,    description: 'Mae' },
         { id: 6,    description: 'Irmao(a)' },
         { id: 7,    description: 'Avo/Avo' },
         { id: 8,    description: 'Sobrinho(a)' },
         { id: 9,    description: 'Enteado(a)' },
         { id: 10,    description: 'Sogro(a)' },
         { id: 11,    description: 'Neto(a)' },
         { id: 12,    description: 'Tio(a)' },
         { id: 13,    description: 'Primo(a)' },
         { id: 14,    description: 'Cunhado(a)' },
         { id: 15,    description: 'Nora' },
         { id: 16,    description: 'Genro' },
         { id: 17,    description: 'Amigo(a)' },
         { id: 18,    description: 'Outros' },
         { id: 19,    description: 'Nao Informado' }
     ];

    $scope.loadBairroOptions = function(cidade) {
        if(cidade){
           $scope.bairrosOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
        }
    };

    $scope.cidadeOptions = servicesFactory.cidades.query();

    $rootScope.refreshDependentes = function() {
        if ($scope.selectedCliente
            && $scope.selectedCliente.id != undefined) {
            $scope.dependentes = servicesFactory.clienteDependentes.get({id: $scope.selectedCliente.id });
        }
    }

    $rootScope.refreshOnlyDependente = function() {

            $scope.dependente = Dependente.get({id: $scope.dependente.id });
            servicesFactory.fotoDependente.get({ id: $stateParams.id }, function(fotoDep){
                    $scope.dependente.fotoDependenteDecoded = fotoDep.fotoDecoded;
                });
    }

    $scope.getInformativo = function(){
        $scope.informativoOptions = [];

        Informativo.get({}, function(dados){
            $scope.informativoOptions = dados;
        });

    }

    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} );
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() );
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() );
        }
    };

    $scope.enviarDependente = function (dependente){
        $scope.enviarDependenteSelect = dependente;

    }

    // LOAD POPOVER

              $scope.myPopover = {

                isOpen: false,

                templateUrl: 'myPopoverInformativo.html',

                open: function open() {
                  $scope.myPopover.isOpen = true;
                  for (var i = 0; i < $scope.informativoOptions.length; i++){
                     if ($scope.informativoOptions[i].value1 == true){
                        $scope.informativoOptions[i].value1 = false;
                    }
                }

                },

                close: function close() {
                    $timeout(function() {
                        angular.element('#popoverBtn').trigger('click');
                    }, 0);
                  for (var i = 0; i < $scope.informativoOptions.length; i++){
                     if ($scope.informativoOptions[i].value1 == true){
                        $scope.informativoOptions[i].value1 = false;
                    }
                    $scope.botaoEnviar = false;
                    $scope.qtdcheck = 0;
                }
                }
              };



    $scope.enviarSMS = function(){

        $scope.informativoSelect = [];
        for (var i = 0; i < $scope.informativoOptions.length; i++){

            if ($scope.informativoOptions[i].value1 == true){

            $scope.informativoSelect.push($scope.informativoOptions[i]);

            }
        }




        var listaInformativo = [];

        for (var i = 0; i < $scope.informativoSelect.length; i++){
            var InformativoDTO = {nmInformativo : $scope.informativoSelect[i].nmInformativo,
                        nrCelular : $scope.enviarDependenteSelect.nrCelular,
                        nrTelefone : $scope.enviarDependenteSelect.nrTelefone};

           listaInformativo.push(InformativoDTO);
        }


        InformativoInfo.save({},listaInformativo, function (data){
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                });
            }, 4000);
            if (data.response[0].returnCode == "001"){
            messageToastr("erro","Não foi possivel enviar a mensagem, verifique o número e celular","ERRO NA MENSAGEM")
            }
            else if (data.response[0].returnCode != "200"){
            messageToastr("erro","Não foi possivel enviar a mensagem, entre em contato com o TI","ERRO NA MENSAGEM")
            }
            else{
            sleep(5000);

            $scope.botaoEnviar = false;
            messageToastr("sucesso","Mensagem enviada, aguarde até 2 minutos para confirmar","MENSAGEM ENVIADA");
            $scope.myPopover.close();
            }

        }, function(erro){
            messageToastr("erro","Não foi possivel enviar a mensagem, verifique o número e celular","ERRO NA MENSAGEM")
        });

        for (var i = 0; i < $scope.informativoOptions.length; i++){
           if ($scope.informativoOptions[i].value1 == true){
                $scope.informativoOptions[i].value1 = false;
            }
              $scope.botaoEnviar = false;
             $scope.qtdcheck = 0;
        }

    }

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }}

    $scope.check = function(valor){


        if (valor){

            $scope.qtdcheck++;
        }
        else
            $scope.qtdcheck--;
    }

    if($state.is('atendimentoCliente.cliente-dependentes')){
        //INIT
        $rootScope.refreshDependentes();
    }

    if($state.is('atendimentoCliente.cliente-dependentes-editar')){
        if ($stateParams.id) {
            $scope.dependente = Dependente.get({ id: $stateParams.id }, function(){
                $scope.loadBairroOptions($scope.dependente.cidade);
                servicesFactory.fotoDependente.get({ id: $stateParams.id }, function(fotoDep){
                    $scope.dependente.fotoDependenteDecoded = fotoDep.fotoDecoded;
                });
            });
        }
    }



    $scope.openModalDigitalDependente = function() {

        $scope.saveDigitalFotoDependente = Dependente.get({ id: $stateParams.id });

        var itemsCliente = {
                            Dependente: servicesFactory.dependentes,
                            selectedDependente: $scope.saveDigitalFotoDependente
                            };



        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'cadDigitalFotoModalDepContent.html',
            controller: 'ModalInstanceCadDigitalFotoDepCtrl',
            backdrop: 'static',
            resolve: {
                    items: function () {
                        return itemsCliente;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                $scope.dependente = Dependente.get({id: $stateParams.id});
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    };

    $scope.showCamera = false;

    $scope.openModalAbrirCameraFoto = function() {

        $scope.showCamera = true;

        var itemsDependente = {
                            idCliente: $scope.selectedCliente.id,
                            Dependente: servicesFactory.dependentes,
                            selectedDependente: $scope.dependente
                            };

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'abrirFotoDependente.html',
            controller: 'ModalInstanceCadFotoDependenteCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                    items: function () {
                        return itemsDependente;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    }

        $scope.loadEndereco = function(){

             if ($scope.endereco.titular){
             $scope.dependente.nrCep = $scope.selectedCliente.nrCEP;
             $scope.dependente.nmLogradouro = $scope.selectedCliente.nmLogradouro;
             $scope.dependente.nrNumero = $scope.selectedCliente.nrNumero;
             $scope.dependente.nmComplemento = $scope.selectedCliente.nmComplemento;
             $scope.dependente.cidade = $scope.selectedCliente.cidade;
             $scope.dependente.bairro = $scope.selectedCliente.bairro;
             $scope.dependente.nmPontoReferencia = $scope.selectedCliente.nmPontoReferencia;

             $scope.loadBairroOptions($scope.dependente.cidade);
            }else{
            $scope.dependente.nrCep = null;
             $scope.dependente.nmLogradouro = null;
             $scope.dependente.nrNumero = null;
             $scope.dependente.nmComplemento = null;
             $scope.dependente.cidade = null;
             $scope.dependente.bairro = null;
             $scope.dependente.nmPontoReferencia = null;
            }
        };

        $scope.updateDependenteCadastro = function() {

        if (!$stateParams.id)
        {
            servicesFactory.depedenteSave.save({id: $scope.selectedCliente.id},
                $scope.dependente,
                function(success){
                    $rootScope.addDefaultTimeoutAlert("Dependente", "criado", "success");
                    $rootScope.refreshDependentes();
                $state.go('atendimentoCliente.cliente-dependentes');
                },function(error){

                });
                 Metronic.scrollTop();

        }else{
            if('string' == typeof $scope.dependente.nrCpf)
            {

                $scope.dependente.nrCpf = $scope.dependente.nrCpf.replace(/\D/g,'');
            }
            if ($('#formDependente').valid()) {
                Dependente.update({id: $scope.dependente.id}, $scope.dependente, function(){
                    $rootScope.addDefaultTimeoutAlert("Dependente", "atualizado", "success");
                    $rootScope.refreshDependentes();
                $state.go('atendimentoCliente.cliente-dependentes');
                }, function(){
                    $rootScope.addDefaultTimeoutAlert("Dependente", "atualizar", "error");
                });

                Metronic.scrollTop();

            }
        }
        };


});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalInstanceCadDigitalFotoDepCtrl', function ($rootScope, $scope, $state, $modalInstance, items) {

    $rootScope.digitalFotoDepMsg = "Capturar Digital";
    var DependenteService = items.Dependente;

    $scope.$watch('digitalCapturadaMsg', function() {
        if($rootScope.digitalCapturadaDepMsg != undefined){

            $rootScope.digitalFotoDepMsg = $rootScope.digitalCapturadaDepMsg;
            $("#btnCadDepDigitalFoto").prop("disabled",true);
        }
    });

    $scope.cadDigitalFoto = function () {
        var socket = io.connect('http://localhost:9092');

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });



        var jsonObject = {id: items.selectedDependente.id,
                          nmCliente: items.selectedDependente.nmDependente,
                          tipoPessoa: 'Dependente'};

        $rootScope.digitalFotoDepMsg = "Processando...";
        $rootScope.digitalCapturadaDepMsg = undefined;

        socket.emit('biometriaregisterevent', jsonObject);

        socket.on('biometriaregisterevent', function(data) {

            $rootScope.digitalCapturadaDepMsg = data;
            var typeEvent;

            if(data != undefined
                && (data.digital != undefined)){
                    items.selectedDependente.digitalDependente = data.digital;
                    items.selectedDependente.idDedoDigital = data.idDedoDigital;

                    DependenteService.update( {id: items.selectedDependente.id},  items.selectedDependente, function(data){
                     $rootScope.addDefaultTimeoutAlert("Digital ", "capturada", "success");
                     $rootScope.refreshOnlyDependente();
                    typeEvent = 'success';
                    socket.emit('disconnect');
                    socket.removeAllListeners();

                    }, function(error){
                    $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                    });


            } else {

                typeEvent = 'danger';
                $rootScope.alerts.push({
                    type: typeEvent,
                    msg: data
                });
            }

            setLabels();
            Metronic.scrollTop();
            $modalInstance.close();
        });
    };

    $scope.exit = function () {

        setLabels();
        $modalInstance.close();
    };

    $scope.cancel = function () {

        setLabels();
        $modalInstance.dismiss('cancel');
    };

    function setLabels(){

        $rootScope.digitalFotoDepMsg = "Capturar Digital";
        $rootScope.digitalCapturadaDepMsg = undefined;
    }
});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalInstanceCadFotoDependenteCtrl', function ($rootScope, $scope, $state, $modalInstance, items) {

    $scope.cameraSettings = {
        videoHeight: 240,
        videoWidth: 320
    };

    $scope.makeSnapshot = function makeSnapshot() {

        var _video = $scope.cameraSettings.video;
        var patData = null;

        $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};
        $scope.patOpts.w = _video.width;
        $scope.patOpts.h = _video.height;

        if (_video) {

            var patCanvas = document.querySelector('#snapshot');
            if (!patCanvas) return;

            patCanvas.width = _video.width;
            patCanvas.height = _video.height;
            var ctxPat = patCanvas.getContext('2d');

            var idata = getVideoData(_video, $scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
            ctxPat.putImageData(idata, 0, 0);

            sendSnapshotToServer(patCanvas.toDataURL(), items.Dependente, items.selectedDependente, items.idCliente);

            patData = idata;
        }
    };

    var getVideoData = function getVideoData(_video, x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = _video.width;
        hiddenCanvas.height = _video.height;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, _video.width, _video.height);
        return ctx.getImageData(x, y, w, h);
    };

    /**
     * This function could be used to send the image data
     * to a backend server that expects base64 encoded images.
     *
     * In this example, we simply store it in the scope for display.
    */
    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64, Dependente, selectedDependente, idCliente) {

        $scope.snapshotData = escape(imgBase64.split(',')[1]);
        selectedDependente.fotoDependenteEncoded = $scope.snapshotData;

        Dependente.update( {id: selectedDependente.id},  selectedDependente, function(data){
            $rootScope.addDefaultTimeoutAlert("Foto do Dependente", "atualizada", "success");
            selectedDependente = Dependente.get({id: selectedDependente.id});
        }, function(error){
            $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
        });

        Metronic.scrollTop();

        $modalInstance.close(
            $rootScope.$broadcast('updateUserBroadcast', 'true')
        );
    };


    $scope.exit = function () {

        $modalInstance.close();
    };

    $scope.cancel = function () {

        $modalInstance.dismiss('cancel');
    };
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
