'use strict';

MedicsystemApp.controller('AnaliseFinanceiraController', function($rootScope,$scope,$state,

	$stateParams,financeiroFactory,Auth, NgTableParams,toastr, $modal) {

	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:10 };
			var initialSettings = {
				counts:[], 
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings );
			
		}
	};
	

  	function messageToastr(type, message, time) {
		if( type == 'erro' ) {
	    	toastr.error(  message, 'ERRO'); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, 'SUCESSO'); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, 'SUCESSO'); 
	    }
	};


	function inicializarData() {
   		var dataAtual = moment();
     
        $scope.dataFiltro = {};

        if( $stateParams.objectSearch == null )
        	$scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};  
        else
        	$scope.dataFiltro.date = {startDate: moment( $stateParams.objectSearch.intervalo.dtInicio, 'DD/MM/YYYY' ), endDate: moment( $stateParams.objectSearch.intervalo.dtFim, 'DD/MM/YYYY' )};  
    }

    function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],

        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	/* 'Hoje': [moment(), moment()],
	   		'Próximos 30 Dias': [ moment(), moment().add(29, 'days')],
	   		'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
			   'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()], */
			'Últimos 12 Meses' : [moment().subtract(12, 'month').startOf('month'), moment().endOf('month')],
			'Últimos 6 Meses': [moment().subtract(6, 'month').startOf('month'), moment().endOf('month')],
			'Últimos 3 Meses': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
			'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,'month').endOf('month')],
			'Este mês': [moment().startOf('month'), moment().endOf('month')], 
			'Próximos Mês': [moment().add(1, 'month').startOf('month'), moment().add(1,'month').endOf('month')],
			'Próximos 3 Meses': [moment().add(1,'month').startOf('month'), moment().add(3, 'month').endOf('month')],
			'Próximos 6 Meses':	[moment().add(1, 'month').startOf('month'), moment().add(6, 'month').endOf('month')]
		}
    };

  	function getEmpresaFinanceiro(){
		financeiroFactory.empresaFinanceiro.query(function(empresaFinanceiroList){
			var obj = {idEmpresaFinanceiro: 0, nmFantasia: "TODOS"}
			empresaFinanceiroList.push(obj);
			if(empresaFinanceiroList.length > 0){
				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
				for (var i = 0; i < empresaFinanceiroList.length; i++) {
					if( empresaFinanceiroList[i].idEmpresaFinanceiro == 0 ) {
					  $scope.empresaFinanceiro.selected = empresaFinanceiroList[i];
					}
			  	};	
			}else {
				$scope.empresaFinanceiro.selected = $stateParams.objectSearch.empresaFinanceiro;
			}
		})
	};

	function getFiltrosCC(){
		$scope.listContaContabil=[];
		var obj = {id: 0, nmContaContabil: "Todos"};
		$scope.listContaContabil.push(obj);

		financeiroFactory.allContasContabil.get({id:0},function(result){
			for(var i=0; i < result.length; i++){
				$scope.listContaContabil.push(result[i]);
			}
		});	
	}
	  
    function getStatusSelecionado( firstLoad ) {
		if( firstLoad == false ) {
			$scope.inStatusFiltro = null;
		} else {
			if( !$scope.inStatusFiltro )	
				$scope.inStatusFiltro = null;
		}

		return $scope.inStatusFiltro;
	};

	$scope.$watch('inStatusFiltro', function() {
		if( $scope.inStatusFiltro != null) {
			$scope.definirCorFiltroStatus( $scope.inStatusFiltro );

		}	
		else {
			if( $scope.empresaFinanceiro.selected ) {
				inicializarColorFiltroStatus()
			}
		}
	});

	
	$scope.loadLista = function(choice){
		if(choice == 1){
			$scope.listContaContabil.selected = undefined;
		} else{
			$scope.listCentroCusto.selected = undefined;
		}
	}

	$scope.loadFilter = {
		selected: undefined
	}

	$scope.filters = [
		{
			nome: 'Aberto',
			status: 0,
			checked: false
		},
		{
			nome: 'Pago',
			status: 1,
			checked: false
		},
		/* {
			nome: 'Atrasado',
			status: 2,
			checked: false
		}, */
		/* {
			nome: 'Não Pago',
			status: 3,
			checked: false
		} , */
		{
			nome: 'Todos',
			status: 2,
			checked: false
		}
	]

	function definirEstiloPorCC( escolhaCC ) {
		$scope.mostrarTab = true;
		$scope.cssStyleObject.color = "blue-steel";
	};

	function inicializarColorFiltroStatus() {
		$scope.cssStyleObject.color1 = 'default';
		$scope.cssStyleObject.color2 = 'default';
		$scope.cssStyleObject.color3 = 'default';
	};

	 function inicializarColorFiltroModalidade(){
		$scope.cssStyleObject.color4 = 'default';
		$scope.cssStyleObject.color5 = 'default';
		$scope.cssStyleObject.color6 = 'default';
	};

	$scope.definirCorFiltroStatus = function( escolhido ) {
		if( escolhido == 2 )  {
			$scope.cssStyleObject.color1 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color3 = 'default';
		} else if( escolhido == 0) {
			$scope.cssStyleObject.color2 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color3 = 'default';
		} else if( escolhido == 1) {
			$scope.cssStyleObject.color3 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
		}

	};

	$scope.definirCorFlitroModalidade = function(modalidade){
		if(modalidade == 4){	
			$scope.cssStyleObject.color4 = "blue-steel";
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
		}else if(modalidade == 5){

			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = "blue-steel";
			$scope.cssStyleObject.color6 = 'default';
		}else if(modalidade == 6){

			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 =  "blue-steel";

		}
	}

	

	$scope.escolhaCC = function (nmCC, firstLoad){
		$scope.primeiroLoad = firstLoad;
		$scope.showTab = false;
		$scope.nmCC = nmCC;
		var statusSelected = getStatusSelecionado(firstLoad);
		if(nmCC == 1){
			$scope.CCescolhido = true;// Conta Contábil
			$scope.tabelaPrincipal = 0;
		} else {
			$scope.CCescolhido = false; //Centro de Custo
			$scope.tabelaPrincipalCentroCusto = 0;

		}
		definirEstiloPorCC( $scope.CCescolhido);
	}
		
	
	function organizar(list, item, isVisible, parent, tipo) {
		if(tipo == 1){
			item.parent = parent;
			item.isVisible = isVisible;
			list.push(item);
			if(item.contasContabil && item.contasContabil.length > 0) {
				for(var i = 0; i < item.contasContabil.length; ++i) {
					organizar(list, item.contasContabil[i], false, item, 1);
				}
			} 
		}else if(tipo == 2){
			item.parent = parent;
			item.isVisible = isVisible;
			list.push(item);
			if(item.centroCustos && item.centroCustos.length >= 0) {
				for(var i = 0; i < item.centroCustos.length; ++i) {
					organizar(list, item.centroCustos[i], false, item, 2);
				}
			}
		}
	
	}

	function getCheckedFilterStatus() {
		var list = [];
		for(var i = 0; i < $scope.filters.length; ++i) {
			if($scope.filters[i].checked) {
				list.push($scope.filters[i].status);
			}
		}
		return list;
	}

	$scope.loadStatus = function(item) {
		if(item == 1){
			/* if($scope.dataFiltro.date.startDate && $scope.dataFiltro.date.endDate && $scope.listContaContabil.selected && $scope.empresaFinanceiro.selected.length > 0) {
				$scope.loadContasContabil($scope.listContaContabil.selected);
			} */
		} else{
			/* if($scope.dataFiltro.date.startDate && $scope.dataFiltro.date.endDate && $scope.listCentroCusto.selected && $scope.empresaFinanceiro.selected.length> 0) {
				$scope.loadCentroCusto($scope.listCentroCusto.selected);
			} */
		}
	}

	$scope.setInstatus = function(item){
		var arrayStatus = [];
	}

	$scope.setVisible = function(itemId, isVisible, tipo, isAnaliseHorizontal) {
		if(!isAnaliseHorizontal){
			var list = $scope.lisaAll;
		}else {
			var list = $scope.lisaAllAH;
		}
		
		if(!list) { return ;}
		for(var i = 0; i < list.length; ++i) {
			if(list[i].parent && list[i].parent.id == itemId) {
				list[i].isVisible = isVisible;
				if(!isVisible) {
					list[i].expanded = false;
					$scope.setVisible(list[i].id, false, tipo);
				}
			}
		}

	}

	

	function getMesesFormatada(dadosMeses){
		var dataFormatada = dadosMeses.mesAno.split('-');
		dadosMeses.mes = dataFormatada[0];
		dadosMeses.ano = dataFormatada[1];
		var mesAnoForm = getNomeMes(dadosMeses);
		dadosMeses.mesAnoFormatada = mesAnoForm;

		return dadosMeses.mesAnoFormatada;
	}

	function getNomeMes(meses){
		if( meses.mes == '1'){
			meses.mesAnoFormatada = "JAN/"+meses.ano;
		} else if(meses.mes == '2'){
			meses.mesAnoFormatada = "FEV/"+meses.ano;
		} else if(meses.mes == '3'){
			meses.mesAnoFormatada = "MAR/"+meses.ano;
		}else if(meses.mes == '4'){
			meses.mesAnoFormatada = "ABR/"+meses.ano;
		}else if(meses.mes == '5'){
			meses.mesAnoFormatada = "MAI/"+meses.ano;
		}else if(meses.mes == '6'){
			meses.mesAnoFormatada = "JUN/"+meses.ano;
		}else if(meses.mes == '7'){
			meses.mesAnoFormatada = "JUL/"+meses.ano;
		}else if(meses.mes == '8'){
			meses.mesAnoFormatada = "AGO/"+meses.ano;
		}else if(meses.mes == '9'){
			meses.mesAnoFormatada = "SET/"+meses.ano;
		}else if(meses.mes == '10'){
			meses.mesAnoFormatada = "OUT/"+meses.ano;
		}else if(meses.mes == '11'){
			meses.mesAnoFormatada = "NOV/"+meses.ano;
		}else if(meses.mes == '12'){
			meses.mesAnoFormatada = "DEZ/"+meses.ano;
		} 

		return meses.mesAnoFormatada;
	}

	function getIndex(list, prop, element){
		for (var i=0; i< list.length; i++){
			if(list[i][prop] == element[prop]){
				return i;
			}
		}
		return;
	}

	
	$scope.changeEmpresaLoad = function (item){
		if(item == 1){
			if($scope.dataFiltro.date.startDate && $scope.dataFiltro.date.endDate && $scope.listContaContabil.selected && $scope.loadFilter) {
				$scope.loadContasContabil($scope.listContaContabil.selected);
			}
		} else {
			if($scope.dataFiltro.date.startDate && $scope.dataFiltro.date.endDate && $scope.listCentroCusto.selected  && $scope.loadFilter) {
				$scope.loadCentroCusto($scope.listCentroCusto.selected);
			}
		}
		
	}

	$scope.choiceEmpresaAH = function(empresa){
		addIdEmpresas(empresa.idEmpresaFinanceiro);
	}
	
	$scope.choiceEmpresa = function(empresa){
		addIdEmpresas(empresa.idEmpresaFinanceiro);
		addEmpresasModal(empresa);
	}
	$scope.choiceContaContabil = function(contaContabil){
		$scope.escolhaCCAH = contaContabil;
	}

	function addIdEmpresas (empresaId){
		$scope.arrayIdEmpresa.push(empresaId);
	}
	function addEmpresasModal(empresas){
		$scope.arrayEmpresa.push(empresas);
	}

	$scope.removeIdEmpresa = function (idEmpresa){
		$scope.arrayIdEmpresa.delete(idEmpresa.idEmpresaFinanceiro);
		$scope.arrayEmpresa.delete(idEmpresa);
	}

	function validarPesquisa(){
		var isValido = true;
		var msgRequired = " é obrigatória (o).";
		var msgSelect = "Por favor, selecione ";
		if($scope.listCentroCusto.selected == undefined && $scope.nmCC == 2){
			isValido = false;
			messageToastr('erro', msgSelect +'o centro custo','Calma');
		} else if($scope.listContaContabil.selected == undefined  && $scope.nmCC == 1){
			isValido = false;
			messageToastr('erro', msgSelect +'a conta contabil','Calma');
		} else if($scope.loadFilter.selected == undefined){
			isValido = false;
			messageToastr('erro', msgSelect +'a modalidade','Calma');
		} else if ($scope.empresaFinanceiro.selected.length == undefined || $scope.empresaFinanceiro.selected.length == 0){
			isValido = false;
			messageToastr('erro', msgSelect +'a empresa','Calma');
		}
		
		return isValido;
	}

	$scope.prePesquisar = function(idEmpresa, tipo, ccescolhido){
		if(tipo == 1){
			if(ccescolhido == true){
				var isValido = validarPesquisa();
				if(isValido) {
					$scope.loadContasContabil($scope.listContaContabil.selected);
				}
			}else {
				var isValido = validarPesquisa();
				if(isValido) {
					$scope.loadCentroCusto($scope.listCentroCusto.selected);
				}
			}
			
		} else {
			$scope.listContaContabilAnalise =[];
			if($scope.listContaContabilAnalise && $scope.empresaFinanceiroAnalise.selected.length > 0){
				$scope.loadAnaliseHorizontal($scope.empresaFinanceiroAnalise);
			}
		}
	
	}

 	$scope.loadAnaliseHorizontal = function(item){
		$scope.arrayIdEmpresa.toString();
		financeiroFactory.analiseHorizontal.get({idEmpresa: $scope.arrayIdEmpresa, idContaContabil: $scope.escolhaCCAH.id}, function(contaContabilAH){
			$scope.contaContabilListAH =[];
			if(contaContabilAH.length > 0 ){
				$scope.tabelaAH = contaContabilAH.length;
			}

			for(var j=0; j < contaContabilAH.length; j++){
				organizar($scope.contaContabilListAH, contaContabilAH[j], true, null, 1);
			}		
			var saberFilhos = $scope.contaContabilListAH;
				for(var i=0; i < saberFilhos.length; i++){
					if(saberFilhos[i].contasContabil.length > 0){
						saberFilhos[i].temFilhos = true;
					} else{
						saberFilhos[i].temFilhos = false;
					}
				}
				var listaTodos = $scope.contaContabilListAH;
				if(listaTodos != null){
					$scope.lisaAllAH = [];
					for(var i=0; i < listaTodos.length; i++){
						var index = getIndex($scope.lisaAllAH, 'id', listaTodos[i]);
						if(index == undefined){
							var obj ={
								nmContaContabil: listaTodos[i].nmContaContabil,
								nrOrdem: listaTodos[i].nrOrdem,
								id: listaTodos[i].id,
								parent: listaTodos[i].parent,
								temFilhos: listaTodos[i].temFilhos,
								isVisible: listaTodos[i].isVisible,
								listaTodos: [listaTodos[i]]
							}
							$scope.lisaAllAH.push(obj);
						} else{
							$scope.lisaAllAH[index].listaTodos.push(listaTodos[i]);
						}
					}
					for (var i=0; i < $scope.lisaAllAH.length; i++){
						
						var totalContaContabil = getTotalContaContabil( $scope.lisaAllAH[i]);
						$scope.lisaAllAH[i].totalContaContabil = totalContaContabil.toLocaleString("pt-BR", {minimumFractionDigits:2});
					}
					contaContabilAH.listaTodos = $scope.lisaAllAH;
				}
				var analiseH = $scope.lisaAllAH;
				
				for(var i=0; i < analiseH.length; i++){
					for (var j = 1	; j < analiseH[i].listaTodos.length; j++){
						analiseH[i].listaTodos[j].valorCalculadoAH = ((analiseH[i].listaTodos[j].valorContaContabil / analiseH[i].listaTodos[0].valorContaContabilBase)-1)*100;
					}
				}

				var dadosContaContabil = contaContabilAH;
				if(dadosContaContabil != null){
					 $scope.dataDados = [];
					for(var j =0; j < dadosContaContabil.length; j++){
						var index = getIndex($scope.dataDados, 'id', dadosContaContabil[j]);	
						if(index == undefined){
							var objeto = {
								nmContaContabil: dadosContaContabil[j].nmContaContabil,
								id: dadosContaContabil[j].id,
								nrOrdem: dadosContaContabil[j].nrOrdem,
								temFilhos: dadosContaContabil[j].temFilhos,
								dadosContaContabil: [dadosContaContabil[j]]
							}
							$scope.dataDados.push(objeto);
						} else{
							$scope.dataDados[index].dadosContaContabil.push(dadosContaContabil[j]);
						}
					} 
					
					$scope.arrayTotaisAH=[];
					var valorTotal = 0;
					for(var i=0; i <$scope.dataDados.length; i++){
						for(var j=0; j <$scope.dataDados[i].dadosContaContabil.length; j++){
							if($scope.arrayTotaisAH[j] == undefined) {
								$scope.arrayTotaisAH[j] = 0;
							}
							$scope.arrayTotaisAH[j] += $scope.dataDados[i].dadosContaContabil[j].valorContaContabil;
						}
					}
					for(var i=0; i < $scope.arrayTotaisAH.length; i++){
						valorTotal += $scope.arrayTotaisAH[i];
					}
					$scope.valorTotal = valorTotal;
					contaContabilAH.info = $scope.dataDados;
					$scope.contaContabilListAH = contaContabilAH.info;
				}
				
				var dContaContabil = contaContabilAH;
				if(dContaContabil != null){
					$scope.anoDados = [];
					for(var j =0; j < dContaContabil.length; j++){
						var index = getIndex($scope.anoDados, 'anoAH', dContaContabil[j]);	
						if(index == undefined){
							var objeto = {
								anoAH: dContaContabil[j].anoAH,
								dContaContabil: [dContaContabil[j]]
							}
							$scope.anoDados.push(objeto);
						} else{
							$scope.anoDados[index].dContaContabil.push(dContaContabil[j]);
						}
					}
					contaContabilAH.anos = $scope.anoDados;
					$scope.tabAno = contaContabilAH.anos;
					for(var i =0 ; i < $scope.tabAno.length; i++){
						var ano = parseInt($scope.tabAno[i].anoAH);
					}
				}

			$scope.showTabAH = true;
		});
	} 

	//centro custo
	function getFiltrosCentroCusto(){
		$scope.listCentroCusto=[];
		var obj = {id: 0, nmCentroCusto: "Todos"};
		$scope.listCentroCusto.push(obj);
		financeiroFactory.allCentroCusto.get({id: 0}, function(result){
			for(var i=0; i < result.length; i++){
				$scope.listCentroCusto.push(result[i]);
			}
		});		
	}
	
	function getTotalCentroCusto(dados){
		var totalCC = 0;
		var totalForm = 0;
		for (var i=0; i < dados.listaTodos.length; i++){
			totalForm = dados.listaTodos[i].valorCentroCusto;
			if(totalForm == 0){
				totalForm = "0,00";
			}
			dados.listaTodos[i].totalForm = totalForm.toLocaleString("pt-BR", {minimumFractionDigits:2});
			totalCC += dados.listaTodos[i].valorCentroCusto;
		}
		
		if(totalCC == 0){
			totalCC = "0,00";
		}
		return totalCC.toLocaleString("pt-BR", {minimumFractionDigits:2});

	}

	$scope.loadCentroCusto = function(item) {
		$scope.arrayTotais=[];
		//$scope.tabelaPrincipalCentroCusto = 0;
		$scope.arrayIdEmpresa.toString();
		var dtInicio = moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
		var dtFim = moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();
		var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
		if(dataValida){
			financeiroFactory.centroCustoAll.query({ dtInicio: dtInicio, dtFim: dtFim,  idEmpresa:$scope.arrayIdEmpresa, idCentroCusto: item.id, tipoStatus: $scope.loadFilter.selected}, function( centroCustos ) {
				
				$scope.centroCustoList = [];
				if(centroCustos.length > 0){
					$scope.tabelaPrincipalCentroCusto = centroCustos.length;
				}
				for(var j=0; j < centroCustos.length; j++){
					organizar($scope.centroCustoList, centroCustos[j], true, null, 2);
				}
				
				var saberFilhos = $scope.centroCustoList;
				for(var i=0; i < saberFilhos.length; i++){
					if(saberFilhos[i].centroCustos.length > 0){
						saberFilhos[i].temFilhos = true;
					} else{
						saberFilhos[i].temFilhos = false;
					}
				}

				var listaTodos = $scope.centroCustoList;
				if(listaTodos != null){
					$scope.lisaAll = [];
					for(var i=0; i < listaTodos.length; i++){
						var index = getIndex($scope.lisaAll, 'id', listaTodos[i]);
						if(index == undefined){
							var obj ={
								nmCentroCusto: listaTodos[i].nmCentroCusto,
								nrOrdem: listaTodos[i].nrOrdem,
								id: listaTodos[i].id,
								parent: listaTodos[i].parent,
								temFilhos: listaTodos[i].temFilhos,
								isVisible: listaTodos[i].isVisible,
								listaTodos: [listaTodos[i]]
							}
							$scope.lisaAll.push(obj);
						} else{
							$scope.lisaAll[index].listaTodos.push(listaTodos[i]);
						}
					}
					for (var i=0; i < $scope.lisaAll.length; i++){
						var totalCentroCusto = getTotalCentroCusto( $scope.lisaAll[i]);
						$scope.lisaAll[i].totalCentroCusto = totalCentroCusto.toLocaleString("pt-BR", {minimumFractionDigits: 2});

					}
					centroCustos.listaTodos = $scope.lisaAll;
				}
				
				for(var i=0; i < $scope.lisaAll.length; i++){
					if(!$scope.lisaAll[i].temFilhos ){
							$scope.lisaAll[i].showNegrito = false;
						if($scope.lisaAll[i].parent == null){
							$scope.lisaAll[i].showNegrito  = true;
						}					
					} else if($scope.lisaAll[i].temFilhos == true){
						$scope.lisaAll[i].showNegrito = true;
					}
				}
				

				var dadosCentroCusto = centroCustos;
				if(dadosCentroCusto != null){
					$scope.dataDados = [];
					for(var j =0; j < dadosCentroCusto.length; j++){
						var index = getIndex($scope.dataDados, 'nmCentroCusto', dadosCentroCusto[j]);	
						if(index == undefined){
							var objeto = {
								nmCentroCusto: dadosCentroCusto[j].nmCentroCusto,
								idCentroCusto: dadosCentroCusto[j].idCentroCusto,
								dadosCentroCusto: [dadosCentroCusto[j]]
							}
							$scope.dataDados.push(objeto);
						} else{
							$scope.dataDados[index].dadosCentroCusto.push(dadosCentroCusto[j]);
						}
					}

					centroCustos.info = $scope.dataDados;
					$scope.centroCustoList = centroCustos.info;
				}

				
				$scope.valorTotal = 0;

				for(var i=0; i <$scope.dataDados.length; i++){
					for(var j=0; j <$scope.dataDados[i].dadosCentroCusto.length; j++){
						if($scope.arrayTotais[j] == undefined) {
							$scope.arrayTotais[j] = 0;
						}
						$scope.arrayTotais[j] += $scope.dataDados[i].dadosCentroCusto[j].valorCentroCusto;
					}
				}
				for(var i=0; i < $scope.arrayTotais.length; i++){
					$scope.valorTotal += $scope.arrayTotais[i];
				}

				var dCentroCusto = centroCustos;
				if(dCentroCusto != null){
					$scope.mesesDados = [];
					for(var j =0; j < dCentroCusto.length; j++){
						var index = getIndex($scope.mesesDados, 'mesAno', dCentroCusto[j]);	
						if(index == undefined){
							var objeto = {
								mesAno: dCentroCusto[j].mesAno,
								dCentroCusto: [dCentroCusto[j]]
							}
							$scope.mesesDados.push(objeto);
						} else{
							$scope.mesesDados[index].dCentroCusto.push(dCentroCusto[j]);
						}
					}
					for(var k = 0; k < $scope.mesesDados.length; k++){
						var mesAnoFinalForm = getMesesFormatada($scope.mesesDados[k]);
						$scope.mesesDados[k].mesAnoFinalForm = mesAnoFinalForm;
					}
					centroCustos.meses = $scope.mesesDados;
					$scope.tabMeses = centroCustos.meses;
					
				}
				$scope.print = centroCustos;
				$scope.showTab = true;				
			});			
		}
	};

	$scope.changeDateLoadCentroCusto = function() {
	/* 	if($scope.empresaFinanceiro.selected && $scope.listCentroCusto.selected && $scope.loadFilter) {
			$scope.loadCentroCusto($scope.listCentroCusto.selected);
		} */
	};
	

	//conta contabil
	$scope.filtrosCC = function(){
		$scope.listContaContabil=[];
		var obj = {id: 0, nmContaContabil: "Todos"};
		$scope.listContaContabil.push(obj);

		financeiroFactory.allContasContabil.get({id:0},function(result){
			for(var i=0; i < result.length; i++){
				$scope.listContaContabil.push(result[i]);
			}
		});	
		
	}
	
	function getTotalContaContabil(dados){
		var totalCC = 0;
		var totalForm = 0;
		for (var i=0; i < dados.listaTodos.length; i++){
			totalForm = dados.listaTodos[i].valorContaContabil;
			if(totalForm == 0){
				totalForm = "0,00";
			}
			dados.listaTodos[i].totalForm = totalForm.toLocaleString("pt-BR", {minimumFractionDigits:2});

			totalCC += dados.listaTodos[i].valorContaContabil;
		}
		if(totalCC == 0){
			totalCC = "0,00";
		}
		return totalCC.toLocaleString("pt-BR", {minimumFractionDigits:2});

	}
	$scope.changeDateLoadContaContabil = function(){
		/* if($scope.empresaFinanceiro.selected && $scope.listContaContabil.selected && $scope.loadFilter) {
			$scope.loadContasContabil($scope.listContaContabil.selected);
		} */
	}
	
	$scope.loadContasContabil = function(item) {
		//console.log("item", item);
		$scope.arrayIdEmpresa.toString();
		var dtInicio = moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
		var dtFim = moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();
		var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
		if(dataValida) {
			financeiroFactory.contaContabilAll.query({ dtInicio: dtInicio, dtFim: dtFim, idEmpresa:$scope.arrayIdEmpresa, idContaContabil: item.id, tipoStatus: $scope.loadFilter.selected}, function( contaContabil ) {
				//console.log(contaContabil);
				$scope.contaContabilList = [];
				if(contaContabil.length > 0){
					$scope.tabelaPrincipal = contaContabil.length;
				}
				for(var j=0; j < contaContabil.length; j++){
					organizar($scope.contaContabilList, contaContabil[j], true, null, 1);
				}		 

				var saberFilhos = $scope.contaContabilList;
				for(var i=0; i < saberFilhos.length; i++){
					if(saberFilhos[i].contasContabil.length > 0){
						saberFilhos[i].temFilhos = true;
					} else{
						saberFilhos[i].temFilhos = false;
					}
				}
				var listaTodos = $scope.contaContabilList;
				if(listaTodos != null){
					$scope.lisaAll = [];
					for(var i=0; i < listaTodos.length; i++){
						var index = getIndex($scope.lisaAll, 'id', listaTodos[i]);
						if(index == undefined){
							var obj ={
								nmContaContabil: listaTodos[i].nmContaContabil,
								nrOrdem: listaTodos[i].nrOrdem,
								id: listaTodos[i].id,
								parent: listaTodos[i].parent,
								temFilhos: listaTodos[i].temFilhos,
								isVisible: listaTodos[i].isVisible,
								listaTodos: [listaTodos[i]]
							}
							$scope.lisaAll.push(obj);
						} else{
							$scope.lisaAll[index].listaTodos.push(listaTodos[i]);
						}
					}
					for (var i=0; i < $scope.lisaAll.length; i++){						
						var totalContaContabil = getTotalContaContabil( $scope.lisaAll[i]);
						$scope.lisaAll[i].totalContaContabil = totalContaContabil.toLocaleString("pt-BR", {minimumFractionDigits:2});
					}
					contaContabil.listaTodos = $scope.lisaAll;
					
				}
				
				for(var i=0; i < $scope.lisaAll.length; i++){
					if(!$scope.lisaAll[i].temFilhos ){
							$scope.lisaAll[i].showNegrito = false;
						if($scope.lisaAll[i].parent == null){
							$scope.lisaAll[i].showNegrito  = true;
						}					
					} else if($scope.lisaAll[i].temFilhos == true){
						$scope.lisaAll[i].showNegrito = true;
					}
				}

				var dadosContaContabil = contaContabil;
				if(dadosContaContabil != null){
					$scope.dataDados = [];
					for(var j =0; j < dadosContaContabil.length; j++){
						var index = getIndex($scope.dataDados, 'id', dadosContaContabil[j]);	
						if(index == undefined){
							var objeto = {
								nmContaContabil: dadosContaContabil[j].nmContaContabil,
								id: dadosContaContabil[j].id,
								nrOrdem: dadosContaContabil[j].nrOrdem,
								temFilhos: dadosContaContabil[j].temFilhos,
								dadosContaContabil: [dadosContaContabil[j]]
							}
							$scope.dataDados.push(objeto);
						} else{
							$scope.dataDados[index].dadosContaContabil.push(dadosContaContabil[j]);
						}
					}

					$scope.arrayTotais=[];
					var valorTotal = 0;

					for(var i=0; i <$scope.dataDados.length; i++){
						for(var j=0; j <$scope.dataDados[i].dadosContaContabil.length; j++){
							if($scope.arrayTotais[j] == undefined) {
								$scope.arrayTotais[j] = 0;
							}
							$scope.arrayTotais[j] += $scope.dataDados[i].dadosContaContabil[j].valorContaContabil;
						}
					}
					for(var i=0; i < $scope.arrayTotais.length; i++){
						valorTotal += $scope.arrayTotais[i];
					}
					$scope.valorTotal = valorTotal;
					contaContabil.info = $scope.dataDados;
					$scope.contaContabilList = contaContabil.info;
				}

				
				var dContaContabil = contaContabil;
				if(dContaContabil != null){
					$scope.mesesDados = [];
					for(var j =0; j < dContaContabil.length; j++){
						var index = getIndex($scope.mesesDados, 'mesAno', dContaContabil[j]);	
						if(index == undefined){
							var objeto = {
								mesAno: dContaContabil[j].mesAno,
								dContaContabil: [dContaContabil[j]]
							}
							$scope.mesesDados.push(objeto);
						} else{
							$scope.mesesDados[index].dContaContabil.push(dContaContabil[j]);
						}
					}
					for(var k = 0; k < $scope.mesesDados.length; k++){
						var mesAnoFinalForm = getMesesFormatada($scope.mesesDados[k]);
						$scope.mesesDados[k].mesAnoFinalForm = mesAnoFinalForm;
					}
					contaContabil.meses = $scope.mesesDados;
					$scope.tabMeses = contaContabil.meses;
					
				}

				

				
				$scope.printContaContabil = contaContabil;
				$scope.showTab = true;
			});

		
		}
	};

	

	$scope.atualizarSelectCC = function(text) {
		var Conta = financeiroFactory.buscaContaContabil;

        if (text) {
			if(text.length>1){
				var queryObject = {};
				text = text.toLowerCase();
				queryObject = { nmContaContabil: text };
				
				if(text){
					var data = Conta.query(queryObject, function() {
						$scope.listCC = data;
							var obj = {id: 0, nmContaContabil: "TODOS"};
						$scope.listCC.push(obj);
						if (data.length == 0) {
							$scope.selectedCCNotFound = "Sem dados disponiveis.";
	
						} else {
							$scope.selectedCCNotFound = "";
	
						}
					}); 
				}
			}
           
        }

	};
	
	$scope.atualizarSelect = function(text) {
		var Centro = financeiroFactory.buscaCentroCusto;

        if (text) {
			if(text.length>1){
				var queryObject = {};
				text = text.toLowerCase();
				queryObject = { nmCentroCusto: text };
				
				if(text){
					var data = Centro.query(queryObject, function() {
						$scope.listCentro = data;
							var obj = {id: 0, nmCentroCusto: "TODOS"};
						$scope.listCentro.push(obj);
						if (data.length == 0) {
							$scope.selectedCCNotFound = "Sem dados disponiveis.";
	
						} else {
							$scope.selectedCCNotFound = "";
	
						}
					}); 
				}
			}
           
        }

    };

	function inicio() {
		$scope.listaAux = [];
		$scope.empresaFinanceiro = {};
		$scope.cssStyleObject = {};
		$scope.empresaFinanceiroAnalise = { selected: []};

		$scope.arrayIdEmpresa =[];
		$scope.arrayEmpresa =[];

		inicializarColorFiltroStatus();
		inicializarColorFiltroModalidade();
	
		$scope.inStatusFiltro = null;
		$scope.inSituacaoDespesa = null;

		inicializarData();

		definirConfigDateRange();
		getFiltrosCC();
		getFiltrosCentroCusto();

		getEmpresaFinanceiro();	

	};

	inicio();


	$scope.gerarPdf = function(escolha) {
		$scope.escolha = escolha;

		getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
	};

  	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
	  	};

	  	img.onload = function() {
		  	callBack(img);
	  	};

	  	img.src = url;
    };

	var print = function ( imgData ){
	  
		var doc = new jsPDF('landscape','pt','letter','a4');
		
		if($scope.escolha == 1){
			var rows = [];		
			var infoMeses =[];
			var teste =[];
	
			var meses = $scope.printContaContabil.meses;
			infoMeses = $scope.printContaContabil.listaTodos;
			var columnMeses = [];
		
			var columns =["Conta Contábil"/* , dataKey: "contac"} */];
			
			for(var i=0; i < meses.length; i++){
				columns.push(meses[i].mesAnoFinalForm.toString()/* , dataKey: "oi"+i} */);
			}
			columns.push(/* {title:  */"Valor Total"/* , dataKey: "valor"} */);

		 	for(var i=0; i < infoMeses.length; i++){
				rows[i] = [infoMeses[i].nrOrdem +" "+ infoMeses[i].nmContaContabil];
				
				if(!infoMeses[i].temFilhos ){
						rows[i].temFilhos = 0;
					if(infoMeses[i].parent == null){
						rows[i].temFilhos = 1;
					}					
				} else if(infoMeses[i].temFilhos == true){
					rows[i].temFilhos = 1;
				}
			}

		
			for(var i=0; i < infoMeses.length; i++){
				for(var j=0; j < infoMeses[i].listaTodos.length;j++){
					rows[i].push(infoMeses[i].listaTodos[j].totalForm.toString());
				}
			} 

			for(var i=0; i < infoMeses.length; i++){
				rows[i].push(infoMeses[i].totalContaContabil.toString());
			}

			
			var situacaoDespesa;
			if( $scope.loadFilter.selected == 0){
				situacaoDespesa = 'ABERTO'
			}else if($scope.loadFilter.selected == 1){
				situacaoDespesa = 'PAGO'
			}/* else if($scope.loadFilter.selected == 3){
				situacaoDespesa = 'NÃO PAGO'
			} */ else if($scope.loadFilter.selected == 2){
				situacaoDespesa = 'TODOS'
			}


			var dataInicio = moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
			var dataFim = moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();
			
			var nomeEmpresaParcial =[];
			for(var i =0; i < $scope.empresaFinanceiro.selected.length; i++){
				nomeEmpresaParcial.push($scope.empresaFinanceiro.selected[i].nmFantasia);
			}
			var nomeEmpresa = nomeEmpresaParcial.toString();
		
			var valorTotalFinal = $scope.valorTotal.toLocaleString("pt-BR", {minimumFractionDigits:2});
			//CABECALHO	
			// doc.addImage(imgData, 'JPEG', 293, 20, 200/*width*/, 43/*heigth*/, 'logo',);
			doc.setFont("Verdana");
			doc.setFontType('bold')
			doc.setFontSize(14);

			doc.writeText(0,90, 'Análise Financeiro',{align:'center'});

			

			// LINE 1
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 130, 'Empresa:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(77, 130,nomeEmpresa);

			// LINE 2
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 150, 'Período Selecionado:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(135, 150, dataInicio);
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(190, 150, '-'); 
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(195, 150,dataFim);

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 170, 'Situação Despesa: ');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(120, 170,situacaoDespesa);
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(620, 210, 'DESPESA:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(690, 210, 'R$ ' + valorTotalFinal); 
			var posicaoTabela = 220;
		

		//doc.autoTable(columns, rows, {startY: 220, margin: {top: 110}, overflow: 'linebreak'});
			doc.autoTable(columns, rows,{
				startY: 220, 
				margin: {horizontal: 20}, 
				bodyStyles: {valign: 'top', fontSize: 9},
				styles: {overflow: 'linebreak'},
				columnStyles: {contac:{overflow:'linebreak', fontSize: 8}},
				createdCell: function(cell, data) {
					if (data.row.raw.temFilhos == 1 ) {
					  cell.styles.fontStyle = 'bold';
					}
				}
			});

			doc.autoPrint();
			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');

		} else {
			var infoMeses =[];
			var rows = [];		
	
			var meses = $scope.print.meses;
			infoMeses = $scope.print.listaTodos;
			var columnMeses = [];
		
			var columns =["Centro Custo"/* , dataKey: "contac"} */];
			
			for(var i=0; i < meses.length; i++){
				columns.push(meses[i].mesAnoFinalForm.toString()/* , dataKey: "oi"+i} */);
			}
			columns.push(/* {title:  */"Valor Total"/* , dataKey: "valor"} */);

		 	for(var i=0; i < infoMeses.length; i++){
				rows[i] = [infoMeses[i].nrOrdem +" "+ infoMeses[i].nmCentroCusto];
				if(!infoMeses[i].temFilhos ){
						rows[i].temFilhos = 0;
					if(infoMeses[i].parent == null){
						rows[i].temFilhos = 1;
					}					
				} else if(infoMeses[i].temFilhos == true){
					rows[i].temFilhos = 1;
				}
			}

		
			for(var i=0; i < infoMeses.length; i++){
				for(var j=0; j < infoMeses[i].listaTodos.length;j++){
					rows[i].push(infoMeses[i].listaTodos[j].totalForm.toString());
				}
			} 

			for(var i=0; i < infoMeses.length; i++){
				rows[i].push(infoMeses[i].totalCentroCusto.toString());
			}

			
			var situacaoDespesa;
			if( $scope.loadFilter.selected == 0){
				situacaoDespesa = 'ABERTO'
			}else if($scope.loadFilter.selected == 1){
				situacaoDespesa = 'PAGO'
			}/* else if($scope.loadFilter.selected == 3){
				situacaoDespesa = 'NÃO PAGO'
			} */ else if($scope.loadFilter.selected == 2){
				situacaoDespesa = 'TODOS'
			}


			var dataInicio = moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
			var dataFim = moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();
			
			var nomeEmpresaParcial =[];
			for(var i =0; i < $scope.empresaFinanceiro.selected.length; i++){
				nomeEmpresaParcial.push($scope.empresaFinanceiro.selected[i].nmFantasia);
			}
			var nomeEmpresa = nomeEmpresaParcial.toString();
		
			var valorTotalFinal = $scope.valorTotal.toLocaleString("pt-BR", {minimumFractionDigits:2});
			//CABECALHO	
			// doc.addImage(imgData, 'JPEG', 293, 20, 200/*width*/, 43/*heigth*/, 'logo',);
			doc.setFont("Verdana");
			doc.setFontType('bold')
			doc.setFontSize(14);

			doc.writeText(0,90, 'Análise Financeiro',{align:'center'});

			

			// LINE 1
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 130, 'Empresa:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(77, 130,nomeEmpresa);

			// LINE 2
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 150, 'Período Selecionado:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(135, 150, dataInicio);
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(190, 150, '-'); 
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(195, 150,dataFim);

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 170, 'Situação Despesa: ');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(120, 170,situacaoDespesa);
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(620, 210, 'DESPESA:');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(690, 210, 'R$ ' + valorTotalFinal); 
			var posicaoTabela = 220;
		

		//doc.autoTable(columns, rows, {startY: 220, margin: {top: 110}, overflow: 'linebreak'});
			doc.autoTable(columns, rows, {
				startY: 220, 
				margin: {horizontal: 20}, 
				bodyStyles: {valign: 'top', fontSize: 9},
				styles: {overflow: 'linebreak'},
				columnStyles: {contac:{overflow:'linebreak', fontSize: 8}},
				createdCell: function(cell, data) {
					if (data.row.raw.temFilhos == 1 ) {
					  cell.styles.fontStyle = 'bold';
					}
				}
			});

			doc.autoPrint();
			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');


		}
	} 
	$scope.aqui = 'aqui';

	$scope.modalDespesas = function(item){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/analiseFinanceira/modal/modalAnaliseFinanceiro.html',
			controller: 'ModalAnaliseFinanceiroCtrl',
			size: 'lg',
			resolve:{
				item: function(){
					return item;
				},
				empresas: function(){
					return $scope.arrayEmpresa;
				},
				idEmpresa: function(){
					return $scope.arrayIdEmpresa.toString();
				},
				inStatus: function(){
					return $scope.loadFilter.selected;
				},
				nmContaContabil : function (){
					return $scope.listContaContabil.selected;
				},
				nmCentroCusto: function (){
					return $scope.listCentroCusto.selected;
				},
				tipoCC: function (){
					return $scope.inStatusFiltro;
				},
				dataInicio: function (){
					return moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
				},
				dataFim: function(){
					return moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();
				},
				formaPag: function(){
					return $scope.loadFilter;
				}
			}
		});
	} 
	
});


MedicsystemApp.controller('ModalAnaliseFinanceiroCtrl', function($scope, $modalInstance, financeiroFactory, item, NgTableParams, idEmpresa, inStatus, nmContaContabil, nmCentroCusto, tipoCC, dataInicio, dataFim, formaPag, empresas){
	function inicioModalDespesa(){

		$scope.showTabCC = false;
		var dadosTotal = item;
		$scope.despesasResult =[];
		$scope.nmEmpresa =[];
		
		for (var i =0; i < empresas.length; i ++){
			$scope.nmEmpresa.push(empresas[i].nmFantasia); //nomes Empresas
		}

		$scope.tipoCC = tipoCC;
		$scope.filtroPag = formaPag.selected;
		$scope.pagamentoTotal = item.totalForm;
	

		if(tipoCC == 1){
			$scope.nomeDespesa = item.nmContaContabil;
			$scope.nmCC = nmContaContabil.nmContaContabil;
		} else {
			$scope.nomeDespesa = item.nmCentroCusto;
			$scope.nmCC = nmCentroCusto.nmCentroCusto;
		}

		if(dadosTotal.valorContaContabil > 0 && dadosTotal.valorContaContabil != undefined){
			$scope.showTabCC = true;
		} else if( dadosTotal.valorCentroCusto > 0 && dadosTotal.valorCentroCusto != undefined){
			$scope.showTabCC = true;
		}
		if(formaPag.selected == 1){
			$scope.formaPag = 'Pago';
			$scope.dataFormaPag = 1;
			$scope.nmColPag = 'Valor Pago';
		} else if( formaPag.selected == 0){
			$scope.formaPag ='Aberto';
			$scope.dataFormaPag =0;
			$scope.nmColPag = 'Valor Despesa';
		} /* else if(formaPag.selected == 3){
			$scope.formaPag = 'Não Pago';
			$scope.dataFormaPag = 2;
			$scope.nmColPag = 'Valor Despesa';
		} */ else if(formaPag.selected == 2){
			$scope.formaPag = 'Todos';
			$scope.dataFormaPag = 3;
			$scope.nmColPag = 'Valor Despesa';
		}
		
		$scope.getValorTotal = function(dados, idCC){
			if(!dados) return 0;
		 	var total =0;
			for(var i = 0; i <dados.length; i++){
				if(idCC == dados[i].idContaContabil){
					total += dados[i].valorAP;
				}
			}
			return total.toLocaleString("pt-BR", {minimumFractionDigits: 2});
		}

		if( item.mes == '1'){
			$scope.mesAnoFormatada = "JANEIRO/"+item.ano;
		} else if(item.mes == '2'){
			$scope.mesAnoFormatada = "FEVEREIRO/"+item.ano;
		} else if(item.mes == '3'){
			$scope.mesAnoFormatada = "MARÇO/"+item.ano;
		}else if(item.mes == '4'){
			$scope.mesAnoFormatada = "ABRIL/"+item.ano;
		}else if(item.mes == '5'){
			$scope.mesAnoFormatada = "MAIO/"+item.ano;
		}else if(item.mes == '6'){
			$scope.mesAnoFormatada = "JUNHO/"+item.ano;
		}else if(item.mes == '7'){
			$scope.mesAnoFormatada = "JULHO/"+item.ano;
		}else if(item.mes == '8'){
			$scope.mesAnoFormatada = "AGOSTO/"+item.ano;
		}else if(item.mes == '9'){
			$scope.mesAnoFormatada = "SETEMBRO/"+item.ano;
		}else if(item.mes == '10'){
			$scope.mesAnoFormatada = "OUTUBRO/"+item.ano;
		}else if(item.mes == '11'){
			$scope.mesAnoFormatada = "NOVEMBRO/"+item.ano;
		}else if(item.mes == '12'){
			$scope.mesAnoFormatada = "DEZEMBRO/"+item.ano;
		} 

		if(tipoCC == 1){
			$scope.dadosDespesa = [];
			financeiroFactory.despesasPorContaContabil.get({idEmpresa: idEmpresa, dtAno: dadosTotal.ano, dtMes: dadosTotal.mes, inStatus: inStatus, idContaContabil: dadosTotal.id }, function(result){
				var despesasResult = result;
				for(var i =0; i< despesasResult.length; i++){
					
					var dtFormatadaPagamento = moment(despesasResult[i].dtPagamento).format("DD/MM/YYYY");
					var dtFormatadaVenc = moment(despesasResult[i].dtVencimento).format("DD/MM/YYYY");
					despesasResult[i].dtFormatadaPagamento = dtFormatadaPagamento;
					despesasResult[i].dtFormatadaVenc = dtFormatadaVenc;
					var index = getIndex($scope.dadosDespesa, 'idEmpresaFinanceiroStart', despesasResult[i]);
					if(index == undefined){
						var obj={
							idEmpresaFinanceiroStart: despesasResult[i].idEmpresaFinanceiroStart,
							idContaContabil: despesasResult[i].idContaContabil,
							nmFantasia: despesasResult[i].nmFantasia,
							despesasResult: [despesasResult[i]]
						}
						$scope.dadosDespesa.push(obj);
					}else{
						$scope.dadosDespesa[index].despesasResult.push(despesasResult[i]);
					}
				}

				for(var i=0; i < $scope.dadosDespesa.length; i++){
					for(var j=0; j < $scope.dadosDespesa[i].despesasResult.length; j++){
						$scope.dadosDespesa[i].despesasResult[j].valorAPForm = $scope.dadosDespesa[i].despesasResult[j].valorAP.toLocaleString("pt-BR", {minimumFractionDigits:2});
					}
				}	

				for(var j=0; j < $scope.dadosDespesa.length; j++){
					var totalEmpresa = 0;
					for(var k =0; k < $scope.dadosDespesa[j].despesasResult.length;k++ ){
						totalEmpresa += $scope.dadosDespesa[j].despesasResult[k].valorAP;
					}
					$scope.dadosDespesa[j].totalEmpresaFinal = totalEmpresa.toLocaleString("pt-BR", {minimumFractionDigits:2});
				}
				$scope.obsTableParams = configurarTabela($scope.dadosDespesa);
			});

			console.log("contacontabil", $scope.dadosDespesa);
			
		} else if(tipoCC == 2){
			$scope.dadosDespesa = [];
			financeiroFactory.despesasPorCentroCusto.get({idEmpresa: idEmpresa, dtAno: dadosTotal.ano, dtMes: dadosTotal.mes, inStatus: inStatus, idCentroCusto: dadosTotal.id }, function(result){
				var despesasResult = result;
				for(var i =0; i< despesasResult.length; i++){
					
					var dtFormatadaPagamento = moment(despesasResult[i].dtPagamento).format("DD/MM/YYYY");
					var dtFormatadaVenc = moment(despesasResult[i].dtVencimento).format("DD/MM/YYYY");

					despesasResult[i].dtFormatadaPagamento = dtFormatadaPagamento;
					despesasResult[i].dtFormatadaVenc = dtFormatadaVenc;
					var index = getIndex($scope.dadosDespesa, 'idEmpresaFinanceiroStart', despesasResult[i]);
					if(index == undefined){
						var obj={
							idEmpresaFinanceiroStart: despesasResult[i].idEmpresaFinanceiroStart,
							idCentroCusto: despesasResult[i].idCentroCusto,
							nmFantasia: despesasResult[i].nmFantasia,
							despesasResult: [despesasResult[i]]
						}
						$scope.dadosDespesa.push(obj);
					}else{
						$scope.dadosDespesa[index].despesasResult.push(despesasResult[i]);
					}
				}
				for(var i=0; i < $scope.dadosDespesa.length; i++){
					for(var j=0; j < $scope.dadosDespesa[i].despesasResult.length; j++){
						$scope.dadosDespesa[i].despesasResult[j].valorAPForm = $scope.dadosDespesa[i].despesasResult[j].valorAP.toLocaleString("pt-BR", {minimumFractionDigits:2});
					}
				}	

				
				for(var j=0; j < $scope.dadosDespesa.length; j++){
					var totalEmpresa = 0;
					for(var k =0; k < $scope.dadosDespesa[j].despesasResult.length;k++ ){
						totalEmpresa += $scope.dadosDespesa[j].despesasResult[k].valorAP;
					}
					$scope.dadosDespesa[j].totalEmpresaFinal = totalEmpresa.toLocaleString("pt-BR", {minimumFractionDigits:2});
				}

				$scope.obsTableParams = configurarTabela($scope.dadosDespesa);
			});
		}
		
	}

	function getIndex(list, prop, element){
		for (var i=0; i< list.length; i++){
			if(list[i][prop] == element[prop]){
				return i;
			}
		}
		return;
	}

	



	function configurarTabela(lista){
		
		var tableParams = new NgTableParams();
			if(lista.length > 0){
				var initialParams = { count: 15};
				var initialSettings = {
					counts:[], 
					paginationMaxBlocks: 13,
					paginationMinBlocks: 2,
					dataset: lista
				};
				tableParams = new NgTableParams(initialParams, initialSettings); 
			}

			
		return tableParams;
	}; 
	

	$scope.close = function(){
		$modalInstance.close();
	};


	inicioModalDespesa();
});

