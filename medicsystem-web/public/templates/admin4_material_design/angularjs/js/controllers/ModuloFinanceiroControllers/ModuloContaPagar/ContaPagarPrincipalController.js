'use strict';

MedicsystemApp.controller('ContaPagarPrincipalController', function($rootScope,$scope,$state,
	$stateParams,financeiroFactory,Auth, NgTableParams,toastr) {

	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:10 };
			var initialSettings = {
				counts:[], 
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
		}
  	};

  	function messageToastr(type, message, time) {
		if( type == 'erro' ) {
	    	toastr.error(  message, 'ERRO'); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, 'SUCESSO'); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, 'SUCESSO'); 
	    }
	};

	function definirEstiloPorEmpresa( empresaFinanceiro ) {

		var MEDIC_LAB_ID = 12;
		var DENTAL_ID = 13;
		var ODONTOMED_ID = 14;
		var SAUDE_PERFORMANCE_ID = 15;
		var MEDLAB_ID = 16
		var DR_CONSULTA_ID = 17;

		if ( empresaFinanceiro.idEmpresaFinanceiro == MEDIC_LAB_ID )
			$scope.cssStyleObject.color = "blue-steel";
		else if ( empresaFinanceiro.idEmpresaFinanceiro == DENTAL_ID )
			$scope.cssStyleObject.color = "red-thunderbird";
		else if ( empresaFinanceiro.idEmpresaFinanceiro == ODONTOMED_ID  ) 
			$scope.cssStyleObject.color = "green-seagreen";
		else if ( empresaFinanceiro.idEmpresaFinanceiro == SAUDE_PERFORMANCE_ID  )
			$scope.cssStyleObject.color = "green-turquoise";
		else if ( empresaFinanceiro.idEmpresaFinanceiro == MEDLAB_ID  )
			$scope.cssStyleObject.color = "purple-intense";
		else if ( empresaFinanceiro.idEmpresaFinanceiro == DR_CONSULTA_ID  )
			$scope.cssStyleObject.color = "blue";
		else
			$scope.cssStyleObject.color = "grey-gallery";
	};

	function inicializarColorFiltroStatus() {
		$scope.cssStyleObject.color1 = 'default';
		$scope.cssStyleObject.color2 = 'default';
		$scope.cssStyleObject.color3 = 'default';
		$scope.cssStyleObject.color7 = 'default';
	};

	function inicializarColorFiltroModalidade(){
		$scope.cssStyleObject.color4 = 'default';
		$scope.cssStyleObject.color5 = 'default';
		$scope.cssStyleObject.color6 = 'default';
	}

	function loadFiltros() {
  		$scope.tipoFiltroStatusDespesa = [
  			{id:0, label:'Em aberto'},{id:1, label:'Pago'},{id:2, label:'Atrasado'}
  		];
  	};

  	function defineConfigMultiSelect() {
      $scope.localLang = {
        selectAll       : "Todos",
        selectNone      : "Desmarcar todos",
        reset           : "...",
        search          : "Pesquise...",
        nothingSelected : "Exibir"
       } 
    };

    function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };


    function inicializarData() {
   		var dataAtual = moment();
     
        $scope.dataFiltro = {};

        if( $stateParams.objectSearch == null )
        	$scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};  
        else
        	$scope.dataFiltro.date = {startDate: moment( $stateParams.objectSearch.intervalo.dtInicio, 'DD/MM/YYYY' ), endDate: moment( $stateParams.objectSearch.intervalo.dtFim, 'DD/MM/YYYY' )};  
    }

    function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	'Hoje': [moment(), moment()],
	   		'Próximos 30 Dias': [ moment(), moment().add(29, 'days')],
	   		'Próximos 15 Dias': [moment(), moment().add(14, 'days')],
	   		'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
	   		'Últimos 15 Dias':[moment().subtract(14, 'days'), moment()],
	   		'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()]

		};
    };

  	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
	  			if( $stateParams.objectSearch == null) {
					if( $stateParams.filtroDashboard ) {
						$scope.empresaFinanceiro.selected = $stateParams.filtroDashboard.empresaFinanceiro;
						$scope.loadContasDespesa( $stateParams.filtroDashboard.nrOpcaoConsulta,true);	  					
	  				} else if ( $stateParams.efReferencia ) {
	  					$scope.empresaFinanceiro.selected = $stateParams.efReferencia;
	  					$scope.loadContasDespesa( null, true );
	  				} else {
	  					for (var i = 0; i < empresaFinanceiroList.length; i++) {
		  					if( empresaFinanceiroList[i].idEmpresaFinanceiro == 12 ) {
		  						$scope.empresaFinanceiro.selected = empresaFinanceiroList[i];
		  						$scope.loadContasDespesa( null,true );
		  					}
		  				};	
	  				}	
  				} else {
  					$scope.empresaFinanceiro.selected = $stateParams.objectSearch.empresaFinanceiro;
	  				$scope.loadContasDespesa( null, true );	
  				}
  				

	  			definirEstiloPorEmpresa( $scope.empresaFinanceiro.selected );
  			}
  		} )
  	};

    function inicializarTotais() {
    	$scope.totalRegistro = 0;
    	$scope.totalPagas = 0;
    	$scope.totalVencidadas = 0;
    	$scope.totalAbertas = 0;
    };

    function definirTotais( lista ) {
    	
    	inicializarTotais();

    	if( lista.length > 0 ) {
    		$scope.totalRegistro = lista.length;

    		lista.forEach( function( currentValue,index,arr) {
    			if( currentValue.inSituacao == 0 ) 
    				$scope.totalAbertas++;
    			else if( currentValue.inSituacao == 1 )	
    				$scope.totalPagas++;
    			else if( currentValue.inSituacao == 2)
    				$scope.totalVencidadas++;
    		}, 'teste a ser usado como this');
    	}
    };

    function getStatusSelecionado( firstLoad ) {
		if( firstLoad == false ) {
			$scope.inStatusFiltro = null;
		} else {
			if( !$scope.inStatusFiltro )	
				$scope.inStatusFiltro = null;
		}

		return $scope.inStatusFiltro;
	};

	$scope.$watch('inStatusFiltro', function() {
		if( $scope.inStatusFiltro != null) {
			$scope.definirCorFiltroStatus( $scope.inStatusFiltro );
			$scope.loadContasDespesa();
		}	
		else {
			if( $scope.empresaFinanceiro.selected ) {
				inicializarColorFiltroStatus()
				$scope.loadContasDespesa();
			}
		}
	});

	$scope.$watch('inStatusModalidade', function(){
		//console.log("1", $scope.inStatusModalidade);
		if($scope.inStatusModalidade != null ){
			$scope.definirCorFiltroStatus($scope.inStatusModalidade);
			$scope.loadContasDespesa();
		}else{	
			//console.log("entrou2", $scope.inStatusModalidade);
			if($scope.empresaFinanceiro.selected){
				inicializarColorFiltroModalidade();
				$scope.loadContasDespesa();
			}		
		}
	});

	$scope.definirCorFiltroStatus = function( escolhido ) {
		if( escolhido == 2 )  {
			$scope.cssStyleObject.color1 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color3 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = 'default';
		} else if( escolhido == 0) {
			$scope.cssStyleObject.color2 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color3 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = 'default';
		} else if( escolhido == 1) {
			$scope.cssStyleObject.color3 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = 'default';
		} else if(escolhido == 7){
			$scope.cssStyleObject.color3 = 'default';
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = $scope.cssStyleObject.color;
		}else if(escolhido == 4){	
			$scope.cssStyleObject.color3 =  $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color4 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = 'default';
		}else if(escolhido == 5){
			$scope.cssStyleObject.color3 =  $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color6 = 'default';
			$scope.cssStyleObject.color7 = 'default';
		}else if(escolhido == 6){
			$scope.cssStyleObject.color3 =  $scope.cssStyleObject.color;
			$scope.cssStyleObject.color2 = 'default';
			$scope.cssStyleObject.color1 = 'default';
			$scope.cssStyleObject.color4 = 'default';
			$scope.cssStyleObject.color5 = 'default';
			$scope.cssStyleObject.color6 = $scope.cssStyleObject.color;
			$scope.cssStyleObject.color7 = 'default';

		}	
	};

	$scope.changeDateLoadContaDespesa = function() {
		$scope.loadContasDespesa(null);
	};

	function getModalidade(item){
		if(item == 4){
			return 2; //dinheiro
		} else if(item == 5){
			return 0 //cheque
		}else if(item == 6){
			return 1 //debito
		}
		
	}

	$scope.loadContasDespesa = function( nrOpcaoConsulta, firstLoad ) {
		$scope.nrOpcaoCons = nrOpcaoConsulta;
		$scope.primeiroLoad = firstLoad;

		$scope.contaDespesaList = [];
		definirEstiloPorEmpresa( $scope.empresaFinanceiro.selected );

		var statusSelected = getStatusSelecionado( firstLoad );
		var intervalo = getIntervaloDataSelecionada();
		var statusModalidade = getModalidade($scope.inStatusModalidade);

		if( $scope.empresaFinanceiro.selected ) {
			financeiroFactory.despesasFin
			.query( {nrIdEmpresa: $scope.empresaFinanceiro.selected.idEmpresaFinanceiro, 
				nrOpcaoConsulta: nrOpcaoConsulta, inSituacao:statusSelected, dtInicio:intervalo.dtInicio,dtFim:intervalo.dtFim , inModalidade: statusModalidade}, 
				function( dfList ) {
					$scope.showButtonConc = true;
					$scope.contaDespesaList = dfList;
					definirTotais( $scope.contaDespesaList );
					configurarTable( $scope.contaDespesaList );
					loadContaContabil(dfList);
			});
		}
	};
	
	$scope.goToCadastrarConta = function( despesaFinanceiro ) {

		var intervalo = getIntervaloDataSelecionada();
		var objectSearch = { empresaFinanceiro: $scope.empresaFinanceiro.selected, intervalo: intervalo };

		if( despesaFinanceiro == null )
			$state.go('contaPagar-cadastro',{nmEmpresaFinanceiro:$scope.empresaFinanceiro.selected.nmFantasia,efCommand:$scope.empresaFinanceiro,despesaFinanceiroSelected:null,objectSearch:objectSearch});
		else
			$state.go('contaPagar-cadastro',{nmEmpresaFinanceiro:$scope.empresaFinanceiro.selected.nmFantasia,efCommand:$scope.empresaFinanceiro,despesaFinanceiroSelected:despesaFinanceiro,objectSearch:objectSearch});
	};

	$scope.excluirDespesaFinanceiro = function( df ) {
		financeiroFactory.despesaFinanceiro.remove( {id:df.id} ,function( result ) {
			messageToastr("sucesso","Despesa excluída com sucesso.","SUCESSO");
      		$scope.loadContasDespesa();					
		}, function( erros){
			messageToastr("erro"," Não foi possível excluir a despesa","SUCESSO");
		} )			
	}


	$scope.desconciliarDadosBancarios = function(item){
		financeiroFactory.desconciliar.edit({idTransBancaria:item.idTransacaoBancaria}, item, function(response){
			if(response.length >0){
				if(response[0] === "S"){
					messageToastr('sucesso', 'Desconciliacao efetuada', 'Sucesso');
					$scope.showButtonConc = false;
				}else{
					messageToastr('erro', 'contate o ti', 'Atenção');
					$scope.showButtonConc = true;
				}
			}

			
		});
	}
	$scope.gerarPdf = function() {
  	getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
  };

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
	};

  var print = function ( imgData ){

		var doc = new jsPDF('landscape','pt','letter','a4');
		var rows = [];

		if($scope.inStatusFiltro == 1 && $scope.inStatusModalidade != undefined){
			var columns = [            
				{title: "Vencimento", dataKey: "venc"}, 
				{title: "Documento", dataKey: "doc"}, 
				{title: "Fornecedor", dataKey: "fornec"},
				{title: "Parcela", dataKey: "parcela"},
				{title: "Conta Contábil", dataKey: "contac"},
				{title: "Centro Custo", dataKey:"centroc"}, 
				{title: "Descrição", dataKey: "desc"},
				{title: "Valor", dataKey: "valor"}
			];
		} else if($scope.inStatusFiltro == null ){
				var columns = [            
					{title: "Vencimento", dataKey: "venc"}, 
					{title: "Documento", dataKey: "doc"}, 
					{title: "Fornecedor", dataKey: "fornec"},
					{title: "Parcela", dataKey: "parcela"},
					{title: "Conta Contábil", dataKey: "contac"},
					{title: "Centro Custo", dataKey:"centroc"},  
					{title: "Descrição", dataKey: "desc"},
					{title: "Situação", dataKey: "situacao"},
					{title: "Valor", dataKey: "valor"}
				];
		} else if($scope.inStatusFiltro == 1 && $scope.inStatusModalidade == undefined){
				var columns = [            
					{title: "Vencimento", dataKey: "venc"}, 
					{title: "Documento", dataKey: "doc"}, 
					{title: "Fornecedor", dataKey: "fornec"},
					{title: "Parcela", dataKey: "parcela"},
					{title: "Conta Contábil", dataKey: "contac"},
					{title: "Centro Custo", dataKey:"centroc"},  
					{title: "Descrição", dataKey: "desc"},
					{title: "Pagamento", dataKey: "formaPag"},
					{title: "Valor", dataKey: "valor"}
				];
		}else {
				var columns = [            
					{title: "Vencimento", dataKey: "venc"}, 
					{title: "Documento", dataKey: "doc"}, 
					{title: "Fornecedor", dataKey: "fornec"},
					{title: "Parcela", dataKey: "parcela"},
					{title: "Conta Contábil", dataKey: "contac"},
					{title: "Centro Custo", dataKey:"centroc"},  
					{title: "Descrição", dataKey: "desc"},
					{title: "Valor", dataKey: "valor"}
				];
		}
		
		if ($scope.listaAux.length == 0){
			var valorTotal = 0;
			for (var i = 0; i < $scope.contaDespesaList.length; i++){
				if($scope.inStatusFiltro == null){
					rows[i] = {                
						"venc": $scope.contaDespesaList[i].dtVencimentoFormatada, 
						"doc": $scope.contaDespesaList[i].nrDocumento == null ? '-': $scope.contaDespesaList[i].nrDocumento,
						"fornec": $scope.contaDespesaList[i].fornecedor == null ? '-': $scope.contaDespesaList[i].fornecedor.nmNomeFantasia,
						"parcela": $scope.contaDespesaList[i].nrOrdemParcela + '/' + $scope.contaDespesaList[i].nrQuantidadeParcelas,
						"contac": $scope.contaDespesaList[i].contaContabil.nmContaContabil == null ? '-': $scope.contaDespesaList[i].contaContabil.nmContaContabil,
						"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
						"desc": $scope.contaDespesaList[i].nmDespesa  == null ? '-': $scope.contaDespesaList[i].nmDespesa,
						"valor": $scope.contaDespesaList[i].vlDespesaFormatado == null ? '-': $scope.contaDespesaList[i].vlDespesaFormatado,
						//"servico":$scope.contaDespesaList[i].nrServico == null ? '-': $scope.contaDespesaList[i].nrServico,
						"situacao": $scope.contaDespesaList[i].inSituacaoFormatado
						//"formaPag": $scope.contaDespesaList[i].inFormaPagamentoFormatado
					};
				} else if($scope.inStatusFiltro == 1 && $scope.inStatusModalidade == undefined ){
					rows[i] = {                
						"venc": $scope.contaDespesaList[i].dtVencimentoFormatada, 
						"doc": $scope.contaDespesaList[i].nrDocumento == null ? '-': $scope.contaDespesaList[i].nrDocumento,
						"fornec": $scope.contaDespesaList[i].fornecedor == null ? '-': $scope.contaDespesaList[i].fornecedor.nmNomeFantasia,
						"parcela": $scope.contaDespesaList[i].nrOrdemParcela + '/' + $scope.contaDespesaList[i].nrQuantidadeParcelas,
						"contac": $scope.contaDespesaList[i].contaContabil.nmContaContabil == null ? '-': $scope.contaDespesaList[i].contaContabil.nmContaContabil,
						"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
						"desc": $scope.contaDespesaList[i].nmDespesa  == null ? '-': $scope.contaDespesaList[i].nmDespesa,
						"formaPag": $scope.contaDespesaList[i].inFormaPagamentoFormatado == null ? '-' : $scope.contaDespesaList[i].inFormaPagamentoFormatado,
						"valor": $scope.contaDespesaList[i].vlDespesaFormatado == null ? '-': $scope.contaDespesaList[i].vlDespesaFormatado,
						//"servico":$scope.contaDespesaList[i].nrServico == null ? '-': $scope.contaDespesaList[i].nrServico,
						//"situacao": $scope.contaDespesaList[i].inSituacaoFormatado
						
					};
				} 
				else {
					rows[i] = {                
						"venc": $scope.contaDespesaList[i].dtVencimentoFormatada, 
						"doc": $scope.contaDespesaList[i].nrDocumento == null ? '-': $scope.contaDespesaList[i].nrDocumento,
						"fornec": $scope.contaDespesaList[i].fornecedor == null ? '-': $scope.contaDespesaList[i].fornecedor.nmNomeFantasia,
						"parcela": $scope.contaDespesaList[i].nrOrdemParcela + '/' + $scope.contaDespesaList[i].nrQuantidadeParcelas,
						"contac": $scope.contaDespesaList[i].contaContabil.nmContaContabil == null ? '-': $scope.contaDespesaList[i].contaContabil.nmContaContabil,
						"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
						"desc": $scope.contaDespesaList[i].nmDespesa  == null ? '-': $scope.contaDespesaList[i].nmDespesa,
						"valor": $scope.contaDespesaList[i].vlDespesaFormatado == null ? '-': $scope.contaDespesaList[i].vlDespesaFormatado,
						//"servico":$scope.contaDespesaList[i].nrServico == null ? '-': $scope.contaDespesaList[i].nrServico,
						//"situacao": $scope.contaDespesaList[i].inSituacaoFormatado
						//"formaPag": $scope.contaDespesaList[i].inFormaPagamentoFormatado
					};
				}
				
	
				valorTotal += $scope.contaDespesaList[i].vlDespesa;
			}
		}else{
			var valorTotal = 0;
			for (var i = 0; i < $scope.listaAux.length; i++){
				if($scope.inStatusFiltro == null ){
					rows[i] = {                
						"venc": $scope.listaAux[i].dtVencimentoFormatada, 
						"doc": $scope.listaAux[i].nrDocumento == null ? '-': $scope.listaAux[i].nrDocumento,
						"fornec": $scope.listaAux[i].fornecedor == null ? '-': $scope.listaAux[i].fornecedor.nmNomeFantasia,
						"parcela": $scope.listaAux[i].nrOrdemParcela + '/' + $scope.listaAux[i].nrQuantidadeParcelas,
						"contac": $scope.listaAux[i].contaContabil.nmContaContabil == null ? '-': $scope.listaAux[i].contaContabil.nmContaContabil, 
						"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
						"desc": $scope.listaAux[i].nmDespesa  == null ? '-': $scope.listaAux[i].nmDespesa,
						"valor": $scope.listaAux[i].vlDespesaFormatado == null ? '-': $scope.listaAux[i].vlDespesaFormatado,
						//"valorpago": $scope.listaAux[i].vlPagoFormatado == null ? '-': $scope.listaAux[i].vlPagoFormatado,
						//"servico":$scope.listaAux[i].nrServico == null ? '-': $scope.listaAux[i].nrServico,
						"situacao": $scope.listaAux[i].inSituacaoFormatado
						
					};
				} else if(($scope.inStatusFiltro == 1 || $scope.inStatusFiltro == 7)  && $scope.inStatusModalidade == undefined ){
					rows[i] = {                
						"venc": $scope.listaAux[i].dtVencimentoFormatada, 
						"doc": $scope.listaAux[i].nrDocumento == null ? '-': $scope.listaAux[i].nrDocumento,
						"fornec": $scope.listaAux[i].fornecedor == null ? '-': $scope.listaAux[i].fornecedor.nmNomeFantasia,
						"parcela": $scope.listaAux[i].nrOrdemParcela + '/' + $scope.listaAux[i].nrQuantidadeParcelas,
						"contac": $scope.listaAux[i].contaContabil.nmContaContabil == null ? '-': $scope.listaAux[i].contaContabil.nmContaContabil, 
						"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
						"desc": $scope.listaAux[i].nmDespesa  == null ? '-': $scope.listaAux[i].nmDespesa,
						"formaPag" : $scope.listaAux[i].inFormaPagamentoFormatado == null ? '-' : $scope.listaAux[i].inFormaPagamentoFormatado,
						"valor": $scope.listaAux[i].vlDespesaFormatado == null ? '-': $scope.listaAux[i].vlDespesaFormatado,
						//"valorpago": $scope.listaAux[i].vlPagoFormatado == null ? '-': $scope.listaAux[i].vlPagoFormatado,
						//"servico":$scope.listaAux[i].nrServico == null ? '-': $scope.listaAux[i].nrServico,
						"situacao": $scope.listaAux[i].inSituacaoFormatado
						
					};
				}
				else {
					rows[i] = {                
					"venc": $scope.listaAux[i].dtVencimentoFormatada, 
					"doc": $scope.listaAux[i].nrDocumento == null ? '-': $scope.listaAux[i].nrDocumento,
					"fornec": $scope.listaAux[i].fornecedor == null ? '-': $scope.listaAux[i].fornecedor.nmNomeFantasia,
					"parcela": $scope.listaAux[i].nrOrdemParcela + '/' + $scope.listaAux[i].nrQuantidadeParcelas,
					"contac": $scope.listaAux[i].contaContabil.nmContaContabil == null ? '-': $scope.listaAux[i].contaContabil.nmContaContabil, 
					"centroc": $scope.contaDespesaList[i].centroCusto.nmCentroCusto == null ? '-': $scope.contaDespesaList[i].centroCusto.nmCentroCusto, 
					"desc": $scope.listaAux[i].nmDespesa  == null ? '-': $scope.listaAux[i].nmDespesa,
					"valor": $scope.listaAux[i].vlDespesaFormatado == null ? '-': $scope.listaAux[i].vlDespesaFormatado,
					//"valorpago": $scope.listaAux[i].vlPagoFormatado == null ? '-': $scope.listaAux[i].vlPagoFormatado,
					//"servico":$scope.listaAux[i].nrServico == null ? '-': $scope.listaAux[i].nrServico,
					//"situacao": $scope.listaAux[i].inSituacaoFormatado
					
					};
				}
				
	
				valorTotal += $scope.listaAux[i].vlDespesa;
			}
		}
	
		var valorTotalFormatada = 'R$ ' + valorTotal.toLocaleString("pt-BR",  {minimumFractionDigits:2, maximumFractionDigits: 2});
		var dataAtual = moment().format('DD/MM/YYYY HH:mm:ss');
		var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
		var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
		var nomeEmpresa = $scope.empresaFinanceiro.selected.nmFantasia;
		var situacaoDespesa;
		var statusModalidade;
	
		if($scope.inStatusFiltro == 0){
			situacaoDespesa = 'EM ABERTO'
		} else if( $scope.inStatusFiltro == 1){
			situacaoDespesa = 'PAGO'
		} else if( $scope.inStatusFiltro == 2){
			situacaoDespesa = 'ATRASADO'
		} else if( $scope.inStatusFiltro == null ){
			situacaoDespesa = 'TODOS'
		} else if($scope.inStatusFiltro == 7){
			situacaoDespesa = 'CONCILIADO'
		}
	
		if($scope.inStatusModalidade == 4){
			statusModalidade = 'DINHEIRO'; //dinheiro 2
		} else if($scope.inStatusModalidade == 5){
			statusModalidade = 'CHEQUE' //cheque 0
		}else if($scope.inStatusModalidade == 6){
			statusModalidade = 'DÉBITO' //debito 1
		}else if ($scope.inStatusModalidade == null){
			statusModalidade = 'DINHEIRO/CHEQUE/DÉBITO'
		}
	
		//CABECALHO	
    doc.setFont("Verdana");
		doc.setFontType('bold')
		doc.setFontSize(14);
	
		doc.writeText(0,90, 'Contas A Pagar',{align:'center'});
	
		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 130, 'Empresa:');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(77, 130, nomeEmpresa);
	
		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 150, 'Período Selecionado:');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(135, 150, dtInicio);
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(190, 150, '-'); 
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(195, 150, dtFim);
		
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 170, 'Situação Despesa: ');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(120, 170, situacaoDespesa);		
	
		if($scope.inStatusFiltro == 1){
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 190, 'Forma Pagamento: ');
	
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(120, 190, statusModalidade);
	
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 210, 'Data Emissão: ');
	
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(100, 210, dataAtual); 
	
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(620, 230, 'DESPESA:');
	
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(690, 230, valorTotalFormatada);
			
			var posicaoTabela = 240;
		} else {
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(20, 190, 'Data Emissão: ');
	
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(100, 190, dataAtual); 
	
			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(620, 210, 'DESPESA:');
	
			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(12);
			doc.text(690, 210, valorTotalFormatada); 
			var posicaoTabela = 220;
		}
		
	
		//        doc.autoTable(columns, rows, {startY: 220, margin: {top: 110}, overflow: 'linebreak'});
		doc.autoTable(columns, rows, {
			startY: posicaoTabela, 
			margin: {horizontal: 20}, 
			bodyStyles: {valign: 'top', fontSize: 9},
			styles: {overflow: 'linebreak'},
			columnStyles: {fornec: {overflow: 'linebreak', fontSize: 8},
							desc:{overflow: 'linebreak', fontSize: 8},
							contac:{overflow:'linebreak', fontSize: 8},
							venc:{ fontSize:8},
							doc:{fontSize:8},
							parcela:{fontSize:8},
							valor: { halign:'right'}
							}
		});

		doc.autoPrint();
		var blob = doc.output('blob');
		var fileURL = URL.createObjectURL(blob);
		window.open(fileURL, '_blank');
		
  };

	function loadContaContabil (lista){
		//console.log("lista", lista);
		$scope.listContaContabil = [];
		var obj = {id: -1, nmContaContabil: "TODOS"};
		$scope.listContaContabil.push(obj);
		var listIdsConta = [];
		for (var i = 0; i < lista.length; i++){
			var retorno = listIdsConta.indexOf(lista[i].contaContabil.id);
			
			if (retorno < 0){
				$scope.listContaContabil.push(lista[i].contaContabil);
				listIdsConta.push(lista[i].contaContabil.id);
			}
		};
	}

	$scope.filterContaContabil = function (tipo){
		//console.log(tipo);
		if (tipo.id == -1){
			$scope.listaAux = [];
			$scope.loadContasDespesa($scope.nrOpcaoCons, $scope.primeiroLoad);
		}else{
			$scope.listaAux = [];
			for (var i = 0; i < $scope.contaDespesaList.length; i++){
				if ($scope.contaDespesaList[i].contaContabil.id == tipo.id)
					$scope.listaAux.push($scope.contaDespesaList[i]);
			}
			definirTotais( $scope.listaAux );
			configurarTable( $scope.listaAux );
		}
	};
	
	function inicio() {
		$scope.listaAux = [];
		$scope.empresaFinanceiro = {};
		$scope.cssStyleObject = {};

		inicializarColorFiltroStatus();
	
		$scope.inStatusFiltro = null;
		
		inicializarTotais(); 
		inicializarData();

		loadFiltros();

		defineConfigMultiSelect();
		definirConfigDateRange();

		loadEmpresaFinanceiro();	
		
	};


	inicio();
});


