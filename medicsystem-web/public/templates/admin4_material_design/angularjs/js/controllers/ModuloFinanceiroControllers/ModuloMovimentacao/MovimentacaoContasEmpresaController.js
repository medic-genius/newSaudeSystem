	'use strict';

MedicsystemApp.controller('MovimentacaoContasEmpresaController', function($rootScope,$scope,
	$state,$stateParams,NgTableParams,$filter, financeiroFactory, Auth, $modal) {

	Number.prototype.format = function( n, x, s, c) {
		var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')', num = this.toFixed(Math.max(0, ~~n));
    
   	 	return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

	function definirPaginacao( colecao ) {
		$scope.currentPage = 1;
		$scope.totalItems = colecao.length;
		$scope.itemsPerPage = 10;
	}

	function configurarTable( colecao ) {
		if( colecao.length > 0 ) {
			colecao.forEach( function( item, index) {
				item.currentPage = 1;
				item.totalItems = item.itens.length;
				item.itemsPerPage = 10;	
			});

			$scope.movimentacoesFinal = colecao;
			definirPaginacao( $scope.movimentacoesFinal );
		} else
			$scope.movimentacoesFinal = [];
  	};

  	function inicializarData() {
   		var dataAtual = moment();
     
        $scope.dataFiltro = {};
        $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};  
    };

    function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	'Hoje': [moment(), moment()],
	   		'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()],
	   		'Últimos 60 Dias': [ moment().subtract(59, 'days'),moment()],
	   		'Últimos 90 Dias': [ moment().subtract(89, 'days'),moment()],
		};
    };

  	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
  			}
  		} )
  	};

  	function footerDocumento( doc, data, totalPagesExp ) {
  		if( $scope.pagesNumber.length > 0 ) {
  			var inEncontrado = $scope.pagesNumber.indexOf( data.pageCount );

  			if( inEncontrado == -1 ) {
  				var str = "Pagina " + data.pageCount;
  				if (typeof doc.putTotalPages === 'function')
        		    str = str + " de " + totalPagesExp;

        		doc.setFontSize(10);
        		doc.text(str, data.settings.margin.left + 450, doc.internal.pageSize.height - 30);
        		$scope.pagesNumber.push(data.pageCount);
  			}
  		} else {
  			var str = "Pagina " + data.pageCount;
  			if (typeof doc.putTotalPages === 'function')
        		str = str + " de " + totalPagesExp;

        	doc.setFontSize(10);
        	doc.text(str, data.settings.margin.left + 450, doc.internal.pageSize.height - 30);
        	$scope.pagesNumber.push(data.pageCount);
  		}
  		
        return doc;
	};

  	// begin funcoes gerar pdf movimentacoes
	function subHeaderDocumento(doc, offset) {
		if(!offset) {
			offset = 0;
		}

		var nmFantasia =  $scope.empresaFinanceiro.selected ? $scope.empresaFinanceiro.selected.nmFantasia : '-';
		var nmContaBancaria = $scope.contaBancaria.selected ? $scope.contaBancaria.selected.nmContaBancaria : '-';
		var dataAtual = moment().format('DD/MM/YYYY HH:mm:ss');
		

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, (50 + offset), 'Empresa Financeiro:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, (50 + offset), nmFantasia );

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, (70 + offset), 'Conta Bancária:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, (70 + offset), nmContaBancaria );

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(350, (70 + offset), 'Data:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(390, (70 + offset), dataAtual );

		return doc;
	};

	function hearderDocumento(doc, data, imgData, offset) {
		/* if(data.pageCount == 1) {
			doc.addImage(imgData, 'JPEG', 40, 20, 200/*width, 43/*heigth, 'logo');
		} */
		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.setTextColor(50);
		doc.writeText(0, (20 +offset), 'Relatório de movimentações',{align:'center'});
		return doc;
	};

	function resumeDocumento(doc, data ) {

			var starPosition = doc.autoTableEndPosY() + 50 ;
			var entrada =  'R$ ' + ($scope.total.entrada).format(2,3,'.',',');
			var saida = 'R$ ' + ($scope.total.saida).format(2,3,'.',',');
			var saldo = 'R$ ' + ($scope.total.saldo).format(2,3,'.',',');
			var estorno = 'R$ ' +  ($scope.total.estorno).format(2,3,'.',',');

			doc.setFont("times");
			doc.setFontType('bold')
			doc.setFontSize(14);
			doc.setTextColor(50);
			doc.writeText(0,starPosition, 'Resumo de Totais',{align:'center'});

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(40, starPosition + 40, 'Entrada');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(10);
			doc.text(160, starPosition + 40, entrada );

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(40, starPosition + 60, 'Saída');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(10);
			doc.text(160, starPosition + 60, saida );

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(40, starPosition + 80, 'Saldo');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(10);
			doc.text(160, starPosition + 80, saldo );

			doc.setFont("times");
			doc.setFontType('bold');
			doc.setFontSize(12);
			doc.text(40, starPosition + 100, 'Estorno');

			doc.setFont("times");
			doc.setFontType('');
			doc.setFontSize(10);
			doc.text(160, starPosition + 100, estorno );

		return doc;
	};

	function gerarTable( doc, colecao, imgData ) {
		var tabelasParaGerar = [];
		var tableItem = {};
		tableItem.columns = [
				{title:'Data', dataKey:'data'},
				{title:'Ação', dataKey:'acao'},
				{title:'Documento', dataKey:'documento'},
				{title:'Fornecedor', dataKey:'fornecedor'},
				{title:'Descrição', dataKey:'descricao'},
				{title:'Entrada', dataKey:'entrada'},
				{title:"Saída", dataKey:"saida"},
				{title:'Saldo', dataKey:'saldo'},
		];
		tableItem.rows = [];

		//definir tabela pra cada movimentacao
		colecao.forEach( function( item, index) {	
			//var tableItem = {};
			if( index == (colecao.length - 1) ) {
				tableItem.saldoCaixa = String(item.nrSaldoFormatado);
			}
			
			/*tableItem.columns = [
				{title:'Data', dataKey:'data'},
				{title:'Ação', dataKey:'acao'},
				{title:'Documento', dataKey:'documento'},
				{title:'Fornecedor', dataKey:'fornecedor'},
				{title:'Descrição', dataKey:'descricao'},
				{title:'Entrada', dataKey:'entrada'},
				{title:"Saída", dataKey:"saida"},
				{title:'Saldo', dataKey:'saldo'},
			];
			tableItem.rows = [];*/

			var subItemList = item.itens;			
			// rows de cada tabela
			subItemList.forEach( function( subItem, subIndex) { //nmAcaoMovimentacao
				var dataFormatada = moment( subItem.dtMovimentacao ).format('DD/MM/YYYY');
				var dataMovimentacaoTemp = subItemList[0];

				if( subItemList[0].nmAcaoMovimentacao == ' Saldo anterior' ) {
					dataMovimentacaoTemp = subIndex == 1 ? dataFormatada : '';
				} else {
					dataMovimentacaoTemp = subIndex == 0 ? dataFormatada : '';
				}
				
				if( subItem.nmAcaoMovimentacao != ' Saldo anterior' )  {
						var dataType = {};
						dataType.data =  dataMovimentacaoTemp;
						dataType.acao = subItem.nmAcaoMovimentacao ? subItem.nmAcaoMovimentacao.toLowerCase() : '-';
						dataType.documento = subItem.nrDocumento ? subItem.nrDocumento : '-';
						dataType.fornecedor = subItem.fornecedores ? subItem.fornecedores.toLowerCase() : '-';
						dataType.descricao = subItem.nmDescricao ? subItem.nmDescricao.toLowerCase() : '-';
						dataType.saida = subItem.nrDebitoFormatado;
						dataType.entrada = subItem.nrCreditoFormatado;
						dataType.saldo = subItem.nrSaldoFormatado;
						dataType.haveEstorno = subItem.haveEstorno;

						tableItem.rows.push( dataType );
				}
			});
			//tabelasParaGerar.push( tableItem );
		});

		tabelasParaGerar.push( tableItem );
		// console.log('tabelasParaGerar',tabelasParaGerar);

		//imprimir tabelas definidas
		var totalPagesExp = "{total_pages_count_string}";	
		tabelasParaGerar.forEach( function( item, index ) {
			
			doc.autoTable( item.columns, item.rows,{
				startY: (index  == 0 ? 206 : doc.autoTableEndPosY() + 50 ) , //206,
				pageBreak: 'auto',
				margin: {left: 40, top:120},
			 	theme: 'plain',
			 	styles: {fontSize:9,overflow: 'linebreak',columnWidth:'auto'},
			 	showHeader: 'firstPage',
			 	headerStyles: {halign:'center'},
			 	columnStyles: {
			 		data: {halign: 'data'},
			 		acao: {halign: 'left'},
			 		documento: {halign: 'left'},
			 		fornecedor: {halign: 'left'},
			 		descricao: {halign: 'left'},
			 		saida:{halign: 'right',textColor: 100},
			 		entrada:{halign:'right',textColor: 100},
			 		saldo:{halign:'right',textColor: 100}
			 	},
			 	addPageContent: function (data) {
					var offset = data.pageCount == 1 ? 120 : 20;
					doc = hearderDocumento(doc, data, imgData, offset);
					doc = subHeaderDocumento(doc, offset);
					doc = footerDocumento(doc, data, totalPagesExp);
		    	},
		    	drawRow: function (row, data) {//textColor: '#e50000'
		    		if( row.raw.haveEstorno ) {
		    			row.cells.acao["styles"].textColor = '#ff6666';
		    			row.cells.descricao["styles"].textColor = '#ff6666';
		    			row.cells.documento["styles"].textColor = '#ff6666';
		    			row.cells.fornecedor["styles"].textColor = '#ff6666';
		    			/*console.log("row",row);
		    			console.log("data",data);*/
		    		}
		    	},
			});
			doc.setFont("times");
			doc.setFontSize(10);
			doc.setTextColor(50);
			doc.setFontStyle('bold');
			doc.text(400, doc.autoTableEndPosY() + 20, 'Saldo em conta:');
			doc.text(515, doc.autoTableEndPosY() + 20,  item.saldoCaixa );

        		
		});

		if (typeof doc.putTotalPages === 'function'  ) {
			doc.putTotalPages(totalPagesExp);
		}

		resumeDocumento( doc );

		return	doc;
	};	

	function imprimirPdfMovimentacao( imgData ) {
		var doc = new jsPDF('p','pt');
		doc = gerarTable( doc, $scope.movimentacoesFinal, imgData );
		doc.autoPrint();
		
		var blob = doc.output('blob');
		var fileURL = URL.createObjectURL(blob);
		window.open(fileURL, '_blank');
	};

	function getImageReport( url, callBack ) {
		var img = new Image();
		img.onError = function() {console.log("Nao pode carregar a imagem " + url);};
		img.onload = function() {callBack(img);};
		img.src = url;
	};

  	$scope.gerarPdfMovimentacao = function() {
  		$scope.pagesNumber = [];
		getImageReport('assets/global/img/medic-lab-logo.jpg',imprimirPdfMovimentacao);
  	};

  	// end funcoes gerar pdf movimentacoes

  	$scope.loadContasBancariaDeEmpresaFinanceiro = function( empresaFinanceiro ) {
  		$scope.contasBancariaOptions = [];
  		$scope.contaBancaria = { selected:null };
  		$scope.empresaFinanceiroId = empresaFinanceiro.idEmpresaFinanceiro;
  		$scope.movimentacoes = [];
  		$scope.movimentacoesFinal = [];

  		financeiroFactory.efContasBancaria
  		.get( { idEmpresaFinanceiro:empresaFinanceiro.idEmpresaFinanceiro}, function( contasBancaria ) {
  			if( contasBancaria.length > 0 ) {
  				$scope.contasBancariaOptions = contasBancaria;
  			}
  		} )
  	};

  	function getIntervaloDataSelecionada() {
    	if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };

    function inicializarTotais() {
    	$scope.total = {};
    	$scope.total.entrada = 0;
    	$scope.total.saida = 0;
    	$scope.total.saldo = 0;
    	$scope.total.estorno = 0;
    	$scope.total.vlSaldoMesAnterior = 0;
    };

    function calcularTotais( lista, saldoMesAnteriorObject ) {
    	inicializarTotais();
    	if( lista.length > 0)
	    	angular.forEach( lista, function( item, index ) {
	    		$scope.total["entrada"] = $scope.total["entrada"] + item.totalEntrada;
	    		$scope.total["saida"] = $scope.total["saida"] + item.totalSaida;
	    		$scope.total["saldo"] = $scope.total["entrada"] - $scope.total["saida"];
	    		$scope.total["estorno"] = $scope.total["estorno"] + item.totalEstorno;
	    	});
	    if( saldoMesAnteriorObject.description ) {
	    	$scope.total['vlSaldoMesAnterior'] = parseFloat(saldoMesAnteriorObject.description);
	    	$scope.total["saldo"] = $scope.total["saldo"] + $scope.total['vlSaldoMesAnterior'];
	    }
    };
  	
  	function filtrarListaMovimentacao( value ) {

  		var resultFinal = [];
  		var filter = null;

  		angular.forEach(value.itens, function(item) {
  			var fornecedor = item.fornecedores ? item.fornecedores.trim().toUpperCase(): '';
  			var acao =  item.nmAcaoMovimentacao ? item.nmAcaoMovimentacao.trim().toUpperCase(): '';
  			var descricao = item.nmDescricao ? item.nmDescricao.trim().toUpperCase(): '';
  			var nrDocumento = item.nrDocumento ? item.nrDocumento.trim().toUpperCase(): '';
  			var data = item.dtMovimentacaoFormatado;

  			if( S(fornecedor).contains( $scope.mode ) || S(acao).contains( $scope.mode ) ||  S(descricao).contains( $scope.mode )
  				||  S(nrDocumento).contains( $scope.mode ) ||  S(data).contains( $scope.mode ) )
  				resultFinal.push(item);
  		});

  		value.itens = resultFinal;

  		return value;
  	};

    $scope.teste = function() {
    	
    	var search = $scope.search.trim().toUpperCase();
    	var listToSearch = JSON.parse(JSON.stringify( $scope.movimentacoes  )); [];
    	var listToUpdate = [];
    	$scope.mode = String(search);

    	var options = {/* http://fusejs.io/ */
    		threshold: 0,
    		tokenize: true,
    		keys: ["itens.dtMovimentacaoFormatado","itens.fornecedores","itens.nmAcaoMovimentacao","itens.nmDescricao","itens.nrDocumento"]
    	};

    
    	var fuse = new Fuse( listToSearch ,options );
    	listToSearch =  fuse.search( search );
    	listToSearch =  listToSearch.filter(filtrarListaMovimentacao) ;

	  	    
	    if( (listToSearch != null && listToSearch.length > 0) && (search != null && search.length > 0 )  ) {
    		listToUpdate = listToSearch;
    		/*console.log("if");*/
	    }
    	else if( search == null ||  search.length == 0 ) {
    		listToUpdate = $scope.movimentacoes;
    	/*	console.log("else if");
    		console.log("$scope.movimentacoes",$scope.movimentacoes);*/
    	}

    	configurarTable( listToUpdate );
		//calcularTotais( listToUpdate );


    };

  	$scope.pesquisarMovimentacaoPorPeriodo = function() {

  		var intervalo = getIntervaloDataSelecionada();
  		var idEmpresaFinanceiro = $scope.empresaFinanceiro.selected ? $scope.empresaFinanceiro.selected.idEmpresaFinanceiro : null;
  		var idContaBancaria = $scope.contaBancaria.selected ? $scope.contaBancaria.selected.id : null;
  		$scope.movimentacoes = [];
  		$scope.movimentacoesFinal = [];

  		if( intervalo != null && idContaBancaria != null ) {
	  		financeiroFactory.movimentacaoPorDia
	  		.get( {dtInicio: intervalo.dtInicio, dtFim: intervalo.dtFim, nrIdEmpresa:idEmpresaFinanceiro , nrIdConta: idContaBancaria, search: $scope.search },
	  			function( result ) {
	  				if( result  ) {
	  			 		$scope.movimentacoes = result
						configurarTable($scope.movimentacoes);
						
						financeiroFactory.entradaSaldoMesAnterior
				  		.get({dtInicio: intervalo.dtInicio , nrIdConta: idContaBancaria}, function( result ) {
				  			calcularTotais( $scope.movimentacoes, result );
				  		})
						
	  				}
	  		});
	  	}
  	};


	function inicio() {		
		$scope.empresaFinanceiro = {};
		$scope.contaBancaria = {};
		$scope.search = null;
		
		inicializarData();
		inicializarTotais();

		definirConfigDateRange();

		loadEmpresaFinanceiro();

	};

	inicio();


	$scope.modalRetiradaDetalhes = function(item, detalhes){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/movimentacao/modal/modalRetiradaDetalhes.html',
			controller: 'ModalRetiradaDetalhesCtrl',
			size: 'lg',
			resolve:{
				item: function(){
					return item;
				},
				detalhes: function(){
					return detalhes;
				},
				dataInicio: function (){
					return moment($scope.dataFiltro.date.startDate);
				},
				dataFim: function(){
					return moment($scope.dataFiltro.date.endDate);
				},
				empresa: function (){
					return $scope.empresaFinanceiro.selected;
				},
				contaBancaria: function(){
					return $scope.contaBancaria.selected;
				}
			}
		});
	} 
	
});

MedicsystemApp.controller('ModalRetiradaDetalhesCtrl', function($scope, $modalInstance, financeiroFactory, item, detalhes, dataInicio, dataFim, empresa, contaBancaria,NgTableParams){	
	function inicio(){

		$scope.showPDF = false;
		$scope.showExcluidos = false;
		$scope.firstLoad = false;
		$scope.print ={};
		var dtInicio = dataInicio.format("YYYY-MM-DD").toString();
		var dtFim = dataFim.format("YYYY-MM-DD").toString();

		if(detalhes == 1){
			$scope.showPDF = false;
			$scope.totalRetirada = item.nrCreditoFormatado;
			financeiroFactory.detalhesRetiradaInd.get({idCaixa:item.idCaixaConferido, idEmpresa: empresa.idEmpresaFinanceiro}, function(result){
				$scope.showTabela = result.length;
				$scope.print = result;
				if(result.length > 0 ){
					$scope.showTab = true;
					$scope.tableParams = configurarTabela(result);
				} else {
					$scope.showTab = false;
				}
				var totalRetiradaGeral = 0;
				var totalRetiradaExcluidas =0;
				for(var i =0; i < result.length ; i ++){
					if(result[i].boExcluido == null ||result[i].boExcluido == false ){
						 totalRetiradaGeral += result[i].vlMovimentacaoCaixa;
					}
						 totalRetiradaExcluidas += result[i].vlMovimentacaoCaixa;
					
				}
				$scope.totalRetirada = totalRetiradaGeral;
				$scope.totalRetiradaExcluidas = totalRetiradaExcluidas;
			});
		}else {
			$scope.showPDF = true;
			var idEmpresa = empresa.idEmpresaFinanceiro;
			var idContaBancaria  = contaBancaria.id;
  		financeiroFactory.visualizarRetiradaGeral.get({idContaBancaria: idContaBancaria, idEmpresa: idEmpresa,
  			dataInicio: dtInicio, dataFim: dtFim}, function (dadosRetirada){
  				$scope.print = dadosRetirada;
  				$scope.showTabela = dadosRetirada.length;
	  		if(dadosRetirada.length > 0 ){
					$scope.showTab = true;
					$scope.tableParams = configurarTabela(dadosRetirada);
				} else {
					$scope.showTab = false;
				}
				var totalRetiradaGeral = 0;
				var totalRetiradaExcluidas =0;
				for(var i =0; i < dadosRetirada.length ; i ++){
					if(dadosRetirada[i].boExcluido == null ||dadosRetirada[i].boExcluido == false ){
						 totalRetiradaGeral += dadosRetirada[i].vlMovimentacaoCaixa;
					}
						 totalRetiradaExcluidas += dadosRetirada[i].vlMovimentacaoCaixa;
					
				}
				$scope.totalRetirada = totalRetiradaGeral;
				$scope.totalRetiradaExcluidas = totalRetiradaExcluidas;
	  	});
		}
	
	}

	$scope.mostrarExcluidos = function (valor){
		$scope.showExcluidos = true;
		if (!valor){
			$scope.showExcluidos = true;
			$scope.firstLoad = true;
		} else {
			$scope.showExcluidos = false;
			$scope.firstLoad = false;
		}
      
	}

	function configurarTabela(lista){
		
		var tableParams = new NgTableParams();
			if(lista.length > 0){
				var initialParams = { count: 15};
				var initialSettings = {
					counts:[], 
					paginationMaxBlocks: 13,
					paginationMinBlocks: 2,
					dataset: lista
				};
				tableParams = new NgTableParams(initialParams, initialSettings); 
			}

			
		return tableParams;
	}; 

	$scope.gerarPdfDetalhes = function() {
  	getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
  };

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
	};

  var print = function ( imgData ){

		var doc = new jsPDF('landscape','pt','letter','a4');
		var rows = [];

		
		var columns = [            
			{title: "Data Movimentação", dataKey: "dtMovimentacao"},
			{title: "Nr Movimentação", dataKey: "nrMovimentacao"},
			{title: "Funcionário", dataKey: "nmFuncionario"},
			{title: "Valor", dataKey: "vlMovimentacaoCaixa"} 
		];
		
		var valorTotal = 0;
		for (var i = 0; i < $scope.print.length; i++){
			if(!$scope.showExcluidos){
				if(!$scope.print[i].boExcluido){
					rows[i] = {                
						"dtMovimentacao": $scope.print[i].dtMovimentacaoCaixaFormatada,
						"nrMovimentacao": $scope.print[i].idMovimentacaoCaixa,
						"nmFuncionario": $scope.print[i].nmFuncionario,
						"vlMovimentacaoCaixa":$scope.print[i].vlMovimentacaoCaixa.toLocaleString("pt-BR", {minimumFractionDigits:2, maximumFractionDigits: 2})
					};
					valorTotal += $scope.print[i].vlMovimentacaoCaixa;
				}
			} else{
				rows[i] = {                
						"dtMovimentacao": $scope.print[i].dtMovimentacaoCaixaFormatada,
						"nrMovimentacao": $scope.print[i].idMovimentacaoCaixa,
						"nmFuncionario": $scope.print[i].nmFuncionario,
						"vlMovimentacaoCaixa":$scope.print[i].vlMovimentacaoCaixa.toLocaleString("pt-BR", {minimumFractionDigits:2, maximumFractionDigits: 2}),
						"boExcluido": $scope.print[i].boExcluido
					};
					valorTotal += $scope.print[i].vlMovimentacaoCaixa;
			}
				
				
		} 
			
		var valorTotalFormatada = 'R$ ' + valorTotal.toLocaleString("pt-BR",  {minimumFractionDigits:2, maximumFractionDigits: 2});
		var dataAtual = moment().format('DD/MM/YYYY HH:mm:ss');
		var dtInicio = dataInicio.format("DD/MM/YYYY");
		var dtFim = dataFim.format("DD/MM/YYYY");
		var nomeEmpresa = empresa.nmFantasia;
		var nmContaBancaria = contaBancaria.nmContaBancaria;


		//CABECALHO	
    doc.setFont("Verdana");
		doc.setFontType('bold')
		doc.setFontSize(14);
	
		doc.writeText(0,90, 'Movimentação Detalhes',{align:'center'});
	
		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 130, 'Empresa:');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(77, 130, nomeEmpresa);

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20,150, 'Conta Bancária:');
		
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(135, 150, nmContaBancaria);

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 170, 'Período Selecionado:');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(135, 170, dtInicio);
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(190, 170, '-'); 
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(195, 170, dtFim);
		
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(20, 190, 'Data Emissão: ');
	
		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(120, 190, dataAtual);		

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(620, 210, 'Valor Total:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(690, 210, valorTotalFormatada); 
		var posicaoTabela = 220;

	
		//        doc.autoTable(columns, rows, {startY: 220, margin: {top: 110}, overflow: 'linebreak'});
		doc.autoTable(columns, rows, {
			startY: posicaoTabela, 
			margin: {horizontal: 20}, 
			bodyStyles: {valign: 'top', fontSize: 9},
			styles: {overflow: 'linebreak'},
			columnStyles: {vlMovimentacaoCaixa: {halign:'right'}}, 
			createdCell: function(cell, data) {
					if (data.row.raw.boExcluido) {
					  cell.styles.fontStyle = 'bold';
					  cell.styles.textColor =  [255, 0, 0]
					}
				}
		});

		doc.autoPrint();
		
		var blob = doc.output('blob');
		var fileURL = URL.createObjectURL(blob);
		window.open(fileURL, '_blank');
		
  };
	

	$scope.close = function(){
		$modalInstance.close();
	};

	inicio();
});


