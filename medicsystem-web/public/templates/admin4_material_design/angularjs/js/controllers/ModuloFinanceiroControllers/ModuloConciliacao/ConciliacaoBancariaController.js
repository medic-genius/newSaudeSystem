'use strict';

MedicsystemApp.controller('ConciliacaoBancariaController',function($rootScope,$scope,$state,	

	$stateParams,financeiroFactory,Auth,NgTableParams,toastr, $modal, $timeout,  $location) {

	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
  			}
  		} )
  };

  $scope.ignorarLancamento = function(item, doc){
		doc.disabled = true;
		if (!item){
			doc.disabled = true;
			doc.showConc = false;
			$scope.firstLoad = true;
		} else {
			doc.disabled = false;
			doc.showConc = true;
			$scope.firstLoad = false;
		}
  }
 
  $scope.loadContasBancariaDeEmpresaFinanceiro = function( empresaFinanceiro ) {
  	$scope.empresaFinanceiroPagante = empresaFinanceiro;
		$scope.contasBancariaOptions = [];
		$scope.contaBancaria = { selected:null };
		if(empresaFinanceiro){
			$scope.empresaFinanceiroId = empresaFinanceiro.idEmpresaFinanceiro;
		}
		$scope.movimentacoes = [];
		$scope.movimentacoesFinal = [];
		if(empresaFinanceiro){
			financeiroFactory.contasBancariasConc.get( { idEmpresaFinanceiro:empresaFinanceiro.idEmpresaFinanceiro}, function( contasBancaria ) {
				if( contasBancaria.length > 0 ) {
					$scope.contasBancariaOptions = contasBancaria;
				}
			})
		}
		$scope.infoExtrato = null;
	};

	$scope.pesquisarConciliacoes = function(empresa, contaBancaria, cont){
		$scope.contaBancariaSelected = contaBancaria;
		$scope.empresaSelected = empresa;
		financeiroFactory.getSugestao.get({idEmpresa: empresa.idEmpresaFinanceiro, idContaBancaria: contaBancaria.id, offset: cont}, function(response){
			if(response.data.length > 0){
				for(var i=0; i < response.data.length; i++){
					response.data[i].disabled = null;
				
					response.data[i].extValorFormatado = (Math.abs(response.data[i].valor)).toLocaleString("pt-BR", {minimumFractionDigits:2});
					if(response.data[i].valorDespesa != null){
						response.data[i].valorDespesaFormatado = 'R$ ' + (Math.abs(response.data[i].valorDespesa)).toLocaleString("pt-BR", {minimumFractionDigits:2});
					}
					if(response.data[i].valor <0){ //colocar por tipo
						response.data[i].isDebit = true;
					}else{
						response.data[i].isDebit =false;
					}

					if(response.data[i].lancamentos){
						response.data[i].showTelaSugestao = true;
						response.data[i].showTelaNovaBusc = false;
						var arrayParam =[];
						var dtFormatado = new Date(response.data[i].despesaData);

						arrayParam.push(response.data[i].lancamentos);
						response.data[i].lancamentos = arrayParam;
					}else{
						response.data[i].showTelaNovaBusc = true;
						response.data[i].showTelaSugestao = false;
						response.data[i].lancamentos = null;
					}
					
					var dataForm = new Date(response.data[i].dataTransacao);
					response.data[i].dataFormatada = moment(dataForm).format("DD/MM/YYYY").toString();
				}
			}
			$scope.infoExtrato = response;
			configurarPage(response);
		});
	/*Fim sujestao*/

	}

	$scope.pageChanged = function(cont){
		$scope.pesquisarConciliacoes($scope.empresaSelected, $scope.contaBancariaSelected, cont);
	}

	function configurarPage( colecao ) {
		if( colecao.data.length > 0 ) {
			colecao.data.forEach( function( item, index) {
				item.currentPage = 1;
				item.totalItems = item.totalItems;
				item.itemsPerPage = 10;	
			});

			$scope.infoExtratoFinal = colecao;
			definirPaginacao( $scope.infoExtratoFinal );
		} else
			$scope.infoExtratoFinal = [];
  };

  function definirPaginacao( colecao ) {
		$scope.currentPage = colecao.offset;
		$scope.totalItems = colecao.total;
		$scope.itemsPerPage = 10;
	}

	$scope.loadContasContabil = function() {
		$scope.contaContabilList = [];
		var tipoTreeElement_contaContabil = 1;

    financeiroFactory.contaContabil
    .query( function( contasContabil ) {
     	$scope.contaContabilList = contasContabil;
     	prepararModalSelectTreeElement( tipoTreeElement_contaContabil, $scope.contaContabilList );
    })  		
	};

 	$scope.goToCadastro = function(empresa){
 		loadModalCadastro(empresa);
 	}

 	$scope.goToImportarExtrato = function(empresa, banco){
 		loadModalImportar(empresa, banco);
 	}

 	$scope.goToCadastroContasAPagar = function(doc, nmDespesa, nmFornecedor){
 		doc.showValorExtrato = false;
 		loadModalContasAPagar(doc, nmDespesa, nmFornecedor);
 	}

 	$scope.loadBuscarLancamento = function(doc){
		loadBuscarLancamento(doc, $scope.showTelaSugestao, $scope.showTelaNovaBusc);
 	}

 	$scope.close = function(doc, desp){
 		
 		for(var i=0; i < doc.lancamentos.length ; i++){
 			if(doc.lancamentos[i].id == desp.id){
 				//desp.isChecked = false;
 				doc.lancamentos[i].vlDesconto = null;
 				doc.lancamentos[i].vlMulta = null;
 				doc.valorDif = null;
 				doc.valorDiferenca = null;
				doc.lancamentos.splice(i, 1);
				break;
 			}
 		}

		calcularDiferenca(doc);	

 		if(doc.lancamentos.length ==0){
 			doc.showTelaSugestao = false;
 			doc.showTelaNovaBusc = true;
 			doc.showTelaDiferenca = false;
 			doc.showValorExtrato = false;
 			doc.valorDif = null;
 			doc.valorDiferenca = null;
 			doc.valorMultaJuros = 0;
 		}
 		
 	}

 	function loadFornecedor() {
		$scope.fornecedorOptions = [];
		
		financeiroFactory.fornecedor
		.query( function( fornecedorList) {
			if( fornecedorList.length > 0 )
				$scope.fornecedorOptions = fornecedorList;
		});
	};

	function calcularDiferenca(retorno){
		
		var vlDiferenca =0;
		var totalSelecSist = 0;
		retorno.showTelaDiferenca = false;
		if(retorno.lancamentos.length > 0){
			var desconto = 0;
			var multa =0;
			var juros = 0;
			var valorTotal = 0;

			for(var i=0; i < retorno.lancamentos.length; i++){
				if(retorno.lancamentos[i].vlDesconto != null){
					desconto += parseFloat(retorno.lancamentos[i].vlDesconto);
				}
				
				if(retorno.lancamentos[i].vlMulta != null){
					multa += retorno.lancamentos[i].vlMulta;
				} 

				if(retorno.lancamentos[i].vlJuros != null){
					juros += retorno.lancamentos[i].vlJuros;
				}
				totalSelecSist += retorno.lancamentos[i].vlDespesa;
			}

			valorTotal = totalSelecSist - desconto + multa + juros;
			if(valorTotal > retorno.valor){
				retorno.isPositive = true;
			} else{
				retorno.isPositive = false;
			}

			vlDiferenca = Math.abs(valorTotal) + retorno.valor;
			if(parseFloat(vlDiferenca.toFixed(2)) != 0){
					retorno.showTelaDiferenca = true;
					retorno.valorDiferenca = vlDiferenca.toLocaleString("pt-BR", {minimumFractionDigits: 2});
					retorno.valorDif = parseFloat(vlDiferenca.toFixed(2));
				}else{
					retorno.showTelaDiferenca = false;
					retorno.valorDiferenca = null;
					retorno.valorDif = null;
				}
		}else{
			retorno.showTelaDiferenca = false;
			retorno.valorDif= null;
		}
	}

	$scope.setarTaxaDesconto = function(despesaSelected, doc){
		for(var i=0; i < doc.lancamentos.length; i++){
			if(doc.lancamentos[i].id == despesaSelected.id){
				if(despesaSelected.vlDespesa > Math.abs(doc.valor)){
					if(doc.valorDif > despesaSelected.vlDespesa){ //seta desconto
						messageToastr('erro', 'Os valores não batem','Aviso');
					}else {
						if(doc.lancamentos[i].vlDesconto != null){
							doc.lancamentos[i].vlDesconto = (Math.abs(doc.valorDif) + doc.lancamentos[i].vlDesconto);
						}else{
							doc.lancamentos[i].vlDesconto = Math.abs(doc.valorDif);
						}

						doc.vlDesconto = Math.abs(doc.lancamentos[i].vlDesconto);
					}
				}else if(despesaSelected.vlDespesa < Math.abs(doc.valor)){ //seta juros
					if(doc.valorDif > despesaSelected.vlDespesa){
						messageToastr('erro', 'Os valores não batem','Aviso');
					}else {
						if(doc.lancamentos[i].vlMulta != null && doc.lancamentos[i].vlJuros != null){
							doc.lancamentos[i].vlMulta = Math.abs(doc.valorDif) + doc.lancamentos[i].vlMulta + doc.lancamentos[i].vlJuros;
						}else{
							doc.lancamentos[i].vlMulta = Math.abs(doc.valorDif);
						}
						
						doc.vlMulta = Math.abs(doc.lancamentos[i].vlMulta);
					}
				}else if(despesaSelected.vlDespesa == Math.abs(doc.valor)){
					doc.lancamentos[i].vlMulta = null;
					doc.lancamentos[i].vlDesconto = null;
					doc.valorMultaJuros = 0;
				}
			}
		}
		calcularDiferenca(doc);
	}

	function messageToastr(type, message, title) {
	    if( type == 'erro' ) {
	       toastr.error(  message, title.toUpperCase() );
	    } else if( type =='sucesso'){
	    	toastr.success(message, title.toUpperCase());
	    }
	};

	$scope.usarValorExtrato = function(doc){
		doc.showValorExtrato = true;
		doc.valorDespesaExtratoFormatado = (Math.abs(doc.valor)).toLocaleString("pt-BR", {minimumFractionDigits: 2});
		doc.lancamentos[0].vlDespesa = Math.abs(doc.valor);
		calcularDiferenca(doc);
	}


	$scope.conciliarDadosBancarios = function(dados){
		var continuarConc = false;
		var arryIdDespesa=[];
		for(var i=0; i < dados.lancamentos.length; i++){
			arryIdDespesa.push(dados.lancamentos[i].id);
		}
		var dfEnviada = finalizarDespesaEnviar(dados);
			financeiroFactory.doConciliacao.save({idTransBancaria: dados.idTransacao, idsDespesa: arryIdDespesa},dfEnviada.lancamentos, function(result){
				if(result.length >0){
					if(result[0] === "S"){
						messageToastr('sucesso', 'Conciliação efetuada', 'Sucesso');
						dados.isConciliado = true;
					}else{
						messageToastr('erro', 'Verifique os dados', 'Atenção');
						dados.isConciliado = false;
					}
				}
			});
	}


	$scope.desconciliarDadosBancarios = function(desconciliaoDados, contaBancaria){
		var arrayIdDespesaDesc =[];
		for(var i=0; i < desconciliaoDados.lancamentos.length; i++){
			arrayIdDespesaDesc.push(desconciliaoDados.lancamentos[i].id);
		}
		financeiroFactory.desconciliar.edit({idTransBancaria:desconciliaoDados.idTransacao},desconciliaoDados.lancamentos, function(response){
			if(response.length >0){
				if(response[0] === "S"){
					desconciliaoDados.lancamentos= [];
		 			desconciliaoDados.showTelaSugestao = false;
		 			desconciliaoDados.showTelaNovaBusc = true;
		 			desconciliaoDados.showTelaDiferenca = false;
		 			desconciliaoDados.showValorExtrato = false;
		 			desconciliaoDados.valorDif = null;
		 			desconciliaoDados.valorDiferenca = null;
		 			desconciliaoDados.isConciliado = false;
			 		messageToastr('sucesso', 'Desconciliacao efetuada', 'Sucesso');
				}else{
					messageToastr('erro', 'Verifique os dados', 'Atenção');
				}
			}

			
		});
	}


	function finalizarDespesaEnviar(df){
		var STATUS_ABERTO = 0;
		var STATUS_PAGO = 1;
		var TIPO_CONTA_PAGAR = 0;
		for(var i=0; i< df.lancamentos.length; i++){
			if(df.lancamentos[i].inStatus != 1){
				df.lancamentos[i].inTipo = TIPO_CONTA_PAGAR;
				df.lancamentos[i].inStatus = STATUS_PAGO;
				//df.lancamentos[i].vlDespesa = Math.abs(df.valor);
				df.lancamentos[i].dtPagamento = new Date(df.dataTransacao);
				df.lancamentos[i].inFormaPagamento = 1;
				if(df.lancamentos[i].vlDesconto != null ){
					df.lancamentos[i].vlPago = Math.abs(df.lancamentos[i].vlDespesa) - df.lancamentos[i].vlDesconto;
				}else if(df.lancamentos[i].vlMulta != null){
					df.lancamentos[i].vlPago = Math.abs(df.lancamentos[i].vlDespesa) + df.lancamentos[i].vlMulta;
				} else{
					df.lancamentos[i].vlPago = Math.abs(df.lancamentos[i].vlDespesa);
				}
				df.lancamentos[i].empresaFinanceiroPagante = $scope.empresaFinanceiroPagante;
				df.lancamentos[i].contaBancaria = $scope.contaBancariaSelected;
			}
			
			delete df.lancamentos[i].isChecked;
			delete df.lancamentos[i].dtVencimentoFormatada;
			delete df.lancamentos[i].dtPagamentoFormatada;
			delete df.lancamentos[i].valorMultaJuros;

		}
		
		return df;
	
	}

	$scope.modalNrServico = function(doc){
		var cont = 0;
		for(var i=0; i< doc.lancamentos.length; i++){
			if(doc.lancamentos[i].inStatus != 1){
				cont++;
			}
		}
		if(cont > 0){
			showModalNrServico(doc);
		}else {
			$scope.conciliarDadosBancarios(doc);
		}
		
	}

	function showModalNrServico(doc){
				var modalInstance = $modal.open({
				animation: true,
				backdrop: 'static',
				templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/modal/modal-nrservico.html',
				controller: 'modalNrServico',
				size: 'sm',
				resolve:{
					doc: function(){
						return doc;
					}
				}
			})
			modalInstance.result.then(function(doc){
				$scope.conciliarDadosBancarios(doc);
			})		
	}


	function inicio() {		
		$scope.empresaFinanceiro = {};
		$scope.contaBancaria = {};
		$scope.search = null;
		$scope.dfSelected = null;
		loadEmpresaFinanceiro();
		loadFornecedor();
	
	};

	inicio();

	function loadModalCadastro (empresa){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/modal/modal-cadastro-contabancaria.html',
			controller: 'modalCadastroContaBancariaCtrl',
			size: 'lg',
			resolve:{
				empresa: function(){
					return empresa;
				}
			}
		})
		modalInstance.result.then(function(empresa){			
			$scope.loadContasBancariaDeEmpresaFinanceiro(empresa);
		})
	}

	function loadModalImportar (empresa, banco){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/modal/modal-importar-extrato.html',
			controller: 'modalImportarExtratoCtrl',
			size: 'lg',
			resolve:{
				empresa: function(){
					return empresa;
				}, 
				banco: function(){
					return banco;
				}
			}
		})

		modalInstance.result.then(function (retorno) {
	   			$scope.infoExtrato = retorno;
	   		})
	}

	function loadModalContasAPagar(doc,nmDespesa, nmFornecedor){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/modal/modal-cadastro-contasapagar.html',
			controller: 'modalCadastroContasApagarCtrl',
			size: 'lg',
			resolve:{
				nmDespesa: function(){
					return nmDespesa;
				}, 
				nmFornecedor: function(){
					return nmFornecedor;
				},
				empresa: function(){
					return $scope.empresaFinanceiro.selected;
				},
				contaBancaria: function(){
					return $scope.contaBancaria.selected;
				},
				doc: function(){
					return doc;
				}
			}
		})
			modalInstance.result.then(
	   		function (retorno) {
	   			if(retorno){
	   				if(!doc.lancamentos) {
	   					doc.lancamentos = [];
	   				}
	   				doc.lancamentos = doc.lancamentos.concat(retorno);
	   				doc.showTelaSugestao = true;
	   				doc.showTelaNovaBusc = false;
	   				doc.isConciliado = false;
	   				doc.valorMultaJuros = 0;
	   				
	   				calcularDiferenca(doc);
	   				
	   			}
	   			
	     	}, 
	    	function () {
	       		console.info('Modal dismissed at: ' + new Date());
	      });
	};

	function loadBuscarLancamento(doc, showTelaSugestao, showTelaNovaBusc){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/conciliacaoBancaria/modal/modal-buscarlancamentos.html',
			controller: 'modalBuscarLancamentosCtrl',
			size: 'lg',
			resolve:{
				doc: function(){
					return doc;
				},
				empresa: function(){
					return $scope.empresaFinanceiro.selected;
				},
				showTelaSugestao: function(){
					return $scope.showTelaSugestao;
				}, 
				showTelaNovaBusc: function(){
					return $scope.showTelaNovaBusc;
				},
				contaBancaria: function(){
					return $scope.contaBancaria.selected;
				}
			}
		});
			modalInstance.result.then(
	   		function (retorno) {
	   			if(retorno){
	   				if(!doc.lancamentos) {
	   					doc.lancamentos = [];
	   				}
	   				if(retorno.length == 0) {
	   					doc.showTelaSugestao = false;
	   					doc.showTelaNovaBusc = true;
	   				}else{
	   					doc.showTelaSugestao = true;
	   					doc.showTelaNovaBusc = false;
	   				}	   			
	   				doc.lancamentos = doc.lancamentos.concat(retorno);
	   				for(var i=0; i < doc.lancamentos.length; i++){
	   					if(doc.lancamentos[i].vlMulta == null || doc.lancamentos[i].vlJuros == null){
	   						doc.lancamentos[i].vlMulta = 0; doc.lancamentos[i].vlJuros  = 0;
	   						doc.lancamentos[i].valorMultaJuros = doc.lancamentos[i].vlMulta + doc.lancamentos[i].vlJuros;
	   					}else{
	   						doc.lancamentos[i].valorMultaJuros = doc.lancamentos[i].vlMulta + doc.lancamentos[i].vlJuros;
	   					}
	   				}
	   				$scope.despesasSistemas = doc;
	   				calcularDiferenca($scope.despesasSistemas);
	   			}
	     	}, 
	    	function () {
	       		console.info('Modal dismissed at: ' + new Date());
	      });
	};

});

MedicsystemApp.controller('modalCadastroContaBancariaCtrl', function ($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window, toastr, empresa) {

	$scope.salvarCadastroContaBan = function(){

		var isValido = validarDadosCadastro ($scope.empresaFinanceiro.selected, $scope.filters.selected, $scope.banco.selected, $scope.agencia.selected);

		if( isValido ) {

			toastr.clear();
			if($scope.filters.selected.status != 2){
				var contaBancaria = {
					nmContaBancaria: $scope.banco.nmContaBancaria,
					empresaFinanceiro: {
						idEmpresaFinanceiro: $scope.empresaFinanceiro.selected.idEmpresaFinanceiro
					},
					inTipo:  $scope.filters.selected.status,
					banco:{
						id: $scope.banco.selected.id
					},
					agencia: {
						id: $scope.agencia.selected.id
					},
					nrDigitoVerificador: $scope.agencia.nrDigitoConta,
					nrContaBancaria: $scope.agencia.nrNumeroConta
				}
			}else{
				var contaBancaria = {
					nmContaBancaria: $scope.banco.nmContaBancaria,
					empresaFinanceiro: {
						idEmpresaFinanceiro: $scope.empresaFinanceiro.selected.idEmpresaFinanceiro
					},
					inTipo:  $scope.filters.selected.status
				}
			}
			
			
			financeiroFactory.cadastroContasBanc.save(null, contaBancaria, function(resposta){
				messageToastr("Informação", "Conta bancária cadastrada com sucesso.", "SUCESSO");
				$modalInstance.close(empresa);
				$state.go('conciliacaoBancaria');
			});

		}
	};

	function validarDadosCadastro (){
		var isValido = true;
		var msgRequired = " é obrigatória (o).";
		var msgSelect = "Por favor, selecione ";

		if( $scope.empresaFinanceiro.selected == undefined) {
			isValido = false;
			messageToastr('erro', msgSelect +'a empresa','Calma');
		} else if( $scope.banco.nmContaBancaria == null || $scope.banco.nmContaBancaria.length == 0) {
			isValido = false;
			messageToastr('erro','Descrição' + msgRequired,'Calma');
		}else if($scope.filters.selected == undefined){
			isValido = false;
			messageToastr('erro', msgSelect + 'o tipo da conta', 'Calma');
		} else if($scope.filters.selected.status != 2){
				if($scope.banco.selected == undefined){
					isValido = false;
					messageToastr('erro', msgSelect +'o banco','Calma');
				}else if($scope.agencia.selected == undefined){
					isValido = false;
					messageToastr('erro', msgSelect +'a agencia','Calma');
				} else if($scope.agencia.nrNumeroConta == null || $scope.agencia.nrNumeroConta.length == 0 || $scope.agencia.nrNumeroConta.length < 4 ){
					isValido = false;
					messageToastr('erro','Preencha com o número correto da conta','Calma');
				}  else if($scope.agencia.nrDigitoConta == null || $scope.agencia.nrDigitoConta.length == 0 || $scope.agencia.nrDigitoConta.length < 2){
					isValido = false;
					messageToastr('erro','Digito da Conta' + msgRequired,'Calma');
				}

		}

		return isValido;
	};
	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
  			}
  		} )
  };

  function loadBancos(){
  	financeiroFactory.banco.get(function(bancos){
  		if(bancos.length>0){
  			$scope.bancoOptions = bancos;
  		}
  	});
  }

 	$scope.loadAgencias = function(banco){
  	financeiroFactory.agenciasByBanco.get({idBanco:banco.id}, function(agencias){
  		if(agencias.length >0 ){
  			$scope.agenciaOptions = agencias;
  		}
  	});
  }

  $scope.loadCondicao = function(valor){
  	if(valor.status == 2){
  		$scope.showResto = false;
  	} else {
  		$scope.showResto = true;
  	}
  }

	$scope.close = function(){
		$modalInstance.close();
	}

	$scope.loadFilter = {
		selected: undefined
	}

	$scope.filters = [
		{
			nome: 'Conta Corrente',
			status: 0,
			checked: false
		},
		{
			nome: 'Conta Poupança',
			status: 1,
			checked: false
		},
		{
			nome: 'Caixa Interno',
			status: 2,
			checked: false
		},
		{
			nome: 'Cartão de Crédito',
			status: 3,
			checked: false
		},
		{
			nome: 'Investimento',
			status: 4,
			checked: false
		}
	]



	function messageToastr(type, message, title) {
	    if( type == 'erro' ) {
	       toastr.error(  message, title.toUpperCase() ); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, title.toUpperCase() ); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, title.toUpperCase() ); 
	    }
	};

	function inicio() {
		$scope.empresaFinanceiro = {};
		if(empresa) {
			$scope.empresaFinanceiro['selected'] = empresa;
		} 
		$scope.banco = {};
		$scope.agencia ={};
		$scope.search = null;
		loadBancos();	
		loadEmpresaFinanceiro();
		$scope.showResto = false;
	}

	inicio();
});

MedicsystemApp.controller('modalImportarExtratoCtrl', function ($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window, toastr,  empresa, banco){

	function inicio(){

	}

	$scope.inicializarForm = function() {
 		$scope.f = null; 
 		$scope.uploadedFile = null;
 	};

 	$scope.preencher = function(files,invalidFiles) {
	  $scope.fileName = null;
	  if ($scope.uploadedFile){
	      $scope.fileName = $scope.uploadedFile[0].name;
	      $scope.startLoad = true;
	      //getBase64View($scope.uploadedFile[0]);
	  }
	};

	function getBase64View(file) {
	  var reader = new FileReader();
	  reader.readAsDataURL(file);
	  reader.onload = function (result) {
	      if (result){
	          $scope.$apply(function() {
	              $scope.fileB64View = reader.result.split(',')[1];
	              $scope.startLoad = false;
	          });
	      }
	      
	      $scope.idContaBancaria = banco.id;
	     	$scope.bankId = banco.banco.cdbanco;
	      
	      
	     
	      financeiroFactory.importExtrato.save({idEmpresa: empresa.idEmpresaFinanceiro, idContaSelected: $scope.idContaBancaria/*bankId: $scope.bankId, nrContaBan: banco.nrContaBancaria, nrDigBan: banco.nrDigitoVerificador, idEmpresa: empresa.idEmpresaFinanceiro*/},$scope.fileB64View, function(result){	      		
	      		var importado, motivo;
	      		if(result.length > 0){
      				if(result[0] == 'S'){

      					financeiroFactory.getSugestao.get({idEmpresa: empresa.idEmpresaFinanceiro, idContaBancaria: $scope.idContaBancaria}, function(response){
      						if(response.length>0){
	       						for(var i =0; i < response.length; i++){
	       							response[i].disabled = null;
	       							response[i].showTelaNovaBusc = true;
	       							response[i].showTelaSugestao = false;
	       							response[i].extValorFormatado =(Math.abs(response[i].valor)).toLocaleString("pt-BR", {minimumFractionDigits:2})
	       							if(response[i].valorDespesa != null){
												response[i].valorDespesaFormatado = 'R$ ' + (Math.abs(response[i].valorDespesa)).toLocaleString("pt-BR", {minimumFractionDigits:2});
											}
											if(response[i].valor <0){ //colocar por tipo
												response[i].isDebit = true;
											}else{
												response[i].isDebit =false;
											}

											if(response[i].lancamentos){
												response[i].showTelaSugestao = true;
												response[i].showTelaNovaBusc = false;
												var arrayParam =[];
												var dtFormatado = new Date(response[i].despesaData);

												arrayParam.push(response[i].lancamentos);
												response[i].lancamentos = arrayParam;
											}else{
												response[i].showTelaNovaBusc = true;
												response[i].showTelaSugestao = false;
												response[i].lancamentos = null;
											}

	       							var dataForm = new Date(response[i].dataTransacao);
	       							response[i].dataFormatada = moment(dataForm).format("DD/MM/YYYY").toString();


			       				}
			       			}

			       			$scope.infoExtratoTemp = response;
									$modalInstance.close($scope.infoExtratoTemp);
			       			
      					});
      					
      				}else{
      					messageToastr('erro', result[1], 'Atenção');
      				}
	      		}
	       }, function (evt) {
	                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	      });
	  };
	  reader.onerror = function (error) {
	      console.log('Error: ', error);
	      $scope.startLoad = false;
	  };
	 
	};

	$scope.importarAquivo = function (){
		
		 if ( !isEmpty( $scope.uploadedFile ) ) {
	    	$scope.buttonDisabled = true;
	    	$scope.lerArquivo($scope.uploadedFile);	
	   } 
	}

 	function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

  	return true;
 	};

 	function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, title.toUpperCase() ); 
    }
  }

 	$scope.lerArquivo = function(file){
 		var files = document.getElementById('file-7').files;
 		if(files.length > 0){
 			getBase64View(files[0]);
 		}
 	}

	$scope.close = function(){
		$modalInstance.close();
	}

	inicio();
});

MedicsystemApp.controller('ModalSelectTreeElementController', function ($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window ,tipoTreeElement, elementos) {

	var TIPO_CONTACONTABIL = 1;
	var TIPO_CENTROCUSTO = 2;

	function finalizarModal ( objectTreeSelected ) {
    	$modalInstance.close(objectTreeSelected);
  	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	$scope.definirContaContabil = function( contaContabilSelected ) {
		finalizarModal( contaContabilSelected );
	}

	$scope.definirCentroCusto = function( centroCustoSelected ) {
		finalizarModal( centroCustoSelected );
	}

	$scope.collapseAll = function () {
    	$scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
    	$scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.inicioModal = function() {

    	$scope.focusFieldSearch = true;
    	
		if( tipoTreeElement == TIPO_CONTACONTABIL ) {
			$scope.boShowCContabil = true;
			$scope.contaContabilList = elementos;
		} else if( tipoTreeElement == TIPO_CENTROCUSTO ) {
			$scope.boShowCCusto = true;
			$scope.centroCustoList = elementos;
		}
	};
  
	$scope.inicioModal();

});

MedicsystemApp.controller('modalCadastroContasApagarCtrl', function($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window, $modal, toastr, $ngBootbox, nmDespesa, nmFornecedor, empresa, contaBancaria,doc){

	function inicio(){

		$scope.buttonSalvarDisabled = false;
		$scope.despesaFinanceiro ={};
		$scope.despesaFinanceiro.empresaFinanceiroSolicitante={};
		$scope.despesaFinanceiro.dtVencimento = new Date();
		$scope.despesaFinanceiro.empresaFinanceiroPagante ={idEmpresaFinanceiro: empresa.idEmpresaFinanceiro};
		$scope.despesaFinanceiro.contaBancaria = {id: contaBancaria.id};

		if(nmDespesa){
			$scope.despesaFinanceiro['nmDespesa'] = nmDespesa;
		}
		if(nmFornecedor){
			$scope.despesaFinanceiro.fornecedor = nmFornecedor;
		}
		loadFornecedor();
		loadEmpresaFinanceiroSolicitante();
	}

	function loadEmpresaFinanceiroSolicitante() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaSolicitanteOptions = empresaFinanceiroList;
  			}
  		})
  };

	function finalizarDfEnviar( df ) {
		var STATUS_ABERTO = 0;
		var STATUS_PAGO = 1;
		var TIPO_CONTA_PAGAR = 0;

		df.inTipo = TIPO_CONTA_PAGAR;
		df.inStatus = STATUS_PAGO;
		df.vlDespesa = Math.abs(doc.valor);
		df.dtPagamento = new Date(doc.dataTransacao);
		df.inFormaPagamento = 1;
		df.vlDesconto = 0;
		df.vlJuros = 0;
		df.vlPago = Math.abs(doc.valor);
		return df;
	}

	function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, title.toUpperCase() ); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, title.toUpperCase() ); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, title.toUpperCase() ); 
    }
	};

	function validarDespesaFinanceiro( df ) {
		var isValido = true;
		var msgRequired = " é obrigatória (o).";
		var msgSelect = "Por favor, selecione ";

		if( empresa == undefined) {
			isValido = false;
			messageToastr('erro', msgSelect +'a empresa pagamente','Calma');
		} else if( df.nmDespesa == null || df.nmDespesa.length == 0 ) {
			isValido = false;
			messageToastr('erro','Descrição' + msgRequired,'Calma');
		} else if( contaBancaria == null) {
			isValido = false;
			messageToastr('erro', msgSelect + 'a conta bancária','Calma');
		} else if( !df.contaContabil) {
			isValido = false;
			messageToastr('erro', msgSelect + 'a conta contábil','Calma');
		} else if(  df.dtVencimento == null ) {
			isValido = false;
			messageToastr('erro', 'Data de vencimento' + msgRequired ,'Calma');
		} else if(  df.nrDocumento == null || df.nrDocumento.trim().length == 0 ){
			isValido = false;
			messageToastr('erro', 'Numero do documento' + msgRequired ,'Calma');
		} else if(!df.centroCusto){
			isValido = false;
			messageToastr('erro', msgSelect + ' o centro custo', 'Calma');
		}

		return isValido;
	};

	$scope.salvarDespesaFinanceiro = function( despesaFinanceiro, tipoSaveSelected) {
		$scope.buttonSalvarDisabled = true;

		var isValido = validarDespesaFinanceiro( despesaFinanceiro );

		if( isValido ) {
			toastr.clear();
			var dfEnviada = finalizarDfEnviar( despesaFinanceiro );
			financeiroFactory.despesasFin.save( {tipoFrequencia: null ,ocorrencia:null} ,dfEnviada,
			 	function( resposta,responseHeaders  ) {
					$modalInstance.close(resposta);
			});
		}
	};

	$scope.loadContasContabil = function() {
		$scope.contaContabilList = [];
		var tipoTreeElement_contaContabil = 1;

    financeiroFactory.contaContabil
    .query( function( contasContabil ) {
     	$scope.contaContabilList = contasContabil;
     	prepararModalSelectTreeElement( tipoTreeElement_contaContabil, $scope.contaContabilList );
      })  		
	};

	$scope.loadCentroCusto = function() {
		$scope.centroCustoList = [];
		var tipoTreeElement_centroCusto = 2;

    financeiroFactory.centroCusto
    .query( function( centrosCusto ) {
     	$scope.centroCustoList = centrosCusto;
     	prepararModalSelectTreeElement( tipoTreeElement_centroCusto, $scope.centroCustoList );
      })  		
	};

	function loadFornecedor() {
		$scope.fornecedorOptions = [];
		
		financeiroFactory.fornecedor
		.query( function( fornecedorList) {
			if( fornecedorList.length > 0 )
				$scope.fornecedorOptions = fornecedorList;
		});
	};

	$scope.verificarDocumentoDuplicado = function( nrDocumento ) {

		nrDocumento = nrDocumento ? nrDocumento : "";

		if( nrDocumento.trim().length > 0 ) {
			financeiroFactory.dfByNrDocumento
			.get( {documento:nrDocumento }, function( result ) {
				if( result.length > 0 ) {
					if( !$scope.despesaFinanceiro.id ) {
						$scope.isNrDocumentoDuplicado = true;	
						enviarMensagemDuplicado();	
					} else {
						if( ($scope.nrDocumentoExistente).trim() != ($scope.despesaFinanceiro.nrDocumento).trim() )	
							enviarMensagemDuplicado();
					}
				}
			});
		}
	};

	function enviarMensagemDuplicado() {
		$ngBootbox.alert('Número de documento duplicado !')
		.then(function() {
			$scope.despesaFinanceiro.nrDocumento = '';
			$scope.isNrDocumentoDuplicado = false;	
		});
	};

	function prepararModalSelectTreeElement ( tipoTreeElement, elementos ) {
  		var TIPO_CONTACONTABIL = 1;
  		var TIPO_CENTROCUSTO = 2;

			var modalCadastro = $modal.open({
				animation: true,
	  		templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/contaPagar/contaPagar-modal-selectTreeElement.html',
	  	  controller: 'ModalSelectTreeElementController',
	    	resolve: {
	    		tipoTreeElement: function () {
	        		return tipoTreeElement;
	        	},
	        	elementos: function () {
	        		return elementos;	
	        	}
	      	}
	    });

	  	modalCadastro.result.then(
	   		function (retorno) {
	   			if( tipoTreeElement == TIPO_CONTACONTABIL  ) {
	   				$scope.despesaFinanceiro.contaContabil = retorno;
	   			} else if( tipoTreeElement == TIPO_CENTROCUSTO  ) {
	   				$scope.despesaFinanceiro.centroCusto = retorno;
	   			}
	     	}, 
	    	function () {
	       		console.info('Modal dismissed at: ' + new Date());
	      });
	};

	$scope.close = function(){
		$modalInstance.close();
	}

	inicio();
});


MedicsystemApp.controller('modalBuscarLancamentosCtrl', function($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window,NgTableParams,doc, empresa, showTelaSugestao, showTelaNovaBusc, contaBancaria){

	function loadContaContabil (lista){
		if(lista){
			$scope.listContaContabil = [];
			var obj = {id: -1, nmContaContabil: "TODOS"};
			$scope.listContaContabil.push(obj);
			var listIdsConta = [];
			for (var i = 0; i < lista.length; i++){
				var retorno = listIdsConta.indexOf(lista[i].contaContabil.id);
				
				if (retorno < 0){
					$scope.listContaContabil.push(lista[i].contaContabil);
					listIdsConta.push(lista[i].contaContabil.id);
				}
			};
		}
	}

	$scope.filterContaContabil = function (tipo){
		if(!tipo){
			$scope.idContaContabil = null;
		}else	if (tipo.id == -1){
			$scope.idContaContabil = null;
		}else{
			$scope.idContaContabil = $scope.contaContabil.selected.id;
		}
		buscarDespesas();
	};

	function inicio(){
		$scope.nmDespesa = doc.nmDespesa;
		$scope.valorDespesaExt = Math.abs((doc.valor)).toLocaleString("pt-BR", {minimumFractionDigits: 2});
		$scope.dataFiltro = {};
    $scope.dataFiltro.date = {startDate: moment(), endDate: moment()}; 
    //$scope.maxDate = new Date();
    $scope.listDespesa ={};
    $scope.selecDespesa={};
    $scope.contaContabil ={selected:null};
    buscarDespesas();
    loadContaContabil();
    //$scope.isChecked = false;
	}

 
  $scope.localeDateRangePicker = {
          "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
          "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
          "customRangeLabel": "Customizado", "weekLabel": "S",
          "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
          "monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
          "firstDay": 0
  };
  
  $scope.rangesDateRangePicker =  {
          'Hoje': [moment(), moment()],
          'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
          'Este mês': [moment().startOf('month'), moment().endOf('month')],
          'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
  }

  function buscarDespesas(text){
  	var queryObject = {};
		var filtro = $scope.searchFiltro;
  	var Despesa = financeiroFactory.buscaDespesaFinanceiro;
  	var dataInicio = moment($scope.dataFiltro.date.startDate).format("YYYY-MM-DD").toString();
		var dataFim = moment($scope.dataFiltro.date.endDate).format("YYYY-MM-DD").toString();
		if(text){
			if(text.length>1){
				text = text.toLowerCase();
				if(filtro && filtro.startsWith("!")){
					if(filtro.startsWith("!cod")){
						queryObject = { nrDocumento: text.trim(), idEmpresa: empresa.idEmpresaFinanceiro, dtInicio: dataInicio, dtFim: dataFim, idContaContabil: $scope.idContaContabil, idContaBancaria: contaBancaria.id};
					} else {
						text = null;
					}
				}else {
					queryObject = { nmDespesa: text.trim(), idEmpresa: empresa.idEmpresaFinanceiro, dtInicio: dataInicio, dtFim: dataFim, idContaContabil: $scope.idContaContabil, idContaBancaria: contaBancaria.id};
				}
			}
		}else{
			queryObject = {nmDespesa: null, idEmpresa: empresa.idEmpresaFinanceiro, dtInicio: dataInicio, dtFim: dataFim, idContaContabil: $scope.idContaContabil, idContaBancaria: contaBancaria.id};
		}
		
		var data = Despesa.query(queryObject, function() {
			$scope.listPesquisa = data;
			for(var i=0; i < $scope.listPesquisa.length; i++){
				$scope.listPesquisa[i].dtVencimentoFormatada = moment($scope.listPesquisa[i].dtVencimento).format("DD/MM/YYYY").toString();
				if($scope.listPesquisa[i].dtPagamento != null ){
					$scope.listPesquisa[i].dtPagamentoFormatada = moment($scope.listPesquisa[i].dtPagamento).format("DD/MM/YYYY").toString();	
				}else {
					$scope.listPesquisa[i].dtPagamentoFormatada = null;
				}
			}

			if(doc.lancamentos){
				for(var i=0; i < 	$scope.listPesquisa.length; i++){
	  			for(var j=0; j< doc.lancamentos.length; j++){
	  				if(	$scope.listPesquisa[i].id == doc.lancamentos[j].id){
	  					$scope.listPesquisa[i].isChecked = true;
	  					$scope.listPesquisa.splice(i, 1);
	  				}
	  			}
	  		}
			}

  		if(data){
  			loadContaContabil(data);
  		}
			if (data.length == 0) {
				$scope.showTabela = false;

			} else {
				$scope.showTabela = true;

			}
			configurarTable($scope.listPesquisa);
		});  	
  }
  
  function configurarTable( lista ) {
		var initialParams = { count:10 };
		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
	    	paginationMaxBlocks: 13,
	    	paginationMinBlocks: 2,
	    	dataset: lista
		};

		$scope.tableParams = new NgTableParams(initialParams, initialSettings ) ;
  };

  $scope.changeDateLoad = function(){
  	if($scope.listDespesa.selected == undefined){
  		buscarDespesas();
  	} else{
  		buscarDespesas();
  	}
  }

  $scope.atualizarSelect = function(text){
  	buscarDespesas(text);
  }

 	$scope.selectDespesa = function(item) {
    /*$scope.selecteDespesaFromSearch = item;
    financeiroFactory.despesaFinanceiro.get( {id: item.id}, function(result){
    	$scope.dadosTabela = result;
    	console.log($scope.dadosTabela); 
    });*/
  };

  function getChecked(list) {
  	var arr = [];
  	angular.forEach(list, function(item) {
  		if(item.isChecked) {
  			arr.push(item);
  		}
  	});  	
  	return arr;
  }

  $scope.utilizar = function(selected){
  	var checkedList = getChecked($scope.listPesquisa);
  	$modalInstance.close(checkedList);
  	$state.go('conciliacaoBancaria');
  }
	$scope.close = function(){
		$modalInstance.close();
		$state.go('conciliacaoBancaria');
	}

	inicio();
});

MedicsystemApp.controller('modalNrServico', function($rootScope, $scope, $state, $modalInstance, doc){

	inicio();
	function inicio(){
		$scope.nrServico = null;
		$scope.buttonSalvarDisabled = false;
	}

	$scope.inserirNrServico = function(nrServico){
		$scope.buttonSalvarDisabled=true;
		for(var i=0; i < doc.lancamentos.length;i++){
			if(doc.lancamentos[i].inStatus != 1){
				doc.lancamentos[i].nrServico = nrServico;
			}
		}
		$modalInstance.close(doc);
	} 

	$scope.close = function(){
		$modalInstance.close(doc);
	}

});