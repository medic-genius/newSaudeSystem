'use strict';

MedicsystemApp.controller('CentroCustoController', function($rootScope,$scope,$state,
	$stateParams,$modal,financeiroFactory,NgTableParams,toastr) {
	
	function configurarTable( lista ) {
    if( lista.length > 0 ) {
      var initialParams = { count:10 };
      var initialSettings = {
        counts:[], 
        paginationMaxBlocks: 13,
        paginationMinBlocks: 2,
        dataset: lista
      };

      $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    } else {
      $scope.tableParams = new NgTableParams();  
    }  	
  };

  function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, 'ERRO'); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, 'SUCESSO'); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, 'SUCESSO'); 
    }
  };

  $scope.inativarCentroCusto = function( centroCusto ) {
    financeiroFactory.centroCusto
    .remove( {id:centroCusto.id}, function( result ) {
      messageToastr("informacao","Centro de custo excluído com sucesso.","SUCESSO");
      $scope.loadCentroCustoCadastrado();
    }, function( erros) {
      messageToastr("erro","Erro ao inativar centro de custo.","Ops,");
    } )  
  };

	$scope.prepararModalCadastroCentroCusto = function( centroCustoEditar ) {
		var modalCadastro = $modal.open({
		  animation: true,
  	  templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/centro-custo-cadastro-modal.html',
  	  controller: 'ModalCadastroCentroCusto',
      resolve: {
    	 centroCustoEditar: function () {
	       return centroCustoEditar;
	     },
    	}
  	});

  	modalCadastro.result.then(
  		function (retorno) {
        $scope.loadCentroCustoCadastrado();
      }, 
      function () {
        console.info('Modal dismissed at: ' + new Date());
      });
	};

	$scope.loadCentroCustoCadastrado = function() {
		
    $scope.centroCustoList = [];

    financeiroFactory.centroCusto
    .query( function( centrosCustos ) {
      console.log("result",centrosCustos);
      $scope.centroCustoList = centrosCustos;
      configurarTable( $scope.centroCustoList );
    } )
	};

	function inicio() {
		$scope.loadCentroCustoCadastrado();
	}

  inicio();

});

MedicsystemApp.controller('ModalCadastroCentroCusto', function ($rootScope, $scope, $state, $modalInstance, financeiroFactory, $window,toastr,centroCustoEditar) {

  function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, title); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, title); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, title); 
    }
  };

  function finalizarModal () {
    $modalInstance.close();
  };

  $scope.salvarCentroCusto = function( centroCustoCompleto ) {

    if( centroCustoCompleto[0].id ) {
      financeiroFactory.centroCusto
      .edit( { id: centroCustoCompleto[0].id },centroCustoCompleto[0], function(result) {
          console.log("result",result);
          messageToastr("informacao", "Centro custo alterado com sucesso.", "SUCESSO");
          finalizarModal();
        })
    } else {

       financeiroFactory.centroCusto
      .save( centroCustoCompleto[0], function( result,responseHeaders) {
        
        var location = responseHeaders('Location');
        if ( location ) {
          var locationArray = location.split("/");
          var centroCustoId = locationArray[locationArray.length - 1];
                
          if( centroCustoId ) {
            messageToastr("sucesso", "Centro de custo cadastrado com sucesso.", "SUCESSO");
            finalizarModal();
          }
        }
      } )
    }
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.newSubItem = function (scope) {
    console.log("scope.$modelValue",scope.$modelValue);
    var nodeData = scope.$modelValue;

    if( nodeData.nmCentroCusto.length > 0 ) {
      nodeData.centroCustos.push({
        nrOrdem: nodeData.nrOrdem + '.' + (nodeData.centroCustos.length + 1),
        nmCentroCusto: '',
        centroCustos: []
      });  
    } else {
      messageToastr("erro","Dê uma descrição ao centro de custo.", "CALMA,");
    }     
  };

  $scope.inicioModal = function() {
    $scope.canSearch = null;

    if( !centroCustoEditar ) {
      $scope.data = [{nrOrdem:1,nmCentroCusto:'',centroCustos:[]},];
      $scope.canSearch = false;
    } else {
      console.log("centroCustoEditar",centroCustoEditar);
      $scope.data =  [];
      $scope.data.push(centroCustoEditar);
      $scope.canSearch = true;
    }

		
	};

	$scope.inicioModal();

    	
	$modalInstance.close();	

   

   
});


