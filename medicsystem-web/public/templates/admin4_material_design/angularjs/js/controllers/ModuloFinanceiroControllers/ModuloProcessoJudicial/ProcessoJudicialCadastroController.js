'use strict';

MedicsystemApp.controller('ProcessoJudicialCadastroController', function($rootScope,$scope,$state,
	$stateParams, $modal,financeiroFactory, toastr,Upload, $timeout,$ngBootbox) {

    $scope.verificarDocumentoDuplicado = function( nrDocumento ) {
        
        nrDocumento = nrDocumento ? nrDocumento : "";

        if( nrDocumento.trim().length > 0 ) {
            /* financeiroFactory.dfByNrDocumento
            .get( {documento:nrDocumento }, function( result ) {
                if( result.length > 0 ) {
                    if( !$scope.despesaFinanceiro.id ) {
                        $scope.isNrDocumentoDuplicado = true;	
                        enviarMensagemDuplicado();	
                    } else {
                        if( ($scope.nrDocumentoExistente).trim() != ($scope.despesaFinanceiro.nrDocumento).trim() )	
                            enviarMensagemDuplicado();
                    }
                }
            }); */
        }
    };
        
    function enviarMensagemDuplicado() {
		$ngBootbox.alert('Número de processo duplicado !')
		.then(function() {
			$scope.processoJud.nrDocumento = '';
			$scope.isNrDocumentoDuplicado = false;	
		});
	};

});