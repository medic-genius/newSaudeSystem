'use strict';

MedicsystemApp.controller('ContaContabilController', function($rootScope,$scope,$state,
	$stateParams,financeiroFactory,NgTableParams, $modal,toastr) {
	
	 function configurarTable( lista ) {
      if( lista.length > 0 ) {
        var initialParams = { count:10 };
        var initialSettings = {
          counts:[], 
          paginationMaxBlocks: 13,
          paginationMinBlocks: 2,
          dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
      } else {
        $scope.tableParams = new NgTableParams();  
      }   
  };

  function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, title); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, title); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, title); 
    }
  };

  $scope.inativarContaContabil = function( contaContabilInativar ) {
    financeiroFactory.contaContabil
    .remove( {id:contaContabilInativar.id}, function( result ) {
      messageToastr("informacao","Conta Contábilexcluída com sucesso.","SUCESSO");
      $scope.loadContasContabeisCadastradas();
    }, function( erros) {
      messageToastr("erro","Erro ao inativar conta contábil.","Ops,");
    } )  
  };

	$scope.prepararModalCadastroContaContabil = function( contaContabilEditar ) {
    var modalCadastro = $modal.open({
	    animation: true,
  		templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/contaContabil-cadastro-modal.html',
  	  controller: 'ModalCadastroContaContabil',
    	resolve: {
    		contaContabilEditar: function () {
        return contaContabilEditar;
        },
      }
    });

  	modalCadastro.result.then(
   		function (retorno) {
        $scope.loadContasContabeisCadastradas();
     	}, 
    	function () {
       	console.info('Modal dismissed at: ' + new Date());
      });
	};

	$scope.loadContasContabeisCadastradas = function() {
      $scope.contaContabilList = [];

      financeiroFactory.contaContabil
      .query( function( contasContabil ) {
        $scope.contaContabilList = contasContabil;
        configurarTable( $scope.contaContabilList );
      })
	};

	function inicio() {
		$scope.loadContasContabeisCadastradas();
	}

  inicio();

});

MedicsystemApp.controller('ModalCadastroContaContabil', function ($rootScope, $scope, $state, $modalInstance, financeiroFactory, $window, toastr, contaContabilEditar) {

  function messageToastr(type, message, title) {
    if( type == 'erro' ) {
       toastr.error(  message, title); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, title); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, title); 
    }
  };

  function finalizarModal () {
    $modalInstance.close();
  };

  $scope.salvarContaContabil = function( contaContabilCompleta ) {
  console.log("aqui", contaContabilCompleta[0]);
  if( contaContabilCompleta[0].id ) {
    financeiroFactory.contaContabil
    .edit( { id: contaContabilCompleta[0].id },contaContabilCompleta[0], function(result) {
        console.log("result",result);
        messageToastr("informacao", "Conta contábil alterada com sucesso.", "SUCESSO");
        finalizarModal();
      })
  } else {
    financeiroFactory.contaContabil
    .save( contaContabilCompleta[0], function( result,responseHeaders) {
      
      var location = responseHeaders('Location');
      if ( location ) {
        var locationArray = location.split("/");
        var contaContabilId = locationArray[locationArray.length - 1];
              
        if( contaContabilId ) {
          messageToastr("sucesso", "Conta contábil cadastrada com sucesso.", "SUCESSO");
          finalizarModal();
        }
      }
    } )
  }
};

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.newSubItem = function (scope) {
    var nodeData = scope.$modelValue;

    if( nodeData.nmContaContabil.length > 0 ) {
      nodeData.contasContabil.push({
        nrOrdem: nodeData.nrOrdem + '.' + (nodeData.contasContabil.length + 1),
        nmContaContabil: '',
        contasContabil: []
      });  
    } else {
      messageToastr("erro","Dê uma descrição a conta contábil.", "CALMA");
    }     
  };

   $scope.inicioModal = function() {
    $scope.canSearch = null;

    if( !contaContabilEditar ) {
      $scope.data = [{nrOrdem:1,nmContaContabil:'',contasContabil:[]},];
      $scope.canSearch = false;
    } else {
      console.log("contaContabilEditar",contaContabilEditar);
      $scope.data =  [];
      $scope.data.push(contaContabilEditar);
      $scope.canSearch = true;
    }    
  };

  $scope.inicioModal();

});
