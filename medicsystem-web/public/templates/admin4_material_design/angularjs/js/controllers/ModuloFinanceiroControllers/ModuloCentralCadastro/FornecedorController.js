'use strict';

MedicsystemApp.controller('FornecedorController', function($rootScope,$scope,$state,
	$stateParams,financeiroFactory,NgTableParams, $modal, $ngBootbox,toastr) {

  function messageToastr(type, message, time) {
    if( type == 'erro' ) {
       toastr.error(  message, 'ERRO'); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, 'SUCESSO'); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, 'SUCESSO'); 
    }
  };

	function configurarTable( lista ) {
		var initialParams = { count:10 };
		var initialSettings = {
			counts:[], 
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings);
  };


  $scope.inativarFornecedor = function( fornecedor ) {
    financeiroFactory.fornecedor
    .remove( {id:fornecedor.id}, function( result ) {
      messageToastr("informacao","Fornecedor excluído com sucesso.");
      $scope.loadFornecedoresCadastrados();
    }, function( erros) {
      messageToastr("erro","Erro ao inativar fornecedor.");
    } )  
  };

   $scope.reativarFornecedor = function( fornecedor ) {
    financeiroFactory.fornecedorReativar
    .reativar( {id:fornecedor.id}, function( result ) {
      messageToastr("informacao","Fornecedor reativado com sucesso.");
      $scope.loadFornecedoresCadastrados();
    }, function( erros) {
      messageToastr("erro","Erro ao reativar fornecedor.");
    } )  
  };

	$scope.prepararModalCadastroFornecedor = function( fornecedorEditar ) {
		var modalCadastro = $modal.open({
		animation: true,
		backdrop: 'static',
		size: 'lg',
  	templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/centralCadastro/fornecedor-cadastro-modal.html',
  	controller: 'ModalCadastroFornecedor',
    resolve: {
      fornecedorEditar : function() {
        return fornecedorEditar;
      }
    }
  });

    modalCadastro.result.then(
      function () {
        $scope.loadFornecedoresCadastrados();
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
  	};

  	$scope.loadFornecedoresCadastrados = function() {
  		$scope.fornecedorList = [];

      financeiroFactory.fornecedor
      .query( { inativos: $scope.filtro.boInativo } ,function( fornecedores ) {
        if( fornecedores.length > 0 ) {
          $scope.fornecedorList = fornecedores;
          configurarTable( $scope.fornecedorList );
        }
      });
  	};

  	function inicio() {
      $scope.filtro = {boInativo: false};
  		$scope.loadFornecedoresCadastrados();
  	}

  	inicio();


});

MedicsystemApp.controller('ModalCadastroFornecedor', function ($rootScope, $scope, $state, $modalInstance, financeiroFactory, $window,toastr, fornecedorEditar) {

  var PESSOA_FISICA = 0;
  var PESSOA_JURIDICA = 1;

  function messageToastr(type, message, time) {
    if( type == 'erro' ) {
       toastr.error(  message, 'ERRO'); 
    } else if( type == 'sucesso' ) {
       toastr.success(  message, 'SUCESSO'); 
    } else if( type == 'informacao' ) {
       toastr.info(  message, 'SUCESSO'); 
    }
  };

  function finalizarModal () {
    $modalInstance.close();
  };

  function loadTipoPessoaOption( indexTipoPessoa ) {
    $scope.tipoPessoaOptions = [
      {id:PESSOA_FISICA, nmTipoPessoa: 'FÍSICA'},
      {id:PESSOA_JURIDICA, nmTipoPessoa: 'JURÍDICA'},
    ];

    $scope.inTipoPessoa = {};

    if( indexTipoPessoa == null )
      $scope.inTipoPessoa.selected = $scope.tipoPessoaOptions[0].id;
    else 
      $scope.inTipoPessoa.selected = $scope.tipoPessoaOptions[indexTipoPessoa].id;
  };

	function loadCidadeOption() {
    financeiroFactory.cidades
    .query( function( cidades ) {
        if( cidades ) {
          $scope.cidadeOptions = cidades;
        }
    } )  
  };

  function inicializar() {

    var indexTipoPessoa = null;

    if( fornecedorEditar ) {
      
      $scope.fornecedor = fornecedorEditar;

      if ( fornecedorEditar.nrCpf  )
        indexTipoPessoa = 0;
      else if( fornecedorEditar.nrCnpj)
        indexTipoPessoa = 1;

      $scope.loadBairroOptions( fornecedorEditar.cidade );

    } else 
      $scope.fornecedor = {};

    loadTipoPessoaOption( indexTipoPessoa );  

  };

  function validarFornecedor( fornecedor ) {
    
    var isValido = true;

    //tipo pessoa
    if ( $scope.inTipoPessoa.selected == PESSOA_FISICA) {
      if ( !fornecedor.nrCpf ) {
        isValido = false;
        messageToastr("erro","informe o CPF para pessoa física");
      } 

      fornecedor.nrCnpj = null;

    } else if ( $scope.inTipoPessoa.selected == PESSOA_JURIDICA ) {
      if( !fornecedor.nrCnpj ) {
        isValido = false;
        messageToastr("erro","informe o CNPJ para pessoa jurídica");
      } 

      fornecedor.nrCpf = null;
    }

    if( !fornecedor.nrNumero ) {
      isValido = false;
      messageToastr("erro","Forneça um número para o endereço");
    }


    if( isValido )
      return fornecedor;
    else
      return null;
  
  };

  $scope.cancel = function () {
    toastr.clear();
    $modalInstance.dismiss('cancel');
  };

  $scope.saveFornecedor = function( fornecedor ) {

   fornecedor = validarFornecedor( fornecedor );


    if( fornecedor ) {

      if( !fornecedor.id ) {
        financeiroFactory.fornecedor
        .save(fornecedor,
          function( resposta,responseHeaders  ) {

            var location = responseHeaders('Location');
            if ( location ) {

              var locationArray = location.split("/");
              var fornecedorId = locationArray[locationArray.length - 1];
              
              if( fornecedorId ) {
                $scope.fornecedor = {};
                messageToastr("sucesso", "Fornecedor cadastrado com sucesso.");
                finalizarModal();
              }
            }
          }, function( err ) {
              console.log('err',err);
            });
      } else {
        financeiroFactory.fornecedor
        .edit( { id: fornecedor.id },fornecedor, function(result) {
          messageToastr("informacao", "Fornecedor alterado com sucesso.");
          finalizarModal();
        })
      }

     
    }  
  };

  $scope.loadBairroOptions = function( cidade ) {
    if( cidade ) {
      financeiroFactory.cidadeBairros
      .get( {id:cidade.id }, function( bairros ) {
        if( bairros ) {
          $scope.bairroOptions = bairros;
        }
      });
    }
  };

  function inicioModal() {
    inicializar();
    loadCidadeOption();
	};

  inicioModal();

});

MedicsystemApp.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    autoDismiss: true,
    positionClass: 'toast-top-right',
  });
});
