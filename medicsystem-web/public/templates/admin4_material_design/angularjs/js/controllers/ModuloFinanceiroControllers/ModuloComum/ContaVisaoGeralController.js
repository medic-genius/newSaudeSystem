'use strict';

MedicsystemApp.controller('ContaVisaoGeralController', function($rootScope,$scope,$state,
	$stateParams, $modal,financeiroFactory) {

	function getTotalContasVencidas() {
		financeiroFactory.qtdCard
		.get( {nrIdEmpresa:$scope.empresaFinanceiro.selected.idEmpresaFinanceiro, nrOpcaoConsulta:0} , 
			function( result ) {
			$scope.cardTotal["totalVencidas"] = { qtd: result.id, vlTotal: result.description };
		})
	};
	
	function getTotalContasVencerDataAtual() {
		financeiroFactory.qtdCard
		.get( {nrIdEmpresa: $scope.empresaFinanceiro.selected.idEmpresaFinanceiro ,nrOpcaoConsulta:1}, 
			function( result ) {
			$scope.cardTotal["totalVencerDtAtual"] = { qtd: result.id, vlTotal: result.description };
		})
	};

	function getTotalContasVencerSemana() {
		financeiroFactory.qtdCard
		.get( {nrIdEmpresa:$scope.empresaFinanceiro.selected.idEmpresaFinanceiro,nrOpcaoConsulta:2},
		 	function( result ) {
			$scope.cardTotal["totalVencerSemana"] = { qtd: result.id, vlTotal: result.description };
		})
	};

	function loadGraphContaPorMes() {
		$scope.dadosGrafico = [];
		$scope.contasPorMes = {};
		var qtdArray =[],vlTotalArray=[],mesArray = [];

		financeiroFactory.graficoByEmpresaFinanceiro
		.get( {id:$scope.empresaFinanceiro.selected.idEmpresaFinanceiro} ,function( dados ) {
			if( dados.length > 0 ) {
				dados.forEach(function(valorAtual){
					qtdArray.push(valorAtual.nrQuantidade);
					vlTotalArray.push(valorAtual.nrValorTotalFormatado);
					mesArray.push(valorAtual.nmMes +'/'+valorAtual.nrAno);
				});

				qtdArray =  qtdArray.toString(),
				vlTotalArray = vlTotalArray.toString(),
				mesArray = mesArray.toString(), 

				$scope.contasPorMes.grafico = {qtdArray,vlTotalArray,mesArray};
				$scope.contasPorMes.grafico.id =  'chart' + '1';
				$scope.dadosGrafico.push($scope.contasPorMes);
			}
		});
	};

	function inicializarCards() {
		$scope.cardTotal = {};
		$scope.cardTotal["totalVencidas"] = {qtd:0,vlTotal:0};
		$scope.cardTotal["totalVencerDtAtual"] = {qtd:0,vlTotal:0};
		$scope.cardTotal["totalVencerSemana"] = {qtd:0,vlTotal:0};
	}

	function loadDadosCards() {
		inicializarCards();
		getTotalContasVencerDataAtual();
		getTotalContasVencidas();	
		getTotalContasVencerSemana();
	};

  	function loadEmpresaFinanceiro() {
  		$scope.empresaFinanceiro = {};

		financeiroFactory.empresaFinanceiro
		.query( function( empresaFinanceiroList ) {
			if( empresaFinanceiroList.length > 0 ) {
				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
				$scope.empresaFinanceiro.selected = empresaFinanceiroList[0];
				$scope.canShowData = true;
				loadDadosCards();
				loadGraphContaPorMes();
			}
		} )
	};

	$scope.loadInformacaoDahsBoard = function() {
		loadDadosCards();
		loadGraphContaPorMes();
	};

	$scope.goToContas = function( tipoCard ) {
		$scope.filtroDashboard = {};
		$scope.filtroDashboard.empresaFinanceiro = $scope.empresaFinanceiro.selected;
		$scope.filtroDashboard.nrOpcaoConsulta = tipoCard;

		$state.go('contaPagar-principal',{filtroDashboard:$scope.filtroDashboard});
	}

	function inicio() {
		$scope.canShowData = false;
		inicializarCards();
		loadEmpresaFinanceiro();
	};


	inicio();

});

