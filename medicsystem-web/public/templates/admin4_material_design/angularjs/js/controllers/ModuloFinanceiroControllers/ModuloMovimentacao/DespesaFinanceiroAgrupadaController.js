'use strict';

MedicsystemApp.controller('DespesaFinanceiroAgrupadaController', function($scope, $state,$stateParams,NgTableParams, financeiroFactory, toastr) {

	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:10 };
			var initialSettings = {
				counts:[10,20,50,100], 
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
		}
  	};

  	function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:3000}); 
    };

    function definirContasBancarias ( colecao ) {
    	
    	if ( colecao.length > 0 ) {
    		var nmBanco = colecao[0].nmBanco;
	    	var nmBancoList =  [];
	    	nmBancoList.push( nmBanco);

	    	angular.forEach( colecao, function( item, index) {
	    		if( item.nmBanco != nmBanco && nmBancoList.indexOf( item.nmBanco ) == -1  )
	    			nmBancoList.push(  item.nmBanco );
	    	} )

	    	$scope.nmBancoToReport = nmBancoList.join();
	    	console.log('final', $scope.nmBancoToReport);
	    }
    };

  	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
  			}
  		} )
	  };
	
	$scope.inicializarData = function(){
		var intervalo = getIntervaloDataSelecionada();
	}

	function getIntervaloDataSelecionada (){
		
		if( $scope.dataFiltro.date.startDate != null &&
			$scope.dataFiltro.date.endDate != null ) {

			var intervalo = {};

			var dtInicio = moment( $scope.dataFiltro.date.startDate).format("MM/DD/YYYY");
			var dtFim = moment( $scope.dataFiltro.date.endDate).format("MM/DD/YYYY");

			
			
			intervalo["dtInicio"] = dtInicio;
			intervalo["dtFim"] = dtFim;

			return intervalo;

		} else 
			return null;
	}

	function inicializarData() {
		$scope.dataFiltro.date = {startDate: null, endDate: null};  
		
 	} 


	function pesquisarDespesasByNrServico( nrServico, idEmpresaPagante ) {
		$scope.despesas = [];
		var dtInicio, dtFim;
		if($scope.dataFiltro.date.startDate == null && $scope.dataFiltro.date.endDate  == null){
			dtInicio = null;
			dtFim = null;
		} else {
			dtInicio = moment($scope.dataFiltro.date.startDate).format("YYYY-MM-DD").toString();
			dtFim = moment($scope.dataFiltro.date.endDate).format("YYYY-MM-DD").toString(); 
		}
		var idContaBancaria;
		if($scope.contaBancaria.selected == null){
			idContaBancaria = null;
		}else {
			idContaBancaria = $scope.contaBancaria.selected.id;
		}

		financeiroFactory.despesasPorNrServico
		.get({servico: nrServico, idempresa: idEmpresaPagante, dataInicio: dtInicio, dataFim: dtFim, idContaBancaria: idContaBancaria}, function( despesaAgrupadaDTO ) {
			$scope.despesaDTO = {};
			$scope.despesaDTO.despesas = despesaAgrupadaDTO.despesaFinanceiroServico;
			$scope.despesaDTO.vlTotalDespesasFormatado = despesaAgrupadaDTO.vlTotalDespesasFormatado;
			$scope.despesaDTO.vlTotalDespesasPagasFormatado = despesaAgrupadaDTO.vlTotalDespesasPagasFormatado;
			
			configurarTable( $scope.despesaDTO.despesas );
			definirContasBancarias( $scope.despesaDTO.despesas )		
		});
		
		
	};

	// BEGIN TEMPLATE REPORT
	var teste = function( data ) {
		 // HEADER
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
        doc.text("Report", data.settings.margin.left + 35, 60);
	};

	function footerDocumento( doc, data, totalPagesExp ) {
        var str = "Página " + data.pageCount;
        if (typeof doc.putTotalPages === 'function')
            str = str + " de " + totalPagesExp;

        doc.setFontSize(10);
        doc.text(str, data.settings.margin.left + 700, doc.internal.pageSize.height - 30);

        return doc;
	};

	function hearderDocumento(doc, data, imgData )  {
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');			
		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Despesas por número de serviço  '.toUpperCase() ,{align:'center'});
		doc.writeText(0,170, $scope.empresaFinanceiro.selected.nmFantasia ,{align:'center'});

		return doc;
	};

	function subHeaderDocumento( doc ) {

		var dataAtual = moment().format('DD/MM/YYYY HH:mm:ss');
		
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 200, 'Número de Serviço:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, 200, $scope.nrServico );

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(320, 200, 'Data de emissão:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(440, 200, dataAtual);

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 220, 'Banco:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, 220, $scope.nmBancoToReport );

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 240, 'Total:' );

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, 240,  $scope.despesaDTO.vlTotalDespesasFormatado );

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 260, 'Total Pago:' );

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(160, 260, $scope.despesaDTO.vlTotalDespesasPagasFormatado );

		return doc;
	};

	function gerarTable( doc, colecao, imgData ) {
		var tabela = {};
		tabela.columns = ['Solic.','Fornec.','Descricao','Obs.','Documento','Parcela','Valor', 'Pago', 'Data' ,'C.Custo'/*,'C.Bancária'*/];
		tabela.rows = [];
		colecao.forEach( function( item, index) {	
			var data = [];
			data[0] = item.nmEmpresaSolicitante ? item.nmEmpresaSolicitante.toLowerCase() : '-';
			data[1] = item.nmFornecedor ? item.nmFornecedor.toLowerCase() : '-';
			data[2] = item.nmDescricao ? item.nmDescricao.toLowerCase() : '-';
			data[3] = item.nmObservacao ? item.nmObservacao.toLowerCase() : '-';
			data[4] = item.nrDocumento ? item.nrDocumento.toLowerCase() : '-';
			data[5] = item.nrOrdemParcela+'/'+item.nrQuantidadeParcelas;
			data[6] = item.nrValorFormatado;
			data[7] = item.nrValorPagoFormatado;
			data[8] = item.dtPagamentoFormatado;
			data[9] = item.nmCentroCusto ? item.nmCentroCusto.toLowerCase() : '-';
			data[10] = item.nmEmpresaPagante ? item.nmEmpresaPagante.toLowerCase() : '-';
			/*data[9] = item.nmContaBancaria ? item.nmContaBancaria.toLowerCase() : '-';*/

			tabela.rows.push( data );
		});

    	var totalPagesExp = "{total_pages_count_string}";
		doc.autoTable( tabela.columns, tabela.rows,{
			pageBreak: 'auto',
			margin: {left: 30, top:280, bottom:50},
			styles: {overflow: 'linebreak',columnWidth:'auto'},
			tableWidth: 'auto',
			theme: 'grid',
			addPageContent: function (data) {
				doc = hearderDocumento( doc, data, imgData );	
				doc = subHeaderDocumento( doc);
				doc = footerDocumento( doc, data, totalPagesExp);
		    },
		});

		if (typeof doc.putTotalPages === 'function')
        	doc.putTotalPages(totalPagesExp);
				
		return	doc;
	};	

	function imprimirPdfAgrupado( imgData ) {
		var doc = new jsPDF('p','pt');
		doc = gerarTable( doc, $scope.despesaDTO.despesas, imgData );
		var blob = doc.output('blob');
		var fileURL = URL.createObjectURL(blob);
		window.open(fileURL, '_blank');
	}

	function getImageReport( url, callBack ) {
		var img = new Image();
		img.onError = function() {console.log("Nao pode carregar a imagem " + url);};
		img.onload = function() {callBack(img);};
		img.src = url;
	};

	$scope.gerarPdfAgrupado = function() {
		getImageReport('assets/global/img/medic-lab-logo.jpg',imprimirPdfAgrupado);
	};

	// END TEMPLATE REPORT

	

 	function definirConfigDateRange() {
		$scope.localeDateRangePicker = {
			"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
			"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para", 
			"customRangeLabel": "Customizado", "weekLabel": "S",
			"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
			"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],

			"firstDay": 0
		};

		$scope.rangesDateRangePicker =  {
			/* 'Hoje': [moment(), moment()],
				'Próximos 30 Dias': [ moment(), moment().add(29, 'days')],
				'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],*/
			'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,'month').endOf('month')],
			'Este mês': [moment().startOf('month'), moment().endOf('month')],
			'Esta semana': [moment().startOf('week').add(1, 'days'),moment().endOf('week')],
			'Próximos Mês': [moment().add(1, 'month').startOf('month'), moment().add(1,'month').endOf('month')]
		}
		$scope.autoupdateinput = false;
 	};

	$scope.loadDespesasByNrServico = function( isFirstRequest ) {
		if (  $scope.nrServico.trim().length > 0 && $scope.empresaFinanceiro.selected  ) {
			$scope.nrServico = String($scope.nrServico );
			pesquisarDespesasByNrServico( $scope.nrServico, $scope.empresaFinanceiro.selected.idEmpresaFinanceiro );
		} else
			$scope.despesas = [];
	};

	//carregar conta bancaria
	$scope.loadContasBancarias = function( empresaFinanceiro ) {
  		$scope.contasBancariaOptions = [];
  		$scope.contaBancaria = { selected:null };
  		$scope.empresaFinanceiroId = empresaFinanceiro.idEmpresaFinanceiro;

  		financeiroFactory.efContasBancaria
  		.get( { idEmpresaFinanceiro:empresaFinanceiro.idEmpresaFinanceiro}, function( contasBancaria ) {
  			if( contasBancaria.length > 0 ) {
  				$scope.contasBancariaOptions = contasBancaria;
  			}
  		})
  };


	
	function inicio() {
		$scope.nrServico = '';
		$scope.despesas = [];
		$scope.empresaFinanceiro = {selected:null};
		$scope.dataFiltro = {};
		loadEmpresaFinanceiro();
		inicializarData();
		definirConfigDateRange();
	};

	inicio();


});




