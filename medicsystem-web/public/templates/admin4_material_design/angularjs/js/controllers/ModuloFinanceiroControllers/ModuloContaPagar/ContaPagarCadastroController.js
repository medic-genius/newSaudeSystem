'use strict';

MedicsystemApp.controller('ContaPagarCadastroController', function($rootScope,$scope,$state,
	$stateParams, $modal,financeiroFactory, toastr,Upload, $timeout,$ngBootbox) {

	function messageToastr(type, message, title) {
	    if( type == 'erro' ) {
	       toastr.error(  message, title.toUpperCase() ); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, title.toUpperCase() ); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, title.toUpperCase() ); 
	    }
	};

	function configurarTable( lista ) {
		var initialParams = { count:10 };
		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
	    	paginationMaxBlocks: 13,
	    	paginationMinBlocks: 2,
	    	dataset: lista
		};

		$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};

  	function loadFilesDF( idDespesaFinanceiro ) {
  		financeiroFactory.arquivosDF
  		.upload( {iddespesa:idDespesaFinanceiro}, function( result ) {
  			if( result.arquivos ) {
  				result.arquivos.forEach( function( item, index  ){
  					var dfAnexo = {};
  					dfAnexo.id = item.id;
  					dfAnexo.name = item.nmNome + '.' +item.nmMimeType;
  					dfAnexo.url = item.nmPath;
  					dfAnexo.extensao = item.nmMimeType;
  					$scope.dfAnexos.push( dfAnexo );
  				});
  			}
  		} );
  	};

  	function inicializarDespesaFinanceiro() {
  		if( $scope.dfSelected ) {
  			$scope.despesaFinanceiro = $scope.dfSelected;
  			loadFilesDF( $scope.despesaFinanceiro.id );
  		} else {
  			$scope.despesaFinanceiro = {};
			$scope.despesaFinanceiro["vlDespesa"] = 0;
			$scope.despesaFinanceiro.dtVencimento = new Date();	
			$scope.despesaFinanceiro.empresaFinanceiroPagante = {};
  		}
  		
  	};

  	function inicializarAnexosDespesaFinanceiro() {
  		$scope.dfAnexos = [];	
  	}

  	function loadFornecedor() {
  		$scope.fornecedorOptions = [];
  		
  		financeiroFactory.fornecedor
  		.query( function( fornecedorList) {
  			if( fornecedorList.length > 0 )
  				$scope.fornecedorOptions = fornecedorList;
  		});
  	};

  	function loadFrequenciaOptions() {
  		financeiroFactory.frequenciaOptions
  		.get( function( frequenciaOptions ) {
  			if( frequenciaOptions.length > 0 ) {
  				$scope.frequenciaOptions = frequenciaOptions;	
  			}
  		});	
  	};

	function loadFormasPagamento(){
		financeiroFactory.pagamentoOptions.get(function(pagamentoOptions){
			if(pagamentoOptions.length > 0){
				$scope.formaPagamentoOptions = pagamentoOptions;
			}
		})
	}
	
  	function loadEmpresaFinanceiro() {
  		financeiroFactory.empresaFinanceiro
  		.query( function( empresaFinanceiroList ) {
  			if( empresaFinanceiroList.length > 0 ) {
  				$scope.empresaFinanceiroOptions = empresaFinanceiroList;
  				if( !$scope.dfSelected ) {
  					//$scope.despesaFinanceiro.empresaFinanceiroPagante.idEmpresaFinanceiro = $stateParams.efCommand.selected.idEmpresaFinanceiro;
  					//$scope.loadContasBancariaDeEmpresaFinanceiro($scope.despesaFinanceiro.empresaFinanceiroPagante);
  				} else {
  					$scope.loadContasBancariaDeEmpresaFinanceiro($scope.dfSelected.empresaFinanceiroPagante);
  				}
  			}
  		} )
  	};

  	$scope.loadContasBancariaDeEmpresaFinanceiro = function( empresaFinanceiro ) {
  		$scope.contasBancariaOptions = [];

  		if( !$scope.dfSelected )
  			$scope.despesaFinanceiro.contaBancaria = null; 

  		if( empresaFinanceiro ) {
  			financeiroFactory.efContasBancaria
	  		.get( { idEmpresaFinanceiro:empresaFinanceiro.idEmpresaFinanceiro}, function( contasBancaria ) {
	  			if( contasBancaria.length > 0 ) {
	  				$scope.contasBancariaOptions = contasBancaria;
	  			}
  			} )	
  		} else {

  		}
  		
  	};

  	function prepararModalSelectTreeElement ( tipoTreeElement, elementos ) {
  		var TIPO_CONTACONTABIL = 1;
  		var TIPO_CENTROCUSTO = 2;

		var modalCadastro = $modal.open({
			animation: true,
	  		templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/contaPagar/contaPagar-modal-selectTreeElement.html',
		  	  controller: 'ModalSelectTreeElementController',
	    	resolve: {
	    		tipoTreeElement: function () {
	        		return tipoTreeElement;
	        	},
	        	elementos: function () {
	        		return elementos;	
	        	}
	      	}
	    });

	  	modalCadastro.result.then(
	   		function (retorno) {
	   			if( tipoTreeElement == TIPO_CONTACONTABIL  ) {
	   				$scope.despesaFinanceiro.contaContabil = retorno;
	   			} else if( tipoTreeElement == TIPO_CENTROCUSTO  ) {
	   				$scope.despesaFinanceiro.centroCusto = retorno;
	   			}
	     	}, 
	    	function () {
	       		console.info('Modal dismissed at: ' + new Date());
	      });
	};

	function inicializarControleFluxo() {
		$scope.boControlesFluxo = {};

		if( $scope.dfSelected && $scope.dfSelected.dtPagamento ) {
			$scope.boControlesFluxo.isPago = true;
		} 
	};

	function inicializarDadosPagamento( beginWithValue ) {
		
		if ( $scope.dfSelected &&  $scope.dfSelected.dtPagamento != null ) {
			if( beginWithValue ) {
				$scope.despesaFinanceiro.dtPagamento = $scope.dfSelected.dtPagamento;
				$scope.despesaFinanceiro.vlDesconto = $scope.dfSelected.vlDesconto;
				$scope.despesaFinanceiro.vlJuros = $scope.dfSelected.vlJuros;
				$scope.despesaFinanceiro.vlPago = $scope.dfSelected.vlPago;	
				$scope.despesaFinanceiro.inFormaPagamento = $scope.dfSelected.inFormaPagamento;
			} else {
				$scope.despesaFinanceiro.dtPagamento = null;
				$scope.despesaFinanceiro.vlDesconto = null;
				$scope.despesaFinanceiro.vlJuros = null;
				$scope.despesaFinanceiro.vlPago = null;
				$scope.despesaFinanceiro.inFormaPagamento = null;
			}		
		} else {
			if( beginWithValue ) {
				$scope.despesaFinanceiro.dtPagamento = new Date();
				$scope.despesaFinanceiro.vlDesconto = 0;
				$scope.despesaFinanceiro.vlJuros = 0;
				$scope.despesaFinanceiro.vlPago = ( $scope.despesaFinanceiro.vlDespesa + $scope.despesaFinanceiro.vlJuros ) - $scope.despesaFinanceiro.vlDesconto;
				//$scope.despesaFinanceiro.inFormaPagamento = $scope.despesaFinanceiro.inFormaPagamento;
			} else {
				$scope.despesaFinanceiro.dtPagamento = null;
				$scope.despesaFinanceiro.vlDesconto = null;
				$scope.despesaFinanceiro.vlJuros = null;
				$scope.despesaFinanceiro.vlPago = null;
				$scope.despesaFinanceiro.inFormaPagamento = null;
			}
		}
	};

	function inicializarDadosPeriodo( beginWithValue ) {
		
		if( beginWithValue ) {
			$scope.boControlesFluxo.inTipoFrequencia = null;	
			$scope.boControlesFluxo.qtdOcorrencia = 1;	
		} else {
			$scope.boControlesFluxo.inTipoFrequencia = null;	
			$scope.boControlesFluxo.qtdOcorrencia = null;	
		}
	};

	function validarDespesaFinanceiro( df ) {
		var isValido = true;
		var msgRequired = " é obrigatória (o).";
		var msgSelect = "Por favor, selecione ";

		if( !df.empresaFinanceiroPagante &&  $scope.boControlesFluxo.isPago ) {
			isValido = false;
			messageToastr('erro', msgSelect +'a empresa pagamente','Calma');
		} else if( df.nmDespesa == null || df.nmDespesa.length == 0 ) {
			isValido = false;
			messageToastr('erro','Descrição' + msgRequired,'Calma');
		} else if( !df.contaBancaria &&  $scope.boControlesFluxo.isPago ) {
			isValido = false;
			messageToastr('erro', msgSelect + 'a conta bancária','Calma');
		} else if( !df.contaContabil) {
			isValido = false;
			messageToastr('erro', msgSelect + 'a conta contábil','Calma');
		} else if(  df.dtVencimento == null ) {
			isValido = false;
			messageToastr('erro', 'Data de vencimento' + msgRequired ,'Calma');
		} else if(  df.vlDespesa == null ) {
			isValido = false;
			messageToastr('erro', 'Valor da despesa' + msgRequired ,'Calma');
		} else if(  df.nrDocumento == null || df.nrDocumento.trim().length == 0 ){
			isValido = false;
			messageToastr('erro', 'Numero do documento' + msgRequired ,'Calma');
		}  else if( df.inFormaPagamento == null &&  $scope.boControlesFluxo.isPago ){
			messageToastr('erro', msgSelect + 'a forma de pagamento', 'Calma');
			isValido = false;
		} else if(!df.centroCusto){
			isValido = false;
			messageToastr('erro', msgSelect + ' o centro custo', 'Calma');
		}

		return isValido;
	};

	function finalizarDfEnviar( df ) {
		var STATUS_ABERTO = 0;
		var STATUS_PAGO = 1;
		var TIPO_CONTA_PAGAR = 0;
		
		df.inTipo = TIPO_CONTA_PAGAR;

		if( $scope.boControlesFluxo.isPago  )
			df.inStatus = STATUS_PAGO;
		else {
			df.inStatus = STATUS_ABERTO;
			df.empresaFinanceiroPagante = null;
			df.contaBancaria = null;
		}

		df.empresaFinanceiroSolicitante = $stateParams.efCommand.selected;

		return df;

	}

	$scope.$watch('boControlesFluxo.isPago', function() {
		if( $scope.boControlesFluxo ) {
			if( !$scope.boControlesFluxo.isPago) {
				inicializarDadosPagamento(false);			
			} else {
				inicializarDadosPagamento(true);			
			}
		}
	});

	$scope.$watch('despesaFinanceiro.nmDespesa', function() {
		if( $scope.despesaFinanceiro && $scope.despesaFinanceiro.nmDespesa ) {
			if( $scope.despesaFinanceiro.nmDespesa.length > 200 )
				$scope.despesaFinanceiro.nmDespesa = $scope.despesaFinanceiro.nmDespesa.slice(0,200);
		}
	});

	$scope.$watch('despesaFinanceiro.nmObservacao', function() {
		if( $scope.despesaFinanceiro && $scope.despesaFinanceiro.nmObservacao ) {
			if( $scope.despesaFinanceiro.nmObservacao.length > 200 ) 
				$scope.despesaFinanceiro.nmObservacao = $scope.despesaFinanceiro.nmObservacao.slice(0,200);
		}
	});

	function enviarMensagemDuplicado() {
		$ngBootbox.alert('Número de documento duplicado !')
		.then(function() {
			$scope.despesaFinanceiro.nrDocumento = '';
			$scope.isNrDocumentoDuplicado = false;	
		});
	};

	$scope.$watch('boControlesFluxo.isPeriodo', function() {
		if( $scope.boControlesFluxo ) {
			if(!$scope.boControlesFluxo.isPeriodo) {
				inicializarDadosPeriodo(false);			
			} else {
				inicializarDadosPeriodo(true);			
			}
		}
	});

	$scope.calcularValorFinal = function() {
		$scope.despesaFinanceiro.vlPago = ( $scope.despesaFinanceiro.vlDespesa + $scope.despesaFinanceiro.vlJuros ) - $scope.despesaFinanceiro.vlDesconto;
	}

	$scope.verificarDocumentoDuplicado = function( nrDocumento ) {

		nrDocumento = nrDocumento ? nrDocumento : "";

		if( nrDocumento.trim().length > 0 ) {
			financeiroFactory.dfByNrDocumento
			.get( {documento:nrDocumento }, function( result ) {
				if( result.length > 0 ) {
					if( !$scope.despesaFinanceiro.id ) {
						$scope.isNrDocumentoDuplicado = true;	
						enviarMensagemDuplicado();	
					} else {
						if( ($scope.nrDocumentoExistente).trim() != ($scope.despesaFinanceiro.nrDocumento).trim() )	
							enviarMensagemDuplicado();
					}
				}
			});
		}
	};

	$scope.preLoadFiles = function( files, errFiles ) {
		if( files ) {
			$scope.dfAnexos = $scope.dfAnexos.concat( files );	 	
        	if(  errFiles ) 
        		$scope.dfAnexos = $scope.dfAnexos.concat(errFiles);	
		}
	};

	function definirArquivosToSend( files ) {
		var arquivosToSend = [];
		files.forEach( function( item, index ) {
			if ( !item.id ) {
				arquivosToSend.push( item );
			}
		});

		return arquivosToSend;
	};

	function atualizarListaArquivos( file ) {
		$scope.dfAnexos.forEach(function(item,index,array) {
			if( item == file) {
				array.splice(index,1);
			}
		}) 	
	};


	$scope.uploadFiles2 = function( idDespesaFinanceiro, files, tipoSaveSelected, isSave ) {

		var arquivos = definirArquivosToSend( files );

		if( arquivos.length > 0 ) {

			var sucesso = 0;

			angular.forEach(arquivos, function(file) {
	            file.upload = Upload.upload({
	                url: 'mediclab/despesafinanceiro/'+ idDespesaFinanceiro +'/upload',
	                data: {file: file}
	            });

	            file.upload.then(function (response) {
	                $timeout(function () {
	                    file.result = response.data;
	                    sucesso++;

	                    if( sucesso == arquivos.length ) {
                   			if( isSave) {
                   		//		messageToastr("sucesso", "Despesa cadastrada com sucesso!", "SUCESSO");
			            		if( tipoSaveSelected == 0 ) {
									//$state.go('contaPagar-principal', {efReferencia:$stateParams.efCommand.selected, objectSearch: $stateParams.objectSearch});
								} else if( tipoSaveSelected == 2  ) {
									inicio();
								} 			
                   			} else {
                   		//		messageToastr("informacao", "Despesa alterada com sucesso.","SUCESSO");
		          				//$state.go('contaPagar-principal', {efReferencia:$stateParams.efCommand.selected, objectSearch: $stateParams.objectSearch });
                   			}
	                    }
	                });
	            }, function (response) {
	                if (response.status > 0) {
	                	$scope.errorMsg = response.status + ': ' + response.data;
	                	file.progress = 0;
	                }
	            }, function (evt) {
	                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        });	
		} else {
			if( isSave) {
   				//messageToastr("sucesso", "Despesa cadastrada com sucesso!", "SUCESSO");
        		if( tipoSaveSelected == 0 ) {
					//$state.go('contaPagar-principal', {efReferencia:$stateParams.efCommand.selected, objectSearch: $stateParams.objectSearch });
				} else if( tipoSaveSelected == 2  ) {
					inicio();
				} 			
   			} else {
   			//	messageToastr("informacao", "Despesa alterada com sucesso.","SUCESSO");
  				//$state.go('contaPagar-principal', {efReferencia:$stateParams.efCommand.selected, objectSearch: $stateParams.objectSearch });
   			}
		}
	};

	$scope.excluirAnexo = function( file ) {
		
		if( file.id ) {
			financeiroFactory.arquivoDfDeletar
			.deletar( { id: file.id }, function( result ) {
				if( result ) {
					atualizarListaArquivos( file );
					messageToastr('sucesso','Arquivo excluido com sucesso','Successo');
				}
			}, function(err){
				messageToastr('erro','Erro ao excluir','Erro');
			});	
		} else {
			atualizarListaArquivos( file );
			messageToastr('sucesso','Arquivo excluido com sucesso','Successo');	
		}
	};

	$scope.backToPrincipal = function() {
		$state.go('contaPagar-principal', {efReferencia:$stateParams.efCommand.selected, objectSearch: $stateParams.objectSearch });
	}

	$scope.salvarDespesaFinanceiro = function( despesaFinanceiro, tipoSaveSelected) {
		
		
		var isValido = validarDespesaFinanceiro( despesaFinanceiro );

		if( isValido ) {
			$scope.buttonDisabled = true;
			toastr.clear();
			var dfEnviada = finalizarDfEnviar( despesaFinanceiro );
			if( dfEnviada.id ) {
				
				financeiroFactory.despesaFinanceiro
		        .edit( { id: dfEnviada.id,tipoFrequencia: $scope.boControlesFluxo.inTipoFrequencia ,ocorrencia: $scope.boControlesFluxo.qtdOcorrencia} ,dfEnviada,
		        	function(result) {
		        		var dfEnviadaArray=[];
		        		dfEnviadaArray.push(dfEnviada.id);
		        		loadModalQrCode(dfEnviadaArray,null, false, $stateParams.efCommand.selected, $stateParams.objectSearch);
		        		$scope.uploadFiles2( dfEnviada.id, $scope.dfAnexos, null, false );
		        })
			} else {
				console.log("dfEnviada", dfEnviada);
				financeiroFactory.despesasFin.save( {tipoFrequencia: $scope.boControlesFluxo.inTipoFrequencia ,ocorrencia: $scope.boControlesFluxo.qtdOcorrencia} ,dfEnviada,
				 	function( resposta) {
				 		var dfSalvaIdArray = [];
				 		if(resposta) {
				 			angular.forEach(resposta, function(item) {
				 				dfSalvaIdArray.push(item.id);
				 			});
				 		}
				 		if ( dfSalvaIdArray.length > 0 ) {
							loadModalQrCode(dfSalvaIdArray,tipoSaveSelected, true, $stateParams.efCommand.selected, $stateParams.objectSearch );
							$scope.uploadFiles2( dfSalvaIdArray[0], $scope.dfAnexos, tipoSaveSelected, true );
						}
	            		/*var location = responseHeaders('Location');
	            		console.log(location);
	            		if ( location ) {
	              			var locationArray = location.split("/");
	              			console.log("locationArray", locationArray);
	              			var dfSalvaId = locationArray[locationArray.length - 1];
	              			var dfSalvaIdArray = [];

	              			for(var i = 0; i < locationArray.length; i++){
	              				if(locationArray[i] && !isNaN(locationArray[i])) {

	              					dfSalvaIdArray.push(locationArray[i]);
	              				}
	              			}
	              
			            	if ( dfSalvaIdArray.length > 0 ) {
			            		loadModalQrCode(dfSalvaIdArray,tipoSaveSelected, true, $stateParams.efCommand.selected, $stateParams.objectSearch );
			            		$scope.uploadFiles2( dfSalvaIdArray[0], $scope.dfAnexos, tipoSaveSelected, true );
			              }
	           		 	}*/
	      }, function( err ) {
	              	console.log('err',err);
	          	}) 
			};
		}
	};

  	$scope.loadContasContabil = function() {
		$scope.contaContabilList = [];
		var tipoTreeElement_contaContabil = 1;

	    financeiroFactory.contaContabil
	    .query( function( contasContabil ) {
	     	$scope.contaContabilList = contasContabil;
	     	prepararModalSelectTreeElement( tipoTreeElement_contaContabil, $scope.contaContabilList );
	      })  		
  	};

  	$scope.loadCentroCusto = function() {
  		$scope.centroCustoList = [];
		var tipoTreeElement_centroCusto = 2;

	    financeiroFactory.centroCusto
	    .query( function( centrosCusto ) {
	     	$scope.centroCustoList = centrosCusto;
	     	prepararModalSelectTreeElement( tipoTreeElement_centroCusto, $scope.centroCustoList );
	      })  		
  	};

  	$scope.definirCentroCusto = function( centroCusto ) {
  		$scope.despesaFinanceiro.contaContabil = centroCusto;
	  };




	function inicio() {	
		$scope.inFormaPagamento = {};
		$scope.dateOptions = {formatYear: 'yyyy',startingDay: 0};
		$scope.format = 'dd/MM/yyyy';
		$scope.buttonDisabled = false;
		inicializarAnexosDespesaFinanceiro();
		inicializarDespesaFinanceiro();
		loadEmpresaFinanceiro();
		loadFornecedor();
		loadFrequenciaOptions();
		inicializarControleFluxo();
		loadFormasPagamento();
		
	};

	if( $state.is('contaPagar-cadastro') && $stateParams.efCommand ){
		$scope.dfSelected = null;
		if( $stateParams.despesaFinanceiroSelected ) {
			$scope.dfSelected =  $stateParams.despesaFinanceiroSelected;
			$scope.nrDocumentoExistente = $scope.dfSelected.nrDocumento ? $scope.dfSelected.nrDocumento : "" ;	
		}
			

		inicio();
	};


	function loadModalQrCode (listIds, tipoSaveSelected, isSave, stateParams, stateParams2){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/financeiro/contaPagar/modal/contaPagar-modal-qrCode.html',
			controller: 'modalQrCodeCtrl',
			size: 'sm',
			resolve:{
				listIds: function(){
					return listIds;
				},
				tipoSaveSelected: function(){
					return tipoSaveSelected;
				}, 
				isSave: function(){
					return isSave;
				},
				stateParams: function(){
					return $stateParams.efCommand.selected;
				}, 
				stateParams2: function(){
					return $stateParams.objectSearch;
				}
			}
		});
	}

});


MedicsystemApp.controller('ModalSelectTreeElementController', function ($rootScope, $scope, $state,
  $modalInstance, financeiroFactory, $window ,tipoTreeElement, elementos) {

	var TIPO_CONTACONTABIL = 1;
	var TIPO_CENTROCUSTO = 2;

	function finalizarModal ( objectTreeSelected ) {
    	$modalInstance.close(objectTreeSelected);
  	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	$scope.definirContaContabil = function( contaContabilSelected ) {
		finalizarModal( contaContabilSelected );
	}

	$scope.definirCentroCusto = function( centroCustoSelected ) {
		finalizarModal( centroCustoSelected );
	}

	$scope.collapseAll = function () {
    	$scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
    	$scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.inicioModal = function() {

    	$scope.focusFieldSearch = true;
    	
		if( tipoTreeElement == TIPO_CONTACONTABIL ) {
			$scope.boShowCContabil = true;
			$scope.contaContabilList = elementos;
		} else if( tipoTreeElement == TIPO_CENTROCUSTO ) {
			$scope.boShowCCusto = true;
			$scope.centroCustoList = elementos;
		}
	};
  
	$scope.inicioModal();

});

MedicsystemApp.controller('modalQrCodeCtrl', function ($rootScope, $scope, $state, $timeout, 
  $modalInstance, financeiroFactory, listIds, tipoSaveSelected, isSave, toastr,
  stateParams, stateParams2) { 

	function inicio(){
		makeQrCode(listIds);
	}

	function makeQrCode(listIds){
		$timeout(function() {
			var HOST_SERVER = "http://li1076-46.members.linode.com/mediclab"
			//var HOST_SERVER = "http://0ab103ef.ngrok.io/main"
	  	var elemento = document.getElementById('qrcode');
	  	var params = [];
	  	for(var i=0; i < listIds.length; i++){
	  			params.push("iddespesa=" +listIds[i]);
	  		
	  	}

	  	var stringParams = params.toString();
	  	while(stringParams.includes(',')){
	  		stringParams = stringParams.replace(',','&');
	  	}
	  	
	  	var url = HOST_SERVER+"/despesafinanceiro/anexos?" + stringParams;
	  	var qrcode = new QRCode(elemento, url, {
				width: 210,
				height: 210,
				colorDark : "#000000",
				colorLight : "#ffffff",
				correctLevel : QRCode.CorrectLevel.H
			});
		}, 300);
	}

	$scope.close = function(){
		if(isSave){
				messageToastr("sucesso", "Despesa cadastrada com sucesso!", "SUCESSO");
				if( tipoSaveSelected == 0 ) {
					$modalInstance.close();
					$state.go('contaPagar-principal', {efReferencia:stateParams, objectSearch: stateParams2});
				} else if (tipoSaveSelected == 2){
					$modalInstance.close();
					$state.go('contaPagar-principal');
				}
		} else {
			messageToastr("informacao", "Despesa alterada com sucesso.", "SUCESSO");
			$modalInstance.close();
			$state.go('contaPagar-principal', {efReferencia:stateParams, objectSearch: stateParams2 });
		}
	};

	$scope.ok = function(){
		if(isSave){
				messageToastr("sucesso", "Despesa cadastrada com sucesso!", "SUCESSO");
				if( tipoSaveSelected == 0 ) {
					$modalInstance.close();
					$state.go('contaPagar-principal', {efReferencia:stateParams, objectSearch: stateParams2});
				} else if (tipoSaveSelected == 2){
					$modalInstance.close();
					$state.go('contaPagar-principal');
				}
		} else {
			messageToastr("informacao", "Despesa alterada com sucesso.", "SUCESSO");
			$modalInstance.close();
			$state.go('contaPagar-principal', {efReferencia:stateParams, objectSearch: stateParams2 });
		}

	}

	function messageToastr(type, message, title) {
	    if( type == 'erro' ) {
	       toastr.error(  message, title.toUpperCase() ); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, title.toUpperCase() ); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, title.toUpperCase() ); 
	    }
	};


	inicio();

});


MedicsystemApp.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    autoDismiss: true,
    positionClass: 'toast-top-right',
  });
});



