'use strict';

MedicsystemApp.controller('ProcessoJudicialController', function($rootScope,$scope,$state,
	$stateParams,financeiroFactory,Auth, NgTableParams,toastr) {
    
    inicio();

    function inicio(){
		$scope.empresa = {};
		$scope.cssStyleObject = {};

		inicializarColorFiltroStatus();
	
		$scope.inStatusFiltro = null;

		inicializarData();

		definirConfigDateRange();

    };

    function inicializarColorFiltroStatus() {
		$scope.cssStyleObject.color1 = 'default';
		$scope.cssStyleObject.color2 = 'default';
		$scope.cssStyleObject.color3 = 'default';
    };
    
    function inicializarData() {
        var dataAtual = moment();
  
        $scope.dataFiltro = {};

        if( $stateParams.objectSearch == null )
             $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};  
        else
             $scope.dataFiltro.date = {startDate: moment( $stateParams.objectSearch.intervalo.dtInicio, 'DD/MM/YYYY' ), endDate: moment( $stateParams.objectSearch.intervalo.dtFim, 'DD/MM/YYYY' )};  
    }

    function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	'Hoje': [moment(), moment()],
	   		'Próximos 30 Dias': [ moment(), moment().add(29, 'days')],
	   		'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
			'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()],
			'Próximos Mês': [moment().startOf('month'), moment().add(2, 'month').endOf('month')],
			'Próximos 2 Meses': [moment().startOf('month'), moment().add(3, 'month').endOf('month')]
		}
	};
	
	function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
	};
	
	$scope.goToCadastrarProcesso  = function(dados){
		console.log("dados", dados);
		var intervalo = getIntervaloDataSelecionada();
		//var objectSearch = { empresaSelecionada: $scope.empresa.selected, intervalo: intervalo};
		if( dados == null){
			$state.go('processoJudicial-cadastro', {});
		} else {
			$state.go('processoJudicial-cadastro', {});
		}
					
			
		/* 	if( despesaFinanceiro == null )
				$state.go('contaPagar-cadastro',{nmEmpresaFinanceiro:$scope.empresaFinanceiro.selected.nmFantasia,efCommand:$scope.empresaFinanceiro,despesaFinanceiroSelected:null,objectSearch:objectSearch});
			else
				$state.go('contaPagar-cadastro',{nmEmpresaFinanceiro:$scope.empresaFinanceiro.selected.nmFantasia,efCommand:$scope.empresaFinanceiro,despesaFinanceiroSelected:despesaFinanceiro,objectSearch:objectSearch});
		}; */
	}
});