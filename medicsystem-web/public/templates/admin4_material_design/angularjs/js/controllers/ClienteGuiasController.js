'use strict';

MedicsystemApp.controller('ClienteGuiasController',
    function($scope, servicesFactory, $state, NgTableParams)
    {
    	function retrieve()
        {
        	if($scope.selectedCliente && $scope.selectedCliente.id)
            {
        		servicesFactory.getGuias.get({id_cliente: $scope.selectedCliente.id}, function(data)
                {
                    $scope.lGuias = data;
                    if($scope.lGuias.hasOwnProperty('data'))
                    {
                        var formatedList = formatList($scope.lGuias.data);
        		    	setTable(formatedList);
                    }
    		    });
        	}
        }

        function formatList(lGuiasOfRest)
        {
            var lGuiasClean = [];
            for (var i = 0 ; i < lGuiasOfRest.length ; i++)
            {
                var itemRow = lGuiasOfRest[i];
                lGuiasOfRest[i].validateFormated = moment(lGuiasOfRest[i].dtValidade).format('DD/MM/YYYY');
                if(lGuiasOfRest[i].dependente)
                {
                    itemRow.titularidade = "Dependente";
                    itemRow.beneficiario = {name: lGuiasOfRest[i].dependente.nmDependente};
                } else
                {
                    itemRow.titularidade = "Titular";
                    itemRow.beneficiario = {name: lGuiasOfRest[i].cliente.nmCliente};
                }
                lGuiasClean[i] = itemRow;
            }
            return lGuiasClean;
        }

    	function setTable(list)
        {
    		var initialParams = { count:10 };
    		var initialSettings =
            {
    			counts:[],
            	paginationMaxBlocks: 13,
            	paginationMinBlocks: 2,
            	dataset: list
    		};
        	$scope.tableParams = new NgTableParams(initialParams, initialSettings )
      	};

        if($state.is('atendimentoCliente.guias')){
            retrieve();
        }
    }
);
