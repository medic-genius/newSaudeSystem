'use strict';

MedicsystemApp.controller
(
    'ConveniadaFaturamentoDetailController',
    function ($scope, itemSummary, $modalInstance, callWhileInstantiating)
    {
    	var callWhileInstantiating = eval(callWhileInstantiating);

    	callWhileInstantiating();

        $scope.itemSummary = itemSummary;

        function prepareData()
        {
            $scope.lDetailServico = [];
            var node;
            var nodeDespesa;
            var indexToPutDespesa;
            for (var i = 0; i < itemSummary.listDespesaServico.length; i++)
            {
                node = {};
                nodeDespesa = [];
                nodeDespesa.idDespesa = itemSummary.listDespesaServico[i].idDespesa;
                nodeDespesa.dateFormated = moment(itemSummary.listDespesaServico[i].dtVencimento).format('DD/MM/YYYY');
                nodeDespesa.nmCliente = itemSummary.listDespesaServico[i].nmCliente;
                nodeDespesa.nmPaciente = ((itemSummary.listDespesaServico[i].nmDependente != null) ? itemSummary.listDespesaServico[i].nmDependente : itemSummary.listDespesaServico[i].nmCliente);
                nodeDespesa.idServico = itemSummary.listDespesaServico[i].idServico;
                nodeDespesa.nrContrato = itemSummary.listDespesaServico[i].nrContrato;
                nodeDespesa.vlServico = itemSummary.listDespesaServico[i].vlServico;
                if(nodeServicoExist(itemSummary.listDespesaServico[i]) < 0)
                {
                    node.nmServico = itemSummary.listDespesaServico[i].nmServico;
                    node.idServico = itemSummary.listDespesaServico[i].idServico;
                    node.lDespesas = [];
                    node.vlTotal = 0;
                    $scope.lDetailServico[$scope.lDetailServico.length] = node;
                }
                indexToPutDespesa = nodeServicoExist(itemSummary.listDespesaServico[i]);
                $scope.lDetailServico[indexToPutDespesa].lDespesas.push(nodeDespesa);
                $scope.lDetailServico[indexToPutDespesa].vlTotal += nodeDespesa.vlServico;
            }
        }

        function nodeServicoExist(servico)
        {
            for (var i = 0; i < $scope.lDetailServico.length; i++)
            {
                if($scope.lDetailServico[i].idServico == servico.idServico)
                {
                    return i;
                }
            }
            return -1;
        }

        $scope.print = function()
        {
            window.print();
        };

        $scope.cancel = function()
        {
            $modalInstance.dismiss('cancel');
        };

        function constructorController()
        {
            prepareData();
        }

        function prepareDataToPrint()
        {
            var data = [];
            var node = {};
            for (var i = 0; i < itemSummary.listDespesaServico.length; i++)
            {
                node = {};
                node = itemSummary.listDespesaServico[i];
                node.nmPaciente = ((itemSummary.listDespesaServico[i].nmDependente != null) ? itemSummary.listDespesaServico[i].nmDependente : itemSummary.listDespesaServico[i].nmCliente);
                if("string" == (typeof node.dtVencimento) && node.dtVencimento.includes("-"))
                {
                    node.dtVencimento = moment(node.dtVencimento, 'YYYY-MM-DD').toDate();
                    node.dtVencimento = moment(node.dtVencimento).format('DD/MM/YYYY');
                }
                node.vlServicoFormated = "R$ " + Number.parseFloat(node.vlServico).toFixed(2);
                data.push(node);
            }
            return data;
        }

        function getTotalCost()
        {
            var total = 0;
            for (var i = 0; i < itemSummary.listDespesaServico.length; i++)
            {
                total += itemSummary.listDespesaServico[i].vlServico
            }
            return total;
        }

        $scope.getReportPdf = function()
        {
            var doc = new jsPDF('portrait','pt','letter','a4');
            doc.setFont("times");
            doc.setFontType('bold');
            doc.setFontSize(12);
            doc.writeText(0,50, "Relatório de consumo - " + itemSummary.nmEmpresaCliente, {align:'center'});
            doc.setFontSize(7);
            doc.setFontType('normal');
            doc.writeText(60, 75, 'Total Serviços: ' + prepareDataToPrint().length + '         Valor Total: R$ ' + getTotalCost().toFixed(2));

			doc.autoTable(getColumns(), prepareDataToPrint(),{
				startY: 80,
				margin: {horizontal: 40},
				bodyStyles: {valign: 'top', fontSize: 7},
				styles: {overflow: 'linebreak'},
				columnStyles: {contac:{overflow:'linebreak', fontSize: 7}},
			});
			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');
        }

        var getColumns = function () {
            return [
                {title: "Data", dataKey: "dtVencimento"},
                {title: "Despesa", dataKey: "idDespesa"},
                {title: "Contrato", dataKey: "nrContrato"},
                {title: "Paciente", dataKey: "nmPaciente"},
                {title: "Serviço", dataKey: "nmServico"},
                {title: "Valor", dataKey: "vlServicoFormated"},
            ];
        };
    }
).value('callWhileInstantiating', "constructorController");;
