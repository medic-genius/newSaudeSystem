'use strict';

MedicsystemApp.component('reportDetailVidas', {
    templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/report-detail-vidas.component.html',
    controller: function (servicesFactory, $scope, NgTableParams)
    {
        var self = this;

        this.$onInit=function(){
            constructorController();
        };

        function constructorController()
        {
            $scope.lShowing = {};
            prepareData();
            $scope.page = 0;
            $scope.limitLinksToPagination = 5;
        }

        function isTitular(node)
        {
            return prepareRow(node).titular;
        }

        function isNotTitular(node)
        {
            return !prepareRow(node).titular;
        }

        function prepareData()
        {
            $scope.patient = {list: [], listPaginated: [], vlTotal: 0};
            var titulares = self.lVidas.filter(isTitular);
            for (var i = 0; i < titulares.length; i++)
            {
                $scope.patient.list.push(prepareRow(titulares[i]));
                $scope.patient.vlTotal += titulares[i].vlContrato;
            }
            var dependentes = self.lVidas.filter(isNotTitular);
            for (var i = 0; i < dependentes.length; i++)
            {
                putDependente(dependentes[i]);
                $scope.patient.vlTotal += dependentes[i].vlContrato;
            }
            $scope.patient.listPaginated = pupulateListPaginated($scope.patient.list);
        }

        function putDependente(node)
        {
            var elementPos = $scope.patient.list.map(function(x){return x.nrContrato; }).indexOf(node.nrContrato);
            if(-1 != elementPos)
            {
                $scope.patient.list[elementPos].hasChild = true;
                $scope.patient.list.splice((elementPos + 1), 0, prepareRow(node));
            }
        }

        function prepareRow(node)
        {
            var nodeToView = {
                nmPlano: node.nmPlano,
                nrContrato: node.nrContrato,
                vlContrato: node.vlContrato,
                name: node.nmCliente,
                titular: true,
                nmTitular: node.nmCliente,
                nmDependente: node.nmDependente,
            };
            if(undefined != node.nmDependente && null != node.nmDependente && "" != node.nmDependente)
            {
                nodeToView.name = node.nmDependente;
                nodeToView.titular = false;
            }
            return nodeToView;
        }

        $scope.showChildrenContrato = function(nrContrato)
        {
            $scope.lShowing[nrContrato] = true;
        }

        $scope.hideChildrenContrato = function(nrContrato)
        {
            $scope.lShowing[nrContrato] = false;
        }

        $scope.getPdfReportDetailVidads = function()
        {
            var doc = new jsPDF('portrait','pt','letter','a4');
            doc.setFont("times");
            doc.setFontType('bold');
            doc.setFontSize(12);
            doc.writeText(0,50, "Relatório de vidas - " + self.company, {align:'center'});
            doc.setFontSize(7);
            doc.setFontType('normal');
            doc.writeText(60, 75, 'Total de vidas: ' + getDataToPrint().length + '         Valor Total: R$ ' + Number.parseFloat(getTotalCost()).toFixed(2) );

			doc.autoTable(getColumns(), getDataToPrint(),{
				startY: 80,
				margin: {horizontal: 40},
				bodyStyles: {valign: 'top', fontSize: 7},
				styles: {overflow: 'linebreak'},
				columnStyles: {contac:{overflow:'linebreak', fontSize: 7}},
			});
			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');
        }

        function getDataToPrint()
        {
            var listToPrint = [];
            for (var i = 0; i < $scope.patient.list.length; i++)
            {
                var node = $scope.patient.list[i];
                if(node.titular)
                {
                    node.vlContratoPretty = "R$ " + Number.parseFloat(node.vlContrato).toFixed(2);
                } else
                {
                    node.name = "       " + node.name;
                    node.nmPlano = "";
                    node.nrContrato = "";
                    if(0 == node.vlContrato)
                    {
                        node.vlContratoPretty = "";
                    } else
                    {
                        node.vlContratoPretty = "R$ " + Number.parseFloat(node.vlServico).toFixed(2);
                    }
                }
                listToPrint.push(node);
            }
            return listToPrint;
        }

        function getTotalCost()
        {
            return $scope.patient.vlTotal;
        }

        var getColumns = function() {
            return [
                {title: "Nome Cliente", dataKey: "name"},
                {title: "Plano", dataKey: "nmPlano"},
                {title: "Contrato", dataKey: "nrContrato"},
                {title: "Valor", dataKey: "vlContratoPretty"},
            ];
        };

        function pupulateListPaginated(sequentialList)
        {
            var listPaginated = Array(new Array());
            var part = 0;
            var titularesInPart = 0;
            for (var i = 0; i < sequentialList.length; i++)
            {
                if(sequentialList[i].titular)
                {
                    if(titularesInPart < 10)
                    {
                        listPaginated[part].push(sequentialList[i]);
                        titularesInPart++;
                    } else
                    {
                        part++;
                        listPaginated[part] = [];
                        listPaginated[part].push(sequentialList[i]);
                        titularesInPart = 1;
                    }
                } else
                {
                    listPaginated[part].push(sequentialList[i]);
                }
            }
            return listPaginated;
        }

        $scope.goToPage = function(page)
        {
            $scope.page = page;
        }
    },
    bindings: {
        'lVidas': '=',
        'company': "<"
    }
});
