'use strict';

MedicsystemApp.controller
(
    'ConveniadaFaturamentoDetailVidasController',
    function ($scope, $modalInstance, callWhileInstantiating, lVidas, company)
    {
    	var callWhileInstantiating = eval(callWhileInstantiating);

    	callWhileInstantiating();

        $scope.print = function()
        {
            window.print();
        };

        $scope.cancel = function()
        {
            $modalInstance.dismiss('cancel');
        };

        function constructorController()
        {
            $scope.lVidas = lVidas;
            $scope.company = company;
        }
    }
).value('callWhileInstantiating', "constructorController");;
