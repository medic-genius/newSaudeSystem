'use strict';

MedicsystemApp.controller('GuiaServicosController',
function($scope, $state, $stateParams, NgTableParams, toastr, $modal, servicesFactory, callWhileInstantiating, $location) {
	var callWhileInstantiating = eval(callWhileInstantiating);

	$scope.filter = {
		key: null,
		lStatus: servicesFactory.getStatusItemOfGuia({}),
		interval: {date: null}
	};
	$scope.detailServicoShowing = 0;
	$scope.filter.lStatusSelecteds = [];
	for(var i = 0; i < servicesFactory.getStatusItemOfGuia({}).length; i++)
	{
		$scope.filter.lStatusSelecteds[i] = true;
	}

	function initializeDate(){
		$scope.filter.interval.date = {startDate: null, endDate: null};
		$scope.maxDate = new Date();
	};

	$scope.applyFilter = function(form)
	{
		var queryFilters = {};
		if($scope.filter.interval.date.startDate)
		{
			var startDate = moment($scope.filter.interval.date.startDate).format("YYYY-MM-DD");
			var endDate = moment($scope.filter.interval.date.endDate).format("YYYY-MM-DD");
			queryFilters.start_date = startDate;
			queryFilters.end_date = endDate;
		}
		if($scope.filter.lStatusSelecteds)
		{
			queryFilters.status = [];
			for(var i = 0; i < $scope.filter.lStatus.length; i++)
			{
				if($scope.filter.lStatusSelecteds[i])
				{
					queryFilters.status[queryFilters.status.length] = i;
				}
			}
			queryFilters.status = queryFilters.status;
		}
		if($scope.filter.conveniada)
		{
			queryFilters.id_conveniada = $scope.filter.conveniada.id;
		}
		$location.path('/relatorio_adm_conveniada.html').search(queryFilters);
		getSummaryServicoByStatus();
		getSummaryGuiaServicoByServico()
	};

	function configDatePickerRange() {
		$scope.localeDateRangePicker = {
			"format": "YYYY-MM-DD", "separator": " - ", "applyLabel": "OK",
			"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
			"customRangeLabel": "Customizado", "weekLabel": "S",
			"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
			"monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
			"firstDay": 0
		};

		$scope.rangesDateRangePicker =  {
			'Hoje': [moment(), moment()],
			'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
			'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
			'Este mês': [moment().startOf('month'), moment().endOf('month')],
			'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	};

	function constructorController()
	{
		initializeDate();
		configDatePickerRange();
		getSummaryServicoByStatus();
		getSummaryGuiaServicoByServico();
	}

	function getSummaryServicoByStatus()
	{
		servicesFactory.getSummaryServicoByStatus.query($location.search(), function(data)
		{
			$scope.lSummaryServicoByStatusHumanized = [];
			var total = {
				custoServicos: 0,
				qdtServico: 0,
				status: "Todos",
				classBold: true
			};
			$scope.lSummaryServicoByStatus = [];

			for(var i=0; i < data.length; i++)
			{
				$scope.lSummaryServicoByStatus[$scope.lSummaryServicoByStatus.length] = {
					custoServicos: data[i].custoServicos,
					qdtServico: data[i].qdtServico,
					status: servicesFactory.getStatusItemOfGuia({id: data[i].status}),
					classBold: false
				};
			}
			var lStatusToShow = [0, 1, 2];
			for(var i = 0; i < lStatusToShow.length; i++)
			{
				$scope.lSummaryServicoByStatusHumanized[i] = {
					custoServicos: 0,
					qdtServico: 0,
					status: servicesFactory.getStatusItemOfGuia({id: i}),
					classBold: false
				};
			}
			for(var i=0; i < data.length; i++)
			{
				if(-1 != lStatusToShow.indexOf(data[i].status))
				{
					total.custoServicos += data[i].custoServicos;
					total.qdtServico += data[i].qdtServico;
					$scope.lSummaryServicoByStatusHumanized[data[i].status].qdtServico += data[i].qdtServico;
					$scope.lSummaryServicoByStatusHumanized[data[i].status].custoServicos += data[i].custoServicos;
				}
			}
			$scope.lSummaryServicoByStatusHumanized[$scope.lSummaryServicoByStatusHumanized.length] = total;
		});
	}

	function getSummaryGuiaServicoByServico()
	{
		servicesFactory.getSummaryGuiaServicoByServico.query($location.search(), function(data)
		{
			$scope.lSummaryGuiaServicoByServico = data;
		});
	}

	$scope.getDetailServico = function(idServico)
	{
		$scope.detailServicoShowing = idServico;
	};

	$scope.hideDetailServico = function(idServico)
	{
		$scope.detailServicoShowing = 0;
	};

	callWhileInstantiating();

	servicesFactory.empresasClienteAtivas.query
	(
		function(data)
		{
			$scope.empresasClienteAtivas = data;
		}
	);

}).value('callWhileInstantiating', "constructorController");
