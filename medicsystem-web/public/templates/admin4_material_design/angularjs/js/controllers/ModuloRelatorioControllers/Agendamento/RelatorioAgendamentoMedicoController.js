'use strict';

MedicsystemApp.controller('RelatorioAgendamentoMedicoController', function($rootScope,$scope, $http, 
	$timeout,$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, Auth) {

	function inicializarData() {
 		var dataAtual = moment();
   
    $scope.dataFiltro = {};
    $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};  
  };

  function definirConfigDateRange() {
  	$scope.localeDateRangePicker = {
    	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
    	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
    	"customRangeLabel": "Customizado", "weekLabel": "S",
    	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
    	"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
    	"firstDay": 0
		};

	  $scope.rangesDateRangePicker =  {
  
    	'Hoje': [moment(), moment()],
      'Ontem':[moment().subtract(1, 'days'), moment().subtract(1,'days')],
      'Dois dias atrás':[moment().subtract(2,'days'), moment().subtract(2, 'days')],
      'Última Semana':[moment().subtract(6,'days'), moment()],
		};
  };

  $scope.loadProfissional = function(item, clean, unidade) {
    clean = typeof clean !== 'undefined' ? clean : false;
    $scope.profissional={selected:undefined};         
    if (item && item.id) {
      $scope.profissionaisOptions = servicesFactory.especialidadeProfissionais.query({ id: item.id, idunidade: unidade});
    }

    /*if (clean) {
      $scope.profissional = null;
    }*/
  };

  $scope.loadEspecialidades = function(item, clean){
    $scope.especialidade={id:undefined};
    $scope.profissional={selected:undefined};
  	clean = typeof clean !== 'undefined' ? clean : false;
  	if ( item && item.id ){
  		$scope.especialidadeOptions = servicesFactory.unidadesEspecialidades.get({ id: item.id });
  	}
  }

  function loadStatusAgendamento (){
  	$scope.statusAgendamento = [];
  	var obj = {id: -1, description: "CRIAÇÃO"};
  	$scope.statusAgendamento.push(obj);

  	servicesFactory.statusAgendamento.get({}, function(result){
  		for(var i=0; i < result.length; i++){
				$scope.statusAgendamento.push(result[i]);
			}
  	});
  }

	$scope.loadGrupoFunc = function(){
		$scope.grupoOptions = [];
		var obj = {id:null, description: "Selecione..."};
		$scope.grupoOptions.push(obj);
		servicesFactory.grupofuncionario.get({}, function(result){
			for(var i=0; i < result.length; i++){
				$scope.grupoOptions.push(result[i]);
			}
		});
	}

  $scope.loadFuncionarios = function(grupo){
  	$scope.funcionario={selected: undefined};
  	$scope.arrayIdFuncionario =[];
  	$scope.funcionarioOptions = servicesFactory.funcionariosAtivos.query({ingrupofuncionario: grupo.id});
  }

  $scope.choiceFuncionario = function(item){
  	$scope.arrayIdFuncionario.push(item.id);
  }

  $scope.removeFuncionario = function(item){
  	$scope.arrayIdFuncionario.delete(item.id);
  }
  
  $scope.pesquisar = function(datas, status, grupofunc, unidade, especialidade, profissional, cont){
    $scope.disabledButton = true;
  	$scope.arrayIdFuncionario.toString();
  	var dataInicio = moment($scope.dataFiltro.date.startDate).format("YYYY-MM-DD").toString();
		var dataFim = moment($scope.dataFiltro.date.endDate).format("YYYY-MM-DD").toString();
  	servicesFactory.relatorioAgendamento.get({dataInicio: dataInicio, dataFim: dataFim, statusAg: status, idOperador: $scope.arrayIdFuncionario, inGrupoFuncionario: grupofunc, idUnidade: unidade, idEspecialidade: especialidade, idProfissional: profissional, offset: cont},function(response){
  		if(response.data.length>0){
        $scope.tamListaAgendamento = response.total;
  			for(var i=0; i < response.data.length; i++){
  				response.data[i].dtInclusaoFormatada = moment(response.data[i].dtInclusaoLog).format("DD/MM/YYYY hh:mm:ss").toString();
  				if(response.data[i].dependente != null){
  					response.data[i].nmTitular = response.data[i].cliente.nmCliente;
  				}else{
  					response.data[i].nmTitular = null;
  				}
  			}
  			$scope.relatorioAgendamento = response.data;
  			$scope.showTab = true;
        $scope.disabledButton = false;
        configurarPage(response);
  			configurarTable($scope.relatorioAgendamento);
  		}else{
  			$scope.relatorioAgendamento = null;
  			$scope.showTab = false;
        $scope.disabledButton = false;
  		}
  	}, function(erro){
        $scope.disabledButton = false;
      });
  }

  function configurarPage( colecao ) {
    if( colecao.data.length > 0 ) {
      colecao.data.forEach( function( item, index) {
        item.currentPage = 1;
        item.totalItems = item.totalItems;
        item.itemsPerPage = 50; 
      });

      $scope.relatorioAgendamentoFinal = colecao;
      definirPaginacao( $scope.relatorioAgendamentoFinal );
    } else
      $scope.relatorioAgendamentoFinal = [];
  };
  
  function definirPaginacao( colecao ) {
    $scope.currentPage = colecao.offset;
    $scope.totalItems = colecao.total;
    $scope.itemsPerPage = 50;
  }
  
  $scope.pageChanged = function(datas, status, grupofunc, unidade, especialidade, profissional, cont){
    $scope.pesquisar(datas, status, grupofunc, unidade, especialidade, profissional, cont);
  }

  function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:50};
			var initialSettings = {
				counts:[], 
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings );
			
		}
	};

	function loadGruposFuncionario(){
		$scope.loadGrupoFunc();
	}

  function loadUnidades(){
    $scope.unidadeOptions = [];
    var obj = {id:null, nmUnidade: "Selecione..."};
    $scope.unidadeOptions.push(obj);
    servicesFactory.unidades.query({}, function(result){
      for(var i=0; i < result.length; i++){
        $scope.unidadeOptions.push(result[i]);
      }
    });
  }


  function inicio(){
  	inicializarData();
  	definirConfigDateRange();

  	$scope.showTab = true;
  	$scope.grupoFuncionario={id: undefined};
  	$scope.status={id: null};
    $scope.unidade ={id:undefined};
  	$scope.especialidade = {id:undefined};
  	$scope.profissional ={selected:undefined};
		$scope.arrayIdFuncionario =[];
  	$scope.funcionario={selected: undefined};
  	$scope.funcionarioOptions =[];

    $scope.disabledButton = false;
  	
    loadUnidades();
  	loadGruposFuncionario();
  	loadStatusAgendamento();
  }

  inicio();
});