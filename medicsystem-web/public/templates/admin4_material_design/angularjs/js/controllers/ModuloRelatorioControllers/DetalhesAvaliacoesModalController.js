MedicsystemApp.controller('DetalhesAvaliacaoModalController', function($scope,
	$state, $rootScope, NgTableParams, servicesFactory, $modalInstance, 
	idFuncionario, idEspecialidade, idUnidade, nameFuncionario) {

	$scope.modalInit = function() {
		$scope.nameFuncionario = $scope.simplifyName(nameFuncionario);

		$scope.loadAvaliacoesSms();
	}

	$scope.showClienteData = function(avaliacao) {
		var data = {
			idCliente: avaliacao.idCliente,
			idDependente: avaliacao.idDependente,
			isDependente: avaliacao.isDependente
		}
		$rootScope.selectedUserData = data;
		$scope.close();
		$state.go('atendimentoCliente');
	}

	$scope.loadAvaliacoesSms = function() {
		var rParams = {
			idUnidade: idUnidade,
			idFuncionario: idFuncionario,
			idEspecialidade: idEspecialidade
		}

		$scope.hasError = false;
		$scope.isProcessing = true;
		servicesFactory.avaliacaoEspMedico.get(
			rParams,
			function(result) {
				if(result && result.length > 0) {
					$scope.avaliacoesList = [];
					for(var i = 0; i < result.length; ++i) {
						var av = {
							idCliente: result[i].idCliente,
							idDependente: result[i].idDependente,
							dtAgendamento: moment(result[i].dtEnvio).format('DD/MM/YYYY'),
							resposta: result[i].resposta.replace(';', ' | '),
							listaResposta: result[i].resposta.split(';')
						}
						if(result[i].idDependente) {
							av.nmCliente = result[i].nmDependente;
							av.nrCodCliente = result[i].nrCodDependente;
							av.isDependente = true;
						} else {
							av.nmCliente = result[i].nmCliente;
							av.nrCodCliente = result[i].nrCodCliente;
							av.isDependente = false;
						}
						$scope.avaliacoesList.push(av);
					}

					$scope.configureTable($scope.avaliacoesList);
				}
			},
			function(err) {
				$scope.avaliacoesList = undefined;
				$scope.hasError = true;
			}
		).$promise.finally(function() {
			$scope.isProcessing = false;
		});
	}

	$scope.simplifyName = function(name) {
		if(!name) {
			return;
		}
		var ln = name.split(' ');
		if(ln.length >= 3) {
			return ln[0] + ' ' + ln[ln.length-1];
		} else if(ln.length == 2) {
			return ln[0] + ' ' + ln[1];
		} else {
			return ln[0];
		}
	}

	$scope.configureTable = function(lista) {
		var tParams = {
			dataset: lista
		};
    $scope.tableParams = new NgTableParams({}, tParams);  
  };

	$scope.close = function() {
		$modalInstance.dismiss();
	}

	$scope.modalInit();
});