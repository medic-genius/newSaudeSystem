'use strict';

MedicsystemApp.controller('RelatorioProdFuncionarioController', function($rootScope,$scope, $http, 
	$timeout,$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, Auth) {

	function inicializarTotais() {
		$scope.totais = {};
		$scope.totais.funcionarios = 0;
		$scope.totais.agendamentosCriados = 0;
	};

	function calcularTotais( lista ) {
		if( lista.length > 0) {

			$scope.totais.funcionarios = lista.length;

			for( var i = 0; i < lista.length;i++ ) {
				$scope.totais.agendamentosCriados = $scope.totais.agendamentosCriados  + lista[i].qtdAgendamento;
			}
		}
	}

	function configurarTable( lista ) {

		var initialParams = { count:50 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};

	$scope.carregarDadosFuncionarios = function() {

		inicializarTotais();

		var dtInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
		var dtFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");

		var dataValida = moment($scope.datePicker.date.startDate).isValid();

		$scope.dadosProdutividadeFuncionario = [];
		
		if( dataValida ) {
			servicesFactory.relatorioProdutividadeFuncionario
			.get( {dtInicio: dtInicio, dtFim:dtFim}, 
		   		function( result ) {
		   			if(result) {
		   				console.log("result",result);
		   				$scope.dadosProdutividadeFuncionario = result;
		   				calcularTotais( $scope.dadosProdutividadeFuncionario );
		   				//$scope.somaTotalComissao = result.somatotalVlComissaoSalario;
		   				configurarTable($scope.dadosProdutividadeFuncionario);
		   			}
	        });	
		}
	};

	var configurarDatePickerRange = function() {

		$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	   		'Hoje': [moment(), moment()],
       		'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       		'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
       		'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
       		'Este mês': [moment().startOf('month'), moment().endOf('month')],
       		'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	};

	var inicializarData = function() {
		$scope.datePicker = {};
		$scope.maxDate = new Date();
		$scope.datePicker.date = {startDate: null, endDate: null};	
	}

	function inicio() {
		inicializarData();
		inicializarTotais();
		configurarDatePickerRange();
	}


	inicio();

});


