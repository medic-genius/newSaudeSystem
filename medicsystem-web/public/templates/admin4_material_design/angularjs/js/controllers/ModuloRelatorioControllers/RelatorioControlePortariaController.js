'use strict';

MedicsystemApp.controller('RelatorioControlePortariaController', function($scope,
	$state, $stateParams, NgTableParams, toastr, $modal, $timeout, servicesFactory) {

    function configurarTable( lista ) {

        var initialParams = { count:20 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };

    $scope.$on('$viewContentLoaded', function() {
		// referencia a unidade selecionada
        $scope.multipleUnidades = {selected: undefined};
        $scope.unidadeOptions = servicesFactory.unidades.query();
        $scope.escolhaUnidade ={};
        $scope.multipleProfissional = {selected: undefined};
        $scope.tablePrint = [];
        $scope.horariosCheck = [];
        $scope.loadUnidades();
        
        $scope.novoRegistro = {
            dtRegistro: undefined,
            listaHorarios: [{
                horaEntrada: undefined,
                horaSaida: undefined
            }]
        }
	});

    $scope.obterEstatisticas = function (item){
        $scope.idUnidade = item.id;
        $scope.loadAllProfissional();
    };


    
    //INICIANDO CALENDARIO
    $scope.dataFiltro = {};
    $scope.dataFiltro.date = {startDate: moment(), endDate: moment()};
    $scope.maxDate = new Date();
    $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
    };
    
    $scope.rangesDateRangePicker =  {
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    }

    $scope.loadUnidades = function() {
		servicesFactory.unidades.query(function(result) {
				if(result) {
                    $scope.unidadeOptions = result;
                }
			}
		);
    }

    $scope.grupoFuncionario = [
        {id: 0, description: 'Funcionários'},
        {id: 1, description:   'Médicos/Dentistas'},
        {id: 2, description: 'Prestadores de Serviço'},
        {id: 3, description: 'Vendedores'},
        {id: null, description: 'Todos'}
    ];

    $scope.carregarPontosFuncionario = function (ponto){
        if($scope.idUnidade != null && $scope.dataFiltro.date.startDate != '' && $scope.dataFiltro.date.endDate != ''){
            var dtInicio = moment($scope.dataFiltro.date.startDate).format("YYYY/MM/DD");
            var dtFim = moment($scope.dataFiltro.date.endDate).format("YYYY/MM/DD");
            servicesFactory.pontofuncionario.query({
                dtInicio: dtInicio,
                dtFim: dtFim,
                idUnidade: $scope.idUnidade,
                idFuncionario: ponto.idFuncionario
            }, function(dadosFuncionario) {
                if(dadosFuncionario.length > 0){
                    $scope.showEmpty = false;
                    $scope.novosRegistros = [];
                
                    ponto.listaPonto = dadosFuncionario;
                    ponto.novoRegistro = {
                        dtRegistro: undefined,
                        listaHorarios: [{
                            horaEntrada: undefined,
                            horaSaida: undefined
                        }]
                    }

                    for(var i=0; i < ponto.listaPonto.length; i++){
                        var dtFormatada = moment(ponto.listaPonto[i].dtRegistro).format("DD/MM/YYYY");
                        ponto.listaPonto[i].dtFormatada = dtFormatada;

                    }
                    $scope.tablePrint = ponto.listaPonto;
                   
                } else {
                   $scope.showEmpty = true;
                }
            });
        }
    };

    $scope.addLinhaRegistro = function(nRegistro, idFuncionario, idUnidade) {
        $scope.desabilitar = true;
        nRegistro.listaHorarios.push({
            horaEntrada: undefined,
            horaSaida: undefined
        });
        $scope.salvarNovoRegistro(nRegistro, idFuncionario, idUnidade);
    }

    $scope.deleteLinhaRegistro = function(item, index) {
        if(index != 0){
            item.listaHorarios.splice(index, 1);
        } else {
            $scope.desabilitar = false;
        }
    }

    function getHorariosFromNovoRegistro(listaHorarios) {
        var arr = [];
        var isValid = true;
        for(var i = 0; i < listaHorarios.length; ++i) {
            if(!listaHorarios[i].horaEntrada || !listaHorarios[i].horaSaida) {
                isValid = false;
                break;
            }
            arr.push(listaHorarios[i].horaEntrada);
            arr.push(listaHorarios[i].horaSaida);
        }
        var result = {
            isValid: isValid,
            list: arr
        }
        return result;
    }

    $scope.salvarNovoRegistro = function(novoRegistro, idFuncionario, idUnidade){
        if(!novoRegistro.dtRegistro) {
            toastr.error('Preencha todos os campos', 'Atenção');
            return ;
        }

        var result = getHorariosFromNovoRegistro(novoRegistro.listaHorarios);
        if(!result.isValid) {
            toastr.error('Preencha todos os campos', 'Atenção');
            return;
        }

        var rParams = {
            dtRegistro: moment(novoRegistro.dtRegistro, "DD/MM/YYYY").format("YYYY-MM-DD"),
            idFuncionario: idFuncionario,
            idUnidade: idUnidade
        }

        servicesFactory.validarDataRegistro.get(rParams, function(dados){
            if(dados.length > 0 ){
                toastr.error('Data de registro existente', 'Atenção');
               return;
            } else {
                var list = result.list;
                servicesFactory.saveRegistroPonto.save(rParams, list, function(response){
                    toastr.success('', 'Registro salvo com sucesso');
                    novoRegistro.dtRegistro = undefined;
                    novoRegistro.listaHorarios = [{
                        horaEntrada: undefined,
                        horaSaida: undefined
                    }];
                    $scope.unidadeOptions = servicesFactory.unidades.query();
                    $scope.escolhaUnidade ={};    
                }, function(error){
                    toastr.error('', 'Não foi possível salvar o registro');
                });
            }
        });
                   
    }

    $scope.maskHorario = function(){
        $timeout(function() {
            $(".time").inputmask({
                mask:"H9:59:59",
                definitions:{'H':{validator: "[0-2]"}, '5':{validator: "[0-5]"}} //mascara para o horario
            });
        }, 500);
    }
    

    $scope.cancelarNovoRegistro = function(novoRegistro, profissional, unidade){
        $scope.cancelar = true;
        novoRegistro.dtRegistro = undefined;
        novoRegistro.listaHorarios = [{
            horaEntrada: undefined,
            horaSaida: undefined
        }];
        $scope.unidadeOptions = servicesFactory.unidades.query();
        $scope.escolhaUnidade ={};
    }

    $scope.loadProfissional = function(item, clean) {
        clean = typeof clean !== 'undefined' ? clean : false;
                
        if (item && item.id) {
          $scope.profissionaisOptions = servicesFactory.profissionaisUnidade.query({ id: item.id });
        }
        if (clean) {
          $scope.escolhaUnidade.profissional = null;
        }
      };

    function calcularDiaria(list){
        var len = list.length >= 4 ? 4 : list.length; 
        var hrs = [undefined, undefined, undefined, undefined];
        for(var i = 0; i < len; ++i) {
            hrs[i] = list[i].hrRegistro;
            
        }
        return calcularDiariaAux(hrs[0], hrs[1], hrs[2], hrs[3]);
        
    };

    $scope.getDiariaFormatada = function(list) {
        var vl = calcularDiaria(list);
        var s1 = moment.utc(+vl).format("HH:mm:ss");
        return s1;
    }

    function calcularDiariaAux(c1, c2, c3, c4){


        var start1 = moment.duration(c1, 'minutes');
        var end1 =  moment.duration(c2, 'minutes');
        var start2 = moment.duration(c3, 'minutes');
        var end2 = moment.duration(c4, 'minutes');

        if((start1 && end1) >0){
            var d = end1-start1;  
        }else {
            d = 0;
        }
        if((start2 && end2) > 0){
            var d2 = end2 - start2;
        } else {
            d2 =0;
        }
/*         var d = end1 - start1;
        var d2 = end2 - start2; */
        
        var total = d +d2;
        return total;   
    };

    $scope.getTotalHorasForm = function (lista){

        $scope.tablePrintTotal = lista;
        if(!lista || !lista.listaPonto) return;
        var ht = getTotalHoras(lista.listaPonto);
        return ht;
    }

    function getTotalHoras(list) {

        var total = 0;
        for(var i=0; i < list.length; i++) {
            total += calcularDiaria(list[i].listRegistros);
        }

        var dia = moment.duration(total).days();
        var horas = moment.duration(total).hours();
        var minutos = moment.duration(total).minutes();
        var segundos = moment.duration(total).seconds();

        var horasParcial = ((dia *24)+ horas);
        if(horasParcial < 10){
            horasFinal = '0'+ horasParcial;
        } else {
            var horasFinal = horasParcial;
        }
        if(minutos < 10){
            var minutosFinal = '0'+minutos;
        } else {
            var minutosFinal = minutos;
        }

        if(segundos < 10){
            var segundosFinal = '0'+segundos;
        } else {
            var segundosFinal = segundos;
        }
    
        var totalHoras = horasFinal + ':' + minutosFinal + ':' + segundosFinal;
        //console.log("totalHoras", totalHoras);
        return totalHoras;

    } 
    

    $scope.returnRegistro = function (registro){
        $scope.showEdit = false;
        $scope.carregarPontosFuncionario(registro);
    };
    $scope.editarRegistro = function (item,profissional) {
        
        $scope.profFront = profissional;
        $scope.showEdit = true;
        profissional.isEditing = true;
        $scope.cancelar = false;
        profissional.expanded = false;
        profissional.objCache = JSON.parse(JSON.stringify(profissional.listRegistros));

    };
    

    function hasChanged(cache, obj) {
        for(var i = 0; i < obj.length; i++){
            if(!cache[i] || cache[i].hrRegistro != obj[i].hrRegistro || cache[i].nmJustificativa != obj[i].nmJustificativa) {
                return true;
            }
        }
        return false;
    };
    

    $scope.updateRegistro = function (registro){
        if($scope.cancelar == true) {
            $scope.showEdit = false;
        } else {
            if(!hasChanged(registro.objCache, registro.listRegistros)) {
                registro.listRegistros = registro.objCache;
                delete registro.objCache;
                return;
            }

            var list = JSON.parse(JSON.stringify(registro.listRegistros));
            for(var i = 0; i < list.length; i++) {
                if(!list[i]['dtRegistro']) {
                    list[i]['dtRegistro'] = registro.dtRegistro + "T04:00:00.000Z";
                }
                if(!list[i]['nmJustificativa']){
                    list[i]['nmJustificativa'] = null;
                }
                
                list[i]['funcionario'] = {
                    id: registro.idFuncionario
                }

                list[i]['unidade'] = {
                    id: list[0].unidade.id
                }

                delete list[i].registroFormatado;
                delete list[i].funcionario.inTipoProfissional;
                delete list[i].funcionario.nrCrmCro;
            }

            servicesFactory.updateRegistroPonto.update(null, list, function(response){
                toastr.success('', 'Registro atualizado com sucesso');
            }, function(error){
                toastr.error('', 'Não foi possível atualizar registro');
                registro.listRegistros = registro.objCache;
                delete registro.objCache;
            });
    
        }

    };

    $scope.cancel = function(registro){
        $scope.cancelar = true;
        $scope.profFront.expanded = false;
    
        registro.listRegistros = registro.objCache;
        delete registro.objCache;
    
    }
    
    $scope.loadAllProfissional = function() {
        if ($scope.idUnidade != null &&  $scope.dataFiltro.date.startDate != '' &&  $scope.dataFiltro.date.endDate != ''){
            var dtInicio = moment($scope.dataFiltro.date.startDate).format("YYYY/MM/DD");
            var dtFim = moment($scope.dataFiltro.date.endDate).format("YYYY/MM/DD");
            

            servicesFactory.pontounidade.query({
                dtInicio: dtInicio,
                dtFim: dtFim,
                idUnidade: $scope.idUnidade,
                idFuncionario: $scope.idFuncionario,
                grupoFuncionario: $scope.multipleProfissional.selected && $scope.multipleProfissional.selected.id
            }, function(dados){
            if (dados.length > 0){
                $scope.tablePrint = dados;
                $scope.tablePrintTodos = dados;

                configurarTable(dados);
                $scope.showTable = true;
            }
            else  {
                $scope.showTable = false;
            }

            
            });
        }
    
    };

  


//Para um funcionario
    $scope.gerarPdf = function() {
  		getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
  	};
      
	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
    };
   /* var getImageReport = function ()
     
    $scope.verificarJus = function(lista){
        console.log("lista", lista);
        if(lista.nmJustificativa != null){
            
        }
    } */
   

    var print = function ( imgData ){
		var doc = new jsPDF('p','pt','letter','a4');
        var rows = [];        

        var columns=[
            {title: "Data", dataKey: "dtFormatada"},
            {title: "Funcionario", dataKey: "nmFuncionario"},
            {title: "Entrada", dataKey:"entrada1"},
            {title: "Saida", dataKey:"saida1"},
            {title: "Entrada", dataKey:"entrada2"},
            {title: "Saida", dataKey: "saida2"},
            {title: "Carga Diaria", dataKey: "cargaHoraria"}
        ];
        
        for(var i=0; i < $scope.tablePrint.length; i++){

            rows[i] = {
                "dtFormatada": $scope.tablePrint[i].dtFormatada,
                "nmFuncionario": $scope.tablePrint[i].nmFuncionario,
                "entrada1": $scope.tablePrint[i].listRegistros[0] && $scope.tablePrint[i].listRegistros[0].hrRegistro /* && $scope.verificarJus($scope.tablePrint[i].listRegistros[0]) */,
                "saida1": $scope.tablePrint[i].listRegistros[1] && $scope.tablePrint[i].listRegistros[1].hrRegistro,
                "entrada2": $scope.tablePrint[i].listRegistros[2] && $scope.tablePrint[i].listRegistros[2].hrRegistro,
                "saida2": $scope.tablePrint[i].listRegistros[3] && $scope.tablePrint[i].listRegistros[3].hrRegistro,
                "cargaHoraria": $scope.getDiariaFormatada($scope.tablePrint[i].listRegistros)
            } 
            

        }
    
        var total = $scope.getTotalHorasForm($scope.tablePrintTotal);
        
		//CABECALHO	
        doc.addImage(imgData, 'JPEG', 205, 32, 200/*width*/, 43/*heigth*/, 'logo');

        doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
        doc.writeText(0,120, 'Controle de Portaria',{align:'center'});
        doc.setFontSize(10);
        doc.writeText(-80, 140, 'Total Horas: ' + total, {align:'right'});
      
		// LINE 1
        doc.autoTable(columns, rows, {margin: {top: 150}});
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
    };



});

