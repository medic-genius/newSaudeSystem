'use strict';

MedicsystemApp.controller('RelatorioComissaoFuncionariosUnidadesController', function($rootScope, Auth, $scope, 
    $timeout,$state,$stateParams,servicesFactory, NgTableParams) {
    
    function configurarTable( lista ) {

        var initialParams = { count: 20 };

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };

    function configurarDatePickerRange() {
        
        $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };

        $scope.rangesDateRangePicker =  {
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };

    function inicializarData() {
        $scope.datePicker = {};
        $scope.datePicker.date = {startDate: moment(), endDate: moment()};  
        $scope.maxDate = new Date();
    };

    $scope.loadUnidades = function() {
    $scope.unidadeOptions = [];
        servicesFactory.unidades.query(
            function(result) {
                if(result) {
                    for ( var i = 0; i < result.length; i++){
                        if (result[i].id == 7 || result[i].id == 2 || result[i].id == 1)
                        $scope.unidadeOptions[i] = result[i];
                    }
                }
            }
        );
    }

    $scope.obterEstatisticas = function (item){
        $scope.idUnidade = item.id;
        $scope.buscarDados();
    };

    $scope.buscarDados = function (){
        if ($scope.datePicker.date.startDate != null && $scope.datePicker.date.endDate != null && $scope.idUnidade != null){
        $scope.showPage = true;
        $scope.somaVenda = 0;
        $scope.somaComissao = 0;
        $scope.somaCont = 0;

            var dtInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
            var dtFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");

         servicesFactory.contratoFuncionariosUnidade.get({
             dtInicio: dtInicio,
             dtFim: dtFim,
             idUnidade: $scope.idUnidade
        }, function( result ) {
            if (result.length > 0){
                console.log(result);
                configurarTable(result);
                $scope.showTable = true;
                    for (var i = 0; i < result.length; i++){
                        $scope.somaCont = $scope.somaCont + result[i].quantcontratos;
                        $scope.somaVenda = $scope.somaVenda + result[i].totalvenda;
                        $scope.somaComissao = $scope.somaComissao +  result[i].totalcomissao;
                    }
            }else  
            $scope.showTable = false;
        
        });
            
            
        }
    }

    var inicio = function() {
        $scope.showPage = false;
        $scope.loadUnidades();
        $scope.idUnidade = null;
        inicializarData();
        configurarDatePickerRange();
        $scope.buscarDados();
    };

    inicio();
    
});