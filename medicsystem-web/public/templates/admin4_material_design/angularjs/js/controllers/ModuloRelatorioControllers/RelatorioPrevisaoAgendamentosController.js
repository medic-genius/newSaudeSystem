'use strict';

MedicsystemApp.controller('PrevisaoAgendamentosController', function($scope,
	$state, $stateParams, NgTableParams, toastr, $modal, servicesFactory) {

	$scope.$on('$viewContentLoaded', function() {
		$scope.DAYS_AHEAD = 7;
		// referencia a unidade selecionada
		$scope.multipleUnidades = {selected: undefined};

		// referencia as opções selecionadas para busca de avaliações
		$scope.avaliacaoOptions = [
		{ nmPeriodo: 'Ultimos 30 dias', valor: 30}
		];

		$scope.avaliacaoPeriodo = $scope.avaliacaoOptions[0];

		$scope.generateRangeAhead();
		$scope.loadUnidades();
	});

	$scope.loadAvaliacoes = function(avaliacaoOpt) {
		console.log(avaliacaoOpt);
		console.log($scope.avaliacaoPeriodo);
	} 

	$scope.loadUnidades = function() {
		servicesFactory.unidades.query(
			function(result) {
				if(result) {
					$scope.unidadeOptions = result;
				}
			}
		);
	}

	$scope.obterEstatisticas = function(unidade) {
		var rParams = {
			idUnidade: unidade.id
		}
		servicesFactory.previsaoAgendamentos.get(
			rParams,
			function(result) {
				$scope.dataList = result;
				$scope.configureTable($scope.dataList);
			},
			function(err) {
				toastr.error('ERRO', 'Não foi possível comunicar com o servidor');
			}
		);
	}

	$scope.configureTable = function(lista) {
		var tParams = {
			dataset: lista
		}
    $scope.tableParams = new NgTableParams({}, tParams);
  };

  $scope.generateRangeAhead = function() {
  	var arr = [];
  	var offset = 0;
  	while(arr.length < 7) {
  		var m = moment();
  		m.add(offset, 'days');
  		// if(m.day() != 0) {
  			var obj = {
  				title: m.format('DD/MM/YYYY'),
  				momentObject: m
  			}
  			arr.push(obj);
  		// }
  		++offset;
  	}
  	$scope.days = arr;
  }

  $scope.formatRate = function(rateValue) {
  	if(rateValue == undefined || rateValue == null) {
  		return;
  	}
  	return parseFloat(rateValue.toFixed(2));
  }

  $scope.showDetailAvaliacao = function(item) {
  	if(item.notasms == null || item.notasms == undefined) {
  		return;
  	}
		var modalInstance = $modal.open({
			animation: true,
			templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/detalhes-avaliacao-modal.html',
			controller: 'DetalhesAvaliacaoModalController',
			backdrop: 'static',
			keyboard: true,
			size: 'lg',
			resolve: {
				idFuncionario: function() {
					return item.idFuncionario;
				},

				idEspecialidade: function() {
					return item.idEspecialidade;
				},

				idUnidade: function() {
					return $scope.multipleUnidades.selected.id;
				},

				nameFuncionario: function() {
					return item.nmFuncionario;
				}
			}
		});
	}
});