'use strict';

MedicsystemApp.controller('ConveniadaFaturamentoController',
function($scope, $state, $stateParams, NgTableParams, toastr, $modal, servicesFactory, callWhileInstantiating, $location)
{
	var callWhileInstantiating = eval(callWhileInstantiating);

	callWhileInstantiating();

	function configurarTable(lista)
	{
		var initialParams = {count:100};
		var initialSettings = {
			counts:[],
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};
    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )
	};

	function loadFaturamentoConveniada() {
		var startDate = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
		var endDate = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");
		var dataValida = moment($scope.datePicker.date.startDate).isValid();
		$scope.lFaturamentoConveniada = [];
		$scope.messageLoading = `Carregando dados de ${moment($scope.datePicker.date.startDate).format("DD/MM/YYYY")} à ${moment($scope.datePicker.date.endDate).format("DD/MM/YYYY")}, aguarde...`;
		if(dataValida)
		{
			resetSummary();
			servicesFactory.getRelatorioFaturamentoConveniada.get( {dtInicio: startDate, dtFim: endDate},
				function(data)
				{
					if(data)
					{
						prepareData(data)
					}
				}
			);
		}
	};

	function prepareData(data)
	{
		$scope.summary.despesa = 0;
		$scope.summary.mensalidade = 0;
		$scope.summary.total = 0;
		var item;
		for(var i=0; i < data.length; i++)
		{
			item = data[i];
			item.detail = prepareSubTableDetail(data[i]);
			$scope.lFaturamentoConveniada[i] = item;
		}
		configurarTable(data);
	}

	function prepareSubTableDetail(data)
	{
		var despesa = {
			label: "Despesa:",
			vida: 0,
			servico: 0,
			valor: "0,00"
		};
		var mensalidade = {
			label: "Mensalidade:",
			vida: 0,
			servico: 0,
			valor: "0,00"
		};
		if(data.inTipo == 1)
		{
			despesa.servico = data.totalServico;
			despesa.vida = null;
			despesa.valor = data.vlTotal.toLocaleString("pt-BR", {minimumFractionDigits: 2});
			$scope.summary.despesa += data.vlTotal;

            mensalidade.servico = null;
            mensalidade.vida = null;

		} else if (data.inTipo == 2)
		{
			mensalidade.servico = data.totalServico;
			mensalidade.vida = data.totalVidas;
			mensalidade.valor = data.vlTotal.toLocaleString("pt-BR", {minimumFractionDigits: 2});
			$scope.summary.mensalidade += data.vlTotal;

            despesa.servico = null;
            despesa.vida = null;
		}
        /*
		var total = {
				label: "Total: ",
				servico: despesa.servico + mensalidade.servico,
				vida: despesa.vida + mensalidade.vida,
				valor: data.vlTotal.toLocaleString("pt-BR", {minimumFractionDigits: 2})
			};
        */
		$scope.summary.total += data.vlTotal;
		var row = [];
		row[row.length] = despesa;
		row[row.length] = mensalidade;
		//row[row.length] = total;

		return row;
	}

	$scope.applyFilter = function()
	{
		loadFaturamentoConveniada();
	}

	function resetSummary()
	{
		$scope.summary = {
			despesa: "Aguarde...",
			mensalidade: "Aguarde...",
			total: "Aguarde..."
		}
	}

	function configurarDatePickerRange()
	{
		$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
		};
	    $scope.rangesDateRangePicker =  {
       		'Este mês': [moment().startOf('month'), moment().endOf('month')],
			'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'Dois Meses Atrás': [moment().subtract(2, 'month').startOf('month'), moment().subtract(2, 'month').endOf('month')]
		}
	};

	function  inicializarData()
	{
		$scope.datePicker = {};
		$scope.maxDate = moment().endOf('month');
		var setMonthCurrentAsDefault = false;
		if(setMonthCurrentAsDefault)
		{
			var date = new Date();
			$scope.datePicker.date = {startDate: (new Date(date.getFullYear(), date.getMonth(), 1)), endDate: (new Date())};
		} else
		{
			$scope.datePicker.date = {};
		}
	}

	function constructorController()
	{
		$scope.dadosGeraisFuncionario =[];
		$scope.dadosReferencia =[];
		resetSummary();
		inicializarData();
		configurarDatePickerRange();
	}

	if($scope.datePicker.date.startDate && $scope.datePicker.date.endDate)
	{
		loadFaturamentoConveniada();
	}

    $scope.openModalDatailServico = function(itemSummary)
    {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/relatorio_adm_conveniada_faturamento_detail.html',
            controller: 'ConveniadaFaturamentoDetailController',
            size: "lg",
            resolve: {
                itemSummary: function ()
				{
                    return itemSummary;
                }
            }
        });
        modalInstance.result.then(function (data){}); // not used
    };

    $scope.openModalHistory = function(idConveniada)
    {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/conveniada_financial_history_modal.html',
            controller: 'ConveniadaFinancialHistoryController',
            size: "lg",
            resolve: {
                idConveniada: function ()
				{
                    return idConveniada;
                }
            }
        });
        modalInstance.result.then(function (data){}); // not used
    };

    $scope.openModalVida = function(lVidas)
    {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/relatorio_adm_conveniada_faturamento_detail_vidas.html',
            controller: 'ConveniadaFaturamentoDetailVidasController',
            size: "lg",
            resolve: {
                lVidas: function ()
				{
                    return lVidas.listClienteDependente;
                },
                company: function ()
				{
                    return lVidas.nmEmpresaCliente;
                }
            }
        });
        modalInstance.result.then(function (data){}); // not used
    };

    $scope.openModalFaturarGeral = function(){
    	var modalInstance = $modal.open({
    		animation: true,
    		templateUrl: 'templates/admin4_material_design/angularjs/views/faturamento/conveniada-faturar-modal.html',
    		controller: 'ConveniadaFaturarModalController',
    		size: "lg"
    	})
    }

}).value('callWhileInstantiating', "constructorController");
