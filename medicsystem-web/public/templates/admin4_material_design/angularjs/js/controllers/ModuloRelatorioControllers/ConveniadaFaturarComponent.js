'use strict';

MedicsystemApp.component('conveniadaFaturarComponent', {
	templateUrl: 'templates/admin4_material_design/angularjs/views/faturamento/conveniada-faturar.component.html',

    controller: function ($scope, servicesFactory)
    {
        this.$onInit=function(){
            constructorController();
        };

        function constructorController()
        {
			carregarEmpresasCliente();
			$scope.showMensagem = false;
			$scope.periodo= {selected: null};
			$scope.faturamento ={};
			$scope.disableButton = false;
			$scope.conveniada ={};
			$scope.showInfos = false;
        }

		function carregarEmpresasCliente(){
			servicesFactory.empresasClienteAtivas.get({}, function(result){
				$scope.listConveniadas = result;
			})
		}

		$scope.carregarPeriodoConv = function (item){
			getPeriodoConv(item);
			$scope.conveniadaSelected = item;
		}

		function getPeriodoConv(item){
			$scope.periodo ={selected: null};
			$scope.conFaturado = undefined;
			servicesFactory.getPeriodoConv.get({idEmpresaCliente: item.id}, function(result){
				$scope.lPeriods = result;
			})
		}

		$scope.verificarCondicao = function(periodo){
			$scope.infoPeriodo = periodo;
			if(periodo.dtFechamento){
				var formatLowerCase = "yyyy-MM-dd".toLowerCase();
				var formatItems = formatLowerCase.split('-');
				var dateItems = periodo.dtFechamento.split('-');
				var monthIndex = formatItems.indexOf("mm");
				var dayIndex = formatItems.indexOf("dd");
				var yearIndex = formatItems.indexOf("yyyy");
				var month = parseInt(dateItems[monthIndex]);
				var dtFechamentoFormatada = dateItems[dayIndex]+"/"+month+"/"+dateItems[yearIndex];
				$scope.infoPeriodo.dtFechamentoFormatada = dtFechamentoFormatada;
				getDetalhesFatura($scope.conveniadaSelected.id, periodo.period);
			}

			$scope.periodo.selected = periodo;
			$scope.gerando = false;
			$scope.faturamento = null;
			$scope.showInfos = true;
			$scope.showMensagem = false;
			if(periodo.dtFechamento == null || periodo.dtFechamento == undefined){
				$scope.conFaturado = false;
			} else
			{
				$scope.conFaturado = true;
			}
		}

		function getDetalhesFatura(idConveniada, period)
		{
			servicesFactory.getDetalhesFatura.get(
				{
					id: idConveniada,
					period: period
				},
				function(data)
				{
					$scope.detalheFatura = data;
				}
			);
		}

		$scope.exportReportFatura = function()
        {
            var doc = new jsPDF('portrait','pt','letter','a4');
            doc.setFont("times");
            doc.setFontType('bold');
            doc.setFontSize(12);
            doc.writeText(0,50, "Relatório de Faturamento - " + $scope.conveniadaSelected.nmFantasia, {align:'center'});
            doc.setFontSize(7);
            doc.setFontType('normal');
            doc.writeText(40, 75, 'Total Serviços: ' + prepareDataToPrint().length + '         Valor Parcial: R$ ' + getTotalCost().toFixed(2) + "         Desconto: R$ " + getDesconto().toFixed(2) + "         Total: R$ " + (getTotalCost() + getDesconto()).toFixed(2));
			doc.writeText(40, 95, "Observaçao: " + ((null != $scope.detalheFatura.conferencia.obsDadosFinanceiros) ? $scope.detalheFatura.conferencia.obsDadosFinanceiros : ""));
			doc.autoTable(getColumns(), prepareDataToPrint(), {
				startY: 120,
				margin: {horizontal: 40},
				bodyStyles: {valign: 'top', fontSize: 7},
				styles: {overflow: 'linebreak'},
				columnStyles: {contac:{overflow:'linebreak', fontSize: 7}},
			});

			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');
        }

        var getColumns = function()
		{
            return [
                {title: "Data", dataKey: "dtVencimento"},
                {title: "Despesa", dataKey: "idDespesa"},
                {title: "Contrato", dataKey: "nrContrato"},
                {title: "Paciente", dataKey: "nmPaciente"},
                {title: "Serviço", dataKey: "nmServico"},
                {title: "Valor", dataKey: "vlServicoFormated"},
            ];
        };

        function prepareDataToPrint()
        {
            var data = [];
            var node = {};
            for (var i = 0; i < $scope.detalheFatura.lDespesaServicoConvenio.length; i++)
            {
                node = {};
                node = $scope.detalheFatura.lDespesaServicoConvenio[i];
                node.nmPaciente = (($scope.detalheFatura.lDespesaServicoConvenio[i].nmDependente != null) ? $scope.detalheFatura.lDespesaServicoConvenio[i].nmDependente : $scope.detalheFatura.lDespesaServicoConvenio[i].nmCliente);
                if("string" == (typeof node.dtVencimento) && node.dtVencimento.includes("-"))
                {
                    node.dtVencimento = moment(node.dtVencimento, 'YYYY-MM-DD').toDate();
                    node.dtVencimento = moment(node.dtVencimento).format('DD/MM/YYYY');
                }
                node.vlServicoFormated = "R$ " + Number.parseFloat(node.vlServico).toFixed(2);
                data.push(node);
            }
            return data;
        }

        function getTotalCost()
        {
            var total = 0;
            for (var i = 0; i < $scope.detalheFatura.lDespesaServicoConvenio.length; i++)
            {
                total += $scope.detalheFatura.lDespesaServicoConvenio[i].vlServico
            }
            return total;
        }

		function getDesconto()
		{
			if(undefined != $scope.detalheFatura.conferencia.desconto && null != $scope.detalheFatura.conferencia.desconto)
			{
				return $scope.detalheFatura.conferencia.desconto;
			} else
			{
				return 0;
			}
		}







		$scope.reabrirFatura = function(reaberturaFat,conveniada)
		{
			reaberturaFat.faturado = false;
			servicesFactory.reabrirFaturamentoCon.put({idEmpresaCliente: conveniada, periodo: reaberturaFat.mesAno},{}, function(response){
				$scope.disableButton = false;
				$scope.conFaturado = false;
				$scope.gerando = false;
			});
		}

		$scope.gerarFaturamentoConveniada = function(periodoSeletec, vlDesconto, nmObservacao, conveniada)
		{
			$scope.gerando = true;
			$scope.showInfos = false;
			servicesFactory.gerarFaturamentoCon.save({idEmpresaCliente: conveniada, periodo: periodoSeletec.mesAno, vlDesconto: vlDesconto, obsFatura: nmObservacao}, {},function(response){
		        var filePath = response.link;
		        var link = document.createElement('A');
		            link.href = filePath;
		            link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
		          document.body.appendChild(link);
		            link.click();
		          document.body.removeChild(link);
		        $scope.gerando = false;
		        $scope.disableButton = false;
		        $scope.conFaturado = true;

		        $scope.conveniada ={};
		        $scope.periodo = {selected: null};
		    });
		}
    },
});
