'use strict';

MedicsystemApp.controller('RelatorioMedicoDadosGeraisController', function($rootScope,$scope, $http, 
	$timeout,$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, Auth) {

	function configurarTable( lista ) {

		var initialParams = { count:100 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
	  };
	  
	$scope.carregarUnidadeTrabalhada = function (dados){
		var dtInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
		var dtFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");
		var idFuncionario = dados.idFuncionario;
		var idProfissionalRef = dados.idProfissionalRef;
		$scope.listaUnidades =[];
		servicesFactory.unidadeTrabalhada.get({
			idFuncionario: idFuncionario,
			dtInicio: dtInicio,
			dtFim: dtFim,
			idProfissionalRef: idProfissionalRef
		}, function(listaUnidades){
			dados.listaUnidades = listaUnidades;
				
		})
		
	};

	$scope.carregarProfissional = function (dadosFuncionario){
		var dtInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
		var dtFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");
		var dataValida = moment($scope.datePicker.date.startDate).isValid();
		var idFuncionario = dadosFuncionario.idFuncionario;
		$scope.horariosCheck =[];

		if(dataValida){
			servicesFactory.relatorioSalarioTipoMedico.get({idFuncionario: idFuncionario, dtInicio: dtInicio, dtFim: dtFim}, function(listaProfissionais){
				if(listaProfissionais){
					dadosFuncionario.dadosGeraisFuncionario = listaProfissionais;
					dadosFuncionario.dadosReferencia = null;
				}

				$scope.dadosPrint = dadosFuncionario.dadosGeraisFuncionario;
				var dados = dadosFuncionario.dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento;
				if(!dados) return;

				//mostrar horarios na tela
				for (var i=0; i < dados.length ;i++  ){
					var dtFormatada = moment(dados[i].dtRegistro).format("DD/MM/YYYY");
					dados[i].dtFormatada = dtFormatada;
					dados[i].horariosCheck = dados[i].hrPontoFinal.split('-');
					
					dados[i].hrFormatada = dados[i].hrTotal.split(':');
					dados[i].hrFormatada[2] = '00';
					dados[i].hrTotalFormatada = dados[i].hrFormatada[0] +':'+dados[i].hrFormatada[1]+':'+dados[i].hrFormatada[2];

					$scope.horariosCheck = dados[i].hrPontoFinal.split('-');

					dados[i].horariosCheck = $scope.horariosCheck; 
					dados[i].horariosCheck0 = $scope.horariosCheck[0];
					dados[i].horariosCheck1 = $scope.horariosCheck[1];
					dados[i].horariosCheck2 = $scope.horariosCheck[2];
					dados[i].horariosCheck3 = $scope.horariosCheck[3];
					dados[i].horariosCheck4 = $scope.horariosCheck[4];
					dados[i].horariosCheck5 = $scope.horariosCheck[5];
					dados[i].horariosCheck6 = $scope.horariosCheck[6];
					dados[i].horariosCheck7 = $scope.horariosCheck[7];
					dados[i].horariosCheck8 = $scope.horariosCheck[8];
					dados[i].horariosCheck9 = $scope.horariosCheck[9];
					dados[i].horariosCheck10 = $scope.horariosCheck[10];
					dados[i].horariosCheck11 = $scope.horariosCheck[11];
					dados[i].horariosCheck12 = $scope.horariosCheck[12];
					dados[i].horariosCheck13 = $scope.horariosCheck[13];
					dados[i].horariosCheck14 = $scope.horariosCheck[14];
					dados[i].horariosCheck15 = $scope.horariosCheck[15]; 
					dados[i].tamanhoHorarios =dados[i].horariosCheck.length;
					
				}
				
				var registrosHoristaAtend = dadosFuncionario.dadosGeraisFuncionario.registrosHorista.listAtendimentoCliente;
				if( registrosHoristaAtend !=  null){
					$scope.unidadeGroupHorista = [];
					for(var j=0; j < registrosHoristaAtend.length; j++){
						var index = getIndex($scope.unidadeGroupHorista, 'idUnidade', registrosHoristaAtend[j]);
						if(index == undefined){
							var objeto ={
								idUnidade: registrosHoristaAtend[j].idUnidade,
								nmUnidade: registrosHoristaAtend[j].nmUnidade,
								registrosHoristaAtend: [registrosHoristaAtend[j]]
							}
							$scope.unidadeGroupHorista.push(objeto);
						} else {
							$scope.unidadeGroupHorista[index].registrosHoristaAtend.push(registrosHoristaAtend[j]);
						}
					}
					dadosFuncionario.dadosGeraisFuncionario.registrosHorista.listAtendimentoCliente = $scope.unidadeGroupHorista;
				}
				
				var registroHoristaHoras = dadosFuncionario.dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento;
				if(registroHoristaHoras != null){
					$scope.unidadeGroupHoraHorista =[];
					for(var j=0; j< registroHoristaHoras.length; j++){
						var index = getIndex($scope.unidadeGroupHoraHorista, 'idUnidade', registroHoristaHoras[j]);
						if(index == undefined){
							var obj={
								idUnidade: registroHoristaHoras[j].idUnidade,
								registroHoristaHoras: [registroHoristaHoras[j]]
								//maxLengthRegistroHorista: registroHoristaHoras[j].horariosCheck.length
							}
							$scope.unidadeGroupHoraHorista.push(obj);
						} else {
							$scope.unidadeGroupHoraHorista[index].registroHoristaHoras.push(registroHoristaHoras[j]);
							//if($scope.unidadeGroupHoraHorista[index].maxLengthRegistroHorista < registroHoristaHoras[j].horariosCheck.length) {
							//	$scope.unidadeGroupHoraHorista[index].maxLengthRegistroHorista = registroHoristaHoras[j].horariosCheck.length;
							//}
						}
					}

					for (var i=0; i < $scope.unidadeGroupHoraHorista.length; i++){
						var salarioHoristaTotalUnidade = getSalarioHoristaUnidade($scope.unidadeGroupHoraHorista[i]);
						$scope.unidadeGroupHoraHorista[i].salarioHoristaUnidade = salarioHoristaTotalUnidade;
					}

					dadosFuncionario.dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento = $scope.unidadeGroupHoraHorista;
					
				}
				
				var registroFixoAtend = dadosFuncionario.dadosGeraisFuncionario.registrosFixo.listAtendimentoCliente;
				if(registroFixoAtend != null ){
					$scope.unidadeGroupAtendFixo = [];
					for(var i=0; i < registroFixoAtend.length; i++){
						var index = getIndex($scope.unidadeGroupAtendFixo, 'idUnidade', registroFixoAtend[i]);
						if(index == undefined){
							var objeto ={
								idUnidade: registroFixoAtend[i].idUnidade,
								nmUnidade: registroFixoAtend[i].nmUnidade,
								registroFixoAtend: [registroFixoAtend[i]]
							}
							$scope.unidadeGroupAtendFixo.push(objeto);
						} else{
							$scope.unidadeGroupAtendFixo[index].registroFixoAtend.push(registroFixoAtend[i]);
						}
					}
					dadosFuncionario.dadosGeraisFuncionario.registrosFixo.listAtendimentoCliente = $scope.unidadeGroupAtendFixo;
				}

				var registrosProdAtend = dadosFuncionario.dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente;
				if(registrosProdAtend != null){
					$scope.unidadeGroupProducao = [];
					for(var i=0; i < registrosProdAtend.length;i++){
						var index = getIndex($scope.unidadeGroupProducao, 'idUnidade', registrosProdAtend[i]);
						if(index == undefined){
							var objet={
								idUnidade: registrosProdAtend[i].idUnidade,
								nmUnidade: registrosProdAtend[i].nmUnidade,
								registrosProdAtend: [registrosProdAtend[i]]
							}
							$scope.unidadeGroupProducao.push(objet);
						} else {
							$scope.unidadeGroupProducao[index].registrosProdAtend.push(registrosProdAtend[i]);
						}
					}
					for (var i=0; i < $scope.unidadeGroupProducao.length; i++){
						var salarioProdUnidade = getSalarioProducaoUnidade($scope.unidadeGroupProducao[i]);
						$scope.unidadeGroupProducao[i].salarioProducaoUnidade = salarioProdUnidade;
					}
					dadosFuncionario.dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente = $scope.unidadeGroupProducao;
				}

				var registrosFixoHoras = dadosFuncionario.dadosGeraisFuncionario.registrosFixo.listFixoAtendimento;
				if(registrosFixoHoras != null){
					$scope.unidadeGroupHorasFixo=[];
					for(var i=0; i < registrosFixoHoras.length; i ++){
						var index = getIndex($scope.unidadeGroupHorasFixo, 'idUnidade', registrosFixoHoras[i]);
						if(index == undefined){
							var obj={
								idUnidade: registrosFixoHoras[i].idUnidade,
								nmUnidade: registrosFixoHoras[i]. nmUnidade,
								registrosFixoHoras: [registrosFixoHoras[i]]
							}
							$scope.unidadeGroupHorasFixo.push(obj);
						} else{
							$scope.unidadeGroupHorasFixo[index].registrosFixoHoras.push(registrosFixoHoras[i]);
						}
					}

					for(var j=0; j< $scope.unidadeGroupHorasFixo.length;j++){
						var salarioFixoUnidade = getSalarioFixoUnidade($scope.unidadeGroupHorasFixo[j]);
						$scope.unidadeGroupHorasFixo[j].salarioTotalFixoUnidade = salarioFixoUnidade;
					}
					dadosFuncionario.dadosGeraisFuncionario.registrosFixo.listFixoAtendimento = $scope.unidadeGroupHorasFixo;
				}
			//console.log("DadosFun", dadosFuncionario);
			
			}).$promise.finally(function() {
				$scope.totalLoadsProfissional += 1; 
				if($scope.totalLoadsProfissional == $scope.dadosGeraisInvestimentoMedico.length) {
					$scope.$broadcast('dados.loaded');
				}
			});
			//$scope.tableParamsRef = dadosFuncionario.idProfissionalRef;
		}
	};

	function getSalarioFixoUnidade(dados){
		var salarioTotalFixoUnidade = 0;
		for (var i=0; i < dados.registrosFixoHoras.length; i++){
			if(dados.registrosFixoHoras[i].statusDesconto == 0 || dados.registrosFixoHoras[i].statusDesconto == 2){
				salarioTotalFixoUnidade += dados.registrosFixoHoras[i].vlDiaria;
			}
		}
		return salarioTotalFixoUnidade;
	}

	function getSalarioHoristaUnidade(dadosUnidade){
		var salarioHoristaTotalUnidade = 0;
		for(var j=0; j < dadosUnidade.registroHoristaHoras.length; j++){
			salarioHoristaTotalUnidade += dadosUnidade.registroHoristaHoras[j].vlTotal;
		}
 	
		return salarioHoristaTotalUnidade;
	}

	function getSalarioProducaoUnidade(dadosProdUnidade){
		var salarioProdTotalUnidade = 0;
		for (var i=0; i < dadosProdUnidade.registrosProdAtend.length; i++){
			salarioProdTotalUnidade += dadosProdUnidade.registrosProdAtend[i].vlComissao;
		}

		return salarioProdTotalUnidade;
	}
	
	$scope.getCalcularTotalUnidade = function(tHorista, tFixo, tProducao, tRespTec){
		if(tHorista == undefined || tFixo == undefined || tProducao == undefined || tRespTec == undefined) return;
		var total  = tHorista + tFixo + tProducao + tRespTec;
		if(!total) return;
		return total;
	}

	function getGeralHorista(dados){
		if(!dados) return;
		var salarioGeralHoristaJoaquim = 0;
		var salarioGeralHoristaTaruma = 0;
		var salarioGeralHoristaGrande = 0;
		for(var i = 0; i < dados.length; i++){
			for(var j=0; j < dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento.length; j++){
				if(dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].idUnidade  == 1){
					salarioGeralHoristaJoaquim += dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].salarioHoristaUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].idUnidade  == 2){
					salarioGeralHoristaTaruma += dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].salarioHoristaUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].idUnidade  == 7){
					salarioGeralHoristaGrande += dados[i].dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento[j].salarioHoristaUnidade;
				}
			}
		}

		$scope.salarioGeralHoristaJoaquim = salarioGeralHoristaJoaquim;
		$scope.salarioGeralHoristaTaruma = salarioGeralHoristaTaruma;
		$scope.salarioGeralHoristaGrande = salarioGeralHoristaGrande;

	}

	function getGeralFixo(dados){
		if(!dados) return;
		var salarioGeralFixoJoaquim =0;
		var salarioGeralFixoTaruma = 0;
		var salarioGeralFixoGrande = 0;
		for(var i=0; i< dados.length; i++){
			for(var j=0; j < dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento.length; j++){
				if(dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].idUnidade  == 1){
					salarioGeralFixoJoaquim += dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].salarioTotalFixoUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].idUnidade  == 2){
					salarioGeralFixoTaruma += dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].salarioTotalFixoUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].idUnidade  == 7){
					salarioGeralFixoGrande += dados[i].dadosGeraisFuncionario.registrosFixo.listFixoAtendimento[j].salarioTotalFixoUnidade;
				}
			}
		
		}
		$scope.salarioGeralFixoJoaquim = salarioGeralFixoJoaquim;
		$scope.salarioGeralFixoTaruma = salarioGeralFixoTaruma;
		$scope.salarioGeralFixoGrande = salarioGeralFixoGrande;

	}

	function getGeralProducao(dados){
		if(!dados) return;
		var salarioGeralProdJoaquim = 0;
		var salarioGeralProdTaruma = 0;
		var salarioGeralProdGrande = 0;

		for(var i=0; i< dados.length; i++){
			for(var j=0; j < dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente.length; j++){
				if(dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].idUnidade  == 1 && dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].idUnidade  != 8 ){
					salarioGeralProdJoaquim += dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].salarioProducaoUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].idUnidade  == 2){
					salarioGeralProdTaruma += dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].salarioProducaoUnidade;
				} else if(dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].idUnidade  == 7){
					salarioGeralProdGrande += dados[i].dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente[j].salarioProducaoUnidade;
				}
			}
		}

		$scope.salarioGeralProdJoaquim = salarioGeralProdJoaquim;
		$scope.salarioGeralProdTaruma = salarioGeralProdTaruma;
		$scope.salarioGeralProdGrande = salarioGeralProdGrande;
	}
    
    function getGeralRespTec(dados){
		if(!dados) return;
		var salarioGeralRespTecJoaquim = 0;
		var salarioGeralRespTecTaruma = 0;
		var salarioGeralRespTecGrande = 0;

		for(var i=0; i< dados.length; i++){
			for(var j=0; j < dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico.length; j++){
				if(dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].idUnidade  == 1 
                   && dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].idUnidade  != 8 ){
					salarioGeralRespTecJoaquim += dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].vlTotal;
				} else if(dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].idUnidade  == 2){
					salarioGeralRespTecTaruma += dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].vlTotal;
				} else if(dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].idUnidade  == 7){
					salarioGeralRespTecGrande += dados[i].dadosGeraisFuncionario.respTecnico.listRespTecnico[j].vlTotal;
				}
			}
		}

		$scope.salarioGeralRespTecJoaquim = salarioGeralRespTecJoaquim;
		$scope.salarioGeralRespTecTaruma = salarioGeralRespTecTaruma;
		$scope.salarioGeralRespTecGrande = salarioGeralRespTecGrande;
	}

	$scope.getCalcularTamanho = function(lista, unidade){
		if (!lista) return;
		var tamanho = calcularTamanho(lista, unidade);
		return tamanho;
	}

	function calcularTamanho(lista, unidade){
		for (var i=0; i < lista.length; i++){
			if(unidade == lista[i].idUnidade){
				var tamanho = lista[i].registrosFixoHoras.length;
			}
		}
		return tamanho;
	}

	function getIndex(list, prop, element){
		for (var i=0; i< list.length; i++){
			if(list[i][prop] == element[prop]){
				return i;
			}
		}
		return;
	}

	$scope.getQtdServicos = function(dados, isConsulta, unidade){
		if (!dados) return 0;
		var qtd = 0;
		for (var i=0; i < dados.length ; i++){
			if(unidade == dados[i].idUnidade){
				for (var j=0; j < dados[i].registrosProdAtend.length; j++){
					if(dados[i].registrosProdAtend[j].boConsulta == isConsulta){
						qtd++;
					}
				}
			}
		}
		return qtd;
	}

	$scope.getQtdServicosNaoRetorno = function(dados, isConsulta, unidade){
		if(!dados) return 0;
		var qtd = 0;
		for(var i=0; i< dados.length; i++){
			if(unidade == dados[i].idUnidade){
				for(var j=0; j < dados[i].registrosProdAtend.length; j++){
					if(dados[i].registrosProdAtend[j].boConsulta == isConsulta && 
                       (dados[i].registrosProdAtend[j].boRetorno == false || dados[i].registrosProdAtend[j].boRetorno == null)){
						qtd++;
					}
				}
			}
		}
		return qtd;
	}

	$scope.selectTab = function(tabId) {
		var link = angular.element("#" + tabId).parent();

		$timeout(function() {
			link.trigger('click');
		}, 0);
		
	}
	
	$scope.tableSorted = function (id){
		$(function(){
			$("#" + id).tablesorter({ sortList: [[0,0], [1,0]] });
		  });
	}

	$scope.getQtdServicosRetorno = function(dados, isConsulta, unidade){
		if(!dados) return 0;
		var qtdRetorno = 0;
		for(var i=0; i< dados.length; i++){
			if(unidade == dados[i].idUnidade){
				for(var j=0; j < dados[i].registrosProdAtend.length; j++){
					if(dados[i].registrosProdAtend[j].boConsulta == isConsulta && dados[i].registrosProdAtend[j].boRetorno == true){
						qtdRetorno++;
					}
				}
			}
		}
		return qtdRetorno;
	}

	$scope.getValorTotalComissao = function(dados, isConsulta, unidade){
		if(!dados) return 0;
		var total =0;
		var qtd = 0;
		for(var i = 0; i <dados.length; i++){
			if(unidade == dados[i].idUnidade){
				for (var j=0; j <dados[i].registrosProdAtend.length; j++){
					if(dados[i].registrosProdAtend[j].boConsulta == isConsulta){
						qtd ++;
						total += dados[i].registrosProdAtend[j].vlComissao;
					}
				}
			}
		}
		
		return total;
	}

	
	function getSalarioGeral(lista){
		var salarioGeral = 0.0;
		for(var i=0; i < lista.length; i++){
			salarioGeral += lista[i].dadosGeraisFuncionario.vlSalario;
		}

		$scope.salarioTotalGeral = salarioGeral;
	}

	function getSalarioHoristaGeral(lista){
		var salarioHoristaGeral = 0;
		for(var i=0; i < lista.length; i++){
			salarioHoristaGeral += lista[i].dadosGeraisFuncionario.registrosHorista.vlSalario;
		}

		$scope.salarioHoristaGeral = salarioHoristaGeral;
	}

	function getSalarioFixoGeral(lista){
		var salarioFixoGeral = 0;
		for(var i = 0; i < lista.length; i++){
			salarioFixoGeral += lista[i].dadosGeraisFuncionario.registrosFixo.vlSalario;
		}
		$scope.salarioFixoGeral = salarioFixoGeral;
	}

	function getSalarioProducaoGeral(lista){
		var salarioProducaoGeral = 0;
		for(var i=0; i < lista.length; i++){
			salarioProducaoGeral += lista[i].dadosGeraisFuncionario.registrosProducao.vlSalario;
		}
		$scope.salarioProducaoGeral = salarioProducaoGeral;
	}
    
    function getSalarioRespTecGeral(lista){
		var salarioRespTecGeral = 0;
		for(var i=0; i < lista.length; i++){
			salarioRespTecGeral += lista[i].dadosGeraisFuncionario.respTecnico.vlSalario;
		}
		$scope.salarioRespTecGeral = salarioRespTecGeral;
	}

	function calcularValorMedioCargaHoraria (valor, unidade){
		var resultadoFinal = 0;
		var resultado = 0;
		for(var i=0; i < valor.length; i++){
			if( unidade == valor[i].idUnidade){
				for(var j=0; j < valor[i].registroHoristaHoras.length; j++){
					resultado += valor[i].registroHoristaHoras[j].vlHora;
				}
				resultadoFinal = (resultado / valor[i].registroHoristaHoras.length);
			}
		}
		return resultadoFinal;
	}

	$scope.getValorMedio = function(resultados, unidade){
		
		if(!resultados ) return;
		var valormedio = calcularValorMedioCargaHoraria(resultados, unidade);

		return valormedio;
	};


	$scope.getCalcularSalarioUnidade = function(lista, unidade, tipo){
		if(!lista) return;
		var salarioFinal = calcularSalarioUnidade(lista, unidade, tipo);	
		return salarioFinal;
	};

	function calcularSalarioUnidade(lista, unidade, tipo){

		//Tipo: 0-Comissao, 1-Fixo, 2-Horista, 3- Particular -SOMENTE NESSE MODAL
		var salario = 0;
		if(tipo == 1){
			for (var i=0; i < lista.length; i++){
				if( unidade == lista[i].idUnidade){
					for(var j=0; j < lista[i].registrosFixoHoras.length; j++){
						if( lista[i].registrosFixoHoras[j].statusDesconto == 0 || lista[i].registrosFixoHoras[j].statusDesconto == 2){
							salario += lista[i].registrosFixoHoras[j].vlDiaria;
						}
					}
				}
			}
		} else if(tipo == 2){
			for (var i=0; i < lista.length; i++){
				if( unidade == lista[i].idUnidade){
					for(var j=0; j < lista[i].registroHoristaHoras.length; j++){
						salario += lista[i].registroHoristaHoras[j].vlTotal;
					}
				}
			}
		}
		
		return salario;
	}

	$scope.getCalcularCargaHoraria = function(lista, unidade){
		if(!lista) return;
		var total =  moment.duration(0, 'minutes');
		for(var i=0; i < lista.length; i++){
			if(unidade == lista[i].idUnidade){
				var parcial; var total =moment.duration(0, 'minutes');
				for(var j=0; j < lista[i].registroHoristaHoras.length; j++){
					parcial = moment.duration(lista[i].registroHoristaHoras[j].hrTotalFormatada, 'minutes');
					total += parcial;
				}
				
				var dia = moment.duration(total).days();
				var horas = moment.duration(total).hours();
				var minutos = moment.duration(total).minutes();
				var segundos = '00';
		
				var horasParcial = ((dia *24)+ horas);
				if(horasParcial < 10){
					horasFinal = '0'+ horasParcial;
				} else {
					var horasFinal = horasParcial;
				}

				if(minutos < 10){
					var minutosFinal = '0'+minutos;
				} else {
					var minutosFinal = minutos;
				}
		
				/* if(segundos < 10){
					var segundosFinal = '0'+segundos;
				} else {
					var segundosFinal = segundos;
				} */
			
				var totalHoras = horasFinal + ':' + minutosFinal + ':' + segundos;
			}
		}
		
		$scope.cargaHoraria = totalHoras;
		return totalHoras;
	};

	$scope.carregarDadosProfissionais = function() {

		var dataInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
		var dataFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");
		var dataValida = moment($scope.datePicker.date.startDate).isValid();
		$scope.dadosGeraisInvestimentoMedico = [];
		
		if( dataValida ) {
			servicesFactory.relatorioSalarioMedicoDadosGerais.get( {dataInicio: dataInicio, dataFim: dataFim},function( result ) {
				if(result) {
					//console.log(result);
					$scope.dadosGeraisInvestimentoMedico = result;
					configurarTable($scope.dadosGeraisInvestimentoMedico);
					if(!$scope.dadosGeraisInvestimentoMedico) return;
					$scope.totalLoadsProfissional = 0;
					$scope.totalLoadsUnidades = 0;
					for(var i=0; i < $scope.dadosGeraisInvestimentoMedico.length; i++) {
						$scope.carregarProfissional($scope.dadosGeraisInvestimentoMedico[i]);
						$scope.carregarUnidadeTrabalhada($scope.dadosGeraisInvestimentoMedico[i]);
					}
				}
			});	
		}
		
	};

	$scope.$on('dados.loaded', function() {
		getSalarioGeral($scope.dadosGeraisInvestimentoMedico);
		getSalarioHoristaGeral($scope.dadosGeraisInvestimentoMedico);
		getSalarioFixoGeral($scope.dadosGeraisInvestimentoMedico);
		getSalarioProducaoGeral($scope.dadosGeraisInvestimentoMedico);
        getSalarioRespTecGeral($scope.dadosGeraisInvestimentoMedico);
		getGeralHorista($scope.dadosGeraisInvestimentoMedico);
		getGeralFixo($scope.dadosGeraisInvestimentoMedico);
		getGeralProducao($scope.dadosGeraisInvestimentoMedico);
        getGeralRespTec($scope.dadosGeraisInvestimentoMedico);
		getProfissionalRef($scope.dadosGeraisInvestimentoMedico);
	})


	var getProfissionalRef = function(dadosProf){
		for(var j = 0; j < dadosProf.length; j++){
			if(dadosProf[j].idProfissionalRef != null){
				var referencia = dadosProf[j].idProfissionalRef;				
				for(var i=0; i < dadosProf.length; i++){
					if(referencia == dadosProf[i].idFuncionario){
						if(!dadosProf[i].dadosReferencia) {
							dadosProf[i].dadosReferencia = [];
						}
						dadosProf[i].dadosReferencia.push(dadosProf[j]);
					}
				}
			}

		}
		getSalario(dadosProf);
		$scope.tableParamsRef = dadosProf;

		$scope.tableParamsRef = new NgTableParams();
		
	}
	function getSalario(listaProf){
		if(!listaProf) return;
		var salarioTotalProf =[];
		var salarioTotal = [];
		for(var i=0; i < listaProf.length; i++){
            var salarioRef = 0;

			if(listaProf[i].dadosReferencia == null){
				salarioTotal.push(listaProf[i].dadosGeraisFuncionario.vlSalario);
			} else{
				for(var j=0; j < listaProf[i].dadosReferencia.length; j++){
					salarioRef += listaProf[i].dadosReferencia[j].dadosGeraisFuncionario.vlSalario;
				}
				salarioTotal.push(listaProf[i].dadosGeraisFuncionario.vlSalario + salarioRef);
			}
			for(var j=0; j < salarioTotal.length; j++){
				listaProf[i].dadosGeraisFuncionario.salarioFinal = salarioTotal[j];
			}
		}
	}

	var configurarDatePickerRange = function() {

		$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {

	   		/* 'Hoje': [moment(), moment()],
       		'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       		'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
			'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()], */
       		'Este mês': [moment().startOf('month'), moment().endOf('month')],
			'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'Dois Meses Atrás': [moment().subtract(2, 'month').startOf('month'), moment().subtract(2, 'month').endOf('month')]   
		}
	};

	var inicializarData = function() {
		$scope.datePicker = {};
		/* $scope.maxDate = new Date(); */
		$scope.maxDate = moment().endOf('month');
		$scope.datePicker.date = {startDate: null, endDate: null};	
	}

	function inicio() {
		$scope.dadosGeraisFuncionario =[];
		$scope.dadosReferencia =[];
		inicializarData();
		configurarDatePickerRange();
	}

	inicio();

	// Modal Para Visualizar Procedimento Comissao
	$scope.modalVisualProcedimentosComissao = function (lista,tipo, unidade){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/modal/procedimentos-modal.html',
			controller: 'ModalVisualProcedimentoCtrl',
			size:'lg',
			resolve:{
				lista: function (){
					return lista;
				},
				tipo: function(){
					return tipo;
				},
				unidade: function(){
					return unidade;
				},
				totalProcedimentos: function(){
					return $scope.getQtdServicos(lista.dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente, tipo, unidade);
				}
			}
			
		});

	}

	// modal para visualizar dias trabalhados Fixo
	$scope.modalVisualDiasTrabalhados = function(lista, unidade, tipo){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/modal/dias-trabalhados-modal.html',
			controller: 'ModalVisualDiasTrabalhadosCtrl',
			size:'md',
			resolve:{
				lista: function(){
					return lista;
				}, 
				unidade: function(){
					return unidade;
				}
			}
		});
	}

	// Modal Para Visualizar Procedimento Nao Comissao
	$scope.modalVisualProcedimentos = function (lista, tipo, unidade){
		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/modal/procedimentos-naoComissao-modal.html',
			controller: 'ModalVisualProcedimentoNaoComissaoCtrl',
			size:'lg',
			resolve:{
				lista: function (){
					return lista;
				},
				tipo: function(){
					return tipo;
				},
				unidade: function(){
					return unidade;
				}
			}
			
		});

	}
	
	//modal para visualizar lista de carga horaria
	$scope.modalVisualCargaHoraria = function(listaCargaHoraria, unidade){

		var modalInstance = $modal.open({
			animation: true,
			backdrop: 'static',
			templateUrl: 'templates/admin4_material_design/angularjs/views/relatorio/modal/cargahoraria-modal.html',
			controller: 'ModalVisualCargaHorariaCtrl',
			size: 'lg',
			resolve:{
				listaCargaHoraria: function(){
					return listaCargaHoraria;
				}, 
				unidade: function(){
					return unidade;
				},
				cargaHoraria: function() {
					return $scope.getCalcularCargaHoraria(listaCargaHoraria.dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento, unidade);
				}
			}
		});
	} 

});


MedicsystemApp.controller('ModalVisualProcedimentoCtrl', function($scope, $modalInstance, servicesFactory, lista, tipo,totalProcedimentos, unidade, NgTableParams){


	function inicioModal(){
		$scope.totalServico = totalProcedimentos;
		$scope.isConsulta = tipo;
		$scope.dadosProcedimentos = lista.dadosGeraisFuncionario.registrosProducao.listAtendimentoCliente;

		$scope.getValorTotalServico = function(dados, isConsulta, idServico){
			if(!dados) return 0;
			var total =0;
			for(var i = 0; i <dados.length; i++){
				if(idServico == dados[i].idServico){
					if(dados[i].boConsulta == isConsulta){
						total += dados[i].vlComissao;
					}
				}
			}
			return total;
		}

		var dados = $scope.dadosProcedimentos;
		if(!dados) return;
		$scope.servGroup = []; 
		$scope.servGroup2 = [];
		for (var i=0; i <dados.length; i++){
			
            for(var j=0; j < dados[i].registrosProdAtend.length; j++){
                var dtFormatada = moment(dados[i].registrosProdAtend[j].dtRegistro).format("DD/MM/YYYY");
                dados[i].registrosProdAtend[j].dtFormatada = dtFormatada;
                if(unidade == dados[i].idUnidade){
                    if(tipo == true && dados[i].registrosProdAtend[j].boConsulta == true){
                        var index = getIndex($scope.servGroup, 'idServico', dados[i].registrosProdAtend[j]);
                        if(index == undefined){
                            var obj={
                                idServico: dados[i].registrosProdAtend[j].idServico,
                                nmServico: dados[i].registrosProdAtend[j].nmServico,
                                registrosProdAtend: [dados[i].registrosProdAtend[j]]
                            }
                            $scope.servGroup.push(obj);						
                        } else{
                            $scope.servGroup[index].registrosProdAtend.push(dados[i].registrosProdAtend[j]);
                        }

                    } else if(tipo == false && dados[i].registrosProdAtend[j].boConsulta == false){
                        var index = getIndex($scope.servGroup, 'idServico', dados[i].registrosProdAtend[j]);
                        if(index == undefined){
                            var obj={
                                idServico: dados[i].registrosProdAtend[j].idServico,
                                nmServico: dados[i].registrosProdAtend[j].nmServico,
                                registrosProdAtend: [dados[i].registrosProdAtend[j]]
                            }

                            $scope.servGroup.push(obj);						
                        } else{
                            $scope.servGroup[index].registrosProdAtend.push(dados[i].registrosProdAtend[j]);
                        }
                    }
                }
            }				
		}
        $scope.tablePrint= dados;
		$scope.obsTableParams1 = configurarTabela($scope.servGroup);
					
	}


	function getIndex(list, prop, element){
		for (var i=0; i< list.length; i++){
			if(list[i][prop] == element[prop]){
				return i;
			}
		}
		return;
	}

 	function configurarTabela(lista){
	
		var tableParams = new NgTableParams();
			if(lista.length > 0){
				var initialParams = { count: 10};
				var initialSettings = {
					// page size buttons (right set of buttons in demo)
					counts:[50,100], 
					// determines the pager buttons (left set of buttons in demo)
					paginationMaxBlocks: 13,
					paginationMinBlocks: 2,
					dataset: lista
				};
				tableParams = new NgTableParams(initialParams, initialSettings); 
			}
			
		return tableParams;
	}; 


    $scope.gerarPdf = function() {
		getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
	};
	
	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
	};

 	var print = function ( imgData ){
		
        var doc = new jsPDF('p','pt','letter','a4');
		var rows = [];        
        var totalAtendimentos = 0;
        var totalComissionados =0;
        var totalNaoComissionados = 0;
        var totalSalarioComissao = 0;
        var count = 0;
        
		var columns=[
			{title: "Data", dataKey: "dtFormatada"},
			{title: "Paciente", dataKey: "paciente"},
			{title: "Serviço", dataKey:"servico"},
			{title: "Valor Comissão", dataKey:"valorComissao"}
		];          
		console.log("tablePrint", $scope.tablePrint);      
        
		for(var i=0; i < $scope.tablePrint.length; i++){			
			for(var k =0; k < $scope.tablePrint[i].registrosProdAtend.length; k++){                
				rows[count] = {
					"dtFormatada": $scope.tablePrint[i].registrosProdAtend[k].dtFormatada,
					"paciente": $scope.tablePrint[i].registrosProdAtend[k].nmPaciente,
					"servico": $scope.tablePrint[i].registrosProdAtend[k].nmServico,
					"valorComissao": "R$ "+$scope.tablePrint[i].registrosProdAtend[k].vlComissao
				}
				if($scope.tablePrint[i].registrosProdAtend[k].vlComissao != 0){
					totalComissionados++;
				} else{
					totalNaoComissionados++;
				}
                count++;
			}
			totalAtendimentos += $scope.tablePrint[i].registrosProdAtend.length;	
			totalSalarioComissao += $scope.tablePrint[i].salarioProducaoUnidade;   
	  	}

	  
	  //CABECALHO	
	  doc.addImage(imgData, 'JPEG', 205, 32, 200/*width*/, 43/*heigth*/, 'logo');

	  doc.setFont("times");
	  doc.setFontType('bold')
	  doc.setFontSize(14);
	  doc.writeText(0,120, lista.nmFuncionario,{align:'center'});
	  doc.setFontSize(8);
	  doc.writeText(80, 140, 'Total Atendimentos: ' + totalAtendimentos);
	  doc.writeText(180, 140, 'Comissionados: '+ totalComissionados);
	  doc.writeText(260, 140, 'Não Comissionados: '+ totalNaoComissionados);
	  doc.setFontType('bold');
	  doc.writeText(500, 140, 'Total: R$ ' + totalSalarioComissao.toFixed(2))
	  // LINE 1
	  doc.autoTable(columns, rows, {margin: {top: 150}, styles:{fontSize: 7}}); 
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');

	};
	  
	$scope.close = function (){
		$modalInstance.close();
	};

	inicioModal();

});

MedicsystemApp.controller('ModalVisualCargaHorariaCtrl', function($scope, $modalInstance, servicesFactory, listaCargaHoraria, unidade,cargaHoraria, NgTableParams){

	function inicoModalCarga(){
		$scope.dadosCarga = listaCargaHoraria; // somente para pegar a carga horario e mostrar no modal
		$scope.cargaHoraria = cargaHoraria;

		var dadosProcedimentos = listaCargaHoraria.dadosGeraisFuncionario.registrosHorista.listHorarioAtendimento;
		$scope.dados=[];
		for(var i=0; i < dadosProcedimentos.length; i++){
			if(unidade == dadosProcedimentos[i].idUnidade){
				for (var j=0; j < dadosProcedimentos[i].registroHoristaHoras.length; j++){
					$scope.dados[j] = dadosProcedimentos[i].registroHoristaHoras[j];
				}
				break;
			}
		}
		
		$scope.obsTableParams2 = new NgTableParams($scope.dados);
		$scope.obsTableParams = configurarTabela($scope.dados);

	}

	$scope.getArrayRange = function(begin, end) {
		begin = begin || 0;
		var arr = [];
		for(var i = begin; i < end; ++i) {
			arr.push(i);
		}
		return arr;
	}

 	function configurarTabela(lista){
	
		var tableParams = new NgTableParams();
		if(lista.length > 0){
			var initialParams = { count: 10};
			var initialSettings = {
				// page size buttons (right set of buttons in demo)
				counts:[50,100], 
				// determines the pager buttons (left set of buttons in demo)
				paginationMaxBlocks: 13,
				paginationMinBlocks: 2,
				dataset: lista
			};
			
			tableParams = new NgTableParams(initialParams, initialSettings) 
		}
			
			return tableParams;
	}; 

	$scope.close = function(){
		$modalInstance.close();
	};

	inicoModalCarga();

});

MedicsystemApp.controller('ModalVisualDiasTrabalhadosCtrl', function($scope, $modalInstance, servicesFactory, lista, unidade, NgTableParams){

	
	function inicioModalDias(){
		var dadosProcedimentos = lista;
		$scope.dados=[];
		for (var i=0; i < dadosProcedimentos.length; i ++){
			if(unidade == dadosProcedimentos[i].idUnidade){
				for (var j=0; j < dadosProcedimentos[i].registrosFixoHoras.length; j++){
					var dtFormatada = moment(dadosProcedimentos[i].registrosFixoHoras[j].dtRegistro).format("DD/MM/YYYY");
					dadosProcedimentos[i].registrosFixoHoras[j].dtFormatada = dtFormatada;
					$scope.dados[j] = dadosProcedimentos[i].registrosFixoHoras[j];
	
				}
			}

		}		
		$scope.obsTableParams1 = configurarTabela($scope.dados);

	}
	
	$scope.getCalcularSalario = function(listaSalario){
		var salario = 0;
		for(var i=0; i < listaSalario.length; i++){
			if(listaSalario[i].statusDesconto == 0 || listaSalario[i].statusDesconto == 2){
				salario += listaSalario[i].vlDiaria;
			}
		}
		return salario;
	}
	$scope.getCalcularSalarioDescontado = function(listaSalario){
		var salarioDesc = 0;
		for(var i=0; i < listaSalario.length; i++){
			if(listaSalario[i].statusDesconto == 1){
				salarioDesc += listaSalario[i].vlDiaria;
			}
		}
		return salarioDesc;
	}

	$scope.getCalcularSalarioBruto = function(listaSalario){
		var salarioBruto = 0;
		for(var i=0; i < listaSalario.length; i++){
			salarioBruto += listaSalario[i].vlDiaria;
		}
		return salarioBruto;
	}
	/* $scope.getCalcularSalarioUnidade = function(lista, unidade, tipo){
		if(!lista) return;
		var salarioFinal = calcularSalarioUnidade(lista, unidade, tipo);	
		return salarioFinal;
	};

	function calcularSalarioUnidade(lista, unidade, tipo){

		//Tipo: 0-Comissao, 1-Fixo, 2-Horista, 3- Particular -SOMENTE NESSE MODAL
		var salario = 0;
		if(tipo == 1){
			for (var i=0; i < lista.length; i++){
				if( unidade == lista[i].idUnidade){
					for(var j=0; j < lista[i].registrosFixoHoras.length; j++){
						if( lista[i].registrosFixoHoras[j].statusDesconto == 0 || lista[i].registrosFixoHoras[j].statusDesconto == 2){
							salario += lista[i].registrosFixoHoras[j].vlDiaria;
						}
					}
				}
			}
		} else if(tipo == 2){
			for (var i=0; i < lista.length; i++){
				if( unidade == lista[i].idUnidade){
					for(var j=0; j < lista[i].registroHoristaHoras.length; j++){
						salario += lista[i].registroHoristaHoras[j].vlTotal;
					}
				}
			}
		}
		
		return salario;
	} */
	function configurarTabela(lista){
		
		var tableParams = new NgTableParams();
			if(lista.length > 0){
				var initialParams = { count: 10};
				var initialSettings = {
					// page size buttons (right set of buttons in demo)
					counts:[50,100], 
					// determines the pager buttons (left set of buttons in demo)
					paginationMaxBlocks: 13,
					paginationMinBlocks: 2,
					dataset: lista
				};
				tableParams = new NgTableParams(initialParams, initialSettings) 
			}
			
			return tableParams;
	}; 
	$scope.close = function (){
		$modalInstance.close();
	};


	inicioModalDias();
});

MedicsystemApp.controller('ModalVisualProcedimentoNaoComissaoCtrl', function($scope, $modalInstance, servicesFactory, lista, tipo, unidade, NgTableParams){
	
	//Tipo: 0-Comissao, 1-Fixo, 2-Horista, 3- Particular
        console.log("lista", lista);
		function inicioModal(){
            if (tipo ==1){
                var dadosProcedimentos = lista.dadosGeraisFuncionario.registrosFixo.listAtendimentoCliente;
				$scope.dados=[];
				for(var i=0; i < dadosProcedimentos.length; i++){
					if(unidade == dadosProcedimentos[i].idUnidade){
						for (var j=0; j < dadosProcedimentos[i].registroFixoAtend.length; j++){
							var dtFormatada = moment(dadosProcedimentos[i].registroFixoAtend[j].dtRegistro).format("DD/MM/YYYY");
							dadosProcedimentos[i].registroFixoAtend[j].dtFormatada = dtFormatada;	
							$scope.dados[j] = dadosProcedimentos[i].registroFixoAtend[j];
						}
					}
				}
			
			    $scope.obsTableParams1 = configurarTabela($scope.dados);
			    $scope.tablePrintCarga = $scope.dados;
			    $scope.tablePrintSalario = lista.dadosGeraisFuncionario.registrosFixo;		
		} else 
		if(tipo ==2){
			var dadosProcedimentos = lista.dadosGeraisFuncionario.registrosHorista.listAtendimentoCliente;
			$scope.dados=[];
			for (var i=0; i < dadosProcedimentos.length; i ++){
				if(unidade == dadosProcedimentos[i].idUnidade){
					for (var j=0; j < dadosProcedimentos[i].registrosHoristaAtend.length; j++){
						var dtFormatada = moment(dadosProcedimentos[i].registrosHoristaAtend[j].dtRegistro).format("DD/MM/YYYY");
						dadosProcedimentos[i].registrosHoristaAtend[j].dtFormatada = dtFormatada;	
						$scope.dados[j] = dadosProcedimentos[i].registrosHoristaAtend[j];
					}
				}
			}		
		
			$scope.tablePrintCarga = $scope.dados;
			$scope.tablePrintSalario = lista.dadosGeraisFuncionario.registrosHorista;
			$scope.obsTableParams1 = configurarTabela($scope.dados);

		}
		
		
	}

	$scope.gerarPdf = function() {
		getImageReport('assets/global/img/empresa/DR. CONSULTA MANAUS.jpg',print);
	};
	
	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
	};


		var print = function ( imgData ){
		var doc = new jsPDF('p','pt','letter','a4');
		var rows = [];        

		var columns=[
			{title: "Data", dataKey: "dtFormatada"},
			{title: "Paciente", dataKey: "paciente"},
			{title: "Serviço", dataKey:"servico"}
		];
		for(var i=0; i < $scope.tablePrintCarga.length; i++){
			rows[i] = {
				"dtFormatada": $scope.tablePrintCarga[i].dtFormatada,
				"paciente": $scope.tablePrintCarga[i].nmPaciente,
				"servico": $scope.tablePrintCarga[i].nmServico
			}
		}
		
	
		//CABECALHO	
		doc.addImage(imgData, 'JPEG', 205, 32, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,120, lista.nmFuncionario,{align:'center'});
		doc.setFontSize(8);
		doc.writeText(80, 140, 'Total Atendimentos: ' + $scope.tablePrintCarga.length);
		doc.writeText(500, 140, 'Total R$ '+ $scope.tablePrintSalario.vlSalario.toFixed(2));
		// LINE 1
		doc.autoTable(columns, rows, {margin: {top: 150}, styles:{fontSize: 7}});
		
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
	};



	function configurarTabela(lista){
	
		var tableParams = new NgTableParams();
			if(lista.length > 0){
				var initialParams = { count: 10};
				var initialSettings = {
					// page size buttons (right set of buttons in demo)
					counts:[50,100], 
					// determines the pager buttons (left set of buttons in demo)
					paginationMaxBlocks: 13,
					paginationMinBlocks: 2,
					dataset: lista
				};
				tableParams = new NgTableParams(initialParams, initialSettings) 
			}
			
			return tableParams;
	}; 

	$scope.close = function (){
		$modalInstance.close();
	};

	inicioModal();
	
});

