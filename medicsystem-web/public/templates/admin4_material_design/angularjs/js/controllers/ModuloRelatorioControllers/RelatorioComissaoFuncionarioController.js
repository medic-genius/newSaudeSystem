'use strict';

MedicsystemApp.controller('RelatorioComissaoFuncionarioController', function($rootScope, Auth, $scope, 
	$timeout,$state,$stateParams,servicesFactory, NgTableParams) {
	
	function configurarTable( lista ) {

        var initialParams = { count: 15 };

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };

	function configurarDatePickerRange() {

		$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
       		'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       		'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
       		'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
			'Esta semana': [moment().startOf('week').add(1, 'days'),moment().endOf('week')],
       		'Este mês': [moment().startOf('month'), moment().endOf('month')],
			
       		'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	};

	function inicializarData() {
		$scope.datePicker = {};
		$scope.datePicker.date = {startDate: moment().startOf('week').add(1, 'days'), endDate: moment().endOf('week')};	
		$scope.maxDate = new Date();
	};

	$scope.buscarDados = function (){
		var dtInicio = moment($scope.datePicker.date.startDate).format("YYYY/MM/DD");
	    var dtFim = moment($scope.datePicker.date.endDate).format("YYYY/MM/DD");
		
		 servicesFactory.contratoFuncionario.get({
			 dtInicio: dtInicio,
			 dtFim: dtFim
		}, function( result ) {
			if (result.length > 0){
            $scope.contratos = result;
			$scope.contratos.reverse();
			configurarTable($scope.contratos);
			$scope.showTable = true;
				for (var i = 0; i < $scope.contratos.length; i++){
					$scope.somaVenda = $scope.somaVenda + $scope.contratos[i].vltotal;
					$scope.somaComissao   = $scope.somaComissao +  $scope.contratos[i].vlproducao;
				}
			}else 
			$scope.showTable = false;
        });
	}

	var inicio = function() {
        $scope.userName =  Auth.authz.idTokenParsed.preferred_username;

		$scope.somaVenda = 0;
		$scope.somaComissao = 0;
		inicializarData();
		configurarDatePickerRange();
		$scope.buscarDados();
	};

	inicio();
	
});