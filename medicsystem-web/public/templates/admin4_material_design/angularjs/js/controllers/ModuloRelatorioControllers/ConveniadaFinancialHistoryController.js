'use strict';

MedicsystemApp.controller
(
    'ConveniadaFinancialHistoryController',
    function ($scope, idConveniada, $modalInstance, callWhileInstantiating, servicesFactory)
    {
    	var callWhileInstantiating = eval(callWhileInstantiating);

    	callWhileInstantiating();

        function setLBilling()
        {
            return (
                [
                    {
                        label: "Não pagos",
                        list: []
                    },
                    {
                        label: "Pagos",
                        list: []
                    }
                ]
            );
        }

        function confDateToView(dateJson)
        {
            var dateToView = (moment(dateJson)).format('DD/MM/YYYY');
            if(dateToView == "Invalid date")
            {
                dateToView = "";
            }
            return dateToView;
        }

        function setInTipoTitle(inTipo)
        {
            var inTipoTitle;
            if("M" == inTipo)
            {
                inTipoTitle = "Mensalidade";
            } else if("D" == inTipo)
            {
                inTipoTitle = "Despesa";
            } else
            {
                inTipoTitle = "Outro";
            }
            return inTipoTitle;
        }

        function increaseTotal(value, index)
        {
            if(!$scope.lBilling[index].hasOwnProperty("total"))
            {
                $scope.lBilling[index].total = 0;
            }
            $scope.lBilling[index].total += value;
        }

        function prepareData(data)
        {
            $scope.lBilling = setLBilling();
            var objToView = {};
            for (var i = 0; i < data.length; i++)
            {
                objToView = data[i];
                objToView.dtVencimento = confDateToView(objToView.dtVencimento);
                objToView.dtPagamento = confDateToView(objToView.dtPagamento);
                objToView.inTipoTitle = setInTipoTitle(objToView.inTipo);
                $scope.lBilling[objToView.inQuitada].list.push(objToView);
                increaseTotal(objToView.vlTotal, objToView.inQuitada);
            }
        }

        function constructorController()
        {
            $scope.idConveniada = idConveniada;
            $scope.showingListPayed = false;
            $scope.showingListNotPayed = false;
            getData().$promise.then(prepareData);
        }

        function getData()
        {
            return servicesFactory.getHistoryConveniada.query
            (
                {idEmpresaCliente: $scope.idConveniada}
            );
        }

        $scope.cancel = function ()
        {
            $modalInstance.dismiss('cancel');
        };
    }
).value('callWhileInstantiating', "constructorController");;
