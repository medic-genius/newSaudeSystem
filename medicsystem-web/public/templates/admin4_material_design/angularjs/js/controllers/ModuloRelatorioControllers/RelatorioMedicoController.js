'use strict';

MedicsystemApp.controller('RelatorioMedicoController', function($rootScope,$scope, 
	$timeout,$state,$stateParams,servicesFactory) {

	
	function inicializarTipoInvestimentoOptions ( ) {
		$scope.optionTipoInvestimento = { value: 0};

    	$scope.tipoInvestimentoOptions = [
    		{value:0, name:'Profissional'},
            {value:1, name:'Unidade'},
    	];
	};

	function configurarDatePickerRange() {

		$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	   		'Hoje': [moment(), moment()],
       		'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       		'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
       		'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
       		'Este mês': [moment().startOf('month'), moment().endOf('month')],
       		'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	};

	function inicializarData() {
		$scope.datePicker = {};
		$scope.maxDate = new Date();
		$scope.datePicker.date = {startDate: null, endDate: null};	
	};

	function inicializarMultiSelectProfissional() {
		$scope.multipleProfissionais = {};
		$scope.multipleProfissionais.selected = [];
	}

	function inicializarMultiSelectUnidade () {
		$scope.multipleUnidades = {};
		$scope.multipleUnidades.selected = [];
	};

	function loadEspecialidades() {
		servicesFactory.especialidadesAtivas.get( {},
			function( result ) {
				if ( result ) {
					$scope.especialidadeOptions = result;
				}
			}
		);
	};

	function loadUnidades() {
		servicesFactory.unidades.query(
			function( result ) {
				if ( result ) {
					$scope.unidadeOptions = result;
				}
			}
		);
	};

	var gerarGrafico = function( estatisticas ) {
		var dados = $scope.optionTipoInvestimento.value == 0 ?  estatisticas.atendimentoProfissionalResults : estatisticas.atendimentoProfissionalUnidadeResult;
		var performanceClinica = estatisticas.performanceClinica;
        var performanceProfissional = estatisticas.performanceProfissional;
        var indiceAgendNaoRealizados = estatisticas.indiceAgendNaoRealizados;
        var nmUnidade = estatisticas.nmUnidade;

		if( dados != null) {

			var estatistica = {};
			estatistica.totais = {};
			estatistica.idProfissional = $scope.optionTipoInvestimento.value == 0 ? dados[0].idProfissional: dados[0].idUnidade ;
			estatistica.nmProfissional = dados[0].nmProfissional;
			estatistica.nmUnidade = nmUnidade;
			estatistica.performanceClinica = performanceClinica;
            estatistica.performanceProfissional = performanceProfissional;
            estatistica.indiceAgendNaoRealizados = indiceAgendNaoRealizados;
            
			var qtdAgendamentoRealizadoArray = [];
			var qtdAtendimentosArray = [];
            var qtdAtendimentosContratadosArray = [];
			var qtdAtendimentoRealizadoArray = [];
			var qtdAgendamentoBloqueado = [];
			var qtdAgendamentoFaltoso = [];
			var labelDescriptionArray = [];

			var totalAgendamentos = 0;
			var totalAtendimentos = 0;
            var totalAtendimentosContratado = 0;
			var totalAgAtendidos = 0;
			var totalAgBloqueados = 0;
			var totalAgFaltosos = 0;

			dados.forEach( function logArrayElements(element, index, array) {
                qtdAtendimentosContratadosArray.push( element.qtdAtendimentoContratado );
	    		qtdAgendamentoRealizadoArray.push( element.qtdAgendamento );
	    		qtdAtendimentosArray.push( element.qtdAtendimento);
	    		qtdAtendimentoRealizadoArray.push( element.qtdAtendido );
	    		qtdAgendamentoBloqueado.push( element.qtdBloqueado );
	    		qtdAgendamentoFaltoso.push( element.qtdFaltoso );
	    		labelDescriptionArray.push( element.dtInclusaoFormatado );

	    		totalAgendamentos += element.qtdAgendamento;
	    		totalAtendimentos += element.qtdAtendimento;
                totalAtendimentosContratado += element.qtdAtendimentoContratado;
	    		totalAgAtendidos  += element.qtdAtendido;
	    		totalAgBloqueados += element.qtdBloqueado;
	    		totalAgFaltosos   +=  element.qtdFaltoso;
			} ) 

			estatistica.totais = {totalAgendamentos,totalAtendimentos,totalAtendimentosContratado,totalAgAtendidos,totalAgBloqueados,totalAgFaltosos};
			
            qtdAtendimentosContratadosArray = qtdAtendimentosContratadosArray.toString();
			qtdAtendimentosArray = qtdAtendimentosArray.toString();            
			qtdAtendimentoRealizadoArray = qtdAtendimentoRealizadoArray.toString();
			qtdAgendamentoRealizadoArray = qtdAgendamentoRealizadoArray.toString();
			labelDescriptionArray = labelDescriptionArray.toString();
			
			estatistica.grafico = { 
                qtdAtendimentosContratadosArray,
				qtdAgendamentoRealizadoArray, 
	    		qtdAtendimentosArray,
	    		qtdAtendimentoRealizadoArray,
	    		qtdAgendamentoBloqueado,qtdAgendamentoFaltoso, 
	    		labelDescriptionArray};
	    	
	    	estatistica.grafico.id = 'chart' + estatistica.idProfissional;
			$scope.estatistica.push( estatistica );
		}
	};
	
	$scope.removerEstatistica = function(objeto) {
		console.log('removerEstatistica',objeto);
		var estatisticas = $scope.estatistica;
		$scope.estatistica = [];

		for( var i=0; i < estatisticas.length; i++) {
			var id = estatisticas[i].idProfissional;
			if( objeto.id != id )
				$scope.estatistica.push(estatisticas[i]);
		}
	};

	$scope.obterEstatistica = function( objeto, tipo ) {
		var dtInicio = moment($scope.datePicker.date.startDate).format("DD/MM/YYYY");
		var dtFim = moment($scope.datePicker.date.endDate).format("DD/MM/YYYY");
		var dataValida = moment($scope.datePicker.date.startDate).isValid();

		if( dataValida ) {
			if( tipo == 'profissional') {
				servicesFactory.relatorioAtendimentoMedico
				.get( {idProfissional: objeto.id, dataInicio: dtInicio, dataFim:dtFim}, 
			   		function( result ) {
			   			if(result) {
			   				gerarGrafico(result);                        
			   			}
		          });	
			} else if ( tipo == 'unidade') {
				servicesFactory.relatorioAtendimentoMedicoPorUnidade
				.get( {idUnidade: objeto.id, dataInicio: dtInicio, dataFim:dtFim}, 
			   		function( result ) {
			   			if(result) {
			   				gerarGrafico(result);                        
			   			}
		          });	
			}
		} else {
			$rootScope.alerts.push({ type: "warning", msg: "Forneca o periodo de pesquisa!" , timeout: 5000 });
         	Metronic.scrollTop(); 
		}

	};

	$scope.nothing = function() {
		$scope.atualizarResultados();
	};

	$scope.atualizarResultados = function() {
		console.log('entrou...');
		$scope.estatistica = [];

		if( $scope.optionTipoInvestimento.value == 0) {
			if( $scope.multipleProfissionais.selected.length > 0 ) {
				$scope.multipleProfissionais.selected
				.forEach( function logArrayElements(profissional, index, array) {
					$scope.obterEstatistica( profissional, 'profissional' );
				})
			}
		} else if ( $scope.optionTipoInvestimento.value == 1) {
			if( $scope.multipleUnidades.selected.length > 0  ) {
				$scope.multipleUnidades.selected
				.forEach( function logArrayElements(unidade, index, array) {
					$scope.obterEstatistica( unidade, 'unidade' );
				})
			}
		};
	};

	$scope.loadProfissionaisByEspecialidade = function( especialidade ) {
		servicesFactory.especialidadeProfissionais.query( {id:especialidade.id},
			function( result ) {
				if ( result ) {
					$scope.profissionais = result;
				}
			}
		);
	};	


	var inicio = function() {
		
		$scope.estatistica = [];

		inicializarTipoInvestimentoOptions();
		inicializarData();
		configurarDatePickerRange();
		inicializarMultiSelectProfissional();
		inicializarMultiSelectUnidade();

  		loadEspecialidades();
  		loadUnidades();
	};

	inicio();
	
});