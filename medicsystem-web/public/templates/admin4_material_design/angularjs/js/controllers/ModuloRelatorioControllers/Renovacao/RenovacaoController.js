'use strict';

MedicsystemApp.controller('RenovacaoController', function($rootScope,$scope, $http, 
	$timeout,$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, Auth) {
	
	function inicializarData() {
		var dataAtual = moment();
		$scope.dataFiltro = {};
		$scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};

		loadFuncionariosList();
	};

  function definirConfigDateRange() {
  	$scope.localeDateRangePicker = {
    	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
    	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
    	"customRangeLabel": "Customizado", "weekLabel": "S",
    	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
    	"monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
    	"firstDay": 0
		};

	  $scope.rangesDateRangePicker =  {
    	'Hoje': [moment(), moment()],
   		'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()],
   		'Últimos 60 Dias': [ moment().subtract(59, 'days'),moment()],
   		'Últimos 90 Dias': [ moment().subtract(89, 'days'),moment()],
		};
  };

	function loadFormaPagamento(){
		$scope.formaPagamentoOptions = [];
		var obj = {id: null, description:"TODOS"};
		$scope.formaPagamentoOptions.push(obj);
		servicesFactory.renovacaoFormaPagamento.query({}, function(result){
			for(var i=0; i < result.length; i++){
				$scope.formaPagamentoOptions.push(result[i]);
			}
		});
	}

	function loadEmpresaGrupo(){
		$scope.empresaOptions = [];
		var obj = {id: null, nmFantasia: "TODOS"};
		$scope.empresaOptions.push(obj);
		servicesFactory.getEmpresaGrupo.get({}, function(result){
			for(var i=0; i <result.length; i++){
				$scope.empresaOptions.push(result[i]);
			}
		});

		getRenovacoesContrato();
	}

	$scope.loadPlano = function(idEmpresa){
		$scope.plano={selected:undefined};
		$scope.planoOptions = [];
		var obj = {id: null, nmPlano:"TODOS"};
		$scope.planoOptions.push(obj);
		servicesFactory.planosByEmpresaGrupo.get({id: idEmpresa}, function(response) {
			for(var i=0; i < response.length; i++){
				$scope.planoOptions.push(response[i]);
			}
		});
		getRenovacoesContrato();
	}

	$scope.dateIntervalHasChanged = function() {
		loadFuncionariosList();
		getRenovacoesContrato();
	}

	$scope.pesquisarRenovacao = function() {
		getRenovacoesContrato();
	}

	function loadFuncionariosList(rParams) {
		// var dataInicio = moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY").toString();
		// var dataFim = moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY").toString();

		var rParams = {
			dtInicio: moment($scope.dataFiltro.date.startDate).format("DD/MM/YYYY"),
			dtFim: moment($scope.dataFiltro.date.endDate).format("DD/MM/YYYY"),
			inFormaPagamento: $scope.formapag.id,
			idPlano: $scope.plano.selected,
			idEmpresaGrupo: $scope.empresa.selected,
			idFuncionario: $scope.funcionario.selected,
		}

		$scope.listFuncionarios = undefined;
		servicesFactory.funcionariosPeriodoRelRenovacao.get(rParams, function(response) {
			$scope.listFuncionarios = response;
			$scope.listFuncionarios.unshift({ idFuncionario: null, nmFuncionario: 'TODOS'});
			$scope.funcionario.selected = $scope.listFuncionarios[0].idFuncionario;
		}, function(err) {
			$scope.listFuncionarios = undefined;
		});
	}

	function getRenovacoesContrato(cont) {
		var dataInicio = moment($scope.dataFiltro.date.startDate).format("YYYY-MM-DD").toString();
		var dataFim = moment($scope.dataFiltro.date.endDate).format("YYYY-MM-DD").toString();

		var rParams = {
			dtInicio: dataInicio,
			dtFim: dataFim,
			inFormaPagamento: $scope.formapag.id,
			idPlano: $scope.plano.selected,
			idEmpresaGrupo: $scope.empresa.selected,
			idFuncionario: $scope.funcionario.selected,
			offset: cont
		}

		servicesFactory.administracaoRelRenovacao.get(rParams, function(result) {
			if(!result.data){
				$scope.relatorioRenovacao = [];
				$scope.showTab = false;
				return;
			}else{
				if(result.data.length > 0){
					$scope.total = result.total;
					$scope.showTab = true;
					$scope.relatorioRenovacao = result.data;
					for(var i=0; i < $scope.relatorioRenovacao.length; i ++){
						$scope.relatorioRenovacao[i].dataRenovacaoFormatada = moment($scope.relatorioRenovacao[i].dtRenovacao).format("DD/MM/YYYY").toString();
						$scope.relatorioRenovacao[i].formaPagamentoFormatada = formatarFormaPagamento($scope.relatorioRenovacao[i].inFormaPagamento);
					}
				}else{
					$scope.showTab = false;
				}
				configurarPage(result);
				configurarTable($scope.relatorioRenovacao);
			}
		});
	}

	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:50};
			var initialSettings = {
				counts:[], 
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings );
			
		}
	};

	function formatarFormaPagamento(formapag){
		if(formapag == 0){
			return 'BOLETO BANCÁRIO'
		}else if(formapag == 1){
			return 'DÉBITO AUTOMÁTICO'
		}else if(formapag == 2){
			return 'CONTRA CHEQUE'
		}else if(formapag == 30){
			return 'CARTÃO RECORRENTE'
		}
	}

	function configurarPage( colecao ) {
		if( colecao.data.length > 0 ) {
			colecao.data.forEach( function( item, index) {
				item.currentPage = 1;
				item.totalItems = item.totalItems;
				item.itemsPerPage = 50;	
			});

			$scope.relatorioRenovacaoFinal = colecao;
			definirPaginacao( $scope.relatorioRenovacaoFinal );
		} else
			$scope.relatorioRenovacaoFinal = [];
  };
  
  function definirPaginacao( colecao ) {
		$scope.currentPage = colecao.offset;
		$scope.totalItems = colecao.total;
		$scope.itemsPerPage = 50;
	}
	
	$scope.pageChanged = function(cont){
		getRenovacoesContrato(cont);
	}

	$scope.gerarBoleto = function (item){
		$scope.buttonDisabled = true;
		servicesFactory.gerarBoletoRenovacao.get(
			{idCliente: item.idCliente, idContrato: item.idContrato},
			function(result) {
				item['boAcaoBoleto'] = true;
		        var fileURL = result.link; 
				window.open(fileURL);
				$scope.buttonDisabled = false;
			}
		);
	}

	function inicio(){
		$scope.formapag={id:undefined};
		$scope.empresa ={selected: undefined};
		$scope.plano ={selected: undefined};
		$scope.funcionario = { selected: undefined };
		$scope.showTab = false;
		$scope.buttonDisabled=false;
		inicializarData();
  		definirConfigDateRange();
		loadFormaPagamento();
		loadEmpresaGrupo();
		$scope.loadPlano();
	}

	inicio();
});