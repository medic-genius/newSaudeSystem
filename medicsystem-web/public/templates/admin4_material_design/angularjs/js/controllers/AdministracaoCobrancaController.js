'use strict';

MedicsystemApp.controller('AdministracaoCobrancaController', function($rootScope, $scope, $http, $timeout, $state, $stateParams, $modal, $sce, servicesFactory, ngTableParams, $filter, Auth) {

	$scope.rwRoles = ['administrador','gerenteadm', 'cadastro', 'gerenteatendimentocliente', 'rh', 'atendimentocliente', 'usuario'];

/////////////////////////////////////////////////////////////////
	var Cliente = servicesFactory.clientes;
    var ClienteView = servicesFactory.clienteView;

    $rootScope.digitalClienteMsg = "Ler Biometria";

    $scope.formaPagamentoOptions = [
        { id: 5, description: "BOLETO BANCÁRIO" },
        { id: 30, description: "CARTÃO RECORRENTE" },
        { id: 0, description: "CONTRA-CHEQUE" },
        { id: 1, description: "DÉBITO AUTOMÁTICO" },
        { id: 2, description: "DINHEIRO" },
        { id: 3, description: "CHEQUE À VISTA" },
        { id: 4, description: "CARTÃO DE CRÉDITO" },
        { id: 6, description: "CHEQUE PRÉ-DATADO" },
        { id: 7, description: "GRATUITO" },
        { id: 9, description: "CARTÃO DE DÉBITO" },
        { id: 10, description: "COBERTURA DO PLANO" },
        { id: 14, description: "BAIXA NO SISTEMA" },
        { id: 15, description: "CONVÊNIO" },
        { id: 17, description: "FOLHA DE PAGAMENTO" }
    ];

    $scope.grupoClienteOptions = [
        { id: 0, description: "FUNCIONÁRIO REAL VIDA" },
        { id: 1, description: "FUNCIONÁRIO PÚBLICO" },
        { id: 2, description: "PARTICULAR ASSOCIADO" },
        { id: 3, description: "EMPRESARIAL" },
        { id: 4, description: "PARTICULAR NÃO ASSOCIADO" }
    ];

    $scope.cidadeOptions = servicesFactory.cidades.query();

    var dataEmpresaGrupos = servicesFactory.empresagrupos.query(function(){
        $scope.empresagrupoOptions = dataEmpresaGrupos;
    });

    var dataBancos = servicesFactory.bancos.query(function(){
        $scope.bancoOptions = dataBancos;
    });

    $scope.atualizarSelectClientes = function(text) {
        if (text) {
            var queryObject = {};
            var filtro = $scope.searchFiltro;
            text = text.toLowerCase();
            if (filtro && filtro.startsWith("!")) {
                if (filtro.startsWith("!cod")){
                    queryObject = { nrCodCliente: text.trim() };
                } else if (filtro.startsWith("!rg")){
                    queryObject = { nrRG: text.trim() };
                } else if (filtro.startsWith("!cpf")){
                    queryObject = { nrCPF: text.trim() };
                } else {
                    text = null;
                }

            } else {
                queryObject = { nmCliente: text };
            }

            if(text){
                var data = Cliente.query(queryObject, function() {
                    $scope.clientes = data;
                    if (data.length == 0) {
                        $scope.selectedClienteNotFound = "Sem dados disponiveis.";

                    } else {
                        $scope.selectedClienteNotFound = "";
                        
                    }
                });
            }
        }
    };

    $scope.showDetails = true;
    $scope.showClienteOptions = false;

    $scope.searchUser = function(item) {
        $scope.selectClienteFromSearch = item;
        $scope.selectedCliente = Cliente.get( {id: item.id}, function(){
            $scope.showDetails = false;
            $scope.showClienteOptions = false;
            $scope.situacoes = servicesFactory.clienteSituacao.query({ id: item.id });
            //$state.go('atendimento');
        });
    };

    //if ($state.is('atendimento')) {
    $scope.showDetails = true;
    $scope.showClienteOptions = false;
    $scope.selectedCliente = new Cliente({});
    $scope.cliente = {};
    $scope.searchFiltro = "";
    $scope.situacoes = [];  

    $scope.$on('updateUserBroadcast', function (event, value) {
        if (value == 'true') {
            $scope.searchUser( { id: $scope.selectedCliente.id } );
        }
    });

///////////////////////////////////////////////////////////////

	$scope.today = function() {
		$scope.dt = new Date();
	};

  	$scope.today();

  	$scope.clear = function () {
    	$scope.dt = null;
  	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};

	$scope.toggleMin();
	$scope.maxDate = new Date(2020, 5, 22);

	$scope.openDtInicial = function($event) {
		$scope.status.openedDtInicial = true;
	};

	$scope.openDtFinal = function($event) {
		$scope.status.openedDtFinal = true;
  	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
  	};

	$scope.formats = ['dd/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];

	$scope.status = {
		opened: false
	};

	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);
	var afterTomorrow = new Date();
	afterTomorrow.setDate(tomorrow.getDate() + 2);
	$scope.events =
		[
	    	{
	        	date: tomorrow,
	        	status: 'full'
	      	},
	    	{
	        	date: afterTomorrow,
	        	status: 'partially'
	      	}
	    ];

  	$scope.getDayClass = function(date, mode) {
	    if (mode === 'day') {
	    	var dayToCheck = new Date(date).setHours(0,0,0,0);

	    	for (var i=0;i<$scope.events.length;i++){
	        	var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
		        if (dayToCheck === currentDay) {
	          		return $scope.events[i].status;
	        	}
	      	}
    	}
    	return '';
  	};

  	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}
});
