'use strict';

MedicsystemApp.controller('ListaCortesiasController', function($scope, Auth, NgTableParams, 
	toastr, CortesiaService) {

	$scope.$on('$viewContentLoaded', function() {
		if(Auth.authz.idTokenParsed.preferred_username == "admin") {
			$scope.showAvaliarActions = true;
			$scope.showLiberarActions = true;
		} else if(Auth.authz.hasResourceRole('gerenteunidade') && 
			!Auth.authz.hasResourceRole('gerenteadm')) {
			$scope.showAvaliarActions = false;
			$scope.showLiberarActions = true;
		} else if(Auth.authz.hasResourceRole('gerenteadm') && 
			!Auth.authz.hasResourceRole('gerenteunidade')) {
			$scope.showAvaliarActions = true;
			$scope.showLiberarActions = false;
		} else {
			$scope.showAvaliarActions = true;
			$scope.showLiberarActions = true;
		}

		// filte status 
		$scope.filterStatus = {
			value: undefined
		}
		initializeStatusOptions();

		// filter utilizacao
		$scope.filterUtilizacao = {
			value: undefined
		}
		initializeUtilizacaoOption();

		// date range init
		inicializarData();
		configurarDatePickerRange();
	});

	function configureTableParams(lista) {
		var initialParams = { count:10 };
		var initialSettings = {
			counts: [5, 10, 20],
			paginationMaxBlocks: 13,
			paginationMinBlocks: 2,
			dataset: lista,
		};
		return new NgTableParams(initialParams, initialSettings )  
	}

	$scope.formatDate = function(date) {
		if(date) {
			return moment(date).format("DD/MM/YYYY");
		}
	}

	$scope.loadData = function() {
		if(!$scope.datePicker.date.startDate || !$scope.datePicker.date.endDate) {
			return;
		}
		var rParams = {
			instatuscortesia: $scope.filterStatus.value,
			boutilizada: $scope.filterUtilizacao.value,
			dtInicio: $scope.datePicker.date.startDate.format("DD/MM/YYYY"),
			dtFim: $scope.datePicker.date.endDate.format("DD/MM/YYYY")
		};
		CortesiaService.getAll.get(
			rParams,
			function(response) {
				if(response) {
					$scope.tableParamsCortesias = configureTableParams(response);
				}
			},
			function(err) {}
		);
	}

	$scope.liberarCortesia = function(cortesiaObj) {
		addToProcessingList(cortesiaObj.id);
		var rBody = JSON.parse(JSON.stringify(cortesiaObj));
		rBody.boUtilizada = true;
		CortesiaService.liberar.put(
			null,
			rBody,
			function(response) {
				if(response) {
					cortesiaObj.boUtilizada = true;
					toastr.success("Cliente pode utilizar o serviço requerido", "Cortesia marcada como usada");
				} else {
					toastr.error("Tente novamente", "Erro ao liberar cortesia");
				}
			},
			function(err) {
				toastr.error("Tente novamente.", "Erro ao contactar servidor");
			}
		).$promise.finally(function() {
			removeFromProcessingList(cortesiaObj.id);
		});
	}

	$scope.atualizarStatus = function(cortesiaObj, value) {
		addToProcessingList(cortesiaObj.id);
		var rParams = {
			idCortesia: cortesiaObj.id,
			inStatus: value
		}
		CortesiaService.averiguar.put(
			rParams,
			null,
			function(response) {
				if(response) {
					cortesiaObj.inStatusCortesia = value;
					toastr.success("", "Status de cortesia alterado");
				} else {
					toastr.error("Tente novamente", "Erro ao salvar análise de cortesia");
				}
			},
			function(err) {
				toastr.error("Tente novamente.", "Erro ao contactar servidor");
			}
		).$promise.finally(function() {
			removeFromProcessingList(cortesiaObj.id);
		});
	}

	/*-------------------------------------------------------------*/
	/** Processing list functions */
	function addToProcessingList(id) {
		if(!$scope.processingList) {
			$scope.processingList = [];
		}
		$scope.processingList.push(id);
	}

	function removeFromProcessingList(id) {
		if($scope.processingList) {
			var index = $scope.processingList.indexOf(id);
			if(index != -1) {
				$scope.processingList.splice(index, 1);
			}
		}
	}

	$scope.isInProcessingList = function(id) {
		return ($scope.processingList && ($scope.processingList.indexOf(id) != -1));
	}

	/*------------------------------------------------------------------------*/
	/** Init functions */
	function configurarDatePickerRange() {
		$scope.localeDateRangePicker = {
			"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
			"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
			"customRangeLabel": "Customizado", "weekLabel": "S",
			"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
			"monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
			"firstDay": 0
		};

		$scope.rangesDateRangePicker =  {
			'Hoje': [moment(), moment()],
			'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
			'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
			'Últimos 60 Dias': [moment().subtract(59, 'days'), moment()]
		}
	};

	function inicializarData() {
		$scope.datePicker = {};
		$scope.maxDate = new Date();
		$scope.datePicker.date = {startDate: null, endDate: null};	
	};

	function initializeStatusOptions() {
		$scope.statusFilterOptions = [
		{
			name: "Todos",
			value: undefined
		},
		{
			name: "Não Averiguadas",
			value: 0
		},
		{
			name: "Aprovadas",
			value: 1
		},
		{
			name: "Rejeitadas",
			value: 2
		}
		];
	}

	function initializeUtilizacaoOption() {
		$scope.utilizacaoFilterOptions = [
		{
			name: "Sim",
			value: true
		},
		{
			name: "Não",
			value: false
		},
		{
			name: "Ambas",
			value: undefined
		}
		];
	}
});