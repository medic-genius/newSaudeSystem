'use strict';

MedicsystemApp.controller('PacienteAnamneseGeralController', function($rootScope,$scope,$timeout,$state,$stateParams,$filter, servicesFactory) {
	

	$scope.recuperarTriagem = function() {
		
		$scope.triagemResult = {};

		servicesFactory.getTriagem
		 .get( { idPaciente: $stateParams.idPaciente ,inTipoPaciente: $stateParams.tipoCliente},
		 	function( result ) {
	   			if(result) {	

	   				$scope.triagemResult = result;
	   				$scope.triagemResult.vlPeso = $scope.triagemResult.vlPeso  != null ? parseFloat( $scope.triagemResult.vlPeso.toFixed(2) ) : null; 
	   				$scope.triagemResult.vlAltura = $scope.triagemResult.vlAltura != null ? parseFloat( $scope.triagemResult.vlAltura.toFixed(2) ) : null; 
	   				$scope.triagemResult.vlIMC = calcularIMC( $scope.triagemResult.vlPeso,$scope.triagemResult.vlAltura);	
	   				$scope.triagemResult.analiseIMC = $scope.triagemResult.vlIMC != null ? analisarIMC( $scope.triagemResult.vlIMC ) : null;
	   			}
	     }); 
	}

	var calcularIMC = function( peso, altura ) {
		
		altura = parseFloat(altura);
		peso = parseFloat(peso);

		if( altura != null && peso != null ) {
			var vlIMC = peso/(altura*altura);
			vlIMC = parseFloat( vlIMC.toFixed(2) ); 
		
			return vlIMC;
		} else {
			return null;
		}
	};

	var analisarIMC = function( vlIMC ) {

		var analiseIMC = {};
	
		if( vlIMC < 17 ) {
			analiseIMC.nivel = 0;
			analiseIMC.descricao = "Muito abaixo do peso";
		} else if( vlIMC >= 17 && vlIMC <= 18.49 ) {
			analiseIMC.nivel = 1;
			analiseIMC.descricao = "Abaixo do peso";
		} else if( vlIMC >= 18.5 && vlIMC <= 24.99 ) {
			analiseIMC.nivel = 2;
			analiseIMC.descricao = "Peso normal";
		} else if( vlIMC >= 25 && vlIMC <= 29.99 ) {
			analiseIMC.nivel = 3;
			analiseIMC.descricao = "Acima do peso";
		} else if( vlIMC >= 30 && vlIMC <= 34.99 ) {
			analiseIMC.nivel = 4;
			analiseIMC.descricao = "Obesidade I";
		} else if( vlIMC >= 35 && vlIMC <= 39.99 ) {
			analiseIMC.nivel = 5;
			analiseIMC.descricao = "Obesidade II (servera)";
		} else if( vlIMC >= 40  ) {
			analiseIMC.nivel = 6;
			analiseIMC.descricao = "Obesidade II (mórbida)";
		}

		return analiseIMC;

	}

	var inicio = function() {
		$scope.buttonFinalizarDisabled = false;
		getAtendimento();

	};

	
	$scope.finalizarAnamneseGeral = function( anamneseGeral ) {
		if ( $('#formAnamneseGeral').valid() ) {
			
			$scope.buttonFinalizarDisabled = true;     
			
			if( $scope.hasAtendimento ) {
			    $scope.updateAnamneseGeral(  anamneseGeral );
			} else {
				$scope.saveAnamneseGeral( anamneseGeral );
			}
		}
	};


	$scope.saveAnamneseGeral = function( anamneseGeral ) {
		
		var anamneseGeralToSave = formatAnamneseGeral( anamneseGeral  );
		var idAgendamento = $stateParams.idAgendamento;

		servicesFactory.saveAnamneseGeral
		 .save( {idAgendamento: idAgendamento}, anamneseGeralToSave , function( result ) {
	   			if(result) {		
	   				$rootScope.alerts.push({
	                type: "success", 
	                msg: " Atendimento salvo com sucesso!",
	                timeout: 5000 });
	                Metronic.scrollTop();   

	                $scope.buttonFinalizarDisabled = false;
	                getAtendimento();      
	   			}
         }); 
	};


	$scope.updateAnamneseGeral = function( anamneseGeral ) {

		var anamneseGeralToUpdate = formatAnamneseGeral( anamneseGeral  );
		var idAgendamento = $stateParams.idAgendamento;

		servicesFactory.updaterAnamneseGeral
		 .update( {idAgendamento: idAgendamento}, anamneseGeralToUpdate , function( result ) {
		 	if(result) {
	 			$rootScope.alerts.push({
                	type: "info", 
                	msg: " Atendimento atualizado com sucesso!",
                	timeout: 5000 });
                	Metronic.scrollTop();   

                	$scope.buttonFinalizarDisabled = false;
                	getAtendimento();     
		 	}
		 });
	};

	var getAtendimento = function() {

		var idAgendamento = $stateParams.idAgendamento;
		servicesFactory.recuperarAtendimento
		 .get( {idAgendamento: idAgendamento} , function( result ) {
	   			
	   			if( result.idAtendimento != null ) {
	   				$scope.anamneseGeral = {};		
	   				$scope.anamneseGeral = result;
	   				$scope.hasAtendimento = true;
	   			} else {
	   				$scope.anamneseGeral = {};	
	   			}

			   	servicesFactory.agendamentos.get({id: $stateParams.idAgendamento}, function(agendamento){
					if(agendamento.boRetorno != null && agendamento.boRetorno){

						$scope.disableRetorno = true;
						$scope.anamneseGeral.boSolicitouRetorno = false;
					}else {

						$scope.disableRetorno = false;
					}
				});
         }); 
	};


	var formatAnamneseGeral = function( anamneseGeral ) {
	
		if( anamneseGeral.idAtendimento != null ) {

			var anamnese = 
				{ 
  				  id : anamneseGeral.idAtendimento,
				  boSolicitouRetorno: anamneseGeral.boSolicitouRetorno,
			  	  dtAtendimento : $filter('date')(new Date(),'yyyy-MM-dd') ,
			  	  nmCondutaMedica: anamneseGeral.nmCondutaMedica,
			  	  nmDiagnostico: anamneseGeral.nmDiagnostico, 
			  	  nmQueixaPaciente : anamneseGeral.nmQueixaPaciente,
			  	  boFinalizado: true } ;			

		} else {

			var anamnese = 
				{ boSolicitouRetorno: anamneseGeral.boSolicitouRetorno,
			  	  dtAtendimento : $filter('date')(new Date(),'yyyy-MM-dd') ,
			  	  nmCondutaMedica: anamneseGeral.nmCondutaMedica,
			  	  nmDiagnostico: anamneseGeral.nmDiagnostico, 
			  	  nmQueixaPaciente : anamneseGeral.nmQueixaPaciente,
			  	  boFinalizado: true } ;			
		}

	
		return anamnese;  	  
	};


	inicio();


});


