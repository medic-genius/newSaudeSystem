'use strict';

MedicsystemApp.controller('TemplatesLaudoMedicoController', function($scope, $state, $timeout, 
	$location, $anchorScroll, $ngBootbox, toastr, servicesFactory) {
	function inicio() {
		$scope.especialidade = {
			selected: undefined
		};

		$scope.templateControl = {
			status: undefined,
			templateClone: undefined
		}

		resetTemplateLaudoObject();

		$scope.buttonDisabled = false;

		$scope.options = {
			height: 300,
			focus: true,
			airMode: false,
			fontSizes: ['8', '9', '10', '11','12','13', '14', '18', '24', '36'],
			toolbar: [
				['font', ['bold', 'italic', 'underline']],
				['color', ['color']],
				['para', ['paragraph', 'ol', 'ul']],
				['textsize', ['fontsize']],
				['table', ['table']],	
				['view', ['fullscreen']],
			],
		};
	}
	inicio();

	if($state.is('atendimentoMedico.templates-laudo')){
		if($scope.selectedMedico && $scope.selectedMedico.id) {
			loadEspecialidades($scope.selectedMedico);
		}
	}

	function resetTemplateLaudoObject() {
		$scope.templateLaudo = {
			nmTemplate: undefined,
			nmConteudo: undefined
		}
	}

	function loadEspecialidades(item) {
		$scope.loadingEspecialidades = true;
		$scope.templateControl.status = undefined;
		$scope.listTemplates = undefined;
		$scope.especialidade.selected = undefined;
		var rParams = {
			idMedico: item.id
		}
		servicesFactory.especialidadesMedico.get(
			rParams,
			function(response) {
				$scope.listEspecialidades = response;
			}
		).$promise.finally(function() {
			$scope.loadingEspecialidades = false;
		});
	}

	$scope.searchMedico = function(nmMedico) {
		if(!nmMedico || nmMedico.length < 3) { return ;}
		var rParams = {
			nmMedico: nmMedico
		}
		servicesFactory.searchMedico.get(
			rParams,
			function(response) {
				$scope.listMedicos = response;
				$scope.selectedMedicoNotFound = undefined;
				if(!response && response.length == 0) {
					$scope.selectedMedicoNotFound = 'Não Encontrado';
				}
			}
		);
	}

	$scope.selectedEspecialidade = function(esp) {
		$scope.templateControl.status = undefined;
		var rParams = {
			idMedico: $scope.selectedMedico.id,
			idEspecialidade: esp.idEspecialidade
		}
		servicesFactory.templatesLaudoForEspProfissional.get(
			rParams,
			function(response) {
				if(response && response.length > 0) {
					$scope.listTemplates = response;
				} else {
					$scope.listTemplates = [];
				}
			}
		);
	}

	//--------------------------------------------------------------------------------
	$scope.configureTemplateLaudo = function(template) {
		if(!template || !template.id) {
			$scope.templateControl.status = 'creating';
		} else {
			$scope.templateLaudo = template;
			$scope.templateControl.templateClone = JSON.parse(JSON.stringify(template));
			$scope.templateControl.status = 'editing';
		}
	}

	$scope.cancelAction = function() {
		if($scope.templateControl.status == 'editing') {
			var tplClone = $scope.templateControl.templateClone;
			$scope.templateLaudo['nmTemplate'] = tplClone.nmTemplate;
			$scope.templateLaudo['nmConteudo'] = tplClone.nmConteudo;
		}
		$scope.templateControl.status = undefined;
		resetTemplateLaudoObject();

		$timeout(function() {
			$location.hash('templatesPortletTitle');
			$anchorScroll.yOffset = 60
			$anchorScroll();
		}, 300);
	}

	//-------------------------------------------------------------------
	$scope.deleteTemplate = function(templateObj) {
		$ngBootbox.confirm('Deseja realmente excluir este template?').then(
			function() {
				templateObj.isDeleting = true;
				var rParams = {
					idTemplate: templateObj.id
				}
				servicesFactory.templateLaudoMedico.delete(rParams, function(response) {
					var index = $scope.listTemplates.indexOf(templateObj);
					if(index != -1) {
						$scope.listTemplates.splice(index, 1);
					}
				}, function(err) {
					templateObj.isDeleting = false;
				});
			}
		);
	}

	$scope.saveTemplate = function() {
		$scope.buttonDisabled = true;
		persistTemplate($scope.templateLaudo);
		return;
	};

	function persistTemplate(template) {
		var finallyCallback = function() {
			$scope.buttonDisabled = false;
			$timeout(function() {
				$location.hash('templatesPortletTitle');
				$anchorScroll.yOffset = 60
				$anchorScroll();
			}, 300);
		}

		if(!template.id) {
			var rParams = {
				idMedico: $scope.selectedMedico.id,
				idEspecialidade: $scope.especialidade.selected.idEspecialidade
			}
			servicesFactory.templatesLaudoForEspProfissional.post(
				rParams,
				template,
				function(response) {
					if(response && response.id) {
						toastr.success('', 'Template salvo');
						$scope.listTemplates.push(response);
						$scope.templateControl.status = undefined;
					} else {
						toastr.error('', 'Erro ao salvar template')
					}
				}
			).$promise.finally(finallyCallback);
		} else {
			var rParams = {
				idTemplate: template.id
			}
			template.espProfissional = null;
			servicesFactory.templateLaudoMedico.put(
				rParams,
				template,
				function(response) {
					if(response && response.id) {
						toastr.success('', 'Template atualizado com sucesso');
						$scope.templateControl.status = undefined;
					} else {
						toastr.error('', 'Erro ao editar template');
					}
				}
			).$promise.finally(finallyCallback);;
		}
	}
	//-------------------------------------------------------------------
});