'use strict';

MedicsystemApp.controller('PacienteReceituarioTmpController', function($scope, Auth) {
	// define se médico pode ou não ver novo receituario
	$scope.configViewReceituario = function() {
		var allowedLogins = ['admin', 'aline.parente', 'rosane.barbosa', 'marcos.bastos', 'marcos.silva' ,'medico'];
		var login =  Auth.authz.idTokenParsed.preferred_username;
		var index = allowedLogins.indexOf(login);
		$scope.showNewReceituario = index != undefined && index != -1;
	}
	$scope.configViewReceituario();
});