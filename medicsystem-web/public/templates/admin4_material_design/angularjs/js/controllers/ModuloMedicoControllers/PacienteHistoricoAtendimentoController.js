/* 
@author : Joelton
@since : 11-05-2016
@last-modification: 19-05-2016
@version :  1.1 
*/

'use strict';

MedicsystemApp.controller('PacienteHistoricoAtendimentoController', function($rootScope,$scope, $http, $timeout,$state,$stateParams,$modal,$filter, servicesFactory) {

	
	$scope.carregarHistoricoByEspecialidade = function( especialidadeSelecionada ) {
		$scope.idEspecialidadeSelected = null;
		carregarHistoricoAtendimento( $stateParams.idPaciente, especialidadeSelecionada.id  );
	};

	var carregarEspecialidades = function() {
		
		servicesFactory.especialidadesAtivas.get( function( result ) {
	   			if(result) {
	   				
	   				$scope.especialidades = result;
	   				for( var i=0;i<$scope.especialidades.length;i++ ) {
	   				    var idEspecialidade = $scope.especialidades[i].id;
	   					if( idEspecialidade == $stateParams.idEspecialidade ) {
	   						$scope.idEspecialidadeSelected = null;
	   						$scope.idEspecialidadeSelected = idEspecialidade;
	   						break;
	   					}
	   				}
	   			}
	      });
	};
	
	var carregarHistoricoAtendimento = function( idPaciente, idEspecialidade ) {

		servicesFactory.pacienteHistoricoAtendimentos.get( { idPaciente: idPaciente, idEspecialidade: idEspecialidade, tipoPaciente: $scope.tipoCliente  },
		 function( result ) {
			
			if (  result ) {

				$scope.events = [];

				for ( i = 0; i < result.length; i++) {

					var atendimentoRealizado = result[i];

					var i;

					$scope.events.push({
						badgeClass: 'info',
						badgeIconClass: 'glyphicon-check',
						title: atendimentoRealizado.nmMedico ,
						content: 'Some awesome content.',
						when: atendimentoRealizado.dtAtendimento,
						nmEspecialidade : atendimentoRealizado.nmEspecialidade,
						nmQueixaPaciente:atendimentoRealizado.nmQueixaPaciente,
						nmDiagnostico: atendimentoRealizado.nmDiagnostico,
						nmCondutaMedica: atendimentoRealizado.nmCondutaMedica,
						nmServico: atendimentoRealizado.nmServico,
						nmReceituario: atendimentoRealizado.nmReceituario
					});
				} 

			} else {
				$scope.noData = true;
			}
		});
	};

	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	$scope.teste = function() {
		alert('casa');
	};
	
	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	var inicio = function() {
		$scope.idEspecialidadeSelected2 = null;
		carregarEspecialidades();
		carregarHistoricoAtendimento( $stateParams.idPaciente, $stateParams.idEspecialidade );		
	};

	inicio();

});


