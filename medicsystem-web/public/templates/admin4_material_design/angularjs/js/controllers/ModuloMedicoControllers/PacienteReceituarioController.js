/* 
@author : Patrick Lima
@since : 04-07-2017
@last-modification: 13-07-2017
@version :  1.1
*/
'use strict';

MedicsystemApp.controller('PacienteReceituarioController', function($rootScope, $scope, 
	$timeout, $stateParams, $filter, $ngBootbox, toastr, servicesFactory, Auth) {
	
	//EDITOR DE TEXTO
	$scope.options = {
		height: 300,
		focus: true,
		airMode: false,
		toolbar: [
			['edit',['undo','redo']],
			['style', ['bold', 'italic', 'clear']],
			['textsize', ['fontsize']],
			['alignment', ['ul', 'lineheight']],
			['table', ['table']],
		]
	};

	// BEGIN PRINT REPORT RECEITUÁRIO
	var gerarPdfExames = function(listaMedicamentos) {
		var params = {
			listaMedicamentos: listaMedicamentos
		}
		getImageReport('assets/global/img/medic-lab-logo.jpg', $scope.printReceituario, params);
	};

	var getImageReport = function(url, callBack, params) {
		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img, params);
		};

		img.src = url;
	};

	$scope.printReceituario = function(imgData, params) {
		var nmMedico = $scope.profissional.nmFuncionario;
		var crm =  $scope.profissional.nrCrmCro;
		var nmPaciente = $stateParams.nmPaciente;

		var data =  new Date();
		var dia = data.getDate() > 9 ? data.getDate() : ('0'+ (data.getDate() ));
		var mes = (data.getMonth() + 1) > 9 ? (data.getMonth() + 1) : ('0'+ (data.getMonth() + 1));
		var ano = data.getFullYear().toString();
		var dtAtendimento = dia+"/"+mes+"/"+ano;
		var receituario =  $("#text-receituario").html();
		
		var doc = new jsPDF('p','pt','letter','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,100, 'Receituário',{align:'center'});

		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 130, 'Médico(a):');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(97, 130, nmMedico );

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 150, 'Paciente:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(92, 150, nmPaciente);

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 170, 'Data do atendimento:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(154, 170, dtAtendimento);
		
		//content
		var specialElementHandlers = {
	      	'DIV to be rendered out': function(element, renderer) {
	       		return true;
	    	}
	    };

		doc.setFont("times");
		doc.setFontSize(12);
		doc.fromHTML(receituario, 40, 180, {
			'width': 500 ,
			'elementHandlers': specialElementHandlers
		});

		// doc.writeText(40, 226 ,receituario,{align:'left',width:100});

		//ASSINATURA
		doc.setFont("times");
		doc.setFontSize(12);
		doc.writeText(0, 723, '____________________________________',{align:'center'});			

		doc.setFont("times");
		doc.setFontSize(12);
		if( nmMedico.length > 30  ) {
			var nmTemp = nmMedico.split(" ");
			nmMedico = nmTemp[0] + " " + nmTemp[nmTemp.length - 1];		
			doc.writeText(0, 752, nmMedico,{align:'center'});
			doc.writeText(0, 772, 'CRM '+crm,{align:'center'});
		} else {
			doc.writeText(0, 752, nmMedico,{align:'center'});
			doc.writeText(0, 772, 'CRM '+ crm,{align:'center'});
		}
		if($scope.hasSelectedMedicamento(params.listaMedicamentos)) {
			params.docObject = doc;
			getImageReport('assets/global/img/DROGAMED_COM_ENDERECO.jpg', $scope.printOrcamento, params);
		} else {
			doc.autoPrint();
			// doc.output('dataurlnewwindow');
			var blob = doc.output('blob');
			var fileURL = URL.createObjectURL(blob);
			window.open(fileURL, '_blank');
		}
	};

	$scope.printOrcamento = function(dataImg, params) {
		var selection = params.listaMedicamentos;
		
		var doc = params.docObject;
		doc.addPage();

		//CABECALHO		
		doc.addImage(dataImg, 'JPEG', 40, 20, 200/*width*/, 49/*heigth*/, 'logo2');

		//content
		var specialElementHandlers = {
			'DIV to be rendered out': function(element, renderer) {
				return true;
			}
		};

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0, 100, 'Orçamento', {align:'center'});

		doc.setFontSize(12);
		var H = 160;
		var total = 0;
		for(var i = 0; i < selection.length; ++i) {
			if(selection[i].medicamento) {
				doc.setFontType('');
				doc.writeText(40, H, selection[i].medicamento.descricao, {align: 'left'});

				doc.setFontType('bold');
				var value = 'R$ ' + selection[i].medicamento.vlMedicamento.toFixed(2).replace('.', ',');
				doc.writeText(-50, H, value, {align: 'right'});

				total += selection[i].medicamento.vlMedicamento;
				H += 25;
			}
		}
		if(total > 0) {
			doc.setFontType('bold');
			doc.setFontSize(14);
			var txt = 'Total:    R$ ' + total.toFixed(2).replace('.', ',');
			doc.writeText(-50, H, txt, {align: 'right'});
		}

		doc.setFontSize(12);
		doc.setFontType('bold');
		H += 40;
		doc.writeText(40, H, '* CLIENTES DR+CONSULTA TEM DESCONTOS DE 5% a 40%');
		      
		doc.setFontType('bold');
		H += 30;
		doc.writeText(5, H, 'Drogamed', {align:'center'});

		doc.setFontType('');
		H += 15;
		doc.writeText(5, H, 'Rua Tapajós, 688 - Centro - 69010-150 - Manaus-AM', {align:'center'});
		H += 15;
		doc.writeText(5, H, 'Telefone: (92) 3343-0622', {align:'center'});
		
		doc.autoPrint();
		// doc.output('dataurlnewwindow');

		var blob = doc.output('blob');
		var fileURL = URL.createObjectURL(blob);
		window.open(fileURL, '_blank');
	}

// END PRINT REPORT RECEITUÁRIO

	var getProfissional = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		servicesFactory.recuperarProfissional.get( {login: login}, 
			function(result) {
				if(result) {
					$scope.profissional = result;
				}
			});
	};

	var getReceituario = function() {
		servicesFactory.recuperarReceituario.get(
			{idAgendamento: $stateParams.idAgendamento},
			function(result) {
				if(result.id) {		
					$scope.receituario = result;
					$scope.generalRec = result.recomendacoes;
					$scope.receituarioList = result.medicamentos;
					$scope.selectedMedicamento = [];
				} else {
					$scope.receituario = {};
					$scope.receituarioList = [];
				}
			}
		).$promise.finally(function() {
			var findex = $scope.receituarioList.length;
			$scope.addMedRow(findex);
			$timeout(function() {
				var input = document.getElementById('medicamento-' + findex);
				input.focus();
			}, 800);
		});
		return $scope.receituario;
	};


	$scope.finalizarReceituario = function() {
		var vselection = $scope.getValidMedSelection($scope.receituarioList);
		$scope.insertIntoNote(vselection);

		$scope.receituario.nmReceituario = $('#text-receituario').html();
		$scope.receituario.recomendacoes = $scope.generalRec;
		$scope.receituario.medicamentos = vselection;

		var receituarioToSave = formatarReceituario($scope.receituario);

		$scope.buttonDisabled = true;
		if(receituarioToSave.id != null) {
			atualizarReceituario(receituarioToSave);
		} else {
			salvarReceituario(receituarioToSave);
		}
		return;

		if( $('#formReceituario').valid() ) {
			var receituarioToSave = formatarReceituario($scope.receituario) ;
			$scope.buttonDisabled = true;
			if( receituarioToSave.id != null) {
				atualizarReceituario( receituarioToSave );
			} else {
				salvarReceituario( receituarioToSave );
			}
		}
	};


	var salvarReceituario = function( receituario ) {
		var rParams = {
			idAgendamento: $stateParams.idAgendamento,
			idPaciente: $stateParams.idPaciente,
			inTipoPaciente: $stateParams.tipoCliente
		}
		servicesFactory.saveReceituario.save(
			rParams,
			receituario,
			function( result ) {
				if(result) {
					$rootScope.alerts.push({
					type: "success",
					msg: " Receituario salvo com sucesso!",
					timeout: 3000});
					Metronic.scrollTop();

					$scope.canFindReceituario = true;

					$scope.buttonDisabled = false;
					gerarPdfExames($scope.receituario.medicamentos);
					getReceituario();
				}
			}
		).$promise.finally(function() {
			$scope.buttonDisabled = false;
		});
	};

	var atualizarReceituario = function(receituarioAtualizar) {
		servicesFactory.updateReceituario.update(
			receituarioAtualizar,
			function( result ) {
				if(result) {
					$rootScope.alerts.push({
						type: "info",
						msg: " Receituario atualizado com sucesso!",
						timeout: 3000
					});
					Metronic.scrollTop();   

					$scope.buttonDisabled = false;

					gerarPdfExames($scope.receituario.medicamentos);
					getReceituario();
	   		}
			}
		).$promise.finally(function() {
			$scope.buttonDisabled = false;
		}); 
	};


	var formatarReceituario = function(receituario) {
		var receituarioToSave = {
			id: receituario.id,
			nmReceituario: receituario.nmReceituario,
			recomendacoes: receituario.recomendacoes,
			dtReceituario:  $filter('date')(new Date(),'yyyy-MM-dd'),
			medicamentos: receituario.medicamentos
		}
		return receituarioToSave;
	};

	//-------------------------------------------------------------------
	$scope.onBlurEvent = function(obj, index) {
		if((obj.medicamento || obj.nmMedicamento) && !$scope.receituarioList[index+1]) {
			$scope.addMedRow(index+1);
		}
		$scope.removeEmptyRows();
	}

	// processa a escolha de um medicamento
	$scope.medSelected = function(item, obj, index) {
		var s = $scope.checkMedSelection(item);
		obj.nmMedicamento = '';
		if(s) {
			obj.medicamento = item;
			var pos = document.getElementById('posologia-'+index);
			pos.focus();
			// $scope.addMedRow(index + 1);
		}
		$scope.removeEmptyRows();
	}

	// metodos para adicionar linha de receituario
	$scope.addMedRow = function(index) {
		if(!$scope.receituarioList) {
			$scope.receituarioList = [];
		}
		if(!$scope.receituarioList[index]) {
			$scope.receituarioList[index] = {
				// idReceituario: $scope.receituario.id,
				medicamento: undefined,
				nmMedicamento: undefined,
				nmPosologia: undefined,
				nmOrientacoes: undefined
			};
			$timeout(function() {
				var txts = document.querySelectorAll('textarea');
				autosize(txts);
			}, 1000);
		}
	}

	// verifica se existem medicamentos no receituario
	$scope.hasInsertedMedicamento = function(listaMedicamentos) {
		var list = listaMedicamentos || $scope.receituarioList;
		if(!list) {
			return false;
		}
		for(var i = 0; i < list.length; ++i) {
			if(list[i] && 
				(list[i].medicamento || 
					list[i].nmMedicamento)) {
				return true;
			}
		}
		return false;
	}

	// verifica se algum medicamento do BD foi marcado
	$scope.hasSelectedMedicamento = function(listaMedicamentos) {
		var list = listaMedicamentos || $scope.receituarioList;
		if(!list) {
			return false;
		}
		for(var i = 0; i < list.length; ++i) {
			if(list[i] && list[i].medicamento) {
				return true;
			}
		}
		return false;
	}

	// verifica se um medicamento já foi selecionado
	$scope.checkMedSelection = function(medicamento) {
		for(var i = 0; i < $scope.receituarioList.length - 1; ++i) {
			var r = $scope.receituarioList[i];
			if(r.medicamento && r.medicamento.codigo == medicamento.codigo) {
				toastr.info('', 'Medicamento já incluido, selecione outro');
				return false;
			}
		}
		return true;
	}

	// remove um medicamento do receituario
	$scope.removeMedAtIndex = function(index) {
		if($scope.receituarioList[index]) {
			$scope.receituarioList[index].medicamento = undefined;
			$scope.receituarioList[index].nmMedicamento = '';
			var input = document.getElementById('medicamento-'+index);
			$timeout(function() {
				input.focus();
				input.select();
			}, 0);
		}
	}

	// remove linhas vazias em determinado indice ou em toda lista de receituario
	$scope.removeEmptyRows = function(index) {
		if(index == null || index == undefined) {
			for(var i = 0; i < $scope.receituarioList.length; ++i) {
				var r = $scope.receituarioList[i];
				if((i != $scope.receituarioList.length - 1) && 
					(!(r.medicamento || r.nmMedicamento) && !r.nmPosologia && !r.nmOrientacoes)) {
					$scope.removeMedRow(i);
				}
			}
		} else {
			$scope.removeMedRow(index);
		}
	}

	// remove uma linha do receituario
	$scope.removeMedRow = function(index) {
		if($scope.receituarioList) {
			var findex;
			if((index != undefined || index != null) && index < $scope.receituarioList.length -1 ) {
				if($scope.receituarioList[index]) {
					$scope.receituarioList.splice(index, 1);
					if(!$scope.receituarioList[index].medicamento) {
						findex = index;
						var r = document.getElementById('medicamento-'+findex);
						$timeout(function() {
							r.select();
							r.focus();
						}, 0);
					}
				}
			}
		}
	}

	// processa o receituario e retorna as linhas validas 
	// [linhas com medicamento selecionado (ou apenas o nome) E posologia]
	$scope.getValidMedSelection = function(medList) {
		var list = [];
		for(var i = 0; i < medList.length; ++i) {
			if(medList[i].nmPosologia && 
				(medList[i].medicamento || medList[i].nmMedicamento)) {
				if(medList[i].medicamento) {
					var m = medList[i].medicamento;
					medList[i].nmMedicamento = m.descricao
					medList[i].vlMedicamento = m.vlMedicamento;
				} else {
					medList[i].vlMedicamento = undefined;
				}
				list.push(medList[i]);
			}
		}
		return list;
	}

	// checa se uma linha linha do receituario é valida
	$scope.isValidMedRow = function(index) {
		var rec = $scope.receituarioList[index];
		var result = (
			(rec.medicamento || rec.nmMedicamento) && rec.nmPosologia) ||
			((!rec.medicamento || !rec.nmMedicamento) && 
				!rec.nmPosologia && !rec.nmOrientacoes);
		return result;
	}

	// indica se o aviso de linha invalida deve ser mostrado
	$scope.shouldShowInvalidWarning = function(index) {
		var r = $scope.receituarioList[index];
		var med = $('#medicamento-'+index);
		var posol = $('#posologia-'+index);
		if((!(r.medicamento || r.nmMedicamento) && med.hasClass('ng-touched'))
			|| (!r.nmPosologia  && posol.hasClass('ng-touched'))) {
			return true;
		} else if(!r.n)
		return false;
	} 

	// retorna a mensagem de erro para uma linha do receituario
	$scope.getTooltipMessage = function(index) {
		var r = $scope.receituarioList[index];
		var msg = '';
		if(!(r.medicamento || r.nmMedicamento)) {
			msg = 'Indique um medicamento';
		}
		if(!r.nmPosologia) {
			var link = '';
			var ending = '';
			if(msg != '') {
				link = ' e ';
			} else {
				if(!r.medicamento) {
					ending = ' ou clique para excluir';
				}
			}
			msg += link + 'Digite a posologia' + ending;
		}
		return msg;
	}
	//-------------------------------------------------------------------
	
	var inicio = function() {
		$scope.selectedMedicamento = [];
		$scope.showingRecs = true;

		$scope.receituario = getReceituario();
		$scope.buttonDisabled = false;		
		getProfissional();
		loadTemplatesReceituario();
	};

	inicio();

	$scope.consultar = function(name) {
		if(!name) { return;}
		var rParams = {
			nmMedicamento: name.toLowerCase()
		}
		return servicesFactory.searchMedicamentos.get(rParams,
			null, null).$promise;
	}

	// formada os dados do receituario para html para ser salvo
	$scope.insertIntoNote = function(list) {
		var sumnote = $('#text-receituario');
		sumnote.html('');

		var tbody = document.createElement('tbody');
		var theader = document.createElement('thead');
		var trh = document.createElement('tr');
		var th1 = document.createElement('td');
		th1.innerHTML = 'Medicamento';
		var th2 = document.createElement('td');
		th2.innerHTML = 'Posologia';
		var th3 = document.createElement('td');
		th3.innerHTML = 'Orientações';
		trh.appendChild(th1);
		trh.appendChild(th2);
		trh.appendChild(th3);
		tbody.appendChild(trh);
		// theader.appendChild(trh);
		// simpleTb.appendChild(theader);
		for(var i = 0; i < list.length; ++i) {
			var r = list[i];
			// linha
			var tr = document.createElement('tr');
			// coluna de medicamento
			var td1 = document.createElement('td');
			td1.innerHTML = r.nmMedicamento;
			// coluna de posologia
			var td2 = document.createElement('td');
			td2.innerHTML = r.nmPosologia || '';
			// coluna de recomendacoes
			var td3 = document.createElement('td');
			td3.innerHTML = r.nmOrientacoes || '';
			tr.appendChild(td1);
			tr.appendChild(td2);
			tr.appendChild(td3);
			tbody.appendChild(tr);
		}
		if(tbody.childElementCount > 1) {
			var simpleTb = document.createElement('table');
			simpleTb.className = 'table table-bordered';
            simpleTb.setAttribute('style', 'font-size: 11px; width: 100%;');
			simpleTb.appendChild(tbody);

			sumnote.append(simpleTb);
		}

		if($scope.generalRec) {
			if(tbody.childElementCount > 1) {
				var br = document.createElement('br');
				sumnote.append(br);
			}
			sumnote.append($scope.generalRec);
		}
	}

	//----------------------------------------------------------------------------
	function loadTemplatesReceituario() {
		var rParams = {
			idAgendamento: $stateParams.idAgendamento
		}
		servicesFactory.templatesReceituarioForAgendamento.get(
			rParams,
			function(response) {
				$scope.listTemplates = response;
			}
		);
	}

	$scope.applyTemplate = function(template) {
		if($scope.hasInsertedMedicamento()) {
			$ngBootbox.confirm('Ao aplicar o template, as alterações feitas até o momento serão \
				perdidas. Deseja mesmo continuar?').then(function() {
					$scope.receituarioList = addReceituarioItems(template);
					$scope.generalRec = undefined;
					var findex = $scope.receituarioList.length;
					$scope.addMedRow(findex);
				});
		} else {
			$scope.receituarioList = addReceituarioItems(template);
			$scope.generalRec = undefined;
			var findex = $scope.receituarioList.length;
			$scope.addMedRow(findex);
		}
	}

	function addReceituarioItems(template) {
		var list = [];
		for(var i = 0; i < template.medicamentosTemplate.length; ++i) {
			list.push({
				medicamento: template.medicamentosTemplate[i].medicamento,
				nmMedicamento: template.medicamentosTemplate[i].nmMedicamento,
				nmPosologia: template.medicamentosTemplate[i].nmPosologia,
				nmOrientacoes: template.medicamentosTemplate[i].nmOrientacoes
			});
		}
		return list;
	}
});