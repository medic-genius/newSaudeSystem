/* 

@author : Joelton
@since : 11-05-2016
@last-modification: 19-05-2016
@version :  1.0 

*/

'use strict';

MedicsystemApp.controller('PacienteAnamneseEspecificaController', function($rootScope,$scope, $http, $timeout,$state,$stateParams,$modal,$filter, servicesFactory) {
	

	var recuperarAnamneseGeral = function( tipoCliente ,idPaciente ) {

		$scope.anamneseGeral = inicializaAnamenseGeral();

		servicesFactory.recuperarPreAnamnese.get( {idPaciente: idPaciente, inTipoPaciente: tipoCliente  } , 
	   		function(  result ) {
	   			if(  result.length > 0  )  {
	   				$scope.anamneseGeral.atendimentomedicoanamneseview = result;
	   			} else {
	   				if(  $scope.anamneseGeral.atendimentomedicoanamneseview.length ==  0  ) {
						servicesFactory.recuperarQuestionarioPreAnamnese.get(function( resultQuestionario ) {
							$scope.anamneseGeral.atendimentomedicoanamneseview = resultQuestionario;	
						});
					}
	   		    }
          });

		servicesFactory.recuperarAnamnese.get( {idPaciente: idPaciente, inTipoPaciente: tipoCliente } , 
	   		function( result ) {
	   			if(result)
	   				$scope.anamneseGeral.anamnese = result;
	   			else 
	   				$scope.anamneseGeral.anamnese = null;
         });
	};


	var inicializaAnamenseGeral = function () {
		 var anamneseGeral =  {
			anamnese : {},
			atendimentomedicoanamneseview : [], //questionario de perguntas
		};	

		return anamneseGeral;
	};

	$scope.finalizarAnamnese = function(anamneseGeral) {
		if ( $('#formAnamnese').valid() ) {

			if( !validarPerguntas( anamneseGeral.atendimentomedicoanamneseview  ) ) {
				$scope.aviso = "Ao responder o questionário, todas as perguntas são obrigatórias *";

				$rootScope.alerts.push({
	                type: "danger", 
	                msg: " Verifique os campos obrigatórios !",
	                timeout: 5000 });
	                Metronic.scrollTop();   
	                $scope.buttonFinalizarDisabled = false;    
			} else {
				anamneseGeral.atendimentomedicoanamneseview = addPropertiesQuestion( anamneseGeral.atendimentomedicoanamneseview  );

				var anamneseGeralToSave = inicializaAnamenseGeral();
				anamneseGeralToSave.anamnese = anamneseGeral.anamnese;
				anamneseGeralToSave.anamnese.idPaciente = $stateParams.idPaciente;
				anamneseGeralToSave.anamnese.inTipoPaciente = $stateParams.tipoCliente;
				anamneseGeralToSave.atendimentomedicoanamneseview = anamneseGeral.atendimentomedicoanamneseview;
			

				var anamneseJson  = JSON.stringify(anamneseGeralToSave);

				$scope.buttonFinalizarDisabled = true;

				if( anamneseGeralToSave.anamnese.idAnamnese == null ) {
					saveAnamnese( anamneseJson );	
				} else {
					updateAnamense( anamneseJson );
				}
			}
		} 
	};


	var saveAnamnese = function( anamneseJsonToSave ) {
		servicesFactory.saveAnamnese
		 .save(  anamneseJsonToSave , function( result ) {
	   			if(result) {
	   				
	   				$rootScope.alerts.push({
	                type: "success", 
	                msg: " Anamnese salva com sucesso !",
	                timeout: 3000 });
	                Metronic.scrollTop();   

	                $scope.buttonFinalizarDisabled = false;      

	           		inicio();     
	   			}
         }); 
	};


	var updateAnamense = function( anamneseJsonToUpdate ) {

		servicesFactory.atualizarAnamnese
		 .update( anamneseJsonToUpdate , function( result ) {
	   			if(result) {
	   				$rootScope.alerts.push({
	                type: "info", 
	                msg: "Anamnese atualizada com sucesso",
	                timeout: 3000 });
	                Metronic.scrollTop();

	                $scope.buttonFinalizarDisabled = false;

	                inicio();             
	   			}
         }); 
	};



	var addPropertiesQuestion = function( questions ) {
		
		var i;
		var idPacinenteConvertido =  $stateParams.idPaciente;
		var qdtRespostaVazia = 0;

		for(i = 0; i < questions.length; i++ ) {

			var question = questions[i];

			if( question.inResposta == null ) {
				questions = [];
				return questions;
			}  

			if( question.inResposta == null ) {
				qdtRespostaVazia++;	
			} 

			if( $stateParams.tipoCliente == 'C' ) {
				question.idCliente =  Number(idPacinenteConvertido);
			} else if( $stateParams.tipoCliente == 'D' ) {
				question.idDependente = Number(idPacinenteConvertido);
			}
		}


		return questions;

	};


	var validarPerguntas = function( questions ) {

		var qdtRespostas = 0;
		var i ;

		for(i = 0; i < questions.length; i++ ) {

			var question = questions[i];

			if( question.inResposta != null )
				qdtRespostas++;
		}

		if( qdtRespostas == 0  || qdtRespostas ==  questions.length )
			return true;
		else 
			return false;

	};


	function isEmpty(obj) {
	    for(var prop in obj) {
	        if(obj.hasOwnProperty(prop))
	            return false;
	    }

    	return true;
	 };


	var inicio = function() {
		recuperarAnamneseGeral( $stateParams.tipoCliente ,$stateParams.idPaciente );
	};
	

	inicio();

});


