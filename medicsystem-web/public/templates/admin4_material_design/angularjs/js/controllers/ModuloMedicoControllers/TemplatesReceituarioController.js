'use strict';

MedicsystemApp.controller('TemplatesReceituarioController', function($scope, $state, $timeout, 
	$ngBootbox, toastr, servicesFactory) {
	function resetTemplateReceituarioObject() {
		$scope.templateReceituario = {
			nmTemplate: undefined,
			medicamentosTemplate: []
		}
	}

	function inicio() {
		$scope.medico = {
			selected: undefined
		};

		$scope.especialidade = {
			selected: undefined
		};

		$scope.templateControl = {
			status: undefined,
			templateClone: undefined
		}

		resetTemplateReceituarioObject();
		$scope.buttonDisabled = false;
	}

	// $scope.$on('$viewContentLoaded', function() {
	// 	inicio();
	// });
	if($state.is('atendimentoMedico.templates-receituario')) {
		inicio();
		if($scope.selectedMedico && $scope.selectedMedico.id) {
			loadEspecialidades($scope.selectedMedico);
		}
	}

	function loadEspecialidades(item) {
		$scope.loadingEspecialidades = true;
		$scope.templateControl.status = undefined;
		$scope.listTemplates = undefined;
		$scope.especialidade.selected = undefined;
		var rParams = {
			idMedico: item.id
		}
		servicesFactory.especialidadesMedico.get(
			rParams,
			function(response) {
				$scope.listEspecialidades = response;
			}
		).$promise.finally(function() {
			$scope.loadingEspecialidades = false;
		});
	}

	$scope.searchMedico = function(nmMedico) {
		if(!nmMedico || nmMedico.length < 3) { return ;}
		var rParams = {
			nmMedico: nmMedico
		}
		servicesFactory.searchMedico.get(
			rParams,
			function(response) {
				$scope.listMedicos = response;
				$scope.selectedMedicoNotFound = undefined;
				if(!response && response.length == 0) {
					$scope.selectedMedicoNotFound = 'Não Encontrado';
				}
			}
		);
	}

	$scope.selectedEspecialidade = function(esp) {
		$scope.templateControl.status = undefined;
		var rParams = {
			idMedico: $scope.selectedMedico.id,
			idEspecialidade: esp.idEspecialidade
		}
		servicesFactory.templatesReceituarioForEspProfissional.get(
			rParams,
			function(response) {
				if(response && response.length > 0) {
					$scope.listTemplates = response;
				} else {
					$scope.listTemplates = [];
				}
			}
		);
	}

	//--------------------------------------------------------------------------------
	/** Funções de edição do receituario */
	$scope.configureTemplateReceituario = function(template) {
		var finallyCallbak = function() {
			var findex = $scope.receituarioList.length;
			addMedRow(findex);
			// if(findex > 0) {
			// 	$timeout(function() {
			// 		var input = document.getElementById('medicamento-' + findex);
			// 		input.focus();
			// 	}, 800);
			// }
		}

		if(!template || !template.id) {
			$scope.templateControl.status = 'creating';
			$scope.receituarioList = [];
			finallyCallbak();
		} else {
			$scope.templateReceituario = template;
			$scope.receituarioList = template.medicamentosTemplate;
			$scope.templateControl.templateClone = JSON.parse(JSON.stringify(template));
			$scope.templateControl.status = 'editing';
			finallyCallbak();
			// // load template salvo
			// var rParams = {
			// 	idTemplate: template.id
			// }
			// servicesFactory.templateReceituario.get(rParams,
			// 	function(response) {
			// 		$scope.templateReceituario = response;
			// 		$scope.receituarioList = response.medicamentosTemplate;
			// 		$scope.templateStatus = 'editing';
			// 	}
			// ).$promise.finally(finallyCallbak);
		}
	}

	$scope.cancelAction = function() {
		if($scope.templateControl.status == 'editing') {
			var tplClone = $scope.templateControl.templateClone;
			$scope.templateReceituario['nmTemplate'] = tplClone.nmTemplate;
			$scope.templateReceituario['medicamentosTemplate'] = tplClone.medicamentosTemplate;
		}
		$scope.templateControl.status = undefined;
		resetTemplateReceituarioObject();
	}

	$scope.deleteTemplate = function(templateObj) {
		$ngBootbox.confirm('Deseja realmente excluir este template?').then(
			function() {
				templateObj.isDeleting = true;
				var rParams = {
					idTemplate: templateObj.id
				}
				servicesFactory.templateReceituario.delete(rParams, function(response) {
					var index = $scope.listTemplates.indexOf(templateObj);
					if(index != -1) {
						$scope.listTemplates.splice(index, 1);
					}
				}, function(err) {
					templateObj.isDeleting = false;
				});
			}
		);
	}

	$scope.saveTemplate = function() {
		$scope.buttonDisabled = true;
		var vselection = getValidMedSelection($scope.receituarioList);
		if(!vselection || vselection.length == 0) {
			toastr.error('', 'Medicamentos não estão preechidos corretamente');
			$scope.receituarioList = vselection;
			$scope.buttonDisabled = true;
			return;
		}
		$scope.templateReceituario.medicamentosTemplate = vselection;
		persistTemplate($scope.templateReceituario);
		return;
	};

	function persistTemplate(template) {
		if(!template.id) {
			var rParams = {
				idMedico: $scope.selectedMedico.id,
				idEspecialidade: $scope.especialidade.selected.idEspecialidade
			}
			servicesFactory.templatesReceituarioForEspProfissional.post(
				rParams,
				template,
				function(response) {
					if(response && response.id) {
						toastr.success('', 'Template salvo');
						$scope.listTemplates.push(response);
						$scope.templateControl.status = undefined;
					} else {
						toastr.error('', 'Erro ao salvar template')
					}
				}
			).$promise.finally(function() {
				$scope.buttonDisabled = false;
			});
		} else {
			var rParams = {
				idTemplate: template.id
			}
			template.espProfissional = null;
			servicesFactory.templateReceituario.put(
				rParams,
				template,
				function(response) {
					if(response && response.id) {
						toastr.success('', 'Template atualizado com sucesso');
						$scope.templateControl.status = undefined;
					} else {
						toastr.error('', 'Erro ao editar template');
					}
				}
			).$promise.finally(function() {
				$scope.buttonDisabled = false;
			});;
		}
	}

	//-------------------------------------------------------------------
	$scope.onBlurEvent = function(obj, index) {
		if((obj.medicamento || obj.nmMedicamento) && !$scope.receituarioList[index+1]) {
			addMedRow(index+1);
		}
		removeEmptyRows();
	}

	// processa a escolha de um medicamento
	$scope.medSelected = function(item, obj, index) {
		var s = checkMedSelection(item);
		obj.nmMedicamento = '';
		if(s) {
			obj.medicamento = item;
			var pos = document.getElementById('posologia-'+index);
			pos.focus();
			// $scope.addMedRow(index + 1);
		}
		removeEmptyRows();
	}

	// metodos para adicionar linha de receituario
	function addMedRow(index) {
		if(!$scope.receituarioList) {
			$scope.receituarioList = [];
		}
		if(!$scope.receituarioList[index]) {
			$scope.receituarioList[index] = {
				// idReceituario: $scope.receituario.id,
				medicamento: undefined,
				nmMedicamento: undefined,
				nmPosologia: undefined,
				nmOrientacoes: undefined
			};
			$timeout(function() {
				var txts = document.querySelectorAll('textarea');
				autosize(txts);
			}, 1000);
		}
	}

	// verifica se existem medicamentos no receituario
	$scope.hasInsertedMedicamento = function(listaMedicamentos) {
		var list = listaMedicamentos || $scope.receituarioList;
		if(!list) {
			return false;
		}
		for(var i = 0; i < list.length; ++i) {
			if(list[i] && 
				(list[i].medicamento || 
					list[i].nmMedicamento)) {
				return true;
			}
		}
		return false;
	}

	// verifica se algum medicamento do BD foi marcado
	function hasSelectedMedicamento(listaMedicamentos) {
		var list = listaMedicamentos || $scope.receituarioList;
		if(!list) {
			return false;
		}
		for(var i = 0; i < list.length; ++i) {
			if(list[i] && list[i].medicamento) {
				return true;
			}
		}
		return false;
	}

	// verifica se um medicamento já foi selecionado
	function checkMedSelection(medicamento) {
		for(var i = 0; i < $scope.receituarioList.length - 1; ++i) {
			var r = $scope.receituarioList[i];
			if(r.medicamento && r.medicamento.codigo == medicamento.codigo) {
				toastr.info('', 'Medicamento já incluido, selecione outro');
				return false;
			}
		}
		return true;
	}

	// remove um medicamento do receituario
	$scope.removeMedAtIndex = function(index) {
		if($scope.receituarioList[index]) {
			$scope.receituarioList[index].medicamento = undefined;
			$scope.receituarioList[index].nmMedicamento = '';
			var input = document.getElementById('medicamento-'+index);
			$timeout(function() {
				input.focus();
				input.select();
			}, 0);
		}
	}

	// remove linhas vazias em determinado indice ou em toda lista de receituario
	function removeEmptyRows(index) {
		if(index == null || index == undefined) {
			for(var i = 0; i < $scope.receituarioList.length; ++i) {
				var r = $scope.receituarioList[i];
				if((i != $scope.receituarioList.length - 1) && 
					(!(r.medicamento || r.nmMedicamento) && !r.nmPosologia && !r.nmOrientacoes)) {
					$scope.removeMedRow(i);
				}
			}
		} else {
			$scope.removeMedRow(index);
		}
	}

	// remove uma linha do receituario
	$scope.removeMedRow = function(index) {
		if($scope.receituarioList) {
			var findex;
			if((index != undefined || index != null) && index < $scope.receituarioList.length -1 ) {
				if($scope.receituarioList[index]) {
					$scope.receituarioList.splice(index, 1);
					if(!$scope.receituarioList[index].medicamento) {
						findex = index;
						var r = document.getElementById('medicamento-'+findex);
						$timeout(function() {
							r.select();
							r.focus();
						}, 0);
					}
				}
			}
		}
	}

	// processa o receituario e retorna as linhas validas 
	// [linhas com medicamento selecionado (ou apenas o nome) E posologia]
	function getValidMedSelection(medList) {
		var list = [];
		for(var i = 0; i < medList.length; ++i) {
			if(medList[i].nmPosologia && 
				(medList[i].medicamento || medList[i].nmMedicamento)) {
				if(medList[i].medicamento) {
					var m = medList[i].medicamento;
					medList[i].nmMedicamento = m.descricao
					// medList[i].vlMedicamento = m.vlMedicamento;
				} else {
					// medList[i].vlMedicamento = undefined;
				}
				list.push(medList[i]);
			}
		}
		return list;
	}

	// checa se uma linha linha do receituario é valida
	$scope.isValidMedRow = function(index) {
		var rec = $scope.receituarioList[index];
		var result = (
			(rec.medicamento || rec.nmMedicamento) && rec.nmPosologia) ||
			((!rec.medicamento || !rec.nmMedicamento) && 
				!rec.nmPosologia && !rec.nmOrientacoes);
		return result;
	}

	// indica se o aviso de linha invalida deve ser mostrado
	$scope.shouldShowInvalidWarning = function(index) {
		var r = $scope.receituarioList[index];
		var med = $('#medicamento-'+index);
		var posol = $('#posologia-'+index);
		if((!(r.medicamento || r.nmMedicamento) && med.hasClass('ng-touched'))
			|| (!r.nmPosologia  && posol.hasClass('ng-touched'))) {
			return true;
		} else if(!r.n)
		return false;
	} 

	// retorna a mensagem de erro para uma linha do receituario
	$scope.getTooltipMessage = function(index) {
		var r = $scope.receituarioList[index];
		var msg = '';
		if(!(r.medicamento || r.nmMedicamento)) {
			msg = 'Indique um medicamento';
		}
		if(!r.nmPosologia) {
			var link = '';
			var ending = '';
			if(msg != '') {
				link = ' e ';
			} else {
				if(!r.medicamento) {
					ending = ' ou clique para excluir';
				}
			}
			msg += link + 'Digite a posologia' + ending;
		}
		return msg;
	}
	//-------------------------------------------------------------------

	$scope.consultar = function(name) {
		if(!name) { return;}
		var rParams = {
			nmMedicamento: name.toLowerCase()
		}
		return servicesFactory.searchMedicamentos.get(rParams,
			null, null).$promise;
	}

	$scope.focusOnRec = function() {
		$scope.showingRecs = !$scope.showingRecs;
		if($scope.showingRecs) {
			var txtRec = document.getElementById('general-rec-txt');
			$timeout(function() {
				txtRec.focus();
			}, 500);
		}
	}
	//--------------------------------------------------------------------------------
});