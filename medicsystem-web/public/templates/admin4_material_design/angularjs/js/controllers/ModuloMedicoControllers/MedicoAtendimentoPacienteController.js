'use strict';

MedicsystemApp.controller('MedicoAtendimentoPacienteController', function($rootScope, $scope, $state, 
	$stateParams, $timeout, $modal, toastr, servicesFactory, Auth) {
	
	var inicio = function( idPaciente, tipoCliente, nmPaciente, idAgendamento, idEspecialidade ) {
		$scope.idPaciente = idPaciente;
		$scope.tipoCliente = tipoCliente;
		$scope.nmPaciente = nmPaciente;
		$scope.idAgendamento = idAgendamento;
		$scope.buttonFinalizarDisabled = false;
		$scope.canSeeOftalmologia = false;
		$scope.idEspecialidade = idEspecialidade;
		getProfissional();
		checkFinalizacaoAtendimento(idAgendamento);
	};

	var verificarEspecialidadeOftalmologico = function( idProfissional ) {
 		servicesFactory.especialidadesProfissional
 		.get( {idProfissional: idProfissional}, function( result ) {
	   			if( result ) {
	   				var especialidades = result;
	   				var idEspecialidadeOftalmologica = 11; 
	   				var idEspecialidadeOptometrista = 82;
	   				var idEspeciadadeSendoUtilizada = $stateParams.idEspecialidade;

	   				for (var i = 0; i < especialidades.length; i++) {
	   					var idEspecialidade = especialidades[i].idEspecialidade;
	   					if( 
	   						(idEspecialidade == idEspecialidadeOftalmologica &&
	   						idEspeciadadeSendoUtilizada == idEspecialidadeOftalmologica) ||
	   						( idEspecialidade == idEspecialidadeOptometrista &&
	   						  idEspeciadadeSendoUtilizada == idEspecialidadeOptometrista)
	   					   ) {
	   						$scope.canSeeOftalmologia = true;
	   						break;
	   					} 
	   				};
	   			}
          });	
 	};


	var getProfissional = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		//var login =  "wilson.ramos";
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {
	   			if( result ) {
	   				$scope.idProfissional = result.id;
	   				verificarEspecialidadeOftalmologico( $scope.idProfissional );
	   			}		
          });
	};


	$scope.finalizarAtendimento = function() {
		$scope.buttonFinalizarDisabled = true;	
		servicesFactory.atualizarAgendamento
	 	.update( {idAgendamento : $scope.idAgendamento, idProfissional: $scope.idProfissional } , function( result ) {
		 	if(result) {
		 		$scope.buttonFinalizarDisabled = false;
		 		$state.go('medico-visualizarPacientes',{idEspecialidade:$scope.idEspecialidade});
		 	}
	 	},function(){
	 		$scope.buttonFinalizarDisabled = false;
	 	});
	};

	$scope.existeAtendimento = function(atendimentoBarcode) {
		servicesFactory.recuperarAtendimento.get(
			{idAgendamento: $stateParams.idAgendamento},
			function( result ) {
			if(result != null ) {
				if(result.idAtendimento != null && (result.nmDiagnostico != null && result.nmDiagnostico.length > 0)) {
					if(!$scope.shouldReadBarcode) {
						$scope.finalizarAtendimento();
					} else {
						if(atendimentoBarcode == $stateParams.idAgendamento) {
							$scope.finalizarAtendimento();
						} else {
							var title = 'Erro';
							var msg = 'Atendimento a ser finalizado não corresponde ao comprovante lido';
							toastr.error(msg, title);
						}
					}
				} else {
					$rootScope.alerts.push({
						type: "danger", 
						msg: "As informações básicas são necessárias para finalizar o atendimento! ( menu atendimento)",
						timeout: 5000
					});
					Metronic.scrollTop(); 
				}
			} else {
				$rootScope.alerts.push({
					type: "danger", 
					msg: "As informações básicas são necessárias para finalizar o atendimento! ( menu atendimento)",
					timeout: 5000 });
				Metronic.scrollTop();
			}
		});
	};

	function checkFinalizacaoAtendimento(agendamentoId) {
		var rParams = {
			idAgendamento: agendamentoId
		}
		servicesFactory.checkFinalizacaoAtendimento.get(
			rParams,
			function(result) {
				$scope.shouldReadBarcode = result && result.lerCodigoDeBarras;
			},
			function(err) {
				$scope.shouldReadBarcode = false;
			}
		);
	}

	if($stateParams.idPaciente) {
		inicio($stateParams.idPaciente, $stateParams.tipoCliente, 
			$stateParams.nmPaciente, $stateParams.idAgendamento, 
			$stateParams.idEspecialidade);
	}

	$scope.openConfirmModal = function() {
		$scope.modalInstance = $modal.open({
			animation: true,
			templateUrl: 'finish-box.html',
			controller: 'ConfirmModalControllerController',
			backdrop: 'static',
			size: 'sm',
			resolve:  {
				shouldReadBarcode: function() {
					return $scope.shouldReadBarcode;
				},
				nmPaciente: function() {
					return $scope.nmPaciente;
				}
			}
		});

		$scope.modalInstance.result.then(function(barcode) {
			$scope.existeAtendimento(barcode);
		});
	}
})

.controller('ConfirmModalControllerController', function($scope, $modalInstance, 
	$timeout, shouldReadBarcode, nmPaciente) {
	$scope.nmPaciente = nmPaciente;
	$scope.shouldReadBarcode = shouldReadBarcode;

	$timeout(function() {
		var bcInput = $("#barcode-input");
		bcInput.focus();
		bcInput.on('blur', function() {
			bcInput.focus();
		});
	}, 500);

	$scope.modelChanged = function() {
		$scope.barcodeModel = ($scope.barcodeModel || '') + event.key;
		$scope.barcodeModel = $scope.barcodeModel.replace(/\D/g, '');
		processValueChanges();
	}

	function processValueChanges() {
		if($scope.barcodeModel && !$scope.valStarted) {
			$scope.valStarted = true;
			$timeout(function() {
				if($scope.barcodeModel && $scope.barcodeModel.length > 5) {
					$scope.modalConfirmBtn($scope.barcodeModel);
				}
				$scope.barcodeModel = '';
				$scope.valStarted = false;
			}, 500);
		}
	};

	$scope.modalConfirmBtn = function(atendimentoBarcode) {
		$("#barcode-input").off('blur');
		$modalInstance.close(atendimentoBarcode);
	}

	$scope.cancel = function() {
		$("#barcode-input").off('blur');
		$modalInstance.dismiss('cancel');
	}
});


