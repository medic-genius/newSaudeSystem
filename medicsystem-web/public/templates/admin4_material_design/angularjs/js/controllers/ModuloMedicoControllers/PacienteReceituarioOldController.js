/* 
@author : Joelton
@since : 12-05-2016
@last-modification: 03-06-2016
@version :  1.1 
*/

'use strict';

MedicsystemApp.controller('PacienteReceituarioOldController', function($rootScope, 
	$scope, $http, $timeout,$state,$stateParams, $modal, $filter, servicesFactory, Auth) {
	
	//EDITOR DE TEXTO
	 $scope.options = {
	    height: 300,
	    focus: true,
	    airMode: false,
	    toolbar: [
	            ['edit',['undo','redo']],
	            ['style', ['bold', 'italic', 'clear']],
	            ['textsize', ['fontsize']],
	            ['alignment', ['ul', 'lineheight']],
	            ['table', ['table']],
	        ],
	  };

	// BEGIN PRINT REPORT RECEITUÁRIO
  	var gerarPdfExames = function() {
  		getImageReport('assets/global/img/medic-lab-logo.jpg',printReceituario);
  	};

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			callBack(img);
		};

		img.src = url;
	};

	var printReceituario = function( imgData ) {

		var nmMedico = $scope.profissional.nmFuncionario;
		var crm =  $scope.profissional.nrCrmCro;
		var nmPaciente = $stateParams.nmPaciente;

		var data =  new Date();
		var dia = data.getDate() > 9 ? data.getDate() : ('0'+ (data.getDate() ));
		var mes = (data.getMonth() + 1) > 9 ? (data.getMonth() + 1) : ('0'+ (data.getMonth() + 1));
		var ano = data.getFullYear().toString();
		var dtAtendimento = dia+"/"+mes+"/"+ano;
		var receituario =  $(".note-editable").html();
		
		var doc = new jsPDF('p','pt','letter','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Receituário',{align:'center'});

		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 170, 'Médico(a):');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(97, 170, nmMedico );

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 190, 'Paciente:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(92, 190, nmPaciente);

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(322, 190, 'Data do atendimento:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(436, 190, dtAtendimento);
		
		//content
		var specialElementHandlers = {
	      	'DIV to be rendered out': function(element, renderer) {
	       		return true;
	    	}
	    };

		doc.setFont("times");
		doc.setFontSize(12);
		doc.fromHTML($('.note-editable, .panel-body').get(0), 40, 226, {
			'width': 500 ,
			'elementHandlers': specialElementHandlers
		});

		//doc.writeText(40, 226 ,receituario,{align:'left',width:100});

		//ASSINATURA
		doc.setFont("times");
		doc.setFontSize(12);
		doc.writeText(0, 723, '____________________________________',{align:'center'});			

		doc.setFont("times");
		doc.setFontSize(12);
		if( nmMedico.length > 30  ) {
			var nmTemp = nmMedico.split(" ");
			nmMedico = nmTemp[0] + " " + nmTemp[nmTemp.length - 1];		
			doc.writeText(0, 752, nmMedico,{align:'center'});
			doc.writeText(0, 772, 'CRM '+crm,{align:'center'});
		} else {
			doc.writeText(0, 752, nmMedico,{align:'center'});
			doc.writeText(0, 772, 'CRM '+ crm,{align:'center'});
		}
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
	};

// END PRINT REPORT RECEITUÁRIO

	var getProfissional = function() {

		var login =  Auth.authz.idTokenParsed.preferred_username;
	
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {

	   			if( result ) {
	   				$scope.profissional = result;
	   			}
          });
	};

	var getReceituario = function() {

		servicesFactory.recuperarReceituario
		 .get( {idAgendamento: $stateParams.idAgendamento} , function( result ) {
	   			if( result ) {		
	   				$scope.receituario = result;
	   			} else {
	   				$scope.receituario = {};
	   			}

         });  
	
		return $scope.receituario;
	};


	$scope.finalizarReceituario = function( receituario ) {

		if( $('#formReceituario').valid() ) {

			var receituarioToSave = formatarReceituario( receituario ) ;
			$scope.buttonDisabled = true;
			if( receituarioToSave.id != null )

				atualizarReceituario( receituarioToSave );

			else 
				salvarReceituario( receituarioToSave );

		}

	};


	var salvarReceituario = function( receituario ) {

		servicesFactory.saveReceituario
		 .save( { idAgendamento: $stateParams.idAgendamento ,
		 	idPaciente: $stateParams.idPaciente, inTipoPaciente: $stateParams.tipoCliente  }, receituario , function( result ) {
	   			if(result) {		
	   				$rootScope.alerts.push({
	                type: "success", 
	                msg: " Receituario salvo com sucesso!",
	                timeout: 3000 });
	                Metronic.scrollTop(); 

	                $scope.canFindReceituario = true;  

	                $scope.buttonDisabled = false;
	                
	                getReceituario();      
	                gerarPdfExames();
	   			}
         }); 
	};


	var atualizarReceituario = function( receituarioAtualizar ) {

		servicesFactory.updateReceituario
		 .update( receituarioAtualizar , function( result ) {
	   			if(result) {		
	   				$rootScope.alerts.push({
		                type: "info", 
		                msg: " Receituario atualizado com sucesso!",
		                timeout: 3000 
	            	});

	                Metronic.scrollTop();   

	                $scope.buttonDisabled = false;
	                
	                getReceituario();
	                gerarPdfExames();
	   			}
         }); 
	};


	var formatarReceituario = function( receituario ) {

		var receituarioToSave;

		if(  receituario.id == null ) {
			receituarioToSave = {
				nmReceituario: receituario.nmReceituario,
				dtReceituario:  $filter('date')(new Date(),'yyyy-MM-dd')
			}
			
		} else {
			receituarioToSave = {
				id: receituario.id,
				nmReceituario: receituario.nmReceituario,
				dtReceituario:  $filter('date')(new Date(),'yyyy-MM-dd')
			}
		}

		return receituarioToSave;

	};
	
	var inicio = function() {
		$scope.receituario = getReceituario();
		$scope.buttonDisabled = false;		
		getProfissional();
	};

	inicio();

});


