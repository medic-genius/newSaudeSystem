'use strict';

MedicsystemApp.controller('PacienteLaudoController', function($rootScope, $sce ,$scope, $http, $timeout,$state,$stateParams,$modal,$filter, servicesFactory, Auth, toastr) {
	var getProfissional = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {
	   				$scope.idProfissional = result.id;
		
				if ($scope.idEspecialidade != null && $scope.idProfissional != null){
					servicesFactory.getTemplate.get({
					idEspecialidade: $scope.idEspecialidade,
					idFuncionario: $scope.idProfissional}
					,function(dados){
					
						$scope.template = dados;

					});
				} 		   
          });

		 
	};
	
	
	//EDITOR DE TEXTO
	 $scope.options = {
	    height: 300,
	    focus: true,
	    airMode: false,
		fontSizes: ['8', '9', '10', '11','12','13', '14', '18', '24', '36'],
	    toolbar: [
	            ['font', ['bold', 'italic', 'underline']],
				['color', ['color']],
	            ['para', ['paragraph', 'ol']],
				['textsize', ['fontsize']],
	        	['table', ['table']],	
				['view', ['fullscreen']],
				
	        ],

		
	  };

	

	  $scope.loadMascara = function (item){
		
		for ( var i=0; i<$scope.template.length; i++ ){
			if (item.id == $scope.template[i].id){

				$scope.content = $scope.template[i].nmConteudo;
			}
		}
		

	  }
	  
	  $scope.clear = function (){
		$scope.templateSelected = {id:undefined};
		$scope.content = "";
		
	  }



	var getLaudo = function() {
		servicesFactory.getLaudo
		 .get( {idAgendamento: $scope.idAgendamento} , function( result ) {
	   				$scope.laudo = result; 
					   
	    if ($scope.laudo.nmConteudo){
			  $scope.content = $scope.laudo.nmConteudo;
		}
		});
		
	}; 
	
	// 1 = IMPRIMIR    0 = SALVAR
	$scope.salvarLaudo = function(print) {
	$scope.buttonDisabledSalvar = $scope.content.length;
	$scope.buttonPrint = true;	

			var laudoToSave = formatarLaudo() ;
			
			if( laudoToSave.id != null ){
				servicesFactory.updateLaudo.update( laudoToSave, 
				function(dados){
				
				if (print == 1){
			
			
				
					Object.size = function(obj) {
						var size = 0, key;
						for (key in obj) {
							if (obj.hasOwnProperty(key)) size++;
						}
						return size;
					};
					// Get the size of an object
					var size = Object.size(dados);
					var str = '';
					var lenght = size - 2;
					for (var i=0; i < lenght ; i ++){
						str = str + dados[i];
					}
					var blob = b64toBlob(str, 'application/pdf');
					var fileURL = URL.createObjectURL(blob);
					window.open(fileURL , '_blank');
					
				$scope.buttonPrint = false;
		
					
				}
				$scope.buttonPrint = false;
				getLaudo();
				});
				messageToastr("sucesso","Laudo atualizado com sucesso","LAUDO ATUALIZADO");
				
			}
			else {
				servicesFactory.saveLaudo.save({idAgendamento: $scope.idAgendamento},laudoToSave, 
				function(dados){

				if (print == 1){
			
			
					Object.size = function(obj) {
						var size = 0, key;
						for (key in obj) {
							if (obj.hasOwnProperty(key)) size++;
						}
						return size;
					};
					// Get the size of an object
					var size = Object.size(dados);
					var str = '';
					var lenght = size - 2;
					for (var i=0; i < lenght ; i ++){
						str = str + dados[i];
					}
					var blob = b64toBlob(str, 'application/pdf');
					var fileURL = URL.createObjectURL(blob);
					window.open(fileURL , '_blank');

				$scope.buttonPrint = false;
				
				}
				$scope.buttonPrint = false;
				getLaudo();
				});
				messageToastr("sucesso","Laudo salvo com sucesso","LAUDO SALVO");
				
			}

	$scope.saveLaudo = false;
	};

	var formatarLaudo = function( ) {
		var laudoToSave;

		if ($scope.laudo.id != null){
			laudoToSave = {
				atendimento: $scope.laudo.atendimento,
				id: $scope.laudo.id,
				nmConteudo: $scope.content
			}
		}
		else{
			laudoToSave = {
				nmConteudo: $scope.content
			}
		}
	
		return laudoToSave;

	};
	
	

	function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} ); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() ); 
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() ); 
        }
    };

	var inicio = function() {
		getLaudo();
		$scope.templateSelected = {id:undefined};
		$scope.content = "";
		$scope.buttonDisabledSalvar = $scope.content.length;	
		$scope.saveLaudo = true;
		$scope.buttonPrint = false;

		getProfissional();
	};

	inicio();

	

});

MedicsystemApp.config(function(toastrConfig) {
    angular.extend(toastrConfig, {
        closeButton: true,
            debug: true,
            newestOnTop: true,
            progressBar: false,
            positionClass: 'toast-top-center',
            preventDuplicates: false,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 4500,
            extendedTimeOut: 1000,
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
    });
  });
