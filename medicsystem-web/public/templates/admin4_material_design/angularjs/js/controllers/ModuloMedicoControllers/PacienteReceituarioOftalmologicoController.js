/* 
@author : Joelton
@since : 20-06-2016
@last-modification: 30-06-2016
@version :  1.0
*/

'use strict';

MedicsystemApp.controller('PacienteReceituarioOftalmologicoController', function($rootScope,
	$scope, $http, $timeout,$state,$stateParams,$modal,$filter,
	servicesFactory, Auth) {
	
// BEGIN PRINT REPORT RECEITUÁRIO

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() 
		{ console.log("Nao pode carregar a imagem " + url);};

		img.onload = function() 
		{callBack(img);};

		img.src = url;
	};

	var gerarReceituarioOftalmologico = function() {
  		getImageReport('assets/global/img/medic-lab-logo.jpg',printReceituarioOftalmologico);
  	};

	var printReceituarioOftalmologico = function( imgData ) {

		var nmMedico = $scope.profissional.nmFuncionario;
		var crm =  $scope.profissional.nrCrmCro;
		var nmPaciente = $stateParams.nmPaciente;

		var receituario =  "";
		
		var doc = new jsPDF('p','pt','letter','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');
		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Receituário Oftalmológico',{align:'center'});

		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 170, 'Médico(a):');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(97, 170, nmMedico );

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 190, 'Paciente:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(92, 190, nmPaciente);

		//content
		var receituario = $scope.receituarioOftalmologico;
					
		//table longe
		var dataTableLonge = gerarTableLonge( receituario );
		doc.autoTable( dataTableLonge.columns, dataTableLonge.rows,{
			 startY: 226,
			 margin: {left: 40},
			 theme: 'grid',
		});

		//table perto
		var dataTablePerto = gerarTablePerto( receituario );
		doc.autoTable( dataTablePerto.columns, dataTablePerto.rows,{
			 startY: 326,
			 margin: {left: 40},
			 theme: 'grid',
		});			
		
		//observacao
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 425, 'Observação:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.fromHTML($('.note-editable').get(0), 40, 445, {
			'width': 500 
		});
		
		//ASSINATURA
		doc.setFont("times");
		doc.setFontSize(12);
		doc.writeText(0, 713, '____________________________________',{align:'center'});			

		doc.setFont("times");
		doc.setFontSize(12);
		if( nmMedico.length > 30  ) {
			var nmTemp = nmMedico.split(" ");
			nmMedico = nmTemp[0] + " " + nmTemp[nmTemp.length - 1];		
			doc.writeText(0, 742, nmMedico,{align:'center'});
			doc.writeText(0, 762, 'CRM '+crm,{align:'center'});
		} else {
			doc.writeText(0, 742, nmMedico,{align:'center'});
			doc.writeText(0, 762, 'CRM '+ crm,{align:'center'});
		}
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
	};

	var gerarTableLonge = function( receituario ) {
		var dataTable = {};
		var columns = ["LONGE",'ESFÉRICO',"CILÍNDRICO",'EIXO'];	
		var rows = [
		["OD",receituario.nrLongeOdEsferico,receituario.nrLongeOdCilindrico ,receituario.nrLongeOdEixo],
		["OE",receituario.nrLongeOeEsferico,receituario.nrLongeOeCilindrico,receituario.nrLongeOeEixo]
		];

		dataTable.columns = columns;
		dataTable.rows = rows;

		return dataTable;
	};	

	var gerarTablePerto = function( receituario ) {
		var dataTable = {};
		var columns = ["PERTO",'ESFÉRICO',"CILÍNDRICO",'EIXO'];	
		var rows = [
		["OD",receituario.nrPertoOdEsferico,receituario.nrPertoOdCilindrico ,receituario.nrPertoOdEixo],
		["OE",receituario.nrPertoOeEsferico,receituario.nrPertoOeCilindrico,receituario.nrPertoOeEixo]
		];

		dataTable.columns = columns;
		dataTable.rows = rows;

		return dataTable;
	};

// END PRINT REPORT RECEITUÁRIO
	
	var pushMessage = function( typeMessage, textMessage, timeMessage ) {
		$rootScope.alerts.push({
	    	type: typeMessage, 
	        msg: textMessage,
	        timeout: timeMessage });
	    Metronic.scrollTop(); 
	};

	$scope.optionsEditor = {
	    height: 150,
	    focus: true,
	    airMode: false,
	    toolbar: [
	            ['edit',['undo','redo']],
	            ['style', ['bold', 'italic', 'clear']],
	            ['textsize', ['fontsize']],
	            ['alignment', ['ul', 'lineheight']],
	            ['table', ['table']],
	        ],
	 };

	 var getHistoricoConsultasOftal = function( idPaciente, inTipoPaciente ) {
		servicesFactory.getHistoricoOftal.get( { idPaciente: idPaciente, inTipoPaciente: inTipoPaciente }, 
	   		function( result ) {
	   			if( result ) 
	   				$scope.consultasOftal = result;
          });
	 }

	$scope.finalizarReceituarioOftalmologico = function( receituario ) {
		
		$scope.buttonSaveDisabled = true;
		
		if( receituario.nmObservacao ) {
			if( receituario.id != null )
				atualizarReceituarioOftalmologico( receituario );
			else 
				salvarReceituarioOftalmologico( receituario );	
		} else {
			pushMessage("warning", "A observação é ncessária para o receituário!", 3000 );
			$scope.buttonSaveDisabled = false;
		}
	};

	var salvarReceituarioOftalmologico = function( receituario ) {
		servicesFactory.saveReceituarioOftal
		 .save( { idAgendamento: $stateParams.idAgendamento ,
		 	idPaciente: $stateParams.idPaciente, inTipoPaciente: $stateParams.tipoCliente  }, receituario , function( result ) {
	   			if(result) {
	   				pushMessage("success", " Receituario salvo com sucesso!", 3000 );		
	   				$scope.buttonSaveDisabled = false;
	              	getReceituarioOftalmologico();      
	               	gerarReceituarioOftalmologico();
	               	getHistoricoConsultasOftal( $stateParams.idPaciente, $stateParams.tipoCliente );
	   			}
         }, function(error){
         	$scope.buttonSaveDisabled = false;	
         }); 
	};

	var atualizarReceituarioOftalmologico = function( receituario ) {
		servicesFactory.updateReceituarioOftal
		 .update( receituario , function( result ) {
	   			if(result) {
	   				pushMessage("info", "Receituario atualizado com sucesso!", 3000 );				
	                $scope.buttonSaveDisabled = false;
	                getReceituarioOftalmologico();
	                gerarReceituarioOftalmologico();
	                getHistoricoConsultasOftal( $stateParams.idPaciente, $stateParams.tipoCliente );
	   			}
         }, function(error) {
         	$scope.buttonSaveDisabled = false;
         }); 
	};

	var getProfissional = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {
	   			if( result ) 
	   				$scope.profissional = result;
          });
	};

	var getReceituarioOftalmologico = function() {
		servicesFactory.recuperarReceituarioOftal
		 .get( {idAgendamento: $stateParams.idAgendamento} , function( result ) {
	   			if( result.id ) {		
	   				$scope.receituarioOftalmologico = result;
	   				$scope.buttonTitle = "Editar";
	   			} else {
	   				$scope.receituarioOftalmologico = {};
	   				$scope.buttonTitle = "Salvar";
	   			}
         });  
	};
	
	var inicio = function() {
		getReceituarioOftalmologico();
		getProfissional();
		getHistoricoConsultasOftal( $stateParams.idPaciente, $stateParams.tipoCliente );
		$scope.buttonSaveDisabled = false;
	};

	inicio();

});


//BEGIN ASSINATURA - TEMP
/*	

var gerarReceituarioOftalmologico2 = function(url) {
		console.log('O que chegou ?', url);
  		getImageReport(url,printReceituarioOftalmologico);
  	};

	var wrapper = document.getElementById("signature-pad"),
	    	clearButton = wrapper.querySelector("[data-action=clear]"),
	    	saveButton = wrapper.querySelector("[data-action=save]"),
	    	canvas = wrapper.querySelector("canvas"),
	    	signaturePad;		

	// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = 700//canvas.offsetWidth * ratio;//700 ;//canvas.offsetWidth * ratio;
    canvas.height = 300// canvas.offsetHeight * ratio; //300;// canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

var signaturePad = new SignaturePad(canvas, {
    minWidth: 3,
    maxWidth: 5,
    backgroundColor: 'rgb(255,255,255)',
});



clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
    	
        gerarReceituarioOftalmologico2(signaturePad.toDataURL("image/jpeg"));
        window.open(signaturePad.toDataURL("image/jpeg"));
    }
});*/

//END ASSINATURA