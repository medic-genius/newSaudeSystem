'use strict';

MedicsystemApp.controller('MedicoLancamentoExamesController', function($rootScope,$scope, $http, $timeout,
	$state,$stateParams,$modal,$filter, servicesFactory,$window,Auth) {

// BEGIN PRINT REPORT LANCAMENTO EXAMES
  	var gerarPdfExames = function() {
  		getImageReport('assets/global/img/medic-lab-logo.jpg',printLancamentoExame);
  	};

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			printLancamentoExame(img);
		};

		img.src = url;
	};

	var printLancamentoExame = function( imgData ) {

		var nmMedico = $scope.profissional.nmFuncionario;
		var crm =  $scope.profissional.nrCrmCro;
		var nmPaciente = $stateParams.nmPaciente;

		var data =  new Date();
		var dia = data.getDate() > 9 ? data.getDate() : ('0'+ (data.getDate() ));
		var mes = (data.getMonth() + 1) > 9 ? (data.getMonth() + 1) : ('0'+ (data.getMonth() + 1));
		var ano = data.getFullYear().toString();
		var dtAtendimento = dia+"/"+mes+"/"+ano;
		
		var doc = new jsPDF('p','pt','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Autorização para exames',{align:'center'});

		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 170, 'Médico(a):');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(97, 170, nmMedico );

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 190, 'Paciente:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(92, 190, nmPaciente);

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(322, 190, 'Data do atendimento:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(436, 190, dtAtendimento);

		//TABLE EXAMES
		var dataTable = gerarTable();
		doc.autoTable( dataTable.columns, dataTable.rows,{
			 startY: 206,
			 margin: {left: 40},
			 theme: 'grid',
		});

		//ASSINATURA
		doc.setFont("times");
		doc.setFontSize(12);
		doc.writeText(0, 743, '____________________________________',{align:'center'});			

		doc.setFont("times");
		doc.setFontSize(12);
		if( nmMedico.length > 30  ) {
			var nmTemp = nmMedico.split(" ");
			nmMedico = nmTemp[0] + " " + nmTemp[nmTemp.length - 1];		
			doc.writeText(0, 772, nmMedico,{align:'center'});
			doc.writeText(0, 792, 'CRM '+crm,{align:'center'});
		} else {
			doc.writeText(0, 772, nmMedico,{align:'center'});
			doc.writeText(0, 792, 'CRM '+ crm,{align:'center'});
		}
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
	};

	var gerarTable = function() {

		var dataRows = [];
		for( var i=0; i< $scope.servicosSelecionado.selecionados.length; i++ ) {
		
			var data = [];
			data[0] =  $scope.servicosSelecionado.selecionados[i].quantidade;
			data[1] = $scope.servicosSelecionado.selecionados[i].nmServico;
			dataRows.push( data );
		}

		var dataTable = {};
		var columns = ['Quantidade','Serviço'];	
		var rows = dataRows;

		dataTable.columns = columns;
		dataTable.rows = rows;

		return dataTable;
	};	
// END PRINT REPORT LANCAMENTO EXAMES

	var inicarTipoEspecialidades = function() {

		servicesFactory.getServicosEspecialidadeToExame
		.get( {idEspecialidade: $stateParams.idEspecialidade}, 
	   		function( result ) {
	   			
	   			$scope.servicosEspecialidade = {};
	   			$scope.servicosEspecialidade = result;
	   			$scope.tiposEspecialidade = [];

	   			for( var i = 0; i< $scope.servicosEspecialidade.length; i++ ) {

	   				var servicoEspecialidade = $scope.servicosEspecialidade[i];

	   				if( servicoEspecialidade.tipo == 'GINECOLOGIA/OBSTETRÍCIA' )
	   					 servicoEspecialidade.tipo  = 'GINECOLOGIA';
	   				else if( servicoEspecialidade.tipo == 'OTORRINOLARINGOLOGIA')
	   					servicoEspecialidade.tipo  = 'OTORRINO';
	   				else if( servicoEspecialidade.tipo == 'CLÍNICA GERAL')
	   					servicoEspecialidade.tipo  = 'CLINICA GERAL';
	   				else if( servicoEspecialidade.tipo == 'RADIOLÓGICO')
	   					servicoEspecialidade.tipo  = 'RADIOLOGICO';
   			
	   				var indice = $scope.tiposEspecialidade.indexOf( servicoEspecialidade.tipo );

	   				if(indice < 0)
		   				$scope.tiposEspecialidade.push(  servicoEspecialidade.tipo );
	   			}

	   			getOrcamento( false );
          });
	};
	

	$scope.getServicosEspecialidade = function( especialidadeSelecionada ) {
		carregarServicosByEspecialidade( especialidadeSelecionada );
		$scope.canLancarServicos = true;
	};


	var carregarServicosByEspecialidade = function( especialidadeSelecionada  ) {
		
		$scope.servicosEspecialidadeRanqueados = []; //lista ranqueados
		$scope.servicosEspecialidadeAvulsos = []; //lista combo pesquisa
		var numeroServicosExibirListaRank = 18;
	
		for( var i = 0; i < $scope.servicosEspecialidade.length; i++) {

			var servicoEspecialidade = $scope.servicosEspecialidade[i];

			if( servicoEspecialidade.tipo == 'GINECOLOGIA/OBSTETRÍCIA' )
	   			servicoEspecialidade.tipo  = 'GINECOLOGIA';
			else if( servicoEspecialidade.tipo == 'OTORRINOLARINGOLOGIA')
				servicoEspecialidade.tipo  = 'OTORRINO';
			else if( servicoEspecialidade.tipo == 'CLÍNICA GERAL')
	   					servicoEspecialidade.tipo  = 'CLINICA GERAL';
	   		else if( servicoEspecialidade.tipo == 'RADIOLÓGICO')
	   					servicoEspecialidade.tipo  = 'RADIOLOGICO';

	   		if( $scope.servicosEspecialidadeRanqueados.length < numeroServicosExibirListaRank 
	   			&& especialidadeSelecionada == servicoEspecialidade.tipo ) {

	   			excluirServicosPendentes( servicoEspecialidade  );
	   			$scope.servicosEspecialidadeRanqueados.push( servicoEspecialidade );	

	   		} else if( especialidadeSelecionada == servicoEspecialidade.tipo ) {
	   			$scope.servicosEspecialidadeAvulsos.push(servicoEspecialidade);
	   		}
		}

		$scope.especialidadeSelecionada = especialidadeSelecionada;
		atualizarListaAvulsoSelecionados();

	};


	var atualizarListaAvulsoSelecionados = function() {

		var avulsosList = $scope.servicosEspecialidadeAvulsos; 
		var selecionadosList =  $scope.servicosSelecionado.selecionados;
		var indicesParaRemover = [];

		for( var i=0;i<avulsosList.length;i++){
			for( var j=0;j<selecionadosList.length;j++){
				if( avulsosList[i].idServico == selecionadosList[j].idServico ) {
					indicesParaRemover.push(i);	
					break;
				}
			}
		}

		for( var i=indicesParaRemover.length -1 ;i >= 0;i--){
			$scope.servicosEspecialidadeAvulsos.splice(indicesParaRemover[i],1);
		}
	};


	$scope.adicionarServicoEspecialidadeAvulso = function(item,model) {

		var tempList = $scope.servicosEspecialidadeAvulsos; 
		var indice = -1;
	
		for( var i=0;i<tempList.length;i++){
			if( tempList[i].idServico == item.idServico ) {
				indice = i;	
				break;
			}
		}

		if( indice >= 0 ) {

			$scope.servicosEspecialidadeAvulsos.splice(indice,1);
			$scope.servicoSelecionado.idServico = undefined;
			item.quantidade = 1;
			item.tipoEspecialidade = $scope.especialidadeSelecionada;
			$scope.servicosSelecionado.selecionados.push(item);
		}		 
		
	};

	$scope.removerServicoEspecialidade = function( item ) {
		var tempList = $scope.servicosSelecionado.selecionados; 
		var indice = -1;

		for( var i=0;i<tempList.length;i++){
			if( tempList[i].idServico == item.idServico ) {
				indice = i;	
				break;
			}
		}

		if ( item.checked ) {

			var totalServicosDiferentes = 0;

			for( var i=0;i<$scope.servicosEspecialidadeRanqueados.length;i++){
				if( $scope.servicosEspecialidadeRanqueados[i].idServico == item.idServico ) {
					$scope.servicosEspecialidadeRanqueados[i].backGroundColor = "#E6E6E6";	

				} else {

					totalServicosDiferentes++;
					if( totalServicosDiferentes == $scope.servicosEspecialidadeRanqueados.length )
						$scope.servicosPendentesExcluir.push(item);
				}
			}

			if( indice >= 0 )
				$scope.servicosSelecionado.selecionados.splice(indice,1);

		} else {

			if( item.idOrcamentoExame && item.idServicoOrcamentoExame) {
				$scope.servicosEspecialidade.push(item);	
				$scope.servicosSelecionado.selecionados.splice(indice,1);

			} else {

			  if( indice >= 0 ) {
				$scope.servicosSelecionado.selecionados.splice(indice,1);
				$scope.servicoSelecionado.idServico = undefined;
				$scope.servicosEspecialidadeAvulsos.push(item);
				}
			}					
		}		
	};

	var excluirServicosPendentes = function( servicoEspecialidade ) {
		
		var indicesListExcluir = [];

		if( $scope.servicosPendentesExcluir
			&& $scope.servicosPendentesExcluir.length > 0) {

			for( var i=0; i<$scope.servicosPendentesExcluir.length; i++  ) {

				var servPendente = $scope.servicosPendentesExcluir[i];

				if(  servPendente.idServico == servicoEspecialidade.idServico ) {
					servicoEspecialidade.backGroundColor = "#E6E6E6";
	   				servicoEspecialidade.checked = false;
	   				indicesListExcluir.push(i);
				}
			}

		}

		//fix - retirar servico do vetor excluidos
		for(var position = 0; position < indicesListExcluir.length; position++ ) {
			$scope.servicosPendentesExcluir.splice(indicesListExcluir[position],1);
		} 

	};


	$scope.checkService = function( servico ) {
	
		if( servico.checked) {
			servico.checked = false;
			servico.backGroundColor = "#E6E6E6";
			servico.tipoEspecialidade = null;
		} else {
			servico.checked = true;
			servico.backGroundColor = "#4ECDC4";
			servico.quantidade = 1;
			servico.tipoEspecialidade = $scope.especialidadeSelecionada;
		}

	};

	

	$scope.finalizarOrcamento = function( servicosSelecionados  ) {
		var orcamentoToFinalizar = formatarOrcamento( servicosSelecionados );
		$scope.buttonFinalizarDisabled = true;

		if( !$scope.existeOrcamento ) {
			salvarOrcamento( orcamentoToFinalizar );
		} else {
			updateOrcamento( orcamentoToFinalizar );
		}
	};


	var salvarOrcamento = function( orcamento ) {

		var idAgendamento = $stateParams.idAgendamento;

		servicesFactory.saveOrcamentoExame
		 .save( {idAgendamento: idAgendamento}, orcamento , function( result ) {
	   			if(result) {		
	   				$rootScope.alerts.push({
	                type: "success", 
	                msg: " Solicitação de exames salva com sucesso!",
	                timeout: 5000 });
	                Metronic.scrollTop();   

	                $scope.buttonFinalizarDisabled = false;
	                getOrcamento(true);  
	   			}
         }); 
	};


	var updateOrcamento = function( orcamento ) {
		servicesFactory.updateOrcamentoExame
		 .update( orcamento , function( result ) {
		 	if(result) {
	 			$rootScope.alerts.push({
                	type: "info", 
                	msg: " Solicitação de exames atualizada com sucesso!",
                	timeout: 5000 });
                	Metronic.scrollTop();   

                	$scope.buttonFinalizarDisabled = false;
                	getOrcamento(true);     
		 	}
		 });

	};


	var getOrcamento = function( boCallPrint ) {

		var idAgendamento = $stateParams.idAgendamento;
		var idEspecialidade = $stateParams.idEspecialidade;

		servicesFactory.getOrcamento
		 .get( {idAgendamento: idAgendamento, idEspecialidade: idEspecialidade} , function( result ) {
	   			if( result.length > 0 ) {	
	   				$scope.existeOrcamento = true;	
	   				$scope.canLancarServicos = false;
	   				$scope.servicosSelecionado = { selecionados:result };
	   				$scope.idOrcamentoExameToPrintPdf = $scope.servicosSelecionado
	   														.selecionados[0].idOrcamentoExame;

	   				if( boCallPrint ){
	   					//$scope.printOrcamentoPDF();
	   					gerarPdfExames();
	   				}
	   					


	   				atualizarServicosEspecialidadetoEdition( $scope.servicosSelecionado.selecionados ); //to see
	   			} 
         }); 
	};


	var formatarOrcamento = function( servicosSelecionados ) {

		var orcamentoExameDTO = [];

		for( var i=0; i<servicosSelecionados.length;i++) {
			
			var servico = servicosSelecionados[i];
			var orcamentoExame = {};

			if( $scope.existeOrcamento ) 
				orcamentoExame.idOrcamentoExame = $scope.idOrcamentoExameToPrintPdf;

			orcamentoExame.idServico = servico.idServico;
			orcamentoExame.quantidade = servico.quantidade;	

			orcamentoExameDTO.push( orcamentoExame );
		}

		return orcamentoExameDTO;

	};


	var atualizarServicosEspecialidadetoEdition = function( sevicosJaSelecionados ) {
		
		var indicesParaRemover = [];
		for( var i = 0; i < sevicosJaSelecionados.length; i++) {
			for( var j = 0; j< $scope.servicosEspecialidade.length; j++ ) {
				if( sevicosJaSelecionados[i].idServico == $scope.servicosEspecialidade[j].idServico  ) {
					indicesParaRemover.push(j);
					break;
				}
			}				
		}

		for( var i=indicesParaRemover.length -1 ;i >= 0;i--){
			$scope.servicosEspecialidade.splice(indicesParaRemover[i],1);
		}

	};

	var getProfissional = function() {

		var login =  Auth.authz.idTokenParsed.preferred_username;
	
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {

	   			if( result ) {
	   				$scope.profissional = result;
	   			}
          });
	};


	var inicio = function() {
		
		inicarTipoEspecialidades();
		getProfissional();
		$scope.servicosSelecionado = { selecionados:[]};		
		$scope.canLancarServicos = false;
		$scope.existeOrcamento = false;	
		$scope.servicoSelecionado = {};
		$scope.servicosPendentesExcluir = [];
	};
	

	inicio();

});


