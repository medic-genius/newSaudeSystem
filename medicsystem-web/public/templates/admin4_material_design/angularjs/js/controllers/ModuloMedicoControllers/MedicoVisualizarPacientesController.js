/* 
@author : Joelton
@since : 10-05-2016
@last-modification: 30-06-2016
@version :  1.2 
*/

'use strict';

MedicsystemApp.controller('MedicoVerificarPacientesController', function($rootScope,$scope, $http, $timeout,$state,$stateParams,$modal,NgTableParams,$filter, servicesFactory, Auth) {

	function configurarTable( lista ) {

		var initialParams = { count:10 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};

	$scope.chamarPacienteConsultorio = function( paciente ,listaPaciente) {

		if ( paciente.clicked  ) {

			paciente.clicked = false;	
			paciente.callPacientSuccess = "";
			paciente.actionChamarPaciente = "";
			paciente.nameStyle = {color: '#0099CC'};

			for( var i=0; i < listaPaciente.length; i++ ) {
				listaPaciente[i].disableCallPaciente = false;
			}

		} else {

					paciente.clicked = true;	
						paciente.actionChamarPaciente = "faa-ring animated";
						paciente.callPacientSuccess = "bg-blue bg-font-blue";
						paciente.nameStyle = {color: '#FFFFFF'};

						for( var i=0; i < listaPaciente.length; i++ ) {
							if( paciente.idAgendamento != listaPaciente[i].idAgendamento  ){
								listaPaciente[i].disableCallPaciente = true;
							}
						}
			servicesFactory.drConsultaPaciente.chamar( {"id_consultorio": paciente.idConsultorio, //to-do unidade
			 	"id_medico": paciente.idmedico , "id_especialidade": paciente.idEspecialidade,
			 	"nome_paciente": paciente.nmPaciente.trim(),"nome_medico": paciente.nmProfissional.trim(), 
			 	"nome_consultorio": paciente.nmConsultorio,
				"especialidade": paciente.nmEspecialidade.trim(), 
				"tratamento": "doutor", "boPrioridade" : paciente.boPrioridade,
				"id_unidade": paciente.idUnidade}, 
	   		
	   			function( data ) {
	   				if(data && data.status == 200) {	   					
	   					var registro = {
	   						idUnidade : paciente.idUnidade,
	   						idAgendamento : paciente.idAgendamento
	   					};
	   			
		   				servicesFactory.registraChamada.registrar(registro, {},function(dados){
	   					
		   				});
		   			
	   					
	   				}
        		}, function(error) {
        			$rootScope.alerts.push({
                	type: "danger", 
                	msg: "Não podemos chamar o paciente! Contate a T.I",
                	timeout: 5000 });
              		Metronic.scrollTop();	
        		}
        	);
		}
	}

	$scope.ausentarPaciente = function( idAgendamento ) {
		console.log("idAgendamento",idAgendamento);
		servicesFactory.profissionalAusentarPaciente.ausenta( {idAgendamento: idAgendamento, idProfissional:$scope.profissional.id}, 
	   		function( result ) {
	   			if(result) {
	   				$scope.atualizarAgendamentos();
	   				$rootScope.alerts.push({
                	type: "info", 
                	msg: "Paciente ausentado com sucesso !",
                	timeout: 10000 });
              		Metronic.scrollTop();            
	   			}
          });
	};

	var paginarTable = function( agendamentosAptos ) {	
		if( agendamentosAptos.length > 0 ) {
			$scope.canSeeAgendamentos = true;
			configurarTable(agendamentosAptos);
		} else {
			$scope.canSeeAgendamentos = false;
		}
	};

	$scope.carregaAgendamentoApto = function( item ) {
		
		inicializarTotais();
		var idProfissional = $scope.profissional.id;
		$scope.especialidadeEscolhida =  item;

		servicesFactory.agendamentosProfissional
		.get( { idProfissional: idProfissional, 
				idEspecialidade: $scope.especialidadeEscolhida.idEspecialidade },
				function( result ) {
				console.log("result", result);
				if( result ) {
					
					$scope.agendamentosAptos = [];
					for( var i = 0; i < result.length; i++) {
					
						var agendamento = result[i];
					
						if( agendamento.inStatus == 4  ) {
							$scope.agendamentosAptos.push( agendamento );
						} else if( agendamento.inStatus == 2 ) {
							$scope.totalAtendidos ++;
						} else if( agendamento.inStatus == 3) {
							$scope.totalFaltosos ++;
						}
					}

					$scope.totalPresentes = $scope.agendamentosAptos.length;
					console.log("totalPresentes", $scope.agendamentosAptos)
					paginarTable( $scope.agendamentosAptos );
				}
			});
	};

	$scope.atualizarAgendamentos = function() {
		inicializarTotais();
		$scope.carregaAgendamentoApto( $scope.especialidadeEscolhida );
	};

	var recuperarAgendamentosPosFinalizarAtendimento = function( idEspecialidade ) {
		var item = {idEspecialidade:idEspecialidade};
		$scope.carregaAgendamentoApto( item );
	};

	var getEspecialidadesProfissional = function( idProfissional ) {
		servicesFactory.especialidadesProfissional
		.get( {idProfissional: idProfissional}, 
	   		function( result ) {
	   			if( result && result.length == 1) {
	   				$scope.idEspecialidadeSelected = result[0].idEspecialidade; 
	   				if( !$stateParams.idEspecialidade )
	   					$scope.carregaAgendamentoApto( result[0] );
	   			}

	   			$scope.especialidades = result;
          });
	};

	var inicializarTotais = function() {
		$scope.totalAtendidos = 0;
		$scope.totalFaltosos = 0;
		$scope.totalPresentes = 0;
	};

	var getProfissional = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		servicesFactory.recuperarProfissional.get( {login: login}, 
	   		function( result ) {
	   			if( result ) {
	   				$scope.profissional = {};
	   				$scope.profissional.id = result.id;
	   				$scope.profissional.nmFuncionario =  result.nmFuncionario;
	   				getEspecialidadesProfissional( $scope.profissional.id );

	   				if( $stateParams.idEspecialidade )
	   					recuperarAgendamentosPosFinalizarAtendimento(  $stateParams.idEspecialidade );
	   			}		
          });
	};

	var inicio = function() {
		getProfissional();
		$scope.idEspecialidadeSelected = null;
		$scope.canSeeAgendamentos = false;
		inicializarTotais();
	};

	if ( $state.is('medico-visualizarPacientes') ) {
		if( !$stateParams.idEspecialidade ) {
			inicio();
		} else {
			getProfissional();
		}
	};	
});


