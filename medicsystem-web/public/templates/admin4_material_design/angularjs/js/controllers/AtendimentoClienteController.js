'use strict';

MedicsystemApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

MedicsystemApp.controller('AtendimentoClienteController', function($rootScope, $timeout,$scope, $state, $stateParams, $modal, servicesFactory,Auth,toastr, NgTableParams, $location) {
    $scope.enableLaudo = ['administrador', 'gerenteunidade', 'gerenteatendimentocliente'];
    $scope.valideBotaoLaudo = !$scope.checkEditRole($scope.enableLaudo);

    $scope.$on('$viewContentLoaded', function() {
        if(!!$rootScope.selectedUserData) {
            var usr = $rootScope.selectedUserData;
            $scope.searchUserAndShowData(usr.idCliente, usr.isDependente, usr.idDependente);
            $rootScope.selectedUserData = undefined;
        }
    });

    $scope.perfisMenuDespesa = ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'];

    $scope.perfisMenuSimplificado = ['gerentetelemarketing','telemarketing'];

    var Cliente = servicesFactory.clientes;
    var ClienteView = servicesFactory.clienteView;
    var Informativo = servicesFactory.informativo;
    var InformativoInfo = servicesFactory.informativoinfo;

    $scope.showDetails = true;
    $scope.showClienteOptions = false;

    $rootScope.digitalClienteMsg = "Ler Biometria";

    $scope.showDetails = true;
    $scope.showClienteOptions = false;
    $scope.selectedCliente = new Cliente({});
    $scope.cliente = {};
    $scope.searchFiltro = "";
    $scope.situacoes = [];
    $scope.qtdcheck = 0;
    $scope.botaoEnviar = false;

    function configurarTable( lista ) {

        var initialParams = { count: 5 };

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[],
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 15,
            paginationMinBlocks: 2,
            dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )
    };

    $scope.buscarDadosComissao = function (){
        function getDates(startDate, stopDate) {
            var dateArray = [];
            var currentDate = moment(startDate);
            var stopDate = moment(stopDate);
            while (currentDate <= stopDate) {
                dateArray.push( moment(currentDate) )
                currentDate = moment(currentDate).add(1, 'days');
            }
            return dateArray;
        }
        var arrayDates = getDates(moment().startOf('week').add(1, 'days'),moment().endOf('week'));

        var dtInicio = moment(arrayDates[0]._d).format("YYYY/MM/DD");
        var dtFim = moment(arrayDates[5]._d).format("YYYY/MM/DD");

		 servicesFactory.contratoFuncionario.get({
			 dtInicio: dtInicio,
			 dtFim: dtFim
		}, function( result ) {
			if (result.length > 0){
            $scope.contratos = result;
            result.reverse();
            //CONFIGURACAO PARA TABLE
            configurarTable(result);
			$scope.showTable = true;

            //CONFIGURACAO PARA GRAFICO
            var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
            "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

            var listDias = [];
            for ( var i = 0; i < arrayDates.length; i++){
                listDias.push(arrayDates[i]._d.getDate()+"/"+monthNames[arrayDates[i]._d.getMonth()]);
            }

            $scope.somaComissao = 0;
            var dadosDTO = {};
            $scope.somaSemanal = 0;
            for (var i =0; i < $scope.contratos.length; i++){
              //SET DATE
              var d = new Date($scope.contratos[i].dtcontrato);
              var mes = monthNames[d.getMonth()];
              var dia = d.getDate()+1;

              $scope.somaSemanal = $scope.somaSemanal + $scope.contratos[i].vlproducao;
              dadosDTO[i] = { somaComissao: $scope.somaComissao + $scope.contratos[i].vlproducao,
                data: dia+"/"+mes
              }
            }
            Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
            };

            // Get the size of an object
            var sizeObj = Object.size(dadosDTO);

            var date = "";
            var graph = [];

            for ( var i = 0; i < arrayDates.length; i++) {
                graph.push({somaComissao: 0});
                date = arrayDates[i]._d.getDate()+"/"+monthNames[arrayDates[i]._d.getMonth()];
                for (var j = 0; j < sizeObj; j++){

                    if ( date == dadosDTO[j].data){
                        graph[i].somaComissao = graph[i].somaComissao + dadosDTO[j].somaComissao;
                        graph[i].data = date
                    }else {
                        graph[i].data = date
                    }

                }
            }
            var listC3 = [];
            for (var i = 0; i < graph.length; i++){
                listC3.push(graph[i]);

            }
            }else{
            $scope.showTable = false;
            }
		$scope.startGraph(listC3);
        });
	}

    $scope.buscarDadosComissao();
    $scope.startGraph = function (lista){
        if (lista != undefined)
        C3Utils.drawComissaoSemanal("#comissaoSemanal", lista);
    }

    $scope.getInformativo = function(){
        $scope.informativoOptions = [];

        Informativo.get({}, function(dados){
            $scope.informativoOptions = dados;
        });

    }


    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} );
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() );
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() );
        }
    };

    $scope.enviar = function(){

        $scope.informativoSelect = [];
        for (var i = 0; i < $scope.informativoOptions.length; i++){

            if ($scope.informativoOptions[i].value1 == true){

            $scope.informativoSelect.push($scope.informativoOptions[i]);
            console.log(" $scope.informativoSelect:  ", $scope.informativoSelect);
            }
        }
        var listaInformativo = [];
        for (var i = 0; i < $scope.informativoSelect.length; i++){
            var InformativoDTO = {nmInformativo : $scope.informativoSelect[i].nmInformativo,
                        nrCelular : $scope.selectedCliente.nrCelular,
                        nrTelefone : $scope.selectedCliente.nrTelefone};

           listaInformativo.push(InformativoDTO);
        }

        //console.log(" $scope.listaInformativos: ",listaInformativo);
        InformativoInfo.save({},listaInformativo, function (data){
            if (data.response[0].returnCode == "001"){
            messageToastr("erro","Não foi possivel enviar a mensagem, verifique o número e celular","ERRO NA MENSAGEM")
            }
            else if (data.response[0].returnCode != "200"){
            messageToastr("erro","Não foi possivel enviar a mensagem, entre em contato com o TI","ERRO NA MENSAGEM")
            }
            else{
            $scope.botaoEnviar = false;
            sleep(5000);
            console.log("data:  ",data);
            messageToastr("sucesso","Mensagem enviada, aguarde até 2 minutos para confirmar","MENSAGEM ENVIADA");
            $scope.myPopover.close();

           }
        }, function(erro){
            messageToastr("erro","Não foi possivel enviar a mensagem, verifique o número e celular","ERRO NA MENSAGEM")
        });
         for (var i = 0; i < $scope.informativoOptions.length; i++){
           if ($scope.informativoOptions[i].value1 == true){
                $scope.informativoOptions[i].value1 = false;
            }
              $scope.botaoEnviar = false;
             $scope.qtdcheck = 0;
        }
    }

        // LOAD POPOVER

              $scope.myPopover = {

                isOpen: false,

                templateUrl: 'myPopoverTemplateInformativo',

                open: function open() {
                  $scope.myPopover.isOpen = true;
                 for (var i = 0; i < $scope.informativoOptions.length; i++){
                     if ($scope.informativoOptions[i].value1 == true){
                        $scope.informativoOptions[i].value1 = false;
                    }
                }
                },

                close: function close() {
                    $timeout(function() {
                        angular.element('#buttonPopover').trigger('click');
                    }, 0);
                  for (var i = 0; i < $scope.informativoOptions.length; i++){
                         if ($scope.informativoOptions[i].value1 == true){
                            $scope.informativoOptions[i].value1 = false;
                        }
                    }
                    $scope.botaoEnviar = false;
                    $scope.qtdcheck = 0;
                }
              };


    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }}

    $scope.check = function(valor){
    //$scope.qtdcheck = 0;

        if (valor){

            $scope.qtdcheck++;
        }
        else
            $scope.qtdcheck--;
    }


    $scope.checkEditRole = function(allowed) {

		var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;

    	for ( role of userRoles ) {
    		if (allowed.indexOf(role) != -1) {
    			return false;
    		}
    	}
    	return true;
	};



    $scope.atualizarSelectClientes = function(text) {
        if (text) {
            var queryObject = {};
            var filtro = $scope.searchFiltro;
            text = text.toLowerCase();
            if (filtro && filtro.startsWith("!")) {
                if (filtro.startsWith("!cod")){
                    queryObject = { nrCodCliente: text.trim() };
                } else if (filtro.startsWith("!rg")){
                    queryObject = { nrRG: text.trim() };
                } else if (filtro.startsWith("!cpf")){
                    queryObject = { nrCPF: text.trim() };
                } else {
                    text = null;
                }

            } else {
                queryObject = { nmCliente: text };
            }

            if(text){
                var data = Cliente.query(queryObject, function() {
                    $scope.clientes = data;
                    if (data.length == 0) {
                        $scope.selectedClienteNotFound = "Sem dados disponiveis.";

                    } else {
                        $scope.selectedClienteNotFound = "";

                    }
                });
            }
        }

    };

    $scope.novoCliente = function() {
        $scope.showTable = false;
        $scope.cliente = {}
        $scope.selectClienteFromSearch = {}
        $scope.selectedCliente = new Cliente({});
        $scope.showDetails = false;
        $scope.showClienteOptions = true;
        $state.go('atendimentoCliente.cliente-conta-novo');
    }

     $scope.searchUser = function(item) {
        $scope.showTable = false;
        $scope.selectClienteFromSearch = item;
        Cliente.get( {id: item.id}, function(dataCliente){
            $scope.selectedCliente = dataCliente;
            servicesFactory.fotoCliente.get( {id: item.id},function(fotoCliente){
                 $scope.selectedCliente.fotoClienteDecoded = fotoCliente.fotoDecoded;
            });
            $scope.showDetails = false;
            $scope.showClienteOptions = false;

            if(item.nrCPF){
                servicesFactory.clienteNewProspect.query({cpf: item.nrCPF}, function(dados){
                    $rootScope.newProspect = dados;
                    $rootScope.isFp = $scope.selectedCliente.boNaoAssociado && $rootScope.newProspect.nrCpf ? true : false;
                });
            }

            $scope.situacoes = servicesFactory.clienteSituacao.query({ id: item.id });
            $state.go('atendimentoCliente');
            $scope.getInformativo();
            $scope.autorizadosLimiteDiario = [];
        });
    };

    $scope.$on('cadastroCliente.finished', function(event, data) {
        $scope.searchUser(data);
    });


    $scope.searchUserAndShowData = function(userId, isDependente, idDependente) {
        Cliente.get( {id: userId}, function(dataCliente){
            $scope.selectedCliente = dataCliente;
            servicesFactory.fotoCliente.get( {id: userId},function(fotoCliente){
                $scope.selectedCliente.fotoClienteDecoded = fotoCliente.fotoDecoded;
            });
            $scope.showDetails = false;
            $scope.showClienteOptions = false;
            $scope.situacoes = servicesFactory.clienteSituacao.query({ id: userId });
            if(!isDependente) {
                $state.go('atendimentoCliente.cliente-conta');
            } else {
                $state.go('atendimentoCliente.cliente-dependentes-editar', {id: idDependente});
            }
            $scope.getInformativo();
        });
    }

    $scope.saveAgendamentoCadastro = function() {
        $state.go('atendimentoCliente.cliente-agendamento');
    }

    $scope.saveDependenteCadastro = function() {
        $state.go('atendimentoCliente.cliente-dependentes');
    }

    $scope.saveContratoCadastro = function() {
        $state.go('atendimentoCliente.cliente-contratos');
    }

    $scope.lerBiometria = function() {
        $scope.showTable = false;
        var socket = io.connect('http://localhost:9092');
        $rootScope.digitalClienteMsg = "Aguardando digital...";
        $rootScope.digitalCliente = undefined;

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });
        socket.emit('biometriaevent', {});

        socket.on('biometriaevent', function(data) {

            if(!isNaN(data)){

                $rootScope.digitalCliente = data;

               $scope.selectClienteFromSearch = ClienteView.get({ id: $rootScope.digitalCliente  }, function(){
                    Cliente.get( {id: $rootScope.digitalCliente }, function(data){
                       $scope.selectedCliente = data;
                        $scope.showDetails = false;
                        $scope.showClienteOptions = false;
                        servicesFactory.fotoCliente.get({id: $scope.selectedCliente.id },function(fotoCliente){
                            $scope.selectedCliente.fotoClienteDecoded = fotoCliente.fotoDecoded;
                        });
                        $scope.situacoes = servicesFactory.clienteSituacao.query({ id: $rootScope.digitalCliente  });
                        $rootScope.digitalClienteMsg = "Biometria aceita - Ler Novamente";
                        $scope.cliente = {};
                        $state.go('atendimentoCliente');
                        $scope.getInformativo();
                    });
                });
            } else {

                $rootScope.digitalClienteMsg = "Digital invalida - Ler Novamente";
                $scope.selectedCliente = {};
                $scope.showDetails = true;
                $scope.showClienteOptions = true;
                $state.go('atendimentoCliente');
            }
        });
    }

    $scope.abrirCameraFoto = function() {

        var itemsCliente = {
                            Cliente: servicesFactory.clientes,
                            selectedCliente: $scope.selectedCliente,
                            clienteSituacao: servicesFactory.clienteSituacao,
                            selectClienteFromSearch: $scope.selectClienteFromSearch,
                            searchUser: $scope.searchUser
                            };

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'abrirFotoCliente.html',
            controller: 'ModalInstanceCadFotoClienteCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                    items: function () {
                        return itemsCliente;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    }

    $scope.$on('updateUserBroadcast', function (event, value) {
        console.log("event",event);
        console.log("value",value   );
        if (value == 'true') {
            console.log("vai chamar",value   );
            $scope.searchUser( { id: $scope.selectedCliente.id } );
        }
    });

    $scope.goToTornarAssociado = function() {
        $scope.canShowFluxoConverterAssociado = true;
        $rootScope.clienteSelecionadoConversao = $scope.selectedCliente;
        $state.go('atendimentoCliente.cliente-conta-novo');
    }

    if($location.search().id_cliente)
    {
        $scope.searchUser({id: $location.search().id_cliente});
    } else
    {
        // $scope.searchUser({id: 237658}); // hack to find cliente ($scope.selectedCliente)
    }
});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalInstanceCadFotoClienteCtrl', function ($rootScope, $scope, $state, $modalInstance, items) {

    $scope.cameraSettings = {
        videoHeight: 240,
        videoWidth: 320
    };

    $scope.makeSnapshot = function makeSnapshot() {

        var _video = $scope.cameraSettings.video;
        var patData = null;

        $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};
        $scope.patOpts.w = _video.width;
        $scope.patOpts.h = _video.height;

        if (_video) {

            var patCanvas = document.querySelector('#snapshot');
            if (!patCanvas) return;

            patCanvas.width = _video.width * 0.5;
            patCanvas.height = _video.width * 0.5;
            var ctxPat = patCanvas.getContext('2d');

            var idata = getVideoData(_video, $scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
            ctxPat.putImageData(idata, 0, 0);

            sendSnapshotToServer(patCanvas.toDataURL(), items.Cliente, items.selectedCliente, items.clienteSituacao, items.searchUser, items.selectClienteFromSearch);

            patData = idata;
        }
    };

    var getVideoData = function getVideoData(_video, x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = _video.width ;
        hiddenCanvas.height = _video.height ;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, _video.width * 0.5, _video.height * 0.5);
        return ctx.getImageData(x, y, w * 0.5, h * 0.5);
    };

    /**
     * This function could be used to send the image data
     * to a backend server that expects base64 encoded images.
     *
     * In this example, we simply store it in the scope for display.
    */
    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64, Cliente, selectedCliente, clienteSituacao, searchUser, selectClienteFromSearch) {

        Cliente.get({id: selectedCliente.id} ,function(dados){

            selectedCliente = dados;
            $scope.snapshotData = escape(imgBase64.split(',')[1]);
            selectedCliente.fotoClienteEncoded = $scope.snapshotData;

            Cliente.update( { id: selectedCliente.id },  selectedCliente, function(data){
            $rootScope.addDefaultTimeoutAlert("Foto do Cliente", "atualizado", "success");
            searchUser(selectClienteFromSearch);

        }, function(error){
            $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
        });

        }, function(erro){
            console.log("Não foi possivel encontrar o cliente");
        });



        Metronic.scrollTop();

        $modalInstance.close();
    };

    $scope.exit = function () {

        $modalInstance.close();
    };

    $scope.cancel = function () {

        $modalInstance.dismiss('cancel');
    };
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
