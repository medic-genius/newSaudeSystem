'use strict';

MedicsystemApp.controller('ClienteDespesaController',
function($rootScope, $scope,
	$http, $timeout, $state, $sce, $stateParams, $location, servicesFactory,
	$modal, $window, NgTableParams, $filter, $ngBootbox)
{
	var Despesa = servicesFactory.despesas;
	var Cliente = servicesFactory.clientes;
	var Servico = servicesFactory.servicos;
	var Contrato = servicesFactory.contratos;
	var Parcela = servicesFactory.parcelas;

	$scope.deleteRoles = ['administrador', 'gerenteadm', 'gerenteunidade', 'gerenteatendimentocliente', 'gerentetelemarketing'];
	$scope.quitarRoles = ['administrador', 'gerenteadm', 'gerenteunidade'];
	$scope.desquitarRoles = ['administrador'];

	$scope.showMotivoQuitacao = false;

	function configurarTable( lista ) {

		var initialParams = { count:5 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[],
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )
  	};

	function updateDespesaValues() {
		$scope.despesa.vlDespesaTotal = 0.0;
		$scope.despesa.vlDespesaCoberta = 0.0;
		$scope.despesa.vlDespesaAberto = 0.0;
		var pendingEnc = false;
		$scope.despesaServicosSelecionados.forEach(function (value, key) {
			if(value.qtServico) {
			    $scope.despesa.vlDespesaTotal += value.vlServico * value.qtServico;
	    		$scope.despesa.vlDespesaCoberta += value.vlCoberto * value.qtServico;
	    		$scope.despesa.vlDespesaAberto += value.vlAberto * value.qtServico;
			}
    		if(!value.encaminhamento.credenciada && value.encaminhamento.profissional == null) {
    			pendingEnc = true;
    		}
		});
		if($scope.despesa.vlDespesaTotal > 0 && !pendingEnc) {
			$scope.buttonDisabled = false;
		}
	}

	function updateDespesasServicoHashMap(despesasServico) {
    	despesasServico.forEach(function(despesaServico) {
	    	if (despesasServico[0].rdTipoEncaminhamento == 0 && despesasServico[0].rdTipoEncaminhamentoInterno == 0 && despesasServico[0].profissionalDTO.profissional)
			{
	    		despesaServico.qtServico = despesaServico.qtServico == null ? 1 : despesaServico.qtServico ;
	    		despesaServico.encaminhamento.profissional = despesaServico.profissionalDTO.profissional;
	    		despesaServico.encaminhamento.credenciada = null;
	    		despesaServico.boEncaminhado = true;
	    		if (despesaServico.profissionalDTO.cobertura) {
		    		despesaServico.inCoberto = true;
		    		despesaServico.vlCoberto = despesaServico.profissionalDTO.valor;
		    		despesaServico.vlServico = despesaServico.profissionalDTO.valor;
		    		despesaServico.vlAberto = 0;
	    		} else {
	    			despesaServico.inCoberto = false;
	    			despesaServico.vlCoberto = 0;
	    			despesaServico.vlServico = despesaServico.profissionalDTO.valor;
	    			despesaServico.vlAberto = despesaServico.profissionalDTO.valor;
	    		}
	    		despesaServico.vlCusto = 0;
	    		$scope.lastDespesaServicoEncaminhado = despesaServico;
	    		$scope.despesaServicosSelecionados.set(despesaServico.servico.id,despesaServico);
		    	updateDespesaValues();
	    	}

	    	if (despesasServico[0].rdTipoEncaminhamento == 0 && despesasServico[0].rdTipoEncaminhamentoInterno == 1 && despesasServico[0].credenciadaInternaDTO.credenciada)
			{
	    		despesaServico.qtServico = despesaServico.qtServico == null ? 1 : despesaServico.qtServico ;
	    		despesaServico.encaminhamento.credenciada = despesaServico.credenciadaInternaDTO.credenciada;
	    		despesaServico.encaminhamento.profissional = null;
	    		despesaServico.boEncaminhado = true;
	    		if (despesaServico.credenciadaInternaDTO.cobertura) {
		    		despesaServico.inCoberto = true;
		    		despesaServico.vlCoberto = despesaServico.credenciadaInternaDTO.valor;
		    		despesaServico.vlServico = despesaServico.credenciadaInternaDTO.valor;
		    		despesaServico.vlAberto = 0;
	    		} else {
	    			despesaServico.inCoberto = false;
	    			despesaServico.vlCoberto = 0;
	    			despesaServico.vlServico = despesaServico.credenciadaInternaDTO.valor;
	    			despesaServico.vlAberto = despesaServico.credenciadaInternaDTO.valor;
	    		}
	    		despesaServico.vlCusto = despesaServico.credenciadaInternaDTO.custo;
	    		$scope.lastDespesaServicoEncaminhado = despesaServico;

	    		$scope.despesaServicosSelecionados.set(despesaServico.servico.id,despesaServico);
		    	updateDespesaValues();
	    	}

	    	if (despesasServico[0].rdTipoEncaminhamento == 1 && despesasServico[0].credenciadaExternaDTO.credenciada)
			{
	    		despesaServico.qtServico = despesaServico.qtServico == null ? 1 : despesaServico.qtServico ;
	    		despesaServico.encaminhamento.credenciada = despesaServico.credenciadaExternaDTO.credenciada;
	    		despesaServico.encaminhamento.profissional = null;
	    		despesaServico.boEncaminhado = true;
	    		if (despesaServico.credenciadaExternaDTO.cobertura)
				{
					if(despesaServico.credenciadaExternaDTO.idPlano == 38 || despesaServico.credenciadaExternaDTO.idPlano == 95)
					{
						despesaServico.inCoberto = true;
			    		despesaServico.vlCoberto = despesaServico.credenciadaExternaDTO.valor;
			    		despesaServico.vlServico = despesaServico.credenciadaExternaDTO.valor;
			    		despesaServico.vlAberto = 0;
					}
	    			despesaServico.inCoberto = despesaServico.credenciadaExternaDTO.cobertura;
	    			despesaServico.vlCoberto = despesaServico.credenciadaExternaDTO.valor;
	    			despesaServico.vlServico = despesaServico.credenciadaExternaDTO.valor;
	    			despesaServico.vlAberto = 0;
	    		} else
				{
					despesaServico.inCoberto = false;
					despesaServico.vlCoberto = 0;
					despesaServico.vlServico = despesaServico.credenciadaExternaDTO.valor;
					despesaServico.vlAberto = despesaServico.credenciadaExternaDTO.valor;
				}
	    		despesaServico.vlCusto = despesaServico.credenciadaExternaDTO.custo;
	    		$scope.lastDespesaServicoEncaminhado = despesaServico;
	    		$scope.despesaServicosSelecionados.set(despesaServico.servico.id, despesaServico);
		    	updateDespesaValues();
	    	}
    	});
	}

	$scope.openDeleteModal = function(item) {
		var modalInstance = $modal.open({
	    	animation: true,
	    	templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-delete-modal.html',
	    	controller: 'ModalDespesaDeleteCtrl',
	    	resolve: {
	    		despesa: function () {
		          return item;
		        },
	      }
	    });

		modalInstance.result.then(function (deleted) {
			if(deleted) {
				$rootScope.alerts.push({
					type: "info",
					msg: "Despesa removida com sucesso",
					timeout: 10000 });
				Metronic.scrollTop();
				$scope.refreshDespesas();
			}
		});
	};


	$scope.abrirTextareaQuitarParcela = function(item) {
		$scope.showMotivoQuitacao = true;
		$scope.parcelaAQuitar = item;
	};

	$scope.quitarParcela = function() {
		if(!$scope.motivoQuitacao || $scope.motivoQuitacao.length < 5) {
			$rootScope.alerts.push({
	            type: "danger",
	            msg: "Motivo de Quitação não informado ou possui menos de 5 caracteres.",
	            timeout: 10000 });
	            Metronic.scrollTop();
		} else {
			servicesFactory.parcelasQuitar.quitar( { id: $scope.parcelaAQuitar.id }, $scope.motivoQuitacao, function(data){
	    		$rootScope.addDefaultTimeoutAlert("Parcela", "quitada", "success");
	    		$state.go("atendimentoCliente.cliente-despesas");
	    	}, function(error){
	    		$rootScope.addDefaultTimeoutAlert("Parcela", "quitar", "error");
	    	});
	    	Metronic.scrollTop();
		}
	}

	$scope.desquitarParcela = function(parcela) {
		servicesFactory.parcelasDesquitar.desquitar( { id: parcela.id }, {}, function(data){
    		$rootScope.addDefaultTimeoutAlert("Parcela", "desquitada", "success");
    		$state.go("atendimentoCliente.cliente-despesas");
    	}, function(error){
    		$rootScope.addDefaultTimeoutAlert("Parcela", "desquitar", "error");
    	});
    	Metronic.scrollTop();
	};

	$scope.saveDespesa = function() {

		$scope.despesa.despesaServicos = $scope.despesaServicosSelecionados.toArray();
		if($scope.despesa.vlDespesaAberto > 0) { // redirect lancamento de despesa
			$state.go("atendimentoCliente.cliente-despesas-nova-lancamento", { despesa: $scope.despesa });

		} else {
			$scope.buttonDisabled = true;
			$scope.despesa.contratoDependente.dependente = $scope.despesa.dependente;

			$scope.despesa.$save(function (object, responseHeaders) {
	        	var location = responseHeaders('Location');
	    		if (location) {
	    			var locationArray = location.split("/");
	    			var despesaId = locationArray[locationArray.length - 1];

	    			$rootScope.alerts.push({
	    				type: "info",
	    				msg: "Despesa criada com sucesso",
	    				timeout: 10000 });
	    			Metronic.scrollTop();

	    			$state.go("atendimentoCliente.cliente-despesas");
	    		}
	        }, function(error){
	        	$scope.buttonDisabled = false;
	        });
		}
	};

	$scope.openEncaminhamentoModal = function (ds, despesa) {

		if ($scope.rdTipoPaciente == 0 && despesa.contratoCliente.contrato.id == undefined) {
			$rootScope.alerts.push({
	            type: "danger",
	            msg: "Contrato Cliente não selecionado.",
	            timeout: 10000 });
	            Metronic.scrollTop();
		} else {

			encaminhamentoModal(ds, despesa);

		}
    };

    $scope.encaminharAutomatico = function (ds, despesa) {

		if(despesa.contratoDependente.dependente !== undefined) {
			servicesFactory.getDespesaServicoAutomaticoParaCliente.get({
			  	id: ds.servico.id,
			  	idCliente: despesa.contratoCliente.cliente.id,
			  	idDependente: despesa.contratoDependente.dependente.id,
				idContrato: despesa.contratoDependente.contrato.id
			}, $scope.lastDespesaServicoEncaminhado, function(data) {
                $scope.despesaServicosSelecionados.set(data.servico.id,data);
				updateDespesaValues();
			});

		} else {
			servicesFactory.getDespesaServicoAutomaticoParaCliente.get({
			  	id: ds.servico.id,
			  	idCliente: despesa.contratoCliente.cliente.id,
				idContrato: despesa.contratoCliente.contrato.id
			}, $scope.lastDespesaServicoEncaminhado, function(data) {
                $scope.despesaServicosSelecionados.set(data.servico.id,data);
				updateDespesaValues();
			});
		}
    };

	$scope.encaminharTodos = function(despesa) {

		$scope.despesaServicosSelecionados.forEach(function (value, key) {
			if(!value.boEncaminhado) {

				if(despesa.contratoDependente.dependente !== undefined) {
					servicesFactory.getDespesaServicoAutomaticoParaCliente.get({
					  	id: value.servico.id,
					  	idCliente: despesa.contratoCliente.cliente.id,
					  	idDependente: despesa.contratoDependente.dependente.id,
						idContrato: despesa.contratoDependente.contrato.id
				  	}, $scope.lastDespesaServicoEncaminhado, function(data) {

					  $scope.despesaServicosSelecionados.set(data.servico.id,data);
					  updateDespesaValues();
				  	});

				} else {
					servicesFactory.getDespesaServicoAutomaticoParaCliente.get({
					  	id: value.servico.id,
					  	idCliente: despesa.contratoCliente.cliente.id,
						idContrato: despesa.contratoCliente.contrato.id
				  	}, $scope.lastDespesaServicoEncaminhado, function(data) {

					  $scope.despesaServicosSelecionados.set(data.servico.id,data);
					  updateDespesaValues();
				  	});
				}

			}
		});

	};

	function encaminhamentoModal(ds, despesa) {
		 var modalInstance = $modal.open({
		    	animation: true,
		    	templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/cliente-despesas-nova-modal-enc.html',
		    	controller: 'ModalEncaminhamentoCtrl',
		    	resolve: {
		    		despesasServico: function () {
			        	return ds;
			        },
		    		despesa: function () {
			          return despesa;
			        },
			        idCliente: function () {
			        	return $scope.selectedCliente.id;
			        },
			        idDependente: function () {
			        	return $scope.despesa.contratoDependente.dependente ? $scope.despesa.contratoDependente.dependente.id : null;
			        },
			        rdTipoPaciente: function() {
			        	return $scope.rdTipoPaciente;
			        }
		      }
		    });

		    modalInstance.result.then(function (despesasServico) {
		    	updateDespesasServicoHashMap(despesasServico);

		    });
	}

	$scope.removeDespesaServico = function(item) {
		$scope.despesaServicosSelecionados.delete(item.servico.id);
		updateDespesaValues();
	};

	$scope.increaseItemQtdServico = function(item) {
		item.qtServico = item.qtServico + 1;
		updateDespesaValues();
	};

	$scope.decreaseItemQtdServico = function(item) {
		if (item.qtServico > 1) {
			item.qtServico = item.qtServico - 1;
			updateDespesaValues();
		}
	};

	$scope.addServico = function(item) {
        $scope.despesaServicosSelecionados.add({
        	servico: item,
        	encaminhamento: {},
        	boEncaminhado: false,
        	inCoberto: false
        }, item.id);
        $scope.buttonDisabled = true;
		if ($scope.despesa.contratoDependente.contrato != null && $scope.despesa.contratoDependente.contrato.id != null){
			servicesFactory.getUltimoAgendamentoCobertoValido.query( {
				id: item.id,
				idPaciente: ($scope.despesa.dependente != null && $scope.despesa.dependente.id != null ? $scope.despesa.dependente.id : $scope.despesa.contratoCliente.cliente.id),
				tipoPaciente: ($scope.despesa.dependente != null && $scope.despesa.dependente.id != null ? "D"  : "C"),
				idContrato: ($scope.rdTipoPaciente != null && $scope.rdTipoPaciente == 1 && $scope.despesa.contratoDependente.contrato != null && $scope.despesa.contratoDependente.contrato.id != null ? $scope.despesa.contratoDependente.contrato.id : $scope.despesa.contratoCliente.contrato.id)
			}, function(dados){
			if(dados != null && dados.dataCoberturaFormatada != null){
				$ngBootbox.confirm('Serviço dentro do prazo de validade contratual com o valor de <b>'+ dados.validadeContratual+' dias</b>. Última utilização realizada com a despesa <b>'+dados.idDespesa+'</b> no dia <b>'+ dados.dataCoberturaFormatada +'</b>. Próxima utilização para cobertura a partir do dia <b>'+dados.dataProximoServicoFormatada+'</b>.')
						.then(function() {
							$scope.dataCobertura = dados;
					});

			}else {
				$scope.dataCobertura = null;
			}

			});
		}

    };

    $scope.addServicosOrcamento = function( servicosOrcamento ) {

    	for( var i=0; i< servicosOrcamento.length;i++ ) {
    		var servico = servicosOrcamento[i];
	    	$scope.despesaServicosSelecionados.add({
	        	servico: servico,
	        	encaminhamento: {},
	        	boEncaminhado: false,
	        	inCoberto: false,
	        	qtServico : servico.quantidade,
	        }, servico.id);
    	}

        $scope.buttonDisabled = true;
    };




    $scope.atualizarSelectServicos = function(text) {
		if((text !== null && text !== undefined && text.length > 0) && $scope.rdTipoPaciente != null && $scope.rdTipoPaciente == 0 && ($scope.despesa.contratoCliente.contrato != undefined && $scope.despesa.contratoCliente.contrato.id == null)){
			$rootScope.alerts.push({
	            type: "danger",
	            msg: "Contrato Cliente não selecionado.",
	            timeout: 10000 });
	            Metronic.scrollTop();

	 }else{

		  if (text) {
        	if (text.length > 1) {
	            var queryObject = {};
	            text = text.toLowerCase();
	            queryObject = { nmServico: text };

	            if(text){
	                var data = Servico.query(queryObject, function() {
	                    $scope.servicos = data;
	                    if (data.length == 0) {
	                        $scope.selectedServicoNotFound = "Nenhum serviço encontrado com os criterios.";
	                    } else {
	                        $scope.selectedServicoNotFound = "";
	                    }
	                });
	            }
        	}
        }
	}


    };

    $scope.$watch('rdTipoPaciente', function() {
        if ($scope.rdTipoPaciente != null) {

            if ($scope.rdTipoPaciente == 0) { // cliente paciente
            	$scope.contratoOptions = [];
            	$scope.contratoOptions = servicesFactory.clienteContratos.get({id: $scope.selectedCliente.id });
	            $scope.despesa.dependente = null;
	    		$scope.despesa.contratoDependente.contrato = null;
            }
        }
    });

	$scope.changePacienteDependente = function(obj) {
		var item = JSON.parse(JSON.stringify(obj));
    	if (item) {
			$scope.msgUsaPlano = null;
    		$scope.despesa.contratoDependente.dependente = item;
    		$scope.contratoDependenteOptions = [];
			$scope.despesa.contratoDependente.contrato = {};

			$scope.contratoDependenteOptions = servicesFactory.clienteContratosDependentes.get({id: $scope.selectedCliente.id, iddependente: item.id },
				function(){
				if( $scope.contratoDependenteOptions.length == 1 &&
                     ( $scope.contratoDependenteOptions[0].contrato.boBloqueado != null &&
                        !$scope.contratoDependenteOptions[0].contrato.boBloqueado) ) {

                    if ($scope.contratoDependenteOptions[0].boFazUsoPlano == true || $scope.contratoDependenteOptions[0].boFazUsoPlano == null)
                        $scope.msgUsaPlano = true;
                    else
                        $scope.msgUsaPlano = false;

                    $scope.despesa.contratoDependente.contrato.id = $scope.contratoDependenteOptions[0].contrato.id;
                }
				else {
                    if ($scope.contratoDependenteOptions[0] == undefined || $scope.contratoDependenteOptions[0] == null)
                        $scope.msgUsaPlano = null;
                    else
                        $scope.msgUsaPlano = true;

                    $scope.despesa.contratoDependente.contrato.id = null;
                }
				});

	      	getOrcamentos( $scope.despesa.contratoDependente.dependente.id, 'D' );

    	}
    };

	$scope.printDespesaPDFButton = "Imprimir Despesa";
	$scope.printOrcamentoPDFButton = "Imprimir Orçamento";

	$scope.printOrcamentoPDF = function() {

		if ($scope.despesaServicosSelecionados.toArray().length == 0) {
			$rootScope.alerts.push({
	            type: "warning",
	            msg: "Adicione pelo menos um serviço para imprimir o orçamento.",
	            timeout: 10000 });
	            Metronic.scrollTop();
	        return;
		}

		var pendingEnc = false;
		$scope.despesaServicosSelecionados.forEach(function (value, key) {

    		if(!value.boEncaminhado) {
    			pendingEnc = true;
    	        return;
    		}
		});

		if (pendingEnc) {
			$rootScope.alerts.push({
	            type: "warning",
	            msg: "Serviços pendentes de encaminhamento.",
	            timeout: 10000 });
	            Metronic.scrollTop();
	        return;
		}

    	$scope.printOrcamentoPDFButton = "Aguarde carregando...";



    	$scope.despesa.despesaServicos = $scope.despesaServicosSelecionados.toArray();

    	$http.post('/mediclab/despesas/orcamento/pdf', $scope.despesa)
    	  .success(function (response) {

    		  $scope.printOrcamentoPDFButton = "Imprimir Orçamento";

    		  var blob = b64toBlob(response, 'application/pdf');
    		  var fileURL = URL.createObjectURL(blob);

    	      $scope.orcamentopdf = $sce.trustAsResourceUrl(fileURL);
    	}).error(function(data, status, header, config) {
    		$scope.printOrcamentoPDFButton = "Falha ao Imprimir";
    	});
    };

    $scope.printDespesaPDF = function() {

    	$scope.printDespesaPDFButton = "Aguarde carregando...";



    	$http.get('/mediclab/despesas/'+ $scope.despesaDTO.despesa.id +'/pdf',{})
    	  .success(function (response) {

    		  $scope.printDespesaPDFButton = "Imprimir Despesa";

    		  var blob = b64toBlob(response, 'application/pdf');
    		  var fileURL = URL.createObjectURL(blob);

    	      $scope.despesapdf = $sce.trustAsResourceUrl(fileURL);
    	}).error(function(data, status, header, config) {
    		$scope.printDespesaPDFButton = "Falha ao Imprimir";
    	});
    };

    $scope.printEncaminhamentoPDFButton = "Imprimir Encaminhamento";

    $scope.printEncaminhamentoPDF = function() {

    	$scope.printEncaminhamentoPDFButton = "Aguarde carregando...";

    	var http = location.protocol;
    	var slashes = http.concat("//");
    	var hostname = slashes.concat(window.location.hostname);


    	$http.get('/mediclab/despesas/'+ $scope.despesaDTO.despesa.id +'/encaminhamento/pdf?path=' + hostname,{})
    	  .success(function (response) {

    		  $scope.printEncaminhamentoPDFButton = "Imprimir Encaminhamento";

    		  var blob = b64toBlob(response, 'application/pdf');
    		  var fileURL = URL.createObjectURL(blob);

    	      $scope.despesapdf = $sce.trustAsResourceUrl(fileURL);
    	}).error(function(data, status, header, config) {
    		$scope.printEncaminhamentoPDFButton = "Falha ao Imprimir";
    	});
    };

	$scope.refreshDespesas = function() {
    	if ($scope.selectedCliente && $scope.selectedCliente.id) {
    		$scope.despesas = servicesFactory.clienteDespesas.get({ id: $scope.selectedCliente.id }, function() {
		    	configurarTable( $scope.despesas );
		    });
    	}
    };

    function openGeneratePdfModal() {
      $scope.animationsEnabled = true;
      $("#btnRegistrar").prop("disabled",true);

      var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'gerarPdfModalContent.html',
          controller: 'ModalGeneratePdfInstanceCtrl',
          resolve: {
              idAgendamento: function () {
                  return $stateParams.idAgendamento;
              }
          }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
          }, function () {
      });
    };


    function openModalManagerAuthorization(despesa) {
    	var modalInstance = $modal.open({
	    	animation: true,
	    	templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
	    	controller: 'ModalLoginGerenteController',
	    	resolve: {
	    		exibeJustificativa: function () {
		          return false;
		        },
		        motivacaoAutorizacao: function () {
  		          	return null;
  		        },
		        perfilNecessario: function () {
		        	return null;
		        }
	    	}

	    });

	    modalInstance.result.then(function (data) {
	    	if (data.autorizado) {
	    		servicesFactory.lancamentosDespesasAutorizado.save({},
	    				$scope.despesaDTO,
	    				function(data) {

			            	$rootScope.alerts.push({
			    				type: "info",
			    				msg: "Despesa criada com sucesso",
			    				timeout: 10000 });
			    			Metronic.scrollTop();

			    			$state.go("atendimentoCliente.cliente-despesas");

		  	          },
		  	          function(error) {
		  	            $rootScope.alerts.push({
		  	              type: "danger",
		  	              msg: "Erro ao salvar Lançamento Despesa no sistema. Verifique os dados e envie novamente.",
		  	              timeout: 10000 });
		  	              Metronic.scrollTop();
		  	          });
	    	}

	    });
    }

    function openAutorizationManagerModal(despesa) { // somente agendamento

      $scope.animationsEnabled = true;

      var itemsAutorizarGerente = {
                            idAgendamento: $stateParams.idAgendamento,
                            Despesa: servicesFactory.clienteAgendamentosDepesas,
                            despesaDTO: despesa,
                            AutorizarGerenteDepesa: servicesFactory.loginGerente
                            };

      var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'autorizarGerenteModalContent.html',
          controller: 'ModalAutorizationManagerInstanceCtrl',
          backdrop: 'static',
          resolve: {
              items: function () {
                  return itemsAutorizarGerente;
              }
          }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
          }, function () {
      });
    };


    $scope.createClienteAgendamentoDespesa = function() {
        $scope.btnRegistrarDisabled = true;

        servicesFactory.clienteAgendamentosDepesas.save({ id: $stateParams.idAgendamento },
        $scope.despesaCliente,
        function(data) {

          if(data.boAutorizadoGerente == null
              || !data.boAutorizadoGerente){

            openGeneratePdfModal();
          } else {

            openAutorizationManagerModal(data);
          }
        },
        function(error) {
          $rootScope.alerts.push({
            type: "danger",
            msg: "Erro ao salvar Despesa Agendamento no banco de dados. Verifique os dados e envie novamente.",
            timeout: 10000 });
            Metronic.scrollTop();
        });
    };

    $scope.saveLancamentoDespesa = function(){
		$scope.btnRegistrarDisabled = true;
    	servicesFactory.lancamentosDespesas.save({},
	      $scope.despesaDTO,
	      function(data) {

	        if(data.boAutorizadoGerente == null
	            || !data.boAutorizadoGerente){

	        	$rootScope.alerts.push({
					type: "info",
					msg: "Despesa criada com sucesso",
					timeout: 10000 });
				Metronic.scrollTop();

				$state.go("atendimentoCliente.cliente-despesas");
	        } else {

	          openModalManagerAuthorization(data);
	        }
	      },
	      function(error) {
	        $rootScope.alerts.push({
	          type: "danger",
	          msg: "Erro ao salvar Lançamento Despesa no sistema. Verifique os dados e envie novamente.",
	          timeout: 10000 });
	          Metronic.scrollTop();
	      });

    };

    if($state.is('atendimentoCliente.cliente-despesas-agendamento-nova')){
        $scope.btnRegistrarDisabled = false;
    	if ($stateParams.idAgendamento) {

    		$scope.despesaCliente = servicesFactory.clienteAgendamentosDepesas.get({ id: $stateParams.idAgendamento }, function(data){
    		  $scope.despesaCliente = data;
    		  if($scope.despesaCliente.boQuitada != null &&  $scope.despesaCliente.boQuitada == true){
    			$state.go("atendimentoCliente.agendamento");
	    		  $rootScope.alerts.push({
		          type: "warning",
		          msg: "Despesa Coberta Gerada Automaticamente com Sucesso!",
		          timeout: 10000 });
		          Metronic.scrollTop();

    		}
    		}, function(error){
    			$state.go("atendimentoCliente.agendamento");
	    	});

    	}

		$scope.statusAdminCredit = false;
		$scope.statusAdminCreditOptions = false;
		$scope.statusAdminDebitOptions = false;

    }

    if ($state.is('atendimentoCliente.cliente-despesas-nova-lancamento')) {
    	if ($stateParams.despesa) {

    		$scope.despesaDTO = {};
    		$scope.despesaDTO.boAutorizadoGerente = false;
    		$scope.despesaDTO.despesa = $stateParams.despesa;

    		$scope.despesaDTO.nmLanctoTitular = $stateParams.despesa.contratoCliente.cliente.nmCliente;

    		$scope.despesaDTO.nmLanctoPlano = "PARTICULAR NAO ASSOCIADO";
    		if (!$stateParams.despesa.contratoCliente.cliente.boNaoAssociado) {
    			$scope.contrato = Contrato.get({ id: $stateParams.despesa.contratoCliente.contrato.id }, function(){
    				$scope.despesaDTO.nmLanctoPlano = $scope.contrato.plano.nmPlano;
    			});
    		}

    		$scope.despesaDTO.nmLanctoPaciente = $scope.despesaDTO.nmLanctoTitular;
    		if ($stateParams.despesa.contratoDependente.dependente) {
    			$scope.despesaDTO.nmLanctoPaciente = $stateParams.despesa.contratoDependente.dependente.nmDependente;
    		}

    		$scope.despesaDTO.nmLanctoTipoDespesa = "Despesa Geral";

    		$scope.despesaDTO.vlLanctoCobertura = $stateParams.despesa.vlDespesaCoberta ? $stateParams.despesa.vlDespesaCoberta : 0;
    		$scope.despesaDTO.vlLanctoDespesaTotal = $stateParams.despesa.vlDespesaTotal ? $stateParams.despesa.vlDespesaTotal : 0;

    		$scope.despesaDTO.vlLanctoCreditoPessoal = $stateParams.despesa.contratoCliente.cliente.vlCreditoPessoalTotal ? $stateParams.despesa.contratoCliente.cliente.vlCreditoPessoalTotal : 0;
    		$scope.despesaDTO.vlLanctoLimite = $stateParams.despesa.contratoCliente.cliente.vlLimite ? $stateParams.despesa.contratoCliente.cliente.vlLimite : 0;
    		$scope.despesaDTO.vlLanctoSalario = $stateParams.despesa.contratoCliente.cliente.vlSalario ? $stateParams.despesa.contratoCliente.cliente.vlSalario : 0;
    		$scope.despesaDTO.vlLanctoPercentual = $stateParams.despesa.contratoCliente.cliente.nrPercentual ? $stateParams.despesa.contratoCliente.cliente.nrPercentual : 0;

    		$scope.despesaDTO.vlLanctoBoletoUsado = 0;
    		$scope.despesaDTO.vlLanctoValorDespesa = $stateParams.despesa.vlDespesaAberto;
    		$scope.despesaDTO.inParcelado = "0";
    		$scope.despesaDTO.inFormaPagamento = -1;
    		$scope.despesaDTO.inIndexTab = 0;
    		$scope.despesaDTO.dtVencimento = new Date();
    		$scope.despesaDTO.inQtdParcela = 1;

    		servicesFactory.clienteSomaValorDebito.get( { id: $stateParams.despesa.contratoCliente.cliente.id }, function(data){
    			$scope.despesaDTO.vlLanctoDebitoUsado = data.soma;
    			$scope.despesaDTO.vlLanctoDisponivel = 0;
    			if ( ( $scope.despesaDTO.vlLanctoLimite - ( $scope.despesaDTO.vlLanctoDebitoUsado + $scope.despesaDTO.vlLanctoBoletoUsado ) ) > 0 ) {
    				$scope.despesaDTO.vlLanctoDisponivel = $scope.despesaDTO.vlLanctoLimite - ( $scope.despesaDTO.vlLanctoDebitoUsado + $scope.despesaDTO.vlLanctoBoletoUsado );
    			}

    		} );

    		$scope.despesaDTO.dependenteList = servicesFactory.clienteDependentes.get({id: $stateParams.despesa.contratoCliente.cliente.id });
    		$scope.despesaDTO.contratoList = servicesFactory.clienteContratos.get({id: $stateParams.despesa.contratoCliente.cliente.id });

    	}

    }

    var getOrcamentos = function( idPaciente, tipoCliente) {


    	if( idPaciente != null  ) {

    		if(  tipoCliente == 'T' ) {

				$scope.orcamentosTitular = [];
				$scope.orcamentoTItular = {};
				$scope.canShowTitularOrcDetails = false;

	    		servicesFactory.getOrcamentosCliente
			 		.get( {idPaciente: idPaciente} , function( result ) {
			   			if( result.length > 0 ) {
			   				$scope.orcamentosTitular = result;
			   				$scope.canShowTitularOrcDetails = true;
			   			}
	         	});

	    	} else if( tipoCliente == 'D') {

	    		$scope.orcamentosDependente = [];
	    		$scope.orcamentoDependente = {};
	    		$scope.canShowDependenteOrcDetails = false;

	    		servicesFactory.getOrcamentosCliente
			 		.get( {idPaciente: idPaciente} , function( result ) {
			   			if( result.length > 0 ) {
			   				$scope.orcamentosDependente = result;
			   				$scope.canShowDependenteOrcDetails = true;
			   			}
	         	});
    		}
    	}
    };

    $scope.carregarServicosOrcamento = function( orcamento, tipoCliente ) {

    	$scope.despesaServicosSelecionados = new SortedArrayMap();

    	if( tipoCliente == 'T' ) {
			$scope.servicosOrcamentoTitular = [];
			servicesFactory.getServicosOrcamento
		 		.get( {idOrcamento: orcamento.idOrcamentoExame}, function( result ) {
		   			if( result.length > 0 ) {
		   				$scope.servicosOrcamentoTitular = result;
		   			}
         	});
    	} else if( tipoCliente == 'D' ) {
    		$scope.servicosOrcamentoDependente = [];
    		servicesFactory.getServicosOrcamento
		 		.get( {idOrcamento: orcamento.idOrcamentoExame}, function( result ) {
		   			if( result.length > 0 ) {
		   				$scope.servicosOrcamentoDependente = result;
		   			}
         	});
    	}
	};

	//aqui
    if ($state.is('atendimentoCliente.cliente-despesas-nova')){

    	$scope.rdTipoPaciente = 0; // titular padrao
    	$scope.despesa = new Despesa({});
    	$scope.buttonDisabled = true;

    	$scope.canShowTitularOrcDetails = false;
    	$scope.canShowDependenteOrcDetails = false;

    	if ($scope.selectedCliente && $scope.selectedCliente.id) {

    		$scope.despesa.contratoCliente = {};
    		$scope.despesa.contratoCliente.contrato = {};

    		$scope.despesa.contratoDependente = {};
    		$scope.despesa.contratoDependente.contrato = {};

    		$scope.despesa.vlDespesaTotal = 0.0;
    		$scope.despesa.vlDespesaCoberta = 0.0;
    		$scope.despesa.vlDespesaAberto = 0.0;

    		$scope.despesa.contratoCliente.cliente = Cliente.get({ id: $scope.selectedCliente.id });

			$scope.contratoOptions = servicesFactory.clienteContratos.get({id: $scope.selectedCliente.id },
				function(dados){
					$scope.despesa.contratoCliente.contrato = dados[0];
				});

	        $scope.dependenteOptions = servicesFactory.clienteDependentes.get({id: $scope.selectedCliente.id });

		}

    	$scope.despesaServicosSelecionados = new SortedArrayMap();

    	//NOVO FLUXO
    	getOrcamentos( $scope.selectedCliente.id, 'T');

    }


    if ($state.is('atendimentoCliente.cliente-despesas-editar')) {
    	if ($stateParams.id) {
	        Despesa.get({ id: $stateParams.id},
				function(data)
				{
					$scope.despesaDTO = data;
					for (var i = 0; i < $scope.despesaDTO.despesa.despesaServicos.length; i++)
					{
						setGuia(i, $scope.despesaDTO.despesa.despesaServicos[i].id);
					}
				}
			)

			function setGuia(i, idDespesa)
			{
				servicesFactory.searchGuiaByIdDespesaServico.get({id: idDespesa},
					function(data)
					{
						$scope.despesaDTO.despesaServicos[i].guia = data;
					}
				);
			}
    	}
    }

    if($state.is('atendimentoCliente.cliente-despesas')){
    	$scope.refreshDespesas();
    }
});

MedicsystemApp.controller('ModalDespesaDeleteCtrl', function ($rootScope, $scope, $modalInstance, servicesFactory, despesa) {

	var Despesa = servicesFactory.despesas;

	$scope.despesa = despesa;
	$scope.motivoExclusao = null;

	$scope.ok = function () {
		if ($scope.motivoExclusao == undefined || $scope.motivoExclusao.length < 5) {
			$rootScope.alerts.push({
                type: "danger",
                msg: "Motivo de exclusão de despesa é obrigatório e deve conter pelo menos 5 caracteres.",
                timeout: 5000 });
		} else {
			var despesaToBeDeleted = Despesa.get({ id: despesa.id }, function() {
				servicesFactory.despesas.remove({ motivo: $scope.motivoExclusao }, despesaToBeDeleted.despesa, function(){
					$modalInstance.close(true);
				});
			});

		}
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});

MedicsystemApp.controller('ModalEncaminhamentoCtrl', function ($scope, $modalInstance, servicesFactory, despesasServico, despesa, idCliente, idDependente, rdTipoPaciente) {

	$scope.ds = [];
	$scope.servicosIDs = [];

	despesasServico.forEach(function (ds){
		ds.rdTipoEncaminhamento = 0;
		ds.rdTipoEncaminhamentoInterno = 0;
		ds.profissionalDTO = {};
		ds.credenciadaInternaDTO = {};
		ds.credenciadaExternaDTO = {};

		$scope.servicosIDs.push(ds.servico.id);
		$scope.ds.push(ds);
	});

	if (rdTipoPaciente == 1) {//Dependente
        var identContrato = despesa.contratoDependente != null && despesa.contratoDependente.contrato != null && despesa.contratoDependente.contrato.id != null ? despesa.contratoDependente.contrato.id : despesa.contratoCliente.contrato.id;

		$scope.profissionaisOptions = servicesFactory.servicosProfissionais.query({
	        id: $scope.servicosIDs,
	        idCliente: idCliente,
	        idDependente: idDependente,
	        idContrato: identContrato
			},
			function()
			{
		            $scope.credenciadasInternaOptions =
					servicesFactory.servicosCredenciadas.query
					({
		            id: $scope.servicosIDs,
		            idCliente: idCliente,
		            idDependente: idDependente,
		            idContrato: identContrato,
		            tipoCredenciada: 1
		            }, function(){
	                    $scope.credenciadasExternaOptions = servicesFactory.servicosCredenciadas.query({
	                    id: $scope.servicosIDs,
	                    idCliente: idCliente,
	                    idDependente: idDependente,
	                    idContrato: identContrato,
	                    tipoCredenciada: 0
	                    });
	            });
        });

	} else {
		$scope.profissionaisOptions = servicesFactory.servicosProfissionais.query({
			id: $scope.servicosIDs,
			idCliente: idCliente,
			idContrato: despesa.contratoCliente.contrato.id
		});

		$scope.credenciadasInternaOptions = servicesFactory.servicosCredenciadas.query({
			id: $scope.servicosIDs,
			idCliente: idCliente,
			idContrato: despesa.contratoCliente.contrato.id,
			tipoCredenciada: 1
		});

		$scope.credenciadasExternaOptions = servicesFactory.servicosCredenciadas.query({
			id: $scope.servicosIDs,
			idCliente: idCliente,
			idContrato: despesa.contratoCliente.contrato.id,
			tipoCredenciada: 0
		});
	}

	$scope.ok = function () {
		$modalInstance.close($scope.ds);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});

// Controller referenced to Modal Instance
MedicsystemApp.controller('ModalGeneratePdfInstanceCtrl', function ($scope, $state, $modalInstance, $window, idAgendamento, $http, $sce) {

	$scope.ok = function () {

   	  $http.get('/mediclab/agendamentos/'+ idAgendamento +'/despesa/pdf',{})
  	  	.success(function (response) {

  		    var blob = b64toBlob(response, 'application/pdf');
  		    var fileURL = URL.createObjectURL(blob);

  	      $scope.pdf = $sce.trustAsResourceUrl(fileURL);
          $("#imprimirPdf").prop("visibled",false);
      });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});

// somente agendamento
MedicsystemApp.controller('ModalAutorizationManagerInstanceCtrl', function ($rootScope, $scope, $state, $modal, $modalInstance, $http, items, servicesFactory) {

  $scope.loginAutorizacaoGerente = function () {

      var loginDTO = {username: $scope.userNameGerente,
                      password: $scope.passwordGerente};

      items.AutorizarGerenteDepesa.save({},
          loginDTO,
          function(data) {

              if(data){
                items.Despesa.save({ id: items.idAgendamento },
                  items.despesaDTO,
                  function(data) {

                    $modalInstance.close(openAutGerenteGeneratePdfModal(items.idAgendamento));
                  },
                  function(error) {
                    $rootScope.alerts.push({
                      type: "danger",
                      msg: "Erro ao salvar Despesa Agendamento no banco de dados. Verifique os dados e envie novamente.",
                      timeout: 10000 });
                      Metronic.scrollTop();
                  });
              }
          },
          function(error) {

            if(error.status == '403'){

              $rootScope.alerts.push({
                type: "danger",
                msg: "Usuário sem autorização para esta ação.",
                timeout: 10000
              });
              $modalInstance.close($state.go("atendimentoCliente.agendamento"));
            } else {

              $rootScope.alerts.push({
                type: "danger",
                msg: "Erro ao salvar Despesa Agendamento no banco de dados. Verifique os dados e envie novamente.",
                timeout: 10000 });

                Metronic.scrollTop();
            }
          });
  }

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  function openAutGerenteGeneratePdfModal(idAgendamento) {

    $scope.animationsEnabled = true;
    $("#btnRegistrar").prop("disabled",true);

    var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'gerarPdfModalContent.html',
        controller: 'ModalGeneratePdfInstanceCtrl',
        resolve: {
            idAgendamento: function () {
                return idAgendamento;
            }
        }
    });

    modalInstance.result.then(function (selectedItem) {
       $scope.selected = selectedItem;
       }, function () {
    });
  };
});
