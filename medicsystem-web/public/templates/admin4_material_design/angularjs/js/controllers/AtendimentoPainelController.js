'use strict';

MedicsystemApp.controller('AtendimentoPainelController', function($rootScope,
		$scope, $http, $timeout, $state, $stateParams, servicesFactory, Auth,NgTableParams) {

	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:10 };
			var initialSettings = {
				counts: [5, 10, 20],
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
		}
  	};

	function loadEspecialidades () {
		
		$scope.especialidades = [];

		servicesFactory.especialidades
		.query( function( especialidadesResult ) {
			if( especialidadesResult )
				$scope.especialidades = especialidadesResult;
		}) ;
	};

	$scope.loadProfissionalByEspecialidade = function ( item ) {
		if( item ) {
			$scope.profissional.selected = null;
			$scope.profissionais = [];
			servicesFactory.especialidadeProfissionais
			.query( { id: item.id }, function( profissionalResult ) {
				$scope.profissionais = profissionalResult;
			});
		}
	};

	$scope.pesquisarAtendimentoProfissional = function() {
		$scope.clientes = [];
		servicesFactory.agendamentosProfissional
		.get({idProfissional: $scope.profissional.selected,idEspecialidade: $scope.especialidade.selected}, 
			function( result ) {
				if( result ) {
					$scope.clientes = result;
					configurarTable( result );
				}
		});
	};

	function inicio () {

		$scope.clientes = [];

		$scope.especialidade = {};
		$scope.profissional = {};
		
		$scope.especialidade.selected = null;
		$scope.profissional.selected = null;
	
		loadEspecialidades();	
	};

	inicio();

});