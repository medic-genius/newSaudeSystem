'use strict';

MedicsystemApp.controller('MovimentacaoCaixaController', function($rootScope,$scope,$state,
    $stateParams,caixaFactory,Auth, NgTableParams, $modal, toastr) {

    //local host 
    /*var DRCONSULTA_ID = 17 | 12 para testar local;
    var PERFORMANCE_ID = 15;
    var DENTAL_ID = 13;*/

    var DRCONSULTA_ID = 17;
    var PERFORMANCE_ID = 15;
    var DENTAL_ID = 13;

    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title); 
        } else if( type == 'informacao' ) {
           toastr.info(  message, title); 
        }
    };

    function definirPaginacaoNgTable ( object ) {
        $scope.tableParamsMensalidade = new NgTableParams();
        $scope.tableParamsDespesa = new NgTableParams();
        $scope.tableParamsOperacao = new NgTableParams();

        var initialParams = { count:10 };
        var initialSettings = { counts:[],  paginationMaxBlocks: 13, paginationMinBlocks: 2};


        if( object.conferenciaMensalidadeList.length > 0 ) {
            initialSettings.dataset = object.conferenciaMensalidadeList;
            $scope.tableParamsMensalidade = new NgTableParams( initialParams, initialSettings );
        }

        if( object.conferenciaParcelaList.length > 0 ) {
            initialSettings.dataset = object.conferenciaParcelaList;
            $scope.tableParamsDespesa = new NgTableParams( initialParams, initialSettings );
        }

        if( object.movimentacaoCaixaList.length > 0 ) {
            initialSettings.dataset = object.movimentacaoCaixaList;
            $scope.tableParamsOperacao = new NgTableParams( initialParams, initialSettings );
        }

    };

    function loadMovimentacao() {
        $scope.movimentacaoCaixaDTO = {};

        $scope.movCaixaRequest =  caixaFactory.movimentacaoCaixa
        .query({ idEmpresaFinanceiro: $stateParams.idEmpresa, idCaixa: $stateParams.idCaixa }, function( resultDTO ) {
            if( resultDTO ) {
                $scope.movimentacaoCaixaDTO = resultDTO;
                definirPaginacaoNgTable( resultDTO );
            }
        })
    };

    function prepararModalEscolherContaBancaria() {
        var valorEntrada = null;

        if( $stateParams.idEmpresa == DRCONSULTA_ID || $stateParams.idEmpresa == DENTAL_ID )
            valorEntrada = $scope.movimentacaoCaixaDTO.vlRetirada;
        else if ( $stateParams.idEmpresa == PERFORMANCE_ID )
            valorEntrada = $scope.movimentacaoCaixaDTO.totalEspecie;
        else
            valorEntrada = null;
    
        var modalInstance = $modal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'templates/admin4_material_design/angularjs/views/caixa/conferencia/conferenciaContaBancariaModal.html',
            controller: 'ModalContaBancariaController',
            resolve: {
                idEmpresaFinanceiro: function () {
                  return $stateParams.idEmpresa;
                },
                valorEntrada : function () {
                    return valorEntrada;
                },
                nmOperador: function () {
                    return $scope.caixaSelected.nmOperador;
                },
                nmUnidade: function () {
                    return $scope.caixaSelected.nmUnidade;
                },
                dtCaixaFormatada: function () {
                    return $scope.caixaSelected.dtCaixaFormatada;
                }
            }
        });

       modalInstance.result.then(function () {
        }, function () {
               console.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.cancelarConferencia = function() {
        $state.go('conferencia-operadorCaixa',{idEmpresa: $stateParams.idEmpresa,nmEmpresa:$stateParams.nmEmpresa, objectSearch: $stateParams.objectSearch})
    };

    $scope.conferirCaixa = function() {
        $scope.load_conferir = true;
        caixaFactory.conferirCaixa
        .query({ idEmpresaFinanceiro: $stateParams.idEmpresa, idCaixa: $stateParams.idCaixa },
            function( object, responseHeaders ) {
                var location = responseHeaders('Location');
                if (location) {
                    var locationArray = location.split("/");
                    var caixaId = locationArray[locationArray.length - 1];
                    console.log(caixaId,caixaId);

                    if( caixaId ) {
                        messageToastr("sucesso","Caixa conferido com sucesso","SUCESSO");
                        $scope.load_conferir = false;
                        prepararModalEscolherContaBancaria();
                    }
                }
        }, function( err ) {
            messageToastr("erro","Erro ao conferir caixa","DESCULPA,");
            $scope.load_conferir = false;
        });
    };

    $scope.desconferirCaixa = function() {
        $scope.load_desconferir = true;

        caixaFactory.desconferirCaixa
        .query({ idEmpresaFinanceiro: $stateParams.idEmpresa, idCaixa: $stateParams.idCaixa },
            function( object, responseHeaders ) {
                var location = responseHeaders('Location');
                if (location) {
                    var locationArray = location.split("/");
                    var caixaId = locationArray[locationArray.length - 1];
                    console.log(caixaId,caixaId);

                    if( caixaId ) {
                        messageToastr("informacao","Caixa desconferido com sucesso","SUCESSO");
                        $scope.load_desconferir = false;
                        $state.go('conferencia-operadorCaixa', { idEmpresa: $stateParams.idEmpresa, nmEmpresa: $stateParams.nmEmpresa, objectSearch :$stateParams.objectSearch });
                    }
                }
        }, function( err ) {
            $scope.load_desconferir = false;
        });
    };

    function inicio( ) {
        loadMovimentacao(); 
    };

    if( $stateParams.caixaSelected ) {
        $scope.objectSearch = $stateParams.objectSearch
        $scope.caixaSelected = $stateParams.caixaSelected;
        inicio();
    } else {
        $state.go('conferencia-operadorCaixa', { idEmpresa: $stateParams.idEmpresa, nmEmpresa: $stateParams.nmEmpresa, objectSearch :$stateParams.objectSearch });
    }


});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalContaBancariaController', function ($scope, $state, $stateParams ,$modalInstance, idEmpresaFinanceiro, valorEntrada, caixaFactory, toastr, nmOperador, nmUnidade, dtCaixaFormatada) {

    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title); 
        } else if( type == 'informacao' ) {
           toastr.info(  message, title); 
        }
    };

    function loadContaBancariaDinheiro() {
        $scope.contasBancariaOptions = [];
        caixaFactory.contasBancariasDinheiro
        .query({idEmpresaFinanceiro: idEmpresaFinanceiro}, function( dataModalResult ) {
            $scope.contasBancariaOptions = dataModalResult;
        })
    }

    $scope.ok = function () {
        if( $scope.contaBancaria.selected ) {
            caixaFactory.movimentacaoEntradaCB
            .get({idContaBancaria: $scope.contaBancaria.selected, valor: valorEntrada, idCaixa: $stateParams.idCaixa, nmOperador: nmOperador,nmUnidade:nmUnidade, dtCaixa: dtCaixaFormatada},
            function( object, responseHeaders ) {
                var location = responseHeaders('Location');
                if (location) {
                    var locationArray = location.split("/");
                    var movimentacaoId = locationArray[locationArray.length - 1];

                    if( movimentacaoId ) {
                        console.log("movimentacaoId",movimentacaoId);
                        messageToastr("sucesso", "Valor adicionado com sucesso.", "SUCESSO");
                        $modalInstance.close($state.go('conferencia-operadorCaixa',{idEmpresa: $stateParams.idEmpresa,nmEmpresa:$stateParams.nmEmpresa,objectSearch:$stateParams.objectSearch}));
                    }
                }
            }, function( err ) {
                messageToastr("erro", "não foi possível realizar a ação.", "DESCULPA,");
            });
        } else
            messageToastr('erro','Escolha a conta bancária, por favor.', 'OPA,');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


    function inicioModal() {
        $scope.contaBancaria = {};
        $scope.valorEntrada = valorEntrada;
        loadContaBancariaDinheiro();
    }

    inicioModal();

});
