'use strict';

MedicsystemApp.controller('ConferenciaEmpresaController', function($rootScope,$scope,$state,
	$stateParams,caixaFactory,Auth, NgTableParams) {

	function loadEmpresas() {
		caixaFactory.empresaGrupoCompartilhado
		.query( function( list) {
			if( list )
				$scope.empresaOptions = list;
		} );	
	};

	$scope.goToCaixa = function( egCompartilhadoSelected ) {
		$state.go('conferencia-operadorCaixa', {idEmpresa: egCompartilhadoSelected.empresaFinanceiro.idEmpresaFinanceiro,nmEmpresa: egCompartilhadoSelected.empresaFinanceiro.nmFantasia});
	};

	function inicio( ) {
		loadEmpresas();
	};

	inicio();

});


