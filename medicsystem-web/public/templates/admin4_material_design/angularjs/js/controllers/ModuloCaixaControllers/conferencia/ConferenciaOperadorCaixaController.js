'use strict';

MedicsystemApp.controller('ConferenciaOperadorCaixaController', function($rootScope,$scope,$state,
	$stateParams,caixaFactory,Auth, NgTableParams) {


	function configurarTable( lista ) {
		
		$scope.tableParams = new NgTableParams();

		if( lista.length > 0 ) {
			var initialParams = { count:10 };
			var initialSettings = {
				counts: [5, 10, 20],
	    		paginationMaxBlocks: 13,
	    		paginationMinBlocks: 2,
	    		dataset: lista
			};

			$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
		}
  	};

	function inicializarData( isRecovery ) {
   		$scope.dataFiltro = {};

        if( !isRecovery ) {
            var dataAtual = moment();
            $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};      
        } else {
            $scope.dataFiltro.date = { startDate: moment( $stateParams.objectSearch.intervalo.dtInicio, 'DD/MM/YYYY' ), endDate: moment( $stateParams.objectSearch.intervalo.dtFim,'DD/MM/YYYY' )  }
        }
        
    };

    function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
	   		'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()],
	   		'Últimos 60 Dias': [ moment().subtract(59, 'days'),moment()],
		};
    };

    function loadUnidadesByEmpresa( idEmpresa ) {
        
        $scope.unidadeOptions = [];

        caixaFactory.unidadeEmpresaGrupoCompartilhado
        .query( {idEmpresaFinanceiro: $stateParams.idEmpresa} ,function( result ) {
            if( result ) {
                $scope.unidadeOptions = result;
            }
        });
    };

    function  loadOperadoresByEmpresa() {
        
        $scope.operadorOptions = [];
        
        caixaFactory.operadorCaixa
        .query( {idEmpresaFinanceiro: $stateParams.idEmpresa }, function( result ) {
            if( result ) {
                $scope.operadorOptions = result;
            }
        })    
    };

    function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null &&
                $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("DD/MM/YYYY");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };

    $scope.goToCaixaMovimentacao = function( caixaSelecionado ) {
        var intervalo = getIntervaloDataSelecionada();
        var unidade =  $scope.unidade.selected ? $scope.unidade.selected: null ;
        var idOperador = $scope.operador.selected ? $scope.operador.selected.id: null;

        var objectSearch = { idEmpresa: $stateParams.idEmpresa, unidade: unidade, intervalo: intervalo, idOperador: idOperador };
    	$state.go('conferencia-movimentacao',{idEmpresa: $stateParams.idEmpresa , nmEmpresa: $stateParams.nmEmpresa , idCaixa: caixaSelecionado.idCaixa, caixaSelected: caixaSelecionado, objectSearch: objectSearch });
    };

    $scope.loadCaixas = function() {
        var intervalo = getIntervaloDataSelecionada();
        var idUnidade =  $scope.unidade.selected ? $scope.unidade.selected.id: null ;
        var idOperador = $scope.operador.selected ? $scope.operador.selected.id: null;
        $scope.caixas = [];

        caixaFactory.caixaByEmpresaFinanceiro
        .query( { idEmpresaFinanceiro: $stateParams.idEmpresa, idUnidade: idUnidade, idOperador: idOperador,
            dtInicio: intervalo.dtInicio, dtFim: intervalo.dtFim} ,function( result ) {
            if ( result.length > 0 ) {
                $scope.caixas = result;
                configurarTable( $scope.caixas );
            }   
        });
    };

	function inicio( ) {
        
        if( $stateParams.objectSearch != null ) {
            inicializarData( true );
            $scope.unidade = {selected: $stateParams.objectSearch.unidade};
        }
        else {
            inicializarData( false );
            $scope.unidade = {};
        }

		definirConfigDateRange();

		$scope.operador = {};
        $scope.nmEmpresa = $stateParams.nmEmpresa;

		loadUnidadesByEmpresa( $stateParams.idEmpresa );
        loadOperadoresByEmpresa( $stateParams.idEmpresa );
		$scope.loadCaixas();
	};

	inicio();

});