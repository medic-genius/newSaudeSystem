'use strict';

MedicsystemApp.controller('ClienteSituacaoFinanceiraController', function($scope,$state,$stateParams,servicesFactory) {

	function getContratosCliente() {
		servicesFactory.situacaoFinanceira
		.contratosCliente({id:$scope.selectedCliente.id }, function( result ) {
			if( result ) {
				$scope.contratos = [];
				$scope.contratos = result;
			}	
		})
	};

	$scope.carregarTitulosEmAberto = function( contrato ) {
		contrato.titulosAbertos = [];	
		servicesFactory.situacaoFinanceiraTitulosAbertos
		.get({'nrContrato':contrato.nrContrato }, function( result ) {
			if( result ) {
				contrato.titulosAbertos = result;
			}
		});
	};

		$scope.carregarTitulosFechado = function( contrato ) {
		contrato.titulosAbertos = [];	
		servicesFactory.situacaoFinanceiraTitulosFechado
		.get({'id':$scope.selectedCliente.id,'nrContrato':contrato.nrContrato }, function( result ) {
			if( result ) {
				contrato.titulosFechados= result;
			}
		});
	};

	$scope.getDependentesContrato = function( contrato ) {
		contrato.expanded = true;
		contrato.dependentes = [];
		servicesFactory.situacaoFinanceiraDependentes
		.get({cliente:$scope.selectedCliente.id, contrato: contrato.idContrato}, function( result) {
			if( result ) {
				contrato.dependentes = result;
			}
		});
	};

	function inicio() {
		getContratosCliente();
	};

	inicio();
			
});


