'use strict';

MedicsystemApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

MedicsystemApp.controller('AtendimentoController', function($rootScope, $scope, $state, $stateParams, $modal, servicesFactory) {

    $scope.perfisMenuDespesa = ['administrador', 'atendimentocliente', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'];

    var Cliente = servicesFactory.clientes;
    var ClienteView = servicesFactory.clienteView;

    $rootScope.digitalClienteMsg = "Ler Biometria";

    $scope.atualizarSelectClientes = function(text) {
        if (text) {
            var queryObject = {};
            var filtro = $scope.searchFiltro;
            text = text.toLowerCase();
            if (filtro && filtro.startsWith("!")) {
                if (filtro.startsWith("!cod")){
                    queryObject = { nrCodCliente: text.trim() };
                } else if (filtro.startsWith("!rg")){
                    queryObject = { nrRG: text.trim() };
                } else if (filtro.startsWith("!cpf")){
                    queryObject = { nrCPF: text.trim() };
                } else {
                    text = null;
                }

            } else {
                queryObject = { nmCliente: text };
            }

            if(text){
                var data = Cliente.query(queryObject, function() {
                    $scope.clientes = data;
                    if (data.length == 0) {
                        $scope.selectedClienteNotFound = "Sem dados disponiveis.";

                    } else {
                        $scope.selectedClienteNotFound = "";
                        
                    }
                });
            }
        }
    
    };
    
    $scope.novoCliente = function() {
        $scope.cliente = {}
        $scope.selectClienteFromSearch = {}
        $scope.selectedCliente = new Cliente({});
        
        $scope.showDetails = false;
        $scope.showClienteOptions = true;

        $state.go('atendimento.cliente-conta-novo');
    }

    $scope.showDetails = true;
    $scope.showClienteOptions = false;

    $scope.searchUser = function(item) {
        console.log('entrou');
        $scope.selectClienteFromSearch = item;
        $scope.selectedCliente = Cliente.get( {id: item.id}, function(){
            $scope.showDetails = false;
            $scope.showClienteOptions = false;
            $scope.situacoes = servicesFactory.clienteSituacao.query({ id: item.id });
            $state.go('atendimento');
        });
    };

    $scope.saveAgendamentoCadastro = function() {
        $state.go('atendimento.cliente-agendamento');
    }

    $scope.saveDependenteCadastro = function() {
        $state.go('atendimento.cliente-dependentes');
    }

    $scope.saveContratoCadastro = function() {
        $state.go('atendimento.cliente-contratos');
    }
    
    
    $scope.showDetails = true;
    $scope.showClienteOptions = false;
    $scope.selectedCliente = new Cliente({});
    $scope.cliente = {};
    $scope.searchFiltro = "";
    $scope.situacoes = [];
 
    $scope.lerBiometria = function() {

        var socket = io.connect('http://localhost:9092');
        $rootScope.digitalClienteMsg = "Aguardando digital...";
        $rootScope.digitalCliente = undefined;

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });
        socket.emit('biometriaevent', {});

        socket.on('biometriaevent', function(data) {

            if(!isNaN(data)){

                $rootScope.digitalCliente = data;

                $scope.selectClienteFromSearch = ClienteView.get({ id: $rootScope.digitalCliente  }, function(){
                    $scope.selectedCliente = Cliente.get( {id: $rootScope.digitalCliente }, function(){
                        $scope.showDetails = false;
                        $scope.showClienteOptions = false;
                        $scope.situacoes = servicesFactory.clienteSituacao.query({ id: $rootScope.digitalCliente  });
                        $rootScope.digitalClienteMsg = "Biometria aceita - Ler Novamente";
                        $scope.cliente = {};
                        $state.go('atendimento');
                    });
                });
            } else {

                $rootScope.digitalClienteMsg = "Digital invalida - Ler Novamente";
                $scope.selectedCliente = {};
                $scope.showDetails = true;
                $scope.showClienteOptions = true;
                $state.go('atendimento');
            }
        });
    }   

    $scope.$on('updateUserBroadcast', function (event, value) {
        if (value == 'true') {
            $scope.searchUser( { id: $scope.selectedCliente.id } );
        }
    });

    $scope.abrirCameraFoto = function() {

        var itemsCliente = {
            Cliente: servicesFactory.clientes, 
            selectedCliente: $scope.selectedCliente,
            clienteSituacao: servicesFactory.clienteSituacao,
            selectClienteFromSearch: $scope.selectClienteFromSearch,
            searchUser: $scope.searchUser
        };

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'abrirFotoCliente.html',
            controller: 'ModalInstanceCadFotoClienteCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                items: function () {return itemsCliente;}
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.info('Modal dismissed at: ' + new Date());
        });
    }
});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalInstanceCadFotoClienteCtrl', function ($rootScope, $scope, $state, $modalInstance, items) {
    
    $scope.cameraSettings = { videoHeight: 240, videoWidth: 320 };

    $scope.makeSnapshot = function makeSnapshot() {
        
        var _video = $scope.cameraSettings.video;
        var patData = null;

        $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};
        $scope.patOpts.w = _video.width;
        $scope.patOpts.h = _video.height;
        
        if (_video) {

            var patCanvas = document.querySelector('#snapshot');
            if (!patCanvas) return;

            patCanvas.width = _video.width;
            patCanvas.height = _video.height;
            var ctxPat = patCanvas.getContext('2d');

            var idata = getVideoData(_video, $scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
            ctxPat.putImageData(idata, 0, 0);

            sendSnapshotToServer(patCanvas.toDataURL(), items.Cliente, items.selectedCliente, items.clienteSituacao, items.searchUser, items.selectClienteFromSearch);

            patData = idata;
        }
    };

    var getVideoData = function getVideoData(_video, x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = _video.width;
        hiddenCanvas.height = _video.height;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, _video.width, _video.height);
        return ctx.getImageData(x, y, w, h);
    };

    /**
     * This function could be used to send the image data
     * to a backend server that expects base64 encoded images.
     *
     * In this example, we simply store it in the scope for display.
    */
    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64, Cliente, selectedCliente, clienteSituacao, searchUser, selectClienteFromSearch) {
        
        $scope.snapshotData = escape(imgBase64.split(',')[1]);
        selectedCliente.fotoClienteEncoded = $scope.snapshotData;

        Cliente.update( { id: selectedCliente.id },  selectedCliente, function(data){
            $rootScope.addDefaultTimeoutAlert("Foto do Cliente", "atualizado", "success");
            searchUser(selectClienteFromSearch);
        }, function(error){
            $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
        });

        Metronic.scrollTop();

        $modalInstance.close();
    };
  
    $scope.exit = function () { $modalInstance.close();};

    $scope.cancel = function () { $modalInstance.dismiss('cancel'); };
});