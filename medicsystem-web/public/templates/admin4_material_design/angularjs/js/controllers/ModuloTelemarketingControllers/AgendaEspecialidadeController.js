
'use strict';

MedicsystemApp.controller('AgendaEspecialidadeController', function($rootScope,$scope,
	 $http, $timeout,$state,$stateParams,$filter, servicesFactory, Auth,
	 $compile,uiCalendarConfig) {
			
  
    // ** BEGIN CALENDAR CALLBACKS
       
    $scope.eventRender = function( event, element, view ) { 
        /*element.attr({'tooltip': event.title,
                     'tooltip-append-to-body': true});
        $compile(element)($scope);*/
    };

    $scope.alertOnEventClick = function( date, jsEvent, view){
        var dadosPreAgendamento = {};
        dadosPreAgendamento.obj = date.obj;
        dadosPreAgendamento.start = date.start.add(12,"hour");
        $state.go('atendimentoCliente',{dadosPreAgendamento: dadosPreAgendamento});
    };
     
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
      $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
      $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    
    $scope.viewRender = function(view, element) {

      $scope.dtInicio = moment(view.intervalStart).format("YYYY/MM/DD");
      $scope.dtFinal = moment(view.intervalEnd).format("YYYY/MM/DD");

      if( $scope.especialidadesSelecionadas != null && $scope.especialidadesSelecionadas.length > 0  )
        $scope.reloadEventsEspecialidade( $scope.especialidadesSelecionadas );
    };
    
    // END CALENDAR CALLBACKS

    $scope.getAgendaProfissionaisPorEspecialidade = function( ids,isloadFuncionario ) {
      
      servicesFactory.agendaMedicosEspecialidade.get( {dtinicio: $scope.dtInicio,
        dtfim: $scope.dtFinal, ids: ids, idsunidade: $scope.idsUnidade, idsfuncionario: $scope.idsFuncionario}, 
        
        function( result ) {
        $scope.funcionario = result;
          if(uiCalendarConfig.calendars['myCalendar']){
              
                $scope.events = [];              
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');

                for(var i=0; i<result.length;i++) {
                  var atendimentoDisponivel = result[i];
                  
                  var nmFuncionarioArray = atendimentoDisponivel.nmFuncionario.split(" ");
                  atendimentoDisponivel.nmFuncionario = nmFuncionarioArray[0] + ' ' + nmFuncionarioArray[nmFuncionarioArray.length - 1];

                  
                  $scope.events.push({
                      title:'\n' + (atendimentoDisponivel.nmApelido ? atendimentoDisponivel.nmApelido.toUpperCase() : "") + ' \n '+(atendimentoDisponivel.boParticular ? 'PARTICULAR \n' : '')+ atendimentoDisponivel.qtdAtend  +' vagas '+ ' - ' + atendimentoDisponivel.nmEspecialidade.toLowerCase() + ' \n '+ atendimentoDisponivel.hrInicio + '-'+ atendimentoDisponivel.hrFim + ' \n '+ atendimentoDisponivel.nmFuncionario,
                      start: atendimentoDisponivel.dtAtend,
                      className: atendimentoDisponivel.idUnidade == 1 ? ["custom1"]: atendimentoDisponivel.idUnidade == 2 ? ["custom2"]: ["custom3"], 
                      obj: atendimentoDisponivel
                  });
                } 
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('addEventSource',$scope.events);
                if (isloadFuncionario){
                $scope.loadFuncionario();
                }
              }
        });  
    } ;   

      $scope.loadFuncionario = function (){
      $scope.funcionarioOptions = [];
      
      for (var i = 0; i < $scope.funcionario.length; i++){
        var hasId = false;
        for (var j = 0; j < $scope.funcionarioOptions.length; j++){
          if($scope.funcionario[i].idFuncionario == $scope.funcionarioOptions[j].idFuncionario){
            hasId = true;
          }
        }
        if(!hasId){
          $scope.funcionarioOptions.push($scope.funcionario[i]);
        }
      }
      $scope.funcionarioOptions = formatarFuncionarios();
      }


      $scope.changeFunc = function (){
      
      $scope.idsFuncionario = [];
      for (var i = 0; i < $scope.funcionarioSelecionados.length; i++){

       var idFuncionario = $scope.funcionarioSelecionados[i].id;
            $scope.idsFuncionario.push(idFuncionario);  
      }
      $scope.reloadEventsEspecialidade(false);
    }

      

    function formatarFuncionarios(){
      var listFormat = [];
  
      for (var i = 0; i < $scope.funcionarioOptions.length; i++){

        var option = {};
            option.id = $scope.funcionarioOptions[i].idFuncionario;
            option.name = $scope.funcionarioOptions[i].nmFuncionario;
            option.selected = false;

            listFormat.push(option);
      }
      return listFormat;
     }



    $scope.changeListUnidade = function (idUnidade){

      var retorno = $scope.idsUnidade.indexOf(idUnidade);

      if (retorno < 0){
      $scope.idsUnidade.push(idUnidade);
     
      }
      else{
      $scope.idsUnidade.splice(retorno,1);
      }
      if ($scope.especialidadesSelecionadas.length > 0){
      $scope.reloadEventsEspecialidade();
      }
    }  

    

    $scope.reloadEventsEspecialidade =  function (isloadFuncionario) {

      if(uiCalendarConfig.calendars['myCalendar'])
          uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');

      var ids = [];
      for( var i = 0; i < $scope.especialidadesSelecionadas.length; i++ ) {
            
            var idEspecialidade = $scope.especialidadesSelecionadas[i].id;
            ids.push(idEspecialidade);

            if( ids.length == $scope.especialidadesSelecionadas.length )
                $scope.getAgendaProfissionaisPorEspecialidade( ids,isloadFuncionario );
        }

    };

    function loadEspecialidades() {

    	$scope.especialidadeOptions = [];

    	servicesFactory.especialidadesAtivas.get( function( result ) {

	   		if( result ) {
	   			
	   			$scope.especialidades = result;

	   			for( var i=0;i<$scope.especialidades.length;i++ ) {
	   			
	   				var especialidade = $scope.especialidades[i];
	   				
	   				var option = {};
            option.id = especialidade.id;
	   				option.name = especialidade.nmEspecialidade;
	   				option.selected = false;

	   				$scope.especialidadeOptions.push(option);

	   			}
	   		}
	    });
    };

   function defineConfigFullCalendar() {
      $scope.uiConfig = {
        calendar:{
          height: 450,
          editable: false,
          selectable: false,
          header:{
            left: 'title',
            center: 'prev,next today',
            right: 'month,basicWeek,basicDay'
          },
          buttonText: {
            today: 'Hoje',
            month: 'Mês',
            week:  'Semana',
            day:   'Dia'
          },
          lang: "pt-br",
          eventClick: $scope.alertOnEventClick,
          eventDrop: $scope.alertOnDrop,
          eventResize: $scope.alertOnResize,
          eventRender: $scope.eventRender,
          viewRender: $scope.viewRender
        }
      };
    };

    function defineConfigMultiSelect() {
      $scope.localLang = {
        selectAll       : "Todos",
        selectNone      : "Desmarcar todos",
        reset           : "...",
        search          : "Pesquise...",
        nothingSelected : "Selecione as especialidades"
       } 
    };

    function defineConfigMultiSelectFunc() {
      $scope.LocalLang = {
        selectAll       : "Todos",
        selectNone      : "Desmarcar todos",
        reset           : "...",
        search          : "Pesquise...",
        nothingSelected : "Selecione os profissionais"
       } 
    };

    function inicio() {   
      defineConfigMultiSelectFunc();
      defineConfigMultiSelect();
    	loadEspecialidades();
      defineConfigFullCalendar();
      $scope.idsUnidade = [];
      $scope.funcionarioOptions = [];
      $scope.funcionarioSelecionados = [];
    };

    inicio();

});

