'use strict';

MedicsystemApp.filter('moment', function () {
  return function (input, momentFn /*, param1, param2, ...param n */) {
    var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
    return momentObj[momentFn].apply(momentObj, args);
  };
});

MedicsystemApp.controller('ClienteAgendamentoController', function($rootScope, $scope,
    $stateParams, $timeout, $state, $window, $http, $q, $filter, $sce, $ngBootbox, $modal,
    NgTableParams, toastr, Auth, servicesFactory) {

    var Agendamento = servicesFactory.agendamentos;
	var Cliente = servicesFactory.clientes;

	$scope.encaixeRoles = ['administrador', 'gerenteadm', 'gerenteunidade', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerenterecepcaomedicaodonto','atendimentocliente','telemarketing'];
	$scope.deleteRoles = ['administrador', 'gerenteadm', 'gerenteunidade', 'gerentetelemarketing', 'gerenterecepcaomedicaodonto', 'telemarketing'];
    $scope.infoagendamento = ['administrador', 'gerenteadm', 'gerenteunidade', 'gerenterecepcaomedicaodonto'];
    $scope.rolesUtimoAgendamentoFaltoso = ['telemarketing', 'gerentetelemarketing','administrador'];
    $scope.minDate = new Date();
    $scope.datasSemAtendimento = [];
    $scope.currentCalendarMonth = new Date().getMonth() + 1;    
    $scope.currentCalendarYear = new Date().getFullYear();
    $scope.disableEdit = true;
    $scope.disableReagendamento = true;
    $scope.disableReagendamentoButton = true;
    $scope.disableDatePickerReagendamento = 'true';
    $scope.viewLastAgendamentoTelemarketing = false;
    $scope.autorizadosPresencaAgendamento = [];
    $scope.isreturn = false;
    $scope.boRetornoParticular = false;
    $scope.msgParticular = false;
    $scope.boAgendamentoNovo = false;
    $scope.msgRetorno = false;
    $rootScope.boLogadoGerente = false;
    $rootScope.showMsgPresenca = false;

     var dateDisableDeferred =  $q.defer();
    $scope.dateRangeDisablePromise = dateDisableDeferred.promise;

    /*  $scope.customizaCalendario = function( date ,mode) {
            console.log("date",date);
            console.log("date", mode);
            return 'full';
         };*/

    $scope.setValidaHorarioAgendamento = function(itemHorario){
        if(itemHorario.boParticular != null && itemHorario.boParticular){
            $scope.agendamento.boParticular = true;

        }else{
            $scope.agendamento.boParticular = false;
        }

        if(itemHorario.boAceitaEncaixe != null && itemHorario.boAceitaEncaixe){
            $scope.boAceitaEncaixe = true;
        }else{
            $scope.boAceitaEncaixe = false;
        }

        $scope.validDivergenciaParticular();
    };

    $scope.validDivergenciaParticular = function(){
        if($scope.boAgendamentoNovo != null && !$scope.boAgendamentoNovo ) {
            if($scope.boRetornoParticular && $scope.agendamento.boParticular != null
                && $scope.agendamento.boParticular){
                $scope.buttonDisabled = true;
                $scope.msgRetorno = true;
                $scope.msgParticular = false;
            } else if(!$scope.boRetornoParticular && $scope.agendamento.boParticular != null
                && $scope.agendamento.boParticular != $scope.origemBoParticular){
                $scope.buttonDisabled = true;
                $scope.msgParticular = true;
                $scope.msgRetorno = false;
            } else {
                $scope.buttonDisabled = false;
                $scope.msgParticular = false;
                $scope.msgRetorno = false;
            }
        }
    }

    var defineUnidadeFuncionario = function() {
        var login =  Auth.authz.idTokenParsed.preferred_username;
        servicesFactory.getFuncionarioByLogin.get( {login: login},
        function( result ) {
            if( result ) {

                servicesFactory.unidades.query(function( resultUnidades ){

                    $scope.unidadeOptions = resultUnidades;

                    if( $scope.agendamentoViaTelemarketing() ) {

                        $scope.agendamento.unidade.id = $stateParams.dadosPreAgendamento.obj.idUnidade;

                        for(var i=0; i<$scope.unidadeOptions.length;i++){

                            if( $scope.agendamento.unidade.id == $scope.unidadeOptions[i].id  ){

                                $scope.loadEspecialidades($scope.unidadeOptions[i],true);
                                break;
                            }
                        }

                    } else {

                        for(var i=0; i< $scope.unidadeOptions.length;i++){
                            if( result.unidade.id ==  $scope.unidadeOptions[i].id  ){
                                $scope.agendamento.unidade.id = $scope.unidadeOptions[i].id ;
                                 $scope.loadEspecialidades($scope.unidadeOptions[i],true);
                                break;
                            }
                        }
                    }
                });
            }
        });
    };

    function configurarTable( lista ) {

        var initialParams = { count:5 };

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[],
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: lista
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )
    };



    $scope.dateRangeDisabled = function(date, mode) {

        if ($scope.datasSemAtendimento == null || $scope.datasSemAtendimento.length == 0){
            return true;
        }
        return ( mode === 'day' && (date.getDay() === 0 ||
            $scope.datasSemAtendimento.indexOf( $filter('date')(date, "dd/MM/yyyy") ) !== -1 ));
    };

    $scope.pushMessage = function( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage,
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop();
    };

    $scope.deleteAgendamento = function(agendamento) {
        $ngBootbox.confirm('Deseja realmente excluir o agendamento ?')
                    .then(function() {

                     $scope.agendamento = Agendamento.get({ id: agendamento.id }, function(){
                         $scope.agendamento.$remove(function(){
                         $scope.pushMessage("info","Agendamento marcado como excluído.",5000);
                         $scope.refreshAgendamentos();
            });
        });

                 });

    };

    $scope.openRetorno = function(agendamento) {
    	var dayAgendamento = moment(agendamento.dtAgendamento);
    	var today = moment(new Date());
    	var diff = today.diff(dayAgendamento, 'days');

        if(!agendamento.servico.boGeraRetornoAgendamento){

            $ngBootbox.alert('O serviço deste agendamento, NÃO CONTEMPLA RETORNO.').then(
                function() {});
    		return;
        }

    	if(diff > 30) {

            $ngBootbox.alert('RETORNO NEGADO, data do agendamento PASSA O LIMITE DE 30 DIAS do dia de hoje.').then(
                function() {});

            //$scope.pushMessage("danger","Retorno negado, data do agendamento passa o limite de 40 dias do dia de hoje.", 10000 );
    		return;
    	}

    	if (agendamento.boSolicitouRetorno && !agendamento.boRetornoGerado) {
    	   $state.go("atendimentoCliente.cliente-agendamento-retorno", { id: agendamento.id });
    	} else {
    	   $scope.agendamento = Agendamento.get({ id: agendamento.id });
    		var modalInstance = $modal.open({
    	    	animation: true,
    	    	templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
    	    	controller: 'ModalLoginGerenteController',
    	    	resolve: {
    	    		exibeJustificativa: function () {
    		          return true;
    		        },
    		        motivacaoAutorizacao: function () {
    		          return "AGENDAMENTO SEM SOLICITAÇÃO DE RETORNO, PARA GERAR UM RETORNO É NECESSÁRIO AUTORIZAÇÃO DO GERENTE DE UNIDADE";
    		        },
    		        perfilNecessario: function () {
    		        	return "gerenteunidade";
    		        }
    	    	}
    	    });

    	    modalInstance.result.then(function (data) {
    	    	if (data.autorizado) {
                    $rootScope.boLogadoGerente = true;
	    	    	$scope.agendamento.boSolicitouRetorno = true;
	    	    	$scope.agendamento.justificativaRetorno = data.justificativa;
	    	    	$scope.agendamento.justificativaLoginGerente = data.loginGerente;
	    	    	Agendamento.update( { id: $scope.agendamento.id }, $scope.agendamento, function(data){
                        $scope.pushMessage("info", "Geração de Retorno do Agendamento Liberado.", 5000 );
			    		$state.go("atendimentoCliente.cliente-agendamento-retorno", { id: agendamento.id });
			    	}, function(error){
			    		$scope.buttonDisabled = false;
			    		$rootScope.addDefaultTimeoutAlert("Agendamento", "atualizar", "error");
			    	});
			    	Metronic.scrollTop();
    	    	}

    	    });
    	}
    };

    $scope.recuperarHorariosDisponiveis = function( idFuncionario, idEspecialidade, tipoConsulta, tipoAgendamento, idUnidade, dtAgendamento ) {
        $scope.horarioOptions = []
        $scope.horarioOptions = servicesFactory.funcionarioHorarios.query({
                    id: idFuncionario,
                    idServico: $scope.agendamento.servico.id,
                    idEspecialidade: idEspecialidade,
                    inTipoConsulta: tipoConsulta,
                    inTipoAgendamento: tipoAgendamento,
                    idUnidade: idUnidade,
                    data: dtAgendamento
        }, function(dados){
            $scope.horarioOptions = dados;
            if ($scope.horarioOptions.length >= 1){
            $scope.agendamento.hrAgendamento = $scope.horarioOptions[0].id;
            $scope.setValidaHorarioAgendamento($scope.horarioOptions[0]);
            }
        });
    };


    var dateDisableCalendarioDeferred =  $q.defer();
    $scope.dateRangeCalendarioDisablePromise = dateDisableCalendarioDeferred.promise;

    $scope.dateRangeCalendarioDisabled = function(date, mode) {
        return ( mode === 'day' && date.getDay() === 0 );
    };

    $scope.checkEditRole = function(allowed) {
		var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
    	for ( role of userRoles ) {
    		if (allowed.indexOf(role) != -1) {
    			return false;
    		}
    	}
    	return true;
	};

    $scope.viewLastAgendamentoTelemarketing = $scope.checkEditRole($scope.rolesUtimoAgendamentoFaltoso);

    $scope.enableReagendamento = function() {
    	$scope.disableReagendamento = false;
    	$scope.disableDatePickerReagendamento = '';
        $scope.pushMessage("info","Reagendamento habilitado, escolha uma unidade, profissional, dia e horário de reagendamento abaixo.",10000);
    };

    $scope.monthChanged = function(month, year) {
        $scope.datasSemAtendimento = [];
        refreshDateRange();

        $scope.currentCalendarMonth = month;
        $scope.currentCalendarYear = year;
        $scope.datasSemAtendimento = servicesFactory.funcionariosDiaSAtendMes.query({
            id: $scope.agendamento.profissional.id,
            idServico: $scope.agendamento.servico.id,
            idEspecialidade: $scope.agendamento.especialidade.id,
            inTipoConsulta: $scope.agendamento.inTipoConsulta,
            idUnidade: $scope.agendamento.unidade.id,
            ano: year,
            mes: month,
            inTipo: $scope.agendamento.inTipo,
            idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
            tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C")
        }, function(){
            refreshDateRange();
        });
    };

    function refreshDateRange() {
        dateDisableDeferred.notify(new Date().getTime());
    };

    $scope.printPDFButton = "Imprimir PDF";

    $scope.printPDF = function() {

    	$scope.printPDFButton = "Aguarde carregando";


    	$http.get('/mediclab/agendamentos/'+ $scope.agendamento.id +'/despesa/pdf',{})
    	  .success(function (response) {

    		$scope.printPDFButton = "Imprimir PDF";
    		var blob = b64toBlob(response, 'application/pdf');
    		var fileURL = URL.createObjectURL(blob);
    	    $scope.pdf = $sce.trustAsResourceUrl(fileURL);

    	}).error(function(data, status, header, config) {
    		$scope.printPDFButton = "Falha ao Imprimir";
    	});
    };

    $scope.resetCombos = function() {

    	$scope.agendamento.unidade = null;
    	$scope.agendamento.especialidade = null;
        $scope.agendamento.subEspecialidade = null;
        $scope.agendamento.profissional = null;
        $scope.agendamento.servico = null;
        $scope.agendamento.inTipoConsulta = null;
        $scope.calendarioSemanal = [];
        $scope.agendamento.dtAgendamento = new Date();
        $scope.agendamento.hrAgendamento = null;

        $scope.especialidadeOptions = [];
        $scope.subEspecialidadeOptions = [];
        $scope.profissionaisOptions = [];
        $scope.servicoOptions = [];
        $scope.datasSemAtendimento = [];
        $scope.horarioOptions = [];
    }

    $scope.loadEspecialidades = function(item, clean) {
        clean = typeof clean !== 'undefined' ? clean : false;
    	if ( item && item.id )
            $scope.especialidadeOptions = servicesFactory.unidadesEspecialidades.get({ id: item.id }, function() {
                if ( $scope.agendamentoViaTelemarketing() ) {

                    $scope.agendamento.especialidade = {};
                    $scope.agendamento.especialidade.id = $stateParams.dadosPreAgendamento.obj.idEspecialidade;

                    for( var i=0; i < $scope.especialidadeOptions.length;i++) {
                        if( $scope.agendamento.especialidade.id == $scope.especialidadeOptions[i].id )
                            $scope.loadSubEspecialidades( $scope.especialidadeOptions[i] ,true);
                    }
                }
            });

        if (clean) {

	        $scope.agendamento.especialidade = null;
	        $scope.agendamento.subEspecialidade = null;
	        $scope.agendamento.profissional = null;
	        $scope.agendamento.servico = null;
	        $scope.calendarioSemanal = [];
	        $scope.agendamento.hrAgendamento = null;
	        $scope.subEspecialidadeOptions = [];
	        $scope.profissionaisOptions = [];
	        $scope.servicoOptions = [];
	        $scope.datasSemAtendimento = [];
            $scope.agendamento.inTipoConsulta = null;
            $scope.tipoConsultaOptions = null;

            if( !$scope.agendamentoViaTelemarketing() ) {
                $scope.agendamento.inTipoConsulta = null;
                $scope.agendamento.dtAgendamento = null;
                $scope.horarioOptions = [];
            }
        }
    };

    $scope.loadProfissionaisDoDia = function() {
    	$scope.profissionaisDoDia = servicesFactory.especialidadeProfissionaisDoDia.query( {
    		id: $scope.agendamento.especialidade.id,
            idunidade: $scope.agendamento.unidade.id,
            data: $scope.agendamento.dtAgendamento
        });
    }

    $scope.loadProfissionalReagendamento = function(item) {
    	$scope.profissionaisOptions = servicesFactory.especialidadeProfissionais.query({
            id: $scope.agendamento.especialidade.id,
            idunidade: item.id
        });

    	$scope.agendamento.profissional = null;
    	$scope.agendamento.inTipoConsulta = null;
    	$scope.calendarioSemanal = [];
        $scope.agendamento.hrAgendamento = null;
        $scope.datasSemAtendimento = [];
        $scope.horarioOptions = [];
    };

    $scope.loadSubEspecialidades = function(item, clean) {

        clean = typeof clean !== 'undefined' ? clean : false;

    	if (item && item.id) {

	        $scope.subEspecialidadeOptions = servicesFactory.subEspecialidades.get({ id: item.id });

	        $scope.profissionaisOptions = servicesFactory.especialidadeProfissionais.query({
	            id: item.id,
	            idunidade: $scope.agendamento.unidade.id
	        }, function() {

                if( $scope.agendamentoViaTelemarketing()  ) {

                    $scope.agendamento.profissional = {};

                    $scope.agendamento.profissional.id = $stateParams.dadosPreAgendamento.obj.idFuncionario;

                    for( var i=0; i < $scope.profissionaisOptions.length;i++) {
                        if ( $scope.agendamento.profissional.id ==  $scope.profissionaisOptions[i].id ) {
                            $scope.loadServicos($scope.profissionaisOptions[i],true);
                            break;
                        }
                    }
                }
                if ($scope.isreturn == false){
                    if ($scope.profissionaisOptions.length == 1){

                       $scope.agendamento.profissional = {};
                       $scope.agendamento.profissional.id = $scope.profissionaisOptions[0].id;
                       $scope.loadServicos($scope.profissionaisOptions[0],true);
                    }
                }
            });
    	}

        //clean up
    	if (clean) {

	        $scope.agendamento.subEspecialidade = null;
	        $scope.agendamento.profissional = null;
	        $scope.agendamento.servico = null;

	        $scope.calendarioSemanal = [];

	        $scope.agendamento.hrAgendamento = null;

	        $scope.servicoOptions = [];
	        $scope.datasSemAtendimento = [];

            if( !$scope.agendamentoViaTelemarketing() ) {
                $scope.agendamento.inTipoConsulta = null;
                $scope.agendamento.dtAgendamento = null;
                $scope.horarioOptions = [];
            }
    	}

    };

    $scope.refreshAgendamentos = function(todos) {
    	todos = typeof todos !== 'undefined' ? todos : false;
    	if ($scope.selectedCliente && $scope.selectedCliente.id) {
    		$scope.agendamentos = servicesFactory.clienteAgendamentos.get({id: $scope.selectedCliente.id, todos: todos }, function(){
                configurarTable( $scope.agendamentos );
		    });
    	}
    };

    $scope.loadServicos = function(item, clean, blink) {

    	clean = typeof clean !== 'undefined' ? clean : false;
    	blink = typeof blink !== 'undefined' ? blink : false;

    	if (blink) {
    		$('#pulsate-once-target' + item.id).pulsate({
                color: "#399bc3",
                repeat: false
            });
    	}

        if($scope.agendamento.servico != null && $scope.agendamento.servico.id != null && $scope.agendamento.id == null){
            $scope.agendamento.servico.id = null;
        }

    	if (item && item.id) {
    		// $scope.servicoOptions = servicesFactory.funcionarioServicos.get({ id: item.id });
            servicesFactory.funcionarioServicos.get({ id: item.id }, function(response) {
                $scope.servicoOptions = response;
                if(response && response.length == 1) {
                    $scope.agendamento.servico = {
                        id: response[0].servico.id
                    }
                    $scope.loadTipoConsulta(response[0]);
                }

                //refresh calendar if possible
                if($scope.agendamento.inTipoConsulta) {
                    $scope.datasSemAtendimento = [];
                    refreshDateRange();
                    $scope.datasSemAtendimento = servicesFactory.funcionariosDiaSAtendMes.query({
                        id: $scope.agendamento.profissional.id,
                        idServico: $scope.agendamento.servico.id,
                        idEspecialidade: $scope.agendamento.especialidade.id,
                        inTipoConsulta: $scope.agendamento.inTipoConsulta,
                        idUnidade: $scope.agendamento.unidade.id,
                        ano: $scope.currentCalendarYear,
                        mes: $scope.currentCalendarMonth,
                        idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
                        tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C")
                    }, function(){
                        refreshDateRange();
                    });
                } else

                if (clean) {
                    $scope.agendamento.hrAgendamento = null;
                    $scope.agendamento.inTipoConsulta = null;
                    $scope.tipoConsultaOptions = null;
                    if( !$scope.agendamentoViaTelemarketing() ) {
                        $scope.agendamento.inTipoConsulta = null;
                    }
                }
            });
        }
        // //refresh calendar if possible
        // if ($scope.agendamento.inTipoConsulta) {
        //     $scope.datasSemAtendimento = servicesFactory.funcionariosDiaSAtendMes.query({
        //         id: $scope.agendamento.profissional.id,
        //         idServico: $scope.agendamento.servico.id,
        //         idEspecialidade: $scope.agendamento.especialidade.id,
        //         inTipoConsulta: $scope.agendamento.inTipoConsulta,
        //         idUnidade: $scope.agendamento.unidade.id,
        //         ano: $scope.currentCalendarYear,
        //         mes: $scope.currentCalendarMonth,
        //         idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
        //         tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C")
        //     }, function(){
        //         refreshDateRange();
        //     });
        // }

        // if (clean) {
	       //  $scope.agendamento.hrAgendamento = null;
        //     $scope.agendamento.inTipoConsulta = null;
        //     $scope.tipoConsultaOptions = null;
        //     if( !$scope.agendamentoViaTelemarketing() ) {
        //         $scope.agendamento.inTipoConsulta = null;
        //     }
        // }
    };

    $scope.loadTipoConsulta = function(item, clean) {

        $scope.dataCobertura = null;

        if($scope.agendamento.inTipoConsulta != null){
            $scope.agendamento.inTipoConsulta = null;
        }

        servicesFactory.getTipoConsulta.query( {
            id: item.servico.id,
            idFuncionario: $scope.agendamento.profissional.id,
            idEspecialidade: $scope.agendamento.especialidade.id
        }, function(dados){

            $scope.tipoConsultaOptions = dados;
                if ($scope.tipoConsultaOptions.length == 1){

                    $scope.agendamento.inTipoConsulta = $scope.tipoConsultaOptions[0].id;
                    $scope.loadCalendarioSemanal($scope.tipoConsultaOptions[0], false, true);
                }else if (dados == null || dados.length == 0){
                    $scope.datasSemAtendimento = [];
                    refreshDateRange();
                    $scope.horarioOptions = [];
                    $scope.hrAgendamento = null;
                }
        });
        if ($scope.agendamento.contrato != null && $scope.agendamento.contrato.id != null){
            servicesFactory.getUltimoAgendamentoCobertoValido.query({
                id: item.servico.id,
                idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
                tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C"),
                idContrato: $scope.agendamento.contrato.id
            }, function(dados) {
                if(dados != null && dados.dataCoberturaFormatada != null){
                    $ngBootbox.confirm('Serviço dentro do prazo de validade contratual com o valor de <b>'+ dados.validadeContratual+' dias</b>. Última utilização realizada com a despesa <b>'+dados.idDespesa+'</b> no dia <b>'+ dados.dataCoberturaFormatada +'</b>. Próxima utilização para cobertura a partir do dia <b>'+dados.dataProximoServicoFormatada+'</b>.')
                            .then(function() {
                                $scope.dataCobertura = dados;
                        });
                }else {
                    $scope.dataCobertura = null;
                }
            });
        }

        if (clean) {
            $scope.agendamento.hrAgendamento = null;
        }
    };

    $scope.cleanTipoConsultaComboReagendamento = function() {
    	$scope.agendamento.inTipoConsulta = null;
    	$scope.calendarioSemanal = [];
        $scope.agendamento.hrAgendamento = null;
        $scope.datasSemAtendimento = [];
        $scope.horarioOptions = [];
    };

    $scope.loadCalendarioSemanal = function(item, cleanCalendar, cleanHour) {
    	cleanCalendar = typeof cleanCalendar !== 'undefined' ? cleanCalendar : false;
    	cleanHour = typeof cleanHour !== 'undefined' ? cleanHour : false;

        $scope.calendarioSemanal = servicesFactory.funcionarioCalendarioSemanal.query({
            id: $scope.agendamento.profissional.id,
            idServico: $scope.agendamento.servico.id,
            idEspecialidade: $scope.agendamento.especialidade.id,
            inTipoConsulta: item.id,
            idUnidade: $scope.agendamento.unidade.id
        });


        var idPaciente = ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id);
        var tipoPaciente = ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C");


        servicesFactory.getUltimoAgendamentoFaltoso.query({id: idPaciente, intipopaciente: tipoPaciente},
           function(date){
                $scope.ultimoAgendamentoFaltoso = (date != null && date[0] != null ? date[0] : null);
        });

        $scope.datasSemAtendimento = [];
        refreshDateRange();


        $scope.datasSemAtendimento = servicesFactory.funcionariosDiaSAtendMes.query({
            id: $scope.agendamento.profissional.id,
            idServico: $scope.agendamento.servico.id,
            idEspecialidade: $scope.agendamento.especialidade.id,
            inTipoConsulta: item.id,
            idUnidade: $scope.agendamento.unidade.id,
            ano: $scope.currentCalendarYear,
            mes: $scope.currentCalendarMonth,
            inTipo: $scope.agendamento.inTipo,
            idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
            tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C")
        }, function(){
            if($scope.agendamentoViaTelemarketing()) {
                var dt = moment($stateParams.dadosPreAgendamento.start);
                var result = $scope.datasSemAtendimento.indexOf(dt.format("DD/MM/YYYY")) === -1;
                if(result) {
                    $scope.agendamento.dtAgendamento = dt.toDate();
                } else {
                    if($scope.agendamento.dtAgendamento) {
                        var d = moment($scope.agendamento.dtAgendamento);
                        if(d.format("DD/MM/YYYY") == dt.format("DD/MM/YYYY")) {
                            toastr.warning('Data escolhida é inválida para este paciente', 'Atenção', {});
                            $scope.agendamento.dtAgendamento = undefined;
                            $scope.agendamento.hrAgendamento = undefined;
                            $scope.horarioOptions = null;

                        }
                    }
                }
            }
            refreshDateRange();
        });

        if ($scope.agendamento.dtAgendamento && $scope.agendamento.profissional.id && !$scope.agendamento.boSolicitouRetorno) {
            $scope.recuperarHorariosDisponiveis( $scope.agendamento.profissional.id, $scope.agendamento.especialidade.id,
                 item.id, $scope.agendamento.inTipo, $scope.agendamento.unidade.id,
                 $filter('date')($scope.agendamento.dtAgendamento, "dd/MM/yyyy") );
        }

        //clean up
        if (cleanHour) {
	        $scope.agendamento.hrAgendamento = null;
        }

        if (cleanCalendar) {
	        $scope.agendamento.dtAgendamento = null;
        }

    };

    $scope.loadCalendarioSemanalReagendamento = function(item) {

        $scope.calendarioSemanal = servicesFactory.funcionarioCalendarioSemanal.query({
            id: $scope.agendamento.profissional.id,
            idServico: $scope.agendamento.servico.id,
            idEspecialidade: $scope.agendamento.especialidade.id,
            inTipoConsulta: item.id,
            idUnidade: $scope.agendamento.unidade.id
        });

        $scope.datasSemAtendimento = [];
        refreshDateRange();

        $scope.datasSemAtendimento = servicesFactory.funcionariosDiaSAtendMes.query({
            id: $scope.agendamento.profissional.id,
            idServico: $scope.agendamento.servico.id,
            idEspecialidade: $scope.agendamento.especialidade.id,
            inTipoConsulta: item.id,
            idUnidade: $scope.agendamento.unidade.id,
            ano: $scope.currentCalendarYear,
            mes: $scope.currentCalendarMonth,
            idPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? $scope.agendamento.dependente.id : $scope.agendamento.cliente.id),
            tipoPaciente: ($scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? "D"  : "C")
        }, function(){
            refreshDateRange();
        });

        if ($scope.agendamento.dtAgendamento && $scope.agendamento.profissional.id) {

            $scope.recuperarHorariosDisponiveis( $scope.agendamento.profissional.id, $scope.agendamento.especialidade.id,
                item.id, $scope.agendamento.inTipo, $scope.agendamento.unidade.id,
                $filter('date')($scope.agendamento.dtAgendamentoFormatado, "dd/MM/yyyy") );

        }

        //clean up
        $scope.agendamento.hrAgendamento = null;

    };

    $scope.$watch('agendamento.dtAgendamento', function() {
        if ($scope.agendamento && $scope.agendamento.dtAgendamento && $scope.agendamento.profissional && ($scope.agendamento.inTipoConsulta >= 0) && $scope.rdTipoVisao == 0) {

            var idEspecialidade = (($scope.agendamento.especialidade) ? $scope.agendamento.especialidade.id : null);
            var idUnidade = (($scope.agendamento.unidade) ? $scope.agendamento.unidade.id : null);
            $scope.recuperarHorariosDisponiveis( $scope.agendamento.profissional.id, idEspecialidade,
                $scope.agendamento.inTipoConsulta, $scope.agendamento.inTipo, idUnidade,
                $filter('date')($scope.agendamento.dtAgendamento, "dd/MM/yyyy") );

        }


        if ($scope.agendamento && $scope.agendamento.dtAgendamento && $scope.rdTipoVisao == 1) {
        	$scope.agendamento.inTipoConsulta = null;
        	$scope.agendamento.profissional.id = null;
        	$scope.agendamento.servico.id = null;
        	$scope.agendamento.hrAgendamento = null;
        }

    });

    $scope.$watch('rdTipoPaciente', function() {
        $scope.msgUsaPlano = null;
        if ($scope.rdTipoPaciente != null) {
            if ($scope.rdTipoPaciente == 0) { //cliente paciente
                $scope.agendamento.cliente = Cliente.get({ id: $scope.selectedCliente.id });
                $scope.agendamento.paciente.nmPaciente = $scope.selectedCliente.nmCliente;
                $scope.agendamento.dependente = null;
                $scope.contratoDependenteOptions = null;

            } else if( $scope.rdTipoPaciente == 1 ) {

                $scope.dependenteOptions = $scope.todosDependentesCliente;

                if( $scope.dependenteOptions.length > 0 ) {

                        $scope.agendamento.dependente = {};

                        if( $scope.dependenteOptions.length == 1  ) {
                            $scope.agendamento.dependente.id = $scope.dependenteOptions[0].id;
                            $scope.changePacienteDependente($scope.dependenteOptions[0]);
                       } else
                            $scope.agendamento.dependente.id = null ;
                }
            }
        }

    });

    $scope.changePacienteDependente = function( obj ) {
        var item = JSON.parse(JSON.stringify(obj));
    	if (item) {
            $scope.msgUsaPlano = null;
    		$scope.agendamento.dependente = item;
    		$scope.contratoDependenteOptions = [];
            $scope.agendamento.contrato = {};

	        $scope.contratoDependenteOptions = servicesFactory.clienteContratosDependentes.get({id: $scope.selectedCliente.id, iddependente: item.id }, function() {

                if( $scope.contratoDependenteOptions.length == 1 &&
                     ( $scope.contratoDependenteOptions[0].contrato.boBloqueado != null &&
                        !$scope.contratoDependenteOptions[0].contrato.boBloqueado) ) {

                    if ($scope.contratoDependenteOptions[0].boFazUsoPlano == true || $scope.contratoDependenteOptions[0].boFazUsoPlano == null)
                        $scope.msgUsaPlano = true;
                    else
                        $scope.msgUsaPlano = false;

                    $scope.agendamento.contrato.id = $scope.contratoDependenteOptions[0].contrato.id;
                }
                else {
                    if ($scope.contratoDependenteOptions[0] == undefined || $scope.contratoDependenteOptions[0] == null)
                        $scope.msgUsaPlano = null;
                    else
                        $scope.msgUsaPlano = true;

                    $scope.agendamento.contrato.id = null;
                }
            });
    	}
    };

    
    $scope.statusPresente = function( agendamento ) {
        if(agendamento.boAtualizarFoto){

            $ngBootbox.alert('Paciente não possui foto cadastrada, atualize para continuar.')
                    .then(function() {

                 });
        }else{

        var idPacienteSelecionado = (agendamento.dependente != null && agendamento.dependente.id != null) ? agendamento.dependente.id:agendamento.cliente.id;
        var isNecessarioTriar = agendamento.servico.boTriagem != null ? agendamento.servico.boTriagem:false;
        var statusAgendamento = agendamento.inStatus;
        if( $scope.autorizadosPresencaAgendamento.length > 0 ) { 
            var boNaoEncontrado = true;
            for( var i=0; i < $scope.autorizadosPresencaAgendamento.length; i++ ) {
                if( idPacienteSelecionado == $scope.autorizadosPresencaAgendamento[i].idAutorizado ) {
                    if ( statusAgendamento == 1 && $scope.autorizadosPresencaAgendamento[i].tipoAcao == 'toPresenca' ) {
                        if( isNecessarioTriar ) {
                            $scope.registrarPresenca( agendamento, isNecessarioTriar, $scope.autorizadosPresencaAgendamento[i].prioridade, $scope.autorizadosPresencaAgendamento[i].tipoAcao);
                                /*$scope.autorizadosPresencaAgendamento[i].isPrioridade*/
                        } else {
                            var isNecessarioTriarPrimeiraAutenticacao = $scope.autorizadosPresencaAgendamento[i].servico.boTriagem != null ? $scope.autorizadosPresencaAgendamento[i].servico.boTriagem : false ;
                            if( isNecessarioTriarPrimeiraAutenticacao ) {
                                $scope.autorizadosPresencaAgendamento[i].servico.boTriagem = false;
                                //$scope.dialogoPrioridade(agendamento, isNecessarioTriar);
                                //$ngBootbox.customDialog($scope.opcoesDialogoPrioridade);            
                                $scope.loadModalPrioridade(agendamento, isNecessarioTriar);
                            } else {
                                $scope.registrarPresenca( agendamento, isNecessarioTriar, $scope.autorizadosPresencaAgendamento[i].prioridade );/*$scope.autorizadosPresencaAgendamento[i].isPrioridade*/
                            }
                        }
                    } else if ( statusAgendamento == 4 && $scope.autorizadosPresencaAgendamento[i].tipoAcao == 'toLiberado' ) {
                        $scope.registrarPresenca( agendamento, isNecessarioTriar, $scope.autorizadosPresencaAgendamento[i].prioridade );
                    } else {
                        var tipoAcao = $scope.definirTipoAcao( statusAgendamento );
                        $scope.prepararModalPresenca( agendamento, tipoAcao );
                    }

                    boNaoEncontrado = false;
                    break;
                }
            }

            if (boNaoEncontrado) {
                var tipoAcao = $scope.definirTipoAcao( statusAgendamento );
                $scope.prepararModalPresenca( agendamento, tipoAcao );
            }

        } else {
            var tipoAcao = $scope.definirTipoAcao( statusAgendamento );
            $scope.prepararModalPresenca( agendamento, tipoAcao );
       }
    }
    };
    //step2
    $scope.prepararModalPresenca = function( agendamento, tipoAcao ) {

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-presenca-modal.html',
            controller: 'ModalRegistraPresencaCtrl',
            resolve: {
                agendamento: function () {
                    return agendamento;
                },
                autorizadosPresencaAgendamento: function() {
                    return $scope.autorizadosPresencaAgendamento;
                },
                tipoAcao: function() {
                    return tipoAcao;
                },
                verificarEncaminhamento: function() {
                    return verificarEncaminhamento;
                },
                definirAtributosImpressao: function() {
                    return definirAtributosImpressao;
                },
                printPresencaReceipt: function() {
                    return $scope.printPresencaReceipt;
                }
            }
        });

        modalInstance.result.then(function (retorno) {
            $scope.autorizadosPresencaAgendamento = retorno;
            if (  $scope.autorizadosPresencaAgendamento[$scope.autorizadosPresencaAgendamento.length -1].registrado ) {
                $scope.refreshAgendamentos();
            }
        }, function() {});
    };

    $scope.registrarPresenca = function( agendamento ,isNecessarioTriar, prioridade, tipoAcao) {
        var prioridadeCliente, statusPrioridade, obsUrgencia;
        if(prioridade){
            if(prioridade.inTipoPrioridade){
                prioridadeCliente = parseInt(prioridade.inTipoPrioridade);
            }
            if(prioridade.statusPrioridade){
                statusPrioridade = prioridade.statusPrioridade;
            }
            if(prioridade.obsurgencia){
                obsUrgencia = prioridade.obsurgencia;
            }
           
        }else {
            prioridadeCliente = null;
            statusPrioridade = null;
            obsUrgencia = null;
        }
        servicesFactory.agendamentosStatusPresente.update(
            {id: agendamento.id,triado:isNecessarioTriar, prioridade: prioridadeCliente, bLogadoGerente:$rootScope.boLogadoGerente, statusPrioridade: prioridade.statusPrioridade, obsUrgencia: prioridade.obsurgencia },
            {},
            function(data) {
                if(data.boForaHorario != null && data.boForaHorario) {
                    $ngBootbox.alert('Não foi possível realizar a presença ! <br><br> Agendamento fora do horário de atendimento médico.').then(function() {});
                } else if(data.boForaPresenca != null && data.boForaPresenca) {
                    $rootScope.loginGerentePresenca();
                } else if(data.inStatus == 4) {
                    $rootScope.addDefaultTimeoutAlert("Presença", "registrada", "success");
                    toastr.success('', 'Aguarde a impressão do comprovante de presença', { timeout: 5000 });
                    $scope.printPresencaReceipt(agendamento, isNecessarioTriar, data.inTipoPrioridade);
                    Metronic.scrollTop();
                } else if (data.inStatus == 0) {
                    $ngBootbox.alert('Agendamento bloqueado Nº '+data.nrAgendamento+'. Verifique a situação do cliente..')
                    .then(function() {});
                } else if(data.inStatus == 1) {
                    $rootScope.addDefaultTimeoutAlert("Liberação", "registrada", "success");
                    Metronic.scrollTop();
                }
                $scope.refreshAgendamentos();

                var statusCallBackPresencaAgendamento = data.inStatus;

                if(statusCallBackPresencaAgendamento == 4 && tipoAcao  == 'toPresenca')
                    $ngBootbox.alert('Encaminhe o paciente para a triagem!').then(function() {});
            }, function(error){
                $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                $scope.refreshAgendamentos();
            }
        );
    };

    $scope.definirTipoAcao = function( statusAgendamento ) {
        var tipoAcao = null;

        if( statusAgendamento == 1) {
            tipoAcao = "toPresenca";
        } else if ( statusAgendamento == 4 ) {
            tipoAcao = "toLiberado";
        } else {
             tipoAcao = "";
        }

        return tipoAcao;

    };

    $scope.cleanHorario = function(dt) {
    	$scope.agendamento.hrAgendamento = null;
    }

    $scope.createDespesaAgendamento = function(agendamento) {
        $state.go("atendimentoCliente.cliente-despesas-agendamento-nova", { idAgendamento: agendamento.id});
    }

    $scope.ok = function (agendamento) {
        $state.go("atendimentoCliente.cliente-despesas-agendamento-nova", { idAgendamento: agendamento.id});
    };

    $scope.cancel = function () {
        $scope.modalInstance.dismiss('cancel');
    };

    $scope.toggleAnimation = function () {
      $scope.animationsEnabled = !$scope.animationsEnabled;
    };

    $scope.changedContrato = function(contrato) {
        $scope.selectedContrato = contrato;
    }

    $scope.saveAgendamento = function() {
        if ($('#formAgendamento').valid()) {
        	$scope.buttonDisabled = true;

            if($scope.selectedContrato 
                // pagamento por contra-cheque
                && $scope.selectedContrato.informaPagamento == 2 
                && (!$scope.contrachequeInfo || !$scope.contrachequeInfo["showed"]) ) {
                showContrachequeUpdateModal();
                return;
            }

            if($scope.agendamento.inTipo == 1 && !$scope.boAceitaEncaixe ) {
                showModalAutorizacaoEncaixe();
                return;
            }

            // encaixe
            if($scope.agendamento.inTipo == 1 ||
                ($scope.selectedContrato && !$scope.selectedContrato.plano) ) {
                persistAgendamento();
                return;
            }

            var rParams = {
                idPaciente: ($scope.rdTipoPaciente == "0" ? $scope.agendamento.cliente.id : $scope.agendamento.dependente.id),
                tipoPaciente: ($scope.rdTipoPaciente == "0" ? 1 : 2),
                data: moment($scope.agendamento.dtAgendamento).format("DD/MM/YYYY"),
                idEspecialidade: $scope.agendamento.especialidade.id,
                idServico: $scope.agendamento.servico.id
            }

            servicesFactory.countAgendamentosPaciente.get(
                rParams,
                function(response) {
                    if(response) {
                        if(response.quantidade && response.quantidade >= 2) {
                            var autoriz = isAutorizadoParaLimiteDiario(rParams.idPaciente, rParams.tipoPaciente)
                            // verifica autorização previa
                            if(autoriz && autoriz.loginGerente) {
                                $scope.agendamento.loginGerenteAutorizacao = autoriz.loginGerente;
                                $scope.agendamento.justificativaLoginGerente = autoriz.justificativa;
                                persistAgendamento();
                            } else {
                                // mostrar modal autorizacao
                                showModalAutorizacaoQuantidade(rParams.idPaciente, rParams.tipoPaciente);
                            }
                        } else {
                            persistAgendamento();
                        }
                    }
                }
            );
        }
    };

    function showContrachequeUpdateModal() {
        $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/modal-atualizacao-contracheque.html',
            controller: function($scope, $modalInstance) {
                $scope.contracheque = {};

                $scope.updateContracheque = function() {
                    $modalInstance.close($scope.contracheque);
                }

                $scope.continueWithoutUpdate = function() {
                    $modalInstance.dismiss('cancel');
                }
            }
        }).result.then(function (data) {
            $scope.contrachequeInfo = {
                showed: true,
                data: data || undefined
            }
            $scope.saveAgendamento();
        }, function() {
            $scope.contrachequeInfo = {
                showed: true,
                data: undefined
            }
            $scope.saveAgendamento();
        });
    }

    function showModalAutorizacaoQuantidade(idPaciente, tipoPaciente) {
        $rootScope.showMsgPresenca = false;
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
            controller: 'ModalLoginGerenteController',
            resolve: {
                exibeJustificativa: function () {
                  return true;
                },
                motivacaoAutorizacao: function () {
                  return 'Este paciente já possui 2 ou mais agendamentos na data selecionada. É preciso autorização gerencial para continuar.';
                },
                perfilNecessario: function () {
                    return "gerenteunidade";
                }
            }
        });

        modalInstance.result.then(function (data) {
            if(data && data.autorizado) {
                if(!isAutorizadoParaLimiteDiario(idPaciente, tipoPaciente)) {
                    addAutorizacaoLimiteDiario(idPaciente, tipoPaciente,
                        data.loginGerente, data.justificativa);
                }

                $scope.agendamento.loginGerenteAutorizacao = data.loginGerente;
                $scope.agendamento.justificativaLoginGerente = data.justificativa;
                persistAgendamento();
            } else {
                $scope.buttonDisabled = false;
            }
        }, function() {
            $scope.buttonDisabled = false;
        });
    }


    function showModalAutorizacaoEncaixe() {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
            controller: 'ModalLoginGerenteController',
            resolve: {
                exibeJustificativa: function () {
                  return true;
                },
                motivacaoAutorizacao: function () {
                  return 'É necessário a autorização do gerente para encaixe neste horário.';
                },
                perfilNecessario: function () {
                    return "gerenteunidade";
                }
            }
        });

        modalInstance.result.then(function (data) {
            if(data && data.autorizado) {
                persistAgendamento();
            } else {
                $scope.buttonDisabled = false;
            }
        }, function() {
            $scope.buttonDisabled = false;
        });
    }

    function registerContrachequeInfo(idAgendamento) {
        servicesFactory.registerContrachequeInfo.put(
            { idAgendamento: idAgendamento },
            $scope.contrachequeInfo.data,
            function(response) {
                $scope.contrachequeInfo["data"] = undefined;
            },
            function(err) {
                $scope.contrachequeInfo["data"] = undefined;
            }
        );
    }

    function persistAgendamento() {
        $scope.agendamento.$save(function (object, responseHeaders) {

            if(null != $scope.agendamento.servico.nmPreparatorio 
                && "" != $scope.agendamento.servico.nmPreparatorio) {
                openModalPreparo([$scope.agendamento.servico]);
            }
            if (object.id != null) {
                if($scope.contrachequeInfo && $scope.contrachequeInfo["data"]) {
                    registerContrachequeInfo(object.id);
                }
                $scope.pushMessage("warning","Informe ao cliente comparecer entre "+object.hrInicio+" e "+object.hrFim,15000);
                if(!$scope.agendamentoViaTelemarketing()) {
                    $scope.printerAgendamento(object);
                }
                $state.go( "atendimentoCliente.agendamento" );
            }
        }, function(error) {
            $scope.buttonDisabled = false;
        });
    }

    function sendEmailContrachequeInfo() {

    }

    $scope.getPreparatorio = function(){
        if($scope.agendamento && $scope.agendamento.servico)
        {
            openModalPreparo([$scope.agendamento.servico]);
        }
    }

    function openModalPreparo(lServicos)
    {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/atendimento/show-preparatorios.html',
            controller: 'ShowPreparatoriosController',
            size: "lg",
            resolve: {
                lServicos: function () {
                    return lServicos;
                }
            }
        });

        modalInstance.result.then(function (retorno)
        {
        }, function ()
        {
        });
    };

    function isAutorizadoParaLimiteDiario(idPaciente, tipoPaciente) {
        var list = $scope.$parent.$parent.autorizadosLimiteDiario;
        if(!list) return ;
        for(var i = 0; i < list.length; ++i) {
            if(list[i].idPaciente == idPaciente && list[i].tipoPaciente == tipoPaciente) {
                return list[i];
            }
        }
    }

    function addAutorizacaoLimiteDiario(idPaciente, tipoPaciente, login, justificativa) {
        if(!$scope.$parent.$parent.autorizadosLimiteDiario) {
            $scope.$parent.$parent.autorizadosLimiteDiario = [];
        }
        var list = $scope.$parent.$parent.autorizadosLimiteDiario;
        var dt = {
            idPaciente: idPaciente,
            tipoPaciente: tipoPaciente,
            loginGerente: login,
            justificativa: justificativa
        }
        if(list.indexOf(dt) == -1) {
            list.push(dt);
        }
    }

    $scope.printerAgendamento2via = function () {
        $scope.printerAgendamento($scope.agendamento);
    }

    $scope.printerAgendamento = function (agendamento) {
        var socket = io.connect('http://localhost:9092');

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });

        var jsonObject = {
            idAgendamento: agendamento.idAgendamento,
            nmUnidade: agendamento.unidade.nmApelido,
            nmPaciente: agendamento.dependente != null ? agendamento.dependente.nmDependente : agendamento.cliente.nmCliente,
            nmEspecialidade: agendamento.especialidade.nmEspecialidade,
            nmMedico: agendamento.profissional.nmFuncionario,
            nmServico: agendamento.servico.nmServico,
            nmSala: agendamento.consultorio.nmConsultorio,
            nmTriagem: 'Não necessária',
            nmPrioridade: agendamento.nmPrioridade,
            idade: agendamento.dependente != null ? agendamento.dependente.idade : agendamento.cliente.idade,
            dtAgendamento: agendamento.dtAgendamentoFormatado,
            hrAgendamento: agendamento.hrInicio,
            hrFim: agendamento.hrFim,
            nmEndereco: agendamento.unidade.nmLogradouro+", "+agendamento.unidade.nrNumero+" - "+agendamento.unidade.bairro.nmBairro,
            codTitular: agendamento.cliente.nrCodCliente,
            nmDescricao: agendamento.servico.nmPreparatorio
        };

        $rootScope.digitalFotoClienteMsg = "Processando...";
        $rootScope.digitalCapturadaMsg = undefined;

        socket.emit('printerAgendamento', jsonObject);

        socket.on('printerAgendamento', function(data) {

            $rootScope.digitalCapturadaMsg = data;
            var typeEvent;

            if(data != undefined
                && (data == 'Agendamento impresso!')){

                $rootScope.addDefaultTimeoutAlert("Agendamento ", "impresso", "success");
                typeEvent = 'success';
                socket.emit('disconnect');
                socket.removeAllListeners();
                //Cliente.get({id: items.id})

            } else {

                typeEvent = 'danger';
                $rootScope.alerts.push({
                    type: typeEvent,
                    msg: data,
                    timeout: 10000
                });
            }

            setLabels();
            Metronic.scrollTop();
            $modalInstance.close();
        });
    };

    //------------------------------------------------------------------------------
    // funcoes para impressao do comprovante de presenca
    function verificarEncaminhamento(agendamento) {
        if(agendamento.despesa != null ) {
            if(agendamento.despesa.despesaServicos[0] != null) {
                if(agendamento.despesa.despesaServicos[0].encaminhamento != null) {
                    if(agendamento.despesa.despesaServicos[0].encaminhamento.nrEncaminhamento != null) {
                        return agendamento.despesa.despesaServicos[0].encaminhamento.nrEncaminhamento;
                    }
                }
            }
        }
        $scope.boSemEncaminhamento = true;
    }

    $scope.getAgendamentoView = function(agendamentoId) {
        if($scope.agendamentoView) {
            toastr.success('', 'Aguarde a impressão do comprovante de presença', {timeout: 5000});
            $scope.printPresencaReceipt($scope.agendamentoView);
            return;
        }
        var rParams = {
            idAgendamento: agendamentoId
        }
        servicesFactory.getAgendamentoView.get(
            rParams,
            function(result) {
                if(result) {
                    $scope.agendamentoView = result;
                    toastr.success('', 'Aguarde a impressão do comprovante de presença', {timeout: 5000});
                    $scope.printPresencaReceipt($scope.agendamentoView);
                } else {
                    toastr.error('', 'Houve um erro ao consultar dados da 2ª via');
                }
            },
            function(err) {
                toastr.error('', 'Houve um erro ao consultar dados da 2ª via');
            }
        );
    }

    function definirAtributosImpressao(agendamento, isNecessarioTriar, prioridade) {

        var comprovanteImpressao = {};
        var textTriagem = "Procure a triagem do térreo.";
        comprovanteImpressao.idAgendamento = agendamento.id;
        comprovanteImpressao.nmUnidade = agendamento.unidade.nmUnidade;
        comprovanteImpressao.nmPaciente =  agendamento.dependente != null ? agendamento.dependente.nmDependente : agendamento.cliente.nmCliente;
        comprovanteImpressao.nmEspecialidade = agendamento.especialidade.nmEspecialidade;
        comprovanteImpressao.nmMedico = agendamento.profissional.nmFuncionario;
        comprovanteImpressao.nmServico = agendamento.servico.nmServico;
        comprovanteImpressao.nmSala = agendamento.localizacao;
        comprovanteImpressao.nmTriagem = (isNecessarioTriar || agendamento.inTipoPrioridade > 0 ? textTriagem : "Não necessária"); // confirmar 
        comprovanteImpressao.nmTitular = agendamento.cliente.nmCliente;
        comprovanteImpressao.nmRetorno = agendamento.retornoFormatado;
        comprovanteImpressao.nrEncaminhamento = verificarEncaminhamento(agendamento);
        comprovanteImpressao.nmPrioridade = (prioridade || agendamento.boPrioridade ? "SIM" : "NÃO"); //confirmar
        comprovanteImpressao.idade = agendamento.dependente != null ? agendamento.dependente.idade : agendamento.cliente.idade;

        return comprovanteImpressao;
    }

    $scope.printPresencaReceipt = function(agendamento, isNecessarioTriar, prioridade) {
        
        var socket = io.connect('http://localhost:9092');
        socket.on('connect', function() {
        });

        socket.on('disconnect', function() {


        });
        var jsonObject = definirAtributosImpressao(agendamento, isNecessarioTriar, prioridade);

        socket.emit('printPresencaReceipt', jsonObject);

        socket.on('printPresencaReceipt', function() {

        });
    }
    //------------------------------------------------------------------------------

    $scope.updateAgendamento = function(){
        if ($('#formAgendamento').valid()) {
        	$scope.buttonDisabled = true;

        	if ($scope.agendamento.id) {

	        	Agendamento.update( { id: $scope.agendamento.id }, $scope.agendamento, function(data){
		    		$rootScope.addDefaultTimeoutAlert("Agendamento", "atualizado", "success");
		    		$state.go("atendimentoCliente.agendamento");
                    $scope.printerAgendamento(data);
		    	}, function(error){
		    		$scope.buttonDisabled = false;
		    		$rootScope.addDefaultTimeoutAlert("Agendamento", "atualizar", "error");
		    	});
		    	Metronic.scrollTop();
        	} else {

        		$scope.saveAgendamento();
        	}

        }
    };

    if($state.is('atendimentoCliente.agendamento')){
    	$scope.refreshAgendamentos();
    };

    $scope.agendamentoViaTelemarketing = function() {

        var boAgendamentoViaTelemarketing = false;

        if( $stateParams.dadosPreAgendamento != null )
            boAgendamentoViaTelemarketing = true;

        return boAgendamentoViaTelemarketing;

    };

    $scope.loadDependentesClienteLocal = function() {
        $scope.todosDependentesCliente =  null;
        $scope.todosDependentesCliente = servicesFactory.clienteDependentes
        .get({id: $scope.selectedCliente.id}, function() {});
    };


    if($state.is('atendimentoCliente.cliente-agendamento-novo')){

        $scope.rdTipoPaciente = 0; //titular padrao
        $scope.rdTipoVisao = 0;
        $scope.profissionaisDoDia = [];
        $scope.agendamento = new Agendamento({});
        $scope.boAgendamentoNovo = true;

        $scope.agendamento.paciente = {};
        $scope.agendamento.unidade = {};
        $scope.agendamento.especialidade = {};
        $scope.agendamento.inTipo = 0; //agendamento
        $scope.agendamento.inTipoAtendimento = 1; //procedimento
        $scope.agendamento.inTipoAgendamento = 0 //medico
        $scope.agendamento.inStatus = 0; //bloqueado
        $scope.agendamento.inTurno = 0; //manha
        $scope.agendamento.dtAgendamento = null;
        $scope.agendamento.boRetorno = false;
        $scope.agendamento.boReagendamento = false;
        $scope.agendamento.boRemanejamento = false;
        $scope.agendamento.boRetornoGerado = false;
        $scope.agendamento.boDespesaGerada = false;

        $scope.agendamento.boSolicitouRetorno = false;
        $scope.agendamento.boExames = false;
        $scope.agendamento.boServicoRealizado = false;
        $scope.agendamento.inTipoConsulta = null;
        $scope.agendamento.idAgendamentoPai = null;

        $scope.agendamento.contrato = {};

        if ($scope.selectedCliente && $scope.selectedCliente.id) {

            $scope.agendamento.cliente = Cliente.get({ id: $scope.selectedCliente.id });

            $scope.agendamento.paciente.nmPaciente = $scope.selectedCliente.nmCliente;

            $scope.contratoOptions = servicesFactory.clienteContratos.get({id: $scope.selectedCliente.id }, function(){
                if($scope.contratoOptions.length == 1 &&
                    ($scope.contratoOptions[0].boBloqueado != null && !$scope.contratoOptions[0].boBloqueado)) {

                    $scope.agendamento.contrato.id =  $scope.contratoOptions[0].id;
                    $scope.selectedContrato = $scope.contratoOptions[0];
                } else {
                    $scope.agendamento.contrato.id = null;
                    $scope.selectedContrato = null;
                }
            });

            $scope.loadDependentesClienteLocal();

            //$scope.tipoConsultaOptions = [
              //  { id: 0,    description: 'Hora Marcada' },
             //   { id: 1,    description: 'Ordem de Chegada' }
           // ];

            if(  $scope.agendamentoViaTelemarketing() ) {
                // esperar escolher serviço para definir data de atendimento
                $scope.agendamento.dtAgendamento = moment($stateParams.dadosPreAgendamento.start).toDate();
                //$scope.agendamento.inTipoConsulta = $scope.tipoConsultaOptions[0].id;

                /*$scope.recuperarHorariosDisponiveis( $stateParams.dadosPreAgendamento.obj.idFuncionario,
                    $stateParams.dadosPreAgendamento.obj.idEspecialidade, $scope.agendamento.inTipoConsulta,
                    $scope.agendamento.inTipo, $stateParams.dadosPreAgendamento.obj.idUnidade,
                    $filter('date')($scope.agendamento.dtAgendamento, "dd/MM/yyyy") );*/
            }
            defineUnidadeFuncionario();
        }
    };

    if($state.is('atendimentoCliente.cliente-agendamento-editar')){
        $scope.isreturn = true;
       $scope.boAgendamentoNovo = false;
        $scope.origemBoParticular = {};
    	if ($stateParams.id) {
	        $scope.tipoConsultaOptions = [
	            { id: 0,    description: 'Hora Marcada' },
	            { id: 1,    description: 'Ordem de Chegada' }
	        ];

            $scope.rdTipoVisao = 0;
	        $scope.unidadeOptions = servicesFactory.unidades.query();

	         Agendamento.get({ id: $stateParams.id }, function(agendamento) {
				$scope.agendamento = agendamento;
				$scope.origemBoParticular = agendamento.boParticular == null ? false : agendamento.boParticular;
	            $scope.despesa = servicesFactory.agendamentosDespesa.get({ id: $scope.agendamento.id });

	            $scope.loadEspecialidades($scope.agendamento.unidade);

	            $scope.loadSubEspecialidades($scope.agendamento.especialidade);

	            $scope.loadServicos($scope.agendamento.profissional);

	            $scope.loadCalendarioSemanal({ id: $scope.agendamento.inTipoConsulta });

                var dayAgendamento = moment(agendamento.dtAgendamento);
                var today = moment(new Date());
                var diff = today.diff(dayAgendamento, 'days');

                $scope.disableReagendamentoButton = (!$scope.checkEditRole($scope.encaixeRoles) && (diff <= 0 ) && agendamento.inTipo != 1 && (agendamento.inStatus == 1 || agendamento.inStatus == 0) ) ? false : true;
                $scope.validDivergenciaParticular();
	        });
    	}

    };

    if($state.is('atendimentoCliente.cliente-agendamento-retorno')){
        $scope.isreturn = true;
        $scope.boRetornoParticular = true;
        $scope.boAgendamentoNovo = false;
        $scope.origemBoParticular = {};
    	if ($stateParams.id) {
	        $scope.tipoConsultaOptions = [
	            { id: 0,    description: 'Hora Marcada' },
	            { id: 1,    description: 'Ordem de Chegada' }
	        ];
            $scope.rdTipoVisao = 0;

	        $scope.unidadeOptions = servicesFactory.unidades.query();

	        $scope.agendamento = Agendamento.get({ id: $stateParams.id }, function() {

	            $scope.despesa = servicesFactory.agendamentosDespesa.get({ id: $scope.agendamento.id });
	            $scope.origemBoParticular = $scope.agendamento.boParticular == null ? false : $scope.agendamento.boParticular;
                //$scope.agendamento.dtAgendamento = ($filter('date')($scope.agendamento.dtAgendamentoFormatado,'yyyy-MM-dd'));

	            $scope.loadEspecialidades($scope.agendamento.unidade);

	            $scope.loadSubEspecialidades($scope.agendamento.especialidade);

	            $scope.loadServicos($scope.agendamento.profissional);

	            $scope.loadCalendarioSemanal({ id: $scope.agendamento.inTipoConsulta });

	            $scope.disableReagendamentoButton = true;

                $scope.rdTipoPaciente = $scope.agendamento.dependente != null && $scope.agendamento.dependente.id != null ? 1 : 0;


	            $scope.agendamento.idAgendamentoAux = $scope.agendamento.id;

	            $scope.disableReagendamento = false;
	        	$scope.disableDatePickerReagendamento = '';

                $scope.agendamento.idAgendamentoPai = $scope.agendamento.id;

	        	$scope.agendamento.id = null;

	        	$scope.agendamento.boRetorno = true;
                $scope.agendamento.inTipo = 0;
	        	$scope.agendamento.nrAgendamento = null;

	            $scope.agendamento.inStatus = 0; //bloqueado
	            $scope.agendamento.inTurno = 0; //manha
	            $scope.agendamento.dtAgendamento = null;

	            $scope.agendamento.boReagendamento = false;
	            $scope.agendamento.boRemanejamento = false;
	            $scope.agendamento.boRetornoGerado = false;
	            $scope.agendamento.boDespesaGerada = false;
	            $scope.agendamento.boSolicitouRetorno = false;
	            $scope.agendamento.boExames = false;
	            $scope.agendamento.boServicoRealizado = false;
                $scope.agendamento.boParticular = false;

	            $.uniform.update();
	        });
    	}

    };

    function convertDateStringsToDates(input) {
        // Ignore things that aren't objects.
    	var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;
    	var data = null;
        // Check for string properties which look like dates.
        if (typeof input === "string" && (input.match(regexIso8601))) {
            var milliseconds = Date.parse(input)
            if (!isNaN(milliseconds)) {
            	data = new Date(milliseconds);
            }
        }
        return data;
    };

    $scope.openModalDespesaGerada = function (agendamento) {

        $scope.animationsEnabled = true;

        if ((agendamento.boDespesaExc
                || (agendamento.boDespesaExc == null))
                && (agendamento.boDespesaGerada == null
                    || agendamento.boDespesaGerada == false)){
            $state.go("atendimentoCliente.cliente-despesas-agendamento-nova", { idAgendamento: agendamento.id});
        } else {

            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'despesaModalContent.html',
                controller: 'ModalDespesaGeradaCtrl',
                resolve: {
                            items: function () {
                              return agendamento;
                            }
                        }
            });

            modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                }, function() {});
        }
    };

});

//step3
MedicsystemApp.controller('ModalRegistraPresencaCtrl', function ($rootScope,
    $scope, $state, $modalInstance, $modal, toastr, $window, $ngBootbox,
    servicesFactory, agendamento, autorizadosPresencaAgendamento, tipoAcao,
    verificarEncaminhamento, definirAtributosImpressao, printPresencaReceipt) {

    var Cliente = servicesFactory.clientes;
    var Dependente = servicesFactory.dependentes;

    //------------------------------------------------------------------
    // funcoes para impressao
    $scope.definirAtributosImpressao = definirAtributosImpressao;
    $scope.verificarEncaminhamento = verificarEncaminhamento;
    $scope.printPresencaReceipt = printPresencaReceipt;
    //------------------------------------------------------------------

    $scope.validCliente = false;
    $scope.validDependente = false;
    $scope.cliente = {};
    $scope.dependente = {};
	$scope.clienteFound = false;
    $scope.bloqueioBotao = false;
	$rootScope.clienteDigital = { message: "Clique para ler digital" };

    var idPacienteSelecionado;
    if (agendamento.dependente != null && agendamento.dependente.id != null){
        idPacienteSelecionado = agendamento.dependente.id;
    } else {
        idPacienteSelecionado = agendamento.cliente.id;
    }

    var dadosAutenticacao = {idAutorizado:idPacienteSelecionado,servico:agendamento.servico, tipoAcao: tipoAcao ,boAutorizado:false};

	$scope.lerBiometria = function() {

    var socket = io.connect('http://localhost:9092');
       var jsonObject = {};

       $rootScope.clienteDigital.message = "Aguardando digital...";

	   socket.on('connect', function() {});
	   socket.on('disconnect', function() {});

        if ( agendamento.dependente != null && agendamento.dependente.id != null ) {
            if ( agendamento.dependente.digitalDependente == null ) {
                $scope.pushMessage("danger","Dependente não possui digital cadastrada.",5000);
                return;
            } else {
                jsonObject = { id: agendamento.dependente.id, digitalCliente: agendamento.dependente.digitalDependente};
            }
        } else {

            if ( agendamento.cliente.digitalCliente == null ) {
                $scope.pushMessage("danger","Cliente não possui digital cadastrada.",5000);
                return;
            } else {
                jsonObject = {id: agendamento.cliente.id, digitalCliente: agendamento.cliente.digitalCliente};

            }
        }

	    socket.emit('biometriapresentevent',jsonObject);

	    socket.on('biometriapresentevent', function(data) {

	       $scope.$apply(function() {
		      if ( !isNaN(data) ) {
                // Dependentes
                if (agendamento.dependente != null){
                    if (data == agendamento.dependente.id){
                        $rootScope.clienteDigital.message = "Dependente identificado";
                        $scope.dependente = Dependente.get({ id: data }, function(){
                            $scope.clienteFound = true;
                            dadosAutenticacao.boAutorizado = true;
                            $scope.validDependente = true;
                        });
                    }
                    else {
                        $scope.clienteFound = false;
                        $rootScope.clienteDigital.message = "Dependente identificado, mas não corresponde ao dependente do agendamento - Ler Novamente";
                        dadosAutenticacao.boAutorizado = false;
                    }
                }
                // CLientes (titulares)
                else {
		        	if ( data == agendamento.cliente.id ) {
		        		$rootScope.clienteDigital.message = "Cliente identificado";
		        		$scope.cliente = Cliente.get({ id: data }, function(){
		        			$scope.clienteFound = true;
                            dadosAutenticacao.boAutorizado = true;
                            $scope.validCliente = true;
		        		});
		        	}
                    else {
		        		$scope.clienteFound = false;
		        		$rootScope.clienteDigital.message = "Cliente identificado, mas não corresponde ao cliente do agendamento - Ler Novamente";
                        dadosAutenticacao.boAutorizado = false;
                    }
    		    }
              }
                else {
    		        $scope.clienteFound = false;
    		      	$rootScope.clienteDigital.message = "Digital invalida - Ler Novamente";
                    dadosAutenticacao.boAutorizado = false;
                }
	    	});
	    });
      /* $scope.cliente = Cliente.get({ id: 216411 }, function(){
                            $scope.clienteFound = true;
                            dadosAutenticacao.boAutorizado = true;
                        });*/
	}

	$scope.registrar = function () {
        if ($scope.dependente != null && $scope.dependente.id != null){
                $scope.bloqueioBotao = true;
                Dependente.update( { id: $scope.dependente.id }, $scope.dependente, function(data) {
                $scope.abrirDialogoPrioridade();
                }, function(error){
                    $rootScope.addDefaultTimeoutAlert("Dependente", "atualizar", "error");
                    $scope.bloqueioBotao = false;
                    dadosAutenticacao.boAutorizado = false;
                });
            }

		else if ( $scope.cliente.id ) {
            $scope.bloqueioBotao = true;
            Cliente.update( { id: $scope.cliente.id }, $scope.cliente, function(data) {
                $scope.abrirDialogoPrioridade();
	    	}, function(error){
	    		$rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                $scope.bloqueioBotao = false;
                dadosAutenticacao.boAutorizado = false;
	    	});
		}
    };

    $scope.registrarPresenca = function( prioridade,isNecessarioTriar) {
        servicesFactory.agendamentosStatusPresente.update(
            {id: agendamento.id,triado:isNecessarioTriar,prioridade: prioridade.inTipoPrioridade, boLogadoGerente: $rootScope.boLogadoGerente, 
                statusPrioridade: prioridade.statusPrioridade, obsUrgencia: prioridade.obsurgencia }, {}, function(data) {

            agendamento.inTipoPrioridade = data.inTipoPrioridade;
            agendamento.inStatusPrioridade = data.inStatusPrioridade;
            agendamento.obsUrgencia = data.obsUrgencia;
            if(data.boForaHorario != null && data.boForaHorario) {
                $ngBootbox.alert('Não foi possível realizar a presença ! <br><br> Agendamento fora do horário de atendimento médico.').then(function() {});
            } else if(data.boForaPresenca != null && data.boForaPresenca) {
                $rootScope.loginGerentePresenca();
            } else if(data.inStatus == 4) { //presente
                $rootScope.addDefaultTimeoutAlert("Presença", "registrada", "success");
                toastr.success('', 'Aguarde a impressão do comprovante de presença', {timeout: 5000});
                $scope.printPresencaReceipt(agendamento, isNecessarioTriar, data.inTipoPrioridade);//mudar para isPrioridade e mudar para nova situacao
                Metronic.scrollTop();
            } else if(data.inStatus == 0){
                $ngBootbox.alert('Agendamento bloqueado Nº '+data.nrAgendamento+'. Verifique a situação do cliente..')
                .then(function() {});
            } else if(data.inStatus == 1) {
                $rootScope.addDefaultTimeoutAlert("Liberação", "registrada", "success");
                Metronic.scrollTop();
            }

            var boJaAutenticado = false;
            dadosAutenticacao.registrado = true;
            dadosAutenticacao.prioridade = prioridade;
            dadosAutenticacao.isNecessarioTriar = isNecessarioTriar;

            for(var i=0; i<autorizadosPresencaAgendamento.length > 0;i++) {
                if( autorizadosPresencaAgendamento[i].idAutorizado == dadosAutenticacao.idAutorizado  ) {
                    autorizadosPresencaAgendamento[i] = dadosAutenticacao;
                    boJaAutenticado = true; break;
                }
            }

            if (!boJaAutenticado)
               autorizadosPresencaAgendamento.push(dadosAutenticacao);

            $modalInstance.close(autorizadosPresencaAgendamento);
            $scope.bloqueioBotao = false;

          /*  if( isNecessarioTriar && dadosAutenticacao.tipoAcao == 'toPresenca' && data.inStatus == 4)
                $ngBootbox.alert('Encaminhe o paciente para a triagem!').then(function() {console.log('Alert closed');}); */

        },function(error){
             dadosAutenticacao.boAutorizado = false;
        });
    };

    $scope.abrirDialogoPrioridade = function() {
        $scope.isNecessarioTriar =  agendamento.servico.boTriagem != null ? agendamento.servico.boTriagem : false;
         //$scope.isNecessarioTriar = false;
        if( ! $scope.isNecessarioTriar ) {
            if( dadosAutenticacao.tipoAcao == 'toLiberado' ) {
                var isPrioridade = false;
                $scope.registrarPresenca(isPrioridade,$scope.isNecessarioTriar);
            }
            else if (  dadosAutenticacao.tipoAcao == 'toPresenca'  ){
                //$ngBootbox.customDialog($scope.opcoesDialogoPrioridade);
                loadModalPrioridade(agendamento, $scope.isNecessarioTriar);
            }else {
                $scope.registrarPresenca(agendamento.inTipoPrioridade,$scope.isNecessarioTriar);
            }
        } else {
            loadModalPrioridade(agendamento,$scope.isNecessarioTriar);
            //$ngBootbox.customDialog($scope.opcoesDialogoPrioridadeParaTriagem);
        }
    };

    function loadModalPrioridade (agendamento,isNecessarioTriar){

        var modalInstance = $modal.open({
            animation: true,
            backdrop: 'static',
            templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-dialogo-prioridade.html',
            controller: 'modalPrioridade',
            size: 'md',
            resolve:{
                agendamento: function(){
                    return agendamento;
                },
                isNecessarioTriar: function(){
                    return $scope.isNecessarioTriar;
                }
            }
        });

        modalInstance.result.then(function(prioridade){
            $scope.registrarPresenca(prioridade, isNecessarioTriar); //mudar
        })

    }

    $scope.pushMessage = function( typeMessage, textMessage, timeMessage ) {
        $rootScope.alerts.push({
            type: typeMessage,
            msg: textMessage,
            timeout: timeMessage });
        Metronic.scrollTop();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.removerPorAtributo = function(array, attr, value) {
        var i = array.length;

        while(i--){
            if( array[i] && array[i].hasOwnProperty(attr)
                && (arguments.length > 2 && array[i][attr] === value ) ){
                array.splice(i,1);
           }
        }
        return array;
    };

    $scope.identificarClienteSemBiometria = function() {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
            controller: 'ModalLoginGerenteController',
            resolve: {
                exibeJustificativa: function () {
                  return false;
                },
                motivacaoAutorizacao: function () {
                  return null;
                },
                perfilNecessario: function () {
                    return null;
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (data.autorizado) {
                $rootScope.boLogadoGerente = true;
                if (agendamento.dependente != null){
                    $scope.dependente = Dependente.get({ id: agendamento.dependente.id });
                    $scope.validDependente = true;
                } else {
                    $scope.cliente = Cliente.get({ id: agendamento.cliente.id });
                    $scope.validCliente = true;
                }
            $scope.clienteFound = true;
            }
        });
    };

    $rootScope.loginGerentePresenca = function  () {
        $rootScope.showMsgPresenca = true;

        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'templates/admin4_material_design/angularjs/views/modal-logingerente.html',
            controller: 'ModalLoginGerenteController',
            resolve: {
                exibeJustificativa: function () {
                  return false;
                },
                motivacaoAutorizacao: function () {
                  return null;
                },
                perfilNecessario: function () {
                    return "gerenteunidade";
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (data.autorizado) {
               $rootScope.boLogadoGerente = true;
               $rootScope.showMsgPresenca = false;
            }else{
                $rootScope.boLogadoGerente = false;
                $rootScope.showMsgPresenca = false;
            }
        });
    };
});
//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalDespesaGeradaCtrl', function ($scope, $state, $modalInstance, items) {

  $scope.ok = function () {
    $modalInstance.close(
            $state.go("atendimentoCliente.cliente-despesas-agendamento-nova", { idAgendamento: items.id})
            );
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


//Controller prioridade recepção
MedicsystemApp.controller('modalPrioridade', function($scope, $state, $modalInstance, agendamento, isNecessarioTriar){
    
    $scope.inicioModal = function(){
        $scope.prioridaderec = {}
        $scope.showInfoTriagem = false;
        if(isNecessarioTriar == true){
            $scope.showInfoTriagem = true;
        }else{
            $scope.showInfoTriagem = false;
        }
    };
    $scope.finalizar = function(prioridaderec){
        prioridaderec.isNecessarioTriar = isNecessarioTriar;
        $modalInstance.close(prioridaderec);
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.inicioModal();
});