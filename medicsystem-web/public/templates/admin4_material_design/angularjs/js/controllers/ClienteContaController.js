'use strict';

MedicsystemApp.controller('ClienteContaController', function($rootScope, $scope, $http, $timeout, $state, $stateParams, $modal, servicesFactory, Auth, $document ) {
    // inicializacao controllers

    /* ================= INICIADORES ====================*/

    var Cliente = servicesFactory.clientes;
    var Dependente = servicesFactory.dependentes;
    var Informativo = servicesFactory.informativo;
    var InformativoInfo = servicesFactory.informativoinfo;

    function loadDependenteProperties() {
        $scope.dependenteSelected = [];
        $scope.isOpen  = true;
        $scope.qtdcheck = 0;
        $scope.botaoEnviar = false;
        $scope.parentescoOptions = [
             { id: 0,    description: 'Esposo(a)' },
             { id: 1,    description: 'Companheiro(a)' },
             { id: 2,    description: 'Filho(a)' },
             { id: 3,    description: 'Filho(a) Adotivo(a)' },
             { id: 4,    description: 'Pai' },
             { id: 5,    description: 'Mae' },
             { id: 6,    description: 'Irmao(a)' },
             { id: 7,    description: 'Avo/Avo' },
             { id: 8,    description: 'Sobrinho(a)' },
             { id: 9,    description: 'Enteado(a)' },
             { id: 10,    description: 'Sogro(a)' },
             { id: 11,    description: 'Neto(a)' },
             { id: 12,    description: 'Tio(a)' },
             { id: 13,    description: 'Primo(a)' },
             { id: 14,    description: 'Cunhado(a)' },
             { id: 15,    description: 'Nora' },
             { id: 16,    description: 'Genro' },
             { id: 17,    description: 'Amigo(a)' },
             { id: 18,    description: 'Outros' },
             { id: 19,    description: 'Nao Informado' }
        ];
    }

     $scope.refreshDependentes = function() {
        if ($scope.selectedCliente
            && $scope.selectedCliente.id != undefined) {
            $scope.dependentes = servicesFactory.clienteDependentes.get({id: $scope.selectedCliente.id });
        }
    }

    $rootScope.refreshClientes = function() {
        if ($scope.selectedCliente
            && $scope.selectedCliente.id != undefined) {
            $scope.selectedCliente = servicesFactory.clientes.get({id: $scope.selectedCliente.id });
        }
    }

    $scope.getInformativo = function(){
        $scope.informativoOptions = [];

        Informativo.get({}, function(dados){
            $scope.informativoOptions = dados;
        });

    }

    $scope.enviarDependente = function (dependente){
        $scope.enviarDependenteSelect = dependente;

    }

     //end dependente


    function defineRolesAccess() {
        $scope.rwRoles = ['administrador','gerenteadm', 'cadastro', 'gerenteatendimentocliente', 'rh', 'atendimentocliente', 'telemarketing', 'gerentetelemarketing'];
        $scope.formSimplificadoRoles = ['gerentetelemarketing','telemarketing'];
    };

    function loadComboOptions() {
        $scope.estadoCivilOptions = [
            { id: 0,    description: 'Solteiro(a)' }, { id: 1,    description: 'Casado(a)' },
            { id: 2,    description: 'Viuvo(a)' }, { id: 3,    description: 'Desquitado(a)' },
            { id: 4,    description: 'Divorciado(a)' }, { id: 5,    description: 'Outros' },
            { id: 6,    description: 'Nao informado' },
        ];

        $scope.percentualOptions = [
            { id: 10,    description: '10%' }, { id: 20,    description: '20%' },
            { id: 30,    description: '30%' }, { id: 40,    description: '40%' }, { id: 50,    description: '50%' }
        ];

        $scope.rdOptionsMigrar = [
            { value: 0, name: 'Não Associado'}, { value: 1,    name: 'Associado' }
        ];

        $scope.rdOptionsPagamento = [
            { value: 0, name: 'Boleto Bancário'},
            { value: 1, name: 'Débito Automático'},
            { value: 3, name: 'Cartão Recorrente'},
        ]

        $scope.cidadeOptions = servicesFactory.cidades.query();
        if($scope.selectedCliente.cidade && $scope.selectedCliente.cidade.id != null){
            $scope.loadBairroOptions($scope.selectedCliente.cidade);
        }
    }

    function inicializarValidacaoCombos() {
        $scope.comboValidacao = { invalidEstadoCivil: false, invalidCidade: false, invalidBairro: false };
    }

    function inicializarRegrasValidacao() {
        $scope.validationOptions = {};
        $scope.validationDependenteOptions = {};

        if ( $scope.canUseSimplesForm ) {
            $scope.validationOptions = {
                rules : {
                    txtCPF : {
                        cpf : { cpf : true, required : true},
                        required : true
                    },
                    txtNome : { minlength : 5, required : true},
                    txtCelular : { minlength : 15, required : true},
                    rdSexo : { required : true },
                }
            }
        } else {
             $scope.validationOptions = {
                rules : {
                    txtCPF : {
                        cpf : { cpf : true, required : true},
                        required : true
                    },
                    txtRG : { minlength : 4, required : true},
                    txtNome : { minlength : 5, required : true},
                    txtTelefone : { minlength : 14, required : false},
                    txtCelular : { minlength : 15, required : true},
                    txtDtNascimento : { dateITA : true, required : true},
                    selEstadoCivil : { required : true },
                    txtCEP1 : { minlength : 8, required : true},
                    rdSexo : { required : true },
                    txtLogradouro : { required : true},
                    txtLogradouroNumero : { required : true }
                }
            }
        }

        $scope.validationDependenteOptions = {
            rules : {
                txtRG : { minlength : 4},
                txtNome : { minlength : 5, required : true},
                txtTelefone : { minlength : 14},
                txtCelular : { minlength : 15, required : true},
                txtDtNascimento : { dateITA : true, required : true},
                txtCEP1 : { minlength : 8, required : true},
                txtLogradouro : { required : true},
                txtLogradouroNumero : { required : true }
            }
        }
    };

    function verificarRoleUsuario() {
        var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
        $scope.canUseSimplesForm = false;

        for ( role of userRoles ) {
            if ($scope.formSimplificadoRoles.indexOf(role) != -1) {
                $scope.canUseSimplesForm  =  true;
            }
        }
    };

    $scope.searchNewProspect = function(selectedCliente) {

        if(!selectedCliente.nrCPF) { return }

        servicesFactory.clienteNewProspect.query({cpf: selectedCliente.nrCPF}, function(dados){
            $rootScope.newProspect = dados;
            $rootScope.isFp = selectedCliente.boNaoAssociado && $rootScope.newProspect.nrCpf ? true : false;
            console.log("newprospect", $rootScope.newProspect);
        });


    };





    /* ================= VIEW-CONTROLLER INTERACTION ====================*/


    $scope.loadBairroOptions = function(cidade) {
        if (cidade) {
            $scope.bairrosOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
            $scope.alterarStatusValidacaoCidade();
        }
    };

    $scope.loadBairroCobrancaOptions = function(cidade) {
        if (cidade) {
           $scope.bairrosCobrancaOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
        }
    };

    $scope.updateCliente = function() {
        if ( $('#formCliente').valid() ) {

            var upcliente = Cliente.get({id: $scope.selectedCliente.id},function(){

                $scope.selectedCliente.digitalCliente = upcliente.digitalCliente;

                Cliente.update( { id: $scope.selectedCliente.id }, $scope.selectedCliente,
                    function( data ) {
                        $rootScope.addDefaultTimeoutAlert("Cliente", "atualizado", "success");
                        $scope.selectedCliente = Cliente.get({id: $scope.selectedCliente.id}, function(){
                           //atualizar situacao geral
                            Cliente.query({ nrCodCliente: String($scope.selectedCliente.nrCodCliente) }, function( data ) {

                                $scope.selectClienteFromSearch.statusGeral = data[0].statusGeral;
                            });
                        });
                    if ($scope.updateExistCliente == true){
                        $scope.fluxControl = {};
                        $scope.fluxControl['wasCadastrado'] = true;
                        $scope.form.next();
                    }
                }, function(error){
                    $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                });

                Metronic.scrollTop();

           },function(error ) {
                $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
           });
    	}
    };

    $scope.alterarStatusValidacaoEstadoCivil = function() {
        if( $scope.comboValidacao && $scope.comboValidacao.invalidEstadoCivil )
           $scope.comboValidacao.invalidEstadoCivil = false;

    };

    $scope.alterarStatusValidacaoCidade = function() {
        if( $scope.comboValidacao && $scope.comboValidacao.invalidCidade )
            $scope.comboValidacao.invalidCidade = false;
    };

    $scope.alterarStatusValidacaoBairro = function() {
        if( $scope.comboValidacao && $scope.comboValidacao.invalidBairro )
            $scope.comboValidacao.invalidBairro = false;
    };

    function combosValidos( form ) {

        var isValido = true;


        if( !$scope.canUseSimplesForm ) {

            if ( form.selEstadoCivil.$invalid ) {
                $scope.comboValidacao.invalidEstadoCivil = true;
                isValido = false;
            }

            if( $scope.selectedCliente.cidade.id == undefined  ) {
                $scope.comboValidacao.invalidCidade = true;
                isValido = false;
            }

            if( $scope.selectedCliente.bairro.id  == undefined ) {
                $scope.comboValidacao.invalidBairro = true;
                isValido = false;
            }

            if (!isValido)
                $rootScope.alerts.push({ type: "danger", msg: "Preencha os campos obrigatórios", timeout: 10000 }); Metronic.scrollTop();
        }

        return isValido;

    }

     function combosValidosDependente( form ) {
        var isValido = true;

        if($scope.dependente.cidade == undefined || $scope.dependente.cidade.id == undefined) {
            $scope.comboValidacao.invalidCidade = true;
            isValido = false;
        }

        if( $scope.dependente.bairro == undefined || $scope.dependente.bairro.id  == undefined) {
            $scope.comboValidacao.invalidBairro = true;
            isValido = false;
        }

        if( $scope.dependente.inParentesco  == undefined ) {
            $scope.comboValidacao.invalidParentesco = true;
            isValido = false;
        }

        if (!isValido)
            $rootScope.alerts.push({ type: "danger", msg: "Preencha os campos obrigatórios", timeout: 10000 }); Metronic.scrollTop();


        return isValido;

    }

    $scope.saveCliente = function( form, canRedirect ) {
        if ($scope.existCliente == true){
            $scope.updateCliente();
            $scope.updateExistCliente = true;
        }else{
            if ( form.validate() && combosValidos( form ) ) {
                $scope.buttonDisabled = true;
                $scope.selectedCliente.$save({tipoCliente: $scope.optionMigrar.value},function (object, responseHeaders) {
                    var location = responseHeaders('Location');
                    if (location) {
                        var locationArray = location.split("/");
                        var clienteId = locationArray[locationArray.length - 1];
                        $scope.selectedCliente.id = clienteId;

                        $rootScope.alerts.push({
                            type: "info",
                            msg: "Cliente criado com sucesso",
                            timeout: 10000 });
                        Metronic.scrollTop();

                        if( canRedirect )
                            $state.go('atendimentoCliente', { id: clienteId });
                        else {
                            $scope.fluxControl = {};
                            $scope.fluxControl['wasCadastrado'] = true;
                            $scope.form.next();
                        }
                    }
                    $scope.buttonDisabled = false;
                }, function( error) {
                $scope.buttonDisabled = false;
                });
            }
        }
    }

    $scope.addListaNovosDependentes = function(dependente) {
        if(!$scope.listaNovosDependentes) {
            $scope.listaNovosDependentes = [];
        }
        $scope.listaNovosDependentes.push(dependente);
    }


    $scope.saveDependente = function( form ) {
        if( form.validate()  && combosValidosDependente( form )) {
            $scope.buttonDisabled = true;
            servicesFactory.depedenteSave.save(
                {id:$scope.selectedCliente.id},
                $scope.dependente,
                function(response, responseHeaders) {
                    var location = responseHeaders('Location');
                    if(location) {
                        var locationArray = location.split("/");
                        var newId = locationArray[locationArray.length - 1];
                        $scope.dependente.id = newId;
                        $scope.addListaNovosDependentes($scope.dependente);

                        $scope.fluxControl.wasCadastradoDependente = true;
                        $scope.fluxControl.canCadastrarDependente = false;
                        $scope.dependente = {};
                        $scope.buttonDisabled = false;
                    }
                },function(err) {
                    $scope.buttonDisabled = false;
                }
            );
        }
    }

    $scope.finishedDependentes = function() {
        $rootScope.$emit('dependentes.finished');
    }

    $scope.habilitarCadastroDependente = function() {
        $scope.fluxControl.canCadastrarDependente = true;
        $scope.fluxControl.canShowAddDependenteOption = false;
    }

    $scope.cancelarCadastroDependente = function() {
         $scope.fluxControl.canCadastrarDependente = false;
        if(! $scope.fluxControl.wasCadastradoDependente) {
            $scope.fluxControl.canShowAddDependenteOption = true;
            loadDependentes();
        }
    }

    $scope.openModalDigitalFotoCliente = function() {

        $scope.saveDigitalFotoCliente = Cliente.get({id: $scope.selectedCliente.id});

        var itemsCliente = {
                            Cliente: servicesFactory.clientes,
                            selectedCliente: $scope.saveDigitalFotoCliente
                            };

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'cadDigitalFotoModalContent.html',
            controller: 'ModalInstanceCadDigitalFotoCtrl',
            backdrop: 'static',
            resolve: {
                    items: function () {
                        return itemsCliente;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                $scope.selectedCliente = Cliente.get({id: $scope.selectedCliente.id});
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    };


    /* ====================== WIZARD CONTROL =================== */

    var nextStep = function () {
        $scope.currentStep++;
    };
    var prevStep = function () {
        $scope.currentStep--;
    };
    var goToStep = function (i) {
        $scope.currentStep = i;
    };
    var errorMessage = function (i) {
        //toaster.pop('error', 'Error', 'please complete the form in this step before proceeding');
    };

    function loadDependentes() {
        //$scope.selectedCliente.id = 216411;

        //dependentes
        servicesFactory.dependentesCliente
        .get({id:$scope.selectedCliente.id}, function(dependentes) {
            console.log("dependentes", dependentes);
             $scope.listaDependentes = dependentes;
             $scope.listaNovosDependentes = dependentes;
        })

    }


    // Initial Value
    $scope.form = {

        next: function (form) {
            if (parseInt($scope.currentStep) == 1) {
                $scope.fluxControl['canShowAddDependenteOption'] = true;
            } else if (parseInt($scope.currentStep) == 2) {
                //responsabilidade converter associado controller;
                loadDependentes();// excluir
            }

            nextStep();
        },
        prev: function (form) {
            //$scope.toTheTop();
            prevStep();
        },
        goTo: function (form, i) {
            if (parseInt($scope.currentStep) > parseInt(i)) {

                if( parseInt(i) == 2 ) {
                    $scope.fluxControl['canShowAddDependenteOption'] = true;
                    $scope.fluxControl['wasCadastradoDependente'] = false;
                }

                if( parseInt(i) != 1 )
                    goToStep(i);

            } else {
                if (form.$valid) {
                    //$scope.toTheTop();
                    goToStep(i);

                } else
                    errorMessage();
            }
        },
        submit: function () {

        },
        reset: function () {

        }
    };

    /* ================= STARTERS ====================*/

    function inicialiarCliente() {
        $scope.selectedCliente = new Cliente({});
        $scope.selectedCliente.inSexo = 0;
        $scope.selectedCliente.boNaoAssociado = true;
        $scope.selectedCliente.boBloqueado = false;
        $scope.selectedCliente.boEstrangeiro = false;

        $scope.selectedCliente.vlCreditoPessoalTotal = 0;
        $scope.selectedCliente.vlLimite = 70.00;
        $scope.selectedCliente.vlSalario = 700.00;

        $scope.selectedCliente.cidade = {};
        $scope.selectedCliente.bairro = {};
    };

    function inicializarDependente() {
        $scope.dependente = {};
        $scope.dependente.inSexo = 0;
    }


    function inicio() {
        $scope.today = new Date();
        $scope.optionMigrar = {};
        $scope.optionPagamentoSelected = {};
        $scope.currentStep = 1;
        defineRolesAccess();
        loadComboOptions();

        //tempo
        $scope.fluxControl = {};
        $scope.fluxControl['wasCadastrado'] = false;
    };

	$scope.$watch('selectedCliente.nrCEP', function(newValue, oldValue)
	{
        resetAddressNonEditable();
        var cep = this.last ? this.last : "";
        if(cep)
        {
            cep = cep.toString();
            cep = cep.replace(/\D/g,'');
            if(cep.length == 8)
            {
                $scope.fetchingAddress = true;
                servicesFactory.getAddressByCep.get(
                    {cep: cep},
                    function(data)
                    {
                        $scope.fetchingAddress = false;
                        if(!data.erro)
                        {
                            $scope.fetchingAddress = false;
                            putCepOnModel(data);
                            putOthersElementsOfAddress(data);
                        }
                    }
                );
            }
        }
    }, true);

	$scope.$watch('dependente.nrCep', function(newValue, oldValue)
	{
        resetAddressNonEditableDependente();
        var cep = this.last ? this.last : "";
        if(cep)
        {
            cep = cep.toString();
            cep = cep.replace(/\D/g,'');
            if(cep.length == 8)
            {
                $scope.fetchingAddress = true;
                servicesFactory.getAddressByCep.get(
                    {cep: cep},
                    function(data)
                    {
                        $scope.fetchingAddress = false;
                        if(!data.erro)
                        {
                            $scope.fetchingAddress = false;
                            putCepOnModelDependente(data);
                            putOthersElementsOfAddressDependente(data);
                        }
                    }
                );
            }
        }
    }, true);

    if($state.is('atendimentoCliente.cliente-conta-novo')){
		if (!$rootScope.clienteSelecionadoConversao){
            inicio();
            inicialiarCliente();
            inicializarValidacaoCombos();
            verificarRoleUsuario();
            inicializarRegrasValidacao();
            inicializarDependente();
            loadDependenteProperties();
        }else{
            $scope.existCliente = true;
            inicio();
            loadDependentes();
            inicializarValidacaoCombos();
            verificarRoleUsuario();
            inicializarDependente();
            inicializarRegrasValidacao();
            loadDependenteProperties();
        }
    };

    function resetAddressNonEditable()
    {
        $scope.selectedCliente.nmLogradouro = "";
        $scope.selectedCliente.nmComplemento = "";
    }

    function resetAddressNonEditableDependente()
    {
        if(undefined != $scope.dependente)
        {
            $scope.dependente.nmLogradouro = "";
            $scope.dependente.nmComplemento = "";
        }
    }

    function putCepOnModel(address)
    {
        $scope.selectedCliente.nmLogradouro = address.logradouro;
        $scope.selectedCliente.nmComplemento = address.complemento;
    }

    function putCepOnModelDependente(address)
    {
        $scope.dependente.nmLogradouro = address.logradouro;
        $scope.dependente.nmComplemento = address.complemento;
    }

    function putOthersElementsOfAddress(address)
    {
        putCidadeInNewAssociado(address);
    }

    function putOthersElementsOfAddressDependente(address)
    {
        putCidadeInNewAssociadoDependente(address);
    }

    function putCidadeInNewAssociado(address)
    {
        servicesFactory.getCidadeByNm.query(
            {nm_uf: address.uf, nm_cidade: address.localidade},
            function(data)
            {
                $scope.selectedCliente.cidade = data;
                $scope.bairrosOptions = servicesFactory.cidadeBairros.get
                (
                    { id: $scope.selectedCliente.cidade.id},
                    function(lBairros)
                    {
                        putBairroInNewAssociado(address);
                    }
                );
            }
        );
    }

    function putCidadeInNewAssociadoDependente(address)
    {
        servicesFactory.getCidadeByNm.query(
            {nm_uf: address.uf, nm_cidade: address.localidade},
            function(data)
            {
                $scope.dependente.cidade = data;
                $scope.bairrosOptions = servicesFactory.cidadeBairros.get
                (
                    { id: $scope.dependente.cidade.id},
                    function(lBairros)
                    {
                        putBairroInNewAssociadoDependente(address);
                    }
                );
            }
        );
    }

    function putBairroInNewAssociado(address)
    {
        servicesFactory.getBairroByNm.query(
            {nm_bairro: address.bairro,id_cidade: $scope.selectedCliente.cidade.id},
            function(data)
            {
                $scope.selectedCliente.bairro = data;
            }
        );
    }

    function putBairroInNewAssociadoDependente(address)
    {
        servicesFactory.getBairroByNm.query(
            {nm_bairro: address.bairro,id_cidade: $scope.dependente.cidade.id},
            function(data)
            {
                $scope.dependente.bairro = data;
            }
        );
    }

    if($state.is('atendimentoCliente.cliente-conta')){

        inicio();

        $scope.selectedCliente = new Cliente($scope.selectedCliente);
        if ($scope.selectedCliente.id) {
            $scope.selectedCliente = Cliente.get({id: $scope.selectedCliente.id}, function(){

                $scope.loadBairroOptions($scope.selectedCliente.cidade);
                if ($scope.selectedCliente.enderecoCobranca && $scope.selectedCliente.enderecoCobranca.cidade)
                    $scope.loadBairroCobrancaOptions($scope.selectedCliente.enderecoCobranca.cidade);
            });
    	}
    };

});

/* ================= MODALS DEFINITIONS  ====================*/

MedicsystemApp.controller('ModalInstanceCadDigitalFotoCtrl', function ($rootScope, $scope, $state, $modalInstance, items) {

    $rootScope.digitalFotoClienteMsg = "Capturar Digital";

    var ClienteService = items.Cliente;



    $scope.$watch('digitalCapturadaMsg', function() {
        if($rootScope.digitalCapturadaMsg != undefined){

            $rootScope.digitalFotoClienteMsg = $rootScope.digitalCapturadaMsg;
            $("#btnCadDigitalFoto").prop("disabled",true);
        }
    });

    $scope.cadDigitalFoto = function () {
        var socket = io.connect('http://localhost:9092');

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });

        var jsonObject = {id: items.selectedCliente.id,
                          nmCliente: items.selectedCliente.nmCliente,
                          tipoPessoa: 'Cliente'};

        $rootScope.digitalFotoClienteMsg = "Processando...";
        $rootScope.digitalCapturadaMsg = undefined;

        socket.emit('biometriaregisterevent', jsonObject);

        socket.on('biometriaregisterevent', function(data) {

            $rootScope.digitalCapturadaMsg = data;
            var typeEvent;


            if(data != undefined
                && (data.digital != undefined)){

                items.selectedCliente.digitalCliente = data.digital;
                items.selectedCliente.idDedoDigital = data.idDedoDigital;

                ClienteService.update( { id: items.selectedCliente.id }, items.selectedCliente,
                    function( data ) {
                        $rootScope.addDefaultTimeoutAlert("Digital ", "capturada", "success");
                        $rootScope.refreshClientes();
                        typeEvent = 'success';
                        socket.emit('disconnect');
                        socket.removeAllListeners();

                }, function(error){
                    $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                });

                //Cliente.get({id: items.id})

            } else {

                typeEvent = 'danger';
                $rootScope.alerts.push({
                    type: typeEvent,
                    msg: data,
                    timeout: 10000
                });
            }

            setLabels();
            Metronic.scrollTop();
            $modalInstance.close();
        });
    };

    $scope.exit = function () {

        setLabels();
        $modalInstance.close();
    };

    $scope.cancel = function () {

        setLabels();
        $modalInstance.dismiss('cancel');
    };

    function setLabels(){

        $rootScope.digitalFotoClienteMsg = "Capturar Digital";
        $rootScope.digitalCapturadaMsg = undefined;
    }
});

MedicsystemApp.config(function ($validatorProvider) {
    $validatorProvider.setDefaults({
        errorElement : 'span', //default input error message container
        errorClass : 'help-block help-block-error', // default input error message class
        focusInvalid : true, // do not focus the last invalid input
        ignore : "", // validate all fields including form hidden input
        invalidHandler : function(event, validator) { //display error alert on form submit
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight : function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight : function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success : function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler : function(form) {
            error.hide();
        }
    });
});
