/*
@author  : JoeltonMatos
@since   : 05-05-2016
@last-modification: 18/06/2016
@version :  1.1 
*/

'use strict';

MedicsystemApp.controller('FuncionarioUploadArquivoController',
	 function($rootScope,$scope, $http, $timeout,$state,$stateParams,servicesFactory, Upload, toastr) {

	function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };

	 $scope.salvarArquivo = function( ) {
	    if ( !isEmpty( $scope.uploadedFile ) ) {
	    	$scope.buttonDisabled = true;
	    	$scope.uploadAnexo($scope.uploadedFile);	
	    } 
	  };

	  function isEmpty(obj) {
	    for(var prop in obj) {
	        if(obj.hasOwnProperty(prop))
	            return false;
	    }

    	return true;
	 };

	  $scope.uploadAnexo = function (file) {

	  		$scope.f = file;

	        Upload.upload({
	            url: 'mediclab/funcionarios/'+$scope.idFuncionario+'/upload',
	            data: {file: file, 'username': $scope.username}
	        })

	        .then(function (resp) {
	        	$scope.buttonDisabled = false;
	        	$scope.uploadedFile = null;
	        	$scope.getArquivosFuncionario( $scope.idFuncionario );
	        	messageToastr('sucesso',"Arquivos enviados com sucesso !","SUCESSO");
	        }, function (resp) {
	        	messageToastr('erro',"Falha ao enviar arquivos! :" + resp.status,"Ops,");
              	$scope.buttonDisabled = false;
	        }, function (evt) {
	        	file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	        });
	   };

	   $scope.preencher = function(files,invalidFiles) {}


	   $scope.inicializarForm = function() {
	   		$scope.f = null; 
	   		$scope.uploadedFile = null;
	   };

	   $scope.getArquivosFuncionario = function( idFuncionarioSelecionado ) {
	   		$scope.employeeImages = [];
	  		
	   		servicesFactory.arquivosFuncionario.get( {idFuncionario: idFuncionarioSelecionado}, 
	   			function( result ) {

             		var i;
             		$scope.arquivosFuncionario = result;
             		$scope.funcionario =  $scope.arquivosFuncionario.funcionario;
             		$scope.arquivos = $scope.arquivosFuncionario.arquivos;

             		if( $scope.arquivos ) {
             			for( i = 0; i < $scope.arquivos.length; i++ ) {
	  						if( $scope.arquivos[i].extensao != 'pdf' ) {
	  							$scope.arquivos[i].extensao = $scope.arquivos[i].extensao == 'jpeg' ? 'jpg' : $scope.arquivos[i].extensao;
	  							$scope.arquivos[i].url = $scope.arquivos[i].url.replace("\\","/");
	  							$scope.employeeImages
	  								.push({ thumb: "http://45.33.93.46/imagens/funcionarios/" + $scope.arquivos[i].url, img : "http://45.33.93.46/imagens/funcionarios/" + $scope.arquivos[i].url, description: $scope.arquivos[i].nome });
	  						}
             			}
					}
           });
	   };
  

	   var inicio = function( id, nome ) {
		$scope.idFuncionario = id;
		$scope.nmFuncionario = nome;
		$scope.canUpload = true;	
		$scope.f = null; 
		$scope.getArquivosFuncionario( $scope.idFuncionario );
		}

 	   if(  $state.is('funcionario_uploadArquivo')  ) {
 	   		if( $stateParams.idFuncionario && $stateParams.nmFuncionario  )
 	   			inicio( $stateParams.idFuncionario, $stateParams.nmFuncionario);
 	   }

});