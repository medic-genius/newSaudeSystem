'use strict';

MedicsystemApp.controller('ModalLoginGerenteController', function ($rootScope, $scope, $state, $modal, $modalInstance, $http, exibeJustificativa, motivacaoAutorizacao, perfilNecessario, servicesFactory) {

	$scope.motivacaoAutorizacao = "Requer autorização de usuário com perfil de gerente";
	$scope.msgPresenca = "Agendamento fora do horário de presença. <br /> Use o perfil de gerente ou aguarde o horário combinado.";
	$scope.justificativaGerente = null;
	$scope.showJustificativa = false;
	$scope.showMsgPresenca = false;
	
	if (exibeJustificativa !== undefined) {
		$scope.showJustificativa = exibeJustificativa;
	}
	
	if (motivacaoAutorizacao) {
		$scope.motivacaoAutorizacao = motivacaoAutorizacao;
	}

	if ($rootScope.showMsgPresenca) {
		$scope.showMsgPresencaModal = $rootScope.showMsgPresenca;
	}
	
	$scope.authorize = function () {
		
		if ($scope.showJustificativa && (!$scope.justificativaGerente || $scope.justificativaGerente.length < 9)) {
			$rootScope.alerts.push({
                type: "danger", 
                msg: "Justificativa obrigatória, minímo de 10 caracteres.",
                timeout: 5000
              });
			return;
		}
		
		var loginDTO = {username: $scope.userNameGerente,
                    password: $scope.passwordGerente, perfilNecessario: perfilNecessario };

		servicesFactory.loginGerente.auth({},
          loginDTO,
          function(data) {
              if(data){
            	  if ($scope.showJustificativa) {
            		  $modalInstance.close( { autorizado: true, justificativa: $scope.justificativaGerente, loginGerente: $scope.userNameGerente } );
            	  } else {
            		  $modalInstance.close( { autorizado: true } );
            	  }
              }
          },
          function(error) {

            if(error.status == '403'){
 
              $rootScope.alerts.push({
                type: "danger", 
                msg: "Usuário/senha inválidos e/ou Gerente sem autorização para esta ação.",
                timeout: 10000
              });
              
            }
          });
		
	}

	  $scope.cancel = function () {
		  $modalInstance.dismiss('cancel');
		  $rootScope.showMsgPresenca = false;
	  };
	  
});