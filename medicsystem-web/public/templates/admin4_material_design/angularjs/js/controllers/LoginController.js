'use strict';

MedicsystemApp.controller('LoginController', function($scope, $location, $auth) {

	$scope.authenticate = function() {
      $auth.authenticate('medicsystem-angular')
        .then(function() {
          console.log('You have successfully signed in with ' + provider);
          $location.path('/#/home.html');
        })
        .catch(function(response) {
        	console.log(response);
        });
    };
    
});