'use strict';

/*
  CONTROLE FILHA DE CLIENTE CONTA CONTROLLER
  RESPONSAVEL PELOS PASSOS A PARTIR DE CONTRATO
  PARA CONVERTER EM ASSOCIADO, COM EXCECAO DO
  CARREGAMENTO DE DEPENDENTES, POSI ESSE DEVE
  SER DINAMICA A CADA CLIQUE PASSO 3
*/

MedicsystemApp.controller('ClienteContratoNovoController', function($scope, $rootScope,
	$state, $stateParams, $timeout, toastr, Auth, servicesFactory) {
	 
	//-------------------------------------------------------------------------------
	// START - STEP 3 [CONTRATO]
	$scope.stepTreeStarted = function() {
  	$scope.loadEmpresas();
  	$scope.configVarsStep3();
  }

  $rootScope.$on('dependentes.finished', function() {
  	$scope.configListaUsuarios();
  });

	//-----------------------------------------------------
	// DEFINIÇÕES DE VARIÁVEIS
	$scope.configVarsStep3 = function() {
		$scope.contrato = {
			empresaGrupo: {
				id: undefined
			},
			plano: {
				id: undefined
			},
			informaPagamento: undefined,
			agencia: {
				id: undefined
			},
			banco: {
				id: undefined
			},
			nrConta: undefined,
			nrDigitoVerificador: undefined,
			dtVencimentoInicial: undefined
		};

		$scope.contratoCliente = {
			inTipoCliente: undefined,
			nrMatricula: undefined,
			orgao: {
				id: undefined
			}
		}

		$scope.cartao = {
			portador: undefined,
			numero: undefined,
			cvv: undefined,
			vencimento: undefined,
			isElo: undefined
		}

		$scope.formasPagamento = [
		{
			id: 0,
			nmFormaPagamento: "BOLETO BANCÁRIO"
		},
		{
			id: 1,
			nmFormaPagamento: "DÉBITO AUTOMÁTICO"
		},
		{
			id: 2,
			nmFormaPagamento: "CONTRA-CHEQUE"
		},
		{
			id: 30,
			nmFormaPagamento: "CARTÃO RECORRENTE"
		}
		];

		$scope.tiposCliente = [
		{
			nmTipo: 'FUNCIONÁRIO PÚBLICO',
			idTipo: 1
		},

		{
			nmTipo: 'PARTICULAR ASSOCIADO',
			idTipo: 2
		}
		];

		$scope.configDatePicker();
	}

	$scope.configListaUsuarios = function() {
		var parent = $scope.$parent.$parent;
		$scope.listaUsuarios = [];
		var cliente = JSON.parse(JSON.stringify(parent.selectedCliente));
		cliente.isTitular = true;
		cliente.checked = true;
		$scope.listaUsuarios.push(cliente);
		if(parent.listaNovosDependentes) {
			for(var i = 0; i < parent.listaNovosDependentes.length; ++i) {
				var dep = JSON.parse(JSON.stringify(parent.listaNovosDependentes[i]));
				dep.isTitular = false;
				dep.checked = false;
				$scope.listaUsuarios.push(dep);
			}
		}
	}
	//-----------------------------------------------------

	//-----------------------------------------------------
	// Date picker configuration

	$scope.configDatePicker = function() {
		$scope.format = ['dd/MM/yyyy', 'MM/yyyy'];
		$scope.status = {
			opened: false
		};
        
		$scope.minDate = new Date();
		$scope.dateOptions = {
			formatYear: 'yyyy',
			startingDay: 1
		};

		$scope.optionsVencCartao = {
			formatYear: 'yyyy',
			minMode: 'month'
		}
		$scope.statusDtVencimento = {
			opened: false
		}

		$scope.disabled = function(date, mode) {
		  return false; //( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.showDatePicker = function() {
			$scope.status.opened = true;
		}
	}
  //--------------------------------------------------------

	$scope.getSelectedUsers = function() {
		var arr = [];
		if($scope.listaUsuarios) {
			for(var i = 0; i < $scope.listaUsuarios.length; ++i) {
				if($scope.listaUsuarios[i].checked) {
					arr.push($scope.listaUsuarios[i]);
				}
			}
		}
		return arr;
	}

	$scope.loadEmpresas = function() {
		servicesFactory.getEmpresaGrupo.get(
			function(response) {
				if(response && response.length > 0) {
					$scope.listaEmpresas = response;
					for(var i = 0; i < response.length; ++i) {
						if(response[i].id == 9) {
							$scope.contrato.empresaGrupo.id = response[i].id;
							$scope.loadPlanos();
							break;
						}
					}                   
				} 
			},
			function(err) {
				// empresa default
				$scope.listaEmpresas = [
				{
					id: 9,
					nmFantasia: "MEDIC LAB SAUDE"
				}
				]
			}
		);
            
    }

	$scope.loadPlanos = function() {
		if(!$scope.contrato.empresaGrupo.id) {
			return;
		}
		var rParams = {
			id: $scope.contrato.empresaGrupo.id
		}
        
        $scope.listaPlanos = [];
        
		servicesFactory.empresagruposPlanos.get(
			rParams,
			function(response) {
				if(response) {
                    var sel = response;                       
                    for(var i = 0; i < sel.length; ++i) {
                        if(sel[i].boVenda && sel[i].boVenda == true) {
                            $scope.listaPlanos.push(sel[i]);                            
                        }
                    }					 
				}
			},
			function(err) {

			}
		);
	}

	$scope.loadOrgaosPublicos = function() {
		servicesFactory.allOrgaosPublicos.get(
			function(response) {
				if(response) {
					$scope.listaOrgaos = response;                    
				}
			},
			function(err) {

			}
		);
	}

	$scope.loadBancos = function() {
		servicesFactory.allBancos.get(
			function(response) {
				if(response) {
					$scope.listaBancos = response;
				}
			},
			function(err) {

			}
		);
	}

	$scope.loadAgenciasBanco = function(idBanco) {
		var rParams = {
			idBanco: idBanco
		}
		servicesFactory.allAgenciasBanco.get(
			rParams,
			function(response) {
				if(response) {
					$scope.listaAgenciasBanco = response;
				}
			},
			function(err) {

			}
		);
	}

	$scope.checkTipoCliente = function(tipoId) {
		if(tipoId == 1) {
			$scope.loadOrgaosPublicos();
            
            if($scope.$parent.$parent.$parent.$parent.$parent.newProspect && $scope.$parent.$parent.$parent.$parent.$parent.newProspect.id){
                $scope.contratoCliente.nrMatricula = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.nrMatricula;            
                $scope.contratoCliente.orgao = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.orgao;
            }
		}
	}
    
	$scope.checkFormaPagamento = function(formaPagamentoId) {
        $scope.contrato.dtVencimentoInicial = null;
                
		if(formaPagamentoId == 1 && !$scope.listaBancos) {
			$scope.loadBancos();   
            
            if($scope.$parent.$parent.$parent.$parent.$parent.newProspect && $scope.$parent.$parent.$parent.$parent.$parent.newProspect.id){
                $scope.contrato.banco.id = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.banco.id;
                $scope.loadAgenciasBanco($scope.contrato.banco.id);
                $scope.contrato.agencia.id = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.agencia.id;
                
                $scope.contrato.nrConta = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.nrConta;
                $scope.contrato.nrDigitoVerificador = $scope.$parent.$parent.$parent.$parent.$parent.newProspect.nrDigitoConta;
                            
            }
        } 
        
        if(formaPagamentoId == 1){
            var dtAtual = new Date();           
            if(dtAtual.getDate() >= 1 && dtAtual.getDate() < 15){
                $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear(), dtAtual.getMonth(), 15);
            }else{
                if((dtAtual.getMonth() + 1) > 12)
                    $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear() + 1, 1, 15);
                else
                    $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear(), dtAtual.getMonth() + 1, 15);
            }
            
        } else if(formaPagamentoId == 2){
            var dtAtual = new Date();           
            if(dtAtual.getDate() >= 1 && dtAtual.getDate() < 15){
                $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear(), dtAtual.getMonth(), 25);
            }else{
                if((dtAtual.getMonth() + 1) > 12)
                    $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear() + 1, 1, 25);
                else
                    $scope.contrato.dtVencimentoInicial = new Date(dtAtual.getFullYear(), dtAtual.getMonth() + 1, 25);
            }            
        }
                
	}

	$scope.updateContratoValues = function() {

		$scope.contrato.vlContrato = $scope.contrato.vlPlano;
	}

	function saveContrato(formObj) {
		var idCliente;
		var idsDependentes = [];
		var sel = $scope.getSelectedUsers();
		for(var i = 0; i < sel.length; ++i) {
			if(sel[i].isTitular) {
				idCliente = sel[i].id;
			} else {
				idsDependentes.push(sel[i].id);
			}
		}
		
        $scope.contrato.boNaoFazUsoPlano = !idCliente;
        
		if(!idCliente) {
			idCliente = $scope.listaUsuarios[0].id;
		}
		var rParams = {
			idCliente: idCliente,
			idDependentes: idsDependentes,
			idAssinaturaRecorrente: $scope.vindiSubscriptionId
		}

		//body
		var rBody = JSON.parse(JSON.stringify($scope.contrato));
        rBody.inTipoCliente = $scope.contratoCliente.inTipoCliente;
        rBody.nrMatricula = $scope.contratoCliente.nrMatricula;
        rBody.idOrgao = $scope.contratoCliente.orgao.id;
        		
		var value = $scope.contrato.plano.vlPlano * $scope.getSelectedUsers().length;
		rBody.vlContrato = value;
		rBody.vlTotal = value;

		$scope.savingContrato = true;
		servicesFactory.createContrato.post(
			rParams,
			rBody,
			function(response, responseHeaders) {
				var location = responseHeaders('Location');
				if (location) {
					var locationArray = location.split("/");
					var contratoId = locationArray[locationArray.length - 1];
					$scope.contrato.id = contratoId;
				}
				$scope.contratoCriado = true;
				var evData = {
					contrato: $scope.contrato,
					listaUsuarios: $scope.listaUsuarios
				}
				$rootScope.$emit('conversao.contratoCriado', evData);
				formObj.next();
			},
			function(err) {
				$scope.contratoErro = true;
				$scope.contratoErroMsg = 'Erro ao salvar contrato. Tente novamente';
				$timeout(function() {
					$scope.contratoErro = false;
					$scope.contratoErroMsg = undefined;
				}, 5000);
			}
		).$promise.finally(function() {
			$scope.savingContrato = false;
		});
	}

	$scope.$watch('contrato.nrDigitoVerificador',function(val,old) {
		if(val) {
			$scope.contrato.nrDigitoVerificador = parseInt(val);
		}
	});

	$scope.$watch('contrato.nrConta',function(val,old) {
		if(val) {
			$scope.contrato.nrConta = parseInt(val);
		}
	});
	// END - STEP 3 [CONTRATO]
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------

	// CRIACAO PAGAMENTO VINDI
	function criarAssinatura(formObj) {
		var rParams = {
			card: $scope.cartao.numero
		}
		$scope.creatingSubscription = true;
		servicesFactory.checkCartao.get(
			rParams,
			function(response) {
				var customer_id;
				if(response.status == "success") {
					customer_id = response.subscriptionId
				}
				rParams = {
					cartaoNomePortador: $scope.cartao.portador,
					cartaoNumero: "" + $scope.cartao.numero,
					cartaoCvv: "" + $scope.cartao.cvv,
					cartaoValidade: moment($scope.cartao.vencimento).format('MM/YY'),
					cartaoIsElo: !!$scope.cartao.isElo,
					customerId: customer_id,
					idEmpresaGrupo: $scope.contrato.empresaGrupo.id,
					planoIdRecorrencia: $scope.contrato.plano.idRecorrenciaPlano,
					valor: $scope.contrato.plano.vlPlano,
					quantidade: $scope.getSelectedUsers().length,
					diaVencimentoInicial: $scope.contrato.dtVencimentoInicial.getDate()
				}
				servicesFactory.criarAssinatura.post(
					rParams,
					function(response) {
						if(response.status == 'success') {
							$scope.assinaturaCriada = true;
							$scope.vindiSubscriptionId = response.subscriptionId;
							// toastr.success('', 'Assinatura criada com sucesso');
							saveContrato(formObj);
						} else {
							$scope.assinaturaCriada = false;
							if(response.status == 'fail') {
								toastr.error('', response.message);
							} else {
								var errors = JSON.parse(response.message).errors;
								if(errors && errors[0]) {
									toastr.error(errors[0].message, errors[0].parameter)
								} else {
									toastr.error('', 'Houve um erro ao criar assinatura');
								}
							}
						}
						$scope.creatingSubscription = false;
					},
					function(err) {
						toastr.warning('', 'Houve um erro na comunicação com o servidor')
						$scope.creatingSubscription = false;
					}
				);
			},
			function(err) {
				toastr.warning('', 'Houve um erro na comunicação com o servidor')
				$scope.creatingSubscription = false;
			}
		);
	}

	$scope.finalizarConversao = function(formObj) {
		if($scope.contrato.informaPagamento == 30 && !$scope.vindiSubscriptionId) {
			criarAssinatura(formObj);
		} else {
			saveContrato(formObj);
		}
	}
	//-------------------------------------------------------------------------------
});
