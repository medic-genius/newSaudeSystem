'use strict';

MedicsystemApp.controller('ClienteComprovanteNovoController', function($scope, $rootScope,$state,
	$stateParams, $timeout, toastr, servicesFactory) {
			var Contrato = servicesFactory.contratoPdf;
			var Boleto = servicesFactory.boletoPdf;
			var Cliente = servicesFactory.clientes;

			$rootScope.$on('conversao.contratoCriado', function(event, dados) {
				$scope.idFormaPagamento = dados.contrato.informaPagamento;
				$scope.contratoId = dados.contrato.id;
				$scope.listaUsuarios = dados.listaUsuarios;
			});

			function messageToastr(type, message, title) {
				if( type == 'erro' ) {
				toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} ); 
				} else if( type == 'sucesso' ) {
				toastr.success(  message, title.toUpperCase() ); 
				} else if( type == 'informacao' ) {
				toastr.info(  message, title.toUpperCase() ); 
				}
			};

			// 0 = DIAMANTE  1 = DIAMENTE SEM CARENCIA
			$scope.printDiamante = function (tipo){
				$scope.idTitular = null;
				$scope.listIdDepend = [];
				$scope.disabledButtonDiamante = true;

					for ( var i = 0; i < $scope.listaUsuarios.length; i++) {
						
						if ($scope.listaUsuarios[i].isTitular == true){
							$scope.idTitular = $scope.listaUsuarios[i].id;
						}else{
							if($scope.listaUsuarios[i].checked == true){
								$scope.listIdDepend.push($scope.listaUsuarios[i].id);
							}
						}
					}

				Contrato.get({idCliente: $scope.idTitular,
				idContrato: $scope.contratoId,
				idDependentes: $scope.listIdDepend,
				tipoPlano: 0
				}, function(dados){
			
					Object.size = function(obj) {
						var size = 0, key;
						for (key in obj) {
							if (obj.hasOwnProperty(key)) size++;
						}
						return size;
					};
					// Get the size of an object
					var size = Object.size(dados);
					var str = '';
					var lenght = size - 2;
					for (var i=0; i < lenght ; i ++){
						str = str + dados[i];
					}
					var blob = b64toBlob(str, 'application/pdf');
					var fileURL = URL.createObjectURL(blob);
					window.open(fileURL , '_blank');
				$scope.disabledButtonDiamante = false;
				});	
			};

			// 0 = DIAMANTE  1 = DIAMENTE SEM CARENCIA
			$scope.printDiamanteSemCarencia = function (tipo){
				$scope.disabledButtonDiamanteSemC = true;
				$scope.idTitular = [];
				$scope.listIdDepend = [];

					for ( var i = 0; i < $scope.listaUsuarios.length; i++) {
							
							if ($scope.listaUsuarios[i].isTitular == true){
								$scope.idTitular = $scope.listaUsuarios[i].id;
							}else{
								if($scope.listaUsuarios[i].checked == true){
									$scope.listIdDepend.push($scope.listaUsuarios[i].id);
								}
							}
						}

				Contrato.get({idCliente: $scope.idTitular,
				idContrato: $scope.contratoId,
				idDependentes: $scope.listIdDepend,
				tipoPlano: 1
				}, function(dados){
			
					Object.size = function(obj) {
						var size = 0, key;
						for (key in obj) {
							if (obj.hasOwnProperty(key)) size++;
						}
						return size;
					};
					// Get the size of an object
					var size = Object.size(dados);
					var str = '';
					var lenght = size - 2;
					for (var i=0; i < lenght ; i ++){
						str = str + dados[i];
					}
					var blob = b64toBlob(str, 'application/pdf');
					var fileURL = URL.createObjectURL(blob);
					window.open(fileURL , '_blank');
					$scope.disabledButtonDiamanteSemC = false;
				});
			};

			$scope.printBoleto = function(){
				$scope.disabledButtonBoleto = true;
				Boleto.get({idContrato: $scope.contratoId},
				function (dados){
					Object.size = function(obj) {
						var size = 0, key;
						for (key in obj) {
							if (obj.hasOwnProperty(key)) size++;
						}
						return size;
					};
					// Get the size of an object
					var size = Object.size(dados);
					var str = '';
					var lenght = size - 2;
					for (var i=0; i < lenght ; i ++){
						str = str + dados[i];
					}
					var blob = b64toBlob(str, 'application/pdf');
					var fileURL = URL.createObjectURL(blob);
					window.open(fileURL , '_blank');
					$scope.disabledButtonBoleto = false;
				});
			};
			

			$scope.finalizar = function(item) {
				var dt = {
					id: item
				};
				messageToastr("sucesso", "Cliente cadastrado com sucesso","Sucesso");
				$scope.$emit('cadastroCliente.finished', dt);				
			};
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
