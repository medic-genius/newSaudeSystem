'use strict';

MedicsystemApp.controller('HomeController', function($scope,$state,Auth) {


	$scope.$on('$viewContentLoaded', function() {
		Metronic.initAjax();
	});

    var defineUrlByRole = function( userRoles ) {

    	var administrador = 'administrador';
    	var medico = 'medico';
    	var atendimentoCliente = 'atendimentocliente';
        var telemarketing = 'telemarketing';
    	var diretoria = 'diretoria';
    	var administrativo = 'administrativo';
    	var enfermagem = 'enfermagem';
        var triagem = 'triagem';
        var cobranca = 'cobranca';
        var financeiro = 'financeiro';
        var conveniada = 'conveniada';
        var rh = 'rh';

    	var gerenteUnidade = 'gerenteunidade';
    	var gerenteAtendimentoCliente = 'gerenteatendimentocliente';
        var gerenteAdm = 'gerenteadm';

    	var qtdUserRoles = userRoles.length;

    	if( qtdUserRoles == 2 ) {

    		for( var i=0; i<userRoles.length; i++) {
	    		var userRole = userRoles[i];

	    		if ( userRole == medico )
	    			$state.go('medico-visualizarPacientes');
	    		else if ( userRole == diretoria )
	    			$state.go('funcionario-lista');
	    		else if ( userRole == atendimentoCliente   )
	    			$state.go('atendimentoCliente');
	    		else if ( userRole == administrativo )
	    			$state.go('<admin-renovacao></admin-renovacao>-contrato');
	    		else if ( userRole == enfermagem )
	    			$state.go('agendamentoMedico');
                else if ( userRole == telemarketing )
                    $state.go('especialidadeAgendaDisponivel');
                else if ( userRole == triagem )
                    $state.go('triagem-visualizarPacientes');
                else if ( userRole == cobranca )
                    $state.go('cobrancaCliente');
                else if ( userRole == financeiro )
                    $state.go('contaPagar-principal');
                else if ( userRole == rh )
                    $state.go('funcionario-lista');
                else if ( userRole == gerenteAdm )
                    $state.go('db_resumoFaturamento');
                else if ( userRole ==  conveniada)
				{
					// state app.js
                    $state.go('associados');
				}

    		}
    	} else {

    		var notFound = -1;

    		//hierarquia
    		if( userRoles.indexOf( administrador ) != notFound  )
    			$state.go('pesquisa_servico');
    		else if( userRoles.indexOf( gerenteUnidade ) != notFound  )
    			$state.go('atendimentoCliente');
    		else if( userRoles.indexOf( gerenteAtendimentoCliente ) != notFound  )
    			$state.go('atendimentoCliente');
			else if(userRoles.indexOf( conveniada ) != notFound)
				$state.go('associados');
            else if (userRoles.indexOf( rh ) != notFound )
                $state.go('funcionario-lista');
            else
                $state.go('atendimentoCliente');
    	}
    };

    var inicio = function() {
    	var userRoles = Auth.authz.resourceAccess["medicsystem-api"].roles;
    	defineUrlByRole( userRoles );
    };

    inicio();

});
