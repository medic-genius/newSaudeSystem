'use strict';

MedicsystemApp.controller('ClienteGuiaDetailsController',
    function($scope, servicesFactory, $state, $stateParams)
    {
        var idGuia = $stateParams.id;

        function loadGuia()
        {
            servicesFactory.getGuiaFromAdm.get({id: idGuia}, function(data)
            {
                $scope.guia = data;
                if($scope.guia && $scope.guia.guiaServicos)
                {
                    for ( var i = 0 ; i < $scope.guia.guiaServicos.length; i++)
                    {
                        $scope.guia.guiaServicos[i].statusLabel = servicesFactory.getStatusItemOfGuia({id: $scope.guia.guiaServicos[i].status});
                        $scope.guia.guiaServicos[i].agendamento = {id: null, label: "Aguarde..."};
                        $scope.guia.guiaServicos[i].despesa = {id: null, label: "Aguarde..."};
                        resolveDespesaGuiaServico(i);
                        if($scope.guia.guiaServicos[i].status == 4)
                        {
                            $scope.guia.guiaServicos[i].tdExclusaoFormated = moment($scope.guia.guiaServicos[i].dtExclusao).format('DD/MM/YYYY');
                        }
                    }
                    if($scope.guia.dependente)
                    {
                        $scope.ownership = "Dependente";
                        $scope.own = $scope.guia.dependente.nmDependente;
                    } else
                    {
                        $scope.ownership = "Titular";
                        $scope.own = $scope.guia.cliente.nmCliente;
                    }
                    $scope.dateValidPretty = moment($scope.guia.dtValidade).format('DD/MM/YYYY');
                    $scope.dateCreatePretty = moment($scope.guia.dtInicio).format('DD/MM/YYYY');
                }
            });
        }

        function resolveDespesaGuiaServico(i)
        {               
            servicesFactory.searchDespesaByGuiaServico.get
            (
                {id: $scope.guia.guiaServicos[i].id},
                function(despesa)
                {
                    if(despesa.id)
                    {            
                        if(despesa.agendamento != null){
                            $scope.guia.guiaServicos[i].agendamento =
                            {
                                id: despesa.agendamento.id,
                                label: despesa.agendamento.id
                            };
                        } else 
                        {
                            $scope.guia.guiaServicos[i].agendamento =
                            {
                                id: null,
                                label: "Não há"
                            };
                        }
                        $scope.guia.guiaServicos[i].despesa =
                        {
                            id: despesa.id,
                            label: despesa.id
                        };                        
                    } else
                    {
                        $scope.guia.guiaServicos[i].agendamento =
                        {
                            id: null,
                            label: "Não há"
                        };
                        $scope.guia.guiaServicos[i].despesa =
                        {
                            id: null,
                            label: "Não há"
                        };
                    }
                }
            );
        };

        servicesFactory.getCompany.get({}, function(company){
            $scope.company = company;
        });

        $scope.print = function()
        {
            window.print();
        }

        $scope.backList = function()
        {
            history.back();
        }
        loadGuia();
    }
);
