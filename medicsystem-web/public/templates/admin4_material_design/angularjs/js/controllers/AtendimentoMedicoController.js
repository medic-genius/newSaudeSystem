'use strict';

MedicsystemApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

MedicsystemApp.controller('AtendimentoMedicoController', function($rootScope, $scope, 
    $state, $stateParams, $modal, servicesFactory, Auth) {

    $scope.perfisMenuDespesa = ['administrador', 'atendimentomedico', 'recepcaomedicaodonto', 'rh', 'gerenteadm', 'cadastro', 'contaspagar', 'faturamento', 'caixa', 'gerenterecepcaomedicaodonto', 'gerenteunidade', 'telemarketing', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerentefinanceiro'];

    $scope.notAllowedSearch = ['medico'];
    $scope.allowedSearch = ['administrador', 'corpoclinico', 'digitadora'];

    var Medico = servicesFactory.medicos;
    var MedicoView = servicesFactory.medicoView;

    $scope.atualizarSelectMedicos = function(text) {
        if (text) {
            var queryObject = {};
            var filtro = $scope.searchFiltro;
            text = text.toLowerCase();

            queryObject = { nmMedico: text };

            if(text) {
                var data = Medico.query(queryObject, function() {
                    $scope.medicos = data;
                    if (data.length == 0) {
                        $scope.selectedMedicoNotFound = "Sem dados disponiveis.";

                    } else {
                        $scope.selectedMedicoNotFound = "";
                    }
                });
            }
        }
    };

    $scope.showDetails = true;
    $scope.showMedicoOptions = false;

    $scope.searchUser = function(item) {
        $scope.selectMedicoFromSearch = item;
        $scope.selectedMedico = Medico.get( {id: item.id}, function(){
            $scope.showDetails = false;
            $scope.showMedicoOptions = false;
            $state.go('atendimentoMedico');
        });
    };

    $scope.saveAgendamentoCadastro = function() {
        $state.go('atendimentoMedico.medico-agendamento');
    }
    
    //if ($state.is('atendimento')) {
    $scope.showDetails = true;
    $scope.showMedicoOptions = false;
    $scope.selectedMedico = new Medico({});
    $scope.medico = {};
    $scope.searchFiltro = "";
    $scope.situacoes = [];

//    if($stateParams.id) {
//      $scope.searchUser({ id: $stateParams.id });
//  }
    //}

    $scope.$on('updateUserBroadcast', function (event, value) {
        if (value == 'true') {
            $scope.searchUser( { id: $scope.selectedMedico.id } );
        }
    });

    $scope.$on('$viewContentLoaded', function() {
        if(!$scope.hasLoadedOnce) {
            firstLoadConfig();
            $scope.hasLoadedOnce = true;
        }
    });

    function firstLoadConfig() {
        $scope.hideSeachComponent = Auth.isAuthorized($scope.notAllowedSearch) && 
        !Auth.isAuthorized($scope.allowedSearch);
        if($scope.hideSeachComponent) {
            var login =  Auth.authz.idTokenParsed.preferred_username;
            retrieveProfessionalFromLogin(login);
        }
    }

    function retrieveProfessionalFromLogin(login) {
        var rParams = {
            login: login
        }
        servicesFactory.recuperarProfissional.get(
            rParams,
            function(response) {
                if(response) {
                    $scope.selectedMedico = response;
                    $scope.showDetails = false;
                    $scope.showMedicoOptions = false;
                    $state.go('atendimentoMedico');
                }
          });
    };
});
