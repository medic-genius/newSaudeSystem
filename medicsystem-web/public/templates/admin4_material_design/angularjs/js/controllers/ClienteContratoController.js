'use strict';

MedicsystemApp.controller('ClienteContratoController', function($scope, $state, $stateParams, servicesFactory) {

	var Contrato = servicesFactory.contratos;

	$scope.tipoClienteOptions = [
		{ id: 0, description: "Funcionário Real Saude" },
		{ id: 1, description: "Funcionario Público" },
		{ id: 2, description: "Particular Associado" },
		{ id: 3, description: "Empresarial" }
	];

	$scope.vinculoOptions = [
 		{ id: 0, description: "Estatuário" },
 		{ id: 1, description: "Sem Vínculo" },
 		{ id: 2, description: "Contrato" },
 		{ id: 3, description: "Temporário" },
 		{ id: 4, description: "A Disposição" }
 	];

	$scope.formaPagamentoOptions = [
  		{ id: 0, description: "Boleto Bancário" },
  		{ id: 1, description: "Débito Automático" },
  		{ id: 2, description: "Contra Cheque" },
  		{ id: 3, description: "Cartão Recorrente" },
  		{ id: 99, description: "A Disposição" }
  	];

	$scope.descontoOptions = [
	  	{ id:  0,  description: "0%" },
		{ id:  1,  description: "5%" },		
		{ id:  2, description: "5,12%" },
		{ id:  3, description: "5,31%" },
		{ id:  4, description: "6,59%" },		
		{ id:  5, description: "7,27%" },
		{ id:  6, description: "7,52%" },
		{ id:  7,  description: "7,7%" },		
		{ id:  8, description: "8,02%" },
		{ id:  9, description: "8,35%" },
		{ id:  10, description: "8,36%" },
		{ id:  11, description: "8,79%" },		
		{ id:  12, description: "8,94%" },		
		{ id:  13,  description: "10%" },
		{ id:  14, description: "10,63%" },
		{ id:  15,  description: "11,14%" },
		{ id:  16, description: "11,15%" },
		{ id:  17,  description: "11,69%" },
		{ id:  18,  description: "14,21%" },
		{ id:  19, description: "14,33%" },
		{ id:  20,  description: "15%" },
		{ id:  21, description: "15,41%" },		
		{ id:  22, description: "15,89%" },
		{ id:  23,  description: "16,69%" },
		{ id:  24, description: "16,72%" },
		{ id:  25, description: "18,21%" },
		{ id:  26, description: "18,57%" },
		{ id:  27,  description: "20%" },
		{ id:  28, description: "21,05%" },
		{ id:  29, description: "24,30%" },
		{ id:  30, description: "25,34%" },		
		{ id:  31, description: "25,56%" },		
		{ id:  32, description: "25,74%" },
		{ id:  33, description: "26,42%" },
		{ id:  34, description: "26,70%" },
		{ id:  35, description: "27,70%" },
		{ id:  36, description: "27,71%" },
		{ id:  37, description: "29,92%" },
		{ id:  38, description: "33,47%" },
		{ id:  39, description: "36,95%" },
		{ id:  40, description: "39,6%" },
		{ id:  41, description: "42,21%" },
		{ id:  42, description: "63,10%" }
	];

	$scope.refreshContratos = function() {
		if ($scope.selectedCliente && $scope.selectedCliente.id) {
			$scope.contratos = servicesFactory.clienteContratos.get({id: $scope.selectedCliente.id });
		}
	}

	if ($state.is('atendimentoCliente.cliente-contratos')) {
		$scope.refreshContratos();
    }

	if ($state.is('atendimentoCliente.cliente-contratos-editar')) {
		if ($stateParams.id) {            
			Contrato.get({ id: $stateParams.id }, function(dados){
                    
                $scope.selectedContrato = dados;
                
                var data = servicesFactory.contratoCliente.query({ idcontrato: $stateParams.id, idcliente: $scope.selectedCliente.id }, function(){
                    $scope.selectedContratoCliente = data;
                    if ($scope.selectedContrato.banco != null){
                        var dataBancosAgencias = servicesFactory.bancosAgencias.query({ id: $scope.selectedContrato.banco.id }, function(){
                            $scope.agenciaOptions = dataBancosAgencias;
                        });
                    }
                    if($scope.selectedContrato.empresaGrupo != null){
                        var dataEmpresagruposVendedores = servicesFactory.empresagruposVendedores.query({ id: $scope.selectedContrato.empresaGrupo.id }, function(){
                            $scope.vendedoresOptions = dataEmpresagruposVendedores;
                        });
                    }
                    
                    var dataContratosDependentes = servicesFactory.contratosDependentes.query({ idcontrato: $scope.selectedContrato.id, idcliente: $scope.selectedCliente.id  }, function(){
                        $scope.contratosDependentes = dataContratosDependentes;

                    });								
                });                    
                            
            });
            
			var dataEmpresaGrupos = servicesFactory.empresagrupos.query(function(){
				$scope.empresagrupoOptions = dataEmpresaGrupos;
			});

			var dataOrgaos = servicesFactory.orgaos.query(function(){
				$scope.orgaoOptions = dataOrgaos;
			});

			var dataBancos = servicesFactory.bancos.query(function(){
				$scope.bancoOptions = dataBancos;
			});
		}
    }
});