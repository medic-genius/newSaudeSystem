'use strict';

MedicsystemApp.controller('AtendimentoPesqServicoController', function($rootScope, $scope, $http,
		$timeout, Auth, servicesFactory, $filter, $state) {
	
	var Servico = servicesFactory.servicos;
	
	$scope.servicoDetalhe = []; 

	$scope.$on('$viewContentLoaded', function() {
		// initialize core components
		Metronic.initAjax();

	});
	
	 $scope.atualizarSelectServicos = function(text) {
	        if (text) {
	        	if (text.length > 1) {
		            var queryObject = {};
		            text = text.toLowerCase();
		            queryObject = { nmServico: text };
		
		            if(text){
		                var data = Servico.query(queryObject, function() {
		                    $scope.servicos = data;
		                    if (data.length == 0) {
		                        $scope.selectedServicoNotFound = "Nenhum serviço encontrado com os criterios.";
		                    } else {
		                        $scope.selectedServicoNotFound = "";
		                    }
		                });
		            }
	        	}
	        }
	    
	    };
	    
    $scope.addServico = function(item) {
		$scope.servico = item;
        $scope.servicoDetalhe = servicesFactory.servicosValores.query({ id: item.id });
    };

});