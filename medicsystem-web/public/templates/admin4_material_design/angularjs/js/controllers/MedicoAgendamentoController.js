'use strict';

MedicsystemApp.filter('moment', function () {
  return function ( input, momentFn /*, param1, param2, ...param n */ ) {
    var args = Array.prototype.slice.call(arguments, 2), momentObj = moment(input);
    return momentObj[momentFn].apply(momentObj, args);
  };
});

MedicsystemApp.controller('MedicoAgendamentoController', function($scope,$state,$filter,NgTableParams,servicesFactory,$timeout, toastr ) {
  
  $scope.enableSMS = ['administrador', 'gerenteunidade', 'gerentetelemarketing', 'gerenteatendimentocliente', 'gerenterecepcaomedicaodonto'];
  $scope.rolesViewDtFaturamentoLaudo = ['administrador', 'faturamento', 'corpoclinico'];
    
  function configurarTable( lista ) {
    $scope.tableParams = new NgTableParams({},{dataset:lista} );  
  };
        
  function contaStatus( lista ) {
    $scope.contStatusBloqueado = 0;
    $scope.contStatusLiberado = 0;
    $scope.contStatusAtendido = 0;
    $scope.contStatusFaltoso = 0;
    $scope.contStatusPresente = 0;
    $scope.total = lista.length;

    for ( var item in lista) {
      if(lista[item].inStatus == 0){
        $scope.contStatusBloqueado += 1;
      } else if(lista[item].inStatus  == 1){
        $scope.contStatusLiberado += 1;
      } else if(lista[item].inStatus  == 2){              
        $scope.contStatusAtendido += 1;
      } else if(lista[item].inStatus == 3){
        $scope.contStatusFaltoso += 1;
      } else if(lista[item].inStatus  == 4){
        $scope.contStatusPresente += 1;
      }
    }     
  };

  $scope.loadCalendarioSemanal = function(item) {
    
    $scope.calendarioSemanal = servicesFactory.funcionarioCalendarioSemanal.query({ 
      id: item.id,
      idUnidade: $scope.agendamento.unidade.id
    });

    $scope.pesquisarAgendamentos();   

  };
  
  $scope.pesquisarAgendamentos = function() {
    if ( $scope.agendamento.profissional.id && $scope.agendamento.dtAgendamento && $scope.agendamento.unidade.id ) {
      $scope.agendamentoDTO = servicesFactory.agendamentosMedico.query({
          id: $scope.agendamento.profissional.id,
          data: $filter('date')($scope.agendamento.dtAgendamento, "yyyy/MM/dd"),
          idUnidade: $scope.agendamento.unidade.id
          }, function() { 
            
            contaStatus($scope.agendamentoDTO);
            for(var i = 0; i < $scope.agendamentoDTO.length; ++i) {
                $scope.agendamentoDTO[i].showLaudoButton = !$scope.agendamentoDTO[i].dtFaturamentoLaudoFormatado;
                if($scope.agendamentoDTO[i].dtFaturamentoLaudoFormatado) {
                    var dt = moment($scope.agendamentoDTO[i].dtFaturamentoLaudoFormatado, "DD/MM/YYYY").format("MM/YYYY");
                    $scope.agendamentoDTO[i].dtFaturamentoLaudoFormatado = dt;
                }
            }
            configurarTable( $scope.agendamentoDTO );
            $scope.reloadSMS();
          });
      }
    
  };

  $scope.reloadSMS = function (){
    $scope.qtdEnviados = 0;
    for(var i = 0; i < $scope.agendamentoDTO.length; i++) {
      if($scope.agendamentoDTO[i].envioSms.length > 0) {
        $scope.agendamentoDTO[i].statusEnviado = false;
        for(var j=0; j< $scope.agendamentoDTO[i].envioSms.length; j++){
          if($scope.agendamentoDTO[i].envioSms[j].boEnviado){
            $scope.agendamentoDTO[i].statusEnviado = true;
            $scope.qtdEnviados++;
            break;
          }
        }
      }
    }
  }

  $scope.loadProfissional = function(item, clean) {
    clean = typeof clean !== 'undefined' ? clean : false;
            
    if (item && item.id) {
      $scope.profissionaisOptions = servicesFactory.profissionaisUnidade.query({ id: item.id });
    }

    if (clean) {
      $scope.agendamento.profissional = null;
      $scope.agendamento.dtAgendamento = new Date();
    }
  };

  $scope.searchAfterChangeDate = function() {
    $scope.pesquisarAgendamentos();
  }
  
  $scope.updateRegistroAgendamento = function(item, dtLaudo) {
            
      dtLaudo = moment(dtLaudo).format("YYYY-MM-DD");      
      servicesFactory.updateLaudoRegistroAgendamento.put({ idagendamento: item.idAgendamento, dtfatlaudo: dtLaudo }, function(response) {
          item.showLaudoButton = false;
      });      
  }     

  function inicio() {
    $scope.agendamento = {};
    $scope.dtfaturamentolaudo = {};
    $scope.agendamento.dtAgendamento = new Date();
    $scope.unidadeOptions = servicesFactory.unidades.query();
    $scope.minDate = new Date();
    $scope.MSG_MAX_LENGTH = 145;
    $scope.valideBotaoSMS = !$scope.checkEditRole($scope.enableSMS);
    $scope.viewDtLaudo = !$scope.checkEditRole($scope.rolesViewDtFaturamentoLaudo);
      console.log("status", $scope.viewDtLaudo);
      
    configDatePicker();       
  };

  if ( $state.is('agendamentoMedico') ) {
    inicio();
  };
  
    //-----------------------------------------------------

    //-----------------------------------------------------
    // Date picker configuration

   function configDatePicker() {
       $scope.format = ['MM/yyyy'];/*'dd/MM/yyyy',*/   
              
       $scope.optionsVencCartao = {
           formatYear: 'yyyy',
           minMode: 'month'           
       };
       
       var currentTime= new Date();
       $scope.minDateFat = {
           minDate: new Date(currentTime.getFullYear(),currentTime.getMonth()-1),
           maxDate: new Date(currentTime.getFullYear(), currentTime.getMonth())
       };
       
       
              
        /*$scope.status = {
            opened: false
        };
        
        $scope.minDate = '-1M'
        //$scope.minDate = new Date();
       // console.log("date:", $scope.minDate);
        
        $scope.dateOptions = {
            formatYear: 'yyyy',
            startingDay: 1
        };
                
        $scope.statusDtVencimento = {
            opened: false
        }

        $scope.disabled = function(date, mode) {
          return false; //( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.showDatePicker = function() {
            $scope.status.opened = true;
        }*/
    }

  $scope.myPopover = {

        isOpen: false,

        templateUrl: 'myPopoverMensagemAgendamento.html',

        msg: '',

        open: function open() {
          $scope.myPopover.isOpen = true;

          },

        close: function close() {
          $timeout(function() {
            angular.element('#popoverBtn').trigger('click');
          }, 0);
          $scope.myPopover.msg = '';
           
        }
  };

   function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase(), {allowHtml:true, tapToDismiss: false,timeOut: 60000} ); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() ); 
        } else if( type == 'warning' ) {
           toastr.warning(  message, title.toUpperCase() ); 
        }
    };

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }}


  $scope.enviarSMS = function() {
    $scope.qtdEnviados = 0;
    var FaltaMedico = servicesFactory.faltamedico;
    var listaMensagens = [];
    for (var i = 0; i < $scope.agendamentoDTO.length; i++) {
      var Mensagens = {
        nmInformativo :  $scope.myPopover.msg,
        nrCelular : $scope.agendamentoDTO[i].nrCelular,
        nrTelefone : $scope.agendamentoDTO[i].nrTelefone,
        idAgendamento : $scope.agendamentoDTO[i].idAgendamento,
        idCliente : $scope.agendamentoDTO[i].idCliente
      };
      listaMensagens.push(Mensagens);
    }
    FaltaMedico.save(null, listaMensagens, function(data) {
      var notSentCount = 0;
      for(var i = 0; i < $scope.agendamentoDTO.length; i++) {
        for(var j = 0; j < data.length; j++) {
          var tam = data.length;
          if($scope.agendamentoDTO[i].idCliente == data[j].idCliente) {
            $scope.agendamentoDTO[i].statusEnviado = false;
            for(var k=0; k < data[j].contatos.length; k++) {
              if(data[j].contatos[k].boEnviado) {
                $scope.agendamentoDTO[i].statusEnviado = true;
                $scope.qtdEnviados++;
                break;
              }
            }
            if(!$scope.agendamentoDTO[i].statusEnviado) {
              ++notSentCount;
            }
          }
        }
      }
      if(notSentCount == 0){
        messageToastr("sucesso","Mensagem enviada para todos os pacientes.", "SUCESSO");
      } else {
        var msg = 'A mensagem não foi enviada para ' + notSentCount;
        if(notSentCount > 1) {
          msg += ' pacientes.';
        } else {
          msg += ' paciente.';
        }
        messageToastr("warning", msg,"AVISO");
      }
    });

    $scope.myPopover.close();
   }


});

 MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 4000,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });