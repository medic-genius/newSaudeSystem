'use strict';

MedicsystemApp.controller('ClienteLaudosController', function($scope, $sce, $state,$stateParams,servicesFactory) {
    var Laudos = servicesFactory.laudos;

    	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + (d.getDate() + 1),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [day, month, year].join('/');
	}

    // 1 - cliente     0 - dependente
    $scope.tipoCliente = function ( tipo ){
        
         var listIds = [];

         if (tipo == 1){
            listIds.push($scope.selectedCliente.id);

            Laudos.get({
             listIdPaciente: listIds,
             tipoPaciente: 'C'
            }, function (dados){
                if (dados.length != 0){
                     $scope.showTable = true;
                }else if ( dados.length == 0)  $scope.showTable = false;

                for ( var i = 0; i < dados.length; i++){
                    dados[i].dtAtendimento = formatDate(dados[i].dtAtendimento);
                }
                $scope.historicoLaudos = dados;
            });

         }
         else {
            if ($scope.dependentes.length > 0){
                for (var i = 0; i < $scope.dependentes.length; i++){
                    var DependentesIds = $scope.dependentes[i].id;
                    listIds.push(DependentesIds);
                }
                Laudos.get({
                listIdPaciente: listIds,
                tipoPaciente: 'D'
                }, function (dados){
                    if (dados.length != 0){
                        $scope.showTable = true;
                    }else if ( dados.length == 0)  $scope.showTable = false;

                    for ( var i = 0; i < dados.length; i++){
                        dados[i].dtAtendimento = formatDate(dados[i].dtAtendimento);
                    }
                    $scope.historicoLaudos = dados;
                });
            } else $scope.showTable = false;
         }
    };

    $scope.pdf = function(arq){
        var decodedData = window.atob(arq);
        var blob = b64toBlob(decodedData, 'application/pdf');
		var fileURL = URL.createObjectURL(blob);		
	    window.open(fileURL , '_blank');
    };

    

    function inicio () {
        if ($scope.selectedCliente.id != null)
        $scope.dependentes = servicesFactory.clienteDependentes.get({id: $scope.selectedCliente.id });
       
	};

	inicio();
    
});
