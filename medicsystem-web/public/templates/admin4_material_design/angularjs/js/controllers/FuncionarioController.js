'use strict';

MedicsystemApp.controller('FuncionarioController', function($rootScope, $scope, $state, $stateParams, NgTableParams, servicesFactory, $modal, Auth, $timeout, toastr) {
	
    $scope.canAccessUploadFileRoles = ['administrador','diretoria','corpoclinico'];

    


    

	function configurarTable( lista ) {

		var initialParams = { count:10 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[10,15,30,50,100], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )          
  	};

    function verificarAutorizacaoAcessoUploadFile() {
        var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
        $scope.canAccessUpload = false;
        
        for ( role of userRoles ) {
            if ($scope.canAccessUploadFileRoles.indexOf(role) != -1) {
                $scope.canAccessUpload  =  true;
            }
        }
    };

      function loadGrupoFunionario() {
        $scope.optionsGrupoFuncionario = [
                    {id:null,nmGrupo:'Todos'},
                    {id:0,nmGrupo:'Funcionários'},
                    {id:1,nmGrupo:'Médicos/Dentistas'},
                    {id:2,nmGrupo:'Prestadores de Serviços'},
                    {id:3,nmGrupo:'Vendedores'},
                    {id:4,nmGrupo:'Estagiários'}

                ];
    };
    
    $scope.loadProfissional = function(item, clean) {
        clean = typeof clean !== 'undefined' ? clean : false;

        if (item && item.id) {
          $scope.profissionaisOptions = servicesFactory.
          funcionariosAtivos.query({ingrupofuncionario : $scope.grupoFuncionarioSelected.grupo.id});
        }

        if (clean) {
          $scope.agendamento.profissional = null;
          $scope.agendamento.dtAgendamento = new Date();
        }
    };
    
    $scope.openModalDigitalFuncionario = function(item) {

        var itemsFuncionarios = {funcionario : item,
                            serviceFuncionario : servicesFactory.funcionarioTotem};
                        
/*        $scope.saveDigitalFuncionario = item;
        serviceFuncionario = servicesFactory.funcionarioTotem,
        ite*/

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'cadDigitalFotoModalContent2.html',
            controller: 'ModalInstanceCadDigitalFotoCtrl2',
            backdrop: 'static',
            resolve: {
                    items: function () {
                        return itemsFuncionarios;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.getListaFuncionarios();
                console.info('Modal dismissed at: ' + new Date());             
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    };

    $scope.openModalAssinaturaDigitalFuncionario = function(item) {

        $scope.saveAssinaturaDigital = item;

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
    	    templateUrl: 'funcionarioAssinaturaDigital.html',
            controller: 'ModalFuncionarioAssinaturaDigitalController',
            backdrop: 'static',
            resolve: {
                    items: function () {
                        return $scope.saveAssinaturaDigital;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.getListaFuncionarios();
                console.info('Modal dismissed at: ' + new Date());             
                }, function () {
                  console.info('Modal dismissed at: ' + new Date());
            });
    };

    $scope.updateAutorizacaoTotem = function(funcionario){
   
        servicesFactory.funcionarioTotem.update({id: funcionario.id, autorizacao: funcionario.boAutorizacaoTotem},{});
    };
	
	$scope.getListaFuncionarios = function() {
		var params = {ingrupofuncionario : $scope.grupoFuncionarioSelected.grupo != null ? $scope.grupoFuncionarioSelected.grupo.id : null};
		$scope.funcionarios = servicesFactory.funcionariosAtivos.query(params,function( result ){
			configurarTable( $scope.funcionarios );
		});
	};

	var inicio = function() {
        
       verificarAutorizacaoAcessoUploadFile();
       loadGrupoFunionario();
       $scope.checkboxes = {items:[]};
       $scope.checkboxes.checked = false;

       $scope.MSG_MAX_LENGTH = 145;
       $scope.totalChecked = 0;

		$scope.funcionarios = null;
        $scope.grupoFuncionarioSelected = {};
		$scope.getListaFuncionarios();
        $scope.profissionaisOptions = servicesFactory.funcionariosAtivos.query({ingrupofuncionario : $scope.grupoFuncionarioSelected.grupo != null ? $scope.grupoFuncionarioSelected.grupo.id : null});
	};

    $rootScope.updateFuncionarios = function() {
        $scope.checkboxes.checked = false;
        var params = {ingrupofuncionario : $scope.grupoFuncionarioSelected.grupo.id};
        configurarTable( null );
        $scope.funcionarios = servicesFactory.funcionariosAtivos.query(params,function( result ){
            configurarTable( $scope.funcionarios );
        });
    };

    function formataObjetoFucionario(){

        

    var listaFuncionarioFormatado = [];

      angular.forEach($scope.funcionarios, function (funcionario) {
            if(funcionario.selected){

                 var formatObj = {nrCelular: funcionario.nrCelular,
                        nrTelefone: funcionario.nrTelefone,
                        nmInformativo: $scope.myPopover.msg}

                listaFuncionarioFormatado.push(formatObj);
            }  
        });
        return listaFuncionarioFormatado;
    };


    $scope.enviarSMS = function () {
        var funcionarios = formataObjetoFucionario();
            servicesFactory.informativoinfo.save({tipoenvio: 1},funcionarios,function(response){
                console.log("Response: ",response);
                toastr.success('', 'Mensagem enviada com sucesso', {timeout: 5000});
            });
        $scope.myPopover.close();
        
    };

    $scope.checkFuncionario = function (funcinario) {

        if( funcinario.selected) 
           $scope.totalChecked ++
        else
          $scope.totalChecked --;
    };

    $scope.checkAll = function () {
        $scope.totalChecked = 0;
        $scope.checkboxes.items = [];

        if ($scope.checkboxes.checked) {
           $scope.checkboxes.checked = true;
        } else {
            $scope.checkboxes.checked = false;
        }

        angular.forEach($scope.funcionarios, function (item) {
            item.selected = $scope.checkboxes.checked;

            if( $scope.checkboxes.checked){
                $scope.totalChecked ++;
            }
                
        });

    };

    $scope.myPopover = {

        isOpen: false,

        templateUrl: 'myPopoverMensagemAgendamento.html',

        msg: '',

        open: function open() {
          $scope.myPopover.isOpen = true;

          },

        close: function close() {
          $timeout(function() {
            angular.element('#popoverBtn').trigger('click');
          }, 0);
          $scope.myPopover.msg = '';
           
        }
  };

	inicio();

});





//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalInstanceCadDigitalFotoCtrl2', function ($rootScope, $scope, $state, $modalInstance, items) {

    $rootScope.digitalFotoClienteMsg = "Capturar Digital";
    var serviceFuncionario = items.serviceFuncionario;

    $scope.$watch('digitalCapturadaMsg', function() {
        if($rootScope.digitalCapturadaMsg != undefined){

            $rootScope.digitalFotoClienteMsg = $rootScope.digitalCapturadaMsg;
            $("#btnCadDigitalFoto").prop("disabled",true);            
        }
    });

    $scope.cadDigitalFoto = function () {
        var socket = io.connect('http://localhost:9092');

        socket.on('connect', function() {

        });

        socket.on('disconnect', function() {

        });
        
        var jsonObject = {id: items.funcionario.id, 
                          nmCliente: items.funcionario.nmFuncionario,
                          tipoPessoa: 'Funcionario'};

        $rootScope.digitalFotoClienteMsg = "Processando...";
        $rootScope.digitalCapturadaMsg = undefined;
        
        socket.emit('biometriaregisterevent', jsonObject);

        socket.on('biometriaregisterevent', function(data) {

            $rootScope.digitalCapturadaMsg = data;
            var typeEvent;

            if(data != undefined 
                && (data.digital != undefined)){

                serviceFuncionario.update( { id: items.funcionario.id,
                                         autorizacao : null,
                                         digital : data.digital,
                                         idDedoDigital : data.idDedoDigital }, {}, 
                    function( resp ) {
                        $rootScope.addDefaultTimeoutAlert("Digital ", "capturada", "success");
                        $rootScope.updateFuncionarios();
                        typeEvent = 'success';
                        socket.emit('disconnect');
                        socket.removeAllListeners();
                                          
                }, function(error){
                    $rootScope.addDefaultTimeoutAlert("Funcionario", "atualizar", "error");
                });

                //Cliente.get({id: items.id})
                
            } else {

                typeEvent = 'danger';
                $rootScope.alerts.push({
                    type: typeEvent, 
                    msg: data,
                    timeout: 10000
                });
            } 
 
            setLabels(); 
            Metronic.scrollTop();
            $modalInstance.close();
        });
    };

    $scope.exit = function () {

        setLabels();
        $modalInstance.close();
    };

    $scope.cancel = function () {

        setLabels();
        $modalInstance.dismiss('cancel');
    };

    function setLabels(){

        $rootScope.digitalFotoClienteMsg = "Capturar Digital";
        $rootScope.digitalCapturadaMsg = undefined;
    }
});


//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalFuncionarioAssinaturaDigitalController', function ($stateParams, toastr, $rootScope, $scope, $state, $modalInstance, items, servicesFactory) {
    
     function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };

	 $scope.salvarArquivo = function( ) {
	    if ( !isEmpty( $scope.uploadedFile ) ) {
	    	$scope.buttonDisabled = true;
	    	$scope.uploadAnexo($scope.uploadedFile);	
	    } 
	  };

	  function isEmpty(obj) {
	    for(var prop in obj) {
	        if(obj.hasOwnProperty(prop))
	            return false;
	    }

    	return true;
	 };


	  $scope.uploadAnexo = function (file) {
        $scope.file = file;

        getBase64($scope.file[0]);
           
	        
	   };

       function getBase64(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (result) {
                $scope.fileB64 = reader.result;
                servicesFactory.saveAssinaturaFuncionario.update( 
                    {id: items.id}, 
                    $scope.fileB64, 
                    function(dados){
                    $scope.exit();
                    messageToastr('sucesso',"Assinatura digital salva com sucesso","SUCESSO");  
			    });
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            messageToastr('erro',"Falha ao salvar a assinatura digital","ERRO");
            };
        
        }

       $scope.preencher = function(files,invalidFiles) {
        $scope.fileB64View = null;
        $scope.fileName = null;
        if ($scope.uploadedFile){
            $scope.fileName = $scope.uploadedFile[0].name;
            $scope.startLoad = true;
            getBase64View($scope.uploadedFile[0]);
        }
       };

       function getBase64View(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (result) {
                if (result){
                    $scope.$apply(function() {
                        $scope.fileB64View = reader.result;
                        $scope.startLoad = false;
                    });
                }
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
                $scope.startLoad = false;
            };
        };

	   $scope.inicializarForm = function() {
           $scope.startLoad = false;
           $scope.uploadedFile = null;
	   };
       
    $scope.exit = function () {

       $modalInstance.close();
    };
});