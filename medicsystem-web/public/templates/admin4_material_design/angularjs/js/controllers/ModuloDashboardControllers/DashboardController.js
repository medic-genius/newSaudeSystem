'use strict';

MedicsystemApp.controller('DashboardController', ['$rootScope', '$scope', '$http', '$timeout', 'mlReportFactory','$state','$stateParams', '$modal', function($rootScope, $scope, $http, $timeout, mlReportFactory,$state, $stateParams, $modal) {
    $scope.enable = ['administrador'];
    $scope.valideBotao = !$scope.checkEditRole($scope.enable);
    var DRCONSULTA_ID = 1;
    var DENTAL_ID = 2;
    var ACADEMIA_ID = 3
        
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        Metronic.initAjax();
    });
    
    $scope.getDetalheContratoDb = function( contrato ) {
        contrato.expanded = true;    
	};

    $scope.getDetalhesFaturamento = function(){
        loadModalFaturamento( $scope.report0001.year,  $scope.report0001.month);
    }

    function loadModalFaturamento(){
        var modalInstance = $modal.open({
            animation: true,
            backdrop: 'static',
            templateUrl: 'templates/admin4_material_design/angularjs/views/dashboard/modal/dashboard-faturamento-modal.html',
            controller: 'ModalFaturamentoDetalhes',
            size: 'lg',
            resolve:{
                ano: function(){
                    return $scope.report0001.year;
                },
                mes: function(){
                    return $scope.report0001.month;
                },
                empresaGrupo: function(){
                    return $stateParams.idEmpresaGrupo;
                }
            }
        });
    }

    function definirLabelListReport10( colecao ) {
        if( colecao.length > 0  ) {
            colecao.forEach( function( item, index) {
                if( item.situacaocontrato == 'NOVOS CONTRATOS (PROPOSTAS)' )
                    item.id = 1;
                else if( item.situacaocontrato == 'NOVOS CONTRATOS (SEM PROPOSTAS)' )
                    item.id = 2;
                else if( item.situacaocontrato == 'NOVOS CONTRATOS (MIGRAÇÃO)' )
                    item.id = 3;
                else if( item.situacaocontrato == 'CONTRATOS INATIVADOS (EMPRESA)' )
                    item.id = 4;
                else if( item.situacaocontrato == 'CONTRATOS INATIVADOS (CLIENTE)')
                    item.id = 5;
                else
                    item.id = 9;
            } )
        }
        
        return colecao;
    };
    
    function inicializarTotais() {
		$scope.totais = {};
		$scope.totais.agendamentosAgendados = 0;
		$scope.totais.agendamentosCriados = 0;
        $scope.totais.agendamentosAtendidos = 0;
        $scope.totais.agendamentosBloqueados = 0;
        $scope.totais.agendamentosFaltosos = 0;
	};

	function calcularTotais( lista ) {
		if( lista.length > 0) {
            
			for( var i = 0; i < lista.length; i++ ) {
                if(lista[i].tipo == 'AGENDADOS' && lista[i].unidade != 'DESCONHECIDA'){
                    $scope.totais.agendamentosAgendados = $scope.totais.agendamentosAgendados + lista[i].totalagendamento;
                    $scope.totais.agendamentosAtendidos = $scope.totais.agendamentosAtendidos + lista[i].totalatendido;
                    $scope.totais.agendamentosBloqueados = $scope.totais.agendamentosBloqueados + lista[i].totalbloqueado;
                    $scope.totais.agendamentosFaltosos = $scope.totais.agendamentosFaltosos + lista[i].totalfaltoso;
                }
                else if(lista[i].tipo == 'CRIADOS' && lista[i].unidade != 'DESCONHECIDA')
				    $scope.totais.agendamentosCriados = $scope.totais.agendamentosCriados  + lista[i].totalagendamento;
			}   
            
            if( isNaN($scope.totais.agendamentosAgendados) )
                $scope.totais.agendamentosAgendados = 0;            
            else if( isNaN($scope.totais.agendamentosAtendidos) )
                $scope.totais.agendamentosAtendidos = 0;
            else if( isNaN($scope.totais.agendamentosBloqueados) )
                $scope.totais.agendamentosBloqueados = 0;
            else if( isNaN($scope.totais.agendamentosFaltosos) )
                $scope.totais.agendamentosFaltosos = 0;
		}        
	}
    
    //BEGIN REPORT 1
    $scope.nextMonthReport0001 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0001.year, $scope.report0001.month);
        $scope.report0001.year = nextMonthOfYear.year;
        $scope.report0001.month = nextMonthOfYear.month;
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        
        report0001query(true);
        report0007query();
    };

    $scope.previousMonthReport0001 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0001.year, $scope.report0001.month);
        $scope.report0001.year = previousMonthOfYear.year;
        $scope.report0001.month = previousMonthOfYear.month;
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        
        report0001query(true);
        report0007query();
    };

    var report0001query = function(updateGraph) {
        
         if( $stateParams.idEmpresaGrupo == 1 ) { // Dr. Consulta
            mlReportFactory.report0001.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    var pie = D3PieUtils.init("report0001piechart", data.graphData);
                    if(updateGraph) {
                        pie.destroy();
                        pie = D3PieUtils.init("report0001piechart", data.graphData);
                    }
                    $scope.report0001.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 2 ) { // Dental
            mlReportFactory.dentalreport0001.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    var pie = D3PieUtils.init("report0001piechart", data.graphData);
                    if(updateGraph) {
                        pie.destroy();
                        pie = D3PieUtils.init("report0001piechart", data.graphData);
                    }
                    $scope.report0001.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 3 ) { // Academia
            mlReportFactory.academiareport0001.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    var pie = D3PieUtils.init("report0001piechart", data.graphData);
                    if(updateGraph) {
                        pie.destroy();
                        pie = D3PieUtils.init("report0001piechart", data.graphData);
                    }
                    $scope.report0001.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }
       
    };    
    //END REPORT 1
    
    //BEGIN REPORT 2
    $scope.nextMonthReport0002 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0002.year, $scope.report0002.month);
        $scope.report0002.year = nextMonthOfYear.year;
        $scope.report0002.month = nextMonthOfYear.month;
        $scope.report0002.firstDay = nextMonthOfYear.firstDay;
        $scope.report0002.lastDay = nextMonthOfYear.lastDay;

        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        
        report0002query();
    };

    $scope.previousMonthReport0002 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0002.year, $scope.report0002.month);
        $scope.report0002.year = previousMonthOfYear.year;
        $scope.report0002.month = previousMonthOfYear.month;
        $scope.report0002.firstDay = previousMonthOfYear.firstDay;
        $scope.report0002.lastDay = previousMonthOfYear.lastDay;

        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        
        report0002query();
    };
        
    var report0002query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0002.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                    $scope.report0002.data = data.metadata;
                    mlReportFactory.report0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                        function(data) {
                           $scope.report0002.dataReport10 = definirLabelListReport10( data.metadata );                          
                        }
                    );
                }
            );
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0002.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                    $scope.report0002.data = data.metadata;
                    mlReportFactory.dentalreport0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                        function(data) {
                            $scope.report0002.dataReport10 = definirLabelListReport10( data.metadata );                          
                        }
                    );

                }
            );
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0002.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                    $scope.report0002.data = data.metadata;
                    mlReportFactory.academiareport0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                        function(data) {
                           definirLabelListReport10( data.metadata );
                           $scope.report0002.dataReport10 = definirLabelListReport10( data.metadata );                            
                        }
                    );
                });
        }
    };    
    //END REPORT 2
        
    //BEGIN REPORT 3
    $scope.nextMonthReport0003 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0003.year, $scope.report0003.month);
        $scope.report0003.year = nextMonthOfYear.year;
        $scope.report0003.month = nextMonthOfYear.month;
        $scope.report0003.firstDay = nextMonthOfYear.firstDay;
        $scope.report0003.lastDay = nextMonthOfYear.lastDay;

        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        
        report0003query();
        report0009query();
    };

    $scope.previousMonthReport0003 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0003.year, $scope.report0003.month);
        $scope.report0003.year = previousMonthOfYear.year;
        $scope.report0003.month = previousMonthOfYear.month;
        $scope.report0003.firstDay = previousMonthOfYear.firstDay;
        $scope.report0003.lastDay = previousMonthOfYear.lastDay;

        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        
        report0003query();        
        report0009query();
    };

    var report0003query = function() {
        if( $stateParams.idEmpresaGrupo == 1 ) { // Dr. Consulta
            mlReportFactory.report0003.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0003.data = data.metadata;                   
                    report0009query();
                    report0014query();
                    report0015query();
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 2 ) { // Dental
            mlReportFactory.dentalreport0003.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0003.data = data.metadata;
                    report0009query();
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 3 ) { // Academia
            mlReportFactory.academiareport0003.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0003.data = data.metadata;
                    mlReportFactory.academiareport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                        function(data){
                            $scope.report0003.dataReport09 = data.metadata;
                            report0009query();
                        }
                    );
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }
    };
    //END REPORT 3
    
    //BEGIN REPORT 4
    var report0004query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0004
            .query( function( data ) {
                $scope.report0004 = data.metadata;
             });  
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0004
            .query( function(data) {
                $scope.report0004 = data.metadata;
            });
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
             mlReportFactory.academiareport0004
            .query( function(data) {
                $scope.report0004 = data.metadata;
            });
        }
    };
    //END REPORT 4
    
    //BEGIN REPORT 6
     var report0006query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0006
            .query( function(data) {                
                C3Utils.drawFaturamentoMesAMes("#report0006chart", data);
            }); 
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0006
            .query( function(data) {                
                C3Utils.drawFaturamentoMesAMes("#report0006chart", data);
            });
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0006
            .query( function(data) {                
                C3Utils.drawFaturamentoMesAMes("#report0006chart", data);
            });
        }
    };    
    //END REPORT 6
    
    //BEGIN REPORT 7    
    var report0007query = function() {
        if( $stateParams.idEmpresaGrupo == 1 ) { // Dr. Consulta
            mlReportFactory.report0007.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    $scope.report0007.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 2 ) { // Dental
            mlReportFactory.dentalreport0007.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    $scope.report0007.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 3 ) { // Academia
            mlReportFactory.academiareport0007.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
                function(data) {
                    $scope.report0007.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }
    };
    //END REPORT 7
    
    //BEGIN REPORT 9
    var report0009query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;                    
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }  
    }; 
    //END REPORT 9

    //BEGIN REPORT 10    
    var report0010query = function( idEmpresaGrupo ) {
        var dataReport10 = {};
        
        if( idEmpresaGrupo == DRCONSULTA_ID ) { 
            mlReportFactory.report0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                   dataReport10 = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( idEmpresaGrupo == DENTAL_ID ) { 
            mlReportFactory.dentalreport0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                   dataReport10 = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0010.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
                function(data) {
                    dataReport10 = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }
        
        return dataReport10;
    };
    //END REPORT 10
    
    //BEGIN REPORT 11
    var report0011query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0011
            .query( function( data ) {
                $scope.report0011 = data.metadata;
             });  
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0011
            .query( function(data) {
                $scope.report0011 = data.metadata;
            });
        } 
    };
    //END REPORT 11
    
    //BEGIN REPORT 12
    var report0012query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0012
            .query( function( data ) {
                $scope.report0012 = data.metadata;               
             });  
        } else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0012
            .query( function(data) {
                $scope.report0012 = data.metadata;
            });
        } 
        else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0012
            .query( function(data) {
                $scope.report0012 = data.metadata;
            });
        } 
    };
    //END REPORT 12
    
     //BEGIN REPORT 13
    $scope.nextMonthReport0013 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0013.year, $scope.report0013.month);
        $scope.report0013.year = nextMonthOfYear.year;
        $scope.report0013.month = nextMonthOfYear.month;
        $scope.report0013.firstDay = nextMonthOfYear.firstDay;
        $scope.report0013.lastDay = nextMonthOfYear.lastDay;

        $scope.report0013.my = DateUtils.prettyMonth()[$scope.report0013.month] + "\\" + $scope.report0013.year;
        
        report0013query();        
    };

    $scope.previousMonthReport0013 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0013.year, $scope.report0013.month);
        $scope.report0013.year = previousMonthOfYear.year;
        $scope.report0013.month = previousMonthOfYear.month;
        $scope.report0013.firstDay = previousMonthOfYear.firstDay;
        $scope.report0013.lastDay = previousMonthOfYear.lastDay;

        $scope.report0013.my = DateUtils.prettyMonth()[$scope.report0013.month] + "\\" + $scope.report0013.year;
        
        report0013query();
    };

    var report0013query = function() {
        inicializarTotais();
        
        if( $stateParams.idEmpresaGrupo == 1 ) { // Dr. Consulta
            mlReportFactory.report0013.query({ inicio: $scope.report0013.firstDay, fim: $scope.report0013.lastDay },
                function(data) {
                    $scope.report0013.data = data.metadata;
                    calcularTotais($scope.report0013.data);
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == 2 ) { // Dental
            mlReportFactory.dentalreport0013.query({ inicio: $scope.report0013.firstDay, fim: $scope.report0013.lastDay },
                function(data) {
                    $scope.report0013.data = data.metadata;
                    calcularTotais($scope.report0013.data);                   
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        }
    };   
    //END REPORT 13
    
    //BEGIN REPORT 14 - Faturamento Despesa Associado por Unidade
    var report0014query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0014.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0014.data = data.metadata;                    
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } 
        /*
        else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } 
        */
    }; 
    //END REPORT 14
    
    //BEGIN REPORT 15 - Faturamento Despesa Não Associado por Unidade
    var report0015query = function() {
        if( $stateParams.idEmpresaGrupo == DRCONSULTA_ID ) {
            mlReportFactory.report0015.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0015.data = data.metadata;                    
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } 
        /*
        else if( $stateParams.idEmpresaGrupo == DENTAL_ID ) {
            mlReportFactory.dentalreport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } else if( $stateParams.idEmpresaGrupo == ACADEMIA_ID ) {
            mlReportFactory.academiareport0009.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
                function(data) {
                    $scope.report0009.data = data.metadata;
                },
                function(data) {
                    console.log(JSON.stringify(data));
                }
            );
        } 
        */
    }; 
    //END REPORT 15
               
    function inicioDashBoard() {
        
        $scope.report0001 = {};
        $scope.report0001.data = [];
        $scope.report0001.year = DateUtils.getYear();
        $scope.report0001.month = DateUtils.getMonth();
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        
        
        $scope.report0002 = {};
        $scope.report0002.data = [];
        $scope.report0002.year = DateUtils.getYear();
        $scope.report0002.month = DateUtils.getMonth();
        $scope.report0002.firstDay = DateUtils.getFirstDayISO();
        $scope.report0002.lastDay = DateUtils.getLastDayISO();
        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        
        $scope.report0003 = {};
        $scope.report0003.data = [];
        $scope.report0003.year = DateUtils.getYear();
        $scope.report0003.month = DateUtils.getMonth();
        $scope.report0003.firstDay = DateUtils.getFirstDayISO();
        $scope.report0003.lastDay = DateUtils.getLastDayISO();
        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        
        $scope.report0007 = {};
        $scope.report0007.data = [];
        
        $scope.report0009 = {};
        $scope.report0009.data = [];
        
        $scope.report0010 = {};
        $scope.report0010.data = [];
        $scope.report0010.year = DateUtils.getYear();
        $scope.report0010.month = DateUtils.getMonth();
        $scope.report0010.firstDay = DateUtils.getFirstDayISO();
        $scope.report0010.lastDay = DateUtils.getLastDayISO();
        $scope.report0010.my = DateUtils.prettyMonth()[$scope.report0010.month] + "\\" + $scope.report0010.year;
        
        $scope.report0013 = {};
        $scope.report0013.data = [];
        $scope.report0013.year = DateUtils.getYear();
        $scope.report0013.month = DateUtils.getMonth();
        $scope.report0013.firstDay = DateUtils.getFirstDayISO();
        $scope.report0013.lastDay = DateUtils.getLastDayISO();
        $scope.report0013.my = DateUtils.prettyMonth()[$scope.report0013.month] + "\\" + $scope.report0013.year;
        
        $scope.report0014 = {};
        $scope.report0014.data = [];
        
        $scope.report0015 = {};
        $scope.report0015.data = [];
       
        
        inicializarTotais();

        //colocar a chamada para ver as informações iniciais
                
        report0001query();
        report0002query();
        report0003query();
        report0004query();
        report0006query();
        report0007query();
        report0009query();
        report0011query();
        report0012query();
        report0013query();
        report0014query();
        report0015query();
    };
    
    
    if($state.is('dashboard')) {
        if( $stateParams.idEmpresaGrupo ) {
            $scope.empresaGrupo = $stateParams.idEmpresaGrupo;
            inicioDashBoard();
        }
    }
    
    starModalMapa();
    //INICIANDO CALENDARIO
    $scope.dataFiltro = {};
    $scope.dataFiltro.date = {startDate:'' , endDate: ''};
    $scope.maxDate = new Date();
    $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };
    
    function starModalMapa () {
        initialize();
        var infoWindow = new google.maps.InfoWindow();
        var map;
        $scope.markers = [];
        $scope.myLatlng = new google.maps.LatLng(-3.0949178, -60.04252009999999);

    function initialize () {

        var mapOptions = {
            zoom: 11,
            center: $scope.myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var imagemDrConsulta = {
            url: 'assets/global/img/mapa/DR+CONSULTA.png',
            scaledSize: new google.maps.Size(32, 32)
        }
        
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var p;
        //MARKERS CLINICAS
        var markerGrandeCircular = new google.maps.Marker({
            position: new google.maps.LatLng(-3.0408059,-59.941559),
            map: map,
            icon: imagemDrConsulta
        }); 
        var markerJoaquimNabuco = new google.maps.Marker({
            position: new google.maps.LatLng(-3.1224223,-60.0206788),
            map: map,
            icon: imagemDrConsulta
        }); 
        var markerTaruma = new google.maps.Marker({
            position: new google.maps.LatLng(-3.1232666,-60.0217416),
            map: map,
            icon: imagemDrConsulta
        });

        //INICIANDO MAPA COM 30 DIAS SELECIONADO
        $scope.dateFim =  moment().format("YYYY-MM-DD");
        $scope.dateInicio = moment().subtract(30, 'days').format("YYYY-MM-DD");
        // INICIANDO MAPA COM NAO ASSOCIADOS SELECIONADO
        $scope.tipo = [2];
        // INICIANDO MAPA COM TARUMÃ SELECIONADO
        $scope.idUnidade = [2];
        // REQUISICAO BACK-END
        mlReportFactory.enderecosmapa.query({
                    inTipo: $scope.tipo,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim,
                    idUnidade: $scope.idUnidade
                } ,function (dados) {
                    for (var i=0; i< dados.length; i++){
               
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                                    
                });
        // FIM REQUISICAO BACK
    }
        $scope.createMarker = function(lat,lng) { 
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat,lng),
            map: map
        });        
        
                $scope.markers.push(marker); 
        }
        
       $scope.deleteMarkers = function() {
              for (var i = 0; i < $scope.markers.length; i++) {
                  $scope.markers[i].setMap(null);
              }
             $scope.markers = [];
                                  
            }

        //start of modal google map
        $('#mapmodals').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter($scope.myLatlng);
        });
        //end of modal google map
    };

    
        //ID UNIDADE -> 1 = JOAQUIM NABUCO; 2 = TARUMA; 7 = GRANDE CIRCULAR 
        //TIPO CLIENTE -> 0 = TODOS; 1 = ASSOCIADOS; 2 = NAO ASSOCIADOS 
        $scope.buscaUnidade = function (id){
        
            var retornoUnid = $scope.idUnidade.indexOf(id);
                
            if (retornoUnid < 0){
                $scope.idUnidade.push(id);
            }    
            else {
                $scope.idUnidade.splice(retornoUnid,1);
            }
            
          
        //REQUISICAO BACK-END
        if ($scope.tipo.length != 0 && $scope.dateInicio != null  && $scope.dateFim != null && $scope.idUnidade.length != 0){
            $scope.deleteMarkers();
            if ($scope.tipo.length == 2){

                mlReportFactory.enderecosmapa.query({
                    inTipo: 0,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim,
                    idUnidade: $scope.idUnidade
                } ,function (dados) {
                 for (var i=0; i< dados.length; i++){
                
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                    
                });
            } else if ($scope.tipo.length == 1){
                
                mlReportFactory.enderecosmapa.query({
                    inTipo: $scope.tipo,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim,
                    idUnidade: $scope.idUnidade
                } ,function (dados) {
                    for (var i=0; i< dados.length; i++){
               
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                                    
                });
                  
            }
            }else{
                $scope.deleteMarkers();
            }
        //FIM REQUISICAO
        }

       

    
        //ID UNIDADE -> 1 = JOAQUIM NABUCO; 2 = TARUMA; 7 = GRANDE CIRCULAR 
        //TIPO CLIENTE -> 0 = TODOS; 1 = ASSOCIADOS; 2 = NAO ASSOCIADOS 
        $scope.buscaClientes = function (tipo){
        
            var retorno = $scope.tipo.indexOf(tipo);
                
            if (retorno < 0){
                $scope.tipo.push(tipo);
            }    
            else {
                $scope.tipo.splice(retorno,1);
            }

        //REQUISICAO BACK-END
        if ($scope.tipo.length != 0 && $scope.dateInicio != null  && $scope.dateFim != null && $scope.idUnidade.length != 0){
            $scope.deleteMarkers();
            if ($scope.tipo.length == 2){
                mlReportFactory.enderecosmapa.query({
                    inTipo: 0,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                 for (var i=0; i< dados.length; i++){
                
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                    
                });
            } else if ($scope.tipo.length == 1){
                mlReportFactory.enderecosmapa.query({
                    inTipo: $scope.tipo,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                    for (var i=0; i< dados.length; i++){
               
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                                    
                });
                  
            }
            }else{
                $scope.deleteMarkers();
            }
          
        }
        //FIM REQUISICAO


        //TIPO CLIENTE -> 0 = TODOS; 1 = ASSOCIADOS; 2 = NAO ASSOCIADOS
        //ID UNIDADE -> 1 = JOAQUIM NABUCO; 2 = TARUMA; 7 = GRANDE CIRCULAR  
        $scope.periodo = function (dias){
            var dias = dias;
            $scope.dateFim = moment().format("YYYY-MM-DD");
            $scope.dateInicio = moment().subtract(dias, 'days').format("YYYY-MM-DD");

        //REQUISICAO BACK-END 
        if ($scope.tipo != null && $scope.dateInicio != null  && $scope.dateFim != null && $scope.idUnidade.length != 0){
         $scope.deleteMarkers();
            if ($scope.tipo.length == 2){
                mlReportFactory.enderecosmapa.query({
                    inTipo: 0,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                   for (var i=0; i< dados.length; i++){
       
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }  
                        
                    });
            } else if ($scope.tipo.length == 1){
                mlReportFactory.enderecosmapa.query({
                    inTipo: $scope.tipo,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                  for (var i=0; i< dados.length; i++){
                 
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }    
                });
            }
        }
        else{
                $scope.deleteMarkers();
            }
        //FIM REQUISICAO
        }

        //TIPO CLIENTE -> 0 = TODOS; 1 = ASSOCIADOS; 2 = NAO ASSOCIADOS 
        //ID UNIDADE -> 1 = JOAQUIM NABUCO; 2 = TARUMA; 7 = GRANDE CIRCULAR 
        $scope.calendarFunc = function (data){
            $scope.dateInicio = data.model.startDate._d;
            $scope.dateFim = data.model.endDate._d;
            $scope.dataFiltro.date = {startDate: $scope.dateInicio, endDate: $scope.dateFim}; 

        //REQUISICAO BACK-END
        if ($scope.tipo != null && $scope.dateInicio != null  && $scope.dateFim != null && $scope.idUnidade.length != 0){
         $scope.deleteMarkers();
            if ($scope.tipo.length == 2){
                mlReportFactory.enderecosmapa.query({
                    inTipo: 0,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                    for (var i=0; i< dados.length; i++){
                     
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                   }
                 
                    });
            } else if ($scope.tipo.length == 1){
                mlReportFactory.enderecosmapa.query({
                    inTipo: $scope.tipo,
                    dtInicio: $scope.dateInicio,
                    dtFim: $scope.dateFim
                } ,function (dados) {
                    for (var i=0; i< dados.length; i++){
                     
                    $scope.createMarker(dados[i].latitude,dados[i].longitude);   
                    }
                 
                    });
            }
        }
        else{
             $scope.deleteMarkers();
        }
        //FIM REQUISICAO
        }
    




//-------------- TRANFORMANDO ADDRESS TO LATITUDE LONGITUDE -------------

    $scope.geocoder = new google.maps.Geocoder();
    $scope.geocodeAddress = function (adress, id) {
            $scope.geocoder.geocode({address: adress}, function (results,status){
                
                if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
            
                
                $scope.clienteUpdate.push({
                    idCliente: id,
                    latitude: lat,
                    longitude: lng
                    
                });
                $scope.check = true;
        
                }
                else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT || status == google.maps.GeocoderStatus.UNKNOWN_ERROR) {
                setTimeout(function() {$scope.geocodeAddress(adress,id)},4000); 
                $scope.check = false; 
                }

                else {
                 
                        $scope.clienteUpdate.push({
                        idCliente: id,
                        latitude: 100,
                        longitude: 100
                    });
                $scope.check = true;
             
            }
            
            $scope.aux = $scope.clienteUpdate.length;
             console.log($scope.aux);
             
             if ($scope.aux == 100 && $scope.check == true){
             
                $scope.disabledButton = false;
                mlReportFactory.updatelatitudelongitude.update({},$scope.clienteUpdate, function(result){
                    console.log("resultado final: ",result);
            
                });
            }

             });
             
        };

        $scope.cadastrarLatLng = function (){
        $scope.clienteUpdate = [];  
        $scope.check = false;
        $scope.aux = 0;
        $scope.disabledButton = true;
        $scope.cliente = [];
            mlReportFactory.latitudelongitude.query({}, function(dados){
            
            for (var i=0; i< dados.length; i++){
                    $scope.cliente[i] = {
                        address: "" + dados[i].nmLogradouro + "," + dados[i].nrNumero + "," + dados[i].nmBairro + ",Amazonas, Brasil",
                        id: dados[i].idCliente
                    }
                    $scope.geocodeAddress($scope.cliente[i].address,$scope.cliente[i].id);
            
            }
            
        });
        }

}]);
MedicsystemApp.controller('ModalFaturamentoDetalhes', function($scope, $modalInstance, mlReportFactory, NgTableParams, ano, mes, empresaGrupo){  
    function inicio(){

        $scope.ano = ano;
        $scope.mes = mes;
        $scope.empresaGrupo = empresaGrupo;

        report0016query();
        report0018query();
    }
        var DRCONSULTA_ID = 1;
        var DENTAL_ID = 2;
        var ACADEMIA_ID = 3;

    //BEGIN REPORT 16 - Faturamento por unidade 
    function report0016query(){
        if( $scope.empresaGrupo == ACADEMIA_ID){
            
            mlReportFactory.academiareport0016.query({ano: $scope.ano, mes: $scope.mes}, function(data){
                $scope.report0016 = data.metadata;
            });
        }   
    }

    //end report 16

    //begin report18 - faturamento unidade pago
    function report0018query(){
        if( $scope.empresaGrupo == ACADEMIA_ID){
            mlReportFactory.academiareport0018.query({ano: $scope.ano, mes: $scope.mes}, function(data){
                $scope.report0018 = data.metadata;
            });
        }   
    }

    $scope.getDetalheUnidades = function(infoUnidade){
        infoUnidade.expanded = true;
        mlReportFactory.academiareport0017.query({ano: $scope.ano, mes: $scope.mes, unidade: infoUnidade.idUnidade}, function(data){
            infoUnidade.dataReport17 = data.metadata;
        });
    }

    $scope.getDetalhePagoUnidades = function(unidade){
        unidade.expanded = true;
        mlReportFactory.academiareport0019.query({ano: $scope.ano, mes: $scope.mes, unidade: unidade.idUnidade}, function(data){
            unidade.dataReport19 = data.metadata;
            console.log("unidade", unidade);
        });
    }

    $scope.close = function(){
        $modalInstance.close();
    };
    inicio();
});