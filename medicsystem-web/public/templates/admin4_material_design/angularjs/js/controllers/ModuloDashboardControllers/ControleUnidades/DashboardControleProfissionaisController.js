'use strict'
MedicsystemApp.controller('DashboardControleProfissionaisController', function($scope, 
	$timeout, $interval, toastr, servicesFactory) {

	function generateProfessionalGraph(value) {
		if(!value) {
			value = 0.0
		}
        
		if($scope.professionalChart) {
			$scope.professionalChart.load({
				columns: [
						['Atendimento', value]
				]
			});
			return;
		}
		$scope.professionalChart = c3.generate({
			bindto: "#professionalsChart",
			data: {
				columns: [
						['Atendimento', value]
				],
				type: 'gauge',
			},
			gauge: {
               label: {
                    show: false // to turn off the min/max labels.
               },
            },
			color: {
				pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
				threshold: {
					values: [81.99, 90.0, 99.9, 100]
				}
			},
			size: {
				height: 200
			}
		});
	}

	function loadData() {
		servicesFactory.dashboardControleUnidadesData.get(
			function(response) {
				if(response) {
					$scope.listProfissionais = response.monitoramento;
					$scope.listAlertas = response.monitoramentoAlert;
					$scope.total = response.nrTotal;
                    $scope.totalOk = response.nrTotalOk;
                    $scope.valorMonitor = response.valorMonitor;                      
					generateProfessionalGraph(response.valorMonitor);
					if(!$scope.updateDataInterval) {
						$scope.updateDataInterval = $interval(function() {
							loadData();
						}, 60000);
					}
				}
			},
			function(err) {
				toastr.error('Tente novamente ou contate a T.I.', 'Não foi possível carregar dados');
			}
		);
	}

	$scope.getRegisterValue = function(register) {
		return register || "Sem registro";
	}

	$scope.formatName = function(nameStr) {
		if(!nameStr) {return;}
		var nmArr = nameStr.split(" ");
		var nName = nmArr[0] + (nmArr.length > 0 ?  " " + nmArr[nmArr.length - 1] : '');
		return nName;
	}

	$scope.formatMobileNum = function(numStr) {
		if(!numStr) { return 'Sem contato';}
		// var newNum = '';
		// if(numStr.length == 9) {
		// 	newNum = numStr.substring(0, 5) + "-" + numStr.substring(5);
		// } else if(numStr.length == 11) {
		// 	newNum = "(" + numStr.substring(0, 2) + ") " + numStr.substring(2, 7) + "-" + numStr.substring(7);
		// }
		return numStr;
	}

	$scope.$on('$destroy', function() {
		if($scope.updateDataInterval) {
			$interval.cancel($scope.updateDataInterval);
		}
	});

	loadData();
});