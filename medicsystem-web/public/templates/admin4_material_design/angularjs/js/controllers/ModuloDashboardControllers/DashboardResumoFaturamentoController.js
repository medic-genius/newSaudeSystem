'use strict';

MedicsystemApp.controller('DashboardResumoFaturamentoController', function($rootScope,$scope,$state,$stateParams,mlReportFactory,Auth) {
	
    
    function getIntervaloDataSelecionada() {
        if( $scope.dataFiltro.date.startDate != null && $scope.dataFiltro.date.endDate != null ) {
    
            var intervalo = {};
            var dtInicio = moment( $scope.dataFiltro.date.startDate).format("YYYY-MM-DD");
            var dtFim = moment( $scope.dataFiltro.date.endDate).format("YYYY-MM-DD");
            var dataValida = moment($scope.dataFiltro.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };
    
    
    function configurarDateFiltroSimplificado() {

        $scope.mostrarCalendario = false;
        $scope.mostrarCustomizado = false;
    
        $scope.rangesDateRangePicker =  {           
            'Hoje': [moment(), moment()],            
            'Últimos 30 dias': [moment().subtract(30, 'days'), moment()],
            'Últimos 60 dias': [moment().subtract(60, 'days'), moment()],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
            'Últimos 120 dias': [moment().subtract(120, 'days'), moment()],
            'Últimos 180 dias': [moment().subtract(180, 'days'), moment()],
        }
    };
    
    
    function configurarDateFiltro() {

        $scope.mostrarCalendario = true;
        $scope.mostrarCustomizado = false;

        $scope.localeDateRangePicker = {
            "format": "DD/MM/YYYY", "separator": " - ", "applyLabel": "Feito",
            "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
            "customRangeLabel": "Customizado", "weekLabel": "S",
            "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            "monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
            "firstDay": 0
        };

        $scope.rangesDateRangePicker =  {
            'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Últimos 90 dias': [moment().subtract(90, 'days'), moment()],
        }
    };
    
    function diaAtualCalendar() {
        var dataAtual = moment();
     
        $scope.dataFiltro = {};
        $scope.dataFiltro.date = {startDate: dataAtual, endDate: dataAtual};          
    };
    
    function inicializarDataCalendario() {
        diaAtualCalendar();
        configurarDateFiltro();
    };
    
           
    function getFaturamentoPorContrato() {
            
        var intervalo = getIntervaloDataSelecionada();
        $scope.faturamento.drConsulta.data = [];
        $scope.faturamento.dental.data = [];
        $scope.faturamento.academia.data = [];
        
        mlReportFactory.report0002
        .query( {inicio: intervalo.dtInicio , fim: intervalo.dtFim} ,function( result ) {
            if( result.metadata ) {
                $scope.faturamento.drConsulta.data = result.metadata;
            }
        });
        
        mlReportFactory.dentalreport0002
        .query( {inicio: intervalo.dtInicio , fim: intervalo.dtFim} ,function( result ) {
            if( result.metadata )
                $scope.faturamento.dental.data = result.metadata;
        });
        
        mlReportFactory.academiareport0010
        .query( {inicio: intervalo.dtInicio , fim: intervalo.dtFim} ,function( result ) {
            if( result.metadata )
                $scope.faturamento.academia.data = result.metadata;
        })
    };    
    
    $scope.pesquisarFaturamentoContrato = function() {
        getFaturamentoPorContrato();
    }
    
    $scope.goToDashboard = function( idEmpresa ) {
		
		$state.go('dashboard',{idEmpresaGrupo:idEmpresa});
	}
    
    $scope.getDetalheContratoEmpresa = function( faturamento ) {
	   faturamento.expanded = true;
	};
    
    function inicio() {
        $scope.faturamento = {};
        $scope.faturamento.drConsulta = {nome:'Dr. Consulta'};
        $scope.faturamento.dental = {nome:'Dental Saúde'};
        $scope.faturamento.academia = {nome:'Academia Performance'};
        
        inicializarDataCalendario();
        
        getFaturamentoPorContrato();    
    };    
    
    inicio();
    
    
});


