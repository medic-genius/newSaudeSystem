'use strict';

MedicsystemApp.controller('DashboardClientePlanoController', function($scope,$state,$stateParams,NgTableParams,mlReportFactory,$window,$rootScope) {
    
    // BEGIN PRINT REPORT LANCAMENTO EXAMES
  	$scope.gerarPdfExames = function() {
  		getImageReport('assets/global/img/medic-lab-logo.jpg', printLancamentoExame);
  	};

    

function loadFormasPagamento() {
        $scope.formaPagamentoOptions = [];

        if( $stateParams.idEmpresaGrupo == 3 )
            $scope.formaPagamentoOptions = [
                {id:400,nmFormaPagamento: 'TODAS'},
                {id:50,nmFormaPagamento: 'BOLETO BANCÁRIO'},
                {id:1,nmFormaPagamento: 'DÉBITO AUTOMÁTICO'},
                {id:2,nmFormaPagamento: 'CONTRA-CHEQUE'},
                {id:3,nmFormaPagamento: 'CARTÃO RECORRENTE'},
            ];
        else
            $scope.formaPagamentoOptions = [
                {id:400,nmFormaPagamento: 'TODAS'},
                {id:0,nmFormaPagamento: 'BOLETO BANCÁRIO'},
                {id:1,nmFormaPagamento: 'DÉBITO AUTOMÁTICO'},
                {id:2,nmFormaPagamento: 'CONTRA-CHEQUE'},
                {id:30,nmFormaPagamento: 'CARTÃO RECORRENTE'},
            ];

         $scope.formaPagamentoSelected.formaPagamento =  $scope.formaPagamentoOptions[0];
    };

	var getImageReport = function( url, callBack) {

		var img = new Image();

		img.onError = function() {
			console.log("Nao pode carregar a imagem " + url);
		};

		img.onload = function() {
			printLancamentoExame(img);
		};

		img.src = url;
	};

	var printLancamentoExame = function( imgData ) {

		var nmPlano = $stateParams.nmPlano;		
		var vlPlano = $stateParams.vlPlano;

		var data =  new Date();
		var dia = data.getDate() > 9 ? data.getDate() : ('0'+ (data.getDate() ));
		var mes = (data.getMonth() + 1) > 9 ? (data.getMonth() + 1) : ('0'+ (data.getMonth() + 1));
		var ano = data.getFullYear().toString();
		//var dtAtendimento = dia+"/"+mes+"/"+ano;
		
		var doc = new jsPDF('l','pt','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Relatório de contratos de clientes',{align:'center'});

		// LINE 1
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 170, 'Plano:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(80, 170, nmPlano );

		// LINE 2
		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(40, 190, 'Valor Plano:');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(110, 190, vlPlano);

		//TABLE EXAMES
		var dataTable = gerarTable();
		doc.autoTable( dataTable.columns, dataTable.rows,{
			 startY: 206,
			 margin: {left: 40},
			 theme: 'grid',
            styles: {overflow: 'linebreak',columnWidth:65},
		});
        
        /*
		//ASSINATURA
		doc.setFont("times");
		doc.setFontSize(12);
		doc.writeText(0, 743, '____________________________________',{align:'center'});			

		doc.setFont("times");
		doc.setFontSize(12);
		if( nmPlano.length > 30  ) {
			var nmTemp = nmPlano.split(" ");
			nmPlano = nmTemp[0] + " " + nmTemp[nmTemp.length - 1];		
			doc.writeText(0, 772, nmPlano,{align:'center'});
			doc.writeText(0, 792, 'CRM '+crm,{align:'center'});
		} else {
			doc.writeText(0, 772, nmPlano,{align:'center'});
			doc.writeText(0, 792, 'CRM '+ crm,{align:'center'});
		}
        */
        
        doc.save('contratocliente.pdf')
		//doc.output('dataurlnewwindow');
	};

	var gerarTable = function() {

		var dataRows = [];
		for( var i=0; i< $scope.report0008.data.length; i++ ) {
		
			var data = [];
			data[0] = $scope.report0008.data[i].nomecliente;
			data[1] = $scope.report0008.data[i].numerocontrato;
            data[2] = $scope.report0008.data[i].situacao;
            data[3] = $scope.report0008.data[i].formapagamento;
            data[4] = $scope.report0008.data[i].vidas;
            data[5] = $scope.report0008.data[i].custoVidaFormatado;
            data[6] = $scope.report0008.data[i].cobradoFormatado;
            data[7] = $scope.report0008.data[i].ultimoPagamentoFormatado;
            data[8] = $scope.report0008.data[i].qtdatendimento;
            data[9] = $scope.report0008.data[i].telefone;
            data[10] = $scope.report0008.data[i].celular;
            data[11] = $scope.report0008.data[i].endereco + ', Nº ' + $scope.report0008.data[i].numeroendereco;
			dataRows.push( data );
		}

		var dataTable = {};
		var columns = ['Cliente','Contrato','Situação','Form. Pagamento','Vida(s)','Custo vida','Valor Contrato','Últ. Pag.','Qtd. Atendimento(s)','Telefone','Celular','Endereço'];	
		var rows = dataRows;

		dataTable.columns = columns;
		dataTable.rows = rows;

		return dataTable;
	};	
// END PRINT REPORT LANCAMENTO EXAMES  
        
        
    function configurarTable( lista ) {

		var initialParams = { count:15 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};
    
    $scope.pesquisarClientesPlanoByQtdAtendimento = function() {
        
        var valido = true;
        $scope.report0008 = {};
        
        console.log("qtdAtendimento",$scope.nrQtdAtendimento);
        console.log("qtdAtendimento",$scope.nrQtdAtendimentoFim);
        console.log("CustoVida",$scope.vlCustoVida);
        console.log("ultimoPagamento",$scope.vlUltimoPagamento);
        
        if( $scope.nrQtdAtendimento  && isNaN($scope.nrQtdAtendimento) ) {
            valido = false;
            $rootScope.alerts.push({
                type: "warning", 
                msg: "Informe apenas número",
                timeout: 10000 });
            Metronic.scrollTop();
        }
        else if( $scope.nrQtdAtendimentoFim  && isNaN($scope.nrQtdAtendimentoFim) ) {
            valido = false;
            $rootScope.alerts.push({
                type: "warning", 
                msg: "Informe apenas número",
                timeout: 10000 });
            Metronic.scrollTop();
        } 
        else if( $scope.vlCustoVida  && isNaN($scope.vlCustoVida) ) {
            valido = false;
            $rootScope.alerts.push({
                type: "warning", 
                msg: "Informe apenas número",
                timeout: 10000 });
            Metronic.scrollTop();    
        } else if( $scope.vlUltimoPagamento  && isNaN($scope.vlUltimoPagamento) ) {
            valido = false;
            $rootScope.alerts.push({
                type: "warning", 
                msg: "Informe apenas número",
                timeout: 10000 });
            Metronic.scrollTop();    
        }

        if( valido )
            $scope.report0008query();
    }
    
    $scope.report0008query = function( ) {
        
        inicioVariavel();
        
        if($stateParams.idEmpresaGrupo == 1) { // Dr. Consulta
            mlReportFactory.report0008.query({ idPlano: $scope.idPlano, qtdAtendimento: $scope.nrQtdAtendimento, 
                qtdAtendimentoFim: $scope.nrQtdAtendimentoFim, custoVida: $scope.vlCustoVida, 
                ultimoPagamento: $scope.vlUltimoPagamento, inFormaPagamento: $scope.formaPagamentoSelected.formaPagamento.id },
                function(data) {
                    $scope.report0008.data = data;
                    configurarTable(data);
                    $scope.totalRegistro = data.length;
                    if( data ) {
                        for( var i = 0; i < data.length; i++) {
                            var contrato = data[i];
                            if( contrato.formapagamento == 'DEBITO'  ) {
                                $scope.totalDebito ++;
                            } else if( contrato.formapagamento == 'BOLETO' ) {
                                $scope.totalBoleto ++;
                            } else if( contrato.formapagamento == 'CONTRA CHEQUE') {
                                $scope.totalContracheque ++;
                            } else if( contrato.formapagamento == 'RECORRENTE') {
                                $scope.totalRecorrente ++;
                            }
                        }                    
                    }

                },
                function(erro) {
                    console.log(JSON.stringify(erro));
                }
            );            
        } else if($stateParams.idEmpresaGrupo == 2){ // Dental
            mlReportFactory.dentalreport0008.query({ idPlano: $scope.idPlano, qtdAtendimento: $scope.nrQtdAtendimento,  
                qtdAtendimentoFim: $scope.nrQtdAtendimentoFim, custoVida: $scope.vlCustoVida, 
                ultimoPagamento: $scope.vlUltimoPagamento, inFormaPagamento: $scope.formaPagamentoSelected.formaPagamento.id },
                function(data) {
                    $scope.report0008.data = data;
                    configurarTable(data);
                    $scope.totalRegistro = data.length;
                    if( data ) {
                        for( var i = 0; i < data.length; i++) {
                            var contrato = data[i];
                            if( contrato.formapagamento == 'DEBITO'  ) {
                                $scope.totalDebito ++;
                            } else if( contrato.formapagamento == 'BOLETO' ) {
                                $scope.totalBoleto ++;
                            } else if( contrato.formapagamento == 'CONTRA CHEQUE') {
                                $scope.totalContracheque ++;
                            } else if( contrato.formapagamento == 'RECORRENTE') {
                                $scope.totalRecorrente ++;
                            }
                        }                    
                    }

                },
                function(erro) {
                    console.log(JSON.stringify(erro));
                }
            );  
        } else if($stateParams.idEmpresaGrupo == 3){ // Academia
            mlReportFactory.academiareport0008.query({ idPlano: $scope.idPlano, inFormaPagamento: $scope.formaPagamentoSelected.formaPagamento.id },
                function(data) {
                    $scope.report0008.data = data;
                    configurarTable(data);
                    $scope.totalRegistro = data.length;
                    if( data ) {
                        for( var i = 0; i < data.length; i++) {
                            var contrato = data[i];
                            if( contrato.formapagamento == 'DEBITO'  ) {
                                $scope.totalDebito ++;
                            } else if( contrato.formapagamento == 'BOLETO' ) {
                                $scope.totalBoleto ++;
                            } else if( contrato.formapagamento == 'CONTRA CHEQUE') {
                                $scope.totalContracheque ++;
                            } else if( contrato.formapagamento == 'RECORRENTE') {
                                $scope.totalRecorrente ++;
                            }
                        }                    
                    }

                },
                function(erro) {
                    console.log(JSON.stringify(erro));
                }
            );  
        }
        
    };
    
    function inicioVariavel(){
        
        $scope.totalDebito = 0;
        $scope.totalBoleto = 0;
        $scope.totalContracheque = 0;
        $scope.totalRecorrente = 0;
        
    }
    
    function inicio( idPlano, nmPlano, vlPlano ) {
		$scope.idPlano = idPlano;
        $scope.nmPlano = nmPlano;
        $scope.vlPlano = vlPlano;
        $scope.empresaGrupo = $stateParams.idEmpresaGrupo;
        $scope.formaPagamentoSelected = {};


        loadFormasPagamento();
       
        $scope.report0008 = {};
        $scope.report0008query();
        
        
	};
    
    inicio( $stateParams.idPlano, $stateParams.nmPlano, $stateParams.vlPlano );
        
});