'use strict';

MedicsystemApp.controller('FolhaImportacaoController', function($rootScope,$scope,$state,
  $stateParams,NgTableParams,servicesFactory,Upload) {
	
  function configurarTable( lista ) {

    var initialParams = { count:5 };

    var initialSettings = {
      counts:[], 
      paginationMaxBlocks: 13,
      paginationMinBlocks: 2,
      dataset: lista
    };

    $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  };

  $scope.submit = function() {
    if ( $scope.file) {
      $scope.upload($scope.file);
    }
  };

  $scope.upload = function (file) {

        $scope.f = file;
        $scope.buttonSumbmitDisabled = true;

        Upload.upload({
            url: 'mediclab/folha/converter',
            data: {file: file, 'username': $scope.username}
        }).then(function (resp) {
            $scope.folhaImportadaResult = resp;
            $scope.isData = true;
            $scope.paginaTabelaResultado( $scope.folhaImportadaResult.data.output );
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
           file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            console.log('progress: ' +  file.progress);
        });
   };

   $scope.inicializarFormUpload = function(){
    $scope.f = null; 
    $scope.file = null;
    $scope.folhaImportadaResult = null;
    $scope.isData = false;
    $scope.buttonSumbmitDisabled = false;
    $scope.codFuncDuplicadoList = [];
   };

    $scope.paginaTabelaResultado = function( dadosTabela ){
      if ( dadosTabela.length > 0) {
        configurarTable( dadosTabela );
      }
    };

    function removeSpaceString( stringToRemoveSpace ) {
      var noSpaces = stringToRemoveSpace.replace(/ +/g, "");
      return noSpaces;
    }

    function compareString( stringReference, stringToCompare  ) {
      stringReference = removeSpaceString( stringReference );
      stringToCompare = removeSpaceString( stringToCompare );
      return stringReference == stringToCompare;
    }    

    function saveFolha() {
     
      var empresaNickNameFromUpload =  $scope.folhaImportadaResult.data.nmEmpresaFinanceiro;
      var empresaNickNameDahsBoard = $scope.nmEmpresaNickName ;
      var isSameCompany = compareString( empresaNickNameDahsBoard.toUpperCase(), empresaNickNameFromUpload.toUpperCase() );

      if(  isSameCompany ) {
        if( $scope.folhaImportadaResult.data.nmUnidade.length > 0 ) {
          $scope.buttonDisabled = true;
          $scope.folhaImportadaResult.data.isFolhaAvulsa = $scope.checkboxOption.isFolhaAvulsa;
          servicesFactory.folhaSave.save(  $scope.folhaImportadaResult.data ,function ( result ) {   
            if ( result ) {
              $rootScope.alerts.push({
                type: "info", 
                msg: "Folha de pagamento cadastrada com sucesso !",
                timeout: 10000 });
              Metronic.scrollTop();            
              $state.go('folha_dashboard',{idEmpresaFinanceiro: $scope.idEmpresaFinanceiro});
            }  
          }, function( error ) {
            $scope.buttonDisabled = false;
          });
        } else {
          $rootScope.alerts.push({
            type: "warning", 
            msg: "Nome da unidade não está presente no arquivo importado!",
            timeout: 10000 });
            Metronic.scrollTop(); 
        }
      } else {
           $rootScope.alerts.push({
              type: "danger", 
              msg: "Empresa presente no arquivo não corresponde ao da empresa selecionada !",
              timeout: 10000 });
            Metronic.scrollTop();
      } 
  };

    $scope.verificarCodFuncDuplicado = function() {
      
      $scope.codFuncDuplicadoList = [];

      servicesFactory.verificarCodFunc
      .duplicados( $scope.folhaImportadaResult.data , function( result ) {
         if( result.length == 0 ) {
            saveFolha();
         } else {
          $scope.codFuncDuplicadoList = result;
          console.log("olha o result", result);
         }
      });
    };


    if ( $state.is('folha_importacao') &&  $stateParams.nmEmpresaNickName  ) { 
         $scope.nmEmpresaNickName =  $stateParams.nmEmpresaNickName; 
         $scope.idEmpresaFinanceiro = $stateParams.idEmpresaFinanceiro;
         $scope.nmFantasia = $stateParams.nmFantasia;
         $scope.canShow = true;       
    };
      
    $scope.checkboxOption = {isFolhaAvulsa: false};

    $scope.$on('$viewContentLoaded', function() {
      Metronic.initAjax();
      $scope.isData = false;
    });
});