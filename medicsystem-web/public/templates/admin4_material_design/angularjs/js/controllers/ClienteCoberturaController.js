'use strict';

MedicsystemApp.controller('ClienteCoberturaController', function($rootScope, $scope, $http, $timeout, $state, $location,
 $filter, servicesFactory, NgTableParams ) {

	function configurarTable( lista ) {

		var initialParams = { count:5 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};

	if ( $scope.selectedCliente ) {
		$scope.coberturas = servicesFactory.clienteCoberturas.get({id: $scope.selectedCliente.id }, 
			function() {
				console.log("$scope.coberturas",$scope.coberturas);
				configurarTable( $scope.coberturas );
	    });
	}

});