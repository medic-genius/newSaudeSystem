'use strict';

MedicsystemApp.controller('AdministracaoRenovacaoController', function($rootScope, $scope, $http, $timeout, $state, $stateParams, $modal, $sce, servicesFactory, ngTableParams, $filter, Auth) {

	$scope.rwRoles = ['administrador','gerenteadm', 'cadastro', 'gerenteatendimentocliente', 'rh', 'atendimentocliente', 'usuario'];

   	$scope.printBoletosButton = "Imprimir Boletos";

    //TODO
    var idEmpresaGrupo = 9;

    $scope.refreshListContratosRenovados = function(dtInicio, dtFinal) {	

    	var params;

    	if(dtInicio != null
    		&& dtFinal != null){

    		params = {dtInicio: dtInicio,
    				  dtFinal: dtFinal,
    				  idEmpresaGrupo: idEmpresaGrupo};

    	} else {

    		params = { idEmpresaGrupo: idEmpresaGrupo };
    	}
	    $scope.contratosRenovados = servicesFactory.administracaoRenovacaoContratos.query(params, function() {
			    	$scope.modifiable = $scope.contratosRenovados;
			    	
			        $scope.tableParams = new ngTableParams({
			            page: 1,            // show first page
			            count: 10           // count per page
			        }, {
			            total: $scope.modifiable.length, // length of data
			            getData: function ($defer, params) {
			            	var filters = params.filter();
			                var newFilters = {};

			                for (var key in filters) {
			                    if (filters.hasOwnProperty(key)) {
			                        switch(key) {
			                            default:
			                                newFilters[key] = filters[key];
			                        }
			                    }
			                }
			                
			            	var orderedData = filters ? $filter('filter')($scope.contratosRenovados, newFilters) : $scope.contratosRenovados;
			            	
			            	$scope.modifiable = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
			            	
			            	params.total(orderedData.length); // set total for recalc pagination
			                $defer.resolve($scope.modifiable);
			            }
			        })    
			    });
	};

	$scope.printBoletosSelecionadosPDF = function(item) {
    	
    	$scope.printBoletosButton = "Aguarde carregando";
    	
    	var objectItemsSelected = $scope.checkboxes.items;
    	var keys = Object.keys($scope.checkboxes.items);
    	var arrayIdsMensalidade = [];

    	for (var i=0; i<keys.length; i++) { 
   			console.log(keys[i], objectItemsSelected[keys[i]]);
   			if(objectItemsSelected[keys[i]]){

   				arrayIdsMensalidade.push(parseFloat(keys[i]));
   			}
		}
    	
    	var prodpath = "/ms";
    	if (location.port) {
    		prodpath = "";
    	} 

		$http({
		  method: 'GET',
		  url: prodpath + '/mediclab/renovacao/contrato/boletos/imprimir?' + $.param({idsMensalidade: arrayIdsMensalidade}),
		  params: {
		  	idContrato: $scope.mensalidadesContrato[0].idContrato,
		  	idCliente: $scope.mensalidadesContrato[0].idCliente
		  }
		})

		.success(function (response) {
    		  
    		$scope.printPDFButton = "Imprimir PDF";
    		  
    		var blob = b64toBlob(response, 'application/pdf');
    		var fileURL = URL.createObjectURL(blob);
    		  
    	    $scope.pdf = $sce.trustAsResourceUrl(fileURL);

    	    $scope.openModalImprimirPDFBoleto($scope.pdf);
    	}).error(function(data, status, header, config) {
    		$scope.printPDFButton = "Falha ao Imprimir";
    	});
    };

    $scope.openModalImprimirPDFBoleto = function (pdf) {

	    var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'imprimirPDFBoletoModalContent.html',
            controller: 'ModalImprimirPDFBoletoCtrl',
            backdrop: 'static',
            size: 'lg',
            resolve: {
                        items: function () {
                            return pdf;
                        }
                    }
            });

        modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    }

	if ($state.is('admin-renovacao-contrato')) {
    	$scope.refreshListContratosRenovados();
    }

	if ($state.is('admin-renovacao-contrato-boletos')) {

	    	if ($stateParams.id) {

			    $scope.mensalidadesContrato = servicesFactory.administracaoRenovacaoContratoMensalidade.query({idEmpresaGrupo: idEmpresaGrupo,
			    															idContrato: $stateParams.id}, function() {
					    	$scope.modifiable = $scope.mensalidadesContrato;
					    	
					        $scope.tableParams = new ngTableParams({
					            page: 1,            // show first page
					            count: 12           // count per page
					        }, {
					            total: $scope.modifiable.length, // length of data
					            getData: function ($defer, params) {
					            	var filters = params.filter();
					                var newFilters = {};

					                for (var key in filters) {
					                    if (filters.hasOwnProperty(key)) {
					                        switch(key) {
					                            default:
					                                newFilters[key] = filters[key];
					                        }
					                    }
					                }
					                
					            	var orderedData = filters ? $filter('filter')($scope.mensalidadesContrato, newFilters) : $scope.mensalidadesContrato;
					            	$scope.modifiable = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
					            	
					            	params.total(orderedData.length); // set total for recalc pagination
					                $defer.resolve($scope.modifiable);
					            }
					        })    
				});
	    	} else {

  		    	$scope.refreshListContratosRenovados();
	    	}
	}


	$scope.atualizarColecao = function () {

    	$scope.refreshListContratosRenovados($scope.dtInicial, $scope.dtFinal);
  	};

    $scope.checkboxes = { 'checked': false, items: {} };

    // watch for check all checkbox
    $scope.$watch('checkboxes.checked', function(value) {
        angular.forEach($scope.modifiable, function(item) {
            if (angular.isDefined(item.idMensalidade)) {
                $scope.checkboxes.items[item.idMensalidade] = value;
            }
        });
    });

    // watch for data checkboxes
    $scope.$watch('checkboxes.items', function(values) {

        if (!$scope.modifiable) {
            return;
        }
        var checked = 0, unchecked = 0,
            total = $scope.modifiable.length;
        angular.forEach($scope.modifiable, function(item) {
            checked   +=  ($scope.checkboxes.items[item.idMensalidade]) || 0;
            unchecked += (!$scope.checkboxes.items[item.idMensalidade]) || 0;
        });
        if ((unchecked == 0) || (checked == 0)) {
            $scope.checkboxes.checked = (checked == total);
        }
        // grayed checkbox
        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);

	$scope.today = function() {
		$scope.dt = new Date();
	};
  	
  	$scope.today();

  	$scope.clear = function () {
    	$scope.dt = null;
  	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};
	
	$scope.toggleMin();
	$scope.maxDate = new Date(2020, 5, 22);

	$scope.openDtInicial = function($event) {
		$scope.status.openedDtInicial = true;
	};

	$scope.openDtFinal = function($event) {
		$scope.status.openedDtFinal = true;
  	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
  	};

	$scope.formats = ['dd/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];

	$scope.status = {
		opened: false
	};

	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);
	var afterTomorrow = new Date();
	afterTomorrow.setDate(tomorrow.getDate() + 2);
	$scope.events =
		[
	    	{
	        	date: tomorrow,
	        	status: 'full'
	      	},
	    	{
	        	date: afterTomorrow,
	        	status: 'partially'
	      	}
	    ];

  	$scope.getDayClass = function(date, mode) {
	    if (mode === 'day') {
	    	var dayToCheck = new Date(date).setHours(0,0,0,0);

	    	for (var i=0;i<$scope.events.length;i++){
	        	var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
		        if (dayToCheck === currentDay) {
	          		return $scope.events[i].status;
	        	}
	      	}
    	}
    	return '';
  	};


  	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}
});

//Controller referenced to Modal Instance
MedicsystemApp.controller('ModalImprimirPDFBoletoCtrl', function ($scope, $state, $modalInstance, items) {
  
	$scope.pdf = items;
  
	$scope.ok = function () {

		$modalInstance.close($state.go("admin-renovacao-contrato"));
	};
});
