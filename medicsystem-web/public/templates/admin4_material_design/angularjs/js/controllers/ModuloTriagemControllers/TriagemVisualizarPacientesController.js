'use strict';

MedicsystemApp.controller('TriagemVisualizarPacientesController', function($rootScope,$scope, 
	$http, $timeout, $state,$stateParams,$modal,NgTableParams,$filter, triagemFactory,
	Auth) {

	function configurarTable( lista ) {

		var initialParams = { count:10 };

		var initialSettings = {
			// page size buttons (right set of buttons in demo)
			counts:[], 
			// determines the pager buttons (left set of buttons in demo)
        	paginationMaxBlocks: 13,
        	paginationMinBlocks: 2,
        	dataset: lista
		};

    	$scope.tableParams = new NgTableParams(initialParams, initialSettings )  
  	};

	var paginarTable = function( agendamentosAptos ) {
		
		if( agendamentosAptos.length > 0 ) {
			$scope.canSeeAgendamentos = true;
			$scope.isSemAgendamento = false;
			configurarTable(agendamentosAptos);
		} else {
			$scope.canSeeAgendamentos = false;
			$scope.isSemAgendamento = true;
		}
	};

	var getFuncionario = function() {
		var login =  Auth.authz.idTokenParsed.preferred_username;
		triagemFactory.getFuncionarioByLogin.get( {login: login}, 
	   		function( result ) {
	   			if( result )
	   				$scope.loadUnidadesAtivas( result.unidade.id );
          });
	};

	var inicializarPropriedadesEspecialidade = function() {
		$scope.multipleEspecialidades = {};
  		$scope.multipleEspecialidades.selectedEspecialidade = [];
	};

	var inicializarPropriedadesProfissional = function() {
		$scope.profissionalOptions = null;
		$scope.profissionaisSelecionado = {};
	};

	var inicializarDadosAgendamentosAptos = function() {
		$scope.agendamentosAptos = [];
		$scope.canSeeAgendamentos = false;
	}

	var inicializarTotais = function() {
		$scope.totalPresentes = 0;
		$scope.totalAtendidos = 0;
	};

	$scope.removerEspecialidade = function() {
		$scope.idsEspSelecionadas = null;
		$scope.idsProfSelecionados = null;
		
		if( $scope.multipleEspecialidades.selectedEspecialidade.length > 0 )
			$scope.loadProfissionaisEspecialidade();
		else {
			inicializarDadosAgendamentosAptos();
			inicializarPropriedadesProfissional();
			$scope.isSemAgendamento = true;
		}	
	};

	$scope.carregaAgendamentoApto = function( idsEspSelecionadas, idsProfSelecionados ) {
		
		inicializarTotais();
		
		triagemFactory.getAtendimentoEnfermagem
		.get( {  idUnidade: $scope.unidadeSelecionada.idUnidade, idEspecialidade: idsEspSelecionadas, idProfissional: idsProfSelecionados },
			function( result ) {
				
				if( result ) {
					
					inicializarDadosAgendamentosAptos();
					for( var i = 0; i < result.length; i++) {
					
						var agendamento = result[i];
						console.log("agendamento.inStatus:",agendamento.inStatus);
					
						if( agendamento.inStatus == 4  ) {
							$scope.agendamentosAptos.push( agendamento );
						} else if( agendamento.inStatus == 2 ) {
							console.log("entrou");
							$scope.totalAtendidos ++;
						}
					}

					$scope.totalPresentes = $scope.agendamentosAptos.length;
					paginarTable( $scope.agendamentosAptos );

				} else {
					inicializarDadosAgendamentosAptos();
				}
			});
	};


	$scope.atualizarAgendamentos = function() {
		$scope.carregaAgendamentoApto( $scope.idsEspSelecionadas, $scope.idsProfSelecionados  );
	};	

	$scope.checkProfissional = function( profissional ) {

		var sizeArrayProfSelecionadoss = $scope.profissionaisSelecionado.selecionados.length;
		$scope.idsProfSelecionados = [];

		if (  sizeArrayProfSelecionadoss > 0  ) {

			for( var i = 0; i < sizeArrayProfSelecionadoss ; i++  ) {

				$scope.idsProfSelecionados.push( $scope.profissionaisSelecionado.selecionados[i].id );

				if ( $scope.idsProfSelecionados.length == sizeArrayProfSelecionadoss  ) {
					
					$scope.carregaAgendamentoApto( $scope.idsEspSelecionadas, $scope.idsProfSelecionados );
				}
			}

		} else {
			$scope.loadProfissionaisEspecialidade();
		}

	};

	$scope.loadProfissionaisEspecialidade = function( isRefresh ) {

		var sizeArrayEspSelecionadas = $scope.multipleEspecialidades.selectedEspecialidade.length;
		$scope.idsEspSelecionadas = [];

		for( var i = 0; i < sizeArrayEspSelecionadas ; i++  ) {

			$scope.idsEspSelecionadas.push( $scope.multipleEspecialidades.selectedEspecialidade[i].id );

			if ( $scope.idsEspSelecionadas.length == sizeArrayEspSelecionadas  ) {
				
				$scope.carregaAgendamentoApto( $scope.idsEspSelecionadas, null  );
				
				if( !isRefresh ) {
					inicializarPropriedadesProfissional();
					triagemFactory.getProfissionaisByEsp.get(
						{ ids: $scope.idsEspSelecionadas},
						function( result ) {
							if ( result ) {
					   			$scope.profissionalOptions = result;
					   		}
		        	});		
		        }
			}
		}
	};

	$scope.loadEspecialidadesUnidade = function ( idUnidadeSelecionada ) {
		
		inicializarPropriedadesEspecialidade();
		inicializarPropriedadesProfissional();
		inicializarDadosAgendamentosAptos();
		
		triagemFactory.unidadesEspecialidades.get( { id: idUnidadeSelecionada },
			function( result ) {
		   		if ( result ) {

		   			$scope.especialidadeOptions = result;

		   			if( $scope.isPosTriagem ) {

		   				for( var i = 0; i < $scope.especialidadeOptions.length; i++  ) {
		   					if( $stateParams.idEspecialidade == $scope.especialidadeOptions[i].id  ) {
		   						$scope.multipleEspecialidades.selectedEspecialidade = [$scope.especialidadeOptions[i]];
		   						$scope.loadProfissionaisEspecialidade();
		   						break;
		   					}
		   				}
		   			}
		   		}
         });
	};

	$scope.loadUnidadesAtivas = function( idUnidade ) {
		if ( idUnidade ) {
			triagemFactory.unidades.query( function( result ) {
	   			$scope.unidadeOptions = result;
	   			for ( var i = 0; i < $scope.unidadeOptions.length; i++ ) {
	   				if ( idUnidade == $scope.unidadeOptions[i].id ) {
	   					$scope.unidadeSelecionada.idUnidade = idUnidade;
	   					break;
	   				}
	   			}

	   			$scope.loadEspecialidadesUnidade($scope.unidadeSelecionada.idUnidade );

	         });
		}	
	};


	var inicio = function() {
		$scope.canSeeAgendamentos = false;
		$scope.unidadeSelecionada = {}; 
		$scope.isSemAgendamento = false;
		$scope.idsEspSelecionadas = null;
		$scope.idsProfSelecionados = null;
		inicializarPropriedadesProfissional();
		inicializarPropriedadesEspecialidade();
		getFuncionario();
	};

	if ( $state.is('triagem-visualizarPacientes') ) {
		
		if( $stateParams.idEspecialidade )
			$scope.isPosTriagem = true;

		inicio();

	};	
});

