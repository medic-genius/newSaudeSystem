'use strict';

MedicsystemApp.controller('TriagemAtendimentoPacienteTriagem', function($rootScope,$scope, 
	$state,$stateParams, triagemFactory) {
	
	function getTriagemDTO() {

		$scope.triagemDTO = {};

		triagemFactory.getDadosTriagemPaciente
		 .get( { id: $stateParams.idPaciente ,intipopaciente: $stateParams.tipoCliente},
		 	function( result ) {
	   			if(result) 
	   				$scope.triagemDTO = result;
	     }); 
	};

	$scope.updateTriagem = function() {

		$scope.triagemDTO.vlAltura = ($scope.triagemDTO.vlAltura != null 
			&& $scope.triagemDTO.vlAltura != "" ) ? parseFloat($scope.triagemDTO.vlAltura) : null;

		$scope.triagemDTO.vlTemperatura = ($scope.triagemDTO.vlTemperatura != null 
			&& $scope.triagemDTO.vlTemperatura != "" ) ? parseInt($scope.triagemDTO.vlTemperatura) : null;

		$scope.buttonSaveDisable = true;
		
		var idAgendamento = $stateParams.idAgendamento;
		var inTipoPaciente = $stateParams.tipoCliente
 
		triagemFactory.triagem
		 .update( { id: $stateParams.idPaciente, idagendamento: idAgendamento, intipopaciente: inTipoPaciente}, $scope.triagemDTO , 
		 	function( result ) {
		 	if(result) {

		 		$scope.buttonSaveDisable = false;

	 			$rootScope.alerts.push({
                	type: "info", 
                	msg: " Triagem atualizado com sucesso!",
                	timeout: 5000 });
                	Metronic.scrollTop();   

                	getTriagemDTO();    
		 	}
		 }, function( error){
			$scope.buttonSaveDisable = false;		 		
		 });
	};

	var inicio = function() {
		$scope.buttonSaveDisable = false;
		getTriagemDTO();
	};

	inicio();


});


