'use strict';

MedicsystemApp.controller('TriagemAtendimentoPacienteController', function($rootScope,$scope, 
	$state,$stateParams, $modal, Auth, triagemFactory,$ngBootbox) {

	$scope.opcoesDialogoPrioridade = {
        title: 'O paciente tem atendimento prioritário ?',
        message: 'escolha uma opção.',
        className: 'prioridade-class',
        buttons: {
            success: {
                label: "Sim",
                className: "btn-default btn-sm",
                callback: function() {
                var isPrioridade = true;
                 
                  if($scope.boPrioridade != isPrioridade){

                    $ngBootbox.confirm('Houve divergência no tipo de atendimento. Deseja realmente coloca-lo como atendimento prioridade?')
                    .then(function() {
                        $scope.atualizarStatusTriagem( isPrioridade );
                     console.log('Alert closed paciente prioridade (true)'); 
                    });
                  }else{
                    $scope.atualizarStatusTriagem( isPrioridade );
                  }
                  
                }
             },
            danger: {
                label: "Nao",
                className: "btn-danger btn-sm",
                callback: function() {
                    var isPrioridade = false;

                     if($scope.boPrioridade != isPrioridade){

                    $ngBootbox.confirm('Houve divergência no tipo de atendimento. Deseja realmente coloca-lo como atendimento normal?')
                    .then(function() {
                        $scope.atualizarStatusTriagem( isPrioridade );
                     console.log('Alert closed paciente prioridade (false)'); 
                    });
                  }else{
                    $scope.atualizarStatusTriagem( isPrioridade );
                  }
                	//$scope.atualizarStatusTriagem( isPrioridade );
                }
             }
        }
    };

    $scope.atualizarStatusTriagem = function(tipoPrioridade, statusPrioridade, obsPrioridade) {
  		triagemFactory.triagemStatus
  		.update( { id: $scope.idAgendamento, prioridade: tipoPrioridade, statusPrioridade: statusPrioridade, obsUrgencia: obsPrioridade },{},function() {
  			$state.go('triagem-visualizarPacientes',{idEspecialidade:$scope.idEspecialidade});
  		});
    };

	$scope.finalizarAtendimento = function() {
		//$ngBootbox.customDialog($scope.opcoesDialogoPrioridade);
    loadModalPrioridade();
	};

	var inicio = function( idPaciente, tipoCliente, nmPaciente, idAgendamento, idEspecialidade, boPrioridade ) {
		$scope.idPaciente = idPaciente;
		$scope.tipoCliente = tipoCliente;
		$scope.nmPaciente = nmPaciente;
		$scope.idAgendamento = idAgendamento;
		$scope.buttonFinalizarDisabled = false;
		$scope.canSeeOftalmologia = false;
		$scope.idEspecialidade = idEspecialidade;
    $scope.boPrioridade  = (boPrioridade == "true");

        
	};

	if( $stateParams.idPaciente ){
		inicio( $stateParams.idPaciente, $stateParams.tipoCliente, $stateParams.nmPaciente, $stateParams.idAgendamento, $stateParams.idEspecialidade, $stateParams.boPrioridade );
	
    }

  function loadModalPrioridade (){
        var modalInstance = $modal.open({
            animation: true,
            backdrop: 'static',
            templateUrl: 'templates/admin4_material_design/angularjs/views/cliente/cliente-agendamento-dialogo-prioridade.html',
            controller: 'modalPrioridadeCtrl',
            size: 'md',
            resolve:{
              boPrioridade: function(){
                return $scope.boPrioridade;
              }
            }
        });

        modalInstance.result.then(function(prioridade){
            //isPrioridade 0 - maior que 80, 1 - prioridade, 2 - sem prioridade
            //statusPrioridade 1-muita urgencia 0 - urgencia
            $scope.atualizarStatusTriagem(prioridade.inTipoPrioridade, prioridade.statusPrioridade, prioridade.obsurgencia); //mudar
        })

    }
			
});

//Controller prioridade recepção
MedicsystemApp.controller('modalPrioridadeCtrl', function($scope, $state, $modal, $modalInstance,$ngBootbox, boPrioridade){
    
    $scope.inicioModal = function(){
        $scope.prioridaderec = {}
        $scope.showInfoTriagem = false;
    };

    $scope.finalizar = function(prioridaderec){
      if(prioridaderec.inTipoPrioridade == 1 || prioridaderec.inTipoPrioridade == 2){
        $scope.boPrioridadeCliente = true;
      } else{
        $scope.boPrioridadeCliente = false;
      }

      if(boPrioridade != $scope.boPrioridadeCliente){
        $ngBootbox.confirm('Houve divergência no tipo de atendimento. Deseja realmente coloca-lo como atendimento prioridade?')
        .then(function() {
            $modalInstance.close(prioridaderec);
           console.log('Alert closed paciente prioridade (true)'); 
          });
      }else{
        $modalInstance.close(prioridaderec);
        //$scope.atualizarStatusTriagem( isPrioridade );
      }
      //$modalInstance.close(prioridaderec);
    }

    $scope.cancel = function(){
       $modalInstance.dismiss('cancel');
    }
    $scope.inicioModal();
});
