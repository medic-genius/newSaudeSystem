'use strict';

MedicsystemApp.controller('GerarBoletoController', 
	function($scope,$state,$stateParams,$element,NgTableParams,administrativoFactory,toastr, $http) {
		
	function getIntervaloDataSelecionada() {
    	if( $scope.fieldSearch.date.startDate != null &&
                $scope.fieldSearch.date.endDate != null ) {
    
            var intervalo = {};
/*            var dtInicio = moment( $scope.fieldSearch.date.startDate).format("DD/MM/YYYY");
            var dtFim = moment( $scope.fieldSearch.date.endDate).format("DD/MM/YYYY");*/

            var dtInicio = moment( $scope.fieldSearch.date.startDate).format("MM/DD/YYYY");
            var dtFim = moment( $scope.fieldSearch.date.endDate).format("MM/DD/YYYY");

            var dataValida = moment($scope.fieldSearch.date.startDate).isValid();
            
            intervalo["dtInicio"] = dtInicio;
            intervalo["dtFim"] = dtFim;
            intervalo["dataValida"] = dataValida;

            return intervalo;

        } else 
            return null;
    };

    function messageToastr(type, message, title) {
	    if( type == 'erro' ) {
	       toastr.error(  message, title.toUpperCase() ); 
	    } else if( type == 'sucesso' ) {
	       toastr.success(  message, title.toUpperCase() ); 
	    } else if( type == 'informacao' ) {
	       toastr.info(  message, title.toUpperCase() ); 
	    }
	};

	function complementoTable( lista ) {
		var complemento = {};
		complemento.initialParams = { count:100 };
		complemento.initialSettings = {
			counts:[10,50,100], 
	    	paginationMaxBlocks: 13,
	    	paginationMinBlocks: 2,
	    	dataset: lista
		};	

		return complemento;
	};

	function configurarTable( lista , etapa ) {

		var complemento = complementoTable( lista );

		if( etapa == 'cobranca' )	{
			$scope.tableCobrancaParams = new NgTableParams();
			$scope.tableCobrancaParams = new NgTableParams( complemento.initialParams, complemento.initialSettings );
		} else if( etapa == 'impressao' ) {
			$scope.tableBoletoParams = new NgTableParams();
			$scope.tableBoletoParams = new NgTableParams( complemento.initialParams, complemento.initialSettings );
		}
		else {
			$scope.tableParams = new NgTableParams();
			$scope.tableParams = new NgTableParams( complemento.initialParams, complemento.initialSettings );
		}
  	};

	function inicializarFormasPagamento() {
		$scope.formaPagamentoOptions = [
			{id:0, label: 'Boleto Bancário'},
			{id:1, label: 'Débito Automático'},
			{id:2, label: 'Contra-Cheque'},
			{id:30, label: 'Cartão Recorrente'},
		];
	};

	function inicializarTipoDesconto() {
     	$scope.fieldSearch.optionDescSelected = { value: 0};
     	$scope.fieldSearch.vlDescDinheiro = 130;
     	$scope.fieldSearch.vlDescPorcentagem = 0.9;

        $scope.rdOptionsTipoDesc = [
            {value:0, name:'Valor Fixo'},
            {value:1, name:'Porcentagem'},
        ];
    };

    function inicializarFiltroExtra() {
     	// filtro negociadas
     	$scope.fieldSearch.boNegociada = { value: false  };
        $scope.filtroNegociadaOptions = [
            {value:false, name:'Negociadas'},
        ];
    };

	function inicializarSliders() {
		$scope.fieldSearch.qtdTituloAbertoSlider = {
	    	value:4,
		    options: {
		        floor: 1,
		        ceil: 20,
		        step: 1
		    }
		};
	}

	function definirConfigDateRange() {
    	$scope.localeDateRangePicker = {
        	"format": "DD/MM/YYYY",	"separator": " - ", "applyLabel": "Feito",
        	"cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
        	"customRangeLabel": "Customizado", "weekLabel": "S",
        	"daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        	"monthNames": ["Janerio","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        	"firstDay": 0
		};

	    $scope.rangesDateRangePicker =  {
	    	'Hoje': [moment(), moment()],
	    	'2011': [moment('01/01/2011','DD/MM/YYYY'), moment('31/12/2011','DD/MM/YYYY')],
	    	'2012': [moment('01/01/2012','DD/MM/YYYY'), moment('31/12/2012','DD/MM/YYYY')],
	    	'2013': [moment('01/01/2013','DD/MM/YYYY'), moment('31/12/2013','DD/MM/YYYY')],
	    	'2014': [moment('01/01/2014','DD/MM/YYYY'), moment('31/12/2014','DD/MM/YYYY')],
	    	'2015': [moment('01/01/2015','DD/MM/YYYY'), moment('31/12/2015','DD/MM/YYYY')],
	    	'2016': [moment('01/01/2016','DD/MM/YYYY'), moment('31/12/2016','DD/MM/YYYY')],
	   		'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
	   		'Últimos 30 Dias': [ moment().subtract(29, 'days'),moment()],
		};
    };

	function inicializarData() {
   		var dataAtual = moment();
     	$scope.fieldSearch.date = {startDate: dataAtual, endDate: dataAtual};  
    };

    function inicializaCheckBox() {
   		$scope.checkboxes = {};

	    $scope.checkboxes = {
	    	checked: false,
	      	items: {},
	    };
    };

    function inicializaCheckBoxCobranca() {
   		$scope.checkboxesCobranca = {};

	    $scope.checkboxesCobranca = {
	    	checked: false,
	      	items: {},
	    };
    };

     function inicializaCheckBoxImpressao() {
   		$scope.checkboxesimpressao = {};

	    $scope.checkboxesimpressao = {
	    	checked: false,
	      	items: {},
	    };
    };



    // pdf

    function gerarTable() {

    	var dataTable = {};
    	var columns = ['Código','Nome','Status','Descrição'];	
    	var rows = [];

		for( var i=0; i < $scope.statusCadastro.erroList.length; i++ ) {	
			var data = [];
			var valor = $scope.statusCadastro.erroList[i];
			data[0] = valor.in_CodCliente;
			data[1] = valor.st_nome_sac;
			data[2] = valor.status;
			data[3] = valor.msg;
			rows.push( data );
		}

		dataTable.columns = columns;
		dataTable.rows = rows;

		return dataTable;
    };

    function layoutPDF( imgData ) {
		var dtAtendimento =  moment().format('DD/MM/YYYY');
		var doc = new jsPDF('p','pt','a4');

		//CABECALHO		
		doc.addImage(imgData, 'JPEG', 40, 20, 200/*width*/, 43/*heigth*/, 'logo');

		doc.setFont("times");
		doc.setFontType('bold')
		doc.setFontSize(14);
		doc.writeText(0,140, 'Clientes não cadastrados',{align:'center'});

		doc.setFont("times");
		doc.setFontType('bold');
		doc.setFontSize(12);
		doc.text(322, 190, 'Data');

		doc.setFont("times");
		doc.setFontType('');
		doc.setFontSize(12);
		doc.text(436, 190, dtAtendimento);

		//TABLE EXAMES
		var dataTable = gerarTable();
		doc.autoTable( dataTable.columns, dataTable.rows,{
			 startY: 206,
			 margin: {left: 40},
			 theme: 'plain',
			 overflow: 'linebreak',
		});
        
        var blob = doc.output('blob');
        var fileURL = URL.createObjectURL(blob);
        window.open(fileURL, '_blank');
	};

    function getImageReport( url, callBack ) {
		var img = new Image();
		img.onError = function() { console.log("Nao pode carregar a imagem " + url); };
		img.onload = function() { callBack(img);};
		img.src = url;
	};

    $scope.gerarPDF = function() {
    	getImageReport('assets/global/img/medic-lab-logo.jpg', layoutPDF );
    };

    // end pdf

    function emailPadrao() {
    	return 'odontomed.ti@gmail.com';
    }

    function validarEmail( email ) {
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    	if( re.test(email) )
    		return email.toLowerCase();
    	else
    		return emailPadrao();
    };

    function recuperarInformacaoClientes( idClienteList ) {
    	var resultado = [];
    	for ( var i = 0; i < $scope.clientes.length; i++) {
    		var indice  = idClienteList.indexOf( String($scope.clientes[i].idContrato) );
    		if( indice != -1 ) {
    			resultado.push($scope.clientes[i]);
    		}
    	};    	
    	return resultado;
    };

    function recuperarInformacaoCobrancas( idCobrancaList ) {
    	var resultado = [];
    	for ( var i = 0; i < $scope.cobrancaPendenteGerarList.length; i++) {
    		var indice  = idCobrancaList.indexOf( String($scope.cobrancaPendenteGerarList[i].idContrato) );
    		if( indice != -1 ) {
    			resultado.push($scope.cobrancaPendenteGerarList[i]);
    		}
    	};    	
    	return resultado;
    };

    function recuperarInformacaoBoleto( idBoletoList ) {
    	var resultado = [];
    	for ( var i = 0; i < $scope.arrayBoletosPrint.length; i++) {
    		var indice  = idBoletoList.indexOf( String($scope.arrayBoletosPrint[i].link) );
    		if( indice != -1 ) {
    			resultado.push($scope.arrayBoletosPrint[i]);
    		}
    	};    	
    	return resultado;
    };

    function recuperarClientesSelecionados() {
    	var idClienteSelecionadoList = [];
    	var clienteCompletoList = null;
    	
    	angular.forEach($scope.checkboxes.items, function(itemChecado,index) {
    		if( itemChecado )
    			idClienteSelecionadoList.push( index );
      	});

      	if( idClienteSelecionadoList.length > 0 ) {
      		clienteCompletoList = recuperarInformacaoClientes( idClienteSelecionadoList )
      	}

      	return clienteCompletoList;
    };

    function recuperarCobrancaSelecionadas() {
    	var idCobrancaSelectList = [];
    	var idCobrancaCompletaList = null;
    	
    	angular.forEach($scope.checkboxesCobranca.items, function(itemChecado,index) {
    		if( itemChecado )
    			idCobrancaSelectList.push( index );
      	});

      	if( idCobrancaSelectList.length > 0 ) {
      		idCobrancaCompletaList = recuperarInformacaoCobrancas( idCobrancaSelectList )
      	}

      	return idCobrancaCompletaList;
    };

    function recuperarBoletoSelecionado() {
    	var idBoletoSelectList = [];
    	var idBoletoCompletaList = null;
    	
    	angular.forEach($scope.checkboxesimpressao.items, function(itemChecado,index) {
    		if( itemChecado ) 
    			idBoletoSelectList.push( index );
    			
      	});

      	if( idBoletoSelectList.length > 0 ) {
      		idBoletoCompletaList = recuperarInformacaoBoleto( idBoletoSelectList )
      	}

      	return idBoletoCompletaList;
    };

    function gerarObjetoCobrancaSP ( item, definirAposCadastroCliente  ) {

    	var cobrancaSP = {};

    	if( definirAposCadastroCliente ) {
			cobrancaSP.id_sacado_sac = item.data.id_sacado_sac;
			cobrancaSP.produtoServicoGeneric = {};
			cobrancaSP.produtoServicoGeneric.id_produto_prd = '999999983'; //produto adesao
			cobrancaSP.produtoServicoGeneric.nm_quantidade_comp = 1;
			cobrancaSP.produtoServicoGeneric.vl_unitario_prd = $scope.clienteDividaMap.has(item.idContrato) ? $scope.clienteDividaMap.get( item.idContrato ).totalPagarFinal : null;  			
			cobrancaSP.id_conta_cb = '1';
			cobrancaSP.id_formapagamento_recb = 0;
			cobrancaSP.in_idCliente = item.in_idCliente;
			cobrancaSP.in_CodCliente = item.in_CodCliente;
			cobrancaSP.st_NomeCliente = item.st_nome_sac;
			cobrancaSP.idContrato = item.idContrato;	
    	} else {
    		cobrancaSP.id_sacado_sac = item.idClienteSl;
	    	cobrancaSP.produtoServicoGeneric = {};
	    	cobrancaSP.produtoServicoGeneric.id_produto_prd = '999999983'; //produto adesao
	    	cobrancaSP.produtoServicoGeneric.nm_quantidade_comp = 1;
	    	cobrancaSP.produtoServicoGeneric.vl_unitario_prd =  item.vlDesconto; 
	    	cobrancaSP.id_conta_cb = '1';
	    	cobrancaSP.id_formapagamento_recb = 0;
	    	cobrancaSP.in_idCliente = item.idCliente;
	    	cobrancaSP.in_CodCliente = item.nrCodCliente;
	    	cobrancaSP.st_NomeCliente = item.nmCliente;
	    	cobrancaSP.idContrato = item.idContrato;
    	}

    	return cobrancaSP;
    };

    function gerarObjetoClienteSP ( item ) {
    	
    	var dtContratoArray =  item.dtContrato.split("/");
    	var dia = dtContratoArray[0];

		var clienteSP = {};
    	clienteSP.st_nome_sac = item.nmCliente;
    	clienteSP.st_nomeref_sac = item.nmCliente;
    	clienteSP.st_diavencimento_sac = dia;
    	clienteSP.st_cgc_sac = item.nrCPF ? item.nrCPF : " ";
    	clienteSP.st_rg_sac = item.nrRG ? item.nrRG : " ";
    	clienteSP.st_cep_sac = item.nrCEP ? item.nrCEP : null;
		clienteSP.st_endereco_sac = item.nmLogradouro ? item.nmLogradouro : null;
		clienteSP.st_numero_sac = item.nrNumero ? item.nrNumero : null;
		clienteSP.st_bairro_sac = item.nmBairro ? item.nmBairro : null;
		clienteSP.st_complemento_sac = item.nmComplemento ? item.nmComplemento : " ";
		clienteSP.st_cidade_sac = item.nmCidade ? item.nmCidade : null;
		clienteSP.st_estado_sac = item.nmUF ? item.nmUF : null;
		clienteSP.fl_mesmoend_sac = 1;
		clienteSP.in_idCliente = item.idCliente;
		clienteSP.st_CodCliente = item.nrCodCliente;
		clienteSP.st_email_sac = item.nmEmail ? (item.nmEmail.trim().length > 0 ? validarEmail(item.nmEmail) : emailPadrao()) : emailPadrao();
    	//clienteSP.st_email_sac = null ;
    	clienteSP.idContrato = item.idContrato;

    	return clienteSP;
    };

    function gerarClienteMap( item ) {
		
		var objectMapCliente = {};
    	
    	objectMapCliente.totalPagarFinal = item.vlDesconto;

    	objectMapCliente.printInformation = {};
    	objectMapCliente.printInformation.nmCliente = item.nmCliente;
    	objectMapCliente.printInformation.endereco = item.endereco;
    	objectMapCliente.printInformation.enderecoComplemento = item.enderecoComplemento;

    	$scope.clienteDividaMap.set( item.idContrato, objectMapCliente );    	
    };

    function gerarBoletoDTO( objectResponseCobranca ) {
    	var boletoDTO = {};

    	boletoDTO.nmCliente = $scope.clienteDividaMap.get( objectResponseCobranca.in_idContrato ).printInformation.nmCliente;
    	boletoDTO.endereco = $scope.clienteDividaMap.get( objectResponseCobranca.in_idContrato ).printInformation.endereco;
    	boletoDTO.enderecoComplemento =  $scope.clienteDividaMap.get( objectResponseCobranca.in_idContrato ).printInformation.enderecoComplemento;
    	boletoDTO.link = objectResponseCobranca.data.link_2via;

    	console.log('boletoDTO', boletoDTO);

    	return boletoDTO;
    };

    function definirEstagioClientes( lista ) {

    	var estagioObject = { cadastro:[], boletoAvulso:[] };

    	angular.forEach( lista, function( item ) {

    		if ( item.idClienteSl ) {
    			estagioObject.boletoAvulso.push( gerarObjetoCobrancaSP( item, false ) );
    		}
    		else {
    			estagioObject.cadastro.push( gerarObjetoClienteSP( item ) );
    		}

    		gerarClienteMap( item );	
    	});

    	return estagioObject;
    };

     function definirEtapaClientes( lista ) {

    	var estagioObject = { cadastro:[], boletoAvulso:[], impressao: []};

    	angular.forEach( lista, function( item ) {

    		if ( item.idClienteSl && (item.boNegociadoAuto == null || item.boNegociadoAuto == false ) ) {
    			estagioObject.boletoAvulso.push( gerarObjetoCobrancaSP( item, false ) );
    		}
    		else if( item.idClienteSl == null ) {
    			estagioObject.cadastro.push( item );
    		}
    		else if( item.idClienteSl && item.boNegociadoAuto == true  ) {
    			var infoImpressao = {};
    			infoImpressao.endereco = item.endereco
    			infoImpressao.enderecoComplemento = item.enderecoComplemento;
    			infoImpressao.link = item.linkBoleto_sl;
    			infoImpressao.nmCliente = item.nmCliente;
    			estagioObject.impressao.push( infoImpressao );
    		}

    		gerarClienteMap( item );	
    	});

    	return estagioObject;
    };

    //contrato
    $scope.tableParams = new NgTableParams();
    // monitora pelo checkBox marcar todos
    $scope.$watch(function() {
     	return  $scope.checkboxes.checked;
    }, function(value) {
    	angular.forEach($scope.tableParams.data, function(item) {
       	$scope.checkboxes.items[item.idContrato] = value;
      });
    });

    // monitora pelo status de cada checkbox gerado
    $scope.$watch(function() {
      return $scope.checkboxes.items;
    }, function(values) {
    	var checked = 0, unchecked = 0, total = $scope.tableParams.data.length;
     	angular.forEach($scope.clientes, function(item) {
     		checked   +=  ($scope.checkboxes.items[item.idContrato]) || 0;
	        unchecked +=  (!$scope.checkboxes.items[item.idContrato]) || 0;
      	});
      	
      	if ((unchecked == 0) || (checked == 0)) {
        	$scope.checkboxes.checked = (checked == total);
      	}	

      	$scope.totalChecked = checked;

      	// grayed checkbox
      	angular.element($element[0].getElementsByClassName("select-all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);

    //cobranca

	$scope.tableCobrancaParams = new NgTableParams();

    // monitora pelo checkBox marcar todos
    $scope.$watch(function() {
     	return  $scope.checkboxesCobranca.checked;
    }, function(value) {
    	angular.forEach($scope.tableCobrancaParams.data, function(item) {
       	$scope.checkboxesCobranca.items[item.idContrato] = value;
      });
    });

    // monitora pelo status de cada checkbox gerado
    $scope.$watch(function() {
      return $scope.checkboxesCobranca.items;
    }, function(values) {
    	var checked = 0, unchecked = 0, total = $scope.tableCobrancaParams.data.length;
     	angular.forEach($scope.cobrancaPendenteGerarList, function(item) {
        	checked   +=  ($scope.checkboxesCobranca.items[item.idContrato]) || 0;
        	unchecked +=  (!$scope.checkboxesCobranca.items[item.idContrato]) || 0;
      	});
      	
      	if ((unchecked == 0) || (checked == 0)) {
        	$scope.checkboxesCobranca.checked = (checked == total);
      	}	

      	$scope.totalCheckedCobranca = checked;

      	// grayed checkbox
      	angular.element($element[0].getElementsByClassName("select-all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);

    //impressao

    $scope.tableBoletoParams = new NgTableParams();
    // monitora pelo checkBox marcar todos
    $scope.$watch(function() {
     	return  $scope.checkboxesimpressao.checked;
    }, function(value) {
    	angular.forEach( $scope.tableBoletoParams.data, function(item) { 
       	$scope.checkboxesimpressao.items[item.link] = value;
      });
    });

    // monitora pelo status de cada checkbox gerado
    $scope.$watch(function() {
      return $scope.checkboxesimpressao.items;
    }, function(values) {
    	var checked = 0, unchecked = 0, total = $scope.tableBoletoParams.data.length;
     	angular.forEach($scope.arrayBoletosPrint, function(item) {
        	checked   +=  ($scope.checkboxesimpressao.items[item.link]) || 0;
        	unchecked +=  (!$scope.checkboxesimpressao.items[item.link]) || 0;
      	});
      	
      	if ((unchecked == 0) || (checked == 0)) {
        	$scope.checkboxesimpressao.checked = (checked == total);
      	}	

      	$scope.totalCheckedImpressao = checked;

      	// grayed checkbox
      	angular.element($element[0].getElementsByClassName("select-all")).prop("indeterminate", (checked != 0 && unchecked != 0));
    }, true);


    function concluirPesquisaFeedBack() {
    	$scope.pesquisarClientesInadimplentes( true );
	    $scope.optionRunning = false;
    };

    function imprimirBoletos( lista ) {
    	console.log('lista final imprimir',lista);
    	if( lista.length > 0 ) {
    		$http.post('mediclab/superlogica/cobranca/boletospdf', lista,
    			{ responseType: 'arraybuffer'} 
    		)
	     	.success(function (data) {
	           var file = new Blob([data], {type: 'application/pdf'});
	           var fileURL = URL.createObjectURL(file);
	           window.open(fileURL);
	           concluirPesquisaFeedBack();
	    	})
	    	.error( function(err) {
	    		concluirPesquisaFeedBack();
	    		messageToastr('erro','Ocorreu um erro ao reimprimir boleto', 'DESCULPA :(')
	    	});
    	}
    };

    function cadastrarCobranca( listaCobrancaAvulsa ) {
    	
    	$scope.statusCobranca = {erroList:[]};

    	administrativoFactory.cadastrarCobrancaSuperLogica
		.salvar( listaCobrancaAvulsa  , function( resultCobranca  ) {
			if ( resultCobranca.length > 0 ) {
				
				var arrayBoletosImprimir = [];
				console.log('Map final', $scope.clienteDividaMap.toObject() );

				angular.forEach( resultCobranca, function( item ) {
					if( item.status == 500 ) 
						$scope.statusCobranca.erroList.push( item );
					else if( item.status == 200 ) {
						
						var idContratoEncontrado = $scope.clienteDividaMap.has(item.in_idContrato);
						console.log('idContratoEncontrado',idContratoEncontrado);

						if( idContratoEncontrado ) {
							arrayBoletosImprimir.push( gerarBoletoDTO( item ) );
						}
					}
						
				});

				console.log("boleto imprimir", arrayBoletosImprimir);
				if( arrayBoletosImprimir.length > 0 )
					imprimirBoletos( arrayBoletosImprimir  );
				else
					$scope.optionRunning = false;
			}
		}, function( err) {
			concluirPesquisaFeedBack();
		})
    };

    function validarGerarBoleto (  ) {
    	
    	var isValido = true;

    	if ( $scope.totalChecked > 100 ) {
    		isValido = false;
    		messageToastr('informacao','Selecione no máximo 100 :)','CALMA,');
    	} 

    	return isValido;
    };

    $scope.gerarBoletoSuperlogica = function() {

   		var isValido = validarGerarBoleto();

    	if( isValido ) {

    		var clientesSelecionados =  recuperarClientesSelecionados();
			var clienteEstagio = {};

			$scope.statusCadastro = {erroList:[]};

			if( clientesSelecionados ) {

				$scope.optionRunning = true;
				
				clienteEstagio = definirEstagioClientes( clientesSelecionados );

				if( clienteEstagio.cadastro.length > 0 ) {

					var clientes = clienteEstagio.cadastro;

					administrativoFactory.cadastrarClienteSuperLogica
					.salvar(clientes, function( result) {
						if( result.length > 0 ) {
							angular.forEach( result, function( item ) {
								if ( item.status == '500') {
									$scope.statusCadastro.erroList.push( item );
								} else if( item.status == '200' ) {
									clienteEstagio.boletoAvulso.push( gerarObjetoCobrancaSP( item, true ) );
								}
							})

							if( clienteEstagio.boletoAvulso.length > 0 )
								cadastrarCobranca( clienteEstagio.boletoAvulso );
							else
								$scope.optionRunning = false;
						}
					}, function(err) {
						concluirPesquisaFeedBack();
					})
				} else if( clienteEstagio.boletoAvulso.length > 0 ) {
					cadastrarCobranca(  clienteEstagio.boletoAvulso );
				}
			}
    	}
    };

    $scope.gerarBoletoSegundaVia = function( isIndividual, cliente ) {
    	if( isIndividual ) {
    		window.open( cliente.linkBoleto_sl , '_blank');
    	} else {
    		var clientesSelecionados =  recuperarClientesSelecionados();
    		if( clientesSelecionados.length > 0 ) {

    			var boletoListDTO = [];
    			angular.forEach( clientesSelecionados, function( item ) {
    				if( item.linkBoleto_sl ) {
    					var boletoDTO = {};
    					boletoDTO.link = item.linkBoleto_sl;
    					boletoDTO.nmCliente = item.nmCliente;
    					boletoDTO.endereco = item.endereco;
    					boletoDTO.enderecoComplemento = item.enderecoComplemento;
    					boletoListDTO.push( boletoDTO );
    				}
    			});

    			$scope.optionRunning = true;
    			imprimirBoletos( boletoListDTO );
    		}
    	}
    };

    //new

    $scope.imprimirBoletosSP = function() {

    	var boletoSelecionados =  recuperarBoletoSelecionado();

    	if( boletoSelecionados.length > 0 ) {
    		$http.post('mediclab/superlogica/cobranca/boletospdf', boletoSelecionados,
    			{ responseType: 'arraybuffer'} 
    		)
	     	.success(function (data) {
	           var file = new Blob([data], {type: 'application/pdf'});
	           var fileURL = URL.createObjectURL(file);
	           window.open(fileURL);
	    	})
	    	.error( function(err) {
	    		messageToastr('erro','Ocorreu um erro ao reimprimir boleto', 'DESCULPA :(')
	    	});
    	}
    };

    $scope.cadastrarCobrancaSP = function( ) {

   		var cobrancaSelecionadas =  recuperarCobrancaSelecionadas();
   		$scope.statusCobranca = {erroList:[]};

		
		if( cobrancaSelecionadas ) {
			
    		administrativoFactory.cadastrarCobrancaSuperLogica
			.salvar( cobrancaSelecionadas  , function( resultCobranca  ) {
				if ( resultCobranca.length > 0 ) {
					
					$scope.arrayBoletosPrint = [];

					angular.forEach( resultCobranca, function( item ) {
						if( item.status == 500 ) 
							$scope.statusCobranca.erroList.push( item );
						else if( item.status == 200 ) {
							var idContratoEncontrado = $scope.clienteDividaMap.has(item.in_idContrato);

							if( idContratoEncontrado ) {
								$scope.arrayBoletosPrint.push( gerarBoletoDTO( item ) );
							}
						}
					});

					$scope.optionRunning = false;
					
			}
		}, function( err) {
			
		})
		}
    };



    $scope.cadastrarClientesSP = function() {
    	
    	var clientesSelecionados =  recuperarClientesSelecionados();
		var clienteEstagio = {};
		$scope.statusCadastro = {erroList:[]};

		console.log('clientesSelecionados',clientesSelecionados);

		if( clientesSelecionados ) {
			clienteEstagio = definirEstagioClientes( clientesSelecionados );
			console.log('clienteEstagio',clienteEstagio);

			if( clienteEstagio.cadastro.length > 0 ) {
				var clientes = clienteEstagio.cadastro;
				administrativoFactory.cadastrarClienteSuperLogica
				.salvar(clientes, function( result) {
						if( result.length > 0 ) {
							angular.forEach( result, function( item ) {
								if ( item.status == '500') {
									$scope.statusCadastro.erroList.push( item );
								} else if( item.status == '200' ) {
									clienteEstagio.boletoAvulso.push( gerarObjetoCobrancaSP( item, true ) );
								}
							})

							if( clienteEstagio.boletoAvulso.length > 0 ) {
								$scope.cobrancaPendenteGerarList = clienteEstagio.boletoAvulso;
								configurarTable( $scope.cobrancaPendenteGerarList,'cobranca');
							}
								

							console.log('$scope.cobrancaPendenteGerarList',$scope.cobrancaPendenteGerarList);
						}
					}, function(err) {
						concluirPesquisaFeedBack();
					})
				} 
			}
    };

    $scope.pesquisarClientesInadimplentes = function( boPesquisaInterna ) {
    	
    	$scope.searching = boPesquisaInterna ? false : true;    	

    	var inFormaPagamento = $scope.fieldSearch.formaPagamento.selected;
    	var qtdMensalidade = $scope.fieldSearch.qtdTituloAbertoSlider.value;
    	var intervalo = getIntervaloDataSelecionada();
    	var boNegociadoAuto = $scope.fieldSearch.boNegociada.value;
    	var inTipoDesconto = $scope.fieldSearch.optionDescSelected.value;
    	var vlDesconto = null;

    	$scope.clientes = [];
    	$scope.cobrancaPendenteGerarList = [];
    	$scope.arrayBoletosPrint = [];
    	$scope.clienteDividaMap = new Map();
    	inicializaCheckBox();

    	if( inTipoDesconto == 0 ) 
    		vlDesconto = $scope.fieldSearch.vlDescDinheiro;
    	else if( inTipoDesconto == 1 )
    		vlDesconto = $scope.fieldSearch.vlDescPorcentagem;

    	administrativoFactory.clientesContratos
    	.get( { inFormaPagamento: inFormaPagamento, qtdMensalidade: qtdMensalidade, dtInicio: intervalo.dtInicio,dtFim:intervalo.dtFim, boNegociadoAuto: boNegociadoAuto, inTipoDesconto:inTipoDesconto, vlDesconto: vlDesconto  }, 
    		function( result ) {
    			if( result ) {
					var clienteEstagio = definirEtapaClientes( result );
					$scope.clientes = clienteEstagio.cadastro;
					$scope.cobrancaPendenteGerarList = clienteEstagio.boletoAvulso;
					$scope.arrayBoletosPrint = clienteEstagio.impressao;

					configurarTable( $scope.clientes, null );
    				configurarTable( $scope.cobrancaPendenteGerarList, 'cobranca');
    				configurarTable( $scope.arrayBoletosPrint, 'impressao');

    				$scope.searching = false;
    			}
    	})
    };



    //end new


/*    $scope.pesquisarClientesInadimplentes = function( boPesquisaInterna ) {
    	
    	$scope.searching = boPesquisaInterna ? false : true;    	

    	var inFormaPagamento = $scope.fieldSearch.formaPagamento.selected;
    	var qtdMensalidade = $scope.fieldSearch.qtdTituloAbertoSlider.value;
    	var intervalo = getIntervaloDataSelecionada();
    	var boNegociadoAuto = $scope.fieldSearch.boNegociada.value;
    	var inTipoDesconto = $scope.fieldSearch.optionDescSelected.value;
    	var vlDesconto = null;

    	$scope.clientes = [];
    	$scope.clienteDividaMap = new Map();
    	inicializaCheckBox();

    	if( inTipoDesconto == 0 ) 
    		vlDesconto = $scope.fieldSearch.vlDescDinheiro;
    	else if( inTipoDesconto == 1 )
    		vlDesconto = $scope.fieldSearch.vlDescPorcentagem;

    	administrativoFactory.clientesContratos
    	.get( { inFormaPagamento: inFormaPagamento, qtdMensalidade: qtdMensalidade, dtInicio: intervalo.dtInicio,dtFim:intervalo.dtFim, boNegociadoAuto: boNegociadoAuto, inTipoDesconto:inTipoDesconto, vlDesconto: vlDesconto  }, 
    		function( result ) {
    			$scope.clientes = result;
    			$scope.searching = false;
    			configurarTable( result );
    	})
    };*/

	function inicio() {
		
		$scope.fieldSearch = {};
		$scope.fieldSearch.formaPagamento = {};
		$scope.clientes = [];
		$scope.cobrancaPendenteGerarList = [];
		$scope.arrayBoletosPrint = [];
		$scope.searching = false;

		inicializarData();
		inicializarSliders();
		inicializarFiltroExtra();
		inicializarFormasPagamento();
		definirConfigDateRange();
		inicializarTipoDesconto();
		inicializaCheckBox();
		inicializaCheckBoxCobranca();
		inicializaCheckBoxImpressao();
	};

	inicio();			
});

/*MedicsystemApp.directive('toolbarTip', function() {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function(scope, element, attrs) {
        	console.log('attrs',attrs);
        	console.log('scope',scope);
        	console.log('element',element);
            $(element).toolbar(scope.$eval(attrs.toolbarTip));
        }
    };
});
*/

