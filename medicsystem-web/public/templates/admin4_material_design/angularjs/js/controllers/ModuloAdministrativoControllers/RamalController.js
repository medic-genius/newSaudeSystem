'use strict';

MedicsystemApp.controller('RamalController', 
	function($scope,$stateParams,NgTableParams) {
		
   function configurarTable( lista ) {
        
        $scope.tableParams = new NgTableParams();

        if( lista.length > 0 ) {
            var initialParams = { count:10 ,  group: "setor"};
            var initialSettings = {
                counts: [5, 10, 20],
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset: lista,
            };

            $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
        }
    };

    function loadUnidades () {
        $scope.unidades = [
            {id:1, nmUnidade:'Tarumã'},
            {id:2, nmUnidade:'Joaquim Nabuco'}
        ];
    };

    function loadRamalJoaquim() {
        var ramais = [
            {nrRamal:'9303', nome:'Bruna Bela', setor:'Telemarketing'},
            {nrRamal:'9309', nome:'Beatriz/Chrisyiano', setor:'Telemarketing'},
            {nrRamal:'9452', nome:'Cintia/Nilberto', setor:'Laboratório'},
            {nrRamal:'9400', nome:'Daniel', setor:'Portaria'},
            {nrRamal:'9346', nome:'Dione/Fernando', setor:'Gerência'},
            {nrRamal:'9415', nome:'Eliane', setor:'Ótica'}, 
            {nrRamal:'9307', nome:'Ester', setor:'Telemarketing'},
            {nrRamal:'9308', nome:'João/Beatriz', setor:'Telemarketing'},
            {nrRamal:'9305', nome:'Jussara', setor:'Telemarketing'},
            {nrRamal:'9360', nome:'katlen', setor:'Estética'},
            {nrRamal:'9355', nome:'Laboratório Subsolo', setor:'Laboratório'},
            {nrRamal:'9447', nome:'Marcela/Edenilsa', setor:'Caixa'},            
            {nrRamal:'9337', nome:'Miquéias ', setor:'Telemarketing'},
            {nrRamal:'9342', nome:'Miquéias', setor:'Telemarketing'},
            {nrRamal:'9357', nome:'Monique', setor:'Ouvidoria'},
            {nrRamal:'9324', nome:'Recepcionista', setor:'Recepção'},
            {nrRamal:'9327', nome:'Recepcionista', setor:'Recepção'},
            {nrRamal:'9304', nome:'Rosimar', setor:'Telemarketing'},
            {nrRamal:'9419', nome:'Ray', setor:'Ouvidoria'},
            {nrRamal:'9302', nome:'Sammya/Adriana', setor:'Telemarketing'},
            {nrRamal:'9395', nome:'Triagem', setor:'Triagem'},
            {nrRamal:'9452', nome:'Triagem Subsolo', setor:'Triagem'},
            {nrRamal:'9311', nome:'Ygor/Yone', setor:'Telemarketing'}, 
        ];

        return ramais;
    };

    function loadRamalTaruma (argument) {
          var ramais = [ 

            {nrRamal:'9335', nome:'Adriana', setor:'Administracao'},
            {nrRamal:'9439', nome:'Alessandra/Suelen', setor:'Rh'},
            {nrRamal:'9358', nome:'Almoxarifado', setor:'Romário'},
            {nrRamal:'9353', nome:'Aninha/Rosangela', setor:'Caixa'},
            {nrRamal:'9409', nome:'Araizia', setor:'Negociação'},
            {nrRamal:'9420', nome:'Ardenilza', setor:'Vendas'},
            {nrRamal:'9432', nome:'Brenna', setor:'Financeiro'},
            {nrRamal:'9394', nome:'Bruno', setor:'Raio X'},
            {nrRamal:'9403', nome:'Caroline', setor:'Administracao'},
            {nrRamal:'9422', nome:'Chara', setor:'Administracao'},
            {nrRamal:'9418', nome:'Cynira', setor:'Dep. Pessoal'},
            {nrRamal:'9421', nome:'Cynira', setor:'Dep. Pessoal'},
            {nrRamal:'9429', nome:'Deborah', setor:'Administracao'},
            {nrRamal:'9391', nome:'Elisangela', setor:'Recepção Imagens'},
            {nrRamal:'9326', nome:'Felipe/Tamilis', setor:'Gerência'},
            {nrRamal:'9414', nome:'Gisele', setor:'Administracao'},
            {nrRamal:'9313', nome:'Jamylli', setor:'Financeiro'},
            {nrRamal:'9356', nome:'Jéssica', setor:'Financeiro'},
            {nrRamal:'9404', nome:'kelen', setor:'Administracao'},
            {nrRamal:'9424', nome:'Marcos', setor:'Jurídico'},
            {nrRamal:'9412', nome:'Matheus', setor:'Negociação'},
            {nrRamal:'9442', nome:'Malena', setor:'Administracao'},
            {nrRamal:'9367', nome:'Michele', setor:'Triagem'},
            {nrRamal:'9389', nome:'Raiane', setor:'Laudo'},
            {nrRamal:'9352', nome:'Recepcionista', setor:'Recepção Térreo'},
            {nrRamal:'9437', nome:'Recepcionista', setor:'Recepção Térreo'},
            {nrRamal:'9450', nome:'Recepcionista', setor:'Recepção Andar 1'},
            {nrRamal:'9312', nome:'Recepcionista', setor:'Recepção Andar 1'},
            {nrRamal:'9374', nome:'Renildo', setor:'Portaria'},
            {nrRamal:'9362', nome:'TI', setor:'T.I'},
            {nrRamal:'9392', nome:'Simone', setor:'Recepção Imagens'},           
            
        ];

        return ramais;
    };

    $scope.loadRamais = function() {

        $scope.ramais = [];

        if( $scope.unidade.selected.id == 1 ) {
            $scope.ramais = loadRamalTaruma();
        } else if ( $scope.unidade.selected.id == 2 ) {
            $scope.ramais = loadRamalJoaquim();
        }

        configurarTable( $scope.ramais );
    };

    function inicio () {
        $scope.unidade = {selected:null};
        loadUnidades();
    };

    inicio();


});