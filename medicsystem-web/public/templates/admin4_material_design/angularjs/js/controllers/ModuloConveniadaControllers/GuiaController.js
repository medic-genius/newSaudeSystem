'use strict';

MedicsystemApp.controller(
    'GuiaController',
    function(
        $scope, servicesFactory, $stateParams, $ngBootbox, $rootScope
    )
    {
        var idGuia = ($stateParams.guiaUrl.match(/\d+/g).map(Number))[0];

        function loadGuia()
        {
            servicesFactory.getGuia.get({id: idGuia}, function(data)
            {
                $scope.showReactive = false;
                $scope.guia = data;
                for ( var i = 0 ; i < $scope.guia.guiaServicos.length ; i++)
                {
                    $scope.guia.guiaServicos[i].statusLabel = servicesFactory.getStatusItemOfGuia({id: $scope.guia.guiaServicos[i].status});
                    if($scope.guia.guiaServicos[i].status == 4)
                    {
                        $scope.guia.guiaServicos[i].tdExclusaoFormated = moment($scope.guia.guiaServicos[i].dtExclusao).format('DD/MM/YYYY');
                    } else if(5 == $scope.guia.guiaServicos[i].status)
                    {
                        $scope.showReactive = true;
                    }
                }
                var idPretty = "" + $scope.guia.id;
                var pad = "000000";
                $scope.idPretty = pad.substring(0, pad.length - idPretty.length) + idPretty;
                if($scope.guia.dependente)
                {
                    $scope.ownership = "Dependente";
                    $scope.own = $scope.guia.dependente.nmDependente;
                } else
                {
                    $scope.ownership = "Titular";
                    $scope.own = $scope.guia.cliente.nmCliente;
                }
                $scope.dateValidPretty = moment($scope.guia.dtValidade).format('DD/MM/YYYY');
                $scope.dateCreatePretty = moment($scope.guia.dtInicio).format('DD/MM/YYYY');
                $scope.dateExclusaoPretty = moment($scope.guia.dtExclusao).format('DD/MM/YYYY');
            });
        }

        servicesFactory.getCompany.get({}, function(company){
            $scope.company = company;
            $scope.company.nrTelefone1 = $rootScope.util.formatPhoneNumber($scope.company.nrTelefone1);
        });

        $scope.print = function()
        {
            window.print();
        }

        $scope.backList = function()
        {
            history.back();
        }

        $scope.removeItemServico = function(guiaServico)
        {
            $ngBootbox.confirm('Deseja realmente excluir este item da Guia? Está é uma ação IRREVERSÍVEL!')
            .then(function()
            {
                servicesFactory.deleteGuiaServico.delete({id: guiaServico.id}, function(data)
                {
                    loadGuia();
                });
            });
        }

        loadGuia();

        $scope.reactiveGuia = function()
        {
            $scope.disabledBtnReactive = true;

            servicesFactory.reactiveGuia.put({id: idGuia}, function(data)
            {
                $scope.showReactive = false;
                $rootScope.alerts.push({
                    type: "success",
                    msg: "Nova Guia criada com o código " + ('000000' + data.id).slice(-6),
                    timeout: 15000 });
            });
        };
    }
);
