'use strict';

MedicsystemApp.controller
(
    'EmitirGuiaController', function ($scope, $modalInstance, servicesFactory, $modal, idContrato, $rootScope)
    {
        $scope.newGuia = {
                "cliente":
                {
                    "id": null
                },
                "dependente": null,
                "contrato":
                {
                    "id": 0
                },
                "guiaServicos": []
            };
        $scope.newGuia.guiaServicos = [];
        $scope.labelBtnSubmit = "Concluir";
        $scope.modal = {
            modalType: "formCreateGuia"
        };

        $scope.cancel = function ()
        {
            $modalInstance.dismiss('cancel');
        };

        $scope.lPeople = [
        ];
        servicesFactory.getClienteByIdContrato.get( {id: idContrato},function(titular){
            $scope.lPeople.push({
                name: titular.nmCliente,
                cpf: titular.nrCPF,
                id: titular.id,
                tipoPaciente: "titular",
                dependente: null
            });
            servicesFactory.getDependentesByIdContrato.get({id: idContrato},function(dependentes){
                var i;
                for (i = 0; i < dependentes.length; i++)
                {
                    if((dependentes[i].boFazUsoPlano || dependentes[i].boFazUsoPlano == null))
                    {
                        $scope.lPeople.push({
                            name: dependentes[i].nmDependente,
                            cpf: dependentes[i].nrCep,
                            id: $scope.lPeople[0].id,
                            tipoPaciente: "dependente",
                            dependente: dependentes[i].id
                        });
                    }
                }
            });
        });

        $scope.atualizarSelectServicos = function(text)
        {
    		if (text && text.length > 1)
            {
	            var queryObject = {};
	            text = text.toLowerCase();
	            queryObject = { name: text };
                servicesFactory.getServicosByConveniada.query(queryObject, function(data){
	                    $scope.servicos = data;
	                });
            }
        };

        $scope.addServico = function(item)
        {
            $scope.newGuia.guiaServicos = addServicoInModel(item, $scope.newGuia.guiaServicos);
        };

        function addServicoInModel(objServico, arr)
        {
            var indexFound = null;
            var idSer = objServico.id
            for(var i = 0; i < arr.length; i++)
            {
                if(arr[i].servico.id == idSer)
                {
                    indexFound = i;
                    break;
                }
            }
            if(null == indexFound)
            {
                arr.push({
                    "servico":
                    objServico
                });
            }
            return arr;
        }

        $scope.setTipoPaciente = function(item)
        {
            $scope.newGuia.cliente.id = item.id;
            if(item.tipoPaciente == 'dependente')
            {
                $scope.newGuia.dependente = {id: item.dependente};
            } else
            {
                $scope.newGuia.dependente = null;
            }
        };

        $scope.submitFormCreateGuia = function(formCreateGuia)
        {
            var jsonData = JSON.stringify($scope.newGuia, function( key, value ) {
                if( key === "$$hashKey" ) {
                    return undefined;
                }
                return value;
            });

            servicesFactory.guiaSave.post(jsonData,function(data)
            {
                $scope.newGuia = data;
                loadScreenPrint();
            }, function(error)
            {
                console.log(error);
            });
        }

        function loadScreenPrint()
        {
            $scope.modal.modalType = "printVoucherGuia";
            var idPretty = "" + $scope.newGuia.id;
            var pad = "000000";
            $scope.idPretty = pad.substring(0, pad.length - idPretty.length) + idPretty;
            if($scope.newGuia.dependente)
            {
                $scope.ownership = "Dependente";
                $scope.newGuia.paciente = {name: $scope.newGuia.dependente.nmDependente};
            } else
            {
                $scope.ownership = "Titular";
                $scope.newGuia.paciente = {name: $scope.newGuia.cliente.nmCliente};
            }
            $scope.dateCreatePretty = moment($scope.newGuia.dtInicio);
            $scope.dateValidPretty = moment($scope.newGuia.dtValidade);
            $scope.dateCreatePretty = $scope.dateCreatePretty.format('DD/MM/YYYY');
            $scope.dateValidPretty = $scope.dateValidPretty.format('DD/MM/YYYY');
        }

        $scope.print = function()
        {
            window.print();
        }

    	$scope.removeRowAndItem = function(item)
        {
            var indexToRemoveModel = null;
            for(var i = 0; i < $scope.newGuia.guiaServicos.length; i++)
            {
                if($scope.newGuia.guiaServicos[i].servico.id == item.servico.id)
                {
                    indexToRemoveModel = i;
                    break;
                }
            }
            if(null != indexToRemoveModel)
            {
                $scope.newGuia.guiaServicos.splice(indexToRemoveModel, 1);
            }
    	};

        servicesFactory.getCompany.get( {},function(company){
            $scope.company = company;
            $scope.company.nrTelefone1 = $rootScope.util.formatPhoneNumber($scope.company.nrTelefone1);
        });
    }
);
