'use strict';

MedicsystemApp.controller(
    'GuiasController',
    function(
        $scope, servicesFactory, $location, $routeParams, $ngBootbox
    )
    {
        $scope.filter = {
            key: null,
            field: {id:false, nmCliente: true, status: 'all'}
        };
        function loadGuias()
        {
            var offset = parseInt($location.search().offset);
            $scope.offset = ((offset && offset > 0) ? offset : 1);
            var queryParams = {};
            queryParams = $location.search();
            servicesFactory.getGuiasByConveniada.query(queryParams, function(data)
            {
                $scope.lGuias = data;
                if($scope.lGuias.data)
                {
                    for (var i = 0 ; i < $scope.lGuias.data.length; i++)
                    {
                        $scope.lGuias.data[i].validateFormated = moment($scope.lGuias.data[i].dtValidade).format('DD/MM/YYYY');
                        if($scope.lGuias.data[i].dtExclusao)
                        {
                            $scope.lGuias.data[i].tdExclusaoFormated = moment($scope.lGuias.data[i].dtExclusao).format('DD/MM/YYYY');
                        }
                        if($scope.lGuias.data[i].dependente)
                        {
                            $scope.lGuias.data[i].titularidade = "Dependente";
                            $scope.lGuias.data[i].beneficiario = $scope.lGuias.data[i].dependente.nmDependente;
                        } else
                        {
                            $scope.lGuias.data[i].titularidade = "Titular";
                            $scope.lGuias.data[i].beneficiario = $scope.lGuias.data[i].cliente.nmCliente;
                        }
                    }

                    var stringParams = "";
                    for (const prop in $location.search()) {
                        if(prop != "offset")
                        {
                            stringParams += `&${prop}=${$location.search()[prop]}`;
                        }
                    }
                    if($scope.offset > 1)
                    {
                        $scope.prevPage = "#/conveniada/guias.html?offset=" + ($scope.offset - 1) + stringParams;
                    } else
                    {
                        $scope.prevPage = null;
                    }
                    if($scope.offset < $scope.lGuias.totalPage)
                    {
                        $scope.nextPage = "#/conveniada/guias.html?offset=" + ($scope.offset + 1) + stringParams;
                    } else
                    {
                        $scope.nextPage = null;
                    }
                } else
                {
                    console.log("empty list");
                }
            });
        }

        $scope.$watch(function()
        {
            return $location.search().offset
        }, function (offset) {
            loadGuias();
        });

        $scope.applyFilter = function(form)
        {
            var queryFilters = {};
            if(form.filter.key)
            {
                if(form.filter.field.nmCliente)
                {
                    queryFilters.nm_cliente = form.filter.key;
                }
                if(form.filter.field.id)
                {
                    queryFilters.id = form.filter.key;
                }
                if(form.filter.field.nmBeneficiario)
                {
                    queryFilters.nm_beneficiario = form.filter.key;
                }
            }
            if(form.filter.field.status == 0 || form.filter.field.status == 1)
            {
                queryFilters.status = form.filter.field.status;
            }
            $location.path('/conveniada/guias.html').search(queryFilters);
            loadGuias();
        }

        $scope.inactiveGuia = function(guia)
        {
            $ngBootbox.confirm('Deseja realmente inativar Guia? Está é uma ação IRREVERSÍVEL!')
            .then(function()
            {
                servicesFactory.deleteGuia.delete({id: guia.id}, function(data)
                {
                    loadGuias();
                });
            });
        };
    }
);
