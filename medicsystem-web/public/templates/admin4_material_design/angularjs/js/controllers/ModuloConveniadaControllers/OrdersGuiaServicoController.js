'use strict';

MedicsystemApp.controller
(
    'OrdersGuiaServicoController', function ($scope, $modalInstance, servicesFactory, $modal, idServico, $location, $callWhileInstantiating)
    {
        var $callWhileInstantiating = eval($callWhileInstantiating);
        function constructorController()
        {
            loadGuiaServicos();
        }

        $callWhileInstantiating();

        $scope.cancel = function ()
        {
            $modalInstance.dismiss('cancel');
        };

        function loadGuiaServicos()
        {
            var offset = parseInt($location.search().offset);
            $scope.offset = ((offset && offset > 0) ? offset : 1);
            var queryParams = {};
            queryParams = $location.search();
            queryParams.id_servico = idServico;
            servicesFactory.getGuiaServicos.query(queryParams, function(data)
            {
                $scope.lGuiaServicos = data;
                if($scope.lGuiaServicos.data)
                {
                    for (var i = 0 ; i < $scope.lGuiaServicos.data.length; i++)
                    {
                        $scope.lGuiaServicos.data[i].validateFormated = moment($scope.lGuiaServicos.data[i].guia.dtValidade).format('DD/MM/YYYY');
                        $scope.lGuiaServicos.data[i].guia.dtInicioFormated = moment($scope.lGuiaServicos.data[i].guia.dtInicio).format('DD/MM/YYYY');
                        $scope.lGuiaServicos.data[i].statusLabel = servicesFactory.getStatusItemOfGuia({id: $scope.lGuiaServicos.data[i].status});
                        if($scope.lGuiaServicos.data[i].dtExclusao)
                        {
                            $scope.lGuiaServicos.data[i].tdExclusaoFormated = moment($scope.lGuiaServicos.data[i].dtExclusao).format('DD/MM/YYYY');
                        }
                        if(0 == i)
                        {
                            $scope.servico = {nmServico: $scope.lGuiaServicos.data[i].servico.nmServico};
                        }
                        if($scope.lGuiaServicos.data[i].guia.dependente)
                        {
                            $scope.lGuiaServicos.data[i].titularidade = "Dependente";
                            $scope.lGuiaServicos.data[i].beneficiario = $scope.lGuiaServicos.data[i].guia.dependente.nmDependente;
                        } else
                        {
                            $scope.lGuiaServicos.data[i].titularidade = "Titular";
                            $scope.lGuiaServicos.data[i].beneficiario = $scope.lGuiaServicos.data[i].guia.cliente.nmCliente;
                        }
                    }

                    var stringParams = "";
                    for (const prop in $location.search())
                    {
                        if(prop != "offset")
                        {
                            stringParams += `&${prop}=${$location.search()[prop]}`;
                        }
                    }
                    if($scope.offset > 1)
                    {
                        $scope.prevPage = "#/conveniada/guia_servicos.html?offset=" + ($scope.offset - 1) + stringParams;
                    } else
                    {
                        $scope.prevPage = null;
                    }
                    if($scope.offset < $scope.lGuiaServicos.totalPage)
                    {
                        $scope.nextPage = "#/conveniada/guia_servicos.html?offset=" + ($scope.offset + 1) + stringParams;
                    } else
                    {
                        $scope.nextPage = null;
                    }
                } else
                {
                    console.log("empty list");
                }
            });
        }
    }
)
.value('$callWhileInstantiating', "constructorController");
