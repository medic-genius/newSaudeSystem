'use strict';

MedicsystemApp.controller(
    'GuiaServicosController',
    function(
        $scope, servicesFactory, $location, $routeParams, $ngBootbox, callWhileInstantiating, $q
    )
    {
        var callWhileInstantiating = eval(callWhileInstantiating);

        $scope.filter = {
            key: null,
            lStatus: servicesFactory.getStatusItemOfGuia({}),
            interval: {date: null}
        };
        
        $scope.detailServicoShowing = 0;
        $scope.filter.lStatusSelecteds = [];
        for(var i = 0; i < servicesFactory.getStatusItemOfGuia({}).length; i++)
        {
            $scope.filter.lStatusSelecteds[i] = false;
            if($location.search().status)
            {
                if($location.search().status.includes(i.toString()))
                {
                    $scope.filter.lStatusSelecteds[i] = true;
                }
            }
        }

        $scope.labelDefaultPeriod = "Criaçao da Guia"

        $scope.$watchCollection('filter.lStatusSelecteds', function() {
            if(arraysAreEqual([true, false, true, false, false, false], $scope.filter.lStatusSelecteds))
            {
                $scope.labelPeriod = "Criaçao e Realizaçao do Serviço"
            } else
            {
                $scope.labelPeriod = $scope.labelDefaultPeriod;
            }
        });
        
    	function  initializeDate()
    	{
            if($location.search().start_date && $location.search().end_date)
            {
                var partsStart = $location.search().start_date.split('-');
                var startDate = new Date(partsStart[0], partsStart[1] - 1, partsStart[2]);
                var partsEnd = $location.search().end_date.split('-');
                var endDate = new Date(partsEnd[0], partsEnd[1] - 1, partsEnd[2]);
                $scope.filter.interval.date = {startDate: startDate, endDate: endDate};
            } else
            {
        		var date = new Date();
                $scope.filter.interval.date = {startDate: (new Date(date.getFullYear(), date.getMonth(), 1)), endDate: (new Date())};
            }
    	}

        function arraysAreEqual(ary1,ary2){
          return (ary1.join('') == ary2.join(''));
        }

        $scope.applyFilter = function(form)
        {
            var queryFilters = {};
            if($scope.filter.interval.date.startDate)
            {
                var startDate = moment($scope.filter.interval.date.startDate).format("YYYY-MM-DD");
                var endDate = moment($scope.filter.interval.date.endDate).format("YYYY-MM-DD");
                queryFilters.start_date = startDate;
                queryFilters.end_date = endDate;
            }
            if($scope.filter.lStatusSelecteds)
            {
                queryFilters.status = [];
                for(var i = 0; i < $scope.filter.lStatus.length; i++)
                {
                    if($scope.filter.lStatusSelecteds[i])
                    {
                        queryFilters.status[queryFilters.status.length] = i;
                    }
                }
            }
            $location.path('/conveniada/guia_servicos.html').search(queryFilters);
            getSummaryServicoByStatus();
            getSummaryGuiaServicoByServico()
        };

        function configDatePickerRange() {
            $scope.localeDateRangePicker = {
                "format": "YYYY-MM-DD", "separator": " - ", "applyLabel": "OK",
                "cancelLabel": "Cancelar", "fromLabel": "De", "toLabel": "Para",
                "customRangeLabel": "Customizado", "weekLabel": "S",
                "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                "monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                "firstDay": 0
            };

            $scope.rangesDateRangePicker =  {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        function constructorController()
        {
    		initializeDate();
    		configDatePickerRange();
            configDataToPrint();
        	if($scope.filter.interval.date.startDate && $scope.filter.interval.date.endDate)
        	{
                $scope.applyFilter();
        	}
        }

        function getSummaryServicoByStatus()
        {
            servicesFactory.getSummaryServicoByStatus.query($location.search(), function(data)
            {
                $scope.lSummaryServicoByStatusHumanized = [];
                var total = {
                    custoServicos: 0,
                    qdtServico: 0,
                    status: "Todos",
                    classBold: true
                };
                $scope.lSummaryServicoByStatus = [];
                var lStatusToShow = [0, 1, 2];
                for(var i = 0; i < lStatusToShow.length; i++)
                {
                    $scope.lSummaryServicoByStatusHumanized[i] = {
                        custoServicos: 0,
                        qdtServico: 0,
                        status: servicesFactory.getStatusItemOfGuia({id: i}),
                        classBold: false
                    };
                }
                for(var i=0; i < data.length; i++)
                {
                    if(-1 != lStatusToShow.indexOf(data[i].status))
                    {
                        total.custoServicos += data[i].custoServicos;
                        total.qdtServico += data[i].qdtServico;
                        $scope.lSummaryServicoByStatusHumanized[data[i].status].qdtServico += data[i].qdtServico;
                        $scope.lSummaryServicoByStatusHumanized[data[i].status].custoServicos += data[i].custoServicos;
                    }
                }
                $scope.lSummaryServicoByStatusHumanized[$scope.lSummaryServicoByStatusHumanized.length] = total;
            });
        }

        function getSummaryGuiaServicoByServico()
        {
            $scope.lSummaryGuiaServicoByServico = [];
            servicesFactory.getSummaryGuiaServicoByServico.query($location.search(), function(lSummary)
            {               
                for (var i = 0; i < lSummary.length; i++)
                {
                    lSummary[i].head = true;
                    $scope.lSummaryGuiaServicoByServico[$scope.lSummaryGuiaServicoByServico.length] = lSummary[i];
                }
                var promises = [];
                for (var i = 0; i < lSummary.length; i++)
                {
                    promises.push(getListDetailServico(lSummary[i].idServico)); // push the Promises to our array
                }
                                
                $q.all(promises).then(function(dataArr)
                {
                    var count = 0;
                    var baseNext = 1;                    
                    dataArr.forEach(function(data) {
                        for(var iRowDetail = 0; iRowDetail < data.data.length; iRowDetail++)
                        {
                            if(0 == iRowDetail)
                            {
                                $scope.lSummaryGuiaServicoByServico.splice(
                                    (count + baseNext),
                                    0,
                                    {head2: true, idServico: data.data[iRowDetail].servico.id}
                                );
                                baseNext++;
                            }
                            var rowDetail =
                            {
                                head: false,
                                nmCliente: data.data[iRowDetail].guia.cliente.nmCliente,
                                idServico: data.data[iRowDetail].servico.id,
                                valorServico: data.data[iRowDetail].valorServico,
                                vlServicoAssociado: data.data[iRowDetail].vlServicoAssociado,
                                statusFormatado: data.data[iRowDetail].statusFormatado
                            };
                            if(data.data[iRowDetail].guia.dependente)
                            {
                                rowDetail.titularidade = "Dependente";
                                rowDetail.beneficiario = data.data[iRowDetail].guia.dependente.nmDependente;
                            } else
                            {
                                rowDetail.titularidade = "Titular";
                                rowDetail.beneficiario = data.data[iRowDetail].guia.cliente.nmCliente;
                            }
                            $scope.lSummaryGuiaServicoByServico.splice((count + baseNext), 0 , rowDetail);
                            baseNext++;                            
                        }
                        count++;
                    });
                }).catch(function(err) {
                    console.log(err);
                });
            });
        }

        function getListDetailServico(idServico)
        {
            var queryParams = {};
            queryParams = $location.search();
            queryParams.id_servico = idServico;
            return servicesFactory.getGuiaServicos.query(queryParams).$promise;
        }

        $scope.getDetailServico = function(idServico)
        {
            $scope.detailServicoShowing = idServico;
        };

        $scope.hideDetailServico = function(idServico)
        {
            $scope.detailServicoShowing = 0;
        };

        $scope.print = function()
        {
            window.print();
        }

        function getDataToPrint(queryParams)
        {
            queryParams.end_date = $location.search().end_date;
            queryParams.start_date = $location.search().start_date;
            servicesFactory.getGuiaServicos.query(
                queryParams
            ).$promise.then
            (
                function(data)
                {
                    for(var i = 0; i < data.data.length; i++)
                    {
                        var node = data.data[i];
                        if(undefined != node.guia.dependente && null != node.guia.dependente)
                        {
                            node.guia.beneficiario = {name: node.guia.dependente.nmDependente};
                        } else
                        {
                            node.guia.beneficiario = {name: node.guia.cliente.nmCliente};
                        }
                        $scope.dataToPrint.list.push(node);
                        $scope.dataToPrint.total += node.vlServicoAssociado;
                    }
                    if(data.total > data.limit && $scope.dataToPrint.list.length < data.total)
                    {
                        var params = queryParams;
                        params.offset = ++data.offset;
                        getDataToPrint(params);
                    } else
                    {
                        $scope.print();
                    }
                }
            );
        }

        $scope.exportPrint = function()
        {
            configDataToPrint();
            getDataToPrint({});
        };

        function configDataToPrint()
        {
            $scope.dataToPrint = {
                list:[],
                total: 0,
            };
        }

        function selectedOnlyIndex(index, list)
        {
            for (var i = 0; i < list.length; i++)
            {
                if((true == list[i] && index != i) || (false == list[i] && i == index))
                {
                    return false;
                }
            }
            return true;
        }
        callWhileInstantiating();
    }
)
.value('callWhileInstantiating', "constructorController");
