'use strict';

MedicsystemApp.controller(
    'CoberturaPlanoController',
    function(
        $scope, servicesFactory, callWhileInstantiating, NgTableParams
    )
    {
        var callWhileInstantiating = eval(callWhileInstantiating);

        callWhileInstantiating();

        function constructorController()
        {
            loadData();
        }

        function loadData()
        {
            servicesFactory.getCoberturaPlano.query(
                {},
                function(data)
                {
                    var dataPretty = [];
                    for (var i = 0; i < data.length; i++)
                    {
                        dataPretty[i] = data[i];
                        dataPretty[i].boCobertura = dataPretty[i].boCobertura ? "Sim" : "Não";
                    }
                    $scope.dataToPrinter = dataPretty;
                    setTable(dataPretty);
                }
            );
        }

        function setTable(item)
        {
            var initialParams = { count: 25};
            var initialSettings = {
                // page size buttons (right set of buttons in demo)
                counts:[],
                // determines the pager buttons (left set of buttons in demo)
                paginationMaxBlocks: 10,
                paginationMinBlocks: 2,
                dataset: item
            };
            $scope.tableParams = new NgTableParams(initialParams, initialSettings )
        };

        $scope.print = function()
        {
            window.print();
        }
    }
)
.value('callWhileInstantiating', "constructorController");
