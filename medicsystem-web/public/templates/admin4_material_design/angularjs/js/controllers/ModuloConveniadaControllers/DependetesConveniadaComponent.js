'use strict';

MedicsystemApp.component('dependentes', {
    templateUrl: 'templates/admin4_material_design/angularjs/views/conveniada/associado_dependentes.component.html',
    controller: function (servicesFactory, $scope, $rootScope){
        var self = this;
        let cliente = null;

        $scope.showForm = false;
        $scope.textButtonTrigger = "Adicionar novo Dependente";
        $scope.searchButtonText = "Salvar";

        $scope.herdarEndereco = 0;

        $scope.dependenteEditing = [];
        $scope.fieldsForm =
        {
            nrCpf: {disabledIfExist: true, disabledNow: false},
            nrRG: {disabledIfExist: true, disabledNow: false},
            nmDependente: {disabledIfExist: true, disabledNow: false},
            nrTelefone: {disabledIfExist: false, disabledNow: false},
            nrCelular: {disabledIfExist: false, disabledNow: false},
            dtNascimento: {disabledIfExist: true, disabledNow: false},
            inEstadoCivil: {disabledIfExist: true, disabledNow: false},
            inParentesco: {disabledIfExist: false, disabledNow: false},
            inSexo: {disabledIfExist: true, disabledNow: false},
            nmEmail: {disabledIfExist: false, disabledNow: false},
            nmOcupacao: {disabledIfExist: true, disabledNow: false},
            nrCEP: {disabledIfExist: true, disabledNow: false},
            nmLogradouro: {disabledIfExist: true, disabledNow: false},
            nrNumero: {disabledIfExist: true, disabledNow: false},
            nmComplemento: {disabledIfExist: true, disabledNow: false},
            cidade: {disabledIfExist: true, disabledNow: false},
            bairro: {disabledIfExist: true, disabledNow: false},
            nmPontoReferencia: {disabledIfExist: true, disabledNow: false}
        };

        $scope.newDependente = {};

        $scope.contrato = self.contrato;

        $scope.showFormNewDependente = function()
        {
            if($scope.showForm)
            {
                $scope.showForm = false;
                $scope.textButtonTrigger = "Adicionar novo Dependente";
            } else
            {
                $scope.showForm = true;
                $scope.textButtonTrigger = "Cancelar adição";
            }
        };

        $scope.parentescoOptions = [
             { id: 0,    description: 'Esposo(a)' },
             { id: 1,    description: 'Companheiro(a)' },
             { id: 2,    description: 'Filho(a)' },
             { id: 3,    description: 'Filho(a) Adotivo(a)' },
             { id: 4,    description: 'Pai' },
             { id: 5,    description: 'Mae' },
             { id: 6,    description: 'Irmao(a)' },
             { id: 7,    description: 'Avo/Avo' },
             { id: 8,    description: 'Sobrinho(a)' },
             { id: 9,    description: 'Enteado(a)' },
             { id: 10,    description: 'Sogro(a)' },
             { id: 11,    description: 'Neto(a)' },
             { id: 12,    description: 'Tio(a)' },
             { id: 13,    description: 'Primo(a)' },
             { id: 14,    description: 'Cunhado(a)' },
             { id: 15,    description: 'Nora' },
             { id: 16,    description: 'Genro' },
             { id: 17,    description: 'Amigo(a)' },
             { id: 18,    description: 'Outros' },
             { id: 19,    description: 'Nao Informado' }
        ];

        $scope.loadBairroOptions = function(cidade)
        {
            if (cidade) {
                $scope.bairrosOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
            }
        };

        $scope.cidadeOptions = servicesFactory.cidades.query();

        $scope.submitNewDependente = function(formHtml, newDependente){
            $scope.searchButtonText = "Salvando";
            if (formHtml.formNewDependente.$valid)
            {
                var jsonNewDependente = JSON.stringify(newDependente);
                servicesFactory.addContratoDependente.post
                (
                    {id: self.contrato},
                    jsonNewDependente,
                    function(data){
                        $scope.master = angular.copy({});
                        servicesFactory.getDependentesByIdContratoNonExclude.get({id: self.contrato}, function(data){
                            $scope.data = data;
                        });
                        $rootScope.alerts.push({
                            type: "info",
                            msg: "Dependente criado com sucesso!",
                            timeout: 10000 });
                        $scope.reset();
                        Metronic.scrollTop();
                    }, function(error)
                    {
                        console.error(error);
                        $scope.searchButtonText = "Salvar";
                        $scope.disabled = false;
                    }
                );
            } else {
                $scope.searchButtonText = "Salvar";
            }
        };

        servicesFactory.getDependentesByIdContratoNonExclude.get({id: self.contrato},function(data){
            $scope.data = data;
        });

        $scope.reset = function() {
            $scope.newDependente = angular.copy({});
            $scope.formNewDependente.$setPristine();
            $scope.formNewDependente.$setUntouched();
            $scope.formNewDependente.$submitted = false;
            $scope.searchButtonText = "Salvar";
        };

        $scope.inactivateDependente = function(id)
        {
            servicesFactory.inactiveContratoDependente.put
            (
                {id: id},
                null,
                function(data){
                    $scope.master = angular.copy({});
                    $rootScope.alerts.push({
                            type: "info",
                            msg: "Dependente inativado!",
                            timeout: 10000 }
                        );
                    servicesFactory.getDependentesByIdContratoNonExclude.get({id: self.contrato},
                        function(data){
                        $scope.data = data;
                    });
                    Metronic.scrollTop();
                }
            );
        };

        $scope.reactivateDependente = function(idDependente)
        {
            var jsonNewDependente = JSON.stringify({id: idDependente});
            servicesFactory.addContratoDependente.post
            (
                {id: self.contrato},
                jsonNewDependente,
                function(data){
                    $scope.master = angular.copy({});
                    $rootScope.alerts.push({
                            type: "info",
                            msg: "Dependente reativado!",
                            timeout: 10000
                        });
                    servicesFactory.getDependentesByIdContratoNonExclude.get({id: self.contrato},function(data){
                        $scope.data = data;
                    });
                    Metronic.scrollTop();
                }
            );
        };

        $scope.toggleFormDependente = function(idDependente)
        {
            if($scope.dependenteEditing[idDependente] == undefined || !$scope.dependenteEditing[idDependente])
            {
                $scope.dependenteEditing[idDependente] = true;
            } else
            {
                $scope.dependenteEditing[idDependente] = false;
            }
            blockFieldsUneditable();
            servicesFactory.getDependente.get( {iddependente: idDependente}, function(data)
            {
                if(!$scope.dependenteUpdate)
                {
                    $scope.dependenteUpdate = {};
                }
                $scope.dependenteUpdate[data.id] = data;
                loadComboOptions(data.id);
            });
        }

        $scope.submitEditDependente = function(formUpdate, dependenteUpdate)
        {
            if (formUpdate.formUpdateDependente.$valid)
            {
                var jsonDependenteUpdate = JSON.stringify(dependenteUpdate);
                servicesFactory.updateDependente.put
                (
                    {iddependente: dependenteUpdate.id},
                    jsonDependenteUpdate,
                    function(data){
                        $scope.master = angular.copy({});
                        servicesFactory.getDependentesByIdContratoNonExclude.get({id: self.contrato},function(data){
                            $scope.data = data;
                        });
                        $rootScope.alerts.push({
                            type: "info",
                            msg: "Dependente atualizado com sucesso!",
                            timeout: 10000 });
                        $scope.reset();
                        Metronic.scrollTop();
                    }
                );
            }
        }

        function blockFieldsUneditable()
        {
            for (const prop in $scope.fieldsForm)
            {
                $scope.fieldsForm[prop].disabledNow = $scope.fieldsForm[prop].disabledIfExist;
            }
        }

        $scope.setAddresWithAssociado = function()
        {
            getAssociado().$promise.then(copyAddresToNewDependente);
        }

        function copyAddresToNewDependente(data)
        {
            $scope.newDependente.nrCep = data.nrCEP;
            $scope.newDependente.nmLogradouro = data.nmLogradouro;
            $scope.newDependente.nrNumero = data.nrNumero;
            $scope.newDependente.nmComplemento = data.nmComplemento;
            $scope.newDependente.cidade = data.cidade;
            $scope.newDependente.bairro = data.bairro;
            $scope.bairrosOptions = servicesFactory.cidadeBairros.get({id: data.cidade.id});
            $scope.newDependente.nmPontoReferencia = data.nmPontoReferencia;
        }

        function getAssociado()
        {
            if(null == cliente)
            {
                cliente = servicesFactory.getClienteByIdContrato.get({id: self.contrato});
            }
            return cliente;
        }

    },
    bindings: {
        'contrato': '='
    }
});
