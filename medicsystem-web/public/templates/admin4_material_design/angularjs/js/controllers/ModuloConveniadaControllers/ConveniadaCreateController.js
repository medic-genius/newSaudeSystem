'use strict';

MedicsystemApp.controller(
    'newAssociadoController',
    function($scope, servicesFactory, $timeout, $rootScope, $location, $window, $route)
    {
        $scope.newAssociado = {};
        $scope.searchButtonText = "Salvar";
        $scope.test = "false";
        $scope.edit_exchange_setting_click = (function(){
            $scope.disabled = false;
        });
        $scope.master = {};
        $scope.validingCpf = false;

        $scope.reset = function() {
            unblockFieldsUneditable();
            $scope.newAssociado = angular.copy({});
            $scope.form.$setPristine();
            $scope.form.$setUntouched();
            $scope.form.$submitted = false;
            $scope.searchButtonText = "Salvar";
        };

        $scope.fieldsForm =
        {
            nrRG: {disabledIfExist: true, disabledNow: false},
            nmCliente: {disabledIfExist: true, disabledNow: false},
            nrTelefone: {disabledIfExist: false, disabledNow: false},
            nrCelular: {disabledIfExist: false, disabledNow: false},
            dtNascimento: {disabledIfExist: true, disabledNow: false},
            inEstadoCivil: {disabledIfExist: true, disabledNow: false},
            inSexo: {disabledIfExist: true, disabledNow: false},
            nmEmail: {disabledIfExist: false, disabledNow: false},
            nmOcupacao: {disabledIfExist: true, disabledNow: false},
            nrCEP: {disabledIfExist: false, disabledNow: false},
            nmLogradouro: {disabledIfExist: false, disabledNow: false},
            nrNumero: {disabledIfExist: false, disabledNow: false},
            nmComplemento: {disabledIfExist: false, disabledNow: false},
            cidade: {disabledIfExist: false, disabledNow: false},
            bairro: {disabledIfExist: false, disabledNow: false},
            nmPontoReferencia: {disabledIfExist: false, disabledNow: false}
        };

        $scope.interacted = function (field)
        {
            var ret = ($scope.submitted || (field ? field.$dirty : false));
            return ret;
        };

        $scope.addNewAssociado = function(newAssociado, form) {
            $scope.test = "true";
            $scope.searchButtonText = "Salvando";

            function addOneAssociado(objAssociado)
            {
                $scope.lAssociados.push(objAssociado);
            }

            if ($scope.form.$valid)
            {
                var newAssociadoToServer = {nrCPF: newAssociado.nrCPF};
                for (var property in $scope.fieldsForm)
                {
                    newAssociadoToServer[property] = newAssociado[property];
                }
                if(undefined != newAssociado.id && null != newAssociado.id)
                {
                    newAssociadoToServer.id = newAssociado.id;
                }
                var jsonData = JSON.stringify(newAssociadoToServer);
                servicesFactory.clienteSave.post(jsonData,function(data)
                {
                    $scope.master = angular.copy({});
                    $rootScope.alerts.push({
                        type: "info",
                        msg: "Cliente criado com sucesso!",
                        timeout: 8000 });
                    addOneAssociado({
                        id: data.cliente.id, cpf: data.cliente.nrCPF, nome: data.cliente.nmCliente, boInativadoConveniada: data.contrato.boInativadoConveniada
                    });
                    $scope.reset();
                    Metronic.scrollTop();
                }, function(error)
                {
                    console.error(error);
                    $scope.searchButtonText = "Salvar";
                    $scope.disabled = false;
                });
            } else
            {
                $scope.searchButtonText = "Salvar";
                $scope.disabled = false;
            }
        };

        $scope.estadoCivilOptions = [
            { id: 0,    description: 'Solteiro(a)' },
            { id: 1,    description: 'Casado(a)' },
            { id: 2,    description: 'Viuvo(a)' },
            { id: 3,    description: 'Desquitado(a)' },
            { id: 4,    description: 'Divorciado(a)' },
            { id: 5,    description: 'Outros' },
            { id: 6,    description: 'Nao informado' },
        ];
        $scope.cidadeOptions = servicesFactory.cidades.query();

        function blockFieldsUneditable()
        {
            for (const prop in $scope.fieldsForm)
            {
                $scope.fieldsForm[prop].disabledNow = $scope.fieldsForm[prop].disabledIfExist;
            }
        }

        function unblockFieldsUneditable()
        {
            for (const prop in $scope.fieldsForm)
            {
                $scope.fieldsForm[prop].disabledNow = false;
            }
        }

        $scope.$watch('newAssociado.nrCPF', function() {
            if(this.last)
            {
                $scope.validingCpf = true;
                servicesFactory.getAssociadoByCpf.get(
                    {cpf: this.last},
                    function(data)
                    {
                        if(data.id)
                        {
                            $scope.newAssociado = data;
                            blockFieldsUneditable();
                        } else
                        {
                            unblockFieldsUneditable();
                            var cpfTemp = $scope.newAssociado.nrCPF;
                            $scope.newAssociado = {nrCPF: cpfTemp};
                        }
                        $scope.validingCpf = false;
                    },
                    function(error)
                    {
                        console.error(error);
                    }
                );
            }
        }, true);

        $scope.$watch('newAssociado.nrCEP', function(newValue, oldValue)
        {
            resetAddressNonEditable();
            var cep = this.last ? this.last : "";
            if(cep)
            {
                cep = cep.toString();
                cep = cep.replace(/\D/g,'');
                if(cep.length == 8)
                {
                    $scope.fetchingAddress = true;
                    servicesFactory.getAddressByCep.get(
                        {cep: cep},
                        function(data)
                        {
                            $scope.fetchingAddress = false;
                            if(!data.erro)
                            {
                                $scope.fetchingAddress = false;
                                putCepOnModel(data);
                                putOthersElementsOfAddress(data);
                            }
                        }
                    );
                }
            }
        }, true);

        function putCepOnModel(address)
        {
            $scope.newAssociado.nmLogradouro = address.logradouro;
            $scope.newAssociado.nmComplemento = address.complemento;
            $scope.newAssociado.cidadeLabel = address.localidade;
            $scope.newAssociado.bairroLabel = address.bairro;
        }

        function resetAddressNonEditable()
        {
            $scope.newAssociado.nmLogradouro = "";
            $scope.newAssociado.nmComplemento = "";
            $scope.newAssociado.cidadeLabel = "";
            $scope.newAssociado.bairroLabel = "";
        }

        function putOthersElementsOfAddress(address)
        {
            putCidadeInNewAssociado(address);
        }

        function putCidadeInNewAssociado(address)
        {
            servicesFactory.getCidadeByNm.query(
                {nm_uf: address.uf, nm_cidade: address.localidade},
                function(data)
                {
                    $scope.newAssociado.cidade = data;
                    $scope.bairrosOptions = servicesFactory.cidadeBairros.get
                    (
                        { id: $scope.newAssociado.cidade.id},
                        function(lBairros)
                        {
                            putBairroInNewAssociado(address);
                        }
                    );
                }
            );
        }

        function putBairroInNewAssociado(address)
        {
            servicesFactory.getBairroByNm.query(
                {nm_bairro: address.bairro,id_cidade: $scope.newAssociado.cidade.id},
                function(data)
                {
                    $scope.newAssociado.bairro = data;
                }
            );
        }
    }
);

MedicsystemApp.config
(
    function(toastrConfig)
    {
        angular.extend
        (
            toastrConfig,
            {
                    closeButton: true,
                    debug: true,
                    newestOnTop: true,
                    progressBar: false,
                    positionClass: 'toast-top-center',
                    preventDuplicates: false,
                    showDuration: 300,
                    hideDuration: 1000,
                    timeOut: 2500,
                    extendedTimeOut: 1000,
                    showEasing: 'swing',
                    hideEasing: 'linear',
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                }
        );
    }
);

MedicsystemApp.config(function ($validatorProvider)
{
    $validatorProvider.setDefaults({
        errorElement : 'span', //default input error message container
        errorClass : 'help-block help-block-error', // default input error message class
        focusInvalid : true, // do not focus the last invalid input
        ignore : "", // validate all fields including form hidden input
        invalidHandler : function(event, validator) { //display error alert on form submit
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight : function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight : function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success : function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler : function(form) {
            error.hide();
        }
    });
});
