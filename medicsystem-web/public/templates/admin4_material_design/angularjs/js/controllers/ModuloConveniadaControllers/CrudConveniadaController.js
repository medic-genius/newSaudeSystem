'use strict';

MedicsystemApp.controller(
    'CrudConveniadaController',
    function($scope, $rootScope, $state,$stateParams,$filter,$sce,NgTableParams,$modal, Auth,$window, $http, servicesFactory, toastr, $ngBootbox)
    {
        var Cliente = servicesFactory.clientes;

        function messageToastr(type, message, title)
        {
            if( type == 'erro' )
               toastr.error(  message, title,{tapToDismiss:true,timeOut:5000});
            else if( type == 'sucesso' )
               toastr.success(  message, title,{tapToDismiss:true,timeOut:5000});
            else if( type == 'informacao' )
               toastr.info(  message, title,{tapToDismiss:true,timeOut:5000});
            else if( type == 'aviso' )
               toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000});
        };

        function configurarTable( item )
        {
            var initialParams = { count: 10 };
            var initialSettings = {
                // page size buttons (right set of buttons in demo)
                counts:[],
                // determines the pager buttons (left set of buttons in demo)
                paginationMaxBlocks: 10,
                paginationMinBlocks: 2,
                dataset: item
            };
            $scope.tableParams = new NgTableParams(initialParams, initialSettings )
        };

        $scope.loadBairroCobrancaOptions = function(cidade) {
            if (cidade) {
               $scope.bairrosCobrancaOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
            }
        };

        $scope.alterarStatusValidacaoCidade = function()
        {
            if( $scope.comboValidacao && $scope.comboValidacao.invalidCidade )
                $scope.comboValidacao.invalidCidade = false;
        };

        $scope.loadBairroOptions = function(cidade)
        {
            if (cidade) {
                $scope.bairrosOptions = servicesFactory.cidadeBairros.get({ id: cidade.id });
                $scope.alterarStatusValidacaoCidade();
            }
        };

        function loadComboOptions(idClient)
        {
            $scope.estadoCivilOptions = [
                { id: 0,    description: 'Solteiro(a)' }, { id: 1,    description: 'Casado(a)' },
                { id: 2,    description: 'Viuvo(a)' }, { id: 3,    description: 'Desquitado(a)' },
                { id: 4,    description: 'Divorciado(a)' }, { id: 5,    description: 'Outros' },
                { id: 6,    description: 'Nao informado' },
            ];

            $scope.percentualOptions = [
                { id: 10,    description: '10%' }, { id: 20,    description: '20%' },
                { id: 30,    description: '30%' }, { id: 40,    description: '40%' }, { id: 50,    description: '50%' }
            ];

            $scope.rdOptionsMigrar = [
                { value: 0, name: 'Não Associado'}, { value: 1,    name: 'Associado' }
            ];

            $scope.rdOptionsPagamento = [
                { value: 0, name: 'Boleto Bancário'},
                { value: 1, name: 'Débito Automático'},
                { value: 3, name: 'Cartão Recorrente'},
            ]

            $scope.cidadeOptions = servicesFactory.cidades.query();
            if($scope.associadosUpdate[idClient].cidade && $scope.associadosUpdate[idClient].cidade.id != null){
                $scope.loadBairroOptions($scope.associadosUpdate[idClient].cidade);
            }
        }

        $scope.cancel = function (formUpdateAssociado, associadoId)
        {
            servicesFactory.clientes.get( {id: associadoId},function(data){
                $scope.associadosUpdate[data.id] = data;
                loadComboOptions(associadoId);
                $scope.associadosUpdate[data.id] = angular.copy(data);
                angular.copy({}, formUpdateAssociado);
            });
        };

        $scope.fieldsForm =
        {
            nrCPF: {editable: true, disabledNow: true},
            nrRG: {editable: true, disabledNow: true},
            nmCliente: {editable: true, disabledNow: true},
            nrTelefone: {editable: true, disabledNow: true},
            nrCelular: {editable: true, disabledNow: true},
            dtNascimento: {editable: true, disabledNow: true},
            inEstadoCivil: {editable: true, disabledNow: true},
            inSexo: {editable: true, disabledNow: true},
            nmEmail: {editable: true, disabledNow: true},
            nmOcupacao: {editable: true, disabledNow: true},
            nrCEP: {editable: true, disabledNow: true},
            nmLogradouro: {editable: true, disabledNow: true},
            nrNumero: {editable: true, disabledNow: true},
            nmComplemento: {editable: true, disabledNow: true},
            cidade: {editable: true, disabledNow: true},
            bairro: {editable: true, disabledNow: true},
            nmPontoReferencia: {editable: true, disabledNow: true}
        };

        function unblockFieldsEditable()
        {
            for (const prop in $scope.fieldsForm)
            {
                $scope.fieldsForm[prop].disabledNow = !$scope.fieldsForm[prop].editable;
            }
        }

        $scope.editAssociado = function(associado)
        {
            $scope.searchButtonText = "Atualizar";
            $scope.test = "false";
            $scope.edit_exchange_setting_click = (function(){
                $scope.disabled = false;
            });

            servicesFactory.clientes.get( {id: associado},function(data){
                if(!$scope.associadosUpdate)
                {
                    $scope.associadosUpdate = {};
                }
                $scope.associadosUpdate[data.id] = data;
                loadComboOptions(data.id);
                unblockFieldsEditable();
            });
        };

        $scope.updateAssociado = function(formUpdateAssociado, idAssociado)
        {
            if (formUpdateAssociado.formUpdate.$valid)
            {
                var upcliente = Cliente.get({id: $scope.associadosUpdate[idAssociado].id},function()
    			{
                    $scope.associadosUpdate[idAssociado].digitalCliente = upcliente.digitalCliente;
                    Cliente.update( { id: $scope.associadosUpdate[idAssociado].id }, $scope.associadosUpdate[idAssociado],
                    function(data)
                    {
                        $rootScope.addDefaultTimeoutAlert("Cliente", "atualizado", "success");
                        $scope.associadosUpdate[idAssociado] = Cliente.get({id: $scope.associadosUpdate[idAssociado].id}, function(){
                           //atualizar situacao geral
                            Cliente.query({ nrCodCliente: String($scope.associadosUpdate[idAssociado].nrCodCliente) }, function(data){});
                        });
                        if ($scope.updateExistCliente == true)
                        {
                            $scope.fluxControl = {};
                            $scope.fluxControl['wasCadastrado'] = true;
                            $scope.form.next();
                        }
                    }, function(error){
                        $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                    });
                    Metronic.scrollTop();
                },function(error ) {
                    $rootScope.addDefaultTimeoutAlert("Cliente", "atualizar", "error");
                });
            }
        };

        function getAssociados(tipo)
        {
            $scope.lAssociados = [];
            $scope.tipo = tipo;
            servicesFactory.getAssociados.query({in_situacao: tipo},function(datas)
            {
                for ( var i = 0 ; i < datas.length ; i++)
                {
                    $scope.lAssociados[i] =
                    {
                        id: datas[i].idCliente,
                        cpf: datas[i].nrCPF,
                        nome: datas[i].nmCliente,
                        nrContrato: datas[i].nrContrato,
                        insituacao: datas[i].insituacao,
                        idContrato: datas[i].idContrato,
                        idContratoCliente: datas[i].idContratoCliente,
                    };
                }
                configurarTable($scope.lAssociados);
            });
        }

        $scope.callGetAssociados = function(tipo)
        {
            getAssociados(tipo);
        }

        $scope.inactivateAssociado = function(id)
        {
            $ngBootbox.confirm('Deseja realmente inativar o associado?')
            .then(function()
            {
                servicesFactory.inactivateAssociado.put({id: id},function(data)
                {
                    getAssociados(0);
                    $rootScope.alerts.push({
                            type: "success",
                            msg: "Beneficiario inativado",
                            timeout: 5000 }
                        );
                    Metronic.scrollTop();
                });
            });
        }

        $scope.reactivateAssociado = function(id)
        {
            $ngBootbox.confirm('Deseja realmente reativar o associado?')
            .then(function()
            {
                servicesFactory.reactivateAssociado.put({id: id},function(data)
                {
                    getAssociados(0);
                    $rootScope.alerts.push(
                            {
                                type: "success",
                                msg: "Beneficiario reativado",
                                timeout: 5000
                            }
                        );
                    Metronic.scrollTop();
                });
            });
        }

        $scope.openModalGenerateGuia = function(idContrato)
        {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'templates/admin4_material_design/angularjs/views/conveniada/modal_guia.html',
                controller: 'EmitirGuiaController',
                size: "lg",
                resolve: {
                    idContrato: function () {
                        return idContrato;
                    }
                }
            });

            modalInstance.result.then(function (retorno)
            {
            }, function ()
            {
            });
        };

    	function inicio()
        {
            getAssociados(0);
    	};

        inicio();
    }
);

MedicsystemApp.config
(
    function(toastrConfig)
    {
        angular.extend
        (
            toastrConfig,
            {
                    closeButton: true,
                    debug: true,
                    newestOnTop: true,
                    progressBar: false,
                    positionClass: 'toast-top-center',
                    preventDuplicates: false,
                    showDuration: 300,
                    hideDuration: 1000,
                    timeOut: 2500,
                    extendedTimeOut: 1000,
                    showEasing: 'swing',
                    hideEasing: 'linear',
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                }
        );
    }
);

MedicsystemApp.config(function ($validatorProvider) {
    $validatorProvider.setDefaults({
        errorElement : 'span', //default input error message container
        errorClass : 'help-block help-block-error', // default input error message class
        focusInvalid : true, // do not focus the last invalid input
        ignore : "", // validate all fields including form hidden input
        invalidHandler : function(event, validator) { //display error alert on form submit
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight : function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight : function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success : function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler : function(form) {
            error.hide();
        }
    });
});
