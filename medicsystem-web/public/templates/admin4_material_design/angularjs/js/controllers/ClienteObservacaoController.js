'use strict';

MedicsystemApp.controller('ClienteObservacaoController', function($rootScope,
		$scope, $http, $timeout, $state, $stateParams, servicesFactory, Auth) {

	var Observacao = servicesFactory.observacoes;

	$scope.refreshObservacoes = function() {
		if ($scope.selectedCliente && $scope.selectedCliente.id) {
			$scope.observacoes = servicesFactory.clienteObservacoes.get({
				id : $scope.selectedCliente.id
			});
		}
	}

	$scope.saveObservacao = function() {
		if ($('#formObservacao').valid()) {
			$scope.buttonDisabled = true;
			$scope.observacao.$save(function(object, responseHeaders) {
				var location = responseHeaders('Location');
				if (location) {
					$rootScope.alerts.push({
						type : "info",
						msg : "Observacao criada com sucesso",
						timeout : 10000
					});
					Metronic.scrollTop();

					$state.go('atendimentoCliente.cliente-observacoes');
				}
			});
		}
	}

	$scope.remove = function(item) {
		if (item && item.id) {
			Observacao.get({
				id : item.id
			}, function(u, getResponseHeaders) {
				u.$remove(function(u, putResponseHeaders) {
					$rootScope.alerts.push({
						type : "info",
						msg : "Observacao removida com sucesso",
						timeout : 10000
					});
					Metronic.scrollTop();

					$scope.refreshObservacoes();
				});
			});
		}
	}

	if ($state.is('atendimentoCliente.cliente-observacoes')) {
		// INIT
		$scope.refreshObservacoes();
	}

	if ($state.is('atendimentoCliente.cliente-observacoes-novo')) {
		$scope.observacao = new Observacao({});
		$scope.observacao.dtObservacaoCliente = new Date();
		$scope.observacao.cliente = {};
		$scope.observacao.cliente.id = $scope.selectedCliente.id;

	}

	if ($state.is('atendimentoCliente.cliente-observacoes-editar')) {
		if ($stateParams.id) {
			$scope.observacao = Observacao.get({
				id : $stateParams.id
			});
		}
	}

});