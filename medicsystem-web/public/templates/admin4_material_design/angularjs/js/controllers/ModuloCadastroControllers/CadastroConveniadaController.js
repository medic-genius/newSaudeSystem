'use strict';

MedicsystemApp.controller('CadastroConveniadaController',
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout, Auth, $ngBootbox) {
     $scope.enable = ['administrador'];
     $scope.valideBotao = !$scope.checkEditRole($scope.enable);

     $scope.TiposPagamentos = [
         { id: 1,    description: 'DESPESA' },
         { id: 2,    description: 'MENSALIDADE' },
     ];

     $scope.acrescimoVencimento = [
         { id: 1,    description: '30 DIAS' },
         { id: 2,    description: '60 DIAS' },
         { id: 3,    description: '90 DIAS' },
         { id: 4,    description: 'NENHUM' },
     ];

    $scope.clear = function (){
        $scope.empresa.inTipoCobranca = 0;
    };

    function messageToastr(type, message, title) {
        if( type == 'erro' ) {
           toastr.error(  message, title.toUpperCase() ); 
        } else if( type == 'sucesso' ) {
           toastr.success(  message, title.toUpperCase() ); 
        } else if( type == 'informacao' ) {
           toastr.info(  message, title.toUpperCase() ); 
        }
    };

    function configurarTable( item ) {

        var initialParams = { count: 10 };


        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[],
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )
    };

    function configurarTableFunc( item ) {

        var initialParams = { count: 10 };


        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[],
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableFunc = new NgTableParams(initialParams, initialSettings )
    };

    $scope.inativarEmpresa = function (empresa) {
        $ngBootbox.confirm('Deseja realmente inativar esta empresa?')
            .then(function() {
                cadastroFactory.inativarEmpresa.update({id: empresa.id}, function(dados){
                    messageToastr("sucesso","Empresa inativada","SUCESSO");
                    $scope.getEmpresas($scope.tipo);
                });
            });
    };

    $scope.ativarEmpresa = function (empresa) {
        $ngBootbox.confirm('Deseja realmente ativar esta empresa?')
            .then(function() {
                cadastroFactory.ativarEmpresa.update({id: empresa.id}, function(dados){
                        messageToastr("sucesso","Empresa ativada","SUCESSO");
                        $scope.getEmpresas($scope.tipo);
                });
            });
    };

    $scope.returnConv = function (){
        $scope.getEmpresas($scope.tipo);
    };

	$scope.empresa = {};
    $scope.novoConv = function (){
        $scope.showValor = false;
        $scope.tipo = {cobranca: null, fixo: null};
        $scope.empresa = {};
        $scope.cidadeOptions = servicesFactory.cidades.query();
    };

    $scope.getBairros = function (cidade){
        $scope.empresa.bairro = null;
        servicesFactory.cidadeBairros.get({id: cidade.id},function(dados){
            $scope.bairrosOptions = dados;
        });
    };

    function validarDadosEmpresa(empresa, tipo){
        var isValido = true;
        var msgRequired = "é obrigatória (o).";
        var msgPreencha = "Por favor, preencha ";
        var msgSelect = "Por favor, selecione ";
        /*DADOS*/
        if(!empresa.nmRazaoSocial){
            isValido = false;
            messageToastr('erro', msgPreencha + 'a razao social ' + msgRequired, 'Calma');
        }

        if(!empresa.nmFantasia){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o Nome Fantasia '  + msgRequired, 'Calma');
        }

        if(!empresa.nrCnpj){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o CNPJ '  + msgRequired, 'Calma');
        }

        /*ENDERECO */
        if(!empresa.nrCep){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o CEP '  + msgRequired , 'Calma');
        }

        if(!empresa.nmLogradouro){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o Logradouro ' + msgRequired , 'Calma');
        }

        if(!empresa.nrNumero){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o Número ' + msgRequired , 'Calma');
        }

        if(!empresa.cidade){
            isValido = false;
            messageToastr('erro', msgSelect + 'a cidade '+ msgRequired, 'Calma');
        }

        if(!empresa.bairro){
            isValido = false;
            messageToastr('erro', msgSelect + 'o bairro ' + msgRequired , 'Calma');
        }
        /*CONTATOS*/
        if(!empresa.nrTelefone1){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o telefone ' + msgRequired, 'Calma');
        }

        /*Configuracoes*/
        if(!empresa.diaVencimento){
            isValido = false;
            messageToastr('erro', msgPreencha + 'o dia de vencimento ' + msgRequired, 'Calma');
        }

        if(!empresa.inTipoPagamento){
            isValido = false;
            messageToastr('erro', msgPreencha + 'a forma de pagamento ' + msgRequired, 'Calma');
        }else {
            if(empresa.inTipoPagamento == 2){
                if(tipo){
                    if(!tipo.cobranca && tipo.cobranca == null && tipo.cobranca == undefined){
                        isValido = false;
                        messageToastr('erro', msgSelect + 'o tipo de cobrança ' + msgRequired , 'Calma');
                    }
                }
               
            }
            if(tipo){
                if((tipo.cobranca == 0 || tipo.cobranca == 1) && empresa.inTipoPagamento == 2){
                    if(!tipo.fixo && tipo.fixo == null && tipo.fixo == undefined){
                        isValido = false;
                         messageToastr('erro', msgPreencha + 'o Valor ' + msgRequired , 'Calma');
                    }
                }
            }
            
        }

        return isValido;
    }

    function enviarMensagemDuplicado() {
        $ngBootbox.alert('CNPJ duplicado !')
        .then(function() {
            $scope.empresa.nrCnpj = '';
            $scope.isCnpjDuplicado = false;  
        });
    };
    $scope.verificarCNPJDuplicado = function(cnpj){


        cnpj = cnpj ? cnpj : "";

        if( cnpj.trim().length > 0 ) {
            servicesFactory.empresaClienteByCnpj
            .get( {cnpj:cnpj }, function( result ) {
                if( result.length > 0 ) {
                    $scope.isCnpjDuplicado = true;
                    enviarMensagemDuplicado();  
                }
            });
        }
    }

    $scope.addConv = function(empresa, tipo){
        var empresaValida = validarDadosEmpresa(empresa, tipo);

        if(empresaValida){
            empresa.nrCnpj = empresa.nrCnpj.replace(/\D/g, '');
            empresa.dtCadastro = moment().format("YYYY-MM-DD");
            empresa.nmRazaoSocial = empresa.nmRazaoSocial.toUpperCase();
            empresa.nmFantasia = empresa.nmFantasia.toUpperCase();
            empresa.nmLogradouro = empresa.nmLogradouro.toUpperCase();

            if(empresa.nmComplemento != null){
                empresa.nmComplemento = empresa.nmComplemento.toUpperCase();
            }
            if(empresa.nmPontoReferencia != null){
                empresa.nmPontoReferencia = empresa.nmPontoReferencia.toUpperCase();
            }
            if(empresa.nmEmail != null){
                empresa.nmEmail = empresa.nmEmail.toUpperCase();
            }
            if(empresa.nmSite != null){
                empresa.nmSite = empresa.nmSite.toUpperCase();
            }

            cadastroFactory.createEmpresa.save({tipoCobranca: tipo.cobranca, vlFixo: tipo.fixo}, empresa, function(success){
                 messageToastr("sucesso", "Empresa cadastrada", "SUCESSO");
                 $state.go('cadastroConveniada', null, { reload: true });
            }, function(erro){
                messageToastr("erro", "Erro no envio de dados", "Erro");
            });
        }
    };

    $scope.editarEmpresa = function(empresa){

        $scope.dadosFunc = [];
        $scope.empresaCancel = empresa;
        $scope.cancelar = false;

        $scope.cidadeOptions = servicesFactory.cidades.query();

        servicesFactory.cidadeBairros.get({ id: empresa.cidade.id },function(dados){
            $scope.bairrosOptions = dados;
        });

        $scope.empresa = empresa;

        servicesFactory.contratoEmpresaCliente.get({id: $scope.empresa.id},
			function(dados)
			{
            	$scope.contrato = {id: dados.id,
                               nmEmpresaGrupo: dados.empresaGrupo.nmFantasia,
                               nrContrato: dados.nrContrato,
                               data: dados.dtContratoFormatado,
                               boBloqueado: dados.boBloqueado,
                               vlTotal: dados.vlTotal};

            if(dados.informaPagamento == 0)
                $scope.contrato.inFormaPagamento = "BOLETO BANCÁRIO";
            else if (dados.informaPagamento == 1)
                $scope.contrato.inFormaPagamento = "DÉBITO AUTOMÁTICO";
        });
    };

    $scope.getFuncionarios = function(empresa){
        cadastroFactory.dadosEmpresaCliente.get({id: empresa.id}, function (dados){
            $scope.dadosFunc = dados;
            configurarTableFunc($scope.dadosFunc);
        });
    };

    $scope.atualizarEmpresa = function (empresa){
        delete empresa.expanded;
        var isValido = validarDadosEmpresa(empresa);
        if(isValido){
            empresa.dtAlteracao = moment().format('YYYY-MM-DD');
            empresa.nmRazaoSocial = empresa.nmRazaoSocial.toUpperCase();
            empresa.nmFantasia = empresa.nmFantasia.toUpperCase();
            empresa.nmLogradouro = empresa.nmLogradouro.toUpperCase();

            if (empresa.nmComplemento != null)
                empresa.nmComplemento = empresa.nmComplemento.toUpperCase();

            if (empresa.nmPontoReferencia != null)
                empresa.nmPontoReferencia = empresa.nmPontoReferencia.toUpperCase();

            if (empresa.nmEmail != null)
                empresa.nmEmail = empresa.nmEmail.toUpperCase();

            if (empresa.nmSite != null)
                empresa.nmSite = empresa.nmSite.toUpperCase();

            cadastroFactory.createEmpresa.update({idContrato: $scope.contrato.id,vlTotal: $scope.contrato.vlTotal}, empresa,function(success){
                empresa.expanded = false;
                $scope.getEmpresas($scope.tipo);
                messageToastr("sucesso", "Empresa atualizada", "SUCESSO");
                $state.go('cadastroConveniada', null, { reload: true });
            }, function(erro){
                messageToastr("erro", "Erro no envio de dados", "Erro");
            });
        }
    };

    $scope.cancel = function () {
        $scope.empresaCancel.expanded = false;
        $scope.cancelar = true;
    };

    // 0 = ATIVA    1 = INATIVA
    $scope.getEmpresas = function(tipo){
        $scope.empresasAtivas = [];
        $scope.empresasInativas = [];
        $scope.tipo = tipo;
        if (tipo == 0){
            servicesFactory.empresasClienteAtivas.query(function(dados){
                for ( var i = 0 ; i < dados.length ; i++){
                    dados.expanded = false;
                }
                $scope.empresasAtivas = dados;
                configurarTable($scope.empresasAtivas);
            });
        }else {
            servicesFactory.empresasClienteInativas.query(function(dados){
                for ( var i = 0 ; i < dados.length ; i++){
                    dados.expanded = false;
                }
                $scope.empresasInativas = dados;
                configurarTable($scope.empresasInativas);
            });
        }
    }

	$scope.inicio = function() {
        $scope.getEmpresas(0);

	};

	$scope.inicio();
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
