'use strict';

MedicsystemApp.controller('CadastroBancoController', 
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout, $ngBootbox) {

    function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };
        
    function configurarTable( item ) {

        var initialParams = { count:5 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item.agencias
        };

        item.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  

    $scope.getAgencias = function(item){

       
            var rParams = {
			    idBanco: item.id
		    }
            servicesFactory.allAgenciasBanco.get(rParams,function(dados){
                if(dados.length > 0){
                    for ( var i = 0 ; i < dados.length ; i++){
                    dados.expanded = false;
                    }
                    $scope.showEmpty = false;
                    item.agencias = {};
                    item.agencias = dados;
                    configurarTable(item);
                }else{
                    $scope.showEmpty = true;
                }
            });
        
    };

    $scope.getBancos = function(){
        servicesFactory.allBancos.get(
            function(response) {
                for ( var i = 0 ; i < response.length ; i++){
                    response[i].expanded = false;
                }
                $scope.listaBancos = response;
            }
        );
    };

    $scope.novaAgencia = function (){
        $scope.cidadeOptions = servicesFactory.cidades.query();
        $scope.ag = {};
        $scope.cidade = null;
    };

    $scope.changeCidade = function (cidade){
        $scope.cidade = cidade;
    };

    $scope.addAgencia = function(banco){
    $scope.banco = {cdbanco: banco.cdbanco,
                    dtAtualizacaoLog: banco.dtAtualizacaoLog,
                    dtInclusaoLog: banco.dtInclusaoLog,
                    id: banco.id,
                    nmBanco: banco.nmBanco};
        if ($scope.ag.nmAgencia != null && $scope.ag.nrAgencia != null && $scope.ag.digito != null && $scope.banco != null && $scope.cidade != null){
            $scope.ag.nmAgencia = $scope.ag.nmAgencia.toUpperCase();

            var novaAgencia = {nmAgencia: $scope.ag.nmAgencia,
                            nrAgencia: $scope.ag.nrAgencia,
                            nrDigitoVerificador: $scope.ag.digito,
                            banco: $scope.banco,
                            cidade: $scope.cidade};

            cadastroFactory.createAgencia.save({},novaAgencia,
                function(result){
                    messageToastr("sucesso","Agência cadastrada","SUCESSO");
                    
            });
        }
        else{
            messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO");
        }
    };

    $scope.addBanco = function(){
        if ($scope.newBanco.cod != null && $scope.newBanco.nmBanco != null){
            $scope.newBanco.nmBanco = $scope.newBanco.nmBanco.toUpperCase();
            var novoBanco = {cdbanco: $scope.newBanco.cod,
                            nmBanco: $scope.newBanco.nmBanco};
            cadastroFactory.createBanco.save({}, novoBanco,
                function(result){
                    messageToastr("sucesso","Banco cadastrado","SUCESSO");
                    
                });
        }else{
            messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
        }
        
    };

    $scope.returnAgencias = function (banco){
        $scope.showEdit = false;
        $scope.getAgencias(banco);
    };

    $scope.editarAgencia = function(agencia){
        $scope.agenciaFront = agencia;
        $scope.showEdit = true;
        $scope.cancelar = false;
        $scope.ag = {};
        $scope.ag = {nrAgencia: agencia.nrAgencia,
                digito: agencia.nrDigitoVerificador,
                nmAgencia: agencia.nmAgencia,
                idCidade: agencia.cidade.id,
                idAgencia: agencia.id};
    
    };

    $scope.updateAgencia = function (banco){
        if ($scope.cancelar == true){
            $scope.showEdit = false;
        }else{
            $scope.banco = {cdbanco: banco.cdbanco,
                        dtAtualizacaoLog: banco.dtAtualizacaoLog,
                        dtInclusaoLog: banco.dtInclusaoLog,
                        id: banco.id,
                        nmBanco: banco.nmBanco};
                        
            if ($scope.ag.nmAgencia != null && $scope.ag.nrAgencia != null && $scope.ag.digito != null && $scope.banco != null && $scope.ag.idCidade != null){
                $scope.ag.nmAgencia = $scope.ag.nmAgencia.toUpperCase();
                $scope.cidade = {id:$scope.ag.idCidade};
                var updateAgencia = {nmAgencia: $scope.ag.nmAgencia,
                                id: $scope.ag.idAgencia,
                                nrAgencia: $scope.ag.nrAgencia,
                                nrDigitoVerificador: $scope.ag.digito,
                                banco: $scope.banco,
                                cidade: $scope.cidade};

                cadastroFactory.createAgencia.update({},updateAgencia,
                    function(result){
                        messageToastr("sucesso","Agência atualizada","SUCESSO")
                        $scope.returnAgencias(banco);
                });
            }
            else{
                messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
            }
        }
    };

    $scope.cancel = function () {
        $scope.cancelar = true;
        $scope.agenciaFront.expanded = false;
	};

	$scope.inicio = function() {
        $scope.cidadeOptions = servicesFactory.cidades.query();
        $scope.cidade = null;
        $scope.banco = null;
        $scope.ag = {};
        $scope.newBanco = {};
        $scope.showEdit = false;
        $scope.getBancos();
	};

	$scope.inicio();			
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
