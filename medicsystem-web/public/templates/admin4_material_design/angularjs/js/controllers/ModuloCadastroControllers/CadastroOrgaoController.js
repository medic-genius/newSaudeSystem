'use strict';

MedicsystemApp.controller('CadastroOrgaoController', 
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout, $ngBootbox) {

     $scope.excluirOrgao = function (orgao) {
        $ngBootbox.confirm('Deseja realmente excluir este órgão?')
            .then(function() {
                cadastroFactory.inativarOrgao.update({id: orgao.id}, function(dados){
                    messageToastr("sucesso","Órgão excluido","SUCESSO");
                    $scope.esferaOrgao($scope.esferaReturn);
                });
            });
    };

     $scope.inEsfera = [
         { id: 0,    description: 'MUNICIPAL' },
         { id: 1,    description: 'ESTADUAL' },
         { id: 2,    description: 'FEDERAL' }
     ];

    function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };

    function configurarTable( item ) {

        var initialParams = { count: 10 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  

    $scope.returnOrgaos = function (){
        if ($scope.esferaReturn != null){
            $scope.esferaOrgao($scope.esferaReturn);
        }
    };

    $scope.novoOrgao = function (){
        $scope.orgao = {cdOrgao: null,
                        nmOrgao: null,
                        inEsfera: null,
                        grupoOrgao: null};
    };

    $scope.addOrgao = function(orgao){
        if ($scope.orgao.inEsfera != null && $scope.orgao.grupoOrgao != null){
            for ( var i = 0; i < $scope.gruposOrgaos.length; i++){
                if ($scope.orgao.grupoOrgao == $scope.gruposOrgaos[i].id){
                    $scope.orgao.grupoOrgao = $scope.gruposOrgaos[i];
                }
            }

            $scope.orgao.nmOrgao = $scope.orgao.nmOrgao.toUpperCase();

            cadastroFactory.createOrgao.save({}, $scope.orgao, function(dados){
                messageToastr("sucesso","Órgão cadastrado","SUCESSO");
            })
        }else{
            messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
        }
    };
    

    $scope.editarOrgao = function(orgao){
        $scope.orgao = {cdOrgao: orgao.cdOrgao,
                        id: orgao.id,
                        nmOrgao: orgao.nmOrgao,
                        inEsfera: orgao.inEsfera,
                        grupoOrgao: orgao.grupoOrgao.id};
    };

    $scope.atualizarOrgao = function (orgao){
        $scope.orgao.grupoOrgao = orgao.grupoOrgao;
        if ($scope.orgao.id != null && $scope.orgao.cdOrgao != null && $scope.orgao.nmOrgao != null && $scope.orgao.inEsfera != null && $scope.orgao.grupoOrgao != null){
            $scope.orgao.nmOrgao = $scope.orgao.nmOrgao.toUpperCase();
            
            cadastroFactory.createOrgao.update({}, $scope.orgao, function(dados){
                messageToastr("sucesso","Órgão atualizado","SUCESSO");
                orgao.expanded = false;
                $scope.esferaOrgao($scope.orgao.inEsfera);
            });
        }else{
            messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
        }
    };

    $scope.cancel = function (item) {
		item.expanded = false;
    };

    $scope.esferaOrgao = function(esfera){
        $scope.esferaReturn = esfera;
        servicesFactory.orgaoByEsfera.get({inEsfera: esfera}, function(dados){
            for ( var i = 0 ; i < dados.length ; i++){
                dados.expanded = false;
            }

            $scope.orgaoOptions = dados;
            configurarTable($scope.orgaoOptions);

        });
    }

	$scope.inicio = function() {
        $scope.orgao = {codigo: null,
                        nmOrgao: null,
                        inEsfera: null,
                        inGrupo: null};
        $scope.esferaOrgao(0);
        servicesFactory.allGrupoOrgaos.get(function(dados){
            $scope.gruposOrgaos = dados;
        });

	};

	$scope.inicio();			
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
