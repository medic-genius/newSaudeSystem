'use strict';

MedicsystemApp.controller('CadastroServicoController', 
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout, $ngBootbox) {

    $scope.excluirServico = function (servico) {
        $ngBootbox.confirm('Deseja realmente excluir este serviço?')
            .then(function() {
                cadastroFactory.inativarServico.update({id: servico.id}, function(dados){
                    messageToastr("sucesso","Serviço excluido","SUCESSO");
                    $scope.tipoServico($scope.tipoLoadAuto);
                });
            });
    };

    $scope.tipoServicoOptions = [
         { id: 0,    description: 'MÉDICO' },
         { id: 2,    description: 'LABORATORIAL' },
         { id: 3,    description: 'ESTÉTICA' }
     ];

    function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };
        
    function configurarTable( item ) {

        var initialParams = { count: 10 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  
   
    $scope.editarServico = function (servico) {
        $scope.servico = {id: servico.id,
                    inTipoServico: servico.inTipoServico,
                    nrCodServico: servico.nrCodServico,
                    nmServico: servico.nmServico,
                    vlCusto: servico.vlCusto,
                    vlServicoAssociado: servico.vlServicoAssociado,
                    vlServico: servico.vlServico,
                    validade: servico.validade,
                    validadeContratual: servico.validadeContratual,
                    nmPreparatorio: servico.nmPreparatorio,
                    boGratuito: servico.boGratuito,
                    boBloqueio: servico.boBloqueio,
                    boValorAberto: servico.boValorAberto,
                    boTriagem: servico.boTriagem,
                    boGeraRetornoAgendamento: servico.boGeraRetornoAgendamento,
                    boSite: servico.boSite,
                    boLaudo: servico.boLaudo,      
                    dtInclusao: servico.dtInclusao};

    };

    $scope.atualizarServico = function (servico){
         if ($scope.cancelar == true){
            servico.expanded = false;
        }else{
            if ($scope.servico.nmPreparatorio != null)
                $scope.servico.nmPreparatorio = $scope.servico.nmPreparatorio.toUpperCase();

            $scope.servico.nmServico = $scope.servico.nmServico.toUpperCase();

            cadastroFactory.createServico.update({},$scope.servico,
                function(result){
                    messageToastr("sucesso","Serviço atualizado","SUCESSO")
                    servico.expanded = false;
                    $scope.tipoServico($scope.tipoLoadAuto);
                });
        }
    };

    $scope.novoServico = function (){
        $scope.servico = {inTipoServico: null,
                    nrCodServico: null,
                    nmServico: null,
                    vlCusto: null,
                    vlServicoAssociado: null,
                    vlServico: null,
                    validade: null,
                    validadeContratual: null,
                    nmPreparatorio: null,
                    boGratuito: false,
                    boBloqueio: false,
                    boValorAberto: false,
                    boTriagem: false,
                    boGeraRetornoAgendamento: false,
                    boSite: false,
                    boLaudo:false};
    };  
    
    $scope.addServico = function (){
        if ($scope.servico.inTipoServico != null){
            if ($scope.servico.nmPreparatorio != null)
                $scope.servico.nmPreparatorio = $scope.servico.nmPreparatorio.toUpperCase();

            $scope.servico.nmServico = $scope.servico.nmServico.toUpperCase();
            $scope.servico.dtInclusao = moment().format('YYYY-MM-DD');

            cadastroFactory.createServico.save({},$scope.servico,
                function(result){
                    messageToastr("sucesso","Serviço cadastrado","SUCESSO");
                    
            });
        }else{
                messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
        }

    };

    $scope.tipoServico = function (tipo) {
        $scope.tipoLoadAuto = tipo;
        servicesFactory.servicosByTipo.get({inTipo: tipo}, function(dados){
            for ( var i = 0 ; i < dados.length ; i++){
                dados.expanded = false;
            }
            $scope.servicos = dados;
            configurarTable(dados);
        });
    };

    $scope.cancel = function () {
        $scope.cancelar = true;
    };

	$scope.inicio = function() {
        $scope.servicos = [];
        $scope.tipoServico(0);

        $scope.servico = {id: null,
                    inTipoServico: null,
                    nrCodServico: null,
                    nmServico: null,
                    vlCusto: null,
                    vlServicoAssociado: null,
                    vlServico: null,
                    validade: null,
                    validadeContratual: null,
                    nmPreparatorio: null,
                    boGratuito: false,
                    boBloqueio: false,
                    boValorAberto: false,
                    boTriagem: false,
                    boGeraRetornoAgendamento: false,
                    boSite: false,
                    boLaudo:false};
	};

	$scope.inicio();			
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
