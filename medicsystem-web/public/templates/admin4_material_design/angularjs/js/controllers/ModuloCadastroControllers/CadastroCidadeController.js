'use strict';

MedicsystemApp.controller('CadastroCidadeController', 
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout) {

    function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };
        
    function configurarTable( item ) {

        var initialParams = { count:5 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item.bairros
        };

        item.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  

    function configurarTablePrincipal( item ) {

        var initialParams = { count: 10 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  

    $scope.getCidades = function(){
        servicesFactory.cidades.query(function(dados){
            for ( var i = 0 ; i < dados.length ; i++){
                dados.expanded = false;
            }
            $scope.cidadeOptions = dados;

            configurarTablePrincipal($scope.cidadeOptions);
        });
        
    };

    $scope.getBairros = function(cidade){
        servicesFactory.cidadeBairros.get({ id: cidade.id },function(dados){
            if(dados.length > 0){
                $scope.showEmpty = false;
                cidade.bairros = {};
                cidade.bairros = dados;
                configurarTable(cidade);
            }
            else{
                $scope.showEmpty = true;
            }
        });    
    };

    $scope.novoBairro = function (){
        $scope.cidade = {nmBairro: null};
    };

    $scope.addBairro = function(cidade){
        if ($scope.cidade.nmBairro != null){
            $scope.cidade.nmBairro = $scope.cidade.nmBairro.toUpperCase();
            var objCidade = {id: cidade.id};
            var objBairro = {cidade: objCidade,
                            nmBairro: $scope.cidade.nmBairro};
            
            cadastroFactory.createBairro.save({},objBairro,
                function(result){
                    messageToastr("sucesso","Bairro cadastrado","SUCESSO");
                    
            });
        }
        
    }

    $scope.novaCidade = function (){
        $scope.cidade = {UF: null,
                        nmCidade: null};
    };

    $scope.addCidade = function(){
        if ($scope.cidade.UF != null && $scope.cidade.nmCidade != null){
            $scope.cidade.nmCidade = $scope.cidade.nmCidade.toUpperCase();

            var novaCidade = {nmUF: $scope.cidade.UF,
                            nmCidade: $scope.cidade.nmCidade};

            cadastroFactory.createCidade.save({},novaCidade,
                function(result){
                    messageToastr("sucesso","Cidade cadastrada","SUCESSO");
                    
            });
        }

    };

    $scope.returnBairros = function (cidade){
        $scope.showEdit = false;
        $scope.getBairros(cidade);
    };

    $scope.editarBairro = function(cidade,bairro){
        $scope.bairroFront = bairro;
        $scope.showEdit = true;
        $scope.cancelar = false;
        $scope.cidade = {};
        $scope.cidade = {nmBairro: bairro.nmBairro,
                        idBairro: bairro.id};
    
    };

    $scope.updateBairro = function (cidade){  
        if ($scope.cancelar == true){
            $scope.showEdit = false;
        }else{           
            if ($scope.cidade.nmBairro != null){
                $scope.cidade.nmBairro = $scope.cidade.nmBairro.toUpperCase();

                var objCidade = {id: cidade.id};
                var updateBairro = {nmBairro: $scope.cidade.nmBairro,
                                id: $scope.cidade.idBairro,
                                cidade: objCidade};

                cadastroFactory.createBairro.update({},updateBairro,
                    function(result){
                        messageToastr("sucesso","Bairro atualizado","SUCESSO")
                        $scope.returnBairros(cidade);
                });
            }
        }
    };  

    $scope.cancel = function () {
        $scope.cancelar = true;
        $scope.bairroFront.expanded = false;
	};

	$scope.inicio = function() {
      $scope.getCidades();
	};

	$scope.inicio();			
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
