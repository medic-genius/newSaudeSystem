'use strict';

MedicsystemApp.controller('CadastroFuncionarioController', 
	function($scope,$state,$stateParams,$element,NgTableParams,cadastroFactory,servicesFactory,toastr, $http, $timeout, Auth, $ngBootbox) {

     $scope.enable = ['administrador'];
     $scope.valideBotao = !$scope.checkEditRole($scope.enable); 


     $scope.grupoFunc = [
         { id: 0,    description: 'FUNCIONÁRIOS' },
         { id: 1,    description: 'MÉDICOS/DENTISTAS' },
         { id: 2,    description: 'PRESTADORES DE SERVIÇOS' },
         { id: 3,    description: 'VENDEDORES' },
         { id: 4,    description: 'ESTAGIÁRIOS' }
     ];

     $scope.sexo = [
         { id: 0,    description: 'MASCULINO' },
         { id: 1,    description: 'FEMININO' }
     ];

     $scope.estadoCivilOptions = [
            { id: 0,    description: 'Solteiro(a)' }, { id: 1,    description: 'Casado(a)' },
            { id: 2,    description: 'Viuvo(a)' }, { id: 3,    description: 'Desquitado(a)' },
            { id: 4,    description: 'Divorciado(a)' }, { id: 5,    description: 'Outros' },
            { id: 6,    description: 'Nao informado' },
        ];

    $scope.inativarFunc = function (func) {
        $ngBootbox.confirm('Deseja realmente inativar este funcionário?')
            .then(function() {
                cadastroFactory.inativarFuncionario.update({id: func.id}, function(dados){
                    messageToastr("sucesso","Funcionário inativado","SUCESSO");
                    $scope.getFuncionarios($scope.tipo);
                });
            });
    };

    $scope.ativarFunc = function (func) {
        $ngBootbox.confirm('Deseja realmente ativar este funcionário?')
            .then(function() {
                cadastroFactory.ativarFuncionario.update({id: func.id}, function(dados){
                        messageToastr("sucesso","Funcionário ativado","SUCESSO");
                        $scope.getFuncionarios($scope.tipo);
                });
            });
    };

    $scope.checkEditRole = function(allowed) {

		var userRoles = Auth.authz.resourceAccess[Auth.authz.clientId].roles;
    	
    	for ( role of userRoles ) {
    		if (allowed.indexOf(role) != -1) {
    			return false;
    		}
    	}
    	return true;
	};

    function messageToastr(type, message, title) {
        if( type == 'erro' ) 
           toastr.error(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'sucesso' )
           toastr.success(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'informacao' )
           toastr.info(  message, title,{tapToDismiss:true,timeOut:5000}); 
        else if( type == 'aviso' )
           toastr.warning(  message, title,{tapToDismiss:true,timeOut:5000}); 
    };

    function configurarTable( item ) {

        var initialParams = { count: 10 };
        

        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts:[], 
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 10,
            paginationMinBlocks: 2,
            dataset: item
        };

        $scope.tableParams = new NgTableParams(initialParams, initialSettings )  
    };  

    $scope.returnFunc = function (){
        $scope.getFuncionarios($scope.tipo);
    };

    $scope.novoFunc = function (){
        $scope.funcionario = {};
        servicesFactory.unidades.query(function( resultUnidades ){
            $scope.unidadeOptions = resultUnidades;
        });
        $scope.cidadeOptions = servicesFactory.cidades.query();
    };

    $scope.getBairros = function (idCidade){
        servicesFactory.cidadeBairros.get({ id: idCidade },function(dados){
            $scope.bairrosOptions = dados;
        }); 
    };

    $scope.addFunc = function(){
        if ($scope.funcionario.dtNascimento != null && $scope.funcionario.inSexo != null && $scope.funcionario.inGrupoFuncionario != null && $scope.funcionario.inEstadoCivil != null 
            && $scope.funcionario.unidade != null && $scope.funcionario.cidade != null && $scope.funcionario.bairro != null && $scope.funcionario.dtContrato != null){

            $scope.funcionario.nmFuncionario = $scope.funcionario.nmFuncionario.toUpperCase();
            $scope.funcionario.nmLogradouro = $scope.funcionario.nmLogradouro.toUpperCase();
            
            if ($scope.funcionario.nmCargo != null)
                $scope.funcionario.nmCargo = $scope.funcionario.nmCargo.toUpperCase();
            
            if ($scope.funcionario.nmComplemento != null)
                $scope.funcionario.nmComplemento = $scope.funcionario.nmComplemento.toUpperCase();

            var objUnid = {id: $scope.funcionario.unidade};
            var objCidade = {id: $scope.funcionario.cidade};
            var objBairro = {id: $scope.funcionario.bairro};

            $scope.funcionario.unidade = objUnid;
            $scope.funcionario.cidade = objCidade;
            $scope.funcionario.bairro = objBairro;
            $scope.funcionario.vlTransporte = 0;
            
            cadastroFactory.createFuncionario.save({}, $scope.funcionario, function(dados){
                messageToastr("sucesso","Funcionário cadastrado","SUCESSO");
            })
        }else{
            messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
        }
    };
    

    $scope.editarFunc = function(funcionario){
        console.log("funcionario:",funcionario);
        $scope.funcionario = {};
        $scope.cancelar = false;
        
        servicesFactory.unidades.query(function( resultUnidades ){
            $scope.unidadeOptions = resultUnidades;
        });
        $scope.cidadeOptions = servicesFactory.cidades.query();

        servicesFactory.cidadeBairros.get({ id: funcionario.cidade.id },function(dados){
            $scope.bairrosOptions = dados;
        }); 

        $scope.funcionario = {
            id: funcionario.id,
            nrMatricula: funcionario.nrMatricula,
            nmFuncionario: funcionario.nmFuncionario,
            dtNascimento: funcionario.dtNascimento,
            inGrupoFuncionario: funcionario.inGrupoFuncionario,
            nrRg: funcionario.nrRg,
            nrCpf: funcionario.nrCpf,
            inSexo: funcionario.inSexo,
            inEstadoCivil: funcionario.inEstadoCivil,
            unidade: funcionario.unidade,
            nrTelefone: funcionario.nrTelefone,
            nrCelular: funcionario.nrCelular,
            nmLogradouro: funcionario.nmLogradouro,
            nrNumero: funcionario.nrNumero,
            nmComplemento: funcionario.nmPontoReferencia,
            nrCep: funcionario.nrCep,
            cidade: funcionario.cidade,
            bairro: funcionario.bairro,
            nmCargo: funcionario.nmCargo,
            dtContrato: funcionario.dtContrato,
            vlSalario: funcionario.vlSalario,
            vlTransporte: 0,
            
            dtCadastro: funcionario.dtCadastro,
            idOperadorCadastro: funcionario.idOperadorCadastro,
            dtInclusaoLog: funcionario.dtInclusaoLog 
                        
        }; 
    };

    $scope.atualizarFuncionario = function (func){
        console.log("funcionarioupdate:",func);
        if ($scope.cancelar == true){
            func.expanded = false;
        }else{
            if ($scope.funcionario.dtNascimento != null && $scope.funcionario.inSexo != null && $scope.funcionario.inGrupoFuncionario != null &&
                $scope.funcionario.inEstadoCivil != null && $scope.funcionario.unidade != null && 
                $scope.funcionario.cidade != null && $scope.funcionario.bairro != null && $scope.funcionario.dtContrato != null){
                    $scope.funcionario.nmFuncionario = $scope.funcionario.nmFuncionario.toUpperCase();
                    $scope.funcionario.nmLogradouro = $scope.funcionario.nmLogradouro.toUpperCase();

                    if ($scope.funcionario.nmCargo != null)
                        $scope.funcionario.nmCargo = $scope.funcionario.nmCargo.toUpperCase();

                    if ($scope.funcionario.nmComplemento != null)
                        $scope.funcionario.nmComplemento = $scope.funcionario.nmComplemento.toUpperCase();

                    $scope.funcionario.nrCpf = $scope.funcionario.nrCpf.replace(/[^0-9]+/g,'');
                    cadastroFactory.createFuncionario.update({}, $scope.funcionario, function(dados){
                    messageToastr("sucesso","Funcionário atualizado","SUCESSO");
                    func.expanded = false;
                    $scope.getFuncionarios($scope.tipo);
                });
            }else{
                messageToastr("erro","Verifique se todos os campos estão preenchidos","ERRO")
            }
        }
    };

    $scope.cancel = function () {
        $scope.cancelar = true;
    };

    // 0 = ATIVO    1 = INATIVO
    $scope.getFuncionarios = function(tipo){
        $scope.tipo = tipo;
        if (tipo == 0){
            servicesFactory.funcionariosAtivos.query(function(dados){
                for ( var i = 0 ; i < dados.length ; i++){
                    dados.expanded = false;
                }
                $scope.funcionariosAtivos = dados;
                configurarTable($scope.funcionariosAtivos);
            });
        }else {
            servicesFactory.funcionariosInativos.query(function(dados){
                for ( var i = 0 ; i < dados.length ; i++){
                    dados.expanded = false;
                }
                $scope.funcionariosInativos = dados;
                configurarTable($scope.funcionariosInativos);
            });
        }
    }

	$scope.inicio = function() {
        $scope.getFuncionarios(0);


	};

	$scope.inicio();			
});

    MedicsystemApp.config(function(toastrConfig) {
      angular.extend(toastrConfig, {
          closeButton: true,
              debug: true,
              newestOnTop: true,
              progressBar: false,
              positionClass: 'toast-top-center',
              preventDuplicates: false,
              showDuration: 300,
              hideDuration: 1000,
              timeOut: 2500,
              extendedTimeOut: 1000,
              showEasing: 'swing',
              hideEasing: 'linear',
              showMethod: 'fadeIn',
              hideMethod: 'fadeOut',
      });
    });
