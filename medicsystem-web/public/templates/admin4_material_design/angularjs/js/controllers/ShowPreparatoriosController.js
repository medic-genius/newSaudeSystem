'use strict';

MedicsystemApp.controller(
    'ShowPreparatoriosController',
    function($scope, $modal, lServicos, $modalInstance)
    {
        $scope.lServicos = lServicos;

        $scope.existPreparatorio = false;
        for (var i = 0; i < lServicos.length; i++)
        {
            if(null != lServicos[i].nmPreparatorio && "" != lServicos[i].nmPreparatorio)
            {
                $scope.existPreparatorio = true;
            }
        }

        $scope.cancel = function ()
        {
            $modalInstance.dismiss('cancel');
        };

        $scope.print = function()
        {
            window.print();
        }
    }
);
