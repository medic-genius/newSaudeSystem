'use strict';

MedicsystemApp.controller('FolhaDashboardGrupoFinanceiroController', function($scope,$stateParams,servicesFactory) {    
  
    var loadGraficoTotalUltimosMeses = function( inTipoGrupo ) {
        var datax = new Date();
        var data =  $scope.removerMesData( datax ,0 ); 
        var color = null;

        if( inTipoGrupo == 0) color = '#135388';
        else if(  inTipoGrupo == 1) color = '#F50057';
        else if(  inTipoGrupo == 2) color = '#00BFB2';
        else if(  inTipoGrupo == 3) color = '#00A8C3';
        else color = '#768C7C';
        
        $scope.folhaUltimoMeses = servicesFactory.folhaGrupoFinanceiroGraficoMeses.gerar( {idGrupoFinanceiro:inTipoGrupo ,data: data}, function( result ){
            var chart = c3.generate({
                data: { type: 'bar', json: result, keys: { x: 'mes', value: ['folha'] },colors:{  folha: color }},
                axis: { x: { type: 'category'}, y : { tick: { format: d3.format("$,.2f") } }},
                bar: { width: { ratio: 0.5 }}
            }); 

            /* d3.selectAll('.c3-axis-x .tick tspan').each(function(d,i){
                var self = d3.select(this);
                var date = self.text();
                var dateFormatada =  formatarDataGraficoUrl(  date  ); //tipoGrupoFinanceiro
                var link = "<a href='#/financeiro/contaPagar/inicio> </a>";
                self.html(link);  
            }) */
        });
    };

    $scope.removerMesData = function( data, qtdMesRemover ) {
        var dataAlterar = data;
        dataAlterar = new Date( dataAlterar.getFullYear(),eval( dataAlterar.getMonth() - qtdMesRemover  ), 1 ); 
        var mes = (dataAlterar.getMonth() + 1) > 9 ? (dataAlterar.getMonth() + 1) : ('0'+ (dataAlterar.getMonth() + 1));
        var ano = dataAlterar.getFullYear();
        
        return  (mes + '/' + ano);
    };

    function loadLabelGrupoFinanceiro() {
        var idGrupoFinanceiro = $stateParams.idGrupoFinanceiro; 

        if( idGrupoFinanceiro == 0 )
            $scope.nmGrupoFinanciero = 'GRUPO MEDIC LAB'; 
        else if ( idGrupoFinanceiro == 1 )
            $scope.nmGrupoFinanciero = 'GRUPO DENTAL SAÚDE';
        else if ( idGrupoFinanceiro == 2 )
            $scope.nmGrupoFinanciero = 'GRUPO SAÚDE PERFORMANCE';
        else if ( idGrupoFinanceiro == 3 )
            $scope.nmGrupoFinanciero = 'GRUPO DR. CONSULTA';
        else if ( idGrupoFinanceiro == 4 )
            $scope.nmGrupoFinanciero = 'DROGA MED';
        else {
            $scope.nmGrupoFinanciero = 'ESCOLHA UM GRUPO FINANCEIRO NO MENU';
        }
    }

    function inicio() {
        loadLabelGrupoFinanceiro();
        loadGraficoTotalUltimosMeses($stateParams.idGrupoFinanceiro);
    }

    inicio();
        
});