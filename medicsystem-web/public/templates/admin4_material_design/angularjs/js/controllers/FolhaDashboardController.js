'use strict';

MedicsystemApp.controller('FolhaDashboardController', function($scope,$stateParams,servicesFactory) {    
        
    var formatarDataGraficoUrl = function( dataFormatar) {
        dataFormatar = dataFormatar.split("/");
        var mes = dataFormatar[0];
        var ano = dataFormatar[1];

        return ( mes + '-' + ano);
    };

    function removerMesData( data, qtdMesRemover ) {  
      var dataAlterar = data;
      dataAlterar = new Date( dataAlterar.getFullYear(),eval( dataAlterar.getMonth() - qtdMesRemover  ), 1 ); 
      var mes = (dataAlterar.getMonth() + 1) > 9 ? (dataAlterar.getMonth() + 1) : ('0'+ (dataAlterar.getMonth() + 1));
      var ano = dataAlterar.getFullYear();
      
      return  (mes + '/' + ano);
    
    };  

    function getFolhaPagamentoUltimoDoisMeses( idEmpresaFinanceiroSelecionada ) {
      var dataMesAtual =  removerMesData( new Date(), 0);   
      var dataMesAnterior =  removerMesData( new Date(), 1);   

      $scope.folhaPagamentoMesAtual = servicesFactory.folhaPagamentoImportarPorMes.get( {idempresafinanceiro: idEmpresaFinanceiroSelecionada,data: dataMesAtual}, function(){});
      $scope.folhaPagamentoMesAnterior = servicesFactory.folhaPagamentoImportarPorMes.get( {idempresafinanceiro: idEmpresaFinanceiroSelecionada,data: dataMesAnterior}, function(){});
      
    };

    function graficoFolhaUltimosDozeMeses( idEmpresaFinanceiroSelecionada, inTipoGrupo ) {
        var datax = new Date();
        var data = removerMesData( datax ,0 ); 
        var color = null;

        if( inTipoGrupo == 0) color = '#135388';
        else if(  inTipoGrupo == 1) color = '#F50057';
        else if(  inTipoGrupo == 2) color = '#00BFB2';
        else if(  inTipoGrupo == 3) color = '#00A8C3';
        else color = '#768C7C';
          
        $scope.folhaUltimoMeses = servicesFactory.folhaPagamentoGraficoMeses.gerar( {idempresafinanceiro:idEmpresaFinanceiroSelecionada ,data: data}, 
              function( result ){
              
                  var chart = c3.generate({
                  data: {type: 'bar',json: result,keys: {x: 'mes',value: ['folha']},colors:{  folha: color } },
                  axis: { x: {type: 'category'}, y : { tick: {format: d3.format("$,.2f")}}},
                  bar: {width: {ratio: 0.5}}
              }); 

                  d3.selectAll('.c3-axis-x .tick tspan')
                    .each(function(d,i){
                         var self = d3.select(this);
                         var date = self.text();
                         var dateFormatada =  formatarDataGraficoUrl(  date  ); //tipoGrupoFinanceiro
                         var link = "<a href='#/folha-visualizacao/"+idEmpresaFinanceiroSelecionada+"/"+inTipoGrupo+"/"+dateFormatada+".html'>"+date+"</a>";
                         self.html(link);    
                  });
        });
    };

    function getEmpresaFinanceiroSelecionado( idEmpresaFinanceiroSelecionada ) {
      servicesFactory.empresasFinanceiro.get({id: idEmpresaFinanceiroSelecionada},function( result ){
        $scope.empresaFinanceiroSelecionada = result;
        getFolhaPagamentoUltimoDoisMeses(  result.idEmpresaFinanceiro);
        graficoFolhaUltimosDozeMeses(  result.idEmpresaFinanceiro, result.inTipoGrupo );
      });
    };

  $scope.$on('$viewContentLoaded', function() {
        Metronic.initAjax();
        getEmpresaFinanceiroSelecionado( $stateParams.idEmpresaFinanceiro );
  });
  
  
   
});