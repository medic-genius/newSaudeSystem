/*
  @author : Joelton Matos
  @since : 01/05/2016
  @version : 1.0 

*/

'use strict';

MedicsystemApp.controller('VisualizarPdfController', function($scope,$state,$stateParams,$http,$window) {
  
  $scope.getNavStyle = function(scroll) {
    if(scroll > 100) return 'pdf-controls fixed';
    else return 'pdf-controls';
  }

  $scope.onError = function(error) {
    console.log(error);
  }

  $scope.onLoad = function() {
    $scope.loading = '';
  }

  $scope.onProgress = function(progress) {
    //console.log(progress);
  }

  var inicio = function( idFuncionario, nmFuncionario, nomeArquivo, url ) {
    $scope.idFuncionario = idFuncionario;
    $scope.nmFuncionario = nmFuncionario;
    $scope.url = url;
    $scope.pdfName = nomeArquivo;

    $scope.url = $scope.url.replace("\\","/");
    $scope.pdfUrl = 'http://45.33.93.46/imagens/funcionarios/' + $scope.url;
    $scope.scroll = 0;
    $scope.loading = 'Carregando...';
  
  };

  if( $state.is( 'funcionario-visualizarArquivo' ) ) {
    if( $stateParams.idFuncionario )
        inicio( $stateParams.idFuncionario , $stateParams.nmFuncionario, $stateParams.nmArquivo ,$stateParams.linkArquivo );      
  };

});