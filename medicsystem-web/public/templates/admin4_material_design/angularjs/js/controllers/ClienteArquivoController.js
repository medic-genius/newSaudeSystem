'use strict';

MedicsystemApp.controller('ClienteArquivoController', ['$rootScope', '$scope', '$http', '$timeout', '$state', '$location','servicesFactory', function($rootScope, $scope, $http, $timeout, $state, $location, servicesFactory ) {

    $scope.refreshArquivos = function() {
      $scope.employeeImages = [];
      var i;
    	if ($scope.selectedCliente) {
    		$scope.arquivos = servicesFactory.clienteArquivos.get({id: $scope.selectedCliente.id },function(){

            if( $scope.arquivos ) {
                for( i = 0; i < $scope.arquivos.length; i++ ) {            
                  $scope.employeeImages
                    .push({ thumb: "http://images.elev-sistema.com.br:8083/" + $scope.arquivos[i].urlImagem, img : "http://images.elev-sistema.com.br:8083/" + $scope.arquivos[i].urlImagem, description: $scope.arquivos[i].nmImagemCliente });
                
                  }
          }
        });
    	}
    }


    $scope.refreshArquivos();
    
}]);


