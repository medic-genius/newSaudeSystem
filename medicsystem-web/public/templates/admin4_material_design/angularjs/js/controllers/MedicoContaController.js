'use strict';

MedicsystemApp.controller('MedicoContaController', function($rootScope, $scope, $http, $timeout, $state, $stateParams, $modal, servicesFactory, Auth ) {
	var Medico = servicesFactory.medicos;

	$scope.rwRoles = ['administrador','gerenteadm', 'cadastro', 'gerenteatendimentocliente', 'rh', 'atendimentocliente'];

    $scope.changeTab = function($event) {
    	console.log($event);
    }

    $scope.today = new Date();

    if($state.is('atendimentoMedico.medico-conta')){
    	$scope.selectedMedico= new Medico($scope.selectedMedico);
    }
});