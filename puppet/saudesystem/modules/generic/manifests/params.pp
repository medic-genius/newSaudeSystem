#System params
class generic::params {

  if $::vagrant { #dev vagrant

    $medicsystem_pg_url = 'jdbc:postgresql://192.169.0.96/dbRealVida'
    $medicsystem_pg_usr = 'postgres'
    $medicsystem_pg_pwd = 'postgres'
    
    $dentalsystem_pg_url = 'jdbc:postgresql://45.79.195.150/dbRealVida'
    $dentalsystem_pg_usr = 'postgres'
    $dentalsystem_pg_pwd = 'medicsaude.2015'

    $dashboard_web_appserver = 'http://10.11.12.44:8080'
    $medicsystem_web_appserver = 'http://10.11.12.43:8080'
    $sendsms_flag = 'false'
  
  } else { #production vagrant
    
    $medicsystem_pg_url = 'jdbc:postgresql://192.169.0.3/dbRealVida'
    $medicsystem_pg_usr = 'postgres'
    $medicsystem_pg_pwd = 'postgres'
    
    $dentalsystem_pg_url = 'jdbc:postgresql://45.79.195.150/dbRealVida'
    $dentalsystem_pg_usr = 'postgres'
    $dentalsystem_pg_pwd = 'medicsaude.2015'

    $dashboard_web_appserver = 'http://192.169.1.10/dashboard-api'
    $medicsystem_web_appserver = 'http://192.169.1.10/medicsystem-api' #TODO change this
    $sendsms_flag = 'true'
  }

  $vagrant_dir = '/vagrant'
  $log_dir = '/var/log'
  $opt_dir = '/opt'
  $sysconf_dir = '/etc/sysconfig'
  $init_dir = '/etc/init'

}