#define centos generic profile
class generic {

  $install = ['binutils.x86_64','wget', 'dos2unix', 'unzip' ]

  package { $install:
    ensure  => present,
  }

  class { '::ntp':
    servers => [
      '0.br.pool.ntp.org',
      '1.br.pool.ntp.org',
      '2.br.pool.ntp.org',
      '3.br.pool.ntp.org',
    ],
  }

  exec { 'rm utc time':
    command => '/bin/rm /etc/localtime',
    cwd     => '/etc',
    require => Class['ntp'],
  }

  exec { 'change to brazil time':
    command => '/bin/ln -s /usr/share/zoneinfo/Brazil/East /etc/localtime',
    cwd     => '/etc',
    require => Exec['rm utc time'],
  }

}