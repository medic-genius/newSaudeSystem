# This wrapper is needed because of http://projects.puppetlabs.com/issues/11989
define generic::upstartservice($ensure) {
  service {$name:
    ensure     => $ensure,
    hasstatus  => true,
    hasrestart => true,
    start      => "/sbin/initctl start ${name}",
    stop       => "/sbin/initctl stop ${name}",
    status     => "/sbin/initctl status ${name} | grep '^${name} start/running'",
    restart    => "/sbin/initctl restart ${name}",
  }
}
