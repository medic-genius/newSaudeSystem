#define medicsystem platform profile
class medicsystem_platform {

  $msp_origin_dir = "${::generic::params::vagrant_dir}/medicsystem-platform"
  $wildfly_dir = "${::generic::params::opt_dir}/wildfly"

  class { 'java':
    distribution => 'jdk',
    package      => 'java-1.8.0-openjdk-devel',
  }

  archive { '/tmp/apache-maven-3.3.3-bin.zip':
    source       => 'http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.zip',
    extract      => true,
    extract_path => '/tmp',
    require      => Class['java'],
  }

  class { 'wildfly':
    version          => '8.2.1.Final',
    java_home        => '/usr/lib/jvm/jre-1.8.0-openjdk',
    install_source   => 'http://download.jboss.org/wildfly/8.2.1.Final/wildfly-8.2.1.Final.tar.gz',
    dirname          => $wildfly_dir,
    config           => 'standalone.xml',
    java_opts         => '-Xms512m -Xmx1024m -XX:MaxPermSize=512m',
  }

  wildfly::config::module { 'org.postgresql':
    source       => 'https://jdbc.postgresql.org/download/postgresql-9.4-1201.jdbc4.jar',
    dependencies => ['javax.api', 'javax.transaction.api']
  }

  wildfly::datasources::driver { 'Driver postgresql':
    driver_name                     => 'postgresql',
    driver_module_name              => 'org.postgresql',
    driver_xa_datasource_class_name => 'org.postgresql.xa.PGXADataSource'
  }
  ->
  wildfly::datasources::datasource { 'medicsystemDS':
    config => {
      'driver-name'       => 'postgresql',
      'connection-url'    => $::generic::params::medicsystem_pg_url,
      'jndi-name'         => 'java:jboss/datasources/medicsystemDS',
      'user-name'         => $::generic::params::medicsystem_pg_usr,
      'password'          => $::generic::params::medicsystem_pg_pwd,
      'use-java-context'  => 'true',
      'pool-prefill'      => 'true',
      'min-pool-size'     => '5',
      'initial-pool-size' => '5',
      'max-pool-size'     => '100',
      'flush-strategy'    => 'FailingConnectionOnly',
      'use-ccm'           => 'true',
      'check-valid-connection-sql' => 'select 1',
      'validate-on-match' => 'true',
      'background-validation' => 'true',
      'background-validation-millis' => '10000',
    }
  }

  exec { 'maven install ireport':
    command   => '/tmp/apache-maven-3.3.3/bin/mvn install:install-file -Dfile=iReport.jar -DgroupId=com.ireport -DartifactId=ireport -Dversion=1.0 -Dpackaging=jar',
    cwd       => '/vagrant/puppet/saudesystem/modules/medicsystem_platform/files',
    logoutput => true,
    timeout   => 2400,
    require   => [
      Archive['/tmp/apache-maven-3.3.3-bin.zip'],
    ],
  }

  exec { 'medicsystem maven install':
    command   => '/tmp/apache-maven-3.3.3/bin/mvn -e clean install',
    cwd       => $msp_origin_dir,
    logoutput => true,
    timeout   => 2400,
    require   => [
      Class['wildfly'], 
      Archive['/tmp/apache-maven-3.3.3-bin.zip'],
      Exec['maven install ireport'],
    ],
  }

  exec { 'deploy medicsystem-platform':
    command => "/bin/cp medicsystem.ear ${wildfly_dir}/standalone/deployments/",
    cwd     => "${msp_origin_dir}/medicsystem-ear/target/",
    require => Exec['medicsystem maven install'],
  }

  firewall { '100 allow http and https access':
    port   => [8080, 443],
    proto  => tcp,
    action => accept,
  }

  file_line { 'append custom env vars':
    path    => '/etc/default/wildfly.conf',
    line    => "export SEND_SMS=${::generic::params::sendsms_flag}", #more vars to come?!
    require => Class['wildfly'],
    notify  => Service['wildfly'],
  }

  #config keycloack adapter
  archive { '/tmp/keycloak-wf8-adapter-dist-1.4.0.Final.zip':
    source       => 'http://downloads.jboss.org/keycloak/1.4.0.Final/adapters/keycloak-wf8-adapter-dist-1.4.0.Final.zip',
    extract      => true,
    extract_path => $wildfly_dir,
    require      => Class['wildfly'],
  }
  
  wildfly::util::exec_cli { 'Reload if necessary':
    command => 'reload',
    onlyif  => '(result == reload-required) of read-attribute server-state'
  }

  exec { 'keycloak adapter-install':
    command   => "${wildfly_dir}/bin/jboss-cli.sh -c --file=adapter-install.cli --user=wildfly --password=wildfly",
    cwd       => "${wildfly_dir}/bin",
    logoutput => true,
    timeout   => 1200,
    require      => [ 
      wildfly::util::exec_cli['Reload if necessary'],
      Archive['/tmp/keycloak-wf8-adapter-dist-1.4.0.Final.zip'],
    ],
    onlyif    => "/bin/sh ${wildfly_dir}/bin/jboss-cli.sh -c --command=\'ls extension\' --user=wildfly --password=wildfly | /bin/grep keycloak"

  }

}