#define dashboard web profile
class dashboard_web {

  $user_group = 'dashboard'

  $dashboard_dir = "${::generic::params::opt_dir}/dashboard"
  $dw_dir = "${dashboard_dir}/dashboard-web"

  $dw_origin_dir = "${::generic::params::vagrant_dir}/dashboard-web"
  $nl_origin_dir = "${::generic::params::vagrant_dir}/node-logger"
  $dw_log_dir = "${::generic::params::log_dir}/dashboard-web"

  $dw_log_file = "${dw_log_dir}/console.log"
  $dw_sysconfig_file = "${::generic::params::sysconf_dir}/dashboard-web"
  $dw_upstart_file = "${::generic::params::init_dir}/dashboard-web.conf"

  group {$user_group:
    ensure => present,
    system => true,
  }

  user {$user_group:
    ensure     => present,
    gid        => $user_group,
    groups     => [$user_group],
    membership => minimum,
    shell      => '/bin/bash',
    require    => Group[$user_group],
  }

  firewall { '100 allow http access':
    port   => 9000,
    proto  => tcp,
    action => accept,
  }

  package {'epel-release':
    ensure   => 'installed',
    provider => 'rpm',
    source   => 'http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm',
  }

  package { ['nodejs','npm']:
    ensure  => 'installed',
    require => Package['epel-release'],
  }

  file { [$dashboard_dir, $dw_log_dir]:
    ensure => 'directory',
    mode   => '0664',
    group  => $user_group,
    owner  => $user_group,
  }

  exec { 'clean before deploy': #provision only
    command => "/bin/rm -rf ${dashboard_dir}/*",
    cwd     => $::generic::params::vagrant_dir,
    require => File[$dashboard_dir],
  }

  exec { 'deploy dashboard-web':
    command => "/bin/cp -rf ${dw_origin_dir} ${dashboard_dir}/",
    cwd     => $::generic::params::vagrant_dir,
    require => Exec['clean before deploy'],
    notify  => Service['dashboard-web'],
  }

  exec { 'deploy node-logger':
    command => "/bin/cp -rf ${nl_origin_dir} ${dashboard_dir}/",
    cwd     => $::generic::params::vagrant_dir,
    require => Exec['clean before deploy'],
  }

  file {'dashboard-web sysconfig':
    ensure  => file,
    path    => $dw_sysconfig_file,
    content => template('dashboard_web/dashboard-web.sysconfig.erb'),
    notify  => Service['dashboard-web'],
  }

  file {'dashboard-web upstart':
    ensure  => file,
    path    => $dw_upstart_file,
    content => template('dashboard_web/dashboard-web.upstart.erb')
  }

  exec {'dashboard-web npm install':
    environment => ['HOME=/home/dashboard'],
    cwd         => $dw_dir,
    command     => '/usr/bin/npm install --unsafe-perm',
    timeout     => 600,
    require     => [ Package['npm'], Exec['deploy dashboard-web'], Exec['deploy node-logger'] ],
  }

  exec {'fix dashboard permissions':
    cwd     => $dashboard_dir,
    command => "/bin/chown -R ${user_group}:${user_group} *",
    require => Exec['dashboard-web npm install'],
    notify  => Service['dashboard-web'],
  }

  # exec {'fix dashboard sysconfig encoding':
  #   cwd     => $::generic::params::sysconf_dir,
  #   command => "/usr/bin/dos2unix ${dw_sysconfig_file}",
  #   require => Exec['fix dashboard permissions'],
  # }

  logrotate::rule {'dashboard-web':
    path         => $dw_log_file,
    rotate       => 5,
    rotate_every => 'day',
    copytruncate => true,
    missingok    => true,
  }

  generic::upstartservice {'dashboard-web':
    ensure  => running,
  }

}