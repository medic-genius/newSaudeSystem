#define keycloak profile
class keycloak {

  $user_group = 'keycloak'
  $unzip_dir = "${::keycloak::params::opt_dir}/keycloak"
  $install_dir = "${unzip_dir}/keycloak-1.4.0.Final"

  user { $pg_user:
    ensure => present,
  }

  group {$user_group:
    ensure => present,
    system => true,
  }

  user {$user_group:
    ensure     => present,
    gid        => $user_group,
    groups     => [$user_group],
    membership => minimum,
    shell      => '/bin/bash',
    require    => Group[$user_group],
  }

  file { '/opt/keycloak':
    ensure => 'directory',
    mode   => '0664',
    group  => $user_group,
    owner  => $user_group,
  }

  firewall { '100 allow http access':
    port   => 8080,
    proto  => tcp,
    action => accept,
  }
  
  include postgresql::server

  postgresql::server::db { $::keycloak::params::pg_db:
    user     => $::keycloak::params::pg_user,
    password => $::keycloak::params::pg_passwd,
  }

  class { 'java':
    distribution => 'jdk',
    package      => 'java-1.8.0-openjdk-devel',
  }

  archive { 'keycloak-1.4.0.Final':
    ensure      => present,
    url         => 'http://downloads.jboss.org/keycloak/1.4.0.Final/keycloak-1.4.0.Final.zip',
    target      => $unzip_dir,
    src_target  => $::keycloak::params::tmp_dir,
    extension   => 'zip',
    checksum    => false,
    user        => $user_group,
    timeout     => 2400,
    require     => Class['java'],
  }

  exec { 'copy postgresql driver':
    command => "/bin/cp -rf postgresql ${install_dir}/modules/system/layers/base/org/",
    cwd     => '/vagrant/puppet/saudesystem/modules/keycloak/files',
    require => Archive['keycloak-1.4.0.Final'],
  }

  exec { 'grant postgresql driver':
    command => "/bin/chown -R ${user_group}:${user_group} postgresql",
    cwd     => "${install_dir}/modules/system/layers/base/org",
    require => Exec['copy postgresql driver'],
  }

  file { "${install_dir}/standalone/configuration/standalone.xml":
    mode    => '0664',
    owner   => $user_group,
    group   => $user_group,
    source  => "puppet:///modules/keycloak/standalone.xml",
    require => Archive['keycloak-1.4.0.Final'],
    notify  => Service['keycloak'],
  }

  file { "${install_dir}/standalone/configuration/keycloak.jks":
    mode    => '0664',
    owner   => $user_group,
    group   => $user_group,
    source  => "puppet:///modules/keycloak/keycloak.jks",
    require => Archive['keycloak-1.4.0.Final'],
  }

  exec { 'cleanup postgresql driver files':
    command => "/usr/bin/dos2unix module.xml",
    cwd     => "/vagrant/puppet/saudesystem/modules/keycloak/files/postgresql/main",
    require => Archive['keycloak-1.4.0.Final'],
  }

  exec { 'cleanup keycloak config files':
    command => "/usr/bin/dos2unix keycloak.conf keycloak.start",
    cwd     => "/vagrant/puppet/saudesystem/modules/keycloak/files",
    require => Archive['keycloak-1.4.0.Final'],
  }

  file { '/etc/default/keycloak.conf':
    mode    => '0664',
    owner   => $user_group,
    group   => $user_group,
    source  => "puppet:///modules/keycloak/keycloak.conf",
    notify  => Service['keycloak'],
    require => Archive['keycloak-1.4.0.Final'],
  }

  file { '/etc/init.d/keycloak':
    mode    => '0755',
    owner   => $user_group,
    group   => $user_group,
    source  => "puppet:///modules/keycloak/keycloak.start",
    notify  => Service['keycloak'],
    require => Archive['keycloak-1.4.0.Final'],
  }

  service { 'keycloak':
    ensure  => 'running',
    require => File['/etc/init.d/keycloak'],
  }

}
