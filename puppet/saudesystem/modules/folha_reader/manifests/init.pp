#define folha-reader profile
class folha_reader {

  $user_group = 'medicsystem'

  $fr_dir = "${::generic::params::opt_dir}/folha-reader"

  $mw_origin_dir = "${::generic::params::vagrant_dir}/folha-reader"

  $mw_upstart_file = "${::generic::params::init_dir}/folha-reader.conf"

  firewall { '100 allow http 5000 access':
    port   => 5000,
    proto  => tcp,
    action => accept,
  }

  package { ['python','python-pip']:
    ensure  => 'installed',
  }

  file { [$fr_dir]:
    ensure => 'directory',
    mode   => '0664',
    group  => $user_group,
    owner  => $user_group,
  }

  exec {'folha-reader pip packages install':
    environment => ['HOME=/home/medicsystem'],
    cwd         => $fr_dir,
    command     => '/usr/bin/pip install pyparsing Flask',
    timeout     => 600,
    require     => [ Package['python', 'python-pip'] ],
  }

  exec { 'clean before deploy folha': #provision only
    command => "/bin/rm -rf ${fr_dir}/*",
    cwd     => $::generic::params::vagrant_dir,
    require => File[$fr_dir],
  }

  exec { 'deploy folha-reader':
    command => "/bin/cp -rf ${mw_origin_dir}/* ${fr_dir}/",
    cwd     => $::generic::params::vagrant_dir,
    require => Exec['clean before deploy'],
    notify  => Service['folha-reader'],
  }

  file {'folha-reader upstart':
    ensure  => file,
    path    => $mw_upstart_file,
    content => template('folha_reader/folha-reader.upstart.erb')
  }

  exec {'fix folhareader permissions':
    cwd     => $fr_dir,
    command => "/bin/chown -R ${user_group}:${user_group} *",
    require => Exec['deploy folha-reader'],
    notify  => Service['folha-reader'],
  }

  generic::upstartservice {'folha-reader':
    ensure  => running,
  }

}