#define medicsystem web profile
class medicsystem_web {

  $user_group = 'medicsystem'

  $medicsystem_dir = "${::generic::params::opt_dir}/medicsystem"
  $mw_dir = "${medicsystem_dir}/medicsystem-web"

  $mw_origin_dir = "${::generic::params::vagrant_dir}/medicsystem-web"
  $nl_origin_dir = "${::generic::params::vagrant_dir}/node-logger"
  $mw_log_dir = "${::generic::params::log_dir}/medicsystem-web"

  $mw_log_file = "${mw_log_dir}/console.log"
  $mw_forever_log_file = "${mw_log_dir}/forever.log"
  $mw_sysconfig_file = "${::generic::params::sysconf_dir}/medicsystem-web"
  $mw_upstart_file = "${::generic::params::init_dir}/medicsystem-web.conf"

  group {$user_group:
    ensure => present,
    system => true,
  }

  user {$user_group:
    ensure     => present,
    gid        => $user_group,
    groups     => [$user_group],
    membership => minimum,
    shell      => '/bin/bash',
    require    => Group[$user_group],
  }

  firewall { '100 allow http access':
    port   => 9000,
    proto  => tcp,
    action => accept,
  }

  package {'epel-release':
    ensure   => 'installed',
    provider => 'rpm',
    source   => 'http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm',
  }

  package { ['nodejs','npm']:
    ensure  => 'installed',
    require => Package['epel-release'],
  }

  file { [$medicsystem_dir, $mw_log_dir]:
    ensure => 'directory',
    mode   => '0664',
    group  => $user_group,
    owner  => $user_group,
  }

  exec { 'clean before deploy': #provision only
    command => "/bin/rm -rf ${medicsystem_dir}/*",
    cwd     => $::generic::params::vagrant_dir,
    require => File[$medicsystem_dir],
  }

  exec { 'deploy medicsystem-web':
    command => "/bin/cp -rf ${mw_origin_dir} ${medicsystem_dir}/",
    cwd     => $::generic::params::vagrant_dir,
    require => Exec['clean before deploy'],
    notify  => Service['medicsystem-web'],
  }

  exec { 'deploy node-logger':
    command => "/bin/cp -rf ${nl_origin_dir} ${medicsystem_dir}/",
    cwd     => $::generic::params::vagrant_dir,
    require => Exec['clean before deploy'],
  }

  file {'medicsystem-web sysconfig':
    ensure  => file,
    path    => $mw_sysconfig_file,
    content => template('medicsystem_web/medicsystem-web.sysconfig.erb'),
    notify  => Service['medicsystem-web'],
  }

  file {'medicsystem-web upstart':
    ensure  => file,
    path    => $mw_upstart_file,
    content => template('medicsystem_web/medicsystem-web.upstart.erb')
  }

  exec {'forever install':
    environment => ['HOME=/home/medicsystem'],
    cwd         => $mw_dir,
    command     => '/usr/bin/npm install -g forever',
    timeout     => 600,
    require     => Package['npm'],
  }

  exec {'medicsystem-web npm install':
    environment => ['HOME=/home/medicsystem'],
    cwd         => $mw_dir,
    command     => '/usr/bin/npm install --unsafe-perm',
    timeout     => 600,
    require     => [ Package['npm'], Exec['deploy medicsystem-web'], Exec['deploy node-logger'], Exec['forever install'] ],
  }

  exec {'fix medicsystem permissions':
    cwd     => $medicsystem_dir,
    command => "/bin/chown -R ${user_group}:${user_group} *",
    require => Exec['medicsystem-web npm install'],
    notify  => Service['medicsystem-web'],
  }

  logrotate::rule {'medicsystem-web':
    path         => $mw_log_file,
    rotate       => 5,
    rotate_every => 'day',
    copytruncate => true,
    missingok    => true,
  }

  generic::upstartservice {'medicsystem-web':
    ensure  => running,
  }

}