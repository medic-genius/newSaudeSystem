#define dashboard platform profile
class dashboard_platform {

  $dp_origin_dir = "${::generic::params::vagrant_dir}/dashboard-platform"
  $wildfly_dir = "${::generic::params::opt_dir}/wildfly"

  class { 'java':
    distribution => 'jdk',
    package      => 'java-1.8.0-openjdk-devel',
  }

  archive { '/tmp/apache-maven-3.3.3-bin.zip':
    source       => 'http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.zip',
    extract      => true,
    extract_path => '/tmp',
    require      => Class['java'],
  }

  class { 'wildfly':
    version          => '8.2.0',
    java_home        => '/usr/lib/jvm/java-1.8.0-openjdk',
    install_source   => 'http://download.jboss.org/wildfly/8.2.0.Final/wildfly-8.2.0.Final.tar.gz',
    group            => 'wildfly',
    user             => 'wildfly',
    dirname          => $wildfly_dir,
    mode             => 'standalone',
    config           => 'standalone.xml',
    java_xmx         => '512m',
    java_xms         => '256m',
    java_maxpermsize => '256m',
    users_mgmt       => {
      'wildfly' => {
          username => 'wildfly',
          password => 'wildfly'
        }
      },
  }

  wildfly::config::module { 'org.postgresql':
    source       => 'https://jdbc.postgresql.org/download/postgresql-9.4-1201.jdbc4.jar',
    dependencies => ['javax.api', 'javax.transaction.api']
  }

  wildfly::standalone::datasources::driver { 'Driver postgresql':
    driver_name                     => 'postgresql',
    driver_module_name              => 'org.postgresql',
    driver_xa_datasource_class_name => 'org.postgresql.xa.PGXADataSource'
  }
  ->
  wildfly::standalone::datasources::datasource { 'medicsystemDS':
    config => {
      'driver-name'    => 'postgresql',
      'connection-url' => $::generic::params::medicsystem_pg_url,
      'jndi-name'      => 'java:jboss/datasources/medicsystemDS',
      'user-name'      => $::generic::params::medicsystem_pg_usr,
      'password'       => $::generic::params::medicsystem_pg_pwd,
    }
  }
  ->
  wildfly::standalone::datasources::datasource { 'dentalsystemDS':
    config => {
      'driver-name'    => 'postgresql',
      'connection-url' => $::generic::params::dentalsystem_pg_url,
      'jndi-name'      => 'java:jboss/datasources/dentalsystemDS',
      'user-name'      => $::generic::params::dentalsystem_pg_usr,
      'password'       => $::generic::params::dentalsystem_pg_pwd,
    }
  }

  exec { 'dashboard maven install':
    command   => '/tmp/apache-maven-3.3.3/bin/mvn -e clean install',
    cwd       => $dp_origin_dir,
    logoutput => true,
    timeout   => 1200,
    require   => Class['wildfly'],
  }

  exec { 'deploy dashboard-platform':
    command => "/bin/cp dashboard.ear ${wildfly_dir}/standalone/deployments/",
    cwd     => "${dp_origin_dir}/dashboard-ear/target/",
    require => Exec['dashboard maven install'],
  }

  firewall { '100 allow http and https access':
    port   => [8080, 443],
    proto  => tcp,
    action => accept,
  }

}