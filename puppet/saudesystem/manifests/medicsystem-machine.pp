node default {
  #aux classes
  include generic::params
  #default centos setup
  include generic

  include medicsystem_platform
  include medicsystem_web
  include folha_reader
}