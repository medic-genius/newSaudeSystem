# @version: 1.5

from flask import Flask, request, make_response, abort
from pyparsing import *
import sys
import time
import csv
import io
import StringIO
import json
    

dash = oneOf("-")
digits = "0123456789"
date = Word( digits ) + "/" + Word( digits ) + "/" + Word( digits )
monthdate = Word( digits ) + "/" + Word( digits )
real = Optional( Word(digits) + ".") + Word( digits ) + "," + Word( digits )
name = Word(alphas) + Optional(".") + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) #juizo nesse ultimo
inoutName =  Optional(Word(digits)) + Word(alphas) + Optional("EXTRA60") + Optional("EXTRA 60 VALOR") + Optional(Word(digits)+"SALARIO") +  Optional(Word(alphas)) +  Optional(Word(alphas)) + Optional(Word(digits)+"DA CLT") + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas))

cargo = name + Optional( "/"+ name ) + Optional( "("+ name + ")" + name ) + Optional( "("+ name + ")" )  + Optional( "."+ name )

cnpj_number =  Word(digits) + "." + Word(digits) + "." + Word(digits) + "/" + Word(digits) + "-" + Word(digits)
cnpj = Optional("C.E.I. / C.N.P.J.:" + cnpj_number)
company_name = name + Optional( Optional("-" + ~ Word(" - C.")) + Optional(name) )  
unidade_name = name + Optional("."+ name);

# FLUXO PRINCIPAL
cabecalhoDescription = Word(digits) + "-" + Group(company_name)  + "-" + Group(cnpj) + "MES:" + Group(monthdate)
unidadeDescription = "DEPTO." + ":" + Word(digits) + "- SETOR:" + Word(digits) + "- SECAO:" + Word(digits) + "-" + Group(unidade_name)
funcDescription = Word( digits ) + "-" + Group(name) + "--ADM:" + Group(date) + "-N.Dep:" + Word( digits ) + "-VR.SAL:" + Group(real)
inoutValues = "|" + Word( digits ) + "-" + Group(inoutName) + Group(real) + Group(real) + Optional(Word( digits ) + "-" + Group(inoutName) + Group(real) + Group(real))
resumoLiquidoValues = "|" + Word("FUNCAO") + ":" + Group(cargo) + Word("CBO:") +  Word(digits) +  "-"+  Word(digits) + Word("VALOR LIQUIDO") + ":" + Group(real)
impostoValues = "|" + Word("BASE IRRF") + ":" + Group(real) + Word("CONTR.INSS") + ":" + Group(real) + Word("INSS SEGURADO") + ":" + Group(real) + Word("BASE FGTS") + ":" + Group(real) + Word("FGTS") + Group(real)
#+ "-" + Group(cargo)
app = Flask(__name__)


def converterFolha(raw):

    f = StringIO.StringIO(raw)

    millis = int(round(time.time() * 1000))

    output = io.BytesIO()
    outputerr = io.BytesIO()

    CSVwriter = csv.writer(output, delimiter=',',
                                 doublequote=True)
    CSVwriter.writerow(['cod','nome',  'cargo' ,'admissao', 'nrodep', 'salario', 'fluxo' ,'liquido', 'base_irrf', 'contr_inss', 'inss_segurado', 'base_fgts', 'fgts'])

    arrayList = []

    csvline = {}

    numFunc = 0
    mesReferencia = ""
    nmUnidade = ""
    nmEmpresaFinanceiro = ""
    
    for line in f:

        try:
            line = line.strip()
            cabecalhoLine = cabecalhoDescription.parseString(line)
            mesReferencia = "".join(cabecalhoLine[6])
            nmEmpresaFinanceiro = "".join(cabecalhoLine[2])
            print( mesReferencia )
        except Exception, err:   
            try:
                unidadeLine = unidadeDescription.parseString(line)
                nmUnidade = "".join(unidadeLine[7])
            except Exception, err:   
                try:
                    #exceptions: use with caution!
                    line = line.replace("-ME"," ME")
                    line = line.replace("-MARK/CRIACAO"," CRIACAO")
                    line = line.replace("-S.G. (JOYCE"," S.G")
                    line = line.replace("MEDICO","-MEDICO")
                    
                    firstLine = funcDescription.parseString(line)
                    arrayList.append(csvline)
                    csvline = {}
                    csvline['fluxo'] = {}
                    i = 0
                    csvline['cod'] = firstLine[0]
                    csvline['name'] = " ".join(firstLine[2])
                    #csvline['cargo'] = " ".join(firstLine[4])
                    csvline['admissao'] = "".join(firstLine[4])
                    csvline['nrodep'] = firstLine[6]
                    csvline['salario'] = "".join(firstLine[8])
                    numFunc = numFunc + 1
                except Exception, err:
                    try:
                            flowLines = inoutValues.parseString(line)
                            i = i + 1

                            if len(flowLines) > 6   :
                                csvline['fluxo']["fluxo" + str(i)] = {
                                "fcodLeft": flowLines[1],
                                "fdescLeft": " ".join(flowLines[3]),
                                "fvlLeft": "".join(flowLines[4]),
                                "fvtLeft": "".join(flowLines[5]),
                                }
                                try:
                                    csvline['fluxo']["fluxo" + str(i)]["fcodRight"] = flowLines[6]
                                    csvline['fluxo']["fluxo" + str(i)]["fdescRight"] = " ".join(flowLines[8])
                                    csvline['fluxo']["fluxo" + str(i)]["fvlRight"] = "".join(flowLines[9])
                                    csvline['fluxo']["fluxo" + str(i)]["fvtRight"] = "".join(flowLines[10])
                                except IndexError, e:
                                    None    
                            else:
                                if flowLines[1].startswith('5') or flowLines[1] == '9860' or flowLines[1] == '8910' :
                                    csvline['fluxo']["fluxo" + str(i)] = {
                                    "fcodRight": flowLines[1],
                                    "fdescRight": " ".join(flowLines[3]),
                                    "fvlRight": "".join(flowLines[4]),
                                    "fvtRight": "".join(flowLines[5]),
                                    }
                                else:
                                    csvline['fluxo']["fluxo" + str(i)] = {
                                    "fcodLeft": flowLines[1],
                                    "fdescLeft": " ".join(flowLines[3]),
                                    "fvlLeft": "".join(flowLines[4]),
                                    "fvtLeft": "".join(flowLines[5]),
                                    }

                    except Exception, err:
                        try:
                            
                            line = line.replace("-MEDICO","MEDICO")

                            resumo = resumoLiquidoValues.parseString(line)

                            #tratamento cargo funcionario
                            cargoArray = resumo[3]
                            resumo[3] = []

                            if len( cargoArray ) == 1 :
                                for description in cargoArray :
                                    cargoArray[0] =  description.replace("CBO", "")
                                    resumo[3].append( cargoArray[0] ) 
                            else :
                                for description in cargoArray :
                                    if "CBO" not in str(description):
                                        resumo[3].append(description);
                                    else: 
                                        description =  description.replace("CBO", "")
                                        if len(description) > 0 :
                                            resumo[3].append(description);
                            #fim tratamento cargo funcionario
                                
                            csvline['cargo'] = " ".join(resumo[3])
                            csvline['liquido'] = "".join(resumo[10])

                        except Exception, err:
                            try:
                                impostos = impostoValues.parseString(line)
                                csvline['base_irrf'] = "".join(impostos[3])
                                csvline['contr_inss'] = "".join(impostos[6])
                                csvline['inss_segurado'] = "".join(impostos[9])
                                csvline['base_fgts'] = "".join(impostos[12])
                                csvline['fgts'] = "".join(impostos[14])
                            except Exception, err:
                                outputerr.write("INVALIDO: " + err.line + "\n")
        
    arrayList.append(csvline)

    for item in arrayList:
        print(item )
        print("\n")
        if item != {}:
            CSVwriter.writerow([
                item['cod'], 
                item['name'], 
                item['cargo'], 
                item['admissao'], 
                item['nrodep'], 
                item['salario'], 
                json.dumps(item['fluxo']), 
                item['liquido'], 
                item['base_irrf'], 
                item['contr_inss'], 
                item['inss_segurado'], 
                item['base_fgts'], 
                item['fgts']])

    data = {}
    data['mesreferencia'] = mesReferencia
    data['nmunidade'] = nmUnidade
    data['nmempresafinanceiro'] = nmEmpresaFinanceiro
    data['nroimportados'] = numFunc
    data['output'] = output.getvalue()
    data['errors'] = outputerr.getvalue()

    print( data )
    print("\n")

    json_data = json.dumps(data)
    return json_data

@app.route('/', methods=['GET', 'POST'])
def convert():
    if request.method == 'POST':
        result = converterFolha(request.data)
        print result
        return result

    return abort(401)

if __name__ == "__main__":
    app.run(host='0.0.0.0')