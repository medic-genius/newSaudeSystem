# @version: 1.4

from flask import Flask, request, make_response, abort
from pyparsing import * #Word, alphas, Group, Optional, ParseException, oneOf
import sys
import time
import csv
import io
import StringIO
import json
    
dash = oneOf("-")
digits = "0123456789"
date = Word( digits ) + "/" + Word( digits ) + "/" + Word( digits )
monthdate = Word( digits ) + "/" + Word( digits )
real = Optional( Word(digits) + ".") + Word( digits ) + "," + Word( digits )
name = Word(alphas) + Optional(".") + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) #juizo nesse ultimo
inoutName =  Optional(Word(digits)) + Word(alphas) + Optional("EXTRA60") + Optional("EXTRA 60 VALOR") + Optional(Word(digits)+"SALARIO") +  Optional(Word(alphas)) +  Optional(Word(alphas)) + Optional(Word(digits)+"DA CLT") + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas)) + Optional(Word(alphas))

cargo = name + Optional( "/"+ name ) + Optional( "("+ name + ")" + name ) + Optional( "("+ name + ")" )  + Optional( "."+ name )

cnpj_number =  Word(digits) + "." + Word(digits) + "." + Word(digits) + "/" + Word(digits) + "-" + Word(digits)
cnpj = Optional("C.E.I. / C.N.P.J.:" + cnpj_number)
company_name = name + Optional( Optional("-" + ~ Word(" - C.")) + Optional(name) )  
unidade_name = name + Optional("."+ name);

# FLUXO PRINCIPAL
cabecalhoDescription = Word(digits) + "-" + Group(company_name)  + "-" + Group(cnpj) + "MES:" + Group(monthdate)
unidadeDescription = "DEPTO." + ":" + Word(digits) + "- SETOR:" + Word(digits) + "- SECAO:" + Word(digits) + "-" + Group(unidade_name)
funcDescription = Word( digits ) + "-" + Group(name) + "--ADM:" + Group(date) + "-N.Dep:" + Word( digits ) + "-VR.SAL:" + Group(real)
inoutValues = "|" + Word( digits ) + "-" + Group(inoutName) + Group(real) + Group(real) + Optional(Word( digits ) + "-" + Group(inoutName) + Group(real) + Group(real))
resumoLiquidoValues = "|" + Word("FUNCAO") + ":" + Group(cargo) + Word("CBO:") +  Word(digits) +  "-"+  Word(digits) + Word("VALOR LIQUIDO") + ":" + Group(real)
impostoValues = "|" + Word("BASE IRRF") + ":" + Group(real) + Word("CONTR.INSS") + ":" + Group(real) + Word("INSS SEGURADO") + ":" + Group(real) + Word("BASE FGTS") + ":" + Group(real) + Word("FGTS") + Group(real)


teste="| FUNCAO: RECEPCIONISTA (A)       CBO: 4221-05                                                         VALOR LIQUIDO:       1.282,00 |"
validar = resumoLiquidoValues.parseString(teste);
print( validar );


#FUNCIONARIO NAME

#UNIDADES
unidadeDentaAutonomos = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - AUTONOMOS"
unidadeDentalCTPS = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - REGISTRO"
unidadeDentalDentistas = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - DENTISTAS"
unidadeLabmedJoaquim = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - JOAQUIM NABUCO"
unidadeLabmedTaruma = "DEPTO.: 0002 - SETOR: 0000 - SECAO: 0000 - TARUMA"
unidadeMedicAutoJoaquim = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - JOAQUIM NABUCO"
unidadeMedicAutoTaruma = "DEPTO.: 0002 - SETOR: 0000 - SECAO: 0000 - TARUMA"
unidadeMedicCTPSJoaquim = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - JOAQUIM NABUCO"
unidadeMedicCTPSTaruma = "DEPTO.: 0002 - SETOR: 0000 - SECAO: 0000 - TARUMA"
unidadeMedicMedico = "DEPTO.: 0003 - SETOR: 0000 - SECAO: 0000 - MEDICOS"
unidadePerformanceCircular = "DEPTO.: 0003 - SETOR: 0000 - SECAO: 0000 - PERFORMANCE G. CIRCULAR"
unidadePerformanceTaruma = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - PERFORMANCE TAPAJOS"
unidadePerformanceVieiralves = "DEPTO.: 0002 - SETOR: 0000 - SECAO: 0000 - PERFORMANCE VIEIRALVES"
unidadeDrConsultaTaruma = "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - TARUMA"
unidadeDentalGoiania =  "DEPTO.: 0001 - SETOR: 0000 - SECAO: 0000 - GOIANIA"


#CABECALHOS
headerDentalAutonomosDone = "0011 - J A LIMA NASCIMENTO EPP - DENTAL SAUDE AUTONOMOS   - C.E.I. / C.N.P.J.: 20.735.214/0001-13                MES:      05/2016"
headerDentalCTPSDone = "0018 - J A LIMA NASCIMENTO                                - C.E.I. / C.N.P.J.: 20.735.214/0001-13                MES:      05/2016"
headerDentalDentistasDone = "0015 - DENTAL SAUDE - DENTISTAS                           - C.E.I. / C.N.P.J.: 20.735.214/0001-13                MES:      05/2016"
headerLabmedDone = "0009 - LABMED LABORATORIO DE ANALISES CLINICAS            - C.E.I. / C.N.P.J.: 03.074.856/0001-08                MES:      05/2016"
headerMedicAutoDone = "0020 - MEDIC LAB  - AUTONOMOS                             - C.E.I. / C.N.P.J.: 14.139.462/0001-07                MES:      05/2016"
headerMedicCTPSDone = "0017 - MEDIC LAB SERVICOS E ATIVIDADE MEDICA LTDA         - C.E.I. / C.N.P.J.: 14.139.462/0001-07                MES:      05/2016"
headerMedicMedicoDone = "0022 - MEDIC LAB - MEDICOS                                - C.E.I. / C.N.P.J.: 14.139.462/0001-07                MES:      05/2016"
headerPerformanceDone = "0043 - RITA CAMPOS DOS SANTOS - PLANO DE SAUDE EPP        - C.E.I. / C.N.P.J.: 19.885.596/0001-82                MES:      05/2016"
headerDrConsultaDone = "0029 - DOUTOR CONSULTA MANAUS CLINICA MEDICA EIRELI -EPP  - C.E.I. / C.N.P.J.: 25.079.896/0001-77                MES:      08/2016"
headerDentalGoianiaDone = "0016 - DENTAL SAUDE SOCIEDADE SIMPLES LTDA EPP            - C.E.I. / C.N.P.J.: 02.806.968/0001-35                MES:      08/2016"




