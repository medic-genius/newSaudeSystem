# Plataforma Saude System

Este diretorio engloba todo o ecosistema chamado Saude System composto pelos sistemas de gerenciamento de planos de saude, plano odontologico (TODO), academia de ginastica (TODO) e o sistema dashboard que acessa todos esses sistemas.

## Sistemas

- medicsystem(-platform,-web,-utils,-biometria)
    (platform) - Criado em JEE tem a funcao de agregar toda API de servico e acesso a dados da base dbRealVida - representa o backend do antigo SaudeSystemWeb
    (web) - Criado em HTML5+Angular+Node.JS representa a camada de apresentacao da API do medicsystem-platform.
    (utils) - Contem bibliotecas nao suportadas pelos repositorios Maven na internet.
    (biometria) - Servico desktop de autenticacao e cadastro de fingerprints usando websockets e APIs de acesso a dispositivo fingerprint reader.

- dashboard(-platform,-web) 
    - Criado em JEE tem a funcao de agregar uma API de alto nivel que produz informacoes gerenciais em relacao as empresas de plano de saude, plano odontologico e academia.
    - Criado em HTML5+Angular+Node.JS representa a camada de apresentacao da API do dashboard-platform.

## Sistemas auxiliares

- keycloak
    - Sistema de autenticação e autorização para os sistemas principais.

- rundeck
    - Sistema de gerenciamento de jobs para os sistemas principais.

## Deploy em produção

### Requisitos

- Vagrant 1.7.2 ou mais recente [link](https://www.vagrantup.com/downloads.html)
- Virtualbox 4.3.x ou mais recente [link](https://www.virtualbox.org/wiki/Downloads)
- Git

### Baixando o projeto e fazendo o deploy

#### Checkout do repositório

Faça o checkout via linha de comando ou pelo cliente de sua preferência utilizando a URL do projeto https://gitlab.com/medic-genius/newSaudeSystem.git ou via SSH utilizando git@gitlab.com:medic-genius/newSaudeSystem.git

#### Instalação

Instale o Virtualbox e em seguida o Vagrant.

#### Configurando o banco de dados

Vá até a pasta `<diretório do projeto>\puppet\medicsaude\files` e abra o arquivo `standalone.xml`.

Procure o bloco abaixo e altere as tags `<connection-url>`, `<user-name>` e `<password>` para o servidor postgresql que deseja apontar.

    <datasource jndi-name="java:jboss/datasources/medicsystemDS" pool-name="medicsystemDS" enabled="true">
        <connection-url>jdbc:postgresql://192.169.0.96/dbRealVida</connection-url>
        <driver>postgres</driver>
        <security>
            <user-name>postgres</user-name>
            <password>postgres</password>
        </security>
    </datasource>

#### Instalando o serviço via vagrant

Abra a linha de comando do Windows.
Direcione para a pasta onde o projeto foi baixado `cd <diretório do projeto>`.

Execute o comando `vagrant up <nome projeto>` para subir o projeto desejado. Acompanhe o log até o OK final do script.

* `<nome projeto>` pode ser substituido por `medicsystem`,`dashboard`, `keycloak`, `rundeck` caso nenhum parametro seja passado o vagrant subirá todos os projetos definidos no Vagrantfile.

* Vagrant up para o `rundeck` deve ser executado dentro da pasta `<diretorio do projeto>\rundeck-jobserver`.

### Configurando NGINX

- Para configurar o NGINX como proxy reverso para os sistemas e necessario seguir as diretrizes usadas no servidor srvnovamedic, as configuracoes usadas neste servidor podem ser encontradas no diretorio puppet\nginx-srvnovamedic.conf.

- (*) Configuração nginx para o rundeck pode ser encontrada em puppet\nginx-rundeck-srvnovamedic.conf

## Acompanhar logs do Wildfly

Abra a linha de comando do Windows.
Direcione para a pasta onde o projeto foi baixado `cd <diretório do projeto>`.

Execute o comando `vagrant ssh <nome projeto>` para acessar a vm do projeto.

O comando acima autenticará na vm e o ambiente da linha de comando passa a ser linux.

Para ver os logs do Wildfly use o comando `tail -f /var/log/wildfly/console.log`.

Se houver dificuldades para acessar o log utilize o comando `sudo su` para se tornar root.

## Realizando novo deploy via vagrant

- Primeiro atualize apartir do servidor o branch com as novidades a serem postadas.
- Execute o comando `vagrant provision <nome projeto>`.
- Acompanhe o log de execução, se alguma mensagem em cor vermelha aparecer verifique as razoes e/ou tente novamente.