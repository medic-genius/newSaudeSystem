var express = require('express');
var bodyParser = require('body-parser'); // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var logger = require('node-logger'); // logger module
var expressWinston = require('express-winston'); // log requests to the console (express4)

var MedicLabAPIProxy = require('./mediclab-api-proxy').MedicLabAPIProxy;
var DentalSaudeAPIProxy = require('./dentalsaude-api-proxy').DentalSaudeAPIProxy;

var medicLabAPIProxy = new MedicLabAPIProxy();
var dentalSaudeAPIProxy = new DentalSaudeAPIProxy();

var app = express();

function errorHandler(err, req, res, next) {
  res.status(500);
  res.render('error', { error: err });
}

function clientErrorHandler(err, req, res, next) {
  if (req.xhr) {
    res.status(500).send({ error: 'Something blew up!' });
  } else {
    next(err);
  }
}

app.use(clientErrorHandler);
app.use(errorHandler);

// set the static files location /public/img will be /img for users
app.use(express.static(require('path').resolve(__dirname + "/../public")));

//integrate node-logger with express
app.use(expressWinston.logger({
  winstonInstance: logger,
  meta: false,
  msg: "HTTP {{req.method}} {{req.url}}",
  expressFormat: true,
  colorStatus: true,
  ignoreRoute: function (req, res) { return false; }
}));

//integrate node-logger with express erros
app.use(expressWinston.errorLogger({
  winstonInstance: logger,
  meta: false,
  msg: "HTTP {{req.method}} {{req.url}}",
  expressFormat: true,
  colorStatus: true,
  ignoreRoute: function (req, res) { return false; }
}));

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({'extended':'true'}));
//parse application/json
app.use(bodyParser.json());
//parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
//log every request to the console
app.use(methodOverride());

  
app.get('/', function(req, res) {
    var options = {
      root: __dirname + "/../",
      dotfiles: 'deny',
      headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
      }
    };

    res.sendFile('public/templates/admin4_material_design/angularjs/index.html', options, function(err){
        if (err) {
          logger.error(err);
          res.status(err.status).end();
        }
    });

});

//medic lab
app.get('/mediclab/report0001', function (req, res) {
    medicLabAPIProxy.report0001(req,res);
});

app.get('/mediclab/report0002', function (req, res) {
    medicLabAPIProxy.report0002(req,res);
});

app.get('/mediclab/report0003', function (req, res) {
    medicLabAPIProxy.report0003(req,res);
});

app.get('/mediclab/report0004', function (req, res) {
    medicLabAPIProxy.report0004(req,res);
});

app.get('/mediclab/report0005', function (req, res) {
    medicLabAPIProxy.report0005(req,res);
});

app.get('/mediclab/report0006', function (req, res) {
    medicLabAPIProxy.report0006(req,res);
});

app.get('/mediclab/report0007', function (req, res) {
    medicLabAPIProxy.report0007(req,res);
});

//Dental Saude
app.get('/dentalsaude/report0001', function (req, res) {
    dentalSaudeAPIProxy.report0001(req,res);
});

app.get('/dentalsaude/report0002', function (req, res) {
    dentalSaudeAPIProxy.report0002(req,res);
});

app.get('/dentalsaude/report0003', function (req, res) {
    dentalSaudeAPIProxy.report0003(req,res);
});

app.get('/dentalsaude/report0004', function (req, res) {
    dentalSaudeAPIProxy.report0004(req,res);
});

app.get('/dentalsaude/report0005', function (req, res) {
    dentalSaudeAPIProxy.report0005(req,res);
});

app.get('/dentalsaude/report0006', function (req, res) {
    dentalSaudeAPIProxy.report0006(req,res);
});

app.get('/dentalsaude/report0007', function (req, res) {
    dentalSaudeAPIProxy.report0007(req,res);
});

// listen (start app with node server.js)
app.listen(process.env.SERVER_PORT || 9000);
logger.info("Dashboard WEB listening on port 9000");