/*!
 *  @license-start
 *  @license-end
 */

"use strict";

var request = require('request');
var logger = require("node-logger");

var baseURL = process.env.DASHBOARD_APPSERVER || 'http://localhost:8080'; //localhost

function DentalSaudeAPIProxy() {
}

DentalSaudeAPIProxy.prototype.report0001 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0001?ano=" + req.query.ano + "&mes=" + req.query.mes)
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);

    //req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0001?ano=" + req.query.ano + "&mes=" + req.query.mes)).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0002 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0002?inicio=" + req.query.inicio + "&fim=" + req.query.fim)
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
    //req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0002?inicio=" + req.query.inicio + "&fim=" + req.query.fim)).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0003 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0003?inicio=" + req.query.inicio + "&fim=" + req.query.fim)
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
   // req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0003?inicio=" + req.query.inicio + "&fim=" + req.query.fim)).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0004 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0004")
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
    //req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0004")).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0005 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0005?plano=" + req.query.plano)
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
    //req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0005?plano=" + req.query.plano)).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0006 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0006")
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
    //req.pipe(request(baseURL + "/report/rest/mediclab/reports/report0006")).pipe(res);
};

DentalSaudeAPIProxy.prototype.report0007 = function (req, res) {

    logger.info("Proxing request to server:", baseURL);
    request (baseURL + "/report/rest/dentalsaude/reports/report0007?ano=" + req.query.ano + "&mes=" + req.query.mes)
    .on('error', function (err){
        console.error(err);
    })
    .pipe(res);
    //req.pipe(request(baseURL + "/report/rest/dentalsaude/reports/report0007?ano=" + req.query.ano + "&mes=" + req.query.mes)).pipe(res);
};


module.exports = {
    DentalSaudeAPIProxy: DentalSaudeAPIProxy
};