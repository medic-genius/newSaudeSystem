/*******************************************************************************
 * Metronic AngularJS App Main Script
 ******************************************************************************/

/* Metronic App */
var DashboardApp = angular.module("DashboardApp", [ "ui.router",
		"ui.bootstrap", "oc.lazyLoad", "ngSanitize", "ngRoute", "ngResource" ]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
DashboardApp.config([ '$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	$ocLazyLoadProvider.config({
		cssFilesInsertBefore : 'ng_load_plugins_before' // load the above css
	// files before a LINK
	// element with this ID.
	// Dynamic CSS files
	// must be loaded
	// between core and
	// theme css files
	});
} ]);

/*******************************************************************************
 * BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
 ******************************************************************************/
/**
 * `$controller` will no longer look for controllers on `window`. The old
 * behavior of looking on `window` for controllers was originally intended for
 * use in examples, demos, and toy apps. We found that allowing global
 * controller functions encouraged poor practices, so we resolved to disable
 * this behavior by default.
 * 
 * To migrate, register your controllers with modules rather than exposing them
 * as globals:
 * 
 * Before:
 * 
 * ```javascript function MyController() { // ... } ```
 * 
 * After:
 * 
 * ```javascript angular.module('myApp', []).controller('MyController',
 * [function() { // ... }]);
 * 
 * Although it's not recommended, you can re-enable the old behavior like this:
 * 
 * ```javascript angular.module('myModule').config(['$controllerProvider',
 * function($controllerProvider) { // this option might be handy for migrating
 * old apps, but please don't use it // in new ones!
 * $controllerProvider.allowGlobals(); }]);
 */

// AngularJS v1.3.x workaround for old style controller declarition in HTML
DashboardApp.config([ '$controllerProvider', function($controllerProvider) {
	// this option might be handy for migrating old apps, but please don't use
	// it
	// in new ones!
	$controllerProvider.allowGlobals();
} ]);

DashboardApp.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

/*******************************************************************************
 * END: BREAKING CHANGE in AngularJS v1.3.x:
 ******************************************************************************/

/* Setup global settings */
DashboardApp.factory('settings', [ '$rootScope', function($rootScope) {
	// supported languages
	var settings = {
		layout : {
			pageSidebarClosed : false, // sidebar state
			pageAutoScrollOnLoad : 1000
		// auto scroll to top on page load
		},
		layoutImgPath : Metronic.getAssetsPath() + 'admin/layout/img/',
		layoutCssPath : Metronic.getAssetsPath() + 'admin/layout/css/'
	};

	$rootScope.settings = settings;

	return settings;
} ]);


/* Setup App Main Controller */
DashboardApp.controller('AppController', [ '$scope', '$rootScope',
		function($scope, $rootScope) {
			$scope.$on('$viewContentLoaded', function() {
				Metronic.initComponents(); // init core components
				// Layout.init(); // Init entire layout(header, footer, sidebar,
				// etc) on page load if the partials included in server side
				// instead of loading with ng-include directive
			});
		} ]);

/*******************************************************************************
 * Layout Partials. By default the partials are loaded through AngularJS
 * ng-include directive. In case they loaded in server side(e.g: PHP include
 * function) then below partial initialization can be disabled and Layout.init()
 * should be called on page load complete as explained above.
 ******************************************************************************/

/* Setup Layout Part - Header */
DashboardApp.controller('HeaderController', [ '$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		Layout.initHeader(); // init header
	});
} ]);

/* Setup Layout Part - Sidebar */
DashboardApp.controller('SidebarController', [ '$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		Layout.initSidebar(); // init sidebar
	});
} ]);

/* Setup Layout Part - Sidebar */
DashboardApp.controller('PageHeadController', [ '$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		Demo.init(); // init theme panel
	});
} ]);

/* Setup Layout Part - Footer */
DashboardApp.controller('FooterController', [ '$scope', function($scope) {
	$scope.$on('$includeContentLoaded', function() {
		Layout.initFooter(); // init footer
	});
} ]);

/* Setup Rounting For All Pages */
DashboardApp
		.config([
				'$stateProvider',
				'$urlRouterProvider',
				function($stateProvider, $urlRouterProvider) {

					// Redirect any unmatched url
					$urlRouterProvider.otherwise("/dashboard.html");

					$stateProvider

							// Dashboard
							.state(
									'dashboard',
									{
										url : "/dashboard.html",
										templateUrl : "templates/admin4_material_design/angularjs/views/dashboard.html",
										data : {
											pageTitle : 'Dashboard',
											pageSubTitle : ''
										},
										controller : "DashboardController",
										resolve : {
											deps : [
													'$ocLazyLoad',
													function($ocLazyLoad) {
														return $ocLazyLoad
																.load({
																	name : 'DashboardApp',
																	insertBefore : '#ng_load_plugins_before', // load
																	// the
																	// above
																	// css
																	// files
																	// before
																	// '#ng_load_plugins_before'
																	files : [
																			'assets/admin/pages/scripts/index3.js',
																								
																			'assets/global/plugins/d3js/d3.min.js',
																			'assets/global/plugins/d3js/d3pie.min.js',
																			'templates/admin4_material_design/angularjs/js/scripts/d3pieutils.js',
																			'templates/admin4_material_design/angularjs/js/scripts/dateutils.js',

																			'assets/global/plugins/c3js/c3.min.css',
																			'assets/global/plugins/c3js/c3.min.js',
																			'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',

																			'templates/admin4_material_design/angularjs/js/services/mediclab_report_service.js',

																			'templates/admin4_material_design/angularjs/js/controllers/DashboardController.js' ]
																});
													} ]
										}
									})
							.state(
									'db_dentalsaude',
									{
										url : "/db_dentalsaude.html",
										templateUrl : "templates/admin4_material_design/angularjs/views/db_dentalsaude.html",
										data : {
											pageTitle : 'Dental Saude - Dashboard',
											pageSubTitle : ''
										},
										controller : "DSDashboardController",
										resolve : {
											deps : [
													'$ocLazyLoad',
													function($ocLazyLoad) {
														return $ocLazyLoad
																.load({
																	name : 'DashboardApp',
																	insertBefore : '#ng_load_plugins_before', // load
																	// the
																	// above
																	// css
																	// files
																	// before
																	// '#ng_load_plugins_before'
																	files : [
																			'assets/admin/pages/scripts/index3.js',
																									
																			'assets/global/plugins/d3js/d3.min.js',
																			'assets/global/plugins/d3js/d3pie.min.js',
																			
																			'templates/admin4_material_design/angularjs/js/scripts/d3pieutils.js',

																			'assets/global/plugins/c3js/c3.min.css',
																			'assets/global/plugins/c3js/c3.min.js',
																			'templates/admin4_material_design/angularjs/js/scripts/c3-utils.js',

																			'templates/admin4_material_design/angularjs/js/scripts/dateutils.js',

																			'templates/admin4_material_design/angularjs/js/services/dentalsaude_report_service.js',

																			'templates/admin4_material_design/angularjs/js/controllers/DSDashboardController.js' ]
																});
													} ]
										}
									})
							
				} ]);

/* Init global settings and run the app */
DashboardApp.run([ "$rootScope", "settings", "$state",
		function($rootScope, settings, $state) {
			$rootScope.$state = $state; // state to be accessed from view
		} ]);