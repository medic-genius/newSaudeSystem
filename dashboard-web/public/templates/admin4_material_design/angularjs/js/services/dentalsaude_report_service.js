DashboardApp.factory('dsReportFactory', ['$resource',
    function($resource) {
        return {
            report0001: $resource('dentalsaude/report0001', {}, {
                query: { method:'GET' }
            }),
            report0002: $resource('dentalsaude/report0002', {}, {
                query: { method:'GET' }
            }),
            report0003: $resource('dentalsaude/report0003', {}, {
                query: { method:'GET' }
            }),
            report0004: $resource('dentalsaude/report0004', {}, {
                query: { method:'GET' }
            }),
            report0006: $resource('dentalsaude/report0006', {}, {
                query: { method:'GET', isArray: true }
            }),
            report0007: $resource('dentalsaude/report0007', {}, {
                query: { method:'GET' }
            }),
        }
    }
]);