DashboardApp.factory('mlReportFactory', ['$resource',
    function($resource) {
        return {
            report0001: $resource('mediclab/report0001', {}, {
                query: { method:'GET' }
            }),
            report0002: $resource('mediclab/report0002', {}, {
                query: { method:'GET' }
            }),
            report0003: $resource('mediclab/report0003', {}, {
                query: { method:'GET' }
            }),
            report0004: $resource('mediclab/report0004', {}, {
                query: { method:'GET' }
            }),
            report0006: $resource('mediclab/report0006', {}, {
                query: { method:'GET', isArray: true }
            }),
            report0007: $resource('mediclab/report0007', {}, {
                query: { method:'GET' }
            }),
        }
    }
]);