'use strict';

DashboardApp.controller('DSDashboardController', ['$rootScope', '$scope', '$http', '$timeout', 'dsReportFactory', function($rootScope, $scope, $http, $timeout, dsReportFactory ) {
    
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        Metronic.initAjax();

        dsReportFactory.report0006.query(
            function(data) {
                C3Utils.drawFaturamentoMesAMes("#report0002chart", data);
            },
            function(data) {
                console.log("ERROR: " + JSON.stringify(data));
            }
        );
        
        $scope.report0001 = {};
        $scope.report0001.data = [];
        $scope.report0001.year = DateUtils.getYear();
        $scope.report0001.month = DateUtils.getMonth();
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        report0001query();

        $scope.report0007 = {};
        $scope.report0007.data = [];
        report0007query();

        $scope.report0002 = {};
        $scope.report0002.data = [];
        $scope.report0002.year = DateUtils.getYear();
        $scope.report0002.month = DateUtils.getMonth();
        $scope.report0002.firstDay = DateUtils.getFirstDayISO();
        $scope.report0002.lastDay = DateUtils.getLastDayISO();
        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        report0002query();

        $scope.report0003 = {};
        $scope.report0003.data = [];
        $scope.report0003.year = DateUtils.getYear();
        $scope.report0003.month = DateUtils.getMonth();
        $scope.report0003.firstDay = DateUtils.getFirstDayISO();
        $scope.report0003.lastDay = DateUtils.getLastDayISO();
        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        report0003query();

        dsReportFactory.report0004.query(
            function(data) {
                $scope.report0004 = data.metadata;
            },
            function(data) {
                console.log(JSON.stringify(data));
            }
        );
    });

    $scope.nextMonthReport0001 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0001.year, $scope.report0001.month);
        $scope.report0001.year = nextMonthOfYear.year;
        $scope.report0001.month = nextMonthOfYear.month;
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        
        report0001query(true);
        report0007query();
    };

    $scope.previousMonthReport0001 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0001.year, $scope.report0001.month);
        $scope.report0001.year = previousMonthOfYear.year;
        $scope.report0001.month = previousMonthOfYear.month;
        $scope.report0001.my = DateUtils.prettyMonth()[$scope.report0001.month] + "\\" + $scope.report0001.year;
        
        report0001query(true);
        report0007query();
    };

    var report0001query = function(updateGraph) {
        dsReportFactory.report0001.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
            function(data) {
                var pie = D3PieUtils.init("report0001piechart", data.graphData);
                if(updateGraph) {
                    pie.destroy();
                    pie = D3PieUtils.init("report0001piechart", data.graphData);
                }
                $scope.report0001.data = data.metadata;
            },
            function(data) {
                console.log(JSON.stringify(data));
            }
        );
    };

    var report0007query = function() {
        dsReportFactory.report0007.query({ ano: $scope.report0001.year, mes: $scope.report0001.month },
            function(data) {
                $scope.report0007.data = data.metadata;
            },
            function(data) {
                console.log(JSON.stringify(data));
            }
        );
    };

    $scope.nextMonthReport0002 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0002.year, $scope.report0002.month);
        $scope.report0002.year = nextMonthOfYear.year;
        $scope.report0002.month = nextMonthOfYear.month;
        $scope.report0002.firstDay = nextMonthOfYear.firstDay;
        $scope.report0002.lastDay = nextMonthOfYear.lastDay;

        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        
        report0002query();
    };

    $scope.previousMonthReport0002 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0002.year, $scope.report0002.month);
        $scope.report0002.year = previousMonthOfYear.year;
        $scope.report0002.month = previousMonthOfYear.month;
        $scope.report0002.firstDay = previousMonthOfYear.firstDay;
        $scope.report0002.lastDay = previousMonthOfYear.lastDay;

        $scope.report0002.my = DateUtils.prettyMonth()[$scope.report0002.month] + "\\" + $scope.report0002.year;
        
        report0002query();
    };

    var report0002query = function() {
        dsReportFactory.report0002.query({ inicio: $scope.report0002.firstDay, fim: $scope.report0002.lastDay },
            function(data) {
                $scope.report0002.data = data.metadata;
            },
            function(data) {
                console.log(JSON.stringify(data));
            }
        );
    };

    $scope.nextMonthReport0003 = function () {
        var nextMonthOfYear = DateUtils.getNextMonthOfYear($scope.report0003.year, $scope.report0003.month);
        $scope.report0003.year = nextMonthOfYear.year;
        $scope.report0003.month = nextMonthOfYear.month;
        $scope.report0003.firstDay = nextMonthOfYear.firstDay;
        $scope.report0003.lastDay = nextMonthOfYear.lastDay;

        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        
        report0003query();
    };

    $scope.previousMonthReport0003 = function () {
        var previousMonthOfYear = DateUtils.getPreviousMonthOfYear($scope.report0003.year, $scope.report0003.month);
        $scope.report0003.year = previousMonthOfYear.year;
        $scope.report0003.month = previousMonthOfYear.month;
        $scope.report0003.firstDay = previousMonthOfYear.firstDay;
        $scope.report0003.lastDay = previousMonthOfYear.lastDay;

        $scope.report0003.my = DateUtils.prettyMonth()[$scope.report0003.month] + "\\" + $scope.report0003.year;
        
        report0003query();
    };

    var report0003query = function() {
        dsReportFactory.report0003.query({ inicio: $scope.report0003.firstDay, fim: $scope.report0003.lastDay },
            function(data) {
                $scope.report0003.data = data.metadata;
            },
            function(data) {
                console.log(JSON.stringify(data));
            }
        );
    };

}]);