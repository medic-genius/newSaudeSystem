var C3Utils = function() {
	return {
		drawFaturamentoMesAMes : function(element,json) {
			var chart = c3.generate({
                size: {
                    height: 240
                },
                bindto: element,
                data: {
                    json: json,
                    keys: {
                        x: 'mes',
                        value: ['contratosnovos', 'contratosinativados']
                    },
                    names: {
                        'contratosnovos': 'Faturamento Novos',
                        'contratosinativados': 'Faturamento Inativados',
                    },
                    types: {
                        'contratosnovos': 'area-spline',
                        'contratosinativados': 'area-spline'
                    },
                    labels: {
                        format: {
                                'contratosnovos': d3.format('$,.2f'),
                                'contratosinativados': d3.format('$,.2f')
                        }
                    }
                },
                axis: {
                    x: {
                        'type': 'categories',
                    },
                    y: {
                        label: "Faturamento"
                    }
                },
                zoom: {
                    enabled: false
                }
            });
		},
	};
}();