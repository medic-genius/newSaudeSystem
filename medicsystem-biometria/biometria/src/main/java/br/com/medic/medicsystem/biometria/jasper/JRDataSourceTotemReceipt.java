package br.com.medic.medicsystem.biometria.jasper;

import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.biometria.model.TotemReceipt;

public class JRDataSourceTotemReceipt implements JRDataSource{

	private Iterator<TotemReceipt> it;
	private TotemReceipt current;
	private boolean goToNext = true;
	
	public JRDataSourceTotemReceipt() {

	}
	
	public JRDataSourceTotemReceipt(List<TotemReceipt> list) {
		this.it = list.iterator();
	}

	public boolean next() throws JRException {
		current = it.hasNext() ? it.next() : null;
		goToNext = (current != null);

		return goToNext;
	}

	public Object getFieldValue(JRField field) throws JRException {

		String nomeCampo  = field.getName();
		if(nomeCampo.equals("idAgendamento")) {
			return current.getIdAgendamento();
		} else if(nomeCampo.equals("nmUnidade")){
			return current.getNmUnidade();
		}else if(nomeCampo.equals("nmEspecialidade")){
			return current.getNmEspecialidade();
		}else if(nomeCampo.equals("nmMedico")){
			return current.getNmMedico();
		}else if(nomeCampo.equals("nmPaciente")){
			return current.getNmPaciente();
		}else if(nomeCampo.equals("nmSala")){
			return current.getNmSala();
		}else if(nomeCampo.equals("nmServico")){
			return current.getNmServico();	
		}else if(nomeCampo.equals("horaImpressao")){
			return current.getHoraImpressao();				
		}else if(nomeCampo.equals("nmTriagem")){
			return current.getNmTriagem();				
		}else if(nomeCampo.equals("nmPrioridade")){
			return current.getNmPrioridade();				
		}else if(nomeCampo.equals("idade")){
			return current.getIdade();				
		}else if(nomeCampo.equals("nmDescricao")){
			return current.getNmDescricao();				
		}else if(nomeCampo.equals("dtAgendamento")){
			return current.getDtAgendamento();			
		}else if(nomeCampo.equals("hrAgendamento")){
			return current.getHrAgendamento();
		}else if(nomeCampo.equals("codTitular")){
			return current.getCodTitular();			
		}else if(nomeCampo.equals("hrFim")){
			return current.getHrFim();			
		}else if(nomeCampo.equals("nmEndereco")){
			return current.getNmEndereco();			
		}else{
			return null;
		}
		
	}

}
