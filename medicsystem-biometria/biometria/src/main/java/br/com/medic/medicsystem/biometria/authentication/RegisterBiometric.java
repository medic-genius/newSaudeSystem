package br.com.medic.medicsystem.biometria.authentication;

import br.com.medic.medicsystem.biometria.dao.ClienteDAO;
import br.com.medic.medicsystem.biometria.enums.TipoPessoa;
import br.com.medic.medicsystem.biometria.model.Cliente;
import br.com.medic.medicsystem.biometria.model.Finger;

import com.nitgen.SDK.BSP.NBioBSPJNI;
import com.nitgen.SDK.BSP.NBioBSPJNI.FIR_TEXTENCODE;
import com.nitgen.SDK.BSP.NBioBSPJNI.INPUT_FIR;
import com.nitgen.SDK.BSP.NBioBSPJNI.IndexSearch;
import com.nitgen.SDK.BSP.NBioBSPJNI.IndexSearch.FP_INFO;

public class RegisterBiometric {

	private ClienteDAO clienteDAO;

	private NBioBSPJNI bsp;

	private NBioBSPJNI.IndexSearch indexSearchEngine;

	private NBioBSPJNI.FIR_TEXTENCODE textSavedFIRBD;

	private NBioBSPJNI.INPUT_FIR inputFIRBD;

	private NBioBSPJNI.INPUT_FIR inputFIRMC;

	private NBioBSPJNI.FIR_TEXTENCODE textSavedFIRMC;

	private NBioBSPJNI.IndexSearch.SAMPLE_INFO sampleInfo;

	private NBioBSPJNI.WINDOW_OPTION winOption;

	public RegisterBiometric() {

	}

	public void dispose() {

		if (indexSearchEngine != null) {
			indexSearchEngine.dispose();
			indexSearchEngine = null;
		}

		if (bsp != null) {
			bsp.CloseDevice();
			bsp.dispose();
			bsp = null;
		}

		if (textSavedFIRBD != null) {
			textSavedFIRBD = null;
		}

		if (textSavedFIRMC != null) {
			textSavedFIRMC = null;
		}

		if (inputFIRBD != null) {
			inputFIRBD = null;
		}

		if (inputFIRMC != null) {
			inputFIRMC = null;
		}

		if (sampleInfo != null) {
			sampleInfo = null;
		}

	}
	
	private int returnIdFing(String digital){
		
		if(digital != null){
			
			NBioBSPJNI bsp = new NBioBSPJNI();
			
			FIR_TEXTENCODE textSavedFIRBD = bsp.new FIR_TEXTENCODE();
			INPUT_FIR inputFIRBD = bsp.new INPUT_FIR();
			IndexSearch indexSearchEngine = bsp.new IndexSearch();
			NBioBSPJNI.IndexSearch.SAMPLE_INFO sampleInfo = null;
			
			textSavedFIRBD.TextFIR = digital;
			inputFIRBD.SetTextFIR(textSavedFIRBD);
			indexSearchEngine.AddFIR(inputFIRBD,1,sampleInfo);
			
			NBioBSPJNI.IndexSearch.FP_INFO fpInfo = indexSearchEngine.new FP_INFO();
			
			indexSearchEngine.Identify(inputFIRBD, 9, fpInfo);
			
			int  idFIR  = fpInfo.FingerID;
			
			return idFIR;
		}
		
		return 0;
		
	}

	private Finger captureAndSaveDigital() throws Exception {
		
		 this.bsp = new NBioBSPJNI();
		 NBioBSPJNI.INIT_INFO_0 initInfo0 = bsp.new INIT_INFO_0();
		 initInfo0.EnrollImageQuality = 90;
		 initInfo0.SecurityLevel =  NBioBSPJNI.FIR_SECURITY_LEVEL.HIGHER;
		 this.bsp.SetInitInfo(initInfo0);
		 
		 Finger finger = new Finger();

		if (bsp.IsErrorOccured()) {

			// TODO
			throw new Exception();
		}

		this.bsp.OpenDevice();

		NBioBSPJNI.FIR_HANDLE hCapture = this.bsp.new FIR_HANDLE();

		FIR_TEXTENCODE textSavedFIR;

		hCapture = bsp.new FIR_HANDLE();
		bsp.Enroll(null, hCapture, null, -1, null, winOption);

		textSavedFIR = bsp.new FIR_TEXTENCODE();
		bsp.GetTextFIRFromHandle(hCapture, textSavedFIR);
		
		finger.setDigital(textSavedFIR.TextFIR);
		finger.setIdDedoDigital(returnIdFing(textSavedFIR.TextFIR));

		return finger;
	}

	public Finger saveDigitalCliente(Integer id, TipoPessoa tipoPessoa)
			throws Exception {

		Cliente cliente = null;
		Cliente clienteAtualizado = null;

		clienteDAO = new ClienteDAO();
		
		if (id != null && tipoPessoa != null) {
			
			cliente = new Cliente();
			cliente.setId(id);
			cliente.setTipoPessoa(tipoPessoa);
			
//			Finger finger = new Finger();
			
			return  captureAndSaveDigital();

//			if(finger != null && finger.getDigital() != null){
//				cliente.setDigitalCliente(finger.getDigital());
//				cliente.setIdDedoDigital(finger.getIdDedoDigital());;
//				
//			}
//
//			if (cliente.getDigitalCliente() == null) {
//				return "Erro ao capturar Digital!";
//			}
//			
//			cliente.setTipoPessoa(tipoPessoa);
//			clienteAtualizado = clienteDAO.atualizarBiometria(cliente);
//		}
//
//		if (clienteAtualizado != null) {
//
//			return "Digital capturada com Sucesso!";
//		} else {
//
//			return "Erro ao capturar Digital!";
//		}
		}
		return new Finger();
	}
	
}
