package br.com.medic.medicsystem.biometria.jasper;

import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.biometria.model.TotemReceipt;

public class JRDataSourceTotemEncaminhamento  implements JRDataSource{
	private Iterator<TotemReceipt> it;
	private TotemReceipt current;
	private boolean goToNext = true;
	
	public JRDataSourceTotemEncaminhamento() {

	}
	
	public JRDataSourceTotemEncaminhamento(List<TotemReceipt> list) {
		this.it = list.iterator();
	}

	public boolean next() throws JRException {
		current = it.hasNext() ? it.next() : null;
		goToNext = (current != null);

		return goToNext;
	}

	public Object getFieldValue(JRField field) throws JRException {

		String nomeCampo  = field.getName();
		
		 if(nomeCampo.equals("nrEncaminhamento")){
			return current.getNrEncaminhamento();
		}else if(nomeCampo.equals("nmMedico")){
			return current.getNmMedico();
		}else if(nomeCampo.equals("nmPaciente")){
			return current.getNmPaciente();
		}else if(nomeCampo.equals("nmServico")){
			return current.getNmServico();	
		}else if(nomeCampo.equals("horaImpressao")){
			return current.getHoraImpressao();				
		}else if(nomeCampo.equals("nmRetorno")){
			return current.getNmRetorno();				
		}else if(nomeCampo.equals("nmTitular")){
			return current.getNmTitular();				
		}else if(nomeCampo.equals("nmUnidade")){
			return current.getNmUnidade();				
		}else{
			return null;
		}
		
	}
}
