package br.com.medic.medicsystem.biometria.enums;

public enum TipoPessoa {
	Cliente, Dependente, Funcionario
}
