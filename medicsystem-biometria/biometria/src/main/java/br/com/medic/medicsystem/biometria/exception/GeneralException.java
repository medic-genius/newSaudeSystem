package br.com.medic.medicsystem.biometria.exception;

public class GeneralException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GeneralException(String mensagem) {
		super(mensagem);
	}

	public GeneralException(String mensagem, Exception e) {
		super(mensagem, e);
	}

}
