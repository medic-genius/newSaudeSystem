package br.com.medic.medicsystem.biometria.authentication;

import java.util.ArrayList;
import java.util.List;

import br.com.medic.medicsystem.biometria.dao.ClienteDAO;
import br.com.medic.medicsystem.biometria.model.Cliente;

import com.nitgen.SDK.BSP.NBioBSPJNI;


public class AuthenticationBiometric {

	private ClienteDAO clienteDAO;
	
	private NBioBSPJNI bsp;
	
	private NBioBSPJNI.IndexSearch indexSearchEngine;
	
	private NBioBSPJNI.FIR_TEXTENCODE textSavedFIRBD;
	
	private NBioBSPJNI.INPUT_FIR inputFIRBD;
	
	private NBioBSPJNI.INPUT_FIR inputFIRMC;
	
	private NBioBSPJNI.FIR_TEXTENCODE textSavedFIRMC;
	
	private NBioBSPJNI.IndexSearch.SAMPLE_INFO sampleInfo;
	
	private NBioBSPJNI.WINDOW_OPTION winOption;
	
	public AuthenticationBiometric() {
		
	}
    
	public void dispose() {
        
		if (indexSearchEngine != null) {
            indexSearchEngine.dispose();
            indexSearchEngine = null;
        }
        
        if (bsp != null) {
            bsp.CloseDevice();
            bsp.dispose();
            bsp = null;
        }
        

        if (textSavedFIRBD != null) {
            textSavedFIRBD = null;
        }
        
        if (textSavedFIRMC != null) {
            textSavedFIRMC = null;
        }

        if (inputFIRBD != null) {
            inputFIRBD = null;
        }
        
        if (inputFIRMC != null) {
            inputFIRMC = null;
        }

        if (sampleInfo != null) {
            sampleInfo = null;
        }

    }
	
	private void populateIndexEngine() {
		
		//Melhorar para trazer somente os clientes com atendimento no dia?
		clienteDAO = new ClienteDAO();
		List<Cliente> listaCliente = clienteDAO.getAllClientes();
		
		// Instancia o Handle para receber a digital
		this.textSavedFIRBD = bsp.new FIR_TEXTENCODE();
		this.inputFIRBD = bsp.new INPUT_FIR();

		// Percorre a lista de Digitais e popula a SerchEngine com o ID do
		// Cliente
		for (Cliente cliente : listaCliente) {
			this.textSavedFIRBD.TextFIR = cliente.getDigitalCliente();
			this.inputFIRBD.SetTextFIR(this.textSavedFIRBD);
			this.indexSearchEngine.AddFIR(this.inputFIRBD,
					Integer.parseInt(String.valueOf(cliente.getId())),
					this.sampleInfo);
		}
		
	}
	
	private void populateIndexEngineOnClient(Cliente client) {
		
		//Melhorar para trazer somente os clientes com atendimento no dia?
		clienteDAO = new ClienteDAO();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		listaCliente.add(client);
		
		// Instancia o Handle para receber a digital
		this.textSavedFIRBD = bsp.new FIR_TEXTENCODE();
		this.inputFIRBD = bsp.new INPUT_FIR();

		// Percorre a lista de Digitais e popula a SerchEngine com o ID do
		// Cliente
		for (Cliente cliente : listaCliente) {
			this.textSavedFIRBD.TextFIR = cliente.getDigitalCliente();
			this.inputFIRBD.SetTextFIR(this.textSavedFIRBD);
			this.indexSearchEngine.AddFIR(this.inputFIRBD,
					Integer.parseInt(String.valueOf(cliente.getId())),
					this.sampleInfo);
		}
		
	}

	private String verificaDigital() {
		
		NBioBSPJNI.FIR_HANDLE hCapture = bsp.new FIR_HANDLE();
		
		this.winOption = bsp.new WINDOW_OPTION();

		this.winOption.WindowStyle = NBioBSPJNI.WINDOW_STYLE.INVISIBLE;
		
		bsp.Capture(NBioBSPJNI.FIR_PURPOSE.IDENTIFY, hCapture, -1, null,
				this.winOption);
	
		NBioBSPJNI.INPUT_FIR inputFIR1;
		inputFIR1 = bsp.new INPUT_FIR();
		inputFIR1.SetFIRHandle(hCapture); 
		
		NBioBSPJNI.IndexSearch.FP_INFO fpInfo = indexSearchEngine.new FP_INFO();
		 
		indexSearchEngine.Identify(inputFIR1, 5, fpInfo);
		
        String id = String.valueOf(fpInfo.ID);
        
        if (id.equals("0")) {
        	id = "Digital invalida - Ler Novamente";
        }

        return id;
	}

	public String getIdClienteDigital() throws Exception {
		
		this.bsp = new NBioBSPJNI();
		
		if(bsp.IsErrorOccured()){
			
			//TODO 
			throw new Exception();
		}
		
		this.indexSearchEngine = bsp.new IndexSearch();
		
		if(this.bsp.IsErrorOccured()){
			
			//TODO 
			throw new Exception();
		}
		
		this.textSavedFIRMC = null;

		this.inputFIRBD = null;
		this.inputFIRMC = null;
		this.sampleInfo = null;
		
		this.bsp.OpenDevice();
	
		//Populated IndexEngine
		populateIndexEngine();
		
		return verificaDigital();
	}
	
public String getIdClienteDigitalOnClient(Cliente client) throws Exception {
		
		this.bsp = new NBioBSPJNI();
		
		if(bsp.IsErrorOccured()){
			
			//TODO 
			throw new Exception();
		}
		
		this.indexSearchEngine = bsp.new IndexSearch();
		
		if(this.bsp.IsErrorOccured()){
			
			//TODO 
			throw new Exception();
		}
		
		this.textSavedFIRMC = null;

		this.inputFIRBD = null;
		this.inputFIRMC = null;
		this.sampleInfo = null;
		
		this.bsp.OpenDevice();
	
		//Populated IndexEngine
		populateIndexEngineOnClient(client);
		
		return verificaDigital();
	}
}
