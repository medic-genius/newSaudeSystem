package br.com.medic.medicsystem.biometria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.biometria.enums.TipoPessoa;
import br.com.medic.medicsystem.biometria.exception.GeneralException;
import br.com.medic.medicsystem.biometria.model.Cliente;

public class ClienteDAO implements IGenericDao<Cliente> {

	public List<Cliente> getAllClientes() {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Cliente> clientes = new ArrayList<Cliente>();
		Cliente cliente;
		String queryListarTodosClientes = null;
		
		try {
			
			conn = ConnectionManager.getConexao();
			stmt = conn.createStatement();
			
			//Obtem os clientes
			queryListarTodosClientes = "SELECT idcliente, nrcodcliente, nmcliente, digitalcliente "
					+ " FROM realvida.tbcliente "
					+ " WHERE digitalcliente is not null "
					+ " ORDER BY dtatualizacaolog DESC ";
			rs = stmt.executeQuery(queryListarTodosClientes);
			
			while (rs.next()) {

				cliente = new Cliente(rs.getInt("idcliente"),
						rs.getString("nrcodcliente"),
						rs.getString("nmcliente"),
						rs.getString("digitalcliente"), null,
						TipoPessoa.Cliente);
				clientes.add(cliente);
			}
			rs.close();
			
			//Obtem os dependentes
			stmt = conn.createStatement();
			queryListarTodosClientes = "SELECT idcliente, nrcodcliente, nmdependente, digitaldependente "
					+ "from realvida.tbdependente "
					+ "WHERE digitaldependente is not null  and dtexclusao is null";
			rs = stmt.executeQuery(queryListarTodosClientes);
			while (rs.next()) {
				cliente = new Cliente(rs.getInt("idcliente"), rs
						.getString("nrcodcliente"), rs
						.getString("nmdependente"), rs
						.getString("digitaldependente"), null,
						TipoPessoa.Dependente);
				clientes.add(cliente);
			}
			rs.close();
			
		} catch (GeneralException e) {

			e.printStackTrace();
		} catch (SQLException e){
			
			e.printStackTrace();
		} finally {

			ConnectionManager.closeAll(conn);
		}
		
		return clientes;
	}
	
	public Cliente getCliente(Integer id, TipoPessoa tipoPessoa) throws GeneralException {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Cliente cliente = null;
		String queryGetCliente = null;
		
		if (tipoPessoa == TipoPessoa.Cliente) {

			queryGetCliente = "SELECT idcliente, nrcodcliente, nmcliente, digitalcliente "
					+ " FROM realvida.tbcliente cli"
					+ " WHERE cli.idcliente = ?" 
					+ " ORDER BY dtatualizacaolog DESC ";
			} else {
				if (tipoPessoa == TipoPessoa.Dependente) {

					queryGetCliente = "SELECT iddependente, nrcodcliente, nmdependente, digitaldependente "
							+ " from realvida.tbdependente dep"
							+ " WHERE dep.iddependente = ?" ;
				}
			}

		try {
			
			conn = ConnectionManager.getConexao();

			stmt = conn.prepareStatement(queryGetCliente);
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			
			String nmIdTipoPessoa = null;
			String nmNrCodigoClienteTipoPessoa = "nrcodcliente";
			String nmTipoPessoa = null;
			String nmDigitalTipoPessoa = null;
						
			while (rs.next()) {
				
				if (tipoPessoa == TipoPessoa.Cliente) {
					
					nmIdTipoPessoa = "idcliente";
					nmTipoPessoa = "nmcliente";
					nmDigitalTipoPessoa = "digitalcliente";
				} else {
					
					nmIdTipoPessoa = "iddependente";
					nmTipoPessoa = "nmdependente";
					nmDigitalTipoPessoa = "digitaldependente";
				}

				cliente = new Cliente(rs.getInt(nmIdTipoPessoa),
						rs.getString(nmNrCodigoClienteTipoPessoa),
						rs.getString(nmTipoPessoa),
						rs.getString(nmDigitalTipoPessoa), null,
						tipoPessoa);
			}
			rs.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionManager.closeAll(conn);
		}
		
		return cliente;
	}
	
	public Cliente atualizarBiometria(Cliente cliente) throws GeneralException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String ATUALIZAR_BIOMETRIA = null;

		if (cliente.getTipoPessoa() == TipoPessoa.Cliente) {
			ATUALIZAR_BIOMETRIA = "UPDATE realvida.tbcliente set digitalcliente = ?, iddedodigital = ? "
					+ " where idcliente = ? ";
		} else if (cliente.getTipoPessoa() == TipoPessoa.Dependente) {
			// incluido pelo telis em 27/07/2015
			ATUALIZAR_BIOMETRIA = "UPDATE realvida.tbdependente set digitalDependente = ?, iddedodigital = ? "
					+ " where iddependente = ? ";
		} else if (cliente.getTipoPessoa() == TipoPessoa.Funcionario) {
			// incluido pelo telis em 27/07/2015
			ATUALIZAR_BIOMETRIA = "UPDATE realvida.tbfuncionario set digitalfuncionario = ?, iddedodigital = ? "
					+ " where idfuncionario = ? ";
		}

		try {
			if (cliente != null) {
				conn = ConnectionManager.getConexao();
				stmt = conn.prepareStatement(ATUALIZAR_BIOMETRIA);
				stmt.setString(1, cliente.getDigitalCliente());
				stmt.setInt(2, cliente.getIdDedoDigital());
				stmt.setInt(3, cliente.getId());
				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionManager.closeAll(conn);
		}
		
		return cliente;
	}

	public void atualizarFoto(Cliente cliente) throws GeneralException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String ATUALIZAR_FOTO = null;
		
		byte[] encodedBytes = Base64.encodeBase64(cliente.getFotoCliente());
		
		if (cliente.getTipoPessoa() == TipoPessoa.Cliente) {
			ATUALIZAR_FOTO = "UPDATE realvida.tbcliente "
					+ " set fotocliente = ?  where idcliente = ? ";
		} else if (cliente.getTipoPessoa() == TipoPessoa.Dependente) {
			ATUALIZAR_FOTO = "UPDATE realvida.tbdependente "
					+ " set fotodependente = ?  where iddependente = ? ";
		} else if (cliente.getTipoPessoa() == TipoPessoa.Funcionario) {
			ATUALIZAR_FOTO = "UPDATE realvida.tbfuncionario "
					+ "set fotofuncionario = ?  where idfuncionario = ? ";
		}

		try {
			if (cliente != null) {
				conn = ConnectionManager.getConexao();
				stmt = conn.prepareStatement(ATUALIZAR_FOTO);
				stmt.setBytes(1, encodedBytes);
				stmt.setInt(2, cliente.getId());
				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeneralException("A foto não foi salva!");
		} finally {
			ConnectionManager.closeAll(conn);
		}
	}
	
}
