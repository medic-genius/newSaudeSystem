package br.com.medic.medicsystem.biometria.socket;

import br.com.medic.medicsystem.biometria.authentication.AuthenticationBiometric;
import br.com.medic.medicsystem.biometria.authentication.RegisterBiometric;
import br.com.medic.medicsystem.biometria.jasper.TotemReceiptPrinter;
import br.com.medic.medicsystem.biometria.model.Cliente;
import br.com.medic.medicsystem.biometria.model.TotemReceipt;

import com.corundumstudio.socketio.AckCallback;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;

public class BiometriaSocketLauncher {

	public static void main(String[] args) throws InterruptedException {

		Configuration config = new Configuration();
		config.setHostname("localhost");
		config.setPort(9092);

		final SocketIOServer server = new SocketIOServer(config);

		server.start();

		server.addEventListener("biometriapresentevent",
				Cliente.class,
				new DataListener<Cliente>() {

			public void onData(SocketIOClient client,
					final Cliente data, AckRequest ackRequest)
					throws Exception {

						AuthenticationBiometric authenticationBiometricData = new AuthenticationBiometric();

						client.sendEvent("biometriapresentevent",
								new AckCallback<String>(String.class) {
									@Override
									public void onSuccess(String result) {
										try {

											System.out.println(data
													.getId());
										} catch (Exception e) {

											e.printStackTrace();
										}
									}
								}, authenticationBiometricData
										.getIdClienteDigitalOnClient(data));
					}
				});

		server.addEventListener("biometriaevent",
				AuthenticationBiometric.class,
				new DataListener<AuthenticationBiometric>() {

					public void onData(SocketIOClient client,
							final AuthenticationBiometric data,
							AckRequest ackRequest) throws Exception {

						AuthenticationBiometric authenticationBiometricData = new AuthenticationBiometric();

						client.sendEvent("biometriaevent",
								new AckCallback<String>(String.class) {
									@Override
									public void onSuccess(String result) {
										try {

											System.out.println(data
													.getIdClienteDigital());
										} catch (Exception e) {

											e.printStackTrace();
										}
									}
								}, authenticationBiometricData
										.getIdClienteDigital());
					}
				});

		server.addEventListener("biometriaregisterevent", Cliente.class,
				new DataListener<Cliente>() {

					public void onData(SocketIOClient client,
							final Cliente data, AckRequest ackRequest)
							throws Exception {

						RegisterBiometric registerBiometricData = new RegisterBiometric();

						client.sendEvent(
								"biometriaregisterevent",
								new AckCallback<String>(String.class) {
									@Override
									public void onSuccess(String result) {
										try {

											System.out.println(data.getId());
										} catch (Exception e) {

											e.printStackTrace();
										}
									}
								},
								registerBiometricData.saveDigitalCliente(
										data.getId(), data.getTipoPessoa()));
					}
				});
		
		server.addEventListener("printerAgendamento", TotemReceipt.class,
				new DataListener<TotemReceipt>() {

					public void onData(SocketIOClient client,
							final TotemReceipt totemReceipt, AckRequest ackRequest)
							throws Exception {

						TotemReceiptPrinter totemReceiptPrinter = new TotemReceiptPrinter();

						client.sendEvent(
								"printreceipt",
								new AckCallback<String>(String.class) {
									@Override
									public void onSuccess(String result) {
										try {

											System.out.println("impresso");
										} catch (Exception e) {
											System.out.println("erro");
											e.printStackTrace();
										}
									}
								},
								
								totemReceiptPrinter.printTotemReceipt(totemReceipt)	);
					}
				});
		
		server.addEventListener("printPresencaReceipt", TotemReceipt.class,
				new DataListener<TotemReceipt>() {

			public void onData(SocketIOClient client,
					final TotemReceipt totemReceipt, AckRequest ackRequest)
							throws Exception {

				TotemReceiptPrinter totemReceiptPrinter = new TotemReceiptPrinter();

				client.sendEvent(
						"printPresencaReceipt",
						new AckCallback<String>(String.class) {
							@Override
							public void onSuccess(String result) {
								try {

									System.out.println("impresso");
								} catch (Exception e) {
									System.out.println("erro");
									e.printStackTrace();
								}
							}
						},

						totemReceiptPrinter.printPresencaReceipt(totemReceipt)
				);
			}
		});

		Thread.sleep(Integer.MAX_VALUE);

		server.stop();
	}

}
