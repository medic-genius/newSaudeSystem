package br.com.medic.medicsystem.biometria.model;

import br.com.medic.medicsystem.biometria.enums.TipoPessoa;

import com.towel.el.annotation.Resolvable;

public class Cliente implements BaseModel, Comparable<Cliente> {
	
	@Resolvable(colName = "Id")
	private int id;	
	
	@Resolvable(colName = "Codigo")
	private String nrcodigo;
	
	@Resolvable(colName = "Nome")
	private String nome;
	
	@Resolvable(colName = "Biometria")
	private String digitalCliente;
	
	@Resolvable(colName = "Foto")
	private byte[] fotoCliente;
	
	@Resolvable(colName = "Tipo de Cadastro")
	private TipoPessoa tipoPessoa;
	
	@Resolvable(colName = "iddedodigital")
	private int idDedoDigital;

	public Cliente(int id, String nrcodigo, String nome, String digitalCliente, byte[] foto, TipoPessoa  tipoPessoa) {
		this.id = id;
		this.nrcodigo = nrcodigo;
		this.nome = nome;
		this.digitalCliente = digitalCliente;
		this.fotoCliente = foto;
		this.tipoPessoa = tipoPessoa;
	}

	public Cliente() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNrcodigo() {
		return nrcodigo;
	}

	public void setNrcodigo(String nrcodigo) {
		this.nrcodigo = nrcodigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDigitalCliente() {
		return digitalCliente;
	}

	public void setDigitalCliente(String digitalCliente) {
		this.digitalCliente = digitalCliente;
	}

	public byte[] getFotoCliente() {
		return fotoCliente;
	}

	public void setFotoCliente(byte[] fotoCliente) {
		this.fotoCliente = fotoCliente;
	}

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public int getIdDedoDigital() {
		return idDedoDigital;
	}

	public void setIdDedoDigital(int idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}

	public int compareTo(Cliente o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
