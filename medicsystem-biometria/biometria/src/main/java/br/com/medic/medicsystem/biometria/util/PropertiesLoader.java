package br.com.medic.medicsystem.biometria.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.medic.medicsystem.biometria.exception.PropertiesLoaderException;

public class PropertiesLoader {

	/** ourInstance */
	private static PropertiesLoader ourInstance = new PropertiesLoader();

	/** logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);

	/**
	 * 
	 * Public static factory method to get an instance of the PropertiesLoader.
	 * 
	 * @return A PropertiesLoader instance.
	 */
	public static PropertiesLoader getInstance() {
		return ourInstance;
	}

	/**
	 * 
	 * Loads a Properties object resolving the properties file location
	 * according to its classpath location. See the project setup documentation
	 * in order to understand how to configure the properties file.
	 * 
	 * @param fileName
	 *            The file name of the properties file.
	 * @return A Properties instance referring to the respective fileName
	 *         properties file.
	 */
	public Properties load(String fileName) {
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException("The fileName cannot be null at this point.");
		}

		Properties props = new Properties();

		String propertiesFilePath = fileName;
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(propertiesFilePath);

		LOGGER.debug(String.format("Loading properties in classpath from filePath: %s", propertiesFilePath));

		if (in != null) {
			try {
				props.load(in);
			} catch (IOException e) {
				final String exceptionMsg = 
						String.format("Exception while loading properties file from the classpath. %s not found in the classpath.",
								propertiesFilePath);
				throw new PropertiesLoaderException(exceptionMsg, e);
			}
		} else {
			LOGGER.debug(String
					.format("Warning that the properties file from the classpath %s is not present. See the JBOSS configuration.",
							propertiesFilePath));
			LOGGER.debug(String
					.format("Looking up EAR for the default properties. The file is %s.", fileName));
			in = this.getClass().getClassLoader().getResourceAsStream(fileName);
			if (in != null) {
				try {
					props.load(in);
				} catch (IOException e1) {
					final String exceptionMsg = 
							String.format("Exception while loading properties file from the classpath. %s not found in the classpath.",
									fileName);
					throw new PropertiesLoaderException(exceptionMsg, e1);
				}
			} else {
				final String exceptionMsg = 
						String.format("Exception while loading properties file from the classpath. %s not found in the classpath.",
								fileName);
				throw new PropertiesLoaderException(exceptionMsg);
			}
		}

		return props;
	}
}