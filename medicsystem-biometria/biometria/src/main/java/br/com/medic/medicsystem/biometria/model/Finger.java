package br.com.medic.medicsystem.biometria.model;

public class Finger {
	
	private int idDedoDigital;
	private String digital;
	
	
	
	public int getIdDedoDigital() {
		return idDedoDigital;
	}
	public void setIdDedoDigital(int idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}
	public String getDigital() {
		return digital;
	}
	public void setDigital(String digital) {
		this.digital = digital;
	}
	
	
	
}
