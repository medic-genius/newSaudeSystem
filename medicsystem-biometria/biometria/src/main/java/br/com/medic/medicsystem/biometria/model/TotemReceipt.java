package br.com.medic.medicsystem.biometria.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.towel.el.annotation.Resolvable;

public class TotemReceipt implements BaseModel, Comparable<Cliente>{
	@Resolvable(colName="idAgendamento")
	private Long idAgendamento;

	@Resolvable(colName = "nmUnidade")
	private String nmUnidade;

	@Resolvable(colName = "nmPaciente")
	private String nmPaciente;
	
	@Resolvable(colName = "nmEspecialidade")
	private String nmEspecialidade;
	
	@Resolvable(colName = "nmMedico")
	private String nmMedico;

	@Resolvable(colName = "nmServico")
	private String nmServico;

	@Resolvable(colName = "nmSala")
	private String nmSala;
	
	@Resolvable(colName = "nmTriagem")
	private String nmTriagem;
	
	@Resolvable(colName = "nmTitular")
	private String nmTitular;
	
	@Resolvable(colName = "nrEncaminhamento")
	private String nrEncaminhamento;
	
	@Resolvable(colName = "nmRetorno")
	private String nmRetorno;
	
	@Resolvable(colName = "nmPrioridade")
	private String nmPrioridade;
	
	@Resolvable(colName = "idade")
	private Integer idade;
	
	@Resolvable(colName = "dtAgendamento")
	private String dtAgendamento;
	
	@Resolvable(colName = "nmDescricao")
	private String nmDescricao;
	
	@Resolvable(colName = "hrAgendamento")
	private String hrAgendamento;
	
	@Resolvable(colName = "codTitular")
	private String codTitular;
	
	@Resolvable(colName = "hrFim")
	private String hrFim;
	
	@Resolvable(colName = "nmEndereco")
	private String nmEndereco;
	
	public Long getIdAgendamento() {
		return idAgendamento;
	}
	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	public String getNmUnidade() {
		return nmUnidade;
	}
	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}
	public String getNmPaciente() {
		return nmPaciente;
	}
	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	public String getNmMedico() {
		return nmMedico;
	}
	public void setNmMedico(String nmMedico) {
		this.nmMedico = nmMedico;
	}
	public String getNmServico() {
		return nmServico;
	}
	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}
	public String getNmSala() {
		return nmSala;
	}
	public void setNmSala(String nmSala) {
		this.nmSala = nmSala;
	}
	public String getNmTriagem() {
		return nmTriagem;
	}
	public void setNmTriagem(String nmTriagem) {
		this.nmTriagem = nmTriagem;
	}
	public String getHoraImpressao() {
		SimpleDateFormat formatter = new SimpleDateFormat();
        formatter.applyPattern("dd/MM/yyyy HH:mm:ss");
        return formatter.format(new Date());
	}
	
	public String getNmTitular() {
		return nmTitular;
	}
	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}
	public String getNrEncaminhamento() {
		return nrEncaminhamento;
	}
	public void setNrEncaminhamento(String nrEncaminhamento) {
		this.nrEncaminhamento = nrEncaminhamento;
	}
	public String getNmRetorno() {
		return nmRetorno;
	}
	public void setNmRetorno(String nmRetorno) {
		this.nmRetorno = nmRetorno;
	}
	
	public String getNmPrioridade() {
		return nmPrioridade;
	}
	public void setNmPrioridade(String nmPrioridade) {
		this.nmPrioridade = nmPrioridade;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	
	public String getDtAgendamento() {
		return dtAgendamento;
	}
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}
	public String getNmDescricao() {
		return nmDescricao;
	}
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	
	public String getHrAgendamento() {
		return hrAgendamento;
	}
	public void setHrAgendamento(String hrAgendamento) {
		this.hrAgendamento = hrAgendamento;
	}
	
	public String getCodTitular() {
		return codTitular;
	}
	public void setCodTitular(String codTitular) {
		this.codTitular = codTitular;
	}
	
	public String getHrFim() {
		return hrFim;
	}
	public void setHrFim(String hrFim) {
		this.hrFim = hrFim;
	}
	
	public String getNmEndereco() {
		return nmEndereco;
	}
	public void setNmEndereco(String nmEndereco) {
		this.nmEndereco = nmEndereco;
	}
	public int compareTo(Cliente o) {
		return 0;
	}
}
