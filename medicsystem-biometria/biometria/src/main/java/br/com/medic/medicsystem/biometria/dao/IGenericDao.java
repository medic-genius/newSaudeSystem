package br.com.medic.medicsystem.biometria.dao;

import java.util.List;

import br.com.medic.medicsystem.biometria.model.Cliente;

public interface IGenericDao<T> {
	
	public List<Cliente> getAllClientes();
}
