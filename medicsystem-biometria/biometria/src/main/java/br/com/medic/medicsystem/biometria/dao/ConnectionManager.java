package br.com.medic.medicsystem.biometria.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.swing.JOptionPane;

import br.com.medic.medicsystem.biometria.exception.GeneralException;
import br.com.medic.medicsystem.biometria.util.PropertiesLoader;

public class ConnectionManager {
	public static Connection getConexao() throws GeneralException {

		Connection conn = null;
		
		try {
			Properties props = PropertiesLoader.getInstance().load(
			        "config.properties");
			String STR_CON = props.getProperty("STR_CON")
					+ props.getProperty("IP") + ":" + props.getProperty("PORT")
					+ "/" + props.getProperty("DATABASE");
			Class.forName(props.getProperty("STR_DRIVER"));
			conn = DriverManager.getConnection(STR_CON,
					props.getProperty("USER"), props.getProperty("PASSWORD"));
			return conn;
		} catch (ClassNotFoundException e) {
			String errorMessage = "Driver nao foi encontrado";
			throw new GeneralException(errorMessage, e);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao conectar com o Banco de dados - Network");
			System.exit(0);
			throw new GeneralException("Erro ao obter a conexao", e);
		}
	}
	
	public static void closeAll(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
