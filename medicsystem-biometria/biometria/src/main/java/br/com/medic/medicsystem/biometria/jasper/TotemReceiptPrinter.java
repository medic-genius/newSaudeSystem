package br.com.medic.medicsystem.biometria.jasper;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleGraphics2DExporterOutput;
import net.sf.jasperreports.export.SimpleGraphics2DReportConfiguration;
import br.com.medic.medicsystem.biometria.model.TotemReceipt;

public class TotemReceiptPrinter implements Printable{
	
	 private  PrintService impressora;
	 private JasperReportsContext jasperReportsContext = DefaultJasperReportsContext.getInstance();
	 private JasperPrint jasperPrint;
	 private int pageOffset;
		
	public void printTotemReceipt(JRDataSourceTotemReceipt jrDataSourceAvaliacaoFisica, TotemReceipt totem) throws Exception{
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logomarca", getClass().getResourceAsStream("/mais_consulta_logo.png"));

		InputStream jrxmlCapa = getClass().getResourceAsStream("/ComprovanteAgendamento.jrxml");
		
		JasperReport reportCapa = JasperCompileManager.compileReport(jrxmlCapa);
		JasperPrint jasperPrintCapa = JasperFillManager.fillReport(reportCapa, params, jrDataSourceAvaliacaoFisica);
		
		DocFlavor df = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
        PrintService[] ps = PrintServiceLookup.lookupPrintServices(df, null);
        
        for (PrintService p: ps) {
        	if(p.getName().contains("MP-4200 TH") || p.getName().contains("mp-4200 th") || p.getName().contains("Foxit Reader PDF Printer")){
        		impressora = p;
        	}
        	
        	System.out.println("Impressora: "+p.getName());
        }
        
        this.jasperPrint = jasperPrintCapa;
        
        printPages(0, jasperPrintCapa.getPages().size() - 1, false);		
	}
	
	public boolean printPages(
			int firstPageIndex,
			int lastPageIndex,
			boolean withPrintDialog
			) throws JRException
		{
			boolean isOK = true;

			if (
				firstPageIndex < 0 ||
				firstPageIndex > lastPageIndex ||
				lastPageIndex >= jasperPrint.getPages().size()
				)
			{
				throw 
					new JRException(
						"paginas invalidas",  
						new Object[]{firstPageIndex, lastPageIndex, jasperPrint.getPages().size()}
						);
			}

			pageOffset = firstPageIndex;

			PrinterJob printJob = PrinterJob.getPrinterJob();
			
			try {
				printJob.setPrintService(impressora);
			} catch (PrinterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// fix for bug ID 6255588 from Sun bug database
			
			PageFormat pageFormat = printJob.defaultPage();
			Paper paper = pageFormat.getPaper();

			printJob.setJobName("JasperReports - " + jasperPrint.getName());
			
			switch (jasperPrint.getOrientationValue())
			{
				case LANDSCAPE :
				{
					pageFormat.setOrientation(PageFormat.LANDSCAPE);
					paper.setSize(jasperPrint.getPageHeight(), jasperPrint.getPageWidth());
					paper.setImageableArea(
						0,
						0,
						jasperPrint.getPageHeight(),
						jasperPrint.getPageWidth()
						);
					break;
				}
				case 
				PORTRAIT :
				default :
				{
					pageFormat.setOrientation(PageFormat.PORTRAIT);
					paper.setSize(jasperPrint.getPageWidth(), jasperPrint.getPageHeight());
					paper.setImageableArea(
						0,
						0,
						jasperPrint.getPageWidth(),
						jasperPrint.getPageHeight()
						);
				}
			}

			pageFormat.setPaper(paper);

			Book book = new Book();
			book.append(this, pageFormat, lastPageIndex - firstPageIndex + 1);
			printJob.setPageable(book);
			try
			{
				if (withPrintDialog)
				{
					if (printJob.printDialog())
					{
						printJob.print();
					}
					else
					{
						isOK = false;
					}
				}
				else
				{
					printJob.print();
				}
			}
			catch (Exception ex)
			{
				throw 
					new JRException(
						"Erro",
						null, 
						ex);
			}

			return isOK;
		}
	
	
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException
	{
		if (Thread.interrupted())
		{
			throw new PrinterException("Current thread interrupted.");
		}

		pageIndex += pageOffset;

		if ( pageIndex < 0 || pageIndex >= jasperPrint.getPages().size() )
		{
			return Printable.NO_SUCH_PAGE;
		}

		try
		{
			JRGraphics2DExporter exporter = new JRGraphics2DExporter(jasperReportsContext);
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			SimpleGraphics2DExporterOutput output = new SimpleGraphics2DExporterOutput();
			output.setGraphics2D((Graphics2D)graphics);
			exporter.setExporterOutput(output);
			SimpleGraphics2DReportConfiguration configuration = new SimpleGraphics2DReportConfiguration();
			configuration.setPageIndex(pageIndex);
			exporter.setConfiguration(configuration);
			exporter.exportReport();
		}
		catch (JRException e)
		{
			

			throw new PrinterException(e.getMessage()); //NOPMD
		}

		return Printable.PAGE_EXISTS;
	}
	
	
	public String printTotemReceipt(TotemReceipt totem) throws Exception{
		try{
			List<TotemReceipt> list = new ArrayList<TotemReceipt>();
			
			list.add(totem);
			
			JRDataSourceTotemReceipt dataSource = new JRDataSourceTotemReceipt(list);
			printTotemReceipt(dataSource, totem);
			return "Agendamento impresso!";
		} catch (Exception ex){
			ex.printStackTrace();
			throw new Exception(ex);
		}
	}
	
//	public void printTotemReceiptEncaminhamento(JRDataSourceTotemEncaminhamento jrDataSourceAvaliacaoFisica) throws Exception{
//		HashMap<String, Object> params = new HashMap<String, Object>();
//		params.put("logomarca", getClass().getResourceAsStream("/LOGO-DR+CONSULTA.png"));
//
//		InputStream jrxmlCapa = getClass().getResourceAsStream("/ComprovanteEncaminhamento.jrxml");
//		
//		JasperReport reportCapa = JasperCompileManager.compileReport(jrxmlCapa);
//		JasperPrint jasperPrintCapa = JasperFillManager.fillReport(reportCapa, params, jrDataSourceAvaliacaoFisica);
//		
//		JasperPrintManager.printReport(jasperPrintCapa, false);
//		
//	}
//	
//	public String printTotemReceiptEncaminhamento(TotemReceipt totem) throws Exception{
//		try{
//			List<TotemReceipt> list = new ArrayList<TotemReceipt>();
//			list.add(totem);
//			
//			JRDataSourceTotemEncaminhamento dataSource = new JRDataSourceTotemEncaminhamento(list);
//			printTotemReceiptEncaminhamento(dataSource);
//			return "Encaminhamento impresso!";
//		} catch (Exception ex){
//			ex.printStackTrace();
//			throw new Exception(ex);
//		}
//	}
	
	// Adicionado por Patrick Lima em 2017-06-01
	public void printPresencaReceipt(JRDataSourceTotemReceipt jrDataSourceAvaliacaoFisica, TotemReceipt totem) throws Exception{
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logomarca", getClass().getResourceAsStream("/mais_consulta_logo.png"));

		InputStream jrxmlCapa = getClass().getResourceAsStream("/TreinoPrincipal.jrxml");
		
		JasperReport reportCapa = JasperCompileManager.compileReport(jrxmlCapa);
		JasperPrint jasperPrintCapa = JasperFillManager.fillReport(reportCapa, params, jrDataSourceAvaliacaoFisica);
		
		JasperPrintManager.printReport(jasperPrintCapa, false);
		
		if(totem.getNrEncaminhamento() != null){
			
			printTotemReceiptEncaminhamento(totem);
		}
		
	}
	
	
	public String printPresencaReceipt(TotemReceipt totem) throws Exception{
		try{
			List<TotemReceipt> list = new ArrayList<TotemReceipt>();
			list.add(totem);
			
			JRDataSourceTotemReceipt dataSource = new JRDataSourceTotemReceipt(list);
			printPresencaReceipt(dataSource, totem);
			return "Agendamento impresso!";
		} catch (Exception ex){
			ex.printStackTrace();
			throw new Exception(ex);
		}
	}
	
	public void printTotemReceiptEncaminhamento(JRDataSourceTotemEncaminhamento jrDataSourceAvaliacaoFisica) throws Exception{
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logomarca", getClass().getResourceAsStream("/mais_consulta_logo.png"));

		InputStream jrxmlCapa = getClass().getResourceAsStream("/ComprovanteEncaminhamento.jrxml");
		
		JasperReport reportCapa = JasperCompileManager.compileReport(jrxmlCapa);
		JasperPrint jasperPrintCapa = JasperFillManager.fillReport(reportCapa, params, jrDataSourceAvaliacaoFisica);
		
		JasperPrintManager.printReport(jasperPrintCapa, false);
		
	}
	
	public String printTotemReceiptEncaminhamento(TotemReceipt totem) throws Exception{
		try{
			List<TotemReceipt> list = new ArrayList<TotemReceipt>();
			list.add(totem);
			
			JRDataSourceTotemEncaminhamento dataSource = new JRDataSourceTotemEncaminhamento(list);
			printTotemReceiptEncaminhamento(dataSource);
			return "Encaminhamento impresso!";
		} catch (Exception ex){
			ex.printStackTrace();
			throw new Exception(ex);
		}
	}

	
}
