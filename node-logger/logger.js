/*
  @license
*/

"use strict";

/*
  logger.info() works like console.log(). logger has log levels silly, debug,
  verbose, info, warn and error, with matching functions. So you can do for example
    logger.debug("foo", {bar: 42}).

  To add fields to logger server events, you can use logger.infoWithMeta():
     logger.infoWithMeta("foo", {stuffThatIsAddedToMessage: 42}, {someMetaInfo: 1});

  To add fields to all logger server events, you can use logger.globalMeta:
     logger.globalMeta = {someGlobalMetaInfo: 1};

 Properties from last object given as arguments are translated to fields.
  All levels are available in WithMeta variants.
  logger can not handle Error object as meta, or as part of meta.

  Logger has properties that can be used for checking
  if message on given log level will be printed. They're called:
    logger.<level>IsEnabled

  This Logger instance exits the process in case of exceptions, which would
  bring the process down. This behavior can be disabled with:
    logger.exitOnError = false;
  This is useful in conjunction with test suites.

  This modules is configured with environment variables:
    MEDIC_LOG_LEVEL
    MEDIC_LOG_FILE
    MEDIC_LOG_SHORT - If not logging to file, use short prefix format
    MEDIC_LOG_PROCESS_TITLE - If not logging to file and not using short format,
                              log also process title.
  */

var winston = require("winston");

var transports = [];

var level = process.env.MEDIC_LOG_LEVEL || "info";

if (process.env.MEDIC_LOG_FILE) {
    transports.push(new winston.transports.File({
        filename: process.env.MEDIC_LOG_FILE,
        // log level set in Logger options does not work.
        // Each transports defaults to info and this overwrites general level,
        // so we need to set the level in each transport:
        level: level,
        handleExceptions: true
    }));
} else {
    var options = {
        level: level,
        timestamp: process.env.MEDIC_LOG_SHORT ? false : true,
        handleExceptions: true,
        prettyPrint: true
    };
    transports.push(new winston.transports.Console(options));
}

var logger = new winston.Logger({
    transports: transports
});

function prependMetaData(oldLogger, args) {
    // This function was inspired by this blog post:
    // http://www.seejohncode.com/2012/09/04/javascript-callstacks/
    var oldPreparer = Error.prepareStackTrace;
    Error.prepareStackTrace = function replacementPreparer(error, trace) {
        return trace;
    };
    // Create an exception to get stack from
    var stack = new Error().stack;
    Error.prepareStackTrace = oldPreparer;

    var stackFrame = 2;
    var fileNameAndPath = stack[stackFrame].getFileName();
    var fileNameIndex = fileNameAndPath.lastIndexOf("/") + 1;
    var fileName = fileNameAndPath.substr(fileNameIndex, fileNameAndPath.length - fileNameIndex);

    if (process.env.MEDIC_LOG_FILE) {
        // Add JSON meta data to log
        var meta;
        if (typeof args[args.length - 1] === "object") {
            meta = args.pop() || {};
        } else {
            meta = {};
        }

        meta.processTitle = process.title;
        meta.filename = fileName;
        meta.functionName = stack[stackFrame].getFunctionName();
        meta.lineNumber = stack[stackFrame].getLineNumber();

        if (logger.globalMeta) {
            for (var key in logger.globalMeta) {
                if (!meta[key] && logger.globalMeta.hasOwnProperty(key)) {
                    meta[key] = logger.globalMeta[key];
                }
            }
        }

        args.push(meta);
    } else {
        var prefixString = "";

        if (process.env.MEDIC_LOG_SHORT) {
            prefixString += fileName + ":" + stack[stackFrame].getLineNumber();
        } else {
            if (process.env.MEDIC_LOG_PROCESS_TITLE) {
                prefixString += process.title + " ";
            }
            prefixString += fileName + ":" + stack[stackFrame].getLineNumber() +
                " " + stack[stackFrame].getFunctionName();
        }

        // prepend prefixString to message, if any,
        // otherwise format commands in message are not evaluated
        if (typeof args[0] === "string") {
            args[0] = prefixString + " " + args[0];
        } else {
            args.unshift(prefixString);
        }
    }
    oldLogger.apply(oldLogger, args);
}

function replaceLevel(level) {
    var originalLevel = logger[level];

    logger[level] = function () {
        var args = Array.prototype.slice.call(arguments);
        args.push(null); // no meta
        prependMetaData(originalLevel, args);
    };

    logger[level + "WithMeta"] = function () {
        var args = Array.prototype.slice.call(arguments);
        prependMetaData(originalLevel, args);
    };
}

// Prepend all log messages with caller's process name, filename and function name
for (var l in winston.levels) {
    replaceLevel(l);

    logger[l + "IsEnabled"] = (winston.levels[l] >= winston.levels[level]);
}

module.exports = logger;
