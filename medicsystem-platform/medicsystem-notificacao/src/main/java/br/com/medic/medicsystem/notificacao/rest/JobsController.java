package br.com.medic.medicsystem.notificacao.rest;

import java.text.ParseException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.notificacao.model.ResponseData;
import br.com.medic.medicsystem.notificacao.service.NotificacaoService;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

@Path("/job")
@RequestScoped
public class JobsController{
	
	//tipoenvio: 0- barateiro, 1-Zenvia

	@Inject
	private NotificacaoService cobrancaService;

	@GET
	@Path("/boletosavencer/executar/{tipoenvio}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarNotificacaoBoletosAVencer(@PathParam("tipoenvio") Integer tipoEnvio,
			@QueryParam("diasAdiante") Integer diasAdiante)
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoBoletosAVencer(tipoEnvio, diasAdiante);
	}

	@GET
	@Path("/boletosrecorrentesvencidos/executar/{tipoenvio}/{informapagamento:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarNotificacaoMensalidadeBoletosRecorrenteVencidos(
			@PathParam("tipoenvio") Integer tipoEnvio,
			@PathParam("informapagamento") Integer inFormaPagamento)
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoMensalidadeBoletosRecorrenteVencidos(tipoEnvio, inFormaPagamento);
	}
	
	@GET
	@Path("/contracheckdavencidos/executar/{tipoenvio}/{informapagamento:[0-9][0-9]*}")//->Funcionando
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarNotificacaoMensalidadeCCheckDaVencidos(
			@PathParam("informapagamento") Integer inFormaPagamento,
			@PathParam("tipoenvio") Integer tipoEnvio)
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoMensalidadeCCheckDaVencidos(inFormaPagamento, tipoEnvio);
	}
	
	@GET
	@Path("/clientesnovos/executar")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<Response>> enviarNotificacaoBoasVindasClientes()
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoBoasVindasClientes();
	}
	
	@GET
	@Path("/clientecontratonovo/executar")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<Response>> enviarNotificacaoLinkCarne(@QueryParam("hoursInterval") Long hoursInterval)
	        throws ClientHumanException, ParseException {
		return cobrancaService.sendNotificationContratoClienteLinkCarne(false, hoursInterval);
	}
	
	@GET
	@Path("/clientecontratorenovado/executar")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<Response>> enviarNotificacaoRenovacao(@QueryParam("hoursInterval") Long hoursInterval)
	        throws ClientHumanException, ParseException {
		return cobrancaService.sendNotificationContratoClienteLinkCarne(true, hoursInterval);
	}
	
	
	@GET
	@Path("/aniversariantes/executar/{tipoenvio}")//->Funcionando
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarNotificacaoAniversariantes(@PathParam("tipoenvio") Integer tipoEnvio)
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoAniversariantesDia(tipoEnvio);
	}
	
	@GET
	@Path("/agendamento/{diasRestantes:[0-9][0-9]*}/executar/{tipoenvio}")//Funcionando
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarNotificacaoAgendamentos(
			@PathParam("diasRestantes") Integer diasRestantes,
			@PathParam("tipoenvio") Integer tipoEnvio)
	        throws ClientHumanException, ParseException {
		return cobrancaService.enviarNotificacaoAgendamentoDia(diasRestantes, tipoEnvio);
	}
}