package br.com.medic.medicsystem.notificacao.job;

import java.text.ParseException;
import java.util.Calendar;

import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.notificacao.service.NotificacaoService;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

@Deprecated
@Stateless
public class BoasVindasClientesNovosJob {

	@Inject
	private Logger logger;

	@Inject
	private NotificacaoService notificacaoService;

	//@Schedule(hour = "22", minute = "2", persistent = false)
	public void doWork(Timer timer) {
		Long hash = Calendar.getInstance().getTimeInMillis();
		logger.info("Job #" + hash
		        + "(Enviar SMS de boas vindas para clientes novos)");
		logger.info("\tNext Timeout: " + timer.getNextTimeout());

		try {
			notificacaoService.enviarNotificacaoBoasVindasClientes();
		} catch (ClientHumanException e) {
			logger.error("Job #" + hash, e);
		} catch (ParseException e) {
			logger.error("Job #" + hash, e);
		}
	}

}
