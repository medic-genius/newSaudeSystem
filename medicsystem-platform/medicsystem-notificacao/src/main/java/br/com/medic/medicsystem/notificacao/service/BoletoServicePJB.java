package br.com.medic.medicsystem.notificacao.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.medic.medicsystem.notificacao.webservice.pjbank.WebServiceBoleto;
import br.com.medic.medicsystem.persistence.dao.BoletoBancarioDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.NegociacaoDAO;
import br.com.medic.medicsystem.persistence.dao.ParcelaDAO;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Negociacao;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;

@Stateless
public class BoletoServicePJB {

	@Inject
	private WebServiceBoleto wsb;
	
	@Inject
	@Named("boletobancario-dao")
	private BoletoBancarioDAO boletobancarioDAO;
	
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;
	
	@Inject
	@Named("parcela-dao")
	private ParcelaDAO parcelaDAO;
	
	@Inject
	@Named("negociacao-dao")
	private NegociacaoDAO negociacaoDAO;
	
	@SuppressWarnings("static-access")
	public BoletoBancario registrarBoleto(Object documento, Object cliente) throws Exception{
		
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		JSONObject json = null;
		List<BoletoBancario> listBoleto = new ArrayList<BoletoBancario>();		
		
		try {
			
			if(documento instanceof Mensalidade){
				mensalidade = (Mensalidade) documento;
				listBoleto = boletobancarioDAO.getBoletoBancario(mensalidade);
			}
			else if(documento instanceof Parcela){
				parcela = (Parcela) documento;
				listBoleto = boletobancarioDAO.getBoletoBancario(parcela);
			}
			
			json = wsb.criarEditarBoleto(documento, cliente);	
		
			if(json.getInt("status") == 200){

				if(listBoleto != null && listBoleto.size() > 0){
					
					listBoleto.get(0).setNrBarra(json.getString("linhaDigitavel"));
					listBoleto.get(0).setNrNossoNumero(json.getString("nossonumero"));
					listBoleto.get(0).setLinkBoleto(json.getString("linkBoleto"));
					
					boletobancarioDAO.update(listBoleto.get(0));
					
					return listBoleto.get(0);
				
				}else{
										
					if( json.has("credencial") && wsb.CREDENCIAL_KEY.equals(json.get("credencial")) ){
						
						BoletoBancario boletoBancario = new BoletoBancario();
						boletoBancario.setNrBoleto( Long.valueOf(json.getString("id_unico")) );
						boletoBancario.setDtInclusao( new Date() );
						boletoBancario.setCdBarra(json.getString("token_facilitador"));
						boletoBancario.setNrBarra(json.getString("linhaDigitavel"));
						boletoBancario.setNrNossoNumero(json.getString("nossonumero"));
						boletoBancario.setLinkBoleto(json.getString("linkBoleto"));
						
						if(mensalidade != null)
							boletoBancario.setIdMensalidade(mensalidade.getId());
						else if(parcela != null)
							boletoBancario.setIdParcela(parcela.getId());
						
						boletobancarioDAO.persist(boletoBancario);
						
						return boletoBancario;						
					}										
				}				
			}
			
			return null;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}		
		
	}
	
	public Integer liquidarCobrancaSl(Integer periodoDias){
		
		//inTipoPago = null -boletos nao pagos
		//inTipoPago = 1 -boletos pagos
						
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		Date dtPagamento;
		Boolean despesaPaga = true;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;		
		String dtInicio, dtFim;
		Integer numPagina = 1;
		JSONArray listJsonTotal = new JSONArray();
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		dtFim = sdf.format(calendar.getTime());
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.add(Calendar.DAY_OF_MONTH, -periodoDias);
		dtInicio = sdf.format(calendar2.getTime());		
		
		try {
			
			JSONArray listJson = wsb.extratoBoleto(dtInicio, dtFim, 1);
			
			while(listJson != null && listJson.length() > 0){								
				numPagina += 1;				
				
				if(listJsonTotal != null && listJsonTotal.length() > 0){
					for (Object object : listJson) {
						listJsonTotal.put(object);
					}
				}else{
					listJsonTotal = listJson;
				}
				
				listJson = wsb.extratoBoleto(dtInicio, dtFim, 1, numPagina);
				System.out.println("Pagina boleto: " + numPagina);	
				
			}
			System.out.println("Total paginas boleto: " + numPagina);			
			int count = 0;
			
			if(listJsonTotal != null){
				
				int len = listJsonTotal.length();		
				
				for(int i = 0; i < len; ++i) {				
					JSONObject jsonObj = listJsonTotal.getJSONObject(i);
					BoletoBancario bb = boletobancarioDAO.getBoletoByNossoNumero( jsonObj.getString("nosso_numero") );	
					
					if(bb != null){
						
						if(bb.getIdMensalidade() != null)
							mensalidade = mensalidadeDAO.searchByKey(Mensalidade.class, bb.getIdMensalidade());
						else if(bb.getIdParcela() != null)
							parcela = parcelaDAO.getParcela(bb.getIdParcela());
									
						dtPagamento = sdf.parse(jsonObj.getString("data_pagamento"));
	//					vlJuros = json.has("data[compo_recebimento][1][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][1][st_valor_comp]") : 0.0;
	//					vlMulta = json.has("data[compo_recebimento][2][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][2][st_valor_comp]") : 0.0;
						
						
						if(parcela != null && parcela.getDtPagamento() == null){
							
							List<Negociacao> listnegociacoes = new ArrayList<Negociacao>();
							
							parcela.setDtPagamento(dtPagamento);
							parcela.setVlPago(Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")));
							parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							parcela.setInFormaPagamento(5);
							parcela.setVlJuros( vlJuros );
							parcela.setVlMulta( vlMulta );				
											
							for(Parcela pc: parcela.getDespesa().getParcelas())					
								if(pc.getDtPagamento() == null && (pc.getBoExcluida() == null || pc.getBoExcluida() == false))
									despesaPaga = false;			
											
							if(despesaPaga)
								parcela.getDespesa().setInSituacao(1);
							
							listnegociacoes = negociacaoDAO.getNegociacaoByDespesa(parcela.getDespesa().getId());
							if(listnegociacoes != null && listnegociacoes.size() > 0)
								for(Negociacao neg : listnegociacoes){
									neg.getMensalidade().setDataPagamento(dtPagamento);
									neg.getMensalidade().setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
									neg.getMensalidade().setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
									neg.getMensalidade().setInStatus(1);
									neg.getMensalidade().setVlJuros(vlJuros);
									neg.getMensalidade().setVlMulta(vlMulta);
									neg.getMensalidade().setBoletoNegociada(true);
									neg.getMensalidade().setDtNegociacao(parcela.getDespesa().getDtDespesa());
									
									mensalidadeDAO.update(neg.getMensalidade());
								}				
							
							
							parcelaDAO.update(parcela);
							count++;
							
							
						}else if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
											
							mensalidade.setDataPagamento(dtPagamento);
							mensalidade.setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
							mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							mensalidade.setInStatus(1);
							mensalidade.setVlJuros(vlJuros);
							mensalidade.setVlMulta(vlMulta);
							
							mensalidadeDAO.update(mensalidade);
							count++;
										
						}
					
					}
				
				}
			}
			
			return count;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return null;		
					
	}
	
	public Integer liquidarCobrancaSl(String dtInicio, String dtFim){
		
		//inTipoPago = null -boletos nao pagos
		//inTipoPago = 1 -boletos pagos
						
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		Date dtPagamento;
		Boolean despesaPaga = true;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;		
		Integer numPagina = 1;
		JSONArray listJsonTotal = new JSONArray();
				
		try {
			
			JSONArray listJson = wsb.extratoBoleto(dtInicio, dtFim, 1);
			
			while(listJson != null && listJson.length() > 0){								
				numPagina += 1;				
				
				if(listJsonTotal != null && listJsonTotal.length() > 0){
					for (Object object : listJson) {
						listJsonTotal.put(object);
					}
				}else{
					listJsonTotal = listJson;
				}
				
				listJson = wsb.extratoBoleto(dtInicio, dtFim, 1, numPagina);
				System.out.println("Pagina boleto: " + numPagina);	
				
			}
			System.out.println("Total paginas boleto: " + numPagina);			
			int count = 0;
			
			if(listJsonTotal != null){
				
				int len = listJsonTotal.length();		
				
				for(int i = 0; i < len; ++i) {				
					JSONObject jsonObj = listJsonTotal.getJSONObject(i);
					BoletoBancario bb = boletobancarioDAO.getBoletoByNossoNumero( jsonObj.getString("nosso_numero") );	
					
					if(bb != null){
						
						if(bb.getIdMensalidade() != null)
							mensalidade = mensalidadeDAO.searchByKey(Mensalidade.class, bb.getIdMensalidade());
						else if(bb.getIdParcela() != null)
							parcela = parcelaDAO.getParcela(bb.getIdParcela());
									
						dtPagamento = sdf.parse(jsonObj.getString("data_pagamento"));
	//					vlJuros = json.has("data[compo_recebimento][1][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][1][st_valor_comp]") : 0.0;
	//					vlMulta = json.has("data[compo_recebimento][2][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][2][st_valor_comp]") : 0.0;
						
						
						if(parcela != null && parcela.getDtPagamento() == null){
							
							List<Negociacao> listnegociacoes = new ArrayList<Negociacao>();
							
							parcela.setDtPagamento(dtPagamento);
							parcela.setVlPago(Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")));
							parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							parcela.setInFormaPagamento(5);
							parcela.setVlJuros( vlJuros );
							parcela.setVlMulta( vlMulta );				
											
							for(Parcela pc: parcela.getDespesa().getParcelas())					
								if(pc.getDtPagamento() == null && (pc.getBoExcluida() == null || pc.getBoExcluida() == false))
									despesaPaga = false;			
											
							if(despesaPaga)
								parcela.getDespesa().setInSituacao(1);
							
							listnegociacoes = negociacaoDAO.getNegociacaoByDespesa(parcela.getDespesa().getId());
							if(listnegociacoes != null && listnegociacoes.size() > 0)
								for(Negociacao neg : listnegociacoes){
									neg.getMensalidade().setDataPagamento(dtPagamento);
									neg.getMensalidade().setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
									neg.getMensalidade().setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
									neg.getMensalidade().setInStatus(1);
									neg.getMensalidade().setVlJuros(vlJuros);
									neg.getMensalidade().setVlMulta(vlMulta);
									neg.getMensalidade().setBoletoNegociada(true);
									neg.getMensalidade().setDtNegociacao(parcela.getDespesa().getDtDespesa());
									
									mensalidadeDAO.update(neg.getMensalidade());
								}				
							
							
							parcelaDAO.update(parcela);
							count++;
							
							
						}else if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
											
							mensalidade.setDataPagamento(dtPagamento);
							mensalidade.setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
							mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							mensalidade.setInStatus(1);
							mensalidade.setVlJuros(vlJuros);
							mensalidade.setVlMulta(vlMulta);
							
							mensalidadeDAO.update(mensalidade);
							count++;
										
						}
					
					}
				
				}
			}
			
			return count;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return null;		
					
	}
	
	public byte[] imprimirBoleto(List<?> documentos) {
		
		String urlBoleto = wsb.imprimirBoleto(documentos);
				
		URL url;
		InputStream in = null;
		ByteArrayOutputStream out = null;
		byte[] pdf = null;
		
		if(urlBoleto != null){
		
			try {
				url = new URL(urlBoleto);
			
				in = new BufferedInputStream(url.openStream());
				out = new ByteArrayOutputStream();
				
				byte[] buf = new byte[1024];
				int n = 0;
				while (-1!=(n=in.read(buf)))
				{out.write(buf, 0, n);}
		
				pdf = out.toByteArray();			
			
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
				try {
					out.close();		
					in.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
				
			}
		}
		
		return pdf;
	}
	
	public String getLinkCarne(List<?> documentos){
		
		return	wsb.imprimirBoleto(documentos);	
	}
	
	public List<BoletoBancario> getBoletosBancarios(Parcela parcela) {
		return boletobancarioDAO.getBoletoBancario(parcela);
	}
	
	public List<BoletoBancario> getBoletosBancario(Mensalidade mensalidade) {
		return boletobancarioDAO.getBoletoBancario(mensalidade);
	}
	
	public List<BoletoBancario> getBoletosBancarios(Long idMensalidade) {
		Mensalidade m = new Mensalidade();
		m.setId(idMensalidade);
		return this.getBoletosBancario(m);
	}
}
