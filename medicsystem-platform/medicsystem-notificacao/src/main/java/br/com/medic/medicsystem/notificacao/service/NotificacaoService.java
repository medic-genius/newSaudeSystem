package br.com.medic.medicsystem.notificacao.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import br.com.medic.medicsystem.notificacao.enums.TipoMensagem;
import br.com.medic.medicsystem.notificacao.model.ResponseData;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dto.NotificacaoClienteDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.BoletoVencido;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.views.AniversariantesView;
import br.com.medic.medicsystem.persistence.model.views.ViewAgendamentoSMS;
import br.com.medic.medicsystem.persistence.security.MongoResource;
import br.com.medic.medicsystem.persistence.security.dto.AuditMessageObject;
import br.com.medic.medicsystem.sms.bean.ListResource;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.enumerator.LayoutTypeEnum;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.MenssageBarateiroService;
import br.com.medic.medicsystem.sms.service.MultipleMessageService;
import br.com.medic.medicsystem.sms.service.URLShortenerService;
import br.com.medic.medicsystem.sms.service.base.BaseService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
public class NotificacaoService {
	private final String NR_TELEFONE = "4003-8040";
	
	private final String BOLETO_A_VENCER_SMS= "Ola %s, voce tem uma mensalidade com vencimento em %s no valor de %s. Duvidas liga para gente " + NR_TELEFONE;

	private final String BOLETO_VENCIDO_SMS = "Ola %s, voce tem uma mensalidade atrasada em %s no valor de %s. Duvidas liga para gente " + NR_TELEFONE;

	private final String CLIENTE_NOVO_BV = "Ola %s, seja bem-vindo(a), estamos felizes por ter voce conosco, qualquer duvida ligue para nossa central " + NR_TELEFONE;
	
	private final String CLIENTE_NOVO_CONTRATO_BOLETO = "Ola %s, obrigado por contratar nosso plano. Segue o link do carne para pagamento: %s. Duvidas " + NR_TELEFONE;
	
	private final String CLIENTE_CONTRATO_RENOVADO = "Olá %s, seguem os boletos de seu plano de saude no link %s. Duvidas: " + NR_TELEFONE;

//	private final String ANIVERSARIANTE_DIA_SMS = "Feliz aniversario %s. Desejamos paz e alegria no seu aniversario.";
	private final String ANIVERSARIANTE_DIA_SMS = "Feliz aniversario %s. Que hoje seja um dia memoravel e a felicidade nunca abandone a sua vida";
	
	private final String AGENDAMENTO_A_REALIZAR_SMS = "Lembrete: Ola %s, voce possui um agendamento em %s, especialidade %s, na unidade %s, de %s as %s";

	private final String PREPARATORIO_SMS = "Confira seu preparatorio de seu exame no site: %s em caso de duvidas ligue " + NR_TELEFONE;

	private final String BR_CODE = "55";

	private final String MAO_CODE = "92";

	private final String NINE_DIGIT = "9";

	private final Integer DIAS_ADIANTE = 2;

	private final Integer DIAS_ADIANTE_ENVIO_SMS = 1;

//	private final String DRCONSULTA = "DR+CONSULTA";

//	private final String MEDICLAB = "MEDICLAB";
	
	private final String DRCARD = "DR. CARD";
	
	private final String MAISCONSULTA = "MAIS CONSULTA";

	@Inject
	MultipleMessageService mms;

	@Inject
	MenssageBarateiroService mmsBarateiro;

	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	private BoletoServicePJB boletoServicePJB;
	
	@Inject
	private MongoResource mongo;
	
	@Inject
	private Logger logger;
	
	@Inject
	private URLShortenerService urlShortenerService;
	
	SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");

	public void setMensalidadeDAO(MensalidadeDAO mensalidadeDAO) {
		this.mensalidadeDAO = mensalidadeDAO;
	}
	
	private Date configDate(Long time){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		calendar.set(Calendar.DAY_OF_MONTH, 23);
		calendar.set(Calendar.MONTH, 1);
		calendar.set(Calendar.YEAR, 2017);
		
		return calendar.getTime();
	}
	
	private Calendar truncarCalendar(Calendar calendar){
		
//		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar;
		
	}
	
	private long mapDatas(Date valor, Date intervaloMinimo, Date intervaloMaximo, Date saidaMinima,
			Date saidaMaxima) {

		return (((valor.getTime() - intervaloMinimo.getTime()) * (saidaMaxima.getTime() - saidaMinima.getTime())) 
				/ (intervaloMaximo.getTime() - intervaloMinimo.getTime())) + saidaMinima.getTime();

	}
	
	public ViewAgendamentoSMS setHorarioPrevisto(ViewAgendamentoSMS agendamentoVW){
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
		
		Agendamento agendamento = agendamentoDAO.getAgendamento(agendamentoVW.getIdAgendamento());
		
		calendar.setTime(agendamento.getDtAgendamento());
		AtendimentoProfissional atendProfissional = new AtendimentoProfissional();
		
		
				
		atendProfissional = getAtendimentoProfissionalByAgendamento(agendamento);
		
		if(atendProfissional != null ){
			if(atendProfissional.getHrOrdemInicio() != null && atendProfissional.getHrOrdemFim() != null){
				
				agendamento.setHrInicio(sdf.format(atendProfissional.getHrOrdemInicio()));
				agendamento.setHrFim(sdf.format(atendProfissional.getHrOrdemFim()));
				
				Date dataAgendamento = configDate(agendamento.getHrAgendamento().getTime());
				Date dataInitProf = configDate(atendProfissional.getHrInicio().getTime());
				Date dataFimProf = configDate(atendProfissional.getHrFim().getTime());
				Date dataInitProfMin = configDate(atendProfissional.getHrOrdemInicio().getTime());
				Date dataFimProfMax = configDate(atendProfissional.getHrOrdemFim().getTime());
				
				calendar.setTimeInMillis(mapDatas(dataAgendamento, dataInitProf, dataFimProf, dataInitProfMin, dataFimProfMax)); 
				
				calendar = truncarCalendar(calendar);
				agendamento.setHrInicio(sdf.format(calendar.getTime()));
				calendar.add(Calendar.HOUR_OF_DAY, 1);
				
				if(calendar.getTime().after(dataFimProfMax)){
					agendamento.setHrFim(sdf.format(dataFimProfMax.getTime()));
				}else{
					agendamento.setHrFim(sdf.format(calendar.getTime()));
				}
				
			}else if(atendProfissional.getBoOrdemProducao() != null && atendProfissional.getBoOrdemProducao()){
				agendamento.setHrInicio(atendProfissional.getHrInicio().toString());
				calendar.setTimeInMillis(atendProfissional.getHrInicio().getTime());
				calendar.add(Calendar.HOUR_OF_DAY, 1);
				calendar.set(Calendar.SECOND, 0);
				agendamento.setHrFim(sdf.format(calendar.getTime()));
				
			}else{
				
				calendar.setTime(agendamento.getHrAgendamento());
				calendar = truncarCalendar(calendar);
				calendar.add(Calendar.HOUR_OF_DAY, -1);
				calendar.set(Calendar.SECOND, 0);
				
				if(calendar.get(Calendar.HOUR_OF_DAY) >= 7){
					agendamento.setHrInicio(sdf.format(calendar.getTime()));
				}else{
					calendar.set(Calendar.HOUR_OF_DAY, 7);
					calendar.set(Calendar.SECOND, 0);
					agendamento.setHrInicio(sdf.format(calendar.getTime()));
				}
				calendar.setTime(agendamento.getHrAgendamento());
				calendar = truncarCalendar(calendar);
				agendamento.setHrFim(sdf.format(calendar.getTime()));
			}
			
		}else{
			agendamento.setHrInicio("00:00");
			agendamento.setHrFim("00:00");
		}

		agendamentoVW.setHrInicioFim(agendamento.getHrInicio()+"-"+agendamento.getHrFim());
		
		return agendamentoVW;
		
	}
	
	public AtendimentoProfissional getAtendimentoProfissionalByAgendamento(Agendamento agendamento){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(agendamento.getDtAgendamento());
		Integer diaSemana = new Integer(calendar.get(Calendar.DAY_OF_WEEK) - 1);
		
		AtendimentoProfissional atendProfissional = agendamentoDAO.getConsultorioPorEspecialidadeProfissional(agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(),
				agendamento.getUnidade().getId(), agendamento.getHrAgendamentoFormatado(), agendamento.getServico().getId(), diaSemana);
		return atendProfissional;
	}

	@SuppressWarnings("unchecked")
	private void persistOnLog(AuditMessageObject aMsg) {
		HashMap<String, Object> result;
		try {
			ObjectMapper mapper = new ObjectMapper();
			// Object to JSON in String
			String jsonInString = mapper.writeValueAsString(aMsg);
			result = new ObjectMapper().readValue(jsonInString, HashMap.class);
			mongo.saveDocumentLogSms(new Document(result));
		} catch (IOException e) {
			logger.error("Nao foi possivel registrar o log no mongo", e);
		}
	}

	private boolean isSuccessfulResponse(int tipoEnvio, Object response) {
		if(tipoEnvio == 0) {
			String res = (String) response;
			try {
				JSONParser parser = new JSONParser();
				JSONObject result = (JSONObject) parser.parse(res);
				if(!result.containsKey("status")) {
					return false;
				}
				Integer stt = Integer.valueOf(result.get("status").toString());
				return stt.intValue() == 1;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		} else if(tipoEnvio == 1) {
			Response r = (Response) response;
			return r.getReturnCode().equals("000");
		}
		return false;
	}

	private void persistMessageSendingBoleto(TipoMensagem tipo, int tipoEnvio, List<BoletoVencido> receivers, 
			String content, List<?> response) {
		String params[] = content.split("\n");

		int index = 0;
		for(BoletoVencido rcv : receivers) {
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getIdCliente());
			aMsg.setTipoMensagem(tipo.ordinal());
			
			String num = null;
			if(rcv.getNrcelular() != null) {
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrcelular()) : phoneNumberCleanupBarateiro(rcv.getNrcelular());
				if(num != null) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(num);
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}

			if(rcv.getNrtelefone() != null) {
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrtelefone()) : phoneNumberCleanupBarateiro(rcv.getNrtelefone());
				if(telNum != null && !telNum.equals(num)) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrtelefone());
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}
		}
	}

	private void persistMessageSendingAgendamento(TipoMensagem tipo, int tipoEnvio, List<ViewAgendamentoSMS> receivers, 
			String content, List<?> response) {

		String params[] = content.split("\n");
		int index = 0;

		for(ViewAgendamentoSMS rcv : receivers) {
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getIdCliente());
			aMsg.setTipoMensagem(tipo.ordinal());
			
			String num = null;
			if(rcv.getNrCelular() != null) {
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(num);
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
					
					if(rcv.getLinkPreparatorio() != null && rcv.getLinkPreparatorio().length() > 0) {
						aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
						msg = params[index].split(";")[1];
						aMsg.setConteudoMensagem(msg);
						persistOnLog(aMsg);
						++index;
					}
				}
			}
			if(rcv.getNrTelefone() != null) {
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(telNum);
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;

					if(rcv.getLinkPreparatorio() != null && rcv.getLinkPreparatorio().length() > 0) {
						aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
						msg = params[index].split(";")[1];
						aMsg.setConteudoMensagem(msg);
						persistOnLog(aMsg);
						++index;
					}
				}
			}
		}
	}

	private void persistMessageSendingAniversario(TipoMensagem tipo, int tipoEnvio, List<AniversariantesView> receivers,
			String content, List<?> response) {
		String params[] = content.split("\n");
		int index = 0;
		for(AniversariantesView rcv : receivers) {
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getId());
			aMsg.setTipoMensagem(tipo.ordinal());
			
			String num = null;
			if(rcv.getNrCelular() != null) {
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(num);
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}

			if(rcv.getNrTelefone() != null) {
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrTelefone());
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}
		}
	}

	private void persistMessageSendingBoletosAVencer(TipoMensagem tipo, int tipoEnvio, List<Object[]> receivers, 
			String content, List<?> response) {
		List<Cliente> rcvs = new ArrayList<Cliente>();
		for(Object o : receivers) {
			rcvs.add((Cliente) o);
		}
		persistMessageSendingCliente(tipo, tipoEnvio, rcvs, content, response);
	}

	private void persistMessageSendingCliente(TipoMensagem tipoMensagem, int tipoEnvio, List<Cliente> receivers, 
			String content, List<?> response) {

		String params[] = content.split("\n");
		int index = 0;
		for(Cliente rcv : receivers) {
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getId());
			aMsg.setTipoMensagem(tipoMensagem.ordinal());
			
			String num = null;
			if(rcv.getNrCelular() != null) {
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrCelular());
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}

			if(rcv.getNrTelefone() != null) {
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrTelefone());
					String msg = params[index].split(";")[1];
					aMsg.setConteudoMensagem(msg);
					persistOnLog(aMsg);
					++index;
				}
			}
		}
	}

	public ResponseData<String, List<Response>> enviarNotificacaoBoletosVencidosCCredito()
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<BoletoVencido> result = mensalidadeDAO.getBoletosVencidosCCredito();

		if (result != null && result.size() > 0) {

			String content = constroiMMSBoletosVencidos(result);

			ListResource message = new ListResource(content,
					LayoutTypeEnum.TYPE_E.getType());

			logger.debug("Mensagens Enviadas: " + message);

			List<Response> response = mms.send(message);

			persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, 1, result, content, response);

			logger.debug("SMS Gateway Response: " + response);

			return new ResponseData<String, List<Response>>(content, response);
		}
		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

	public ResponseData<String, List<Response>> enviarNotificacaoBoletosVencidosCDebito()
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<BoletoVencido> result = mensalidadeDAO.getBoletosVencidosCDebito();

		if (result != null && result.size() > 0) {

			String content = constroiMMSBoletosVencidos(result);

			ListResource message = new ListResource(content,
					LayoutTypeEnum.TYPE_E.getType());

			logger.debug("Mensagens Enviadas: " + message);

			List<Response> response = mms.send(message);

			logger.debug("SMS Gateway Response: " + response);

			persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, 1, result, content, response);

			return new ResponseData<String, List<Response>>(content, response);
		}
		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

	private String validaDiaEnvio_DA_Contracheck(){
		Calendar cal = Calendar.getInstance();

		if(cal.get(Calendar.DAY_OF_MONTH) == 15 || cal.get(Calendar.DAY_OF_MONTH) == 19 ||
				cal.get(Calendar.DAY_OF_MONTH) == 23 || cal.get(Calendar.DAY_OF_MONTH) == 27){
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			return sqlFormatter.format(cal.getTime());

		}else if(cal.get(Calendar.DAY_OF_MONTH) == 1 || cal.get(Calendar.DAY_OF_MONTH) == 5 
				|| cal.get(Calendar.DAY_OF_MONTH) == 9){

			cal.add(Calendar.MONTH, -2);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			return sqlFormatter.format(cal.getTime());

		}

		return null;


	}

	public ResponseData<String, List<?>> enviarNotificacaoMensalidadeCCheckDaVencidos(Integer inFormaPagamento, Integer tipoEnvio)
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		String dtInicio = validaDiaEnvio_DA_Contracheck();

		if(dtInicio != null && dtInicio.length() > 0){

			List<BoletoVencido> result = mensalidadeDAO.getMensalidadesDaCCheckVencidas(dtInicio, inFormaPagamento);

			if (result != null && result.size() > 0) {
				
				if(tipoEnvio.equals(1)){
					String content = constroiMMSBoletosVencidos(result);

					ListResource message = new ListResource(content,
							LayoutTypeEnum.TYPE_E.getType());

					logger.debug("Mensagens Enviadas: " + message);

					List<Response> response = mms.send(message);

					logger.debug("SMS Gateway Response: " + response);

					persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, 1, result, content, response);

				 return new ResponseData<String, List<?>>(content, response);
				}else if(tipoEnvio.equals(0)) {
					StringBuffer  content = new StringBuffer();
					List<String> response = new ArrayList<String>();

					for (BoletoVencido boletoVencido : result) {
						String numeroCelular = phoneNumberCleanupBarateiro(boletoVencido.getNrcelular());
						if(numeroCelular != null){
							try {
								String mensagem = DRCARD+": " + getSMSMessage(BOLETO_VENCIDO_SMS,
										getNomeSimplificado(boletoVencido.getNmcliente()),
										boletoVencido.getDataVencimentoFormatado(),
										boletoVencido.getValorMensalidadeFormatado());
								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
								Thread.sleep(10000);
								response.add(r);
								content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						String numeroTelefone = phoneNumberCleanupBarateiro(boletoVencido.getNrtelefone());
						if (numeroTelefone != null) {
							if (!numeroTelefone.equals(numeroCelular)) {
								try {
									String mensagem = DRCARD+": " + getSMSMessage(BOLETO_VENCIDO_SMS,
											getNomeSimplificado(boletoVencido.getNmcliente()),
											boletoVencido.getDataVencimentoFormatado(),
											boletoVencido.getValorMensalidadeFormatado());
									String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
									String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

									String r = mmsBarateiro.send(mensagem, numeroTelefone, dtEnvio, hrEnvio);
									Thread.sleep(10000);
									response.add(r);
									content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
					String ctnt = content.toString();
					if(!ctnt.isEmpty()) {
						persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, tipoEnvio.intValue(), 
								result, ctnt, response);
					}

					return new ResponseData<String, List<?>>(ctnt, response);
				}

				
			}
		}

		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

	public ResponseData<String, List<Response>> enviarNotificacaoBoletosVencidosCRecorrente()
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<BoletoVencido> result = mensalidadeDAO.getBoletosVencidosCRecorrente();

		if (result != null && result.size() > 0) {

			String content = constroiMMSBoletosVencidos(result);

			ListResource message = new ListResource(content,
					LayoutTypeEnum.TYPE_E.getType());

			logger.debug("Mensagens Enviadas: " + message);

			List<Response> response = mms.send(message);

			logger.debug("SMS Gateway Response: " + response);

			persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, 1, result, content, response);

			return new ResponseData<String, List<Response>>(content, response);
		}
		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

	public ResponseData<String, List<?>> enviarNotificacaoAgendamentoDia(
			Integer diasRestantes, Integer tipoEnvio) throws ParseException, ClientHumanException {
		logger.info("Preparando e Enviando mensagens");
		Collection<ViewAgendamentoSMS> result = clienteDAO.getAgendamentosDiasRestantesPorCliente(diasRestantes);

		if (result != null && result.size() > 0) {
			
			if(tipoEnvio.equals(1)) {
				String content = constroiMMSAgendamentosDiasRestantesPorCliente(result, diasRestantes);

				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);

				List<Response> response = mms.send(message);

				logger.debug("SMS Gateway Response: " + response);

				List<ViewAgendamentoSMS> rcvs = new ArrayList<ViewAgendamentoSMS>(result);
				if(response != null){
					persistMessageSendingAgendamento(TipoMensagem.LEMBRETE_AGENDAMENTO, tipoEnvio.intValue(), 
							rcvs, content, response);

				}
				
				return new ResponseData<String, List<?>>(content, response);

			} else if (tipoEnvio.equals(0)) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf.parse(sdf.format(new Date())));
				cal.add(Calendar.DATE, diasRestantes + 1);
//				String dataAgendamentoFormatado = sdf.format(cal.getTime());
				
				StringBuffer content = new StringBuffer();
				List<String> response = new ArrayList<String>();

				for (ViewAgendamentoSMS object : result) {
					setHorarioPrevisto(object);
					String numeroCelular = phoneNumberCleanupBarateiro(object.getNrCelular());
					if(numeroCelular != null) {
						try {
							String mensagem = MAISCONSULTA+": " + getSMSMessage(AGENDAMENTO_A_REALIZAR_SMS,
									getNomeSimplificado(object.getNmPaciente()),
									sdf.format(object.getDtAgendamento()),object.getNmEspecialidade(), object.getNmapelido(), object.getHrOrdemInicio(), object.getHrOrdemFim() );
							String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
							String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

							String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
							Thread.sleep(5000);
							response.add(r);

							content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");

							//envia mms do preparatorio 
							if(object.getLinkPreparatorio() != null && object.getLinkPreparatorio().length() > 0){
								String mensagemAux = MAISCONSULTA+": " + getSMSMessage(PREPARATORIO_SMS, object.getLinkPreparatorio());
								String dtSent = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrSent = geHoraAgendamentoEnvioSMSBarateiro();

								String auxR = mmsBarateiro.send(mensagemAux, numeroCelular, dtSent, hrSent);
								Thread.sleep(5000);
								response.add(auxR);

								content.append(numeroCelular+";" + mensagemAux+";" + dtSent+";" + hrSent+"\n");
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					String numeroTelefone = phoneNumberCleanupBarateiro(object.getNrTelefone());
					if (numeroTelefone != null) {
						if (!numeroTelefone.equals(numeroCelular)) {
							try {
								String mensagem = MAISCONSULTA+": " + getSMSMessage(AGENDAMENTO_A_REALIZAR_SMS,
										getNomeSimplificado(object.getNmPaciente()),
										sdf.format(object.getDtAgendamento()),object.getNmEspecialidade(), object.getNmapelido(), object.getHrOrdemInicio(), object.getHrOrdemFim());
								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroTelefone, dtEnvio, hrEnvio);
								Thread.sleep(5000);
								response.add(r);

								content.append(numeroTelefone+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
								//envia mms do preparatorio 
								if(object.getLinkPreparatorio() != null && object.getLinkPreparatorio().length() > 0){
									String mensagemAux = MAISCONSULTA+": " + getSMSMessage(PREPARATORIO_SMS, object.getLinkPreparatorio());
									String dtSent = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
									String hrSent = geHoraAgendamentoEnvioSMSBarateiro();

									String auxR = mmsBarateiro.send(mensagemAux, numeroTelefone, dtSent, hrSent);
									Thread.sleep(5000);
									response.add(auxR);

									content.append(numeroTelefone+";" + mensagemAux+";" + dtSent+";" + hrSent+"\n");
								}

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				String ctnt = content.toString();
				if(!ctnt.isEmpty()) {
					List<ViewAgendamentoSMS> rcvs = new ArrayList<ViewAgendamentoSMS>(result);
					persistMessageSendingAgendamento(TipoMensagem.LEMBRETE_AGENDAMENTO, tipoEnvio.intValue(), 
							rcvs, ctnt, response);
				}

				return new ResponseData<String, List<?>>(ctnt, response);
			}
		}
		logger.info("Nenhum novo cliente foi encontrado");
		return null;
	}

	public ResponseData<String, List<?>> enviarNotificacaoAniversariantesDia(Integer tipoEnvio) throws ParseException, ClientHumanException {
		logger.info("Preparando e Enviando mensagens");

		Collection<AniversariantesView> result = clienteDAO.getAniversariantes();
		if (result != null && result.size() > 0) { 
			if(tipoEnvio.equals(1)) {
				String content = constroiMMSAniversariantes(result);
				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);

				List<Response> response = mms.send(message);
				List<AniversariantesView> rcvs = new ArrayList<AniversariantesView>(result);
				persistMessageSendingAniversario(TipoMensagem.ANIVERSARIANTE, tipoEnvio.intValue(), rcvs, content, response);

				logger.debug("SMS Gateway Response: " + response);

				return new ResponseData<String, List<?>>(content, response);
			} else if (tipoEnvio.equals(0)) {
				List<String> response = new ArrayList<String>();
				StringBuffer content = new StringBuffer();
				for (AniversariantesView object : result) {
					String numeroCelular = phoneNumberCleanupBarateiro(object.getNrCelular());
					if(numeroCelular != null){
						try {

							String mensagem = DRCARD+": " + getSMSMessage(ANIVERSARIANTE_DIA_SMS,

									getNomeSimplificado(object.getNmCliente()));
							String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
							String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

							String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
							Thread.sleep(5000);
							response.add(r);
							content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					String numeroTelefone = phoneNumberCleanupBarateiro(object.getNrTelefone());
					if (numeroTelefone != null) {
						if (!numeroTelefone.equals(numeroCelular)) {
							try {
								String mensagem = DRCARD+": " + getSMSMessage(ANIVERSARIANTE_DIA_SMS,
										getNomeSimplificado(object.getNmCliente()));

								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
								Thread.sleep(5000);
								response.add(r);
								content.append(numeroTelefone+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				String ctnt = content.toString();
				if(!ctnt.isEmpty()) {
					List<AniversariantesView> rcvs = new ArrayList<AniversariantesView>(result);
					persistMessageSendingAniversario(TipoMensagem.ANIVERSARIANTE, tipoEnvio.intValue(), 
							rcvs, ctnt, response);
				}
				
				return new ResponseData<String, List<?>>(ctnt, response);
			}
		}
		logger.info("Nenhum novo cliente foi encontrado");
		return null;
	}
	
	public ResponseData<String, List<?>> enviarNotificacaoBoletosAVencer(Integer tipoEnvio)
			throws ClientHumanException, ParseException {
		return this.enviarNotificacaoBoletosAVencer(tipoEnvio, null);
	}

	public ResponseData<String, List<?>> enviarNotificacaoBoletosAVencer(Integer tipoEnvio, Integer diasAdiante)
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");
		
		int daysAhead = DIAS_ADIANTE;
		if(diasAdiante != null && diasAdiante > daysAhead) {
			daysAhead = diasAdiante;
		}
		
		ArrayList<Object[]> result = mensalidadeDAO
				.obterBoletosAVencer(getProximoDia(daysAhead));

		if (result != null && result.size() > 0) {
			if(tipoEnvio.equals(1)) {
				String content = constroiMMSBoletosAVencer(result);

				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);
				
				List<Response> response = mms.send(message);

				logger.debug("SMS Gateway Response: " + response);

//				persistMessageSendingBoletosAVencer(TipoMensagem.LEMBRETE_MENSALIDADE, tipoEnvio.intValue(), 
//						result, content, response);

				return new ResponseData<String, List<?>>(content, response);
			} else if (tipoEnvio.equals(0)) {

				StringBuffer content = new StringBuffer();
				List<String> response = new ArrayList<String>();

				for (Object[] objects : result) {
					Mensalidade mensalidade = (Mensalidade) objects[0];
					Cliente cliente = (Cliente) objects[1];

					String numeroCelular = phoneNumberCleanupBarateiro(cliente.getNrCelular());
					if(numeroCelular != null) {
						try {
							String mensagem = DRCARD+": " + getSMSMessage(BOLETO_A_VENCER_SMS,
									getNomeSimplificado(cliente.getNmCliente()),
									mensalidade.getDataVencimentoFormatado(),
									mensalidade.getValorMensalidadeFormatado());
							String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
							String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

							String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
							Thread.sleep(5000);
							response.add(r);
							content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					String numeroTelefone = phoneNumberCleanupBarateiro(cliente.getNrTelefone());
					if (numeroTelefone != null) {
						if (!numeroTelefone.equals(numeroCelular)) {
							try {
								String mensagem = DRCARD+": " + getSMSMessage(BOLETO_A_VENCER_SMS,
										getNomeSimplificado(cliente.getNmCliente()),
										mensalidade.getDataVencimentoFormatado(),
										mensalidade.getValorMensalidadeFormatado());
								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroTelefone, dtEnvio, hrEnvio);
								Thread.sleep(5000);
								response.add(r);
								content.append(numeroTelefone+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}	
				}
				String ctnt = content.toString();
				if(!ctnt.isEmpty()) {
					persistMessageSendingBoletosAVencer(TipoMensagem.LEMBRETE_MENSALIDADE, tipoEnvio.intValue(),
							result, ctnt, response);
				}
				return new ResponseData<String, List<?>>(ctnt, response);
			}
		}

		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

//	private String diaVencimentoMensalidade(Integer diasAnteriores){
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.MONTH, -diasAnteriores);
//		return sqlFormatter.format(cal.getTime());
//
//	}

	public ResponseData<String, List<?>> enviarNotificacaoMensalidadeBoletosRecorrenteVencidos(
			Integer tipoEnvio, Integer inFormaPagamento )
					throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<BoletoVencido> result = mensalidadeDAO.getBoletosRecorrenteVencidos(inFormaPagamento);

		if (result != null && result.size() > 0) {
			if(tipoEnvio.equals(1)) {

				String content = constroiMMSBoletosVencidos(result, true);

				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);
				
				List<Response> response = mms.send(message);

//				persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, tipoEnvio.intValue(), 
//						result, content, response);

				logger.debug("SMS Gateway Response: " + response);

				return new ResponseData<String, List<?>>(content, null);
			} else if(tipoEnvio.equals(0)) {
				StringBuffer  content = new StringBuffer();
				List<String> response = new ArrayList<String>();

				for (BoletoVencido boletoVencido : result) {
					String numeroCelular = phoneNumberCleanupBarateiro(boletoVencido.getNrcelular());
					if(numeroCelular != null){
						try {
							String mensagem = DRCARD+": " + getSMSMessage(BOLETO_VENCIDO_SMS,
									getNomeSimplificado(boletoVencido.getNmcliente()),
									boletoVencido.getDataVencimentoFormatado(),
									boletoVencido.getValorMensalidadeFormatado());
							String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
							String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

							String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio);
							Thread.sleep(5000);
							response.add(r);
							content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					String numeroTelefone = phoneNumberCleanupBarateiro(boletoVencido.getNrtelefone());
					if (numeroTelefone != null) {
						if (!numeroTelefone.equals(numeroCelular)) {
							try {
								String mensagem = DRCARD+": " + getSMSMessage(BOLETO_VENCIDO_SMS,
										getNomeSimplificado(boletoVencido.getNmcliente()),
										boletoVencido.getDataVencimentoFormatado(),
										boletoVencido.getValorMensalidadeFormatado());
								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroTelefone, dtEnvio, hrEnvio);
								Thread.sleep(5000);
								response.add(r);
								content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				String ctnt = content.toString();
				if(!ctnt.isEmpty()) {
					persistMessageSendingBoleto(TipoMensagem.COBRANCA_MENSALIDADE, tipoEnvio.intValue(), 
							result, ctnt, response);
				}

				return new ResponseData<String, List<?>>(ctnt, response);
			}
		}
		logger.info("Nenhum boleto foi encontrado");
		return null;
	}

	public ResponseData<String, List<Response>> enviarNotificacaoBoasVindasClientes()
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<Cliente> result = clienteDAO.getNovosClientesDoDia(Calendar
				.getInstance().getTime());

		if (result != null && result.size() > 0) {
			String content = constroiMMSNovosClientes(result);

			ListResource message = new ListResource(content,
					LayoutTypeEnum.TYPE_E.getType());

			logger.debug("Mensagens Enviadas: " + message);

			List<Response> response = mms.send(message);

			persistMessageSendingCliente(TipoMensagem.BOAS_VINDAS_CLIENTE, 1, result, content, response);

			logger.debug("SMS Gateway Response: " + response);

			return new ResponseData<String, List<Response>>(content, response);
		}
		logger.info("Nenhum novo cliente foi encontrado");
		return null;
	}
	
	public ResponseData<String, List<Response>> sendNotificationContratoClienteLinkCarne(Boolean isRenovacao, Long hoursInterval)
			throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");
		
		//Consultar os clientes com contratos novos em boleto
		List<NotificacaoClienteDTO> result = null;
		if(isRenovacao) {
			result = clienteDAO.getClientesContratoRenovado(hoursInterval);
		} else {
			result = clienteDAO.getClientesNovoContratoBoleto(hoursInterval);
		}

		if (result != null && result.size() > 0) {
			List<NotificacaoClienteDTO> toSend = new ArrayList<NotificacaoClienteDTO>();

			for (NotificacaoClienteDTO ncDTO : result) {
				// Consultar as mensalidades em boleto, vinculadas ao contrato
				// acima
				List<Mensalidade> mensalidades = null;
				if(isRenovacao) {
					mensalidades = mensalidadeDAO.getMensalidadesBoletoForContratoRenovado(ncDTO.getIdContrato());
				} else {
					mensalidades = mensalidadeDAO.getMensalidadeByContrato(ncDTO.getIdContrato());
				}

				// Solicitar link carne pjbank das mensalidades informadas acima
				String linkCarne = boletoServicePJB.getLinkCarne(mensalidades);
				if (linkCarne != null) {
					String shortLink = urlShortenerService
							.shortenURL(linkCarne);
					if (shortLink != null) {
						ncDTO.setLinkCarne(shortLink.replace("https://", ""));
						toSend.add(ncDTO);
					}
				}
			}
			
			if (!toSend.isEmpty()) {
				String content = constroiMMSNovoContratoCliente(toSend, isRenovacao);

				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);

				List<Response> response = mms.send(message);

//				persistMessageSendingCliente(TipoMensagem.BOAS_VINDAS_CLIENTE,
//						1, result, content, response);

				logger.debug("SMS Gateway Response: " + response);

				return new ResponseData<String, List<Response>>(content,
						response);
			}
		}
		logger.info("Nenhum contrato novo foi encontrado");
		return null;
	}

	private Date getProximoDia(Integer incremento) {
		Calendar vencimento = Calendar.getInstance();
		vencimento.add(Calendar.DAY_OF_MONTH, incremento);
		return vencimento.getTime();
	}

	private String constroiMMSNovosClientes(List<Cliente> result)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (Cliente object : result) {
			String numeroCelular = phoneNumberCleanup(object.getNrCelular());
			if (numeroCelular != null) {
				constroiMMSTypeENovosClientes(content, numeroCelular, object);
			}

			String numeroTelefone = phoneNumberCleanup(object
					.getNrTelefone());
			if (numeroTelefone != null) {
				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeENovosClientes(content, numeroTelefone,
							object);
				}
			}
		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}
	
	private String constroiMMSNovoContratoCliente(List<NotificacaoClienteDTO> result, Boolean isRenovacao)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (NotificacaoClienteDTO object : result) {
			String numeroCelular = phoneNumberCleanup(object.getNrCelular());
			if (numeroCelular != null) {
				constroiMMSTypeENovoContratoCliente(content, numeroCelular, object, isRenovacao);
			}

			String numeroTelefone = phoneNumberCleanup(object
					.getNrTelefone());
			if (numeroTelefone != null) {
				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeENovoContratoCliente(content, numeroTelefone,
							object, isRenovacao);
				}
			}
		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}

	private String constroiMMSAgendamentosDiasRestantesPorCliente(
			Collection<ViewAgendamentoSMS> result, Integer diasRestantes) throws ParseException {
		StringBuffer content = new StringBuffer();
		for (ViewAgendamentoSMS object : result) {
			
			setHorarioPrevisto(object);


			String numeroCelular = phoneNumberCleanup(object.getNrCelular());

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(sdf.format(new Date())));
//			cal.add(Calendar.DATE, diasRestantes + 1);
			String dataAgendamentoFormatado = sdf.format(object.getDtAgendamento());			

			if (numeroCelular != null) {

				constroiMMSTypeEAgendamentosDias(content, numeroCelular, object, dataAgendamentoFormatado);
			}

			String numeroTelefone = phoneNumberCleanup(object.getNrTelefone());

			if (numeroTelefone != null) {

				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeEAgendamentosDias(content, numeroTelefone,
							object, dataAgendamentoFormatado);
				}
			}
		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}
	
	private String constroiMMSBoletosVencidos(List<BoletoVencido> result)
			throws ParseException {
		return this.constroiMMSBoletosVencidos(result, false);
	}
	
	private String constroiMMSBoletosVencidos(List<BoletoVencido> result, boolean appendLink)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (BoletoVencido object : result) {
			
			String linkBoleto = null;
			if (appendLink) {
				// busca boleto associado com mensalidade
				List<BoletoBancario> boletos = boletoServicePJB
						.getBoletosBancarios(object.getIdMensalidade());
				if (boletos != null && !boletos.isEmpty()) {
					String link = boletos.get(0).getLinkBoleto();
					if (link != null) {
						link = urlShortenerService.shortenURL(link);
						logger.debug("shortlink: " + link);
						if (link != null) {
							linkBoleto = link.replace("https://", "");
						}
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							logger.debug("-->: Erro ao interromper thread (BoletoCobranca)");
						}
					}
				}
			}
			
			String numeroCelular = phoneNumberCleanup(object.getNrcelular());
			if (numeroCelular != null) {
				constroiMMSTypeEBoletoVencido(content, numeroCelular, object, linkBoleto);
			}

			String numeroTelefone = phoneNumberCleanup(object.getNrtelefone());
			if (numeroTelefone != null) {

				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeEBoletoVencido(content, numeroTelefone,
							object, linkBoleto);
				}
			}

		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}

	private String constroiMMSBoletosAVencer(ArrayList<Object[]> result)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (Object[] objects : result) {
			Mensalidade mensalidade = (Mensalidade) objects[0];
			Cliente cliente = (Cliente) objects[1];
			String numeroCelular = phoneNumberCleanup(cliente.getNrCelular());
			
			// busca link do boleto
			List<BoletoBancario> boletos = boletoServicePJB.getBoletosBancario(mensalidade);
			String linkBoleto = null;
			if(boletos != null && !boletos.isEmpty()) {
				if(boletos.get(0).getLinkBoleto() != null) {
					linkBoleto = urlShortenerService.shortenURL(boletos.get(0).getLinkBoleto());
					if(linkBoleto != null) {
						linkBoleto = linkBoleto.replace("https://", "");
					}
					try {
						Thread.sleep(3000);
					} catch(Exception e) {
						logger.debug("-->: Erro ao interromper thread (BoletosAVencer)");
					}
				}
			}
			
			if (numeroCelular != null) {
				constroiMMSTypeEBoletosAVencer(content, numeroCelular,
						mensalidade, cliente, linkBoleto);
			}

			String numeroTelefone = phoneNumberCleanup(cliente.getNrTelefone());
			if (numeroTelefone != null) { // TODO Regra pra telefone igual ao
				// celular
				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeEBoletosAVencer(content, numeroTelefone,
							mensalidade, cliente, linkBoleto);
				}
			}

		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}


	private String constroiMMSAniversariantes(Collection<AniversariantesView> result)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (AniversariantesView object : result) {
			String numeroCelular = phoneNumberCleanup(object.getNrCelular());
			if (numeroCelular != null) {
				constroiMMSTypeEAniversariantes(content, numeroCelular, object);
			}

			String numeroTelefone = phoneNumberCleanup(object
					.getNrTelefone());
			if (numeroTelefone != null) {

				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeEAniversariantes(content, numeroTelefone,
							object);
				}
			}
		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}


	private StringBuffer constroiMMSTypeENovosClientes(StringBuffer content,
			String numero, Cliente object) throws ParseException {
		return buildGenericMMSTypeE(
				content,
				numero,
				getSMSMessage(CLIENTE_NOVO_BV,
						getNomeSimplificado(object.getNmCliente())),
				getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS), DRCARD);
	}

	private StringBuffer constroiMMSTypeEBoletoVencido(StringBuffer content,
			String numero, BoletoVencido bv, String linkBoleto) throws ParseException {
		
		String complementoMsg = "";
		if(linkBoleto != null && !linkBoleto.isEmpty()) {
			complementoMsg = " boleto via " + linkBoleto;
		}

		return buildGenericMMSTypeE(
				content,
				numero,
				getSMSMessage(BOLETO_VENCIDO_SMS,
						getNomeSimplificado(bv.getNmcliente()),
						bv.getDataVencimentoFormatado(),
						(bv.getValorMensalidadeFormatado() + complementoMsg)),
				getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS), DRCARD);

	}
	
	private StringBuffer constroiMMSTypeENovoContratoCliente(StringBuffer content,
			String numero, NotificacaoClienteDTO object, Boolean isRenovacao) throws ParseException {
		String msg = null;
		if(isRenovacao) {
			msg = getSMSMessage(CLIENTE_CONTRATO_RENOVADO,
					getNomeSimplificado(object.getNmCliente()),
					object.getLinkCarne());
		} else {
			msg = getSMSMessage(CLIENTE_NOVO_CONTRATO_BOLETO,
					getNomeSimplificado(object.getNmCliente()),
					object.getLinkCarne());
		}
		return buildGenericMMSTypeE(
				content,
				numero,
				msg, getEnvioSMSInformativoNow(),
				DRCARD);
	}

//	private String constroiMMSTypeEBoletosBarateiro(String mensagem, BoletoVencido object) throws ParseException {
//
//		return getSMSMessage(mensagem,
//				getNomeSimplificado(object.getNmcliente()),
//				object.getDataVencimentoFormatado(),
//				object.getValorMensalidadeFormatado());
//
//	}

	private StringBuffer constroiMMSTypeEAniversariantes(StringBuffer content,
			String numeroCelular, AniversariantesView object) throws ParseException {

		return buildGenericMMSTypeE(
				content,
				numeroCelular,
				getSMSMessage(ANIVERSARIANTE_DIA_SMS,
						getNomeSimplificado(object.getNmCliente())),
				getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS));		
	}

	private StringBuffer buildGenericMMSTypeE(StringBuffer content,
			String number, String message, Object schedule)
					throws ParseException {
		content.append(number);
		content.append(";");
		content.append(message);
		content.append(";");
		content.append(generateShortRandomUUID());
		content.append(";"+DRCARD+";");
		content.append(schedule);
		content.append("\n");
		return content;

	}
	
	private StringBuffer buildGenericMMSTypeE(StringBuffer content,
			String number, String message, Object schedule, String empresa)
					throws ParseException {
		content.append(number);
		content.append(";");
		content.append(message);
		content.append(";");
		content.append(generateShortRandomUUID());
		content.append(";"+empresa+";");
		content.append(schedule);
		content.append("\n");
		return content;

	}

	private StringBuffer constroiMMSTypeEBoletosAVencer(StringBuffer content,
			String numero, Mensalidade mensalidade, Cliente cliente, String linkBoleto)
					throws ParseException {
		String compl = "";
		if(linkBoleto != null && !linkBoleto.isEmpty()) {
			compl = " boleto via " + linkBoleto;
		}
		return buildGenericMMSTypeE(
				content,
				numero,
				getSMSMessage(BOLETO_A_VENCER_SMS,
						getNomeSimplificado(cliente.getNmCliente()),
						mensalidade.getDataVencimentoFormatado(),
						mensalidade.getValorMensalidadeFormatado() + compl),
				getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS), DRCARD);

	}
	
	private Object getEnvioSMSInformativoNow()
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Date proximo = getProximoDia(0);
		Calendar proximoDiaDataMarcada = Calendar.getInstance(locale);
		proximoDiaDataMarcada.setTime(proximo);
		proximoDiaDataMarcada.add(Calendar.MINUTE, 1);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		return format.format(proximoDiaDataMarcada.getTime());
	}

	private Object getDataAgendamentoEnvioSMS(Integer incremento)
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Date proximo = getProximoDia(incremento);
		Calendar proximoDiaDataMarcada = Calendar.getInstance(locale);
		proximoDiaDataMarcada.setTime(proximo);
		proximoDiaDataMarcada.set(Calendar.HOUR_OF_DAY, 8);
		proximoDiaDataMarcada.set(Calendar.MINUTE, 0);
		proximoDiaDataMarcada.set(Calendar.SECOND, 0);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		return format.format(proximoDiaDataMarcada.getTime());

	}

	private String getDataAgendamentoEnvioSMSBarateiro(Integer incremento)
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Date proximo = getProximoDia(incremento);
		Calendar proximoDiaDataMarcada = Calendar.getInstance(locale);
		proximoDiaDataMarcada.setTime(proximo);
		proximoDiaDataMarcada.set(Calendar.HOUR_OF_DAY, 8);
		proximoDiaDataMarcada.set(Calendar.MINUTE, 0);
		proximoDiaDataMarcada.set(Calendar.SECOND, 0);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		return format.format(proximoDiaDataMarcada.getTime());

	}

	private String geHoraAgendamentoEnvioSMSBarateiro()
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Calendar horaEnvio = Calendar.getInstance(locale);
		horaEnvio.setTime(new Date());
		horaEnvio.set(Calendar.HOUR_OF_DAY, 8);
		horaEnvio.set(Calendar.MINUTE, 0);

		SimpleDateFormat format = new SimpleDateFormat("HH:mm");

		return format.format(horaEnvio.getTime());

	}

	public static Object getNomeSimplificado(String nomeCliente) {
		nomeCliente = nomeCliente.trim();
		if (nomeCliente != null) {
			if (nomeCliente.indexOf(" ") != -1) {
				return nomeCliente.split(" ")[0];
			}
		}
		return nomeCliente;

	}

	public String phoneNumberCleanup(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");

			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {

				if (numberStr.length() == 8) {

					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					return BR_CODE + MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					return BR_CODE + numberStr;
				}

			}
			System.out.println("Numero não reconhecido: "+numberStr);
		}

		return null;
	}

	public String phoneNumberCleanupBarateiro(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");

			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {

				if (numberStr.length() == 8) {
					System.out.println(numberStr);

					return  MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					System.out.println(numberStr);
					return  MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					System.out.println(numberStr);
					return MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					System.out.println(numberStr);
					return  numberStr;
				}



			}

			System.out.println("Numero não reconhecido: "+numberStr);
		}

		return null;
	}

	// TODO Sistema de recuperacao de mensagem aprimorado
	private String getSMSMessage(String message, Object... args) {

		return String.format(message, args);
	}

	private String generateShortRandomUUID() {
		UUID uuid = UUID.randomUUID();
		long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
		return Long.toString(l, Character.MAX_RADIX);
	}

	public static String generateNextRandomID() {
		Random rand = new Random();
		return Long.toString(rand.nextInt(BaseService.ID_MAX_LENGTH));
	}

	private StringBuffer constroiMMSTypeEAgendamentosDias(StringBuffer content,
			String numeroCelular, ViewAgendamentoSMS object, String dataAgendamentoFormatado) throws ParseException {
		if(object.getLinkPreparatorio() != null && object.getLinkPreparatorio().length() > 0){
			buildGenericMMSTypeE(
					content,
					numeroCelular,
					getSMSMessage(AGENDAMENTO_A_REALIZAR_SMS,
							getNomeSimplificado(object.getNmPaciente()),
							dataAgendamentoFormatado,object.getNmEspecialidade(), object.getNmapelido(), object.getHrOrdemInicio(), object.getHrOrdemFim()),
					getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS),MAISCONSULTA);

			return buildGenericMMSTypeE(
					content,
					numeroCelular,
					getSMSMessage(PREPARATORIO_SMS, object.getLinkPreparatorio()),
					getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS),MAISCONSULTA);
		}else {
			return buildGenericMMSTypeE(
					content,
					numeroCelular,
					getSMSMessage(AGENDAMENTO_A_REALIZAR_SMS,
							getNomeSimplificado(object.getNmPaciente()),
							dataAgendamentoFormatado,object.getNmEspecialidade(), object.getNmapelido(), object.getHrOrdemInicio(), object.getHrOrdemFim()),
					getDataAgendamentoEnvioSMS(DIAS_ADIANTE_ENVIO_SMS),MAISCONSULTA);
		}
	}
}
