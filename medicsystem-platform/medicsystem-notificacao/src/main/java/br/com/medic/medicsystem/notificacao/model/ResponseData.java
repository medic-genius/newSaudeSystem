package br.com.medic.medicsystem.notificacao.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ResponseData<T, E> {

	private T request;

	private E response;

	public ResponseData(T request, E response) {
		this.request = request;
		this.response = response;
	}

	public T getRequest() {
		return request;
	}

	public E getResponse() {
		return response;
	}

	public void setRequest(T request) {
		this.request = request;
	}

	public void setResponse(E response) {
		this.response = response;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
