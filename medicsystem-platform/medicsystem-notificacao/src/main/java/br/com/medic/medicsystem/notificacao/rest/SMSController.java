package br.com.medic.medicsystem.notificacao.rest;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.sms.bean.ListResource;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.bean.SimpleMessage;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.MultipleMessageService;
import br.com.medic.medicsystem.sms.service.SimpleMessageService;

@ApplicationScoped
@Path("/sms")
public class SMSController {

	@Inject
	private SimpleMessageService sms;

	@Inject
	private MultipleMessageService mms;

	@POST
	@Path("/simples/enviar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Response> enviarSMS(SimpleMessage message)
			throws ClientHumanException {

		return sms.send(message);

	}

	@POST
	@Path("/multiplo/enviar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Response> enviarSMSMultiplo(ListResource message)
			throws ClientHumanException {

		return mms.send(message);

	}

}