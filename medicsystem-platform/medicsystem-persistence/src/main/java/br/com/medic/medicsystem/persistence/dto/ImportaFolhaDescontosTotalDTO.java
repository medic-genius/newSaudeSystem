package br.com.medic.medicsystem.persistence.dto;

/**
 * 
 * @author Joelton Matos
 * @since 28/05/2016
 * @version 1.0
 * 
 *
 */

public class ImportaFolhaDescontosTotalDTO {
	
	private String nrCodigo;
	
	private String descricao;
	
	private Double total;
	
	public String getNrCodigo() {
		return nrCodigo;
	}
	public void setNrCodigo(String nrCodigo) {
		this.nrCodigo = nrCodigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
}
