package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tbtituloda")
public class TituloDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TITULODA_ID_SEQ")
	@SequenceGenerator(name = "TITULODA_ID_SEQ", sequenceName = "realvida.tituloda_id_seq", allocationSize = 1)
	@Column(name = "idtituloda")
	private Long id;
	
	@Basic
	@Column(name = "acrescimo")
	private Integer acrescimo;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtemissao")
	private Date dtEmissao;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name = "inexcluido")
	private Boolean inExcluido;
	
	@Basic
	@Column(name = "inmigracao")
	private Integer inMigracao;
	
	@Basic
	@Column(name = "inmotivoretorno")
	private Integer inMotivoRetorno;
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;
	
	@Basic
	@Column(name = "intipo")
	private Integer inTipo;
	
	@Basic
	@Column(name = "nmstatusbb")
	private String nmStatusBb;
	
	@Basic
	@Column(name = "nrcontacorrente")
	private String nrContaCorrente;
	
	@Basic
	@Column(name = "nrdigitoverificador")
	private String nrDigitoVerificador;
	
	@Basic
	@Column(name = "nrtitulo")
	private String nrTitulo;
	
	@Basic
	@Column(name = "vltitulo")
	private Double vlTitulo;
	
	@Basic
	@Column(name = "vltotal")
	private Double vlTotal;

	@ManyToOne
	@JoinColumn(name="idagencia")
	private Agencia agencia;

	@ManyToOne
	@JoinColumn(name="idbanco")
	private Banco banco;

	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="idmensalidade")
	private Mensalidade mensalidade;

	@ManyToOne
	@JoinColumn(name="idparcela")
	private Parcela parcela;

	@ManyToOne
	@JoinColumn(name="idrazao")
	private Razao razao;

	public TituloDa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(Integer acrescimo) {
		this.acrescimo = acrescimo;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Boolean getInExcluido() {
		return inExcluido;
	}

	public void setInExcluido(Boolean inExcluido) {
		this.inExcluido = inExcluido;
	}

	public Integer getInMigracao() {
		return inMigracao;
	}

	public void setInMigracao(Integer inMigracao) {
		this.inMigracao = inMigracao;
	}

	public Integer getInMotivoRetorno() {
		return inMotivoRetorno;
	}

	public void setInMotivoRetorno(Integer inMotivoRetorno) {
		this.inMotivoRetorno = inMotivoRetorno;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public String getNmStatusBb() {
		return nmStatusBb;
	}

	public void setNmStatusBb(String nmStatusBb) {
		this.nmStatusBb = nmStatusBb;
	}

	public String getNrContaCorrente() {
		return nrContaCorrente;
	}

	public void setNrContaCorrente(String nrContaCorrente) {
		this.nrContaCorrente = nrContaCorrente;
	}

	public String getNrDigitoVerificador() {
		return nrDigitoVerificador;
	}

	public void setNrDigitoVerificador(String nrDigitoVerificador) {
		this.nrDigitoVerificador = nrDigitoVerificador;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public Double getVlTitulo() {
		return vlTitulo;
	}

	public void setVlTitulo(Double vlTitulo) {
		this.vlTitulo = vlTitulo;
	}

	public Double getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Double vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Mensalidade getMensalidade() {
		return mensalidade;
	}

	public void setMensalidade(Mensalidade mensalidade) {
		this.mensalidade = mensalidade;
	}

	public Parcela getParcela() {
		return parcela;
	}

	public void setParcela(Parcela parcela) {
		this.parcela = parcela;
	}

	public Razao getRazao() {
		return razao;
	}

	public void setRazao(Razao razao) {
		this.razao = razao;
	}
}