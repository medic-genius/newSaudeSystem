package br.com.medic.medicsystem.persistence.dto;

public class CalEspecialidadeDTO {
	
	
	private Long idEspecialidade;
	private String nmEspecialidade;
	private Long idServico;
	private String nmServico;
	private Double vlServico;
	
	
	public Long getIdEspecialidade() {
		return idEspecialidade;
	}
	
	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}
	
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	
	public Long getIdServico() {
		return idServico;
	}
	
	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}
	
	public String getNmServico() {
		return nmServico;
	}
	
	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}
	
	public Double getVlServico() {
		return vlServico;
	}
	
	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}
	
	

}
