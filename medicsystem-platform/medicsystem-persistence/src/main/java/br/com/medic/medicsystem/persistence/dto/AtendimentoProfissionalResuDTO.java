package br.com.medic.medicsystem.persistence.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AtendimentoProfissionalResuDTO {
	
	private Long idProfissional;
	
	private String nmProfissional;
	
	private String nmUnidade;
	
	private Integer totalAtendido;
	
	private Integer totalCombinado;
	
	private Integer totalAgendamento;
	
	private Integer totalConsultaComissao;
	
	private Double totalVlComissaoConsulta;
	
	private Double vlAtendimentoConsultaComissao;
	
	private Integer totalProcedimentoComissao;
	
	private Double totalVlComissaoProcedimento;
	
	private Double vlAtendimentoProcedimentoComissao;
	
	private Double salario;
	
	private Integer totalConsultaNaoComissao;

	private Integer totalProcedimentoNaoComissao;
	
	private Double vlAtendimentoNaoComissao;

	public String getNmProfissional() {
		return nmProfissional;
	}

	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Integer getTotalAtendido() {
		return totalAtendido;
	}

	public void setTotalAtendido(Integer totalAtendido) {
		this.totalAtendido = totalAtendido;
	}

	public Integer getTotalCombinado() {
		return totalCombinado;
	}

	public void setTotalCombinado(Integer totalCombinado) {
		this.totalCombinado = totalCombinado;
	}

	public Integer getTotalAgendamento() {
		return totalAgendamento;
	}

	public void setTotalAgendamento(Integer totalAgendamento) {
		this.totalAgendamento = totalAgendamento;
	}

	public Integer getTotalConsultaComissao() {
		return totalConsultaComissao;
	}

	public void setTotalConsultaComissao(Integer totalConsultaComissao) {
		this.totalConsultaComissao = totalConsultaComissao;
	}

	public Double getTotalVlComissaoConsulta() {
		if(totalVlComissaoConsulta != null)
			return new Double(new BigDecimal(this.totalVlComissaoConsulta.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
			return totalVlComissaoConsulta;
	}

	public void setTotalVlComissaoConsulta(Double totalVlComissaoConsulta) {
		this.totalVlComissaoConsulta = totalVlComissaoConsulta;
	}

	public Double getVlAtendimentoConsultaComissao() {
		if(vlAtendimentoConsultaComissao != null)
			return new Double(new BigDecimal(this.vlAtendimentoConsultaComissao.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
			return vlAtendimentoConsultaComissao;
	}

	public void setVlAtendimentoConsultaComissao(
			Double vlAtendimentoConsultaComissao) {
		this.vlAtendimentoConsultaComissao = vlAtendimentoConsultaComissao;
	}

	public Integer getTotalProcedimentoComissao() {
		return totalProcedimentoComissao;
	}

	public void setTotalProcedimentoComissao(Integer totalProcedimentoComissao) {
		this.totalProcedimentoComissao = totalProcedimentoComissao;
	}

	public Double getTotalVlComissaoProcedimento() {
		if(totalVlComissaoProcedimento != null)
			return new Double(new BigDecimal(this.totalVlComissaoProcedimento.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
			return totalVlComissaoProcedimento;
	}

	public void setTotalVlComissaoProcedimento(Double totalVlComissaoProcedimento) {
		this.totalVlComissaoProcedimento = totalVlComissaoProcedimento;
	}

	public Double getVlAtendimentoProcedimentoComissao() {
		if(vlAtendimentoProcedimentoComissao != null)
			return new Double(new BigDecimal(this.vlAtendimentoProcedimentoComissao.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
			return vlAtendimentoProcedimentoComissao;
	}

	public void setVlAtendimentoProcedimentoComissao(
			Double vlAtendimentoProcedimentoComissao) {
		this.vlAtendimentoProcedimentoComissao = vlAtendimentoProcedimentoComissao;
	}

	public Double getSalario() {
		if(salario != null)
			return new Double(new BigDecimal(this.salario.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Integer getTotalConsultaNaoComissao() {
		return totalConsultaNaoComissao;
	}

	public void setTotalConsultaNaoComissao(Integer totalConsultaNaoComissao) {
		this.totalConsultaNaoComissao = totalConsultaNaoComissao;
	}

	public Integer getTotalProcedimentoNaoComissao() {
		return totalProcedimentoNaoComissao;
	}

	public void setTotalProcedimentoNaoComissao(Integer totalProcedimentoNaoComissao) {
		this.totalProcedimentoNaoComissao = totalProcedimentoNaoComissao;
	}

	public Double getVlAtendimentoNaoComissao() {
		if(vlAtendimentoNaoComissao != null)
			return new Double(new BigDecimal(this.vlAtendimentoNaoComissao.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
		else
		return vlAtendimentoNaoComissao;
	}

	public void setVlAtendimentoNaoComissao(Double vlAtendimentoNaoComissao) {
		this.vlAtendimentoNaoComissao = vlAtendimentoNaoComissao;
	}

	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}
	
	
	
	
	
	
	
	

}
