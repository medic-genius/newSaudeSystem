package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.ClienteConvDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Plano;
import br.com.medic.medicsystem.persistence.model.views.ClienteDependenteConvenioView;
import br.com.medic.medicsystem.persistence.model.views.EmpresaClienteContratoClienteView;


@Named("empresaCliente-dao")
@ApplicationScoped
public class EmpresaClienteDAO extends RelationalDataAccessObject<EmpresaCliente> {

	@SuppressWarnings("unchecked")
	public List<EmpresaCliente> getListaEmpresasInativas() {

		Query query = entityManager
		        .createQuery("SELECT empresa FROM EmpresaCliente empresa where empresa.dtExclusao is not null "
		        		+ "order by empresa.nmFantasia ");

		List<EmpresaCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaCliente> getListaEmpresasAtivas() {

		Query query = entityManager
		        .createQuery("SELECT empresa FROM EmpresaCliente empresa where empresa.dtExclusao is null "
		        		+ "order by empresa.nmFantasia ");

		List<EmpresaCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}


	@SuppressWarnings("unchecked")
	public List<EmpresaClienteContratoClienteView> getDadosCliente(Long idConveniada) {
		
		Query query = entityManager
		        .createQuery("SELECT ec FROM EmpresaClienteContratoClienteView ec "
				+ "WHERE ec.idEmpresaCliente = " + idConveniada);
		
		try {
			List<EmpresaClienteContratoClienteView> result = query.getResultList();
			if(result != null && result.size() > 0)
				return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ClienteConvDTO> getClientesConveniada(String nmLogin, Integer inSituacao) {

		String query = "SELECT ec.idCliente, ec.nmCliente, ec.nrCPF, ec.insituacao, ec.nrContrato, ec.idcontrato, ec.idcontratocliente"
				+ " FROM realvida.tbempresacliente_tbcontratocliente_view ec "
				+ " WHERE lower(ec.nmLogin) = lower(:nmLogin)"
				+ " AND ec.nmCliente IS NOT NULL"
				+ " AND (ec.boEmpresaCliente IS FALSE OR ec.boEmpresaCliente IS NULL)"
				+ " AND ec.insituacao = " + inSituacao
				+ " ORDER BY ec.nmCliente";
		
		try {
			return findByNativeQuery(query)
			.setParameter("nmLogin", nmLogin)
			.unwrap(SQLQuery.class)	
		        .addScalar("idCliente", LongType.INSTANCE)
		        .addScalar("nmCliente", StringType.INSTANCE)
		        .addScalar("nrCPF", StringType.INSTANCE)
		        .addScalar("inSituacao", IntegerType.INSTANCE)
		        .addScalar("nrContrato", StringType.INSTANCE)
		        .addScalar("idContrato", LongType.INSTANCE)
		        .addScalar("idContratoCliente", LongType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ClienteConvDTO.class)).list();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
	}
	
	public EmpresaCliente getEmpresaCliente(String nmLogin) {
		
		Query query = entityManager
		        .createQuery("SELECT ec FROM EmpresaCliente ec "
				+ "WHERE lower(ec.nmLogin) = lower(:nmLogin)");
		
		try {
			query.setMaxResults(1);
			query.setParameter("nmLogin", nmLogin);
			EmpresaCliente result = (EmpresaCliente) query.getSingleResult();
			
			return result;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public EmpresaCliente getEmpresaCliente(long idEmpresaCliente) {
		
		Query query = entityManager
		        .createQuery("SELECT ec FROM EmpresaCliente ec "
				+ "WHERE id = :idEmpresaCliente)");
		
		try {
			query.setMaxResults(1);
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
			EmpresaCliente result = (EmpresaCliente) query.getSingleResult();
			
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	public Contrato getContratoEmpresaCliente(Long idEmpresaCliente) {
		Query query = entityManager
		        .createQuery("SELECT con FROM Contrato con "
				+ " WHERE con.empresaCliente.id = :idEmpresaCliente"
				+ " AND con.boEmpresaCliente IS TRUE"
				+ " AND con.situacao = 0");
		try {
			query.setMaxResults(1);
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
			Contrato result = (Contrato) query.getSingleResult();
			return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public Contrato getContratoEmpresaClienteByCliente(Long idCliente, Long idConveniada){
		Query query = entityManager
		        .createQuery("SELECT cc.contrato FROM ContratoCliente cc "
				+ " WHERE cc.cliente.id = :idCliente"
				+ " AND cc.contrato.empresaCliente.id = :idConveniada"
				+ " AND (cc.contrato.boEmpresaCliente IS FALSE OR cc.contrato.boEmpresaCliente IS NULL)"
				+ " AND cc.contrato.situacao = 0"
				+ " AND cc.contrato.dtInativacao IS NULL");
		try {
			query.setMaxResults(1);
			query.setParameter("idCliente", idCliente)
				 .setParameter("idConveniada", idConveniada);
			Contrato result = (Contrato) query.getSingleResult();
			return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public List<Dependente> getDependentesByClientesEmpresaConveniada(Long idCliente, Long idEmpresaCliente){
		
		String query = "select dep.iddependente as id, dep.nmDependente, dep.inParentesco, dep.nrCpf, dep.dtNascimento, "
							+ "	cd.idContratoDependente, cd.boInativadoConveniada, cd.boFazUsoPlano"
							+" FROM realvida.tbdependente dep "
		        			+" LEFT JOIN realvida.tbcontratodependente cd ON cd.iddependente = dep.iddependente AND cd.insituacao = 0 "
		        			+ " AND (cd.boinativadoconveniada IS NULL OR cd.boinativadoconveniada IS FALSE) "
		        			+" LEFT JOIN realvida.tbcontrato c ON c.idcontrato = cd.idcontrato AND c.idempresacliente = :idEmpresaCliente "
		        			+ " AND (c.boinativadoconveniada IS NULL OR c.boinativadoconveniada IS FALSE)"
		        			+" LEFT JOIN realvida.tbcontratocliente cc ON cc.idcontrato = c.idcontrato "
		        			+" AND cc.idcliente = :idCliente AND c.insituacao = 0 AND c.dtinativacao IS NULL"
		        			+" WHERE dep.idcliente = :idCliente AND dep.dtexclusao IS NULL";

		@SuppressWarnings("unchecked")
		List<Dependente> dependentes = this.findByNativeQuery(query)
				.setParameter("idCliente", idCliente)
				 .setParameter("idEmpresaCliente", idEmpresaCliente)
				.unwrap(SQLQuery.class)
				.addScalar("id", LongType.INSTANCE)
				.addScalar("nmDependente", StringType.INSTANCE)
				.addScalar("inParentesco", IntegerType.INSTANCE)
				.addScalar("nrCpf", StringType.INSTANCE)
				.addScalar("dtNascimento", DateType.INSTANCE)
				.addScalar("idContratoDependente", LongType.INSTANCE)
				.addScalar("boInativadoConveniada", BooleanType.INSTANCE)
				.addScalar("boFazUsoPlano", BooleanType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(Dependente.class)).list();
		return dependentes;
	}

public ContratoDependente getContratoDepByClientesEmpresaConveniada(Long idCliente, Long idEmpresaCliente, Long idDependente, boolean boinativadoconveniada){

		String query = "select cd.* "
							+" FROM realvida.tbdependente dep "
		        			+" LEFT JOIN realvida.tbcontratodependente cd ON cd.iddependente = dep.iddependente AND cd.insituacao = 0 "
		        			+(boinativadoconveniada ? " AND (cd.boinativadoconveniada IS TRUE)":" AND (cd.boinativadoconveniada IS NULL OR cd.boinativadoconveniada IS FALSE) ")
		        			+" LEFT JOIN realvida.tbcontrato c ON c.idcontrato = cd.idcontrato AND c.idempresacliente = :idEmpresaCliente "
		        			+(boinativadoconveniada ?  " AND (c.boinativadoconveniada IS TRUE)" : " AND (c.boinativadoconveniada IS NULL OR c.boinativadoconveniada IS FALSE)")
		        			+" LEFT JOIN realvida.tbcontratocliente cc ON cc.idcontrato = c.idcontrato "
		        			+" AND cc.idcliente = :idCliente AND c.insituacao = 0 AND c.dtinativacao IS NULL"
		        			+" WHERE dep.idcliente = :idCliente AND dep.dtexclusao IS NULL"
		        			+ " AND dep.iddependente = :idDependente";

		@SuppressWarnings("unchecked")
		List<ContratoDependente> dependentes = findByNativeQuery(query)
				 .setParameter("idCliente", idCliente)
				 .setParameter("idEmpresaCliente", idEmpresaCliente)
				 .setParameter("idDependente", idDependente)
				 .unwrap(SQLQuery.class).addEntity(ContratoDependente.class).list();
		if(dependentes != null && dependentes.size() > 0 ){
			return dependentes.get(0);
		}

		return null;
	}

	public Plano getPlanoEmpresaCliente(long idEmpresaCliente){
		Query query = entityManager
		        .createQuery("SELECT plano FROM Plano plano "
				+ "WHERE plano.empresaCliente.id = :idEmpresaCliente"
		        + " and plano.dtExclusao is null");
		try {
			query.setMaxResults(1);
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
			Plano result = (Plano) query.getSingleResult();
			return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public List<ClienteDependenteConvenioView> getClienteDependenteConveniada(long idEmpresaCliente){

		String queryStr = "SELECT cdView FROM ClienteDependenteConvenioView cdView"
				+ " where cdView.idEmpresaCliente = " + idEmpresaCliente;

		TypedQuery<ClienteDependenteConvenioView> query = entityManager.createQuery(queryStr, ClienteDependenteConvenioView.class);
		try
		{
			List<ClienteDependenteConvenioView> result = query.getResultList();
			return result;
		} catch (Exception e)
		{
			return null;
		}
	}

	public Contrato getContratosEmpresaClienteByCliente(Long idCliente, Long idConveniada){

		Query query = entityManager
		        .createQuery("SELECT cc.contrato FROM ContratoCliente cc "
				+ " WHERE cc.cliente.id = :idCliente"
				+ " AND cc.contrato.empresaCliente.id = :idConveniada"
				+ " AND (cc.contrato.boEmpresaCliente IS FALSE OR cc.contrato.boEmpresaCliente IS NULL)"
				+ " AND cc.contrato.dtInativacao IS NULL");

		try {
			query.setMaxResults(1);
			query.setParameter("idCliente", idCliente)
				 .setParameter("idConveniada", idConveniada);
			Contrato result = (Contrato) query.getSingleResult();

			return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	

	public List<Dependente> getDependentesByContratoAndNo(Long idContrato){
		
		String query = "select "
				+ "dep.iddependente as id, "
				+ "dep.nmDependente, "
				+ "dep.inParentesco, "
				+ "dep.nrCpf, "
				+ "dep.dtNascimento, "
				+ "cd.idcontratodependente as idContratoDependente, "
				+ "cd.bofazusoplano as boFazUsoPlano, "
				+ "cc.idcontrato as idContrato"
			+" FROM realvida.tbdependente dep "
			+" inner join realvida.tbcontratocliente as cc on cc.idcontrato = :idContrato"
			+" inner join realvida.tbcliente as cli on cli.idcliente = cc.idcliente and cli.idcliente = dep.idcliente"
			+" LEFT JOIN realvida.tbcontratodependente as cd ON cd.iddependente = dep.iddependente and cd.idcontrato = :idContrato"
			+" WHERE dep.dtexclusao IS NULL";

		@SuppressWarnings("unchecked")
		List<Dependente> dependentes = this.findByNativeQuery(query)
				.setParameter("idContrato", idContrato)
				.unwrap(SQLQuery.class)
				.addScalar("id", LongType.INSTANCE)
				.addScalar("nmDependente", StringType.INSTANCE)
				.addScalar("inParentesco", IntegerType.INSTANCE)
				.addScalar("nrCpf", StringType.INSTANCE)
				.addScalar("dtNascimento", DateType.INSTANCE)
				.addScalar("idContratoDependente", LongType.INSTANCE)
				.addScalar("boFazUsoPlano", BooleanType.INSTANCE)
				.addScalar("idContrato", LongType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(Dependente.class)).list();
		return dependentes;
	}
	
	/*add 24/04/2018*/
	public List<EmpresaCliente> getEmpresaByCNPJ(String nrCNPJ) {
		List<EmpresaCliente> empresas = null;
		
		try{
			String queryStr = "SELECT emp From EmpresaCliente emp WHERE emp.nrCnpj = :nrCNPJ AND emp.dtExclusao IS NULL";
			TypedQuery<EmpresaCliente> query = entityManager.createQuery(queryStr, EmpresaCliente.class).setParameter("nrCNPJ",nrCNPJ);
			empresas = query.getResultList();
			return empresas;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}

}
