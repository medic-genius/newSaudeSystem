package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.ConferenciaEmpresaClienteDTO;
import br.com.medic.medicsystem.persistence.model.Conferencia;

@Named("conferencia-dao")
@ApplicationScoped
public class ConferenciaDAO extends RelationalDataAccessObject<Conferencia> {
	
	public Conferencia getConferenciaEmpresaClienteByMesAno(long idEmpresaCliente, String mesAno) {

		Query query = entityManager
		        .createQuery("SELECT conf FROM Conferencia conf "
		        		+ " where conf.empresaCliente.id = " + idEmpresaCliente
		        		+ " and conf.mesAno = '" + mesAno + "'");
		
		try {
			
			query.setMaxResults(1);
			Conferencia result = (Conferencia) query.getSingleResult();
			
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ConferenciaEmpresaClienteDTO> getPeriodoFaturamento(long idEmpresaCliente) {
		String queryStr = "select"
				+ " t1.mesano,"
				+ " t1.period,"
				+ " conf.dtfechamento,"
				+ " f.nmfuncionario,"
				+ " conf.observacao,"
				+ " conf.obsdadosfinanceiros,"
				+ " mensalidade.dtpagamento,"
				+ " mensalidade.vlpago,"
				+ " mensalidade.vlmensalidade,"
				+ " boleto.linkboleto "
				+ " from"
				+ "  	("
				+ " 		SELECT to_char( t, 'MM/YYYY') as mesano,"
				+ " 		to_char(t, 'YYYYMM') as period"
				+ " 		FROM generate_series(current_date - interval '6 month', current_date - interval'1 month' , INTERVAL'1 month') as t"
				+ "  	) as t1 "
				+ "left join realvida.tbconferencia conf on t1.mesano = lpad(conf.mesano,7,'0') and conf.idempresacliente = :idEmpresaCliente and conf.dtfechamento is not null "
				+ "left join realvida.tbfuncionario f on conf.idusufechamento = f.idfuncionario "
				+ "left join realvida.tbempresacliente as empresacliente on empresacliente.idempresacliente = conf.idempresacliente "
				+ "left join realvida.tbcontrato as contrato on contrato.idempresacliente = empresacliente.idempresacliente and contrato.boempresacliente is true and contrato.insituacao = 0 "
				+ "left join realvida.tbmensalidade as mensalidade on  	mensalidade.idcontrato = contrato.idcontrato  	and mensalidade.idcontrato is not null 	and lpad(mensalidade.nrmensalidade, 7, '0') = t1.mesano and mensalidade.dtexclusao is null "
				+ "left join realvida.tbboletobancario as boleto on boleto.idmensalidade = mensalidade.idmensalidade "
				+ "group by t1.mesano, t1.period, conf.dtfechamento, f.nmfuncionario, conf.observacao, "
				+ "conf.obsdadosfinanceiros, contrato.idcontrato, mensalidade.idmensalidade, "
				+ "mensalidade.nrmensalidade, boleto.linkboleto "
				+ "order by t1.period";
		try {
			List<ConferenciaEmpresaClienteDTO> conferenciaEC = this.findByNativeQuery(queryStr)
					.setParameter("idEmpresaCliente", idEmpresaCliente)					
					.unwrap(SQLQuery.class)
					.addScalar("mesAno", StringType.INSTANCE)
					.addScalar("period", StringType.INSTANCE)
					.addScalar("dtFechamento", DateType.INSTANCE)
					.addScalar("nmFuncionario", StringType.INSTANCE)
					.addScalar("observacao", StringType.INSTANCE)
					.addScalar("obsDadosFinanceiros", StringType.INSTANCE)
					.addScalar("dtPagamento", DateType.INSTANCE)
					.addScalar("vlPago", BigDecimalType.INSTANCE)
					.addScalar("vlMensalidade", BigDecimalType.INSTANCE)
					.addScalar("linkBoleto", StringType.INSTANCE)				
					.setResultTransformer(
							Transformers.aliasToBean(ConferenciaEmpresaClienteDTO.class)).list();			
									 						
			return conferenciaEC;			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}
	
	

}
