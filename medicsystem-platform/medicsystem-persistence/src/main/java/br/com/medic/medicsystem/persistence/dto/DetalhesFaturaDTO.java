package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.Conferencia;
import br.com.medic.medicsystem.persistence.model.views.DespesaConvenioView;

public class DetalhesFaturaDTO {
	
	private Conferencia conferencia;
	
	private List<DespesaConvenioView> lDespesaServicoConvenio;

	public Conferencia getConferencia() {
		return conferencia;
	}

	public void setConferencia(Conferencia conferencia) {
		this.conferencia = conferencia;
	}

	public List<DespesaConvenioView> getlDespesaServicoConvenio() {
		return lDespesaServicoConvenio;
	}

	public void setlDespesaServicoConvenio(List<DespesaConvenioView> lDespesaServicoConvenio) {
		this.lDespesaServicoConvenio = lDespesaServicoConvenio;
	}
}
