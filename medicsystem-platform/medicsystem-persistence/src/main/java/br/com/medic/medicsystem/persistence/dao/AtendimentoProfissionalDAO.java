package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoProfissionalView;

@Named("atendimentoprofissional-dao")
@ApplicationScoped
public class AtendimentoProfissionalDAO extends
        RelationalDataAccessObject<AtendimentoProfissional> {

	@SuppressWarnings("unchecked")
	public List<AtendimentoProfissionalDTO> getCalendarioSemanalProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta, Boolean boParticular, Boolean bovalidaHorarioFull) {

		String query = "SELECT AP.NMTIPOATENDIMENTO, AP.NMUNIDADE, "
		        + "AP.NMESPECIALIDADE, AP.NMSUBESPECIALIDADE, "
		        + "AP.NMDIASEMANA, AP.NMTURNO, AP.NMHRINICIO, "
		        + "AP.NMHRFIM, AP.NMTIPOCONSULTA, AP.QTPACIENTEATENDER, AP.INDIASEMANA, "
		        + "AP.nmhrtempoconsulta, AP.intipoconsulta, AP.BOPARTICULAR, AP.IDATENDIMENTOPROFISSIONAL, AP.BOPRODUCAO, AP.BOACEITAENCAIXE "
		        + "FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
		        + "JOIN REALVIDA.tbatendimentoservprofissional ASP ON AP.IDATENDIMENTOPROFISSIONAL =  ASP.IDATENDIMENTOPROFISSIONAL AND ASP.DTEXCLUSAO IS NULL "
		        + "WHERE AP.IDPROFISSIONAL = " + idProfissional;
		
		if (boParticular != null && boParticular.booleanValue()) {
			query += " and (AP.boparticular is true) ";
		}else if (boParticular != null && !boParticular.booleanValue()){
			query += " and (AP.boparticular is false or AP.boparticular is null) ";
		}
		
		if (bovalidaHorarioFull != null && bovalidaHorarioFull.booleanValue()) {
			query += " and TO_TIMESTAMP(CONCAT(current_date,' ', AP.hrordemfim), 'YYYY-MM-DD HH24:MI:SS') > current_timestamp ";
		}
		
		if(idServico != null){
					
			query += " AND ASP.IDSERVICO = " + idServico;
		}
		
		if(inTipoConsulta != null){
			
			query += " AND AP.inTipoConsulta = " + inTipoConsulta;
		}
			

		if (idUnidade != null && idUnidade > 0) {

			query += " AND AP.IDUNIDADE = " + idUnidade;
		}

		if (idEspecialidade != null && idEspecialidade > 0) {

			query += " AND AP.IDESPPROFISSIONAL = " + idEspecialidade;
		}

		if (idSubEspecialidade != null && idSubEspecialidade > 0) {

			query += " AND AP.IDSUBESPPROFISSIONAL = " + idSubEspecialidade;
		} else {

			query += " AND AP.IDSUBESPPROFISSIONAL IS NULL ";
		}
		
		query += " GROUP BY  AP.NMTIPOATENDIMENTO, AP.NMUNIDADE, AP.NMESPECIALIDADE, AP.NMSUBESPECIALIDADE, AP.NMDIASEMANA, "
				+ " AP.NMTURNO, AP.NMHRINICIO, AP.NMHRFIM, AP.NMTIPOCONSULTA, AP.QTPACIENTEATENDER, AP.INDIASEMANA, AP.nmhrtempoconsulta, "
				+ " AP.intipoconsulta,AP.INTURNO, AP.HRINICIO, AP.BOPARTICULAR, AP.IDATENDIMENTOPROFISSIONAL, AP.BOPRODUCAO, AP.BOACEITAENCAIXE "
				+ " ORDER BY AP.INDIASEMANA, AP.INTURNO, AP.HRINICIO";

		List<AtendimentoProfissionalDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmTipoAtendimento", StringType.INSTANCE)
		        .addScalar("nmUnidade", StringType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("nmSubEspecialidade", StringType.INSTANCE)
		        .addScalar("nmDiaSemana", StringType.INSTANCE)
		        .addScalar("nmTurno", StringType.INSTANCE)
		        .addScalar("nmHrInicio", StringType.INSTANCE)
		        .addScalar("nmHrFim", StringType.INSTANCE)
		        .addScalar("nmTipoConsulta", StringType.INSTANCE)
		        .addScalar("qtPacienteAtender", IntegerType.INSTANCE)
		        .addScalar("inDiaSemana", IntegerType.INSTANCE)
		        .addScalar("nmHrTempoConsulta", StringType.INSTANCE)
		        .addScalar("inTipoConsulta", IntegerType.INSTANCE)
		        .addScalar("boParticular", BooleanType.INSTANCE)
		        .addScalar("idAtendimentoProfissional", LongType.INSTANCE)
		        .addScalar("boAceitaEncaixe", BooleanType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(AtendimentoProfissionalDTO.class))
		        .list();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Collection<AgendamentoView> getConsultasMarcadasNoMes(
	        Long idProfissional, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta, String inicio,
	        String fim) {

		String queryStr = "SELECT agv.* "
		        + " FROM realvida.tbagendamento_vw agv "
		        + " WHERE agv.dtexclusao is null "
		        + " AND agv.inTipoConsulta = " + inTipoConsulta
		        + " AND agv.idfuncionario = " + idProfissional
		        + " AND agv.idunidade = " + idUnidade
		        + " AND agv.idespecialidade = " + idEspecialidade;

		if (idSubEspecialidade != null) {
			queryStr += " AND agv.idsubespecialidade = " + idSubEspecialidade;
		}
		queryStr += " AND agv.dtAgendamento between '" + inicio + "' AND '"
		        + fim + "' ORDER BY agv.dtagendamento";

		Collection<AgendamentoView> result = findByNativeQuery(queryStr,
		        AgendamentoView.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public HashMap<String, String> getDisponibilidadeProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta, String inicio,
	        String fim, Boolean boParticular) {

		String queryStr = "SELECT agv.dtagendamento, concat(count(agv.idagendamento), '/', "
		        + " COALESCE(( SELECT sum(AP.QTPACIENTEATENDER) "
		        + " FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
		        + " WHERE 1 = 1 ";
		if (inTipoConsulta != null) {
			queryStr += " AND AP.inTipoConsulta = " + inTipoConsulta;
		}
		
		if (boParticular != null && boParticular.booleanValue()) {
			queryStr += "AND (AP.BOPARTICULAR IS TRUE) ";
		}else if (boParticular != null && !boParticular.booleanValue()){
			queryStr += "AND (AP.BOPARTICULAR IS FALSE OR AP.BOPARTICULAR IS NULL) ";
		}
		
		queryStr += " AND AP.IDPROFISSIONAL = " + idProfissional
		        + "	AND AP.IDUNIDADE = " + idUnidade
		        + " AND AP.IDESPPROFISSIONAL = " + idEspecialidade;
		
//		if(idServico != null){
//			queryStr += " AND ASP.IDSERVICO  = " + idServico;
//		}
		
		if (idSubEspecialidade != null) {
			queryStr += " AND AP.IDSUBESPPROFISSIONAL = " + idSubEspecialidade;
		}
		queryStr += "	AND AP.INDIASEMANA = cast(to_char(agv.dtagendamento,'D') as int)  - 1 "
		        + "	GROUP BY AP.IDPROFISSIONAL, AP.INDIASEMANA "
		        + " ORDER BY AP.INDIASEMANA ),0), '/', "
		        
		        
		 + " case"
		 + " when "
	    	 + " (COALESCE((select count(agvv.idagendamento) "
	    	 + " FROM realvida.tbagendamento_vw agvv"
	    	 + " inner join REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW app on agvv.idfuncionario = app.idprofissional and agvv.idespecialidade = app.idespprofissional"
	    	 + " inner join realvida.tbatendimentoservprofissional aspp on app.idatendimentoprofissional = aspp.idatendimentoprofissional and aspp.dtexclusao is null "
	    	 + " WHERE cast(agvv.hragendamento as time) between app.hrinicio and app.hrfim"
	    	 + " and agvv.dtagendamento = agv.dtagendamento"
	    	 + " AND app.INDIASEMANA = cast(to_char(agv.dtagendamento,'D') as int)  - 1"
	    	 + " and agvv.dtexclusao is null "
	    	 + " AND agvv.idespecialidade = "+idEspecialidade;
	    	 
				if (boParticular != null && boParticular.booleanValue()) {
					queryStr += "AND (APP.BOPARTICULAR IS TRUE) ";
				}else if (boParticular != null && !boParticular.booleanValue()){
					queryStr += "AND (APP.BOPARTICULAR IS FALSE OR APP.BOPARTICULAR IS NULL) ";
				}
		
		queryStr += " AND agvv.idunidade = "+idUnidade
	    	 + " and app.idprofissional = " + idProfissional
	    	 + " and aspp.idservico = " + idServico
	    	 + " AND agvv.inTipoConsulta = "+inTipoConsulta+"), 0)) "
		       
	    	+" < "
		        
		+ " (COALESCE(( SELECT sum(AP.QTPACIENTEATENDER) " 
		+ " FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP " 
			    				+ " inner join realvida.tbatendimentoservprofissional asp on ap.idatendimentoprofissional = asp.idatendimentoprofissional and asp.dtexclusao is null "
			    				+ " WHERE 1 = 1  "
			    				+ " AND AP.inTipoConsulta = " + inTipoConsulta
			    				+ " AND AP.IDPROFISSIONAL = " + idProfissional
			    				+ " AND AP.IDUNIDADE = " + idUnidade;
								if (boParticular != null && boParticular.booleanValue()) {
									queryStr += "AND (AP.BOPARTICULAR IS TRUE) ";
								}else if (boParticular != null && !boParticular.booleanValue()){
									queryStr += "AND (AP.BOPARTICULAR IS FALSE OR AP.BOPARTICULAR IS NULL) ";
								}
			    				queryStr += " AND AP.IDESPPROFISSIONAL  = " + idEspecialidade
			    				+ " AND AP.INDIASEMANA = cast(to_char(agv.dtagendamento,'D') as int)  - 1" 	
			    				+ " and asp.idservico =  " + idServico
			    				+ " GROUP BY AP.IDPROFISSIONAL, AP.INDIASEMANA  ORDER BY AP.INDIASEMANA ),0))"
			    				+ " THEN 1"
			    			+ " ELSE 0 "
			    			+ " END ) as validatendservico "  
		        + " FROM realvida.tbagendamento_vw agv "
		        + " WHERE agv.dtexclusao is null ";
		if (inTipoConsulta != null) {
			queryStr += " AND agv.inTipoConsulta = " + inTipoConsulta;
		}
		queryStr += " AND agv.idfuncionario = " + idProfissional
		        + " AND agv.idunidade = " + idUnidade
		        + " AND agv.idespecialidade = " + idEspecialidade;
		
		
		if (idSubEspecialidade != null) {
			queryStr += " AND agv.idsubespecialidade = " + idSubEspecialidade;
		}
		
		if (boParticular != null && boParticular.booleanValue()) {
			queryStr += "AND (AGV.BOPARTICULAR IS TRUE) ";
		}else if (boParticular != null && !boParticular.booleanValue()){
			queryStr += "AND (AGV.BOPARTICULAR IS FALSE OR AGV.BOPARTICULAR IS NULL) ";
		}
		
		queryStr += " AND agv.dtAgendamento between '" + inicio + "' AND '"
		        + fim + "' GROUP BY agv.dtagendamento, agv.idfuncionario "
		        + " ORDER BY agv.dtagendamento";

		@SuppressWarnings("unchecked")
		List<Object[]> result = findByNativeQuery(queryStr).getResultList();

		HashMap<String, String> hashmapResult = new HashMap<String, String>();

		for (Object[] objects : result) {
			hashmapResult.put(((Date) objects[0]).toString(),
			        (String) objects[1]);
		}

		return hashmapResult;
	}

	public List<Integer> getDiasDaSemanaAtendimentoProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta) {
		String queryStr = "SELECT AP.INDIASEMANA "
		        + " FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
		        + " JOIN REALVIDA.TBATENDIMENTOSERVPROFISSIONAL ASP "
		        + " ON AP.IDATENDIMENTOPROFISSIONAL = ASP.IDATENDIMENTOPROFISSIONAL AND ASP.DTEXCLUSAO IS NULL "
		        + " WHERE 1=1 ";
		if (inTipoConsulta != null) {
			queryStr += " AND AP.INTIPOCONSULTA = " + inTipoConsulta;
		}
		queryStr += " AND AP.IDPROFISSIONAL = " + idProfissional
		        + "	AND AP.IDUNIDADE = " + idUnidade
		        + " AND AP.IDESPPROFISSIONAL = " + idEspecialidade;
		if (idSubEspecialidade != null) {
			queryStr += " AND AP.IDSUBESPPROFISSIONAL " + idSubEspecialidade;
		}
		
		if (idServico != null) {
			queryStr += " AND ASP.IDSERVICO = " + idServico;
		}

		queryStr += " GROUP BY AP.INDIASEMANA ORDER BY AP.INDIASEMANA";

		Query q = findByNativeQuery(queryStr);
		@SuppressWarnings("unchecked")
		List<Integer> result = (List<Integer>) q.getResultList();

		return result;

	}
	
	public String getDiasDaSemanaAtendimentoProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, List<Long> idUnidades, 
	        Integer inTipoConsulta, String dataVaga, boolean boParticular) {
		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String StrDiaVaga = sqlFormatter.format(new java.util.Date());
		
		String queryStr = "SELECT concat (AP.INDIASEMANA,'/',AP.idunidade,'/',"
				+ " AP.nmapelido,'/',AP.nmlogradouro,'/', AP.nrnumero,'/', AP.nmbairro,'/', AP.nmcidade) "
		        + " FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
		        + " JOIN REALVIDA.TBATENDIMENTOSERVPROFISSIONAL ASP "
		        + " ON AP.IDATENDIMENTOPROFISSIONAL = ASP.IDATENDIMENTOPROFISSIONAL AND ASP.DTEXCLUSAO IS NULL "
		        + " WHERE 1=1 "
		        + " AND AP.INDIASEMANA = cast(to_char(cast ('"+dataVaga+"' as date),'D') as int)  - 1 ";
		
		if (boParticular) {
			queryStr += "AND (AP.BOPARTICULAR IS TRUE) ";
		}else{
			queryStr += "AND (AP.BOPARTICULAR IS FALSE OR AP.BOPARTICULAR IS NULL) ";
		}
		
		if(StrDiaVaga.equals(dataVaga)){
			queryStr += "AND TO_TIMESTAMP(CONCAT(current_date,' ', AP.hrordemfim), 'YYYY-MM-DD HH24:MI:SS') > current_timestamp ";
		}
		
		if (inTipoConsulta != null) {
			queryStr += " AND AP.INTIPOCONSULTA = " + inTipoConsulta;
		}
		queryStr += " AND AP.IDPROFISSIONAL = " + idProfissional ;
		queryStr += idUnidades != null &&  idUnidades.size() > 0 ? " AND AP.IDUNIDADE NOT IN (:unidades)" : "";
		queryStr += " AND AP.IDESPPROFISSIONAL = " + idEspecialidade;
		
		if (idServico != null) {
			queryStr += " AND ASP.IDSERVICO = " + idServico;
		}

		queryStr +=  " ORDER BY AP.hrinicio limit 1";

		Query q = idUnidades != null &&  idUnidades.size() > 0 ?
				findByNativeQuery(queryStr).setParameter("unidades", idUnidades) :
				findByNativeQuery(queryStr);
		
		
		String result = (String) q.unwrap(SQLQuery.class).uniqueResult();

		return result;

	}

	public List<String> getHorariosConsultasMarcadasNoDia(Long idProfissional,
	        Long idEspecialidade, Long idUnidade, Long idSubEspecialidade,
	        Integer inTipoConsulta, String data, Boolean boParticular) {

		String queryStr = "SELECT agv.hragendamento "
		        + " FROM realvida.tbagendamento_vw agv "
		        + " WHERE agv.dtexclusao is null "
		        + " AND agv.inTipoConsulta = " + inTipoConsulta
		        + " AND agv.idfuncionario = " + idProfissional
		        + " AND agv.idunidade = " + idUnidade
		        + " AND agv.idespecialidade = " + idEspecialidade;

		if (idSubEspecialidade != null) {
			queryStr += " AND agv.idsubespecialidade = " + idSubEspecialidade;
		}
		
		if (boParticular != null && boParticular.booleanValue()) {
			queryStr += " and (agv.boparticular is true) ";
		}else if (boParticular != null && !boParticular.booleanValue()){
			queryStr += " and (agv.boparticular is false or agv.boparticular is null) ";
		}
		queryStr += " AND agv.dtAgendamento = '" + data
		        + "' ORDER BY agv.dtagendamento";

		Query q = findByNativeQuery(queryStr);
		@SuppressWarnings("unchecked")
		List<String> result = (List<String>) q.getResultList();

		return result;
	}
	
	public Collection<Profissional> getProfissionaisPorUnidade(Long idUnidade) {

		String query = "SELECT DISTINCT func.*, prof.intipoprofissional, prof.nrcrmcro"
		        + " FROM realvida.tbAtendimentoProfissional_View ap"
		        + " INNER JOIN realvida.tbprofissional prof ON prof.idfuncionario = ap.idprofissional"
		        + " INNER JOIN realvida.tbfuncionario func ON func.idfuncionario = prof.idfuncionario"
		        + " WHERE ap.inTipoAtendimento = inTipoAtendimento ";

		if (idUnidade != null && idUnidade > 0) {
			query += " AND ap.idUnidade = " + idUnidade;
		}


		query += " ORDER BY func.nmfuncionario";

		Collection<Profissional> result = findByNativeQuery(query,
		        Profissional.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	
	public List<AtendimentoProfissionalView> getProfissionaisByEspecialidade(Long idEspecialidade, Long idProfissional, boolean boParticular){
		
		String query = "SELECT distinct ap.idprofissional, ap.nmprofissional, ap.idespprofissional," 
						+" ap.intipoconsulta, apserv.idservico, serv.nmservico, ap.nrcrmcro"
						+" FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
						+" join REALVIDA.tbatendimentoservprofissional apserv" 
						+" on apserv.idatendimentoprofissional = AP.idatendimentoprofissional"
						+" join realvida.tbservico serv on serv.idservico =  apserv.idservico"
						+" WHERE apserv.dtexclusao is null";
						if (boParticular) {
							query += " AND (AP.BOPARTICULAR IS TRUE) ";
						}else{
							query += " AND (AP.BOPARTICULAR IS FALSE OR AP.BOPARTICULAR IS NULL) ";
						}
						
						if (idProfissional != null) {
							query += " AND ap.idprofissional = "+idProfissional;
						}
		
						query +=" and ap.idespprofissional = :idEspecialidade"
						+" and serv.bosite is true";
		
		@SuppressWarnings("unchecked")
		List<AtendimentoProfissionalView> result = findByNativeQuery(query)
				.setParameter("idEspecialidade", idEspecialidade)
		        .unwrap(SQLQuery.class)
		        .addScalar("idProfissional", LongType.INSTANCE)
		        .addScalar("nmProfissional", StringType.INSTANCE)
		        .addScalar("idEspProfissional", LongType.INSTANCE)
		        .addScalar("inTipoConsulta", IntegerType.INSTANCE)
		        .addScalar("idServico", LongType.INSTANCE)
		        .addScalar("nmServico", StringType.INSTANCE)
		        .addScalar("nrCrmCro", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(AtendimentoProfissionalView.class))
		        .list();
		
		return result;
		
	}

	public AtendimentoProfissional getAtendimentoProfissional(Date dtagendamento, Long idProfissional, Long idUnidade, Date hragendamento, Long idEspecialidade) {
		
		AtendimentoProfissional result = null;
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtagendamento);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK) - 1;
						
		String queryStr = "SELECT ap FROM AtendimentoProfissional ap "				
				+ " where ap.inDiaSemana = " + diaSemana
		        + " and ap.profissional.id = " + idProfissional
		        + " and ap.unidade.id = " + idUnidade
		        + " and '" + df.format(hragendamento) + "' between ap.hrInicio and ap.hrFim "
		        + " and ap.espProfissional.especialidade.id = " + idEspecialidade;
						
		try {
			TypedQuery<AtendimentoProfissional> query = entityManager.createQuery(queryStr,
					AtendimentoProfissional.class);

			query.setMaxResults(1);
			result = query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}		

		return result;
	}
	
	public BigInteger getAtendimentoProfissional(Date dtagendamento, Long idProfissional, Long idUnidade) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtagendamento);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK) - 1;
		
		String queryStr = "select count(*) from( "
				+ " select inturno from realvida.tbatendimentoprofissional "
				+ " where idfuncionario = "+ idProfissional +" and indiasemana = " + diaSemana
				+ " and idunidade = "+ idUnidade
				+ " group by inturno) t1 ";
										
		try {			
			
			Query q = findByNativeQuery(queryStr);
		
			BigInteger result = (BigInteger) q.getSingleResult();
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}		

		
	}
	
	public List<AtendimentoProfissionalView> getAtendimentoProfissionalVWByDiaSemana(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Integer inTipoConsulta, Integer inDiaSemana, Boolean boProducao) {
		String queryStr = "SELECT AP.* "
		        + " FROM REALVIDA.TBATENDIMENTOPROFISSIONAL_VIEW AP "
		        + " JOIN REALVIDA.TBATENDIMENTOSERVPROFISSIONAL ASP "
		        + " ON AP.IDATENDIMENTOPROFISSIONAL = ASP.IDATENDIMENTOPROFISSIONAL AND ASP.DTEXCLUSAO IS NULL "
		        + " WHERE 1=1 ";
		if (inTipoConsulta != null) {
			queryStr += " AND AP.INTIPOCONSULTA = " + inTipoConsulta;
		}
		queryStr += " AND AP.IDPROFISSIONAL = " + idProfissional
		        + "	AND AP.IDUNIDADE = " + idUnidade
		        + " AND AP.IDESPPROFISSIONAL = " + idEspecialidade;
		if (inDiaSemana != null) {
			queryStr += " AND AP.INDIASEMANA = " + inDiaSemana;
		}
		
		if (idServico != null) {
			queryStr += " AND ASP.IDSERVICO = " + idServico;
		}
		
		if (boProducao != null && boProducao.booleanValue()) {
			queryStr += " AND AP.BOPRODUCAO IS TRUE ";
		}else if (boProducao != null && !boProducao.booleanValue()){
			queryStr += " AND (AP.BOPRODUCAO IS NULL OR AP.BOPRODUCAO IS FALSE) ";
		}

//		queryStr += " GROUP BY AP.INDIASEMANA ORDER BY AP.INDIASEMANA";

		Query q = entityManager.createNativeQuery(queryStr, AtendimentoProfissionalView.class);
		@SuppressWarnings("unchecked")
		List<AtendimentoProfissionalView> result =  q.getResultList();

		return result;

	}

}
