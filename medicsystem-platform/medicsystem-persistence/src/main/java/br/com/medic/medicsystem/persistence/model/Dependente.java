package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbdependente")
public class Dependente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPENDENTE_ID_SEQ")
	@SequenceGenerator(name = "DEPENDENTE_ID_SEQ", sequenceName = "realvida.dependente_id_seq", allocationSize = 1)
	@Column(name = "iddependente")
	private Long id;

	@Basic
	@Column(name = "btfotodependente")
	private byte[] btFotoDependente;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtnascimento")
	private Date dtNascimento;

	@Basic
	@Column(name = "iddependenteaux")
	private Long idDependenteAux;

	@Basic
	@Column(name = "iddependenteodonto")
	private Long idDependenteOdonto;

	@Basic
	@Column(name = "idplanoaux")
	private Long idPlanoAux;

	@Basic
	@Column(name = "idplanoodonto")
	private Long idPlanoOdonto;

	@Basic
	@Column(name = "inparentesco")
	private Integer inParentesco;

	@Basic
	@Column(name = "insexo")
	private Integer inSexo;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nmdependente")
	private String nmDependente;

	@Basic
	@Column(name = "nmemail")
	private String nmEmail;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nmpressaoarterial")
	private String nmPressaoArterial;

	@Basic
	@Column(name = "nmpressaoarterialexcelente")
	private String nmPressaoArterialExcelente;

	@Basic
	@Column(name = "nmtiposanguineo")
	private String nmTipoSanguineo;

	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;

	@Basic
	@Column(name = "nrcodcliente")
	private String nrCodCliente;

	@Basic
	@Column(name = "nrcpf")
	private String nrCpf;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;

	@Basic
	@Column(name = "observacao")
	private String observacao;

	@Basic
	@Column(name = "txglicose")
	private Integer txGlicose;

	@Basic
	@Column(name = "vlaltura")
	private Double vlAltura;

	@Basic
	@Column(name = "vlpeso")
	private Double vlPeso;

	@Basic
	@Column(name = "vltemperatura")
	private Integer vlTemperatura;

	@Basic
	@Column(name = "digitaldependente")
	private String digitalDependente;
	
	@Basic
	@Column(name = "fotodependente")
	@JsonIgnore
	private byte[] fotoDependente;
	
	@Transient
	@JsonIgnore
	private String fotoDependenteDecoded;
	
	@Transient
	@JsonIgnore
	private String fotoDependenteEncoded;

	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	@JsonIgnore
	@Transient
	private String parentescoFormatado;
	
	@Transient
	@JsonIgnore
	private Integer idade;
	
	@Basic
	@Column(name = "iddedodigital")
	private Integer idDedoDigital;
	
	@Transient
	@JsonProperty
	private Long idContratoDependente;

	@Transient
	@JsonProperty
	private Boolean boInativadoConveniada;
	
	@Transient
	@JsonProperty
	private Boolean boFazUsoPlano;
	
	@Transient
	@JsonProperty
	private Long idContrato;

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Dependente() {
	}

	public Boolean getBoFazUsoPlano() {
		return boFazUsoPlano;
	}

	public void setBoFazUsoPlano(Boolean boFazUsoPlano) {
		this.boFazUsoPlano = boFazUsoPlano;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@JsonIgnore
	public byte[] getBtFotoDependente() {
		return btFotoDependente;
	}

	public void setBtFotoDependente(byte[] btFotoDependente) {
		this.btFotoDependente = btFotoDependente;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Long getIdDependenteAux() {
		return idDependenteAux;
	}

	public void setIdDependenteAux(Long idDependenteAux) {
		this.idDependenteAux = idDependenteAux;
	}

	public Long getIdDependenteOdonto() {
		return idDependenteOdonto;
	}

	public void setIdDependenteOdonto(Long idDependenteOdonto) {
		this.idDependenteOdonto = idDependenteOdonto;
	}

	public Long getIdPlanoAux() {
		return idPlanoAux;
	}

	public void setIdPlanoAux(Long idPlanoAux) {
		this.idPlanoAux = idPlanoAux;
	}

	public Long getIdPlanoOdonto() {
		return idPlanoOdonto;
	}

	public void setIdPlanoOdonto(Long idPlanoOdonto) {
		this.idPlanoOdonto = idPlanoOdonto;
	}

	public Integer getInParentesco() {
		return inParentesco;
	}

	public void setInParentesco(Integer inParentesco) {
		this.inParentesco = inParentesco;
	}

	public Integer getInSexo() {
		return inSexo;
	}

	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNmPressaoArterial() {
		return nmPressaoArterial;
	}

	public void setNmPressaoArterial(String nmPressaoArterial) {
		this.nmPressaoArterial = nmPressaoArterial;
	}

	public String getNmPressaoArterialExcelente() {
		return nmPressaoArterialExcelente;
	}

	public void setNmPressaoArterialExcelente(String nmPressaoArterialExcelente) {
		this.nmPressaoArterialExcelente = nmPressaoArterialExcelente;
	}

	public String getNmTipoSanguineo() {
		return nmTipoSanguineo;
	}

	public void setNmTipoSanguineo(String nmTipoSanguineo) {
		this.nmTipoSanguineo = nmTipoSanguineo;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getTxGlicose() {
		return txGlicose;
	}

	public void setTxGlicose(Integer txGlicose) {
		this.txGlicose = txGlicose;
	}

	public Double getVlAltura() {
		return vlAltura;
	}

	public void setVlAltura(Double vlAltura) {
		this.vlAltura = vlAltura;
	}

	public Double getVlPeso() {
		return vlPeso;
	}

	public void setVlPeso(Double vlPeso) {
		this.vlPeso = vlPeso;
	}

	public Integer getVlTemperatura() {
		return vlTemperatura;
	}

	public void setVlTemperatura(Integer vlTemperatura) {
		this.vlTemperatura = vlTemperatura;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@JsonProperty
	public String getParentescoFormatado() {
		if(null != getInParentesco())
		{
			switch (getInParentesco()) {
			case 0:
				return "Esposo(a)";
			case 1:
				return "Companheiro(a)";
			case 2:
				return "Filho(a)";
			case 3:
				return "Filho(a) Adotivo(a)";
			case 4:
				return "Pai";
			case 5:
				return "Mãe";
			case 6:
				return "Irmão(a)";
			case 7:
				return "Avô/Avó";
			case 8:
				return "Sobrinho(a)";
			case 9:
				return "Enteado(a)";
			case 10:
				return "Sogro(a)";
			case 11:
				return "Neto(a)";
			case 12:
				return "Tio(a)";
			case 13:
				return "Primo(a)";
			case 14:
				return "Cunhado(a)";
			case 15:
				return "Nora";
			case 16:
				return "Genro";
			case 17:
				return "Amigo(a)";
			case 18:
				return "Outros";
			case 19:
				return "Não Informado";
			default:
				break;
			}
		}
		return null;
	}

	public String getDigitalDependente() {
		return digitalDependente;
	}

	public void setDigitalDependente(String digitalDependente) {
		this.digitalDependente = digitalDependente;
	}
	
	@JsonIgnore
	public String getFotoDependenteDecoded() {
		if (getFotoDependente() != null) {

			return new String(getFotoDependente());
		}
		return null;
	}
	
	@JsonIgnore
	public void setFotoDependenteDecoded(String fotoDependenteDecoded) {
		this.fotoDependenteDecoded = fotoDependenteDecoded;
	}
	
	@JsonProperty
	public String getFotoDependenteEncoded() {
		return fotoDependenteEncoded;
	}
	
	@JsonProperty
	public void setFotoDependenteEncoded(String fotoDependenteEncoded) {
		this.fotoDependenteEncoded = fotoDependenteEncoded;
	}

	public byte[] getFotoDependente() {
		return fotoDependente;
	}

	public void setFotoDependente(byte[] fotoDependente) {
		this.fotoDependente = fotoDependente;
	}
	
	public Integer getIdDedoDigital() {
		return idDedoDigital;
	}

	public void setIdDedoDigital(Integer idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}
	
	@JsonProperty
	public Integer getIdade() {
		GregorianCalendar hj=new GregorianCalendar();
		GregorianCalendar nascimento=new GregorianCalendar();
		
		if(getDtNascimento() != null){
				nascimento.setTime(getDtNascimento());
						
				int anohj=hj.get(Calendar.YEAR);
				int anoNascimento=nascimento.get(Calendar.YEAR);
				
				
				 idade = anohj-anoNascimento;
				
				 
				 if ( hj.get(Calendar.MONTH) < nascimento.get(Calendar.MONTH)) {
					 idade --;  
				    }
				
				return idade;
		
		}
		
		return 0;
	}

	public Long getIdContratoDependente() {
		return idContratoDependente;
	}

	public void setIdContratoDependente(Long idContratoDependente) {
		this.idContratoDependente = idContratoDependente;
	}

	public Boolean getBoInativadoConveniada() {
		return boInativadoConveniada;
	}

	public void setBoInativadoConveniada(Boolean boInativadoConveniada) {
		this.boInativadoConveniada = boInativadoConveniada;
	}


	
	


	
	

}