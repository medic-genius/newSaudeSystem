package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.MedicamentoReceituario;

public class ReceituarioDTO {
	
	
	private Long id;
	
	private Date dtReceituario;
	
	private String nmReceituario;
	
	private List<MedicamentoReceituario> medicamentos;
	
	private String recomendacoes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtReceituario() {
		return dtReceituario;
	}

	public void setDtReceituario(Date dtReceituario) {
		this.dtReceituario = dtReceituario;
	}

	public String getNmReceituario() {
		return nmReceituario;
	}

	public void setNmReceituario(String nmReceituario) {
		this.nmReceituario = nmReceituario;
	}

	public List<MedicamentoReceituario> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<MedicamentoReceituario> medicamentos) {
		this.medicamentos = medicamentos;
	}

	public String getRecomendacoes() {
		return recomendacoes;
	}

	public void setRecomendacoes(String recomendacoes) {
		this.recomendacoes = recomendacoes;
	}
}