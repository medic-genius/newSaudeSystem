package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbcredenciada")
public class Credenciada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CREDENCIADA_ID_SEQ")
	@SequenceGenerator(name = "CREDENCIADA_ID_SEQ", sequenceName = "realvida.credenciada_id_seq", allocationSize = 1)
	@Column(name = "idcredenciada")
	private Long id;

	@Basic
	@Column(name = "bogerafatura")
	private Boolean boGeraFatura;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtcadastro")
	private Date dtCadastro;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idcredenciadaaux")
	private Long idCredenciadaAux;

	@Basic
	@Column(name = "idcredenciadaodonto")
	private Long idCredenciadaOdonto;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "inlocalatendimento")
	private Integer inLocalAtendimento;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nmemail")
	private String nmEmail;

	@Basic
	@Column(name = "nmfantasia")
	private String nmFantasia;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nmrazaosocial")
	private String nmRazaoSocial;

	@Basic
	@Column(name = "nmsite")
	private String nmSite;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;

	@Basic
	@Column(name = "nrcnpj")
	private String nrCnpj;

	@Basic
	@Column(name = "nrcpf")
	private String nrCpf;

	@Basic
	@Column(name = "nrcredenciada")
	private String nrCredenciada;

	@Basic
	@Column(name = "nrfax")
	private String nrFax;

	@Basic
	@Column(name = "nrinscestadual")
	private String nrInscEstadual;

	@Basic
	@Column(name = "nrinscmunicipal")
	private String nrInscMunicipal;

	@Basic
	@Column(name = "nrinscsuframa")
	private String nrInscSuframa;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nrtelefone1")
	private String nrTelefone1;

	@Basic
	@Column(name = "nrtelefone2")
	private String nrTelefone2;

	@Basic
	@Column(name = "vlch")
	private Double vlCh;

	@JsonIgnore
	@OneToMany(mappedBy = "credenciada")
	private List<Conferencia> conferencias;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	@JsonIgnore
	@OneToMany(mappedBy = "credenciada")
	private List<Encaminhamento> encaminhamentos;

	@JsonIgnore
	@OneToMany(mappedBy = "credenciada")
	private List<ServicoCredenciada> servicoCredenciadas;

	@Transient
	@JsonIgnore
	private String descricao;

	public Credenciada() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoGeraFatura() {
		return boGeraFatura;
	}

	public void setBoGeraFatura(Boolean boGeraFatura) {
		this.boGeraFatura = boGeraFatura;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdCredenciadaAux() {
		return idCredenciadaAux;
	}

	public void setIdCredenciadaAux(Long idCredenciadaAux) {
		this.idCredenciadaAux = idCredenciadaAux;
	}

	public Long getIdCredenciadaOdonto() {
		return idCredenciadaOdonto;
	}

	public void setIdCredenciadaOdonto(Long idCredenciadaOdonto) {
		this.idCredenciadaOdonto = idCredenciadaOdonto;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Integer getInLocalAtendimento() {
		return inLocalAtendimento;
	}

	public void setInLocalAtendimento(Integer inLocalAtendimento) {
		this.inLocalAtendimento = inLocalAtendimento;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}

	public String getNmSite() {
		return nmSite;
	}

	public void setNmSite(String nmSite) {
		this.nmSite = nmSite;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrCnpj() {
		return nrCnpj;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrCredenciada() {
		return nrCredenciada;
	}

	public void setNrCredenciada(String nrCredenciada) {
		this.nrCredenciada = nrCredenciada;
	}

	public String getNrFax() {
		return nrFax;
	}

	public void setNrFax(String nrFax) {
		this.nrFax = nrFax;
	}

	public String getNrInscEstadual() {
		return nrInscEstadual;
	}

	public void setNrInscEstadual(String nrInscEstadual) {
		this.nrInscEstadual = nrInscEstadual;
	}

	public String getNrInscMunicipal() {
		return nrInscMunicipal;
	}

	public void setNrInscMunicipal(String nrInscMunicipal) {
		this.nrInscMunicipal = nrInscMunicipal;
	}

	public String getNrInscSuframa() {
		return nrInscSuframa;
	}

	public void setNrInscSuframa(String nrInscSuframa) {
		this.nrInscSuframa = nrInscSuframa;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNrTelefone1() {
		return nrTelefone1;
	}

	public void setNrTelefone1(String nrTelefone1) {
		this.nrTelefone1 = nrTelefone1;
	}

	public String getNrTelefone2() {
		return nrTelefone2;
	}

	public void setNrTelefone2(String nrTelefone2) {
		this.nrTelefone2 = nrTelefone2;
	}

	public Double getVlCh() {
		return vlCh;
	}

	public void setVlCh(Double vlCh) {
		this.vlCh = vlCh;
	}

	public List<Conferencia> getConferencias() {
		return conferencias;
	}

	public void setConferencias(List<Conferencia> conferencias) {
		this.conferencias = conferencias;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Encaminhamento> getEncaminhamentos() {
		return encaminhamentos;
	}

	public void setEncaminhamentos(List<Encaminhamento> encaminhamentos) {
		this.encaminhamentos = encaminhamentos;
	}

	public List<ServicoCredenciada> getServicoCredenciadas() {
		return servicoCredenciadas;
	}

	public void setServicoCredenciadas(
	        List<ServicoCredenciada> servicoCredenciadas) {
		this.servicoCredenciadas = servicoCredenciadas;
	}

	public Conferencia addConferencia(Conferencia conferencia) {
		getConferencias().add(conferencia);
		conferencia.setCredenciada(this);

		return conferencia;
	}

	public Conferencia removeConferencia(Conferencia conferencia) {
		getConferencias().remove(conferencia);
		conferencia.setCredenciada(null);

		return conferencia;
	}

	public Encaminhamento addEncaminhamento(Encaminhamento encaminhamento) {
		getEncaminhamentos().add(encaminhamento);
		encaminhamento.setCredenciada(this);

		return encaminhamento;
	}

	public Encaminhamento removeEncaminhamento(Encaminhamento encaminhamento) {
		getEncaminhamentos().remove(encaminhamento);
		encaminhamento.setCredenciada(null);

		return encaminhamento;
	}

	public ServicoCredenciada addServicoCredenciada(
	        ServicoCredenciada servicoCredenciada) {
		getServicoCredenciadas().add(servicoCredenciada);
		servicoCredenciada.setCredenciada(this);

		return servicoCredenciada;
	}

	public ServicoCredenciada removeServicoCredenciada(
	        ServicoCredenciada servicoCredenciada) {
		getServicoCredenciadas().remove(servicoCredenciada);
		servicoCredenciada.setCredenciada(null);

		return servicoCredenciada;
	}

	@JsonProperty
	public String getDescricao() {
		String description = "";
		if (getNmFantasia() != null) {
			description += getNmFantasia() + " - ";
		}
		return description += getNmRazaoSocial() != null ? getNmRazaoSocial() : "";
	}

}