package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "financeiro", name = "tbarquivodespesa")
public class ArquivoDespesaFinanceiro implements Serializable{

	private static final long serialVersionUID = -7100898308899239405L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARQUIVODESPES_ID_SEQ")
	@SequenceGenerator(name= "ARQUIVODESPES_ID_SEQ", sequenceName = "financeiro.arquivodespesa_id_seq", allocationSize = 1)
	@Column(name = "idarquivodespesa")
	private Long id;
	
	@Column(name = "nmnome")
	private String nmNome;

	@Column(name = "dataupload")
	private Date dataUpload;
	
	@Column(name = "nmpath")
	private String nmPath;
	
	@Column(name = "nmmimetype")
	private String nmMimeType;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="iddespesa")
	private DespesaFinanceiro despesaFinanceiro;

	@JsonIgnore
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	public ArquivoDespesaFinanceiro() {

	}
	
	public ArquivoDespesaFinanceiro(String nmNome, Date dataUpload, String nmPath, String nmMimeType, DespesaFinanceiro despesaFinanceiro) {
		this.nmNome = nmNome;
		this.dataUpload = dataUpload;
		this.nmPath = nmPath;
		this.nmMimeType = nmMimeType;
		this.despesaFinanceiro = despesaFinanceiro;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNmNome() {
		return nmNome;
	}
	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}
	public Date getDataUpload() {
		return dataUpload;
	}
	public void setDataUpload(Date dataUpload) {
		this.dataUpload = dataUpload;
	}
	public String getNmPath() {
		return nmPath;
	}
	public void setNmPath(String nmPath) {
		this.nmPath = nmPath;
	}
	public String getNmMimeType() {
		return nmMimeType;
	}
	public void setNmMimeType(String nmMimeType) {
		this.nmMimeType = nmMimeType;
	}
	public DespesaFinanceiro getDespesaFinanceiro() {
		return despesaFinanceiro;
	}
	public void setDespesaFinanceiro(DespesaFinanceiro despesaFinanceiro) {
		this.despesaFinanceiro = despesaFinanceiro;
	}
	public Date getDtExclusao() {
		return dtExclusao;
	}
	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
}
