package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaServico;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DespesaDTO {
	
	private String nmLanctoTitular;
	
	private String nmLanctoPlano;
	
	private String nmLanctoPaciente;
	
	private String nmLanctoTipoDespesa;
	
	private Boolean boAutorizadoGerente;
	
	private Double vlLanctoCobertura;
	
	private Double vlLanctoDespesaTotal;
	
	private Double vlLanctoCreditoPessoal;
	
	private Double vlLanctoSalario;
	
	private Double vlLanctoPercentual;
	
	private Double vlLanctoDebitoUsado;
	
	private Double vlLanctoBoletoUsado;
	
	private Double vlLanctoDisponivel;
	
	private Double vlLanctoValorDespesa;
	
	private Double vlLanctoLimite;
	
	private String inParcelado;
	
	private Integer inFormaPagamento;
	
	private Integer inIndexTab;
	
	private Date dtVencimento;
	
	private Integer inQtdParcela;
	
	private String formaPagamentoCredito;
	
	private String formaPagamentoDebito;
	
	private Integer formaPagamentoTipo;
	
	private Integer formaPagamentoCreditoTipo;
	
	private Integer formaPagamentoDebitoTipo;
	
	private String userName;

	private String password;
	
	private String nmObservacao;
	
	private Boolean boQuitada;
	
	private DespesaServico despesaServico;
	
	private Despesa despesa;

	private List<Dependente> dependenteList;

	@JsonIgnore
	private List<Contrato> contratoList;
	
	@JsonIgnore
	private List<ContratoCliente> contratoClienteList;
	
	public DespesaDTO(){
		
	}

	public String getNmLanctoTitular() {
		return nmLanctoTitular;
	}

	public void setNmLanctoTitular(String nmLanctoTitular) {
		this.nmLanctoTitular = nmLanctoTitular;
	}

	public String getNmLanctoPlano() {
		return nmLanctoPlano;
	}

	public void setNmLanctoPlano(String nmLanctoPlano) {
		this.nmLanctoPlano = nmLanctoPlano;
	}

	public String getNmLanctoPaciente() {
		return nmLanctoPaciente;
	}

	public void setNmLanctoPaciente(String nmLanctoPaciente) {
		this.nmLanctoPaciente = nmLanctoPaciente;
	}

	public String getNmLanctoTipoDespesa() {
		return nmLanctoTipoDespesa;
	}

	public void setNmLanctoTipoDespesa(String nmLanctoTipoDespesa) {
		this.nmLanctoTipoDespesa = nmLanctoTipoDespesa;
	}

	public Double getVlLanctoCobertura() {
		return vlLanctoCobertura;
	}

	public void setVlLanctoCobertura(Double vlLanctoCobertura) {
		this.vlLanctoCobertura = vlLanctoCobertura;
	}

	public Double getVlLanctoDespesaTotal() {
		return vlLanctoDespesaTotal;
	}

	public void setVlLanctoDespesaTotal(Double vlLanctoDespesaTotal) {
		this.vlLanctoDespesaTotal = vlLanctoDespesaTotal;
	}

	public Double getVlLanctoCreditoPessoal() {
		return vlLanctoCreditoPessoal;
	}

	public void setVlLanctoCreditoPessoal(Double vlLanctoCreditoPessoal) {
		this.vlLanctoCreditoPessoal = vlLanctoCreditoPessoal;
	}

	public Double getVlLanctoSalario() {
		return vlLanctoSalario;
	}

	public void setVlLanctoSalario(Double vlLanctoSalario) {
		this.vlLanctoSalario = vlLanctoSalario;
	}

	public Double getVlLanctoPercentual() {
		return vlLanctoPercentual;
	}

	public void setVlLanctoPercentual(Double vlLanctoPercentual) {
		this.vlLanctoPercentual = vlLanctoPercentual;
	}

	public Double getVlLanctoDebitoUsado() {
		return vlLanctoDebitoUsado;
	}

	public void setVlLanctoDebitoUsado(Double vlLanctoDebitoUsado) {
		this.vlLanctoDebitoUsado = vlLanctoDebitoUsado;
	}

	public Double getVlLanctoBoletoUsado() {
		return vlLanctoBoletoUsado;
	}

	public void setVlLanctoBoletoUsado(Double vlLanctoBoletoUsado) {
		this.vlLanctoBoletoUsado = vlLanctoBoletoUsado;
	}

	public Double getVlLanctoDisponivel() {
		return vlLanctoDisponivel;
	}

	public void setVlLanctoDisponivel(Double vlLanctoDisponivel) {
		this.vlLanctoDisponivel = vlLanctoDisponivel;
	}

	public Double getVlLanctoValorDespesa() {
		return vlLanctoValorDespesa;
	}

	public void setVlLanctoValorDespesa(Double vlLanctoValorDespesa) {
		this.vlLanctoValorDespesa = vlLanctoValorDespesa;
	}

	public String getInParcelado() {
		return inParcelado;
	}

	public void setInParcelado(String inParcelado) {
		this.inParcelado = inParcelado;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Integer getInIndexTab() {
		return inIndexTab;
	}

	public void setInIndexTab(Integer inIndexTab) {
		this.inIndexTab = inIndexTab;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Integer getInQtdParcela() {
		return inQtdParcela;
	}

	public void setInQtdParcela(Integer inQtdParcela) {
		this.inQtdParcela = inQtdParcela;
	}

	public List<Contrato> getContratoList() {
		return contratoList;
	}

	public void setContratoList(List<Contrato> contratoList) {
		this.contratoList = contratoList;
	}

	public List<ContratoCliente> getContratoClienteList() {
		return contratoClienteList;
	}

	public void setContratoClienteList(List<ContratoCliente> contratoClienteList) {
		this.contratoClienteList = contratoClienteList;
	}

	public Double getVlLanctoLimite() {
		return vlLanctoLimite;
	}

	public void setVlLanctoLimite(Double vlLanctoLimite) {
		this.vlLanctoLimite = vlLanctoLimite;
	}

	public List<Dependente> getDependenteList() {
		return dependenteList;
	}

	public void setDependenteList(List<Dependente> dependenteList) {
		this.dependenteList = dependenteList;
	}

	public Boolean getBoAutorizadoGerente() {
		return boAutorizadoGerente;
	}

	public void setBoAutorizadoGerente(Boolean boAutorizadoGerente) {
		this.boAutorizadoGerente = boAutorizadoGerente;
	}

	public String getFormaPagamentoCredito() {
		return formaPagamentoCredito;
	}

	public void setFormaPagamentoCredito(String formaPagamentoCredito) {
		this.formaPagamentoCredito = formaPagamentoCredito;
	}

	public String getFormaPagamentoDebito() {
		return formaPagamentoDebito;
	}

	public void setFormaPagamentoDebito(String formaPagamentoDebito) {
		this.formaPagamentoDebito = formaPagamentoDebito;
	}

	public DespesaServico getDespesaServico() {
		return despesaServico;
	}

	public void setDespesaServico(DespesaServico despesaServico) {
		this.despesaServico = despesaServico;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Integer getFormaPagamentoTipo() {
		return formaPagamentoTipo;
	}

	public void setFormaPagamentoTipo(Integer formaPagamentoTipo) {
		this.formaPagamentoTipo = formaPagamentoTipo;
	}

	public Integer getFormaPagamentoCreditoTipo() {
		return formaPagamentoCreditoTipo;
	}

	public void setFormaPagamentoCreditoTipo(Integer formaPagamentoCreditoTipo) {
		this.formaPagamentoCreditoTipo = formaPagamentoCreditoTipo;
	}

	public Integer getFormaPagamentoDebitoTipo() {
		return formaPagamentoDebitoTipo;
	}

	public void setFormaPagamentoDebitoTipo(Integer formaPagamentoDebitoTipo) {
		this.formaPagamentoDebitoTipo = formaPagamentoDebitoTipo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public Boolean getBoQuitada() {
		return boQuitada;
	}

	public void setBoQuitada(Boolean boQuitada) {
		this.boQuitada = boQuitada;
	}
	
	
}
