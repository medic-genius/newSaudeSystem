package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.GrupoOrgao;

@Named("grupoOrgao-dao")
@ApplicationScoped
public class GrupoOrgaoDAO extends RelationalDataAccessObject<GrupoOrgao>{
	
	public List<GrupoOrgao> getAllGrupoOrgao() {

		TypedQuery<GrupoOrgao> query = entityManager.createQuery(
		        "SELECT go FROM GrupoOrgao go order by go.nmGrupoOrgao asc", GrupoOrgao.class);
		List<GrupoOrgao> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public GrupoOrgao getGrupo(Long idGrupo) {
		return searchByKey(GrupoOrgao.class, idGrupo);
	}
}
