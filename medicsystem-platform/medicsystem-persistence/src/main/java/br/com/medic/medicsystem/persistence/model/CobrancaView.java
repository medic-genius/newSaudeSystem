package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(catalog = "realvida", name = "tbcobranca_mp_view")
public class CobrancaView implements Serializable{

	private static final long serialVersionUID = -5099319423870931866L;
	
	@Id
	@Column(name = "idcliente")
	private Long id;
	
	@Column(name = "nmcliente")
	private String nmCliente;
	
	@Column(name = "nrcpf")
	private String nrCpf;
	
	@Column(name = "nrtelefone")
	private String nrTelefone;
	
	@Column(name = "nrcelular")
	private String nrCelular;
	
	@Column(name = "idempresagrupo")
	private Long idEmpresaGrupo;
	
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Column(name = "informapagamento")
	private Integer inFormaDePagamento;

	@Column(name = "intipo")
	private String inTipo;
	
	@Column(name = "nrtitulo")
	private String nrTitulo;

	@Column(name = "dtvencimento")
	private Date dataVencimento;

	@Column(name = "inadimplente")
	private Integer inInadimplente;
	
	@Column(name = "vltotal")
	private Double valorDivida;

	@Column(name = "vljuros")
	private Double valorJuros;
	
	@Column(name = "vlmultaatraso")
	private Double valorMultaAtraso;
	
	@Column(name = "ultimocontato")
	private String ultimoContato;
		
	@Transient
	private Date dtPrevisaoPagamento;
	
	@Transient
	private Date dtProximaLigacao;

	@Transient
	private Long qdteDevido;
	
	@Transient
	private Long qtdeDespesa;
	
	@Transient
	private Long qtdeMensalidade;
	
	@Transient
	private Double somaTotalDividas;

	@Transient
	private Double somaTotalJuros;
	
	@Transient
	private Double somaTotalMultas;
	
	@Column(name = "nmcidade")
	private String nmCidade;

	@Column(name = "nmbairro")
	private String nmBairro;
	
	@Column(name = "nmlogradouro")
	private String nmLogradouro;
	
	@Column(name = "nrnumero")
	private String nrNumero;
	
	@Column(name = "nmcomplemento")
	private String nmComplemento;
	
	@Column(name = "nrcep")
	private String nrCEP;
	
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;
	
	public CobrancaView() {

	}
	
	public CobrancaView(Long id) {
		this.id = id;
	}
	
	public CobrancaView(Long id, String nmCliente, String nrCpf, String nrTelefone, String nrCelular, Long qdteDevido,
			Double somaTotalDividas, Double somaTotalJuros, Double somaTotalMultas) {
		this.id = id;
		this.nmCliente = nmCliente;
		this.nrCpf = nrCpf;
		this.nrTelefone = nrTelefone;
		this.nrCelular = nrCelular;
		this.qdteDevido = qdteDevido;
		this.somaTotalDividas = somaTotalDividas;
		this.somaTotalJuros = somaTotalJuros;
		this.somaTotalMultas = somaTotalMultas;
	}
	
	public CobrancaView(Long id, String nmCliente, String nrCpf, String nrTelefone, 
			String nrCelular,String nmCidade, String nmBairro, String nmLogradouro, 
			String nrNumero, String nmComplemento, String nrCEP, String nmPontoReferencia, 
			Long qdteDevido, Double somaTotalDividas, Double somaTotalJuros, Double somaTotalMultas) {
		this.id = id;
		this.nmCliente = nmCliente;
		this.nrCpf = nrCpf;
		this.nrTelefone = nrTelefone;
		this.nrCelular = nrCelular;
		this.qdteDevido = qdteDevido;
		this.somaTotalDividas = somaTotalDividas;
		this.somaTotalJuros = somaTotalJuros;
		this.somaTotalMultas = somaTotalMultas;
	}

	public CobrancaView(Long id, String nmCliente, String nrCpf, String nrTelefone, String nrCelular, String ultimoContato,
			Long qdteDevido, Double somaTotalDividas, Double somaTotalJuros, Double somaTotalMultas, Long qtdeMensalidade, 
			Long qtdeDespesa) {
		this.id = id;
		this.nmCliente = nmCliente;
		this.nrCpf = nrCpf;
		this.nrTelefone = nrTelefone;
		this.nrCelular = nrCelular;		
		this.ultimoContato = ultimoContato;
		this.qdteDevido = qdteDevido;
		this.somaTotalDividas = somaTotalDividas;
		this.somaTotalJuros = somaTotalJuros;
		this.somaTotalMultas = somaTotalMultas;
		this.qtdeDespesa = qtdeDespesa;
		this.qtdeMensalidade = qtdeMensalidade;
	}
	
	public CobrancaView(Long id, String nmCliente, String nrCpf, String nrTelefone, String nrCelular,
			String nmCidade, String nmBairro, String nmLogradouro, String nrNumero, String nmComplemento, 
			String nrCEP, String nmPontoReferencia, Long qdteDevido, Double somaTotalDividas, Double somaTotalJuros, 
			Double somaTotalMultas, Long qtdeMensalidade, Long qtdeDespesa) {
		this.id = id;
		this.nmCliente = nmCliente;
		this.nrCpf = nrCpf;
		this.nrTelefone = nrTelefone;
		this.nrCelular = nrCelular;
		this.qdteDevido = qdteDevido;
		this.somaTotalDividas = somaTotalDividas;
		this.somaTotalJuros = somaTotalJuros;
		this.somaTotalMultas = somaTotalMultas;
		this.qtdeDespesa = qtdeDespesa;
		this.qtdeMensalidade = qtdeMensalidade;
		this.nmCidade = nmCidade;
		this.nmBairro = nmBairro;
		this.nmLogradouro = nmLogradouro;
		this.nrNumero = nrNumero;
		this.nmComplemento = nmComplemento;
		this.nrCEP = nrCEP;
		this.nmPontoReferencia = nmPontoReferencia;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public Integer getInFormaDePagamento() {
		return inFormaDePagamento;
	}

	public void setInFormaDePagamento(Integer inFormaDePagamento) {
		this.inFormaDePagamento = inFormaDePagamento;
	}

	public Double getValorDivida() {
		return valorDivida;
	}

	public void setValorDivida(Double valorDivida) {
		this.valorDivida = valorDivida;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Double getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) {
		this.valorJuros = valorJuros;
	}

	public Double getValorMultaAtraso() {
		return valorMultaAtraso;
	}

	public void setValorMultaAtraso(Double valorMultaAtraso) {
		this.valorMultaAtraso = valorMultaAtraso;
	}

	public Date getDtPrevisaoPagamento() {
		return dtPrevisaoPagamento;
	}

	public void setDtPrevisaoPagamento(Date dtPrevisaoPagamento) {
		this.dtPrevisaoPagamento = dtPrevisaoPagamento;
	}

	public Long getQdteDevido() {
		return qdteDevido;
	}

	public void setQdteDevido(Long qdteDevido) {
		this.qdteDevido = qdteDevido;
	}

	public Double getSomaTotalDividas() {
		return somaTotalDividas;
	}

	public void setSomaTotalDividas(Double somaTotalDividas) {
		this.somaTotalDividas = somaTotalDividas;
	}

	public Double getSomaTotalJuros() {
		return somaTotalJuros;
	}

	public void setSomaTotalJuros(Double somaTotalJuros) {
		this.somaTotalJuros = somaTotalJuros;
	}

	public Double getSomaTotalMultas() {
		return somaTotalMultas;
	}

	public void setSomaTotalMultas(Double somaTotalMultas) {
		this.somaTotalMultas = somaTotalMultas;
	}

	public Date getDtProximaLigacao() {
		return dtProximaLigacao;
	}

	public void setDtProximaLigacao(Date dtProximaLigacao) {
		this.dtProximaLigacao = dtProximaLigacao;
	}

	public Integer getInInadimplente() {
		return inInadimplente;
	}

	public void setInInadimplente(Integer inInadimplente) {
		this.inInadimplente = inInadimplente;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public String getInTipo() {
		return inTipo;
	}

	public void setInTipo(String inTipo) {
		this.inTipo = inTipo;
	}

	public Long getQtdeDespesa() {
		return qtdeDespesa;
	}

	public void setQtdeDespesa(Long qtdeDespesa) {
		this.qtdeDespesa = qtdeDespesa;
	}

	public Long getQtdeMensalidade() {
		return qtdeMensalidade;
	}

	public void setQtdeMensalidade(Long qtdeMensalidade) {
		this.qtdeMensalidade = qtdeMensalidade;
	}

	public String getNmCidade() {
		return nmCidade;
	}

	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNrCEP() {
		return nrCEP;
	}

	public void setNrCEP(String nrCEP) {
		this.nrCEP = nrCEP;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}
	
	public String getUltimoContato() {
		return ultimoContato;
	}

	public void setUltimoContato(String ultimoContato) {
		this.ultimoContato = ultimoContato;
	}
		
}
