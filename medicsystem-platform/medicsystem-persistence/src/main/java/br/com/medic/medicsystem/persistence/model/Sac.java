package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(catalog = "realvida", name = "tbsac")
public class Sac implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SAC_ID_SEQ")
	@SequenceGenerator(name = "SAC_ID_SEQ", sequenceName = "realvida.sac_id_seq", allocationSize = 1)
	@Column(name = "idsac")
	private Long id;
	
	@JsonIgnore
	@OneToOne(mappedBy = "sac", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Cobranca cobranca;
	
	@Basic
	@Column(name = "data")
	private Date data;
	
	@Basic
	@Column(name = "dataagendamento")
	private Date dataAgendamento;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtcadastrolog")
	private Date dtCadastroLog;
	
	@Basic
	@Column(name = "hrfinal")
	private Time hrFinal;
	
	@Basic
	@Column(name = "hrinicial")
	private Time hrInicial;
	
	@Basic
	@Column(name = "informapagamento")
	private Integer informaPagamento;
	
	@Basic
	@Column(name = "motivo")
	private Integer motivo;
	
	@Basic
	@Column(name = "observacao", length = 5000)
	private String observacao;
	
	@Basic
	@Column(name = "status")
	private Integer status;
	
	@Basic
	@Column(name = "tiposac")
	private Integer tipoSac;
	
	@Basic
	@Column(name = "vldebito")
	private double vlDebito;
	
	@Basic
	@Column(name = "vlnegociado")
	private double vlNegociado;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;
	
	//bi-directional many-to-one association to Tbcontratocliente
	@ManyToOne
	@JoinColumn(name="idcontratocliente")
	private ContratoCliente contratoCliente;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idnegociador")
	private Funcionario funcionario1;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idnegociadoralteracao")
	private Funcionario funcionario2;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;
	
	public Sac() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(Date dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtCadastroLog() {
		return dtCadastroLog;
	}

	public void setDtCadastroLog(Date dtCadastroLog) {
		this.dtCadastroLog = dtCadastroLog;
	}

	public Time getHrFinal() {
		return hrFinal;
	}

	public void setHrFinal(Time hrFinal) {
		this.hrFinal = hrFinal;
	}

	public Time getHrInicial() {
		return hrInicial;
	}

	public void setHrInicial(Time hrInicial) {
		this.hrInicial = hrInicial;
	}

	public Integer getInformaPagamento() {
		return informaPagamento;
	}

	public void setInformaPagamento(Integer informaPagamento) {
		this.informaPagamento = informaPagamento;
	}

	public Integer getMotivo() {
		return motivo;
	}

	public void setMotivo(Integer motivo) {
		this.motivo = motivo;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTipoSac() {
		return tipoSac;
	}

	public void setTipoSac(Integer tipoSac) {
		this.tipoSac = tipoSac;
	}

	public double getVlDebito() {
		return vlDebito;
	}

	public void setVlDebito(double vlDebito) {
		this.vlDebito = vlDebito;
	}

	public double getVlNegociado() {
		return vlNegociado;
	}

	public void setVlNegociado(double vlNegociado) {
		this.vlNegociado = vlNegociado;
	}

	public ContratoCliente getContratoCliente() {
		return contratoCliente;
	}

	public void setContratoCliente(ContratoCliente contratoCliente) {
		this.contratoCliente = contratoCliente;
	}

	public Funcionario getFuncionario1() {
		return funcionario1;
	}

	public void setFuncionario1(Funcionario funcionario1) {
		this.funcionario1 = funcionario1;
	}

	public Funcionario getFuncionario2() {
		return funcionario2;
	}

	public void setFuncionario2(Funcionario funcionario2) {
		this.funcionario2 = funcionario2;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cobranca getCobranca() {
		return cobranca;
	}

	public void setCobranca(Cobranca cobranca) {
		this.cobranca = cobranca;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}
	
}