package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbobservacaocliente")
public class ObservacaoCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OBSERVACAOCLIENTE_ID_SEQ")
	@SequenceGenerator(name = "OBSERVACAOCLIENTE_ID_SEQ", sequenceName = "realvida.observacaocliente_id_seq", allocationSize = 1)
	@Column(name = "idobservacaocliente")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtobservacaocliente")
	private Date dtObservacaoCliente;

	@NotNull
	@Basic
	@Column(name = "nmobservacaocliente")
	private String nmObservacaoCliente;

	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Funcionario funcionario;

	public ObservacaoCliente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtObservacaoCliente() {
		return dtObservacaoCliente;
	}

	public void setDtObservacaoCliente(Date dtObservacaoCliente) {
		this.dtObservacaoCliente = dtObservacaoCliente;
	}

	public String getNmObservacaoCliente() {
		return nmObservacaoCliente;
	}

	public void setNmObservacaoCliente(String nmObservacaoCliente) {
		this.nmObservacaoCliente = nmObservacaoCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
}