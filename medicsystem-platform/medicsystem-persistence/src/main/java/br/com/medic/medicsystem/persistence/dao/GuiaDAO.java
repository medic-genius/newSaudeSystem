package br.com.medic.medicsystem.persistence.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.bytecode.buildtime.spi.ExecutionException;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.Servico;

@Named("guia-dao")
@ApplicationScoped
public class GuiaDAO extends RelationalDataAccessObject<Guia> {
	@SuppressWarnings("unchecked")
	public List<Guia> getListGuia(int offset, int limit, Long id, String nmCliente) {
		Query query = entityManager
				.createQuery("SELECT guia FROM Guia guia where guia.dtExclusao is null " + "order by guia.id desc")
				.setMaxResults(limit).setFirstResult((offset - 1) * limit);
		List<Guia> result = query.getResultList();
		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}

	public Long getTotal() {
		Query query = entityManager.createQuery("SELECT count(guia.id) FROM Guia guia where guia.dtExclusao is null ");
		try {
			query.setMaxResults(1);
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public Guia getGuia(Long id) {
		Query query = entityManager.createQuery("SELECT g FROM Guia g " + "WHERE g.id = :id");
		try {
			query.setMaxResults(1);
			query.setParameter("id", id);
			Guia guia = (Guia) query.getSingleResult();
			return guia;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Servico> getListServicoByIds(List<Long> ids) {
		Query query = entityManager.createQuery("SELECT servico FROM Servico servico where servico.id in :ids");
		query.setParameter("ids", ids);
		List<Servico> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public GenericPaginateDTO getListByConveniada(GenericPaginateDTO paginator, Long idEmpresaCliente, Long id,
			String nmCliente, String nameBeneficiary, Integer status) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaBuilder criteriaBuilderCount = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Guia> criteriaQuery = criteriaBuilder.createQuery(Guia.class);
		CriteriaQuery<Long> criteriaQueryCount = criteriaBuilderCount.createQuery(Long.class);
		Root<Guia> entity = criteriaQuery.from(Guia.class);
		Root<Guia> entityCount = criteriaQueryCount.from(Guia.class);

		criteriaQuery.orderBy(criteriaBuilder.desc(entity.get("id")));

		List<Predicate> p = new ArrayList<Predicate>();
		List<Predicate> pCount = new ArrayList<Predicate>();

		criteriaQueryCount.select(criteriaBuilderCount.count(entityCount));
		if (idEmpresaCliente != null) {
			p.add(criteriaBuilder.and(
					criteriaBuilder.equal(entity.get("contrato").get("empresaCliente").get("id"), idEmpresaCliente)));
			pCount.add(criteriaBuilderCount.and(criteriaBuilderCount
					.equal(entityCount.get("contrato").get("empresaCliente").get("id"), idEmpresaCliente)));
		}
		if (id != null) {
			p.add(criteriaBuilder.and(criteriaBuilder.equal(entity.get("id"), id)));
			pCount.add(criteriaBuilderCount.and(criteriaBuilderCount.equal(entityCount.get("id"), id)));
		}
		if (nmCliente != null) {
			String nmClienteFormat = "%" + nmCliente.toUpperCase() + "%";
			p.add(criteriaBuilder.like(entity.get("cliente").get("nmCliente"), nmClienteFormat));
			pCount.add(criteriaBuilderCount.like(entityCount.get("cliente").get("nmCliente"), nmClienteFormat));
		}
		if (null != status) {
			if (0 == status) {
				p.add(criteriaBuilder.or(
						criteriaBuilder.isNotNull(entity.get("dtExclusao")),
						criteriaBuilder.lessThan(entity.get("dtValidade"), new Date())
					)); 
				pCount.add(criteriaBuilderCount.or(
						criteriaBuilderCount.isNotNull(entityCount.get("dtExclusao")),
						criteriaBuilderCount.lessThan(entityCount.get("dtValidade"), new Date())
					)); 
			} else if (1 == status) {
				p.add(criteriaBuilder.and(
						criteriaBuilder.isNull(entity.get("dtExclusao")),
						criteriaBuilder.greaterThan(entity.get("dtValidade"), new Date())
					));
				pCount.add(criteriaBuilderCount.and(
						criteriaBuilderCount.isNull(entityCount.get("dtExclusao")),
						criteriaBuilderCount.greaterThan(entityCount.get("dtValidade"), new Date())
					)); 
			}
		}
		
		if (!p.isEmpty()) {
			Predicate[] pr = new Predicate[p.size()];
			p.toArray(pr);
			criteriaQuery.where(pr);
			Predicate[] prCount = new Predicate[pCount.size()];
			pCount.toArray(prCount);
			criteriaQueryCount.where(prCount);
		}

		Query query = entityManager.createQuery(criteriaQuery);
		Query queryCouny = entityManager.createQuery(criteriaQueryCount);

		Long count = (Long) queryCouny.getSingleResult();

		paginator.setTotal(count);
		query.setFirstResult((paginator.getOffset() - 1) * paginator.getLimit());
		query.setMaxResults(paginator.getLimit());

		List<Guia> result = query.getResultList();
		paginator.setData(result);
		return paginator;
	}

	public Long getTotalByConveniada(Long idEmpresaCliente, Long id, String nmCliente) {
		Query query = entityManager.createQuery(
				"SELECT count(guia.id) FROM Guia guia where guia.dtExclusao is null and  guia.contrato.empresaCliente.id = :idEmpresaCliente");
		try {
			query.setMaxResults(1);
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public GenericPaginateDTO getListByCliente(GenericPaginateDTO paginator, Long idCliente) {
		if (idCliente == null) {
			throw new ExecutionException("Null is not allowed!");
		}
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaBuilder criteriaBuilderCount = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Guia> criteriaQuery = criteriaBuilder.createQuery(Guia.class);
		CriteriaQuery<Long> criteriaQueryCount = criteriaBuilderCount.createQuery(Long.class);
		Root<Guia> entity = criteriaQuery.from(Guia.class);
		Root<Guia> entityCount = criteriaQueryCount.from(Guia.class);
		criteriaQuery.orderBy(criteriaBuilder.desc(entity.get("id")));

		List<Predicate> p = new ArrayList<Predicate>();
		List<Predicate> pCount = new ArrayList<Predicate>();

		criteriaQueryCount.select(criteriaBuilderCount.count(entityCount));

		p.add(criteriaBuilder.and(criteriaBuilder.equal(entity.get("cliente").get("id"), idCliente)));
		pCount.add(
				criteriaBuilderCount.and(criteriaBuilderCount.equal(entityCount.get("cliente").get("id"), idCliente)));

		Predicate[] pr = new Predicate[p.size()];
		p.toArray(pr);
		criteriaQuery.where(pr);
		Predicate[] prCount = new Predicate[pCount.size()];
		pCount.toArray(prCount);
		criteriaQueryCount.where(prCount);

		Query query = entityManager.createQuery(criteriaQuery);
		Query queryCouny = entityManager.createQuery(criteriaQueryCount);

		Long count = (Long) queryCouny.getSingleResult();

		paginator.setTotal(count);
		query.setFirstResult((paginator.getOffset() - 1) * paginator.getLimit());
		query.setMaxResults(paginator.getLimit());
		List<Guia> result = query.getResultList();
		paginator.setData(result);

		return paginator;
	}

	public GuiaServico getGuiaValidabyClienteDependenteServico(Agendamento agendamento) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		String strQuery = "SELECT gs FROM GuiaServico gs " 
				+ " WHERE '" + df.format(agendamento.getDtAgendamento() != null ? agendamento.getDtAgendamento() : new Date()) + "' between gs.guia.dtInicio and gs.guia.dtValidade "
				+ " and gs.guia.dtExclusao is null "
				+ " and gs.guia.cliente.id = " + agendamento.getCliente().getId() 
				+ " and gs.guia.dependente.id "	+ (agendamento.getDependente() != null ? " = " + agendamento.getDependente().getId() : " is null ")
				+ " and gs.guia.contrato.id = " + agendamento.getContrato().getId()
				+ " and gs.servico.id = " + agendamento.getServico().getId()
				+ " and gs.status = 0";

		try {

			TypedQuery<GuiaServico> query = entityManager.createQuery(strQuery, GuiaServico.class);

			query.setMaxResults(1);

			GuiaServico result = query.getSingleResult();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
		}

		return null;
	}
	
	public GuiaServico getGuiaValidabyClienteDependenteServico(DespesaServico despesaServico) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		String strQuery = "SELECT gs FROM GuiaServico gs " 
				+ " WHERE '" + df.format(despesaServico.getDespesa().getDtDespesa() != null ? despesaServico.getDespesa().getDtDespesa() : new Date()) + "' between gs.guia.dtInicio and gs.guia.dtValidade "
				+ " and gs.guia.dtExclusao is null "				
				+ " and gs.guia.cliente.id = " + despesaServico.getDespesa().getContratoCliente().getCliente().getId() 
				+ " and gs.guia.dependente.id "	+ (despesaServico.getDespesa().getContratoDependente() != null ? " = " + despesaServico.getDespesa().getContratoDependente().getDependente().getId() : " is null ")
				+ " and gs.guia.contrato.id = " + despesaServico.getDespesa().getContratoCliente().getContrato().getId()
				+ " and gs.servico.id = " + despesaServico.getServico().getId()
				+ " and gs.status = 0";

		try {

			TypedQuery<GuiaServico> query = entityManager.createQuery(strQuery, GuiaServico.class);

			query.setMaxResults(1);

			GuiaServico result = query.getSingleResult();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
		}

		return null;
	}

}
