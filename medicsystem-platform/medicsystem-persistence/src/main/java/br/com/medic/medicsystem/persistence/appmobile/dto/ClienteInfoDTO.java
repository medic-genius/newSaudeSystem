package br.com.medic.medicsystem.persistence.appmobile.dto;

public class ClienteInfoDTO {

	private Long idCliente;
	
	private String nmCliente;
	
	private String nrCelular;
	
	private String nmEmail;
	
	private Integer tipoCliente;
	
	private Integer situacaoGeral;
	
	public Long getIdCliente() {
		return idCliente;
	}
	
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
	public String getNmCliente() {
		return nmCliente;
	}
	
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	
	public String getNrCelular() {
		return nrCelular;
	}
	
	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}
	
	public String getNmEmail() {
		return nmEmail;
	}
	
	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public Integer getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Integer getSituacaoGeral() {
		return situacaoGeral;
	}

	public void setSituacaoGeral(Integer situacaoGeral) {
		this.situacaoGeral = situacaoGeral;
	}
	
	public String nmSituacaoGeral() {
		String situacao = "";
		if(situacaoGeral != null) {
			if(situacaoGeral == 0) {
				situacao = "LIBERADO";
			} else if(situacaoGeral == 1) {
				situacao = "SEMI-BLOQUEADO";
			} else if(situacaoGeral == 2) {
				situacao = "BLOQUEADO";
			} else if(situacaoGeral == 3) {
				situacao = "INADIMPLENTE";
			} else if(situacaoGeral == 4) {
				situacao = "CADASTRO PENDENTE";
			}
		}
		return situacao;
	}
}
