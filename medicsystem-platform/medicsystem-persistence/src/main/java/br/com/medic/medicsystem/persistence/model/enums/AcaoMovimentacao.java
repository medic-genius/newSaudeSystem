package br.com.medic.medicsystem.persistence.model.enums;

public enum AcaoMovimentacao {

	PAGAMENTO(0, "Pagamento de conta"), RECEBIMENTO(1, "Recebimento"), ESTORNO(2, "Estorno"), 
	RECEBIMENTO_CAIXA(3, "Entrada do caixa"), SALDO_ANTERIOR(4, " Saldo anterior");
	
	private Integer id;
	private String description;

	private AcaoMovimentacao(Integer id, String description){
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
