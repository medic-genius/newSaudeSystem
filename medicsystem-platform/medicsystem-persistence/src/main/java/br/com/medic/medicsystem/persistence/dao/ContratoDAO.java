package br.com.medic.medicsystem.persistence.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.ContratoProducaoVendaDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoContratoEnum;
import br.com.medic.medicsystem.persistence.model.views.MensalidadeParcelaView;
import br.com.medic.medicsystem.persistence.model.views.RenovacaoContratoView;
import br.com.medic.medicsystem.persistence.model.views.ViewRenovacaoContrato;

@Named("contrato-dao")
@ApplicationScoped
public class ContratoDAO extends RelationalDataAccessObject<Contrato> {

	public Contrato getContrato(Long idContrato) {
		return searchByKey(Contrato.class, idContrato);
	}
	
	public Contrato getContratoAtivoEmpresaCliente(Long idEmpresaCliente) {
		String queryStr = "select c from Contrato c where c.boEmpresaCliente = true "
		        + " and c.situacao = 0 and c.empresaCliente.id = "
		        + idEmpresaCliente;

		TypedQuery<Contrato> query = entityManager.createQuery(queryStr,
		        Contrato.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		Contrato result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public ContratoCliente getContratoCliente(Long idContrato) {
		String queryStr = "select cc from ContratoCliente cc where cc.contrato.id = "
		        + idContrato;

		TypedQuery<ContratoCliente> query = entityManager.createQuery(queryStr,
		        ContratoCliente.class);
		        		
		query.setMaxResults(1);
		ContratoCliente result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}

	public ContratoDependente getContratoDependente(Long idContrato) {
		String queryStr = "select cc from ContratoDependente cc where cc.contrato.id = "
		        + idContrato;

		TypedQuery<ContratoDependente> query = entityManager.createQuery(
		        queryStr, ContratoDependente.class);
		
		query.setMaxResults(1);
		ContratoDependente result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	public List<ContratoDependente> getContratosDependentes(Long idContrato) {
		String queryStr = "select cc from ContratoDependente cc where cc.contrato.id = "
		        + idContrato;

		TypedQuery<ContratoDependente> query = entityManager.createQuery(
		        queryStr, ContratoDependente.class);
				
		List<ContratoDependente> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	public ContratoDependente getContratoDependente(Long idContrato, Long idDependente) {
		String queryStr = "select cd from ContratoDependente cd "
				+ " where cd.inSituacao = 0 "
				+ " and cd.contrato.id = " + idContrato 
				+ " and cd.dependente.id = " + idDependente;

		TypedQuery<ContratoDependente> query = entityManager.createQuery(
		        queryStr, ContratoDependente.class);
		
		query.setMaxResults(1);
		try {			
			ContratoDependente result = query.getSingleResult();

			return result;
			
		} catch (NoResultException nre) {
			return null;
		}
	}

	public String getUltimoCodigoGerado() {

		Calendar calendar = Calendar.getInstance();
		int anoAtual = calendar.get(Calendar.YEAR);

		String queryStr = " select c.nrContrato "
		        + " from Contrato c where c.nrContrato like '%" + anoAtual + "'" 
				+ " order by c.nrContrato DESC";

		Query query = entityManager.createQuery(queryStr);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			Object result = query.getSingleResult();
			return (String) result;
			
		} catch (NoResultException nre) {
			return null;
		}

	}
	
	public String getUltimoNumeroTalaoGerado() {
				
		String queryStr = "select nrtalao "
				+ " from realvida.tbcontrato "
				+ " where nrtalao is not null "
				+ " order by nrtalao DESC limit 1";
		
		return (String) findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)		        
		        .uniqueResult();

	}
	
		
	public ContratoCliente getContratoCliente(Long idContrato, Long idCliente) {
		String queryStr = " select cc "
		        + " from ContratoCliente cc where cc.contrato.id ="
		        + idContrato + " and cc.cliente.id = " + idCliente;

		TypedQuery<ContratoCliente> query = entityManager.createQuery(queryStr,
		        ContratoCliente.class);

		query.setMaxResults(1);
		return query.getSingleResult();

	}

	public List<ContratoDependente> getContratosDependentes(Long idcontrato,
	        Long idcliente) {
		TypedQuery<ContratoDependente> query = entityManager.createQuery(
		        "SELECT cd FROM ContratoDependente cd where cd.contrato.id = "
		                + idcontrato + " and cd.dependente.cliente.id = "
		                + idcliente, ContratoDependente.class);
		List<ContratoDependente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Integer getUltimoNumeroBoletoGerado() {
		
		String query = "select cast(nrboleto as integer) as numboleto "
				+ " from realvida.tbboletobancario "
				+ " where nrboleto is not null "
				+ " order by nrboleto desc limit 1";
		
		return (Integer) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .addScalar("numboleto", IntegerType.INSTANCE).uniqueResult();
	}
	
	public Integer renovacaoAutomaticaJob(Long idEmpresaGrupo){
		
		String query = "select realvida.renovacaoautomatica(" + idEmpresaGrupo + ")";

		return (Integer) findByNativeQuery(query).unwrap(SQLQuery.class).uniqueResult();
	}
	
	public Contrato getContratoPorEmpresaCliente(Long idEmpresaCliente) {

		TypedQuery<Contrato> query = entityManager.createQuery("SELECT c FROM Contrato c "
				+ " where c.boEmpresaCliente = true "
				+ " and c.situacao = 0 and c.empresaCliente.id = " + idEmpresaCliente, Contrato.class);

		query.setMaxResults(1);
		Contrato result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoProducaoVendaDTO> getContratoProdVenda(Long idVendedor, String dtInicio, String dtFim){
		
		String queryStr = "select cli.nmcliente, c.nrcontrato, c.dtcontrato, c.informapagamento, c.vltotal, pv.vlproducao "
				+ "	from realvida.tbcontrato c "
				+ "	inner join realvida.tbcontratocliente cc on c.idcontrato = cc.idcontrato "
				+ " inner join realvida.tbcliente cli on cli.idcliente = cc.idcliente "
				+ " left join realvida.tbproducaovenda pv on c.idcontrato = pv.idcontrato "
				+ "	where cli.dtexclusao is null "
				+ " and c.idplano is not null "
				+ "	and c.idvendedor = " + idVendedor
				+ "	and c.dtcontrato between '" + dtInicio + "' and '" + dtFim + "' "
				+ " order by c.dtcontrato, c.idcontrato";
		
		List<ContratoProducaoVendaDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrcontrato", StringType.INSTANCE)
		        .addScalar("dtcontrato", DateType.INSTANCE)
		        .addScalar("informapagamento", IntegerType.INSTANCE)
		        .addScalar("vltotal", DoubleType.INSTANCE)
		        .addScalar("vlproducao", DoubleType.INSTANCE)		       
		        .setResultTransformer(
		                Transformers.aliasToBean(ContratoProducaoVendaDTO.class)).list();
		
		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContratoProducaoVendaDTO> getContratoProdVendaFuncionarios(String dtInicio, String dtFim, Integer idUnidade){
		
		String queryStr = "select fun.nmfuncionario, count(c.idcontrato) as quantcontratos, sum(c.vltotal) as totalvenda, sum(pv.vlproducao) as totalcomissao"
				+ " from realvida.tbcontrato c " 
				+ " inner join realvida.tbcontratocliente cc on c.idcontrato = cc.idcontrato "
				+ " inner join realvida.tbfuncionario fun on c.idvendedor = fun.idfuncionario "
				+ " left join realvida.tbproducaovenda pv on c.idcontrato = pv.idcontrato "
				+ " where c.idplano is not null "
				+ " and c.unidadevenda =" + idUnidade
				+ "	and c.dtcontrato between '" + dtInicio + "' and '" + dtFim + "' "
				+ " group by fun.nmfuncionario"
				+ " order by fun.nmfuncionario";
		
		List<ContratoProducaoVendaDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmfuncionario", StringType.INSTANCE)
		        .addScalar("quantcontratos", IntegerType.INSTANCE)
		        .addScalar("totalvenda", DoubleType.INSTANCE)
		        .addScalar("totalcomissao", DoubleType.INSTANCE)		       
		        .setResultTransformer(
		                Transformers.aliasToBean(ContratoProducaoVendaDTO.class)).list();
		
		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		
		return result;
	}
	
	public Contrato getContratoNAssociadoPorCliente(Long idCliente) {

		TypedQuery<Contrato> query = entityManager.createQuery("SELECT cc.contrato "
				+ " FROM ContratoCliente cc "
				+ " where cc.cliente.id = " + idCliente
				+ " and cc.contrato.situacao = 0 "
				+ " and cc.boAtivo = true "
				+ " and cc.contrato.plano is null ", Contrato.class);

		query.setMaxResults(1);
		Contrato result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Contrato getContratoPorCliente(Long idCliente) {
		
		try {
			TypedQuery<Contrato> query = entityManager.createQuery("SELECT cc.contrato "
					+ " FROM ContratoCliente cc "
					+ " where cc.cliente.id = " + idCliente
					+ " and cc.contrato.situacao = 0 "
					+ " and cc.boAtivo = true"
					, Contrato.class);
	
			query.setMaxResults(1);
			Contrato result = query.getSingleResult();
				
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}		
			
				
	}

	public Integer atualizarContratoRecorrentebyIdsubscription(Long idSubscription) {
		
		String queryStr = "select financeiro.pagarmensalidadesrecorrente_com_parametro("+idSubscription+");";
		
		return (Integer) findByNativeQuery(queryStr).unwrap(SQLQuery.class).uniqueResult();
						
	}

	public Contrato getContratoEmpresaClienteById(Long idEmpresaCliente) {
		
		Query query = entityManager
		        .createQuery("SELECT con FROM Contrato con "
				+ " WHERE con.empresaCliente.id = :idEmpresaCliente"
				+ " AND con.boEmpresaCliente IS TRUE"
				+ " AND con.situacao = 0");
		
		try {
			query.setMaxResults(1);
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
			Contrato result = (Contrato) query.getSingleResult();
			
			if (result == null) {
				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			}
			
			return result;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}	
						
	}
	
	public List<ContratoCliente> getContratosClienteByIdCliente(Long idCliente) {
		String queryStr = "SELECT cc FROM ContratoCliente cc "
				+ " WHERE cc.cliente.id = :idCliente "
				+ " AND cc.contrato.dtInativacao IS NULL "
				+ " AND cc.contrato.situacao = 0" //contrato ativo
				+ " AND cc.boAtivo IS TRUE AND cc.contrato.boBloqueado IS FALSE "
				+ " AND cc.contrato.boNaoFazUsoPlano IS FALSE "
				+ " ORDER BY cc.contrato.plano ASC";
		TypedQuery<ContratoCliente> query = entityManager.createQuery(queryStr, ContratoCliente.class)
				.setParameter("idCliente", idCliente);
		
		List<ContratoCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<MensalidadeParcelaView> getMensalidadeParcelaConveniada(long idContrato){
		
		String queryStr = "SELECT mpView FROM MensalidadeParcelaView mpView"
				+ " where mpView.idContrato = " + idContrato
				+ " order by mpView.dtVencimento desc";

	
		TypedQuery<MensalidadeParcelaView> query = entityManager.createQuery(queryStr, MensalidadeParcelaView.class);
		try
		{
			List<MensalidadeParcelaView> result = query.getResultList();
			return result;
		} catch (Exception e)
		{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<RenovacaoContratoView> getRenovacoesContrato(int offset,
			int limit, Timestamp dtInicio, Timestamp dtFim,
			Long inFormaPagamento, Long idPlano, Long idEmpresaGrupo,
			Long idFuncionario) {
		
		String queryStr = "SELECT renov.* FROM realvida.rel_renovacaocontrato_view renov "
		+ " WHERE renov.dtrenovacao BETWEEN '"+dtInicio+"' AND '"+ dtFim+"' ";
		
		String orderStr = "ORDER BY renov.dtrenovacao ASC";
		
		if(inFormaPagamento != null){
		 queryStr += " AND renov.informapagamento = "+ inFormaPagamento;
		}
		
		if(idPlano != null){
			queryStr += " AND renov.idplano = "+ idPlano;
		}
		
		if(idEmpresaGrupo != null){
			queryStr += " AND renov.idempresagrupo = "+ idEmpresaGrupo; 
		}
		
		if(idFuncionario != null) {
			queryStr += " AND renov.idfuncionario = " + idFuncionario;
		}
		
		String  queryFinal= queryStr += orderStr;
		
		Query query = entityManager.createNativeQuery(queryFinal, RenovacaoContratoView.class);
		query.setMaxResults(limit).setFirstResult((offset-1)*limit);
		List<RenovacaoContratoView> result = query.getResultList();
		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
		
		
	}

	public Long getTotalRenovacoesContrato(Timestamp dtInicio, Timestamp dtFim,
			Long inFormaPagamento, Long idPlano, Long idEmpresaGrupo, Long idFuncionario) {
		String queryStr = "SELECT count(renov.*) AS counter FROM realvida.rel_renovacaocontrato_view renov "
				+ "WHERE renov.dtrenovacao BETWEEN '"+ dtInicio +"' AND '"+dtFim+"' ";
			if(inFormaPagamento != null){
			 queryStr += " AND renov.informapagamento = "+ inFormaPagamento;
			}
			
			if(idPlano != null){
				queryStr += " AND renov.idplano = "+ idPlano;
			}
			
			if(idEmpresaGrupo != null){
				queryStr += " AND renov.idempresagrupo = "+ idEmpresaGrupo; 
			}
			
			if(idFuncionario != null) {
				queryStr += " AND renov.idfuncionario = " + idFuncionario;
			}
		
		try {
			Query query = entityManager.createNativeQuery(queryStr);
			Long quantidade = (Long) query.unwrap(SQLQuery.class)
					.addScalar("counter", LongType.INSTANCE).uniqueResult();
			return quantidade;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FuncionarioDTO> getFuncionariosRenovacao(Date dtInicio,
			Date dtFim, Long inFormaPagamento, Long idPlano, Long idEmpresaGrupo) {
		Timestamp tmInicio = new Timestamp(dtInicio.getTime());
		Timestamp tmFim = new Timestamp(dtFim.getTime());
		
		String queryStr = "SELECT renov.idfuncionario, renov.nmfuncionario "
				+ " FROM realvida.rel_renovacaocontrato_view renov "
				+ " WHERE renov.dtrenovacao BETWEEN :dtInicio AND :dtFim ";
		if (inFormaPagamento != null) {
			queryStr += " AND renov.informapagamento = " + inFormaPagamento;
		}

		if (idPlano != null) {
			queryStr += " AND renov.idplano = " + idPlano;
		}

		if (idEmpresaGrupo != null) {
			queryStr += " AND renov.idempresagrupo = " + idEmpresaGrupo;
		}
		String orderAndGroup = " GROUP BY renov.idfuncionario, renov.nmfuncionario "
				+ " ORDER BY renov.nmfuncionario ASC ";
		String queryFinal = queryStr + orderAndGroup;
		
		try {
			Query query = entityManager.createNativeQuery(queryFinal)
					.setParameter("dtInicio", tmInicio)
					.setParameter("dtFim", tmFim);
			List<FuncionarioDTO> result = query
					.unwrap(SQLQuery.class)
					.addScalar("idFuncionario", LongType.INSTANCE)
					.addScalar("nmFuncionario", StringType.INSTANCE)
					.setResultTransformer(
							Transformers.aliasToBean(FuncionarioDTO.class))
					.list();
			return result;
		} catch (Exception e) {

		}
		return null;
	}
	
	
	public List<ViewRenovacaoContrato> getContratosParaRenovacao(Date dtTermino, FormaPagamentoContratoEnum formaPgto) {
		String queryStr = "SELECT ren FROM ViewRenovacaoContrato ren "
				+ " WHERE ren.dtTerminoContrato = :dtTermino "
				+ " AND ren.inFormaPagamento = :formaPgto ";
		TypedQuery<ViewRenovacaoContrato> query = entityManager
				.createQuery(queryStr, ViewRenovacaoContrato.class)
				.setParameter("dtTermino", dtTermino)
				.setParameter("formaPgto", formaPgto.getId().longValue());
		try {
			return query.getResultList();
		} catch(Exception e) {
			
		}
		return null;
	}
	
	public boolean renovaContratoProcedure(Long idContrato, Integer subscriptionId) {
		Long idSubs = subscriptionId.longValue();
		List<Integer> list = new ArrayList<Integer>();
		try{
			Session session = entityManager.unwrap(Session.class);
			session.doWork(new Work() {
				@Override
				public void execute(Connection conn) throws SQLException {
					CallableStatement stmt = conn.prepareCall("{call realvida.insert_renovacaoautomatica(?, ?)}");
					stmt.setLong(1, idContrato);
					stmt.setLong(2, idSubs);
					
					stmt.execute();
					
					Integer qtd = 1;
					
					stmt.close();
					
					list.add(qtd);
				}
			});

		}catch(Exception e){
			e.printStackTrace();
		}
		return !list.isEmpty() && list.get(0) > 0;
	}
}
