package br.com.medic.medicsystem.persistence.appmobile.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegisterPK;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;

@Named("usuario-register-dao")
@ApplicationScoped
public class UsuarioRegisterDAO extends RelationalDataAccessObject<UsuarioRegister>{
	public UsuarioRegister getUsuarioByUsername(String login) {
		String queryStr = "SELECT usr FROM UsuarioRegister usr WHERE lower(usr.login) = lower(:loginStr)";
		TypedQuery<UsuarioRegister> query = createQuery(queryStr, UsuarioRegister.class)
				.setParameter("loginStr", login);
		try {
			UsuarioRegister user = query.getSingleResult();
			return user;
		} catch(Exception e) {
		}
		return null;
	}
	
	public UsuarioRegister getUsuarioRegisterByKey(Long idUsuario, Integer tipoUsuario) {
		UsuarioRegisterPK pk = new UsuarioRegisterPK();
		pk.setIdUsuario(idUsuario);
		pk.setTipoUsuario(tipoUsuario);
		try {
			return searchByKey(UsuarioRegister.class, pk);
		} catch(Exception e) {
			
		}
		return null;
	}
}
