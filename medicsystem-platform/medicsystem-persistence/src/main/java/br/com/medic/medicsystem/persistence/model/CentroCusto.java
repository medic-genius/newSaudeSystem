package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "financeiro", name = "tbcentrocusto")
public class CentroCusto implements Serializable{

	private static final long serialVersionUID = -1572656888534266488L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CENTRO_CUSTO_ID_SEQ")
	@SequenceGenerator(name = "CENTRO_CUSTO_ID_SEQ", sequenceName = "financeiro.centro_custo_id_seq", allocationSize = 1)
	@Column(name = "idcentrocusto")
	private Long id;
	
	@Column(name = "nmcentrocusto")
	private String nmCentroCusto;

	@Transient
	private String nrOrdem;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "centrocusto_idcentrocusto")
	private CentroCusto centroCusto;
	
	@OneToMany(mappedBy = "centroCusto", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<CentroCusto> centroCustos;

	@JsonIgnore
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@JsonProperty
	@Transient
	private BigDecimal valorCentroCusto;
	
	@JsonProperty
	@Transient
	private String mesAno;
	
	@JsonProperty
	@Transient 
	private Integer mes;
	
	@JsonProperty
	@Transient
	private Integer ano;
	
	
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public CentroCusto() {
		valorCentroCusto = new BigDecimal(0);
	}
	public BigDecimal getValorCentroCusto() {
		return valorCentroCusto;
	}

	public void setValorCentroCusto(BigDecimal valorCentroCusto) {
		this.valorCentroCusto = valorCentroCusto;
	}

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmCentroCusto() {
		return nmCentroCusto;
	}

	public void setNmCentroCusto(String nmCentroCusto) {
		this.nmCentroCusto = nmCentroCusto;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getNrOrdem() {
		return nrOrdem;
	}

	public void setNrOrdem(String nrOrdem) {
		this.nrOrdem = nrOrdem;
	}

	public List<CentroCusto> getCentroCustos() {
		return centroCustos;
	}

	public void setCentroCustos(List<CentroCusto> centroCustos) {
		for (CentroCusto centroCusto : centroCustos) {
			centroCusto.setCentroCusto(this);
		}
		this.centroCustos = centroCustos;
	}
}
