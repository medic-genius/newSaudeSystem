package br.com.medic.medicsystem.persistence.dao;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.AgendamentoPrevisaoDTO;
import br.com.medic.medicsystem.persistence.dto.AprazamentoDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResuDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalUnidadeResultDTO;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissionalResult;



@Named("estatistica-dao")
@ApplicationScoped
public class EstatisticaDAO extends RelationalDataAccessObject<AtendimentoProfissionalResult>{
	
	public List<AtendimentoProfissionalResult> getRelarioAtendimentoMedico(Long idProfissional, Date dataInicio, Date dataFim){
		
		String query = "SELECT apr FROM AtendimentoProfissionalResult apr WHERE apr.idProfissional = :idProfissional "
				+ "AND dtInclusao BETWEEN :dataInicio AND :dataFim";
		
		TypedQuery<AtendimentoProfissionalResult> result = entityManager.createQuery(query,
				AtendimentoProfissionalResult.class).setParameter("idProfissional", idProfissional)
				.setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE);
//		String query = "SELECT apr FROM AtendimentoProfissionalResult apr WHERE apr.idProfissional = "+idProfissional
//				+ " AND dtInclusao BETWEEN '"+dataInicio+"' AND '"+dataFim+"'";
//		
//		TypedQuery<AtendimentoProfissionalResult> result = entityManager.createQuery(query,
//				AtendimentoProfissionalResult.class);
		try {
			List<AtendimentoProfissionalResult> apr = result.getResultList();
			return apr;
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<AtendimentoProfissionalUnidadeResultDTO> getRelarioAtendimentoProfissionalUnidade(Long idUnidade, Date dataInicio, Date dataFim){
		
		String query = "SELECT 	apr.dtInclusao, "
				+ "apr.idUnidade, "
				+ "sum(apr.qtdAtendido) as qtdatendido, "
				+ "sum(apr.qtdAtendimento) as qtdatendimento, "
				+ "sum(apr.qtdFaltoso) as qtdfaltoso, "
				+ "sum(apr.qtdBloqueado) as qtdbloqueado, "
				+ "sum(apr.qtdagendamento) as qtdagendamento, "
				+ "sum(apr.qtdAtendimentocontratado) as qtdatendimentocontratado "
				+ "FROM estatistica.tbatendimentoprofissional_result apr "
				+ "WHERE apr.idUnidade = :idUnidade "
				+ "AND dtInclusao BETWEEN :dataInicio AND :dataFim "
				+ "group by apr.dtInclusao, apr.idUnidade "
				+ "order by apr.dtInclusao";						        
						        
		
		List<AtendimentoProfissionalUnidadeResultDTO> result = findByNativeQuery(query).setParameter("idUnidade", idUnidade).setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtInclusao", DateType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("qtdAtendido", IntegerType.INSTANCE)
		        .addScalar("qtdAtendimento", IntegerType.INSTANCE)
		        .addScalar("qtdFaltoso", IntegerType.INSTANCE)
		        .addScalar("qtdBloqueado", IntegerType.INSTANCE)
		        .addScalar("qtdAgendamento", IntegerType.INSTANCE)
		        .addScalar("qtdAtendimentoContratado", IntegerType.INSTANCE)		        
		        .setResultTransformer(
		                Transformers.aliasToBean(AtendimentoProfissionalUnidadeResultDTO.class)).list();
		
				
		try {
			
			if(result != null && result.size() > 0)
				return result;
			else
				return null;			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AtendimentoProfissionalResuDTO> getRelarioAtendimentoMedicoPagamento( Date dataInicio, Date dataFim){
	
	String query ="	select t1.idprofissional, t1.nmprofissional, sum(t1.totalatendido) as totalatendido, sum(t1.totalcombinado) as totalcombinado, "
			+ " sum(t1.totalagendamento) as totalagendamento, sum(t1.totalconsultacomissao) as totalconsultacomissao, sum(t1.totalvlcomissaoconsulta) as totalvlcomissaoconsulta, "
			+ " sum(t1.vlatendimentoconsultacomissao) as vlatendimentoconsultacomissao, sum(t1.totalprocedimentocomissao) as totalprocedimentocomissao, "
			+ " sum(t1.totalvlcomissaoprocedimento) as totalvlcomissaoprocedimento, sum(t1.vlatendimentoprocedimentocomissao) as vlatendimentoprocedimentocomissao, "
			+ " sum(t1.salario) as salario, sum(t1.totalconsultanaocomissao) as totalconsultanaocomissao, sum(t1.totalprocedimentonaocomissao) as totalprocedimentonaocomissao, "
			+ " sum(t1.vlatendimentonaocomissao) as vlatendimentonaocomissao "
			+ " from( " 
					+ "SELECT idprofissional, nmprofissional,SUM(qtdatendido) AS totalatendido, SUM(qtdatendimento) AS totalcombinado, "
					+" SUM(qtdagendamento) AS totalagendamento, SUM(qtdconsultacomissao) AS totalconsultacomissao, SUM(totalvlcomissaoconsulta) AS totalvlcomissaoconsulta, "
					+" CASE WHEN SUM(qtdconsultacomissao) = 0 THEN SUM(totalvlcomissaoconsulta) "
					+" ELSE TRUNC( CAST ((SUM(totalvlcomissaoconsulta) /  SUM(qtdconsultacomissao)) AS NUMERIC), 2) "
					+" END AS vlatendimentoconsultacomissao, "
					+" SUM(qtdprocedimentocomissao) AS totalprocedimentocomissao,SUM(totalvlcomissaoprocedimento) AS totalvlcomissaoprocedimento, "
					+" CASE WHEN SUM(qtdprocedimentocomissao) = 0 THEN SUM(totalvlcomissaoprocedimento) "
					+" ELSE TRUNC( CAST ((SUM(totalvlcomissaoprocedimento) /  SUM(qtdprocedimentocomissao)) AS NUMERIC), 2) "
					+" END AS vlatendimentoprocedimentocomissao, "
				
					+" COALESCE((SELECT sum(s.vlsalario) FROM realvida.tbsalariomd s  "
							 +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
							 +" AND s.mes BETWEEN Extract('Month' From cast(:dataInicio as date)) AND Extract('Month' From cast(:dataFim as date))  "
							 +" AND s.ano BETWEEN Extract('Year' From cast(:dataInicio as date)) AND Extract('Year' From cast(:dataFim  as date)) ),0)  "
					  +" + ( "
							 +" CASE WHEN  "
										+" Extract('Month' From cast(:dataFim as date)) >= Extract('Month' From cast(current_date as date)) AND "
										+" Extract('Year' From cast(:dataFim as date)) >= Extract('Year' From cast(current_date as date))  " 
								   +" THEN "
										+" CASE WHEN COALESCE((SELECT vlsalario FROM realvida.tbsalariomd s  "
														  +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
														  +" AND s.mes = Extract('Month' From cast(current_date as date)) AND  "
														  +" s.ano = Extract('Year' From cast(current_date as date))  "
														 +" ),0) = 0  "
											+" THEN  "
												+" (SELECT vlsalario FROM realvida.tbfuncionario f WHERE f.idfuncionario = apr.idprofissional)  "
											+" ELSE "
											+" 0 "
										+" END "
									+" ELSE  "
									+"	0 "
							+" END  "
					+" ) as salario, "	
					
					 +" SUM(qtdconsultanaocomissao) AS totalconsultanaocomissao, SUM(qtdprocedimentonaocomissao) AS totalprocedimentonaocomissao, "
					  
					 +" CASE WHEN COALESCE ((SUM(qtdconsultanaocomissao) + SUM(qtdprocedimentonaocomissao)),0) = 0  "
							+" THEN  "
									+" (COALESCE((SELECT sum(s.vlsalario) FROM realvida.tbsalariomd s  "
									 +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
									 +" AND s.mes BETWEEN Extract('Month' From cast(:dataInicio as date)) AND Extract('Month' From cast(:dataFim as date))  "
									 +" AND s.ano BETWEEN Extract('Year' From cast(:dataInicio as date)) AND Extract('Year' From cast(:dataFim  as date)) ),0)  "
							+"  + ( "
									 +" CASE WHEN  "
												+" Extract('Month' From cast(:dataFim as date)) >= Extract('Month' From cast(current_date as date)) AND "
												+" Extract('Year' From cast(:dataFim as date)) >= Extract('Year' From cast(current_date as date))   "
										  +" THEN "
												+" CASE WHEN COALESCE((SELECT vlsalario FROM realvida.tbsalariomd s  "
																 +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
																 +" AND s.mes = Extract('Month' From cast(current_date as date)) AND  "
																 +" s.ano = Extract('Year' From cast(current_date as date))  "
																 +" ),0) = 0  "
													+" THEN  "
														+" (SELECT vlsalario FROM realvida.tbfuncionario f WHERE f.idfuncionario = apr.idprofissional)  "
													 +" ELSE "
													+" 0 "
												+" END "
											+" ELSE  "
												+" 0 "
									+" END  "
								+" )	) "
					 +" ELSE TRUNC (CAST((COALESCE((SELECT sum(s.vlsalario) FROM realvida.tbsalariomd s  "
					 +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
					 +" AND s.mes BETWEEN Extract('Month' From cast(:dataInicio as date)) AND Extract('Month' From cast(:dataFim as date))  "
					 +" AND s.ano BETWEEN Extract('Year' From cast(:dataInicio as date)) AND Extract('Year' From cast(:dataFim  as date)) ),0)  "
					 +"  + ( "
					 +" CASE WHEN  "
					 +"	Extract('Month' From cast(:dataFim as date)) >= Extract('Month' From cast(current_date as date)) AND "
					 +" Extract('Year' From cast(:dataFim as date)) >= Extract('Year' From cast(current_date as date))   "
					       +" THEN "
						+" CASE WHEN COALESCE((SELECT vlsalario FROM realvida.tbsalariomd s  "
							   +" WHERE s.idfuncionario = apr.idprofissional and s.idunidade = apr.idunidade "
							   +" AND s.mes = Extract('Month' From cast(current_date as date)) AND  "
							   +" s.ano = Extract('Year' From cast(current_date as date))  "
							  +" ),0) = 0  "
						     +" THEN  "
							+" (SELECT vlsalario FROM realvida.tbfuncionario f WHERE f.idfuncionario = apr.idprofissional)  "
						     +" ELSE "
							+" 0 "
						+" END "
					  +" ELSE  "
						+" 0 "
					+" END )) /(SUM(qtdconsultanaocomissao) + SUM(qtdprocedimentonaocomissao)) AS NUMERIC ), 2) END AS vlatendimentonaocomissao	 "
					
					+" FROM estatistica.tbatendimentoprofissional_result apr INNER JOIN realvida.tbunidade u ON u.idunidade = apr.idunidade  "
					+" WHERE dtinclusao BETWEEN :dataInicio  AND :dataFim "
					+" and apr.idespecialidade <> 83 "
					+" GROUP BY idprofissional, nmprofissional, apr.idunidade "
					+" ORDER BY nmprofissional "
			+ " ) t1 " 
			+ "group by t1.idprofissional, t1.nmprofissional "
			+ "ORDER BY t1.nmprofissional ";

		
		List<AtendimentoProfissionalResuDTO> result = findByNativeQuery(query).setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE)
		        .unwrap(SQLQuery.class)
		        .addScalar("idProfissional", LongType.INSTANCE)
		        .addScalar("nmProfissional", StringType.INSTANCE)
		        .addScalar("totalAtendido", IntegerType.INSTANCE)
		        .addScalar("totalCombinado", IntegerType.INSTANCE)
		        .addScalar("totalAgendamento", IntegerType.INSTANCE)
		        .addScalar("totalConsultaComissao", IntegerType.INSTANCE)
		        .addScalar("totalVlComissaoConsulta", DoubleType.INSTANCE)
		        .addScalar("vlAtendimentoConsultaComissao", DoubleType.INSTANCE)
		        .addScalar("totalProcedimentoComissao", IntegerType.INSTANCE)
		        .addScalar("totalVlComissaoProcedimento", DoubleType.INSTANCE)
		        .addScalar("vlAtendimentoProcedimentoComissao", DoubleType.INSTANCE)
		        .addScalar("salario", DoubleType.INSTANCE)
		        .addScalar("totalConsultaNaoComissao", IntegerType.INSTANCE)
		        .addScalar("totalProcedimentoNaoComissao", IntegerType.INSTANCE)
		        .addScalar("vlAtendimentoNaoComissao", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(AtendimentoProfissionalResuDTO.class)).list();

		try {
			
			if(result != null && result.size() > 0)
				return result;
			else
				return null;
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<AgendamentoPrevisaoDTO> getPrevisaoAgendamento(Long idUnidade){
		
		String query = " select t1.idespecialidade, t1.nmespecialidade, t1.idfuncionario, t1.nmfuncionario, t1.idunidade, "
				+" t1.notasms, t1.performance, "
				+" sum(t1.dia1) as dia1 , sum(t1.dia2) as dia2, sum(t1.dia3) as dia3, sum(t1.dia4) as dia4, sum(t1.dia5) as dia5, sum(t1.dia6) as dia6, sum(t1.dia7) as dia7 "		
				+" from("
				+" select * FROM crosstab("
				+" 'select t1.funcionario, t1.nmfuncionario, t1.nmespecialidade,t1.idfuncionario, t1.idespecialidade, t1.idunidade, t1.notasms as notasms,"
				 +" ROUND((t1.agendamentoscriados::float4 / t1.totalvagas::float4)*100) as performance, t1.dtagendamento, t1.vagasdisponiveis " 
				+" from( "
				+" select (f.nmfuncionario::text || esp.nmespecialidade::text) as funcionario, f.nmfuncionario, esp.nmespecialidade, "
				    + " f.idfuncionario, esp.idespecialidade, u.idunidade,"
					+ " (select realvida.processar_avaliacao(f.idfuncionario,esp.idespecialidade,u.idunidade)) as notasms, "   
					+ " ag.dtagendamento, "
					+ " (select count(idagendamento) from realvida.tbagendamento "
					+ " where idfuncionario = ag.idfuncionario and idespecialidade = espprof.idespecialidade "
					+ " and dtagendamento between current_date and current_date + 6 "
					+ " and dtexclusao is null) as agendamentoscriados,"
					+ " (select sum(atendprof.qtpacienteatender) "
					+ " from realvida.tbatendimentoprofissional atendprof "
					+ " where atendprof.idfuncionario = f.idfuncionario "
					+ " and atendprof.idespprofissional = espprof.idespprofissional "
					+ " and espprof.idespecialidade = esp.idespecialidade) as totalvagas,"
					+ " ((select sum(atendprof.qtpacienteatender) "
					+ " from realvida.tbatendimentoprofissional atendprof	"
					+ " where atendprof.idfuncionario = f.idfuncionario"
					+ " and atendprof.idespprofissional = espprof.idespprofissional"
					+ " and atendprof.indiasemana = EXTRACT( DOW FROM ag.dtagendamento)"
					+ " and espprof.idespecialidade = esp.idespecialidade"
					+ " and atendprof.idunidade = u.idunidade"
					+ " and (select count(*) from realvida.tbbloqueio bloq "
					+ " where bloq.dtexclusaobloqueio is null and bloq.idfuncionario = atendprof.idfuncionario and bloq.idunidade = atendprof.idunidade "
					+ " and bloq.inturno = atendprof.inturno and ag.dtagendamento between bloq.dtinicio and bloq.dtfim ) = 0 "
					+ " ) - count(ag.idagendamento) ) as vagasdisponiveis " 	 	
				+" from realvida.tbprofissional p "
				+" inner join realvida.tbfuncionario f on f.idfuncionario = p.idfuncionario and f.dtexclusao is null"
				+" inner join realvida.tbespprofissional espprof on f.idfuncionario = espprof.idfuncionario"
				+" inner join realvida.tbespecialidade esp on espprof.idespecialidade = esp.idespecialidade"
				+" left join realvida.tbagendamento ag on f.idfuncionario = ag.idfuncionario and ag.idespecialidade = espprof.idespecialidade"
				+" left join realvida.tbunidade u on u.idunidade = ag.idunidade"
				+" where ag.dtagendamento between current_date and current_date + 6"
				+" and ag.dtexclusao is null"
				+" and u.idunidade = "+idUnidade
				+" group by esp.idespecialidade, f.idfuncionario, ag.dtagendamento, ag.idfuncionario, espprof.idespprofissional, u.idunidade"
				+" order by 1,2) as t1',"
				+" 'select current_date + plus as date from generate_series(0, 6) as plus')"
				+" as (funcionario text, nmfuncionario text, nmespecialidade text, idfuncionario bigint, idespecialidade bigint, idunidade bigint, "
				+ "notasms FLOAT, performance int, dia1 int, dia2 int, dia3 int, dia4 int, dia5 int, dia6 int, dia7 int)"
				+" order by funcionario"
				+" ) as t1"
				+" group by t1.nmespecialidade, t1.nmfuncionario, t1.notasms, t1.performance, t1.idfuncionario, t1.idespecialidade, t1.idunidade"
				+" order by t1.nmespecialidade, t1.nmfuncionario ";					        
						        
		
		List<AgendamentoPrevisaoDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)
		        .addScalar("dia1", IntegerType.INSTANCE)
		        .addScalar("dia2", IntegerType.INSTANCE)
		        .addScalar("dia3", IntegerType.INSTANCE)
		        .addScalar("dia4", IntegerType.INSTANCE)
		        .addScalar("dia5", IntegerType.INSTANCE)
		        .addScalar("dia6", IntegerType.INSTANCE)
		        .addScalar("dia7", IntegerType.INSTANCE)
		        .addScalar("notasms", DoubleType.INSTANCE)
		        .addScalar("performance", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(AgendamentoPrevisaoDTO.class)).list();
		
				
		try {
			
			if(result != null && result.size() > 0)
				return result;
			else
				return null;			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<AprazamentoDTO> getAprazamento(Integer dias){
		
		String StrQuery = "select t2.nmespecialidade, min(t2.dtagendamento) as dtdisponivel, "
				+ " trim(right(t2.nmunidade, position(' - ' in reverse(t2.nmunidade)))) as nmunidade "
				+ " from ( "
				+ " select t1.nmunidade, t1.nmespecialidade, t1.dtagendamento, sum(t1.vagasdisponiveis) as vagasdisponiveis "
				+ " from( "
				+ " select  f.idfuncionario, f.nmfuncionario, esp.nmespecialidade, u.nmunidade, ag.dtagendamento, "
				+ " (( select sum(atendprof.qtpacienteatender) "
				+ " from realvida.tbatendimentoprofissional atendprof "
				+ " where atendprof.idfuncionario = f.idfuncionario "
				+ " and atendprof.idespprofissional = espprof.idespprofissional "
				+ " and atendprof.indiasemana = EXTRACT( DOW FROM ag.dtagendamento) "
				+ " and espprof.idespecialidade = esp.idespecialidade"
				+ " and atendprof.idunidade = u.idunidade) - count(ag.idagendamento)) as vagasdisponiveis "
				+ " from realvida.tbprofissional p "
				+ " inner join realvida.tbfuncionario f on f.idfuncionario = p.idfuncionario and f.dtexclusao is null "
				+ " inner join realvida.tbespprofissional espprof on f.idfuncionario = espprof.idfuncionario "
				+ " inner join realvida.tbespecialidade esp on espprof.idespecialidade = esp.idespecialidade "
				+ " left join realvida.tbagendamento ag on f.idfuncionario = ag.idfuncionario and ag.idespecialidade = espprof.idespecialidade "
				+ " left join realvida.tbunidade u on u.idunidade = ag.idunidade "
				+ " where ag.dtagendamento between current_date and current_date + " + dias 
				+ " and ag.dtexclusao is null"
				+ " and u.idunidade in (1,2,7) " // Unidade: 1-JN, 2-Taruma, 7-GC
				+ " and f.nmfuncionario not like '%DR.%' "
				+ " group by esp.idespecialidade, ag.dtagendamento, espprof.idespprofissional, u.idunidade, f.idfuncionario "
				+ " order by esp.nmespecialidade, ag.dtagendamento "
				+ " ) as t1 "
				+ " where (select count(idbloqueio) from realvida.tbbloqueio where t1.dtagendamento between dtinicio and dtfim and dtexclusaobloqueio is null and inturno = -1 and idfuncionario = t1.idfuncionario) = 0 "
				+ " group by t1.nmunidade, t1.nmespecialidade, t1.dtagendamento "
				+ " having sum(t1.vagasdisponiveis) > 0 "
				+ " order by nmespecialidade "
				+ " ) t2 "
				+ " group by t2.nmespecialidade, t2.nmunidade "
				+ " order by t2.nmespecialidade, dtdisponivel, t2.nmunidade";
		
		List<AprazamentoDTO> result = findByNativeQuery(StrQuery)
				.unwrap(SQLQuery.class)
				.addScalar("nmEspecialidade", StringType.INSTANCE)
				.addScalar("dtDisponivel", DateType.INSTANCE)
				.addScalar("nmUnidade", StringType.INSTANCE)
				.setResultTransformer(
		                Transformers.aliasToBean(AprazamentoDTO.class)).list();
		
		try {
			
			if(result != null && result.size() > 0)
				return result;
			else
				return null;			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}
		
	}

}
