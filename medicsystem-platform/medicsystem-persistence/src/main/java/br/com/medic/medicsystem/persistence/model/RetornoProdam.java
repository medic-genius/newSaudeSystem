package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbretornoprodam")
public class RetornoProdam implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RETORNOPRODAM_ID_SEQ")
	@SequenceGenerator(name = "RETORNOPRODAM_ID_SEQ", sequenceName = "realvida.retornoprodam_id_seq")
	@Column(name = "idretornoprodam")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtreferencia")
	private String dtReferencia;
	
	@Basic
	@Column(name = "dtretornoprodam")
	private Date dtRetornoProdam;
	
	@Basic
	@Column(name = "idfuncionario")
	private Long idFuncionario;
	
	@Basic
	@Column(name = "insituacaoarquivo")
	private Integer inSituacaoArquivo;
	
	@Basic
	@Column(name = "nmarquivo")
	private String nmArquivo;
	
	@Basic
	@Column(name = "nrcnpj")
	private String nrCnpj;
	
	@Basic
	@Column(name = "nrmesreferencia")
	private Integer nrMesReferencia;

	//bi-directional many-to-one association to Tbmovretornoprodam
	@OneToMany(mappedBy="tbretornoprodam")
	private List<MovRetornoProdam> movRetornoProdam;

	//bi-directional many-to-one association to Tbempresagrupo
	@ManyToOne
	@JoinColumn(name="idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	public RetornoProdam() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getDtReferencia() {
		return dtReferencia;
	}

	public void setDtReferencia(String dtReferencia) {
		this.dtReferencia = dtReferencia;
	}

	public Date getDtRetornoProdam() {
		return dtRetornoProdam;
	}

	public void setDtRetornoProdam(Date dtRetornoProdam) {
		this.dtRetornoProdam = dtRetornoProdam;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Integer getInSituacaoArquivo() {
		return inSituacaoArquivo;
	}

	public void setInSituacaoArquivo(Integer inSituacaoArquivo) {
		this.inSituacaoArquivo = inSituacaoArquivo;
	}

	public String getNmArquivo() {
		return nmArquivo;
	}

	public void setNmArquivo(String nmArquivo) {
		this.nmArquivo = nmArquivo;
	}

	public String getNrCnpj() {
		return nrCnpj;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public Integer getNrMesReferencia() {
		return nrMesReferencia;
	}

	public void setNrMesReferencia(Integer nrMesReferencia) {
		this.nrMesReferencia = nrMesReferencia;
	}

	public List<MovRetornoProdam> getMovRetornoProdam() {
		return movRetornoProdam;
	}

	public void setMovRetornoProdam(List<MovRetornoProdam> movRetornoProdam) {
		this.movRetornoProdam = movRetornoProdam;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public MovRetornoProdam addMovretornoProdam(MovRetornoProdam movRetornoProdam) {
		getMovRetornoProdam().add(movRetornoProdam);
		movRetornoProdam.setRetornoProdam(this);

		return movRetornoProdam;
	}

	public MovRetornoProdam removeTbmovretornoprodam(MovRetornoProdam movRetornoProdam) {
		getMovRetornoProdam().remove(movRetornoProdam);
		movRetornoProdam.setRetornoProdam(null);

		return movRetornoProdam;
	}
}