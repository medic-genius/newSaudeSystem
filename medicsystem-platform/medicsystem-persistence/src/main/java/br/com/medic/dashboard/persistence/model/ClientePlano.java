package br.com.medic.dashboard.persistence.model;

import java.text.NumberFormat;
import java.util.Locale;

public class ClientePlano {

	private String numerocontrato;
	private String situacao;
	private String formapagamento;
	private String nomecliente;
	private Integer vidas;
	private Integer qtdatendimento;
	private Double plano;
	private Double cobrado;
	private Double ultimopagamento;
	private String telefone;
	private String celular;
	private String endereco;
	private String numeroendereco;

	private String planoFormatado;
	private String cobradoFormatado;
	private String custoVidaFormatado;
	private String ultimoPagamentoFormatado;
	
	public ClientePlano() {
		// TODO Auto-generated constructor stub
	}

	public String getNumerocontrato() {
		return numerocontrato;
	}

	public void setNumerocontrato(String numerocontrato) {
		this.numerocontrato = numerocontrato;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}

	public String getNomecliente() {
		return nomecliente;
	}

	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}

	public Integer getVidas() {
		return vidas;
	}

	public void setVidas(Integer vidas) {
		this.vidas = vidas;
	}

	public Double getPlano() {
		return plano;
	}

	public void setPlano(Double plano) {
		this.plano = plano;
	}

	public Double getCobrado() {
		return cobrado;
	}

	public void setCobrado(Double cobrado) {
		this.cobrado = cobrado;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroendereco() {
		return numeroendereco;
	}

	public void setNumeroendereco(String numeroendereco) {
		this.numeroendereco = numeroendereco;
	}

	public String getPlanoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getPlano() != null)
			planoFormatado = currencyFormatter.format(getPlano());
		return planoFormatado;
	}

	public void setPlanoFormatado(String planoFormatado) {
		this.planoFormatado = planoFormatado;
	}

	public String getCobradoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getCobrado() != null)
			cobradoFormatado = currencyFormatter.format(getCobrado());
		return cobradoFormatado;
	}

	public void setCobradoFormatado(String cobradoFormatado) {
		this.cobradoFormatado = cobradoFormatado;
	}

	public String getCustoVidaFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		Double custovidacalc = new Double(0);
		if(vidas > 0)
			custovidacalc = getCobrado() / vidas;
		
		if (custovidacalc  != null)
			custoVidaFormatado = currencyFormatter.format(custovidacalc);
		return custoVidaFormatado;
	}

	public void setCustoVidaFormatado(String custoVidaFormatado) {
		this.custoVidaFormatado = custoVidaFormatado;
	}

	public Integer getQtdatendimento() {
		return qtdatendimento;
	}

	public void setQtdatendimento(Integer qtdatendimento) {
		this.qtdatendimento = qtdatendimento;
	}

	public Double getUltimopagamento() {
		return ultimopagamento;
	}

	public void setUltimopagamento(Double ultimopagamento) {
		this.ultimopagamento = ultimopagamento;
	}

	public String getUltimoPagamentoFormatado() {
		
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getUltimopagamento() != null)
			ultimoPagamentoFormatado = currencyFormatter.format(getUltimopagamento());
		
		return ultimoPagamentoFormatado;
	}

	public void setUltimoPagamentoFormatado(String ultimoPagamentoFormatado) {
		this.ultimoPagamentoFormatado = ultimoPagamentoFormatado;
	}

	
	

}
