package br.com.medic.medicsystem.persistence.model.views;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbdespesa_view")
public class DespesaView {

	@Id
	@Column(name = "iddespesa")
	private Long id;

	@Basic
	@Column(name = "nrdespesa")
	private String nrDespesa;

	@Basic
	@Column(name = "nrmatricula")
	private String nrMatricula;
	@Basic
	@Column(name = "idcliente")
	private Long idCliente;

	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;

	@Basic
	@Column(name = "nrcpf")
	private String nrCpf;

	@Basic
	@Column(name = "nrrg")
	private String nrRg;

	@Basic
	@Column(name = "nmdependente")
	private String nmDependente;

	@Basic
	@Column(name = "nmorgao")
	private String nmOrgao;

	@Basic
	@Column(name = "dtdespesa")
	private Date dtDespesa;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;

	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;

	@Basic
	@Column(name = "nrencaminhamento")
	private String nrEncaminhamento;

	public DespesaView() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrDespesa() {
		return nrDespesa;
	}

	public void setNrDespesa(String nrDespesa) {
		this.nrDespesa = nrDespesa;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrRg() {
		return nrRg;
	}

	public void setNrRg(String nrRg) {
		this.nrRg = nrRg;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}

	public String getNmOrgao() {
		return nmOrgao;
	}

	public void setNmOrgao(String nmOrgao) {
		this.nmOrgao = nmOrgao;
	}

	public Date getDtDespesa() {
		return dtDespesa;
	}

	public void setDtDespesa(Date dtDespesa) {
		this.dtDespesa = dtDespesa;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public String getNrEncaminhamento() {
		return nrEncaminhamento;
	}

	public void setNrEncaminhamento(String nrEncaminhamento) {
		this.nrEncaminhamento = nrEncaminhamento;
	}

	public String getPaciente() {
		if (getNmDependente() != null) {
			return getNmDependente();
		}
		return getNmCliente();
	}

	public String getDtDespesaFormatado() {
		if (getDtDespesa() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtDespesa());
		}
		return null;
	}

}
