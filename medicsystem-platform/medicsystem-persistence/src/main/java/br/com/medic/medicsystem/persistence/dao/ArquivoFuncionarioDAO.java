package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ArquivoFuncionario;


/**
 *  
 * @author Joelton
 * @since 02/05/2016
 * @version 1.0
 */

@Named("arquivofuncionario-dao")
@ApplicationScoped
public class ArquivoFuncionarioDAO extends RelationalDataAccessObject<ArquivoFuncionario> {
	
	@Inject
	private Logger logger;

}
