package br.com.medic.medicsystem.persistence.appmobile.enums;

public enum TipoUsuario {
	CLIENTE(1),
	DEPENDENTE(2);
	
	public int valorTipo;
	
	private TipoUsuario(int valor) {
		this.valorTipo = valor;
	}
}
