package br.com.medic.medicsystem.persistence.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "vendas", name = "new_prospect")
public class NewProspect {
	
	@Id
	@Column(name = "id")
	private Long id;
		
	@Basic
	@Column(name="matricula")
	private String nrMatricula;
	
	@Basic
	@Column(name="nome")
	private String nmNome;
	
	@Basic
	@Column(name="orgao")
	private String nrOrgao;
	
	@Basic
	@Column(name="idfuncionario")
	private String idFuncionario;
	
	@Basic
	@Column(name="cpf")
	private String nrCpf;
	
	@Basic
	@Column(name="vinc")
	private String vinculo;
	
	@Basic
	@Column(name="situacao")
	private String situacao;
	
	@Basic
	@Column(name="uf")
	private String nmUnidadeFederativa;
	
	@Basic
	@Column(name="data_term")
	private String dtTermino;
	
	@Basic
	@Column(name="ativo")
	private String ativo;
	
	@Basic
	@Column(name="nascimento")
	private String dtNascimento;
	
	@Basic
	@Column(name="endereco")
	private String nmEndereco;
	
	@Basic
	@Column(name="cep")
	private String nrCep;
	
	@Basic
	@Column(name="fone")
	private String nrTelefone;
	
	@Basic
	@Column(name="bairro")
	private String nmBairro;
	
	@Basic
	@Column(name="banco")
	private String nrBanco;
	
	@Basic
	@Column(name="agencia")
	private String nrAgencia;
	
	@Basic
	@Column(name="digitoagencia")
	private String nrDigitoAgencia;
	
	@Basic
	@Column(name="conta")
	private String nrConta;
	
	@Basic
	@Column(name="digitoconta")
	private String nrDigitoConta;
	
	@Basic
	@Column(name="cargo")
	private String nmCargo;
	
	@Basic
	@Column(name="nuluz")
	private String jsonStr;
	
	@Basic
	@Column(name="celular")
	private String nrCelular;
	
	@Basic
	@Column(name="email")
	private String nmEmail;
	
	@Basic
	@Column(name="esfera")
	private String nmEsfera;
	
	@JsonProperty
	@Transient
	private Orgao orgao;
	
	@JsonProperty
	@Transient
	private Banco banco;
	
	@JsonProperty
	@Transient
	private Agencia agencia;
	

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public String getNmNome() {
		return nmNome;
	}

	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}

	public String getNrOrgao() {
		return nrOrgao;
	}

	public void setNrOrgao(String nrOrgao) {
		this.nrOrgao = nrOrgao;
	}

	public String getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(String idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getVinculo() {
		return vinculo;
	}

	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getNmUnidadeFederativa() {
		return nmUnidadeFederativa;
	}

	public void setNmUnidadeFederativa(String nmUnidadeFederativa) {
		this.nmUnidadeFederativa = nmUnidadeFederativa;
	}

	public String getDtTermino() {
		return dtTermino;
	}

	public void setDtTermino(String dtTermino) {
		this.dtTermino = dtTermino;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public String getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNmEndereco() {
		return nmEndereco;
	}

	public void setNmEndereco(String nmEndereco) {
		this.nmEndereco = nmEndereco;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

	public String getNrBanco() {
		return nrBanco;
	}

	public void setNrBanco(String nrBanco) {
		this.nrBanco = nrBanco;
	}

	public String getNrAgencia() {
		return nrAgencia;
	}

	public void setNrAgencia(String nrAgencia) {
		this.nrAgencia = nrAgencia;
	}

	public String getNrDigitoAgencia() {
		return nrDigitoAgencia;
	}

	public void setNrDigitoAgencia(String nrDigitoAgencia) {
		this.nrDigitoAgencia = nrDigitoAgencia;
	}

	public String getNrConta() {
		return nrConta;
	}

	public void setNrConta(String nrConta) {
		this.nrConta = nrConta;
	}

	public String getNrDigitoConta() {
		return nrDigitoConta;
	}

	public void setNrDigitoConta(String nrDigitoConta) {
		this.nrDigitoConta = nrDigitoConta;
	}

	public String getNmCargo() {
		return nmCargo;
	}

	public void setNmCargo(String nmCargo) {
		this.nmCargo = nmCargo;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmEsfera() {
		return nmEsfera;
	}

	public void setNmEsfera(String nmEsfera) {
		this.nmEsfera = nmEsfera;
	}

}
