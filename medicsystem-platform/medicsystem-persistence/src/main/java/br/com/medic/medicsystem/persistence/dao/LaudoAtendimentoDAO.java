package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.LaudoAtendimento;

@Named("laudoatendimento-dao")
@ApplicationScoped
public class LaudoAtendimentoDAO extends RelationalDataAccessObject<LaudoAtendimento>{
	
	public LaudoAtendimento getLaudoAtendimento(Long id) {
		return searchByKey(LaudoAtendimento.class, id);
	}
	
	public LaudoAtendimento getLaudoAtendimentoPorAgendamento(Long idAgendamento) {
		
		String query = "SELECT la FROM LaudoAtendimento la "
				+ "WHERE la.atendimento.agendamento.id = "+ idAgendamento;
		
		TypedQuery<LaudoAtendimento> queryStr = entityManager.createQuery(query, LaudoAtendimento.class);
						
		try {
			return queryStr.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<LaudoAtendimento> getLaudoAtendimentoPorCliente(Long idCliente) {

		String query = "SELECT la FROM LaudoAtendimento la "
				+ "WHERE la.atendimento.agendamento.cliente.id = "+ idCliente;

		TypedQuery<LaudoAtendimento> queryStr = entityManager.createQuery(query, LaudoAtendimento.class);

		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}	

}
