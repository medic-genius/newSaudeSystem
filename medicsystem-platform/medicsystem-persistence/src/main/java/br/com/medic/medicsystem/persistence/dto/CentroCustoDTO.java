package br.com.medic.medicsystem.persistence.dto;

public class CentroCustoDTO {
	private Long idEmpresaFinanceiroStart;
	private String nmCentroCusto;
	private Long idCentroCusto;
	private Double totalDespesa;
	private String mesAno;


	public Long getIdEmpresaFinanceiroStart() {	
		return idEmpresaFinanceiroStart;
	}
	public void setIdEmpresaFinanceiroStart(Long idEmpresaFinanceiroStart) {
		this.idEmpresaFinanceiroStart = idEmpresaFinanceiroStart;
	}
	public String getNmCentroCusto() {
		return nmCentroCusto;
	}
	public void setNmCentroCusto(String nmCentroCusto) {
		this.nmCentroCusto = nmCentroCusto;
	}
	public Long getIdCentroCusto() {
		return idCentroCusto;
	}
	public void setIdCentroCusto(Long idCentroCusto) {
		this.idCentroCusto = idCentroCusto;
	}
	public Double getTotalDespesa() {
		return totalDespesa;
	}
	public void setTotalDespesa(Double totalDespesa) {
		this.totalDespesa = totalDespesa;
	}

	public String getMesAno() {
		return mesAno;
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}


}
