package br.com.medic.medicsystem.persistence.model.views;

import java.io.Serializable;

import javax.persistence.*;

import br.com.medic.medicsystem.persistence.dto.VagaDTO;

import java.sql.Time;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbatendimentoprofissional_view")
public class AtendimentoProfissionalView implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idatendimentoprofissional")
	private Long idAtendimentoProfissional;
	
	@Basic
	@Column(name = "hrfim")
	private Time hrFim;
	
	@Basic
	@Column(name = "hrinicio")
	private Time hrInicio;
	
	@Basic
	@Column(name = "hrtempoconsulta")
	private Time hrTempoConsulta;
	
	@Basic
	@Column(name = "idespprofissional")
	private Long idEspProfissional;
	
	@Basic
	@Column(name = "idprofissional")
	private Long idProfissional;
	
	@Basic
	@Column(name = "idsubespprofissional")
	private Long idSubEspProfissional;
	
	@Basic
	@Column(name = "idunidade")
	private Long idUnidade;
	
	@Basic
	@Column(name = "indiasemana")
	private Integer inDiaSemana;
	
	@Basic
	@Column(name = "intipoatendimento")
	private Integer inTipoAtendimento;
	
	@Basic
	@Column(name = "intipoconsulta")
	private Integer inTipoConsulta;
	
	@Basic
	@Column(name = "intipoprofissional")
	private Integer inTipoProfissional;
	
	@Basic
	@Column(name = "inturno")
	private Integer inTurno;

	@Basic
	@Column(name = "nmdiasemana")
	private String nmDiaSemana;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "nmhrfim")
	private String nmHrFim;
	
	@Basic
	@Column(name = "nmhrinicio")
	private String nmHrInicio;
	
	@Basic
	@Column(name = "nmhrtempoconsulta")
	private String nmHrTempoConsulta;
	
	@Basic
	@Column(name = "nmprofissional")
	private String nmProfissional;
	
	@Basic
	@Column(name = "nmsubespecialidade")
	private String nmSubEspecialidade;
	
	@Basic
	@Column(name = "nmtipoatendimento")
	private String nmTipoAtendimento;
	
	@Basic
	@Column(name = "nmtipoconsulta")
	private String nmTipoConsulta;
	
	@Basic
	@Column(name = "nmturno")
	private String nmTurno;
	
	@Basic
	@Column(name = "nmunidade")
	private String nmUnidade;
	
	@Basic
	@Column(name = "qtpacienteatender")
	private Integer qtPacienteAtender;
	
	@Basic
	@Column(name = "nrcrmcro")
	private String nrCrmCro;
	
	@Basic
	@Column(name = "bofixo")
	private Boolean boFixo;
	
	@Basic
	@Column(name = "boproducao")
	private Boolean boProducao;
	
	@Transient
	private Long idServico;
	
	@Transient
	private String nmServico;
	
	@Transient
	private List<VagaDTO> vagas;
	
	@Transient
	private List<VagaDTO> vagasParticular;
	
	
	
	public AtendimentoProfissionalView() {
	}

	public Time getHrFim() {
		return hrFim;
	}

	public void setHrFim(Time hrFim) {
		this.hrFim = hrFim;
	}

	public Time getHrInicio() {
		return hrInicio;
	}

	public void setHrInicio(Time hrInicio) {
		this.hrInicio = hrInicio;
	}

	public Time getHrTempoConsulta() {
		return hrTempoConsulta;
	}

	public void setHrTempoConsulta(Time hrTempoConsulta) {
		this.hrTempoConsulta = hrTempoConsulta;
	}

	public Long getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Long idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public Long getIdEspProfissional() {
		return idEspProfissional;
	}

	public void setIdEspProfissional(Long idEspProfissional) {
		this.idEspProfissional = idEspProfissional;
	}

	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Long getIdSubEspProfissional() {
		return idSubEspProfissional;
	}

	public void setIdSubEspProfissional(Long idSubEspProfissional) {
		this.idSubEspProfissional = idSubEspProfissional;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Integer getInDiaSemana() {
		return inDiaSemana;
	}

	public void setInDiaSemana(Integer inDiaSemana) {
		this.inDiaSemana = inDiaSemana;
	}

	public Integer getInTipoAtendimento() {
		return inTipoAtendimento;
	}

	public void setInTipoAtendimento(Integer inTipoAtendimento) {
		this.inTipoAtendimento = inTipoAtendimento;
	}

	public Integer getInTipoConsulta() {
		return inTipoConsulta;
	}

	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}

	public Integer getInTipoProfissional() {
		return inTipoProfissional;
	}

	public void setInTipoProfissional(Integer inTipoProfissional) {
		this.inTipoProfissional = inTipoProfissional;
	}

	public Integer getInTurno() {
		return inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public String getNmDiaSemana() {
		return nmDiaSemana;
	}

	public void setNmDiaSemana(String nmDiaSemana) {
		this.nmDiaSemana = nmDiaSemana;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmHrFim() {
		return nmHrFim;
	}

	public void setNmHrFim(String nmHrFim) {
		this.nmHrFim = nmHrFim;
	}

	public String getNmHrInicio() {
		return nmHrInicio;
	}

	public void setNmHrInicio(String nmHrInicio) {
		this.nmHrInicio = nmHrInicio;
	}

	public String getNmHrTempoConsulta() {
		return nmHrTempoConsulta;
	}

	public void setNmHrTempoConsulta(String nmHrTempoConsulta) {
		this.nmHrTempoConsulta = nmHrTempoConsulta;
	}

	public String getNmProfissional() {
		return "Dr(a) "+nmProfissional;
	}

	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}

	public String getNmSubEspecialidade() {
		return nmSubEspecialidade;
	}

	public void setNmSubEspecialidade(String nmSubEspecialidade) {
		this.nmSubEspecialidade = nmSubEspecialidade;
	}

	public String getNmTipoAtendimento() {
		return nmTipoAtendimento;
	}

	public void setNmTipoAtendimento(String nmTipoAtendimento) {
		this.nmTipoAtendimento = nmTipoAtendimento;
	}

	public String getNmTipoConsulta() {
		return nmTipoConsulta;
	}

	public void setNmTipoConsulta(String nmTipoConsulta) {
		this.nmTipoConsulta = nmTipoConsulta;
	}

	public String getNmTurno() {
		return nmTurno;
	}

	public void setNmTurno(String nmTurno) {
		this.nmTurno = nmTurno;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Integer getQtPacienteAtender() {
		return qtPacienteAtender;
	}

	public void setQtPacienteAtender(Integer qtPacienteAtender) {
		this.qtPacienteAtender = qtPacienteAtender;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public List<VagaDTO> getVagas() {
		return vagas;
	}

	public void setVagas(List<VagaDTO> vagas) {
		this.vagas = vagas;
	}

	public String getNrCrmCro() {
		return nrCrmCro;
	}

	public void setNrCrmCro(String nrCrmCro) {
		this.nrCrmCro = nrCrmCro;
	}

	public List<VagaDTO> getVagasParticular() {
		return vagasParticular;
	}

	public void setVagasParticular(List<VagaDTO> vagasParticular) {
		this.vagasParticular = vagasParticular;
	}

	public Boolean getBoFixo() {
		return boFixo;
	}

	public void setBoFixo(Boolean boFixo) {
		this.boFixo = boFixo;
	}

	public Boolean getBoProducao() {
		return boProducao;
	}

	public void setBoProducao(Boolean boProducao) {
		this.boProducao = boProducao;
	}
	
	


	
	
	
	
}