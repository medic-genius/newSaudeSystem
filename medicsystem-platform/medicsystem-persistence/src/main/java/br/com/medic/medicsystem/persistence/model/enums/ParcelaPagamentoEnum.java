package br.com.medic.medicsystem.persistence.model.enums;

public enum ParcelaPagamentoEnum {

	FORMA_PAGAMENTO_DEBITOAUTOMATICO(1), FORMA_PAGAMENTO_CARTAOCREDITO(4), FORMA_PAGAMENTO_CONVENIO(
	        15), FORMA_PAGAMENTO_FOLHA_DE_PAGAMENTO(17);

	private Integer id;

	ParcelaPagamentoEnum(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
