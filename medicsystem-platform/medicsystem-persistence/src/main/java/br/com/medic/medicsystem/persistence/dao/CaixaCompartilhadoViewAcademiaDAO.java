package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObjectAcademia;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.CaixaRetiradaDTO;
import br.com.medic.medicsystem.persistence.model.Caixa;
import br.com.medic.medicsystem.persistence.model.CaixaCompartilhadoView;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.views.ConferenciaMensalidade;
import br.com.medic.medicsystem.persistence.model.views.ConferenciaParcela;
import br.com.medic.medicsystem.persistence.model.views.FormaPagamentoCaixa;
import br.com.medic.medicsystem.persistence.model.views.FormaPagamentoCartao;
import br.com.medic.medicsystem.persistence.model.views.MovimentacaoCaixaComportilhadoView;

@ApplicationScoped
@Named("caixacompartilhadoviewacademia-dao")
public class CaixaCompartilhadoViewAcademiaDAO extends RelationalDataAccessObjectAcademia<Caixa>{

	public List<CaixaCompartilhadoView> getConferenciasCaixa(Date dtInicial, Date dtFinal, Long idEmpresaGrupo, Long idUnidade, Long idConferente, Long idOperador){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CaixaCompartilhadoView> criteria = cb.createQuery(CaixaCompartilhadoView.class);

		Root<CaixaCompartilhadoView> caixaRoot = criteria.from(CaixaCompartilhadoView.class);
		criteria.select(caixaRoot);

		Predicate p = cb.conjunction();
		
		if (idEmpresaGrupo != null) {
			p = cb.and(p, cb.equal(caixaRoot.get("idEmpresaGrupo"), idEmpresaGrupo));
		}
		if (idUnidade != null) {
			p = cb.and(p, cb.equal(caixaRoot.get("idUnidade"), idUnidade));
		}
		if (idConferente != null) {
			p = cb.and(p, cb.equal(caixaRoot.get("idConferente"), idConferente));
		}
		if (idOperador != null) {
			p = cb.and(p, cb.equal(caixaRoot.get("idOperador"), idOperador));
		}
		if(dtInicial != null && dtFinal != null){
			p = cb.and(p, cb.between(caixaRoot.get("dtCaixa"), dtInicial, dtFinal));
		}

		criteria.where(p);
		criteria.orderBy(cb.desc(caixaRoot.get("dtCaixa")), cb.asc(caixaRoot.get("nmOperador")));
	
		TypedQuery<CaixaCompartilhadoView> q = entityManager.createQuery(criteria);
		List<CaixaCompartilhadoView> result = q.getResultList();

		if (result.isEmpty()) {
			return null;
		}

		return result;
	}
	
	public List<Unidade> getUnidadesByEmpresaGrupo(Long idEmpresa){
		String queryStr = "SELECT u FROM Unidade u WHERE u.empresaGrupo.id = " + idEmpresa + " and u.dtInativacao is null ";
		TypedQuery<Unidade> query = entityManager.createQuery(queryStr, Unidade.class);
		List<Unidade> unidades = query.getResultList();
		return unidades;
	}

	public List<FormaPagamentoCartao> getFormaPagamentoCartao(Long idCaixa) {
		List<FormaPagamentoCartao> pagamentosCartao;
		String queryStr = "SELECT fpc FROM FormaPagamentoCartao fpc where fpc.idCaixa = " + idCaixa;
		TypedQuery<FormaPagamentoCartao> query = entityManager.createQuery(queryStr, FormaPagamentoCartao.class);
		pagamentosCartao = query.getResultList();
		return pagamentosCartao;
	}

	@SuppressWarnings("unchecked")
	public List<FormaPagamentoCaixa> getFormaPagamentoCaixa(Long idCaixa) {
		String queryStr = "select fpc.* from realvida.caixa_pagamento_view fpc where fpc.idcaixa = " + idCaixa ;
		List<FormaPagamentoCaixa> pagtosCaixa =  findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("idCaixa", LongType.INSTANCE)
		        .addScalar("inFormaPagamento", IntegerType.INSTANCE)
		        .addScalar("vlMovdetalhamento", FloatType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(FormaPagamentoCaixa.class))
		        .list();
		
		return pagtosCaixa;
	}

	@SuppressWarnings("unchecked")
	public List<BaseDTO> getOperadoresCaixa() {
		String queryStr = "SELECT DISTINCT ON (fun.idfuncionario, fun.nmfuncionario) fun.idfuncionario AS id, fun.nmfuncionario AS description "
				+ "FROM realvida.tbperfil per, realvida.tbfuncionario fun "
				+ "WHERE nmperfil IN ('caixa', 'administrador', 'gerentefinanceiro', 'rh') "
				+ "AND fun.dtexclusao IS NULL "
				+ "AND per.nmlogin = fun.nmlogin ORDER BY fun.nmfuncionario";

		List<BaseDTO> operadores =  findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("id", LongType.INSTANCE)
		        .addScalar("description", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BaseDTO.class))
		        .list();
		
		return operadores;
	}
	
	public List<ConferenciaParcela> getMovimentacaoParcela(Long idCaixa) {
		List<ConferenciaParcela> conferenciasParcelas;
		String queryStr = "SELECT cp FROM ConferenciaParcela cp where cp.idCaixa = " + idCaixa;
		TypedQuery<ConferenciaParcela> query = entityManager.createQuery(queryStr, ConferenciaParcela.class);
		conferenciasParcelas = query.getResultList();
		return conferenciasParcelas;
	}

	public List<ConferenciaMensalidade> getMovimentacaoMensalidade(Long idCaixa) {
		List<ConferenciaMensalidade> conferenciasMensalidade;
		String queryStr = "SELECT cm FROM ConferenciaMensalidade cm where cm.idCaixa = " + idCaixa;
		TypedQuery<ConferenciaMensalidade> query = entityManager.createQuery(queryStr, ConferenciaMensalidade.class);
		conferenciasMensalidade = query.getResultList();
		return conferenciasMensalidade ;
	}
	
	public List<MovimentacaoCaixaComportilhadoView> getMovimentaoesCaixaCompartilhado(Long idCaixa) {
		List<MovimentacaoCaixaComportilhadoView> movimentacoes;
		String queryStr = "SELECT mccv FROM MovimentacaoCaixaComportilhadoView mccv WHERE mccv.idCaixa = " + idCaixa;
		TypedQuery<MovimentacaoCaixaComportilhadoView> query = entityManager.createQuery(queryStr, MovimentacaoCaixaComportilhadoView.class);
		movimentacoes = query.getResultList();
		return movimentacoes ;
	}

	public void conferirCaixa(Caixa caixa, String nmLogin ){
		caixa.setInstatus(1);
		caixa.setDtconferencia(new Date());
		
		Funcionario func = new Funcionario();
		func.setId(getFuncionarioByLogin(nmLogin) );
		
		caixa.setTbfuncionario1( func  );
		entityManager.merge(caixa);
	}
	
	private Long getFuncionarioByLogin( String nmLogin) {
		
		Long result = 0L;
		String queryStr = "SELECT f.idfuncionario from realvida.tbfuncionario f where f.nmlogin =  '" + nmLogin + "'";
		
		try {
		
			result = (Long) findByNativeQuery(queryStr).unwrap(SQLQuery.class)
			        .addScalar("idfuncionario", LongType.INSTANCE).uniqueResult();
			
		} catch (Exception e) {
			return 0L; 
		}
		
		return result;
	}

	public void desconferirCaixa(Caixa caixa) {
		caixa.setInstatus(1);
		caixa.setDtconferencia( null );
		caixa.setTbfuncionario1( null );
		
		entityManager.merge(caixa);	
	}
	
	@SuppressWarnings("unchecked")
	public List<CaixaRetiradaDTO> getRetiradaGeralDetalhes(Long idContaBancaria, Long idEmpresa, String dtInicio,
			String dtFim, String caixas) {
		String queryStr = " SELECT mov.dtmovimentacaocaixa, f.nmfuncionario, SUM(mp.vlmovdetalhamento) as vlmovimentacaocaixa , mov.boexcluido, mov.idmovimentacaocaixa FROM realvida.tbcaixa c"
				+ " INNER JOIN realvida.tbmovimentacaocaixa mov ON c.idcaixa = mov.idcaixa"
				+ " INNER JOIN realvida.tbmovimentacaopagto mp on mov.idmovimentacaocaixa = mp.idmovimentacaocaixa"
				+ " INNER JOIN realvida.tbtipoentsaicaixa tipo ON mov.idtipoentsaicaixa = tipo.idtipoentsaicaixa"
				+ " INNER JOIN realvida.tbfuncionario f ON c.idoperador = f.idfuncionario"
				+ " WHERE tipo.idtipoentsaicaixa = 6 OR (tipo.idtipoentsaicaixa = 2 AND mp.informapagamento = 2)" //retirada
				+ " AND c.idconferente is not null and c.dtconferencia is not null"
				+ " AND c.idcaixa in (" + caixas +")"
				+ " GROUP BY mov.dtmovimentacaocaixa, f.nmfuncionario, mov.boexcluido, mov.idmovimentacaocaixa"
				+ " ORDER BY mov.dtmovimentacaocaixa, f.nmfuncionario";
		
		List<CaixaRetiradaDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("dtMovimentacaoCaixa", DateType.INSTANCE)
				.addScalar("nmFuncionario", StringType.INSTANCE)
				.addScalar("vlMovimentacaoCaixa", FloatType.INSTANCE)
				.addScalar("boExcluido", BooleanType.INSTANCE)
				.addScalar("idMovimentacaoCaixa", LongType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(CaixaRetiradaDTO.class)).list();
		
		return result; 
	}
	
	@SuppressWarnings("unchecked")
	public List<CaixaRetiradaDTO> getRetiradaDetalhes(Long idCaixa) {
		
		String queryStr = " SELECT mov.dtmovimentacaocaixa, f.nmfuncionario,SUM(mp.vlmovdetalhamento) as vlmovimentacaocaixa, mov.boexcluido, mov.idmovimentacaocaixa FROM realvida.tbcaixa c"
				+ " INNER JOIN realvida.tbmovimentacaocaixa mov ON c.idcaixa = mov.idcaixa"
				+ " INNER JOIN realvida.tbmovimentacaopagto mp on mov.idmovimentacaocaixa = mp.idmovimentacaocaixa"
				+ " INNER JOIN realvida.tbtipoentsaicaixa tipo ON mov.idtipoentsaicaixa = tipo.idtipoentsaicaixa"
				+ " INNER JOIN realvida.tbfuncionario f ON c.idoperador = f.idfuncionario"
				+ " WHERE tipo.idtipoentsaicaixa = 6 OR (tipo.idtipoentsaicaixa = 2 AND mp.informapagamento = 2)" //retirada
				+ " AND c.idconferente is not null and c.dtconferencia is not null"
				+ " AND c.idcaixa = " + idCaixa
				+ " GROUP BY mov.dtmovimentacaocaixa, f.nmfuncionario, mov.boexcluido, mov.idmovimentacaocaixa"
				+ " ORDER BY mov.dtmovimentacaocaixa, f.nmfuncionario";
		
		List<CaixaRetiradaDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("dtMovimentacaoCaixa", DateType.INSTANCE)
				.addScalar("nmFuncionario", StringType.INSTANCE)
				.addScalar("vlMovimentacaoCaixa", FloatType.INSTANCE)
				.addScalar("boExcluido", BooleanType.INSTANCE)
				.addScalar("idMovimentacaoCaixa", LongType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(CaixaRetiradaDTO.class)).list();
		return result; 
	}
}
