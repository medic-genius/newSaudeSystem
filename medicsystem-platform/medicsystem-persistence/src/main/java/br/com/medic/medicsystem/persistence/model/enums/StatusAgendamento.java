package br.com.medic.medicsystem.persistence.model.enums;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum StatusAgendamento {
	BLOQUEADO(0, "BLOQUEADO"), LIBERADO(1, "LIBERADO"), ATENDIDO(2, "ATENDIDO"), FALTOSO(3, "FALTOSO"), PRESENTE(4, "PRESENTE");
	
	private Integer id;
	private String description;
	
	private StatusAgendamento(Integer id, String description){
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static List<StatusAgendamento> getStatus(){
		List<StatusAgendamento> list = new ArrayList<StatusAgendamento>();
		for (StatusAgendamento status : StatusAgendamento.values()) {
			list.add(status);
		}
		
		return list;
	}
}
