package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;

@Named("servicoprofissional-dao")
@ApplicationScoped
public class ServicoProfissionalDAO extends RelationalDataAccessObject<ServicoProfissional>{
	
	public ServicoProfissional getServicoProfissional(Long idProfissional, Long idServico) {
		
		ServicoProfissional result = null;	
						
		String queryStr = "SELECT sp FROM ServicoProfissional sp "				
				+ " where (sp.boAtivo is null or sp.boAtivo = false) "
		        + " and sp.profissional.id = " + idProfissional
		        + " and sp.servico.id = " + idServico;
						
		try {
			TypedQuery<ServicoProfissional> query = entityManager.createQuery(queryStr,
					ServicoProfissional.class);

			query.setMaxResults(1);
			result = query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}		

		return result;
	}

}
