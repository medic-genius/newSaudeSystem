package br.com.medic.medicsystem.persistence.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ArqConciliacaoBancaria;
import br.com.medic.medicsystem.persistence.model.SugestaoConciliacaoBancaria;

@Named("conciliacaofinanceiro-dao")
@ApplicationScoped
public class ConciliacaoFinanceiroDAO extends RelationalDataAccessObject<ArqConciliacaoBancaria> {

	
	public List<ArqConciliacaoBancaria> getInfoExtratos(Long idEmpresa, Long idContaBancaria){
		
		TypedQuery<ArqConciliacaoBancaria> query = entityManager.createQuery(
				"SELECT i FROM ArqConciliacaoBancaria i "
				+ " WHERE idEmpresa = "+ idEmpresa 
				+ " AND idContaBancaria = "+ idContaBancaria
				+ " AND conciliado = false "
				+ " AND tipo like '%DEBIT%'", ArqConciliacaoBancaria.class);
		
		List<ArqConciliacaoBancaria> result = query.getResultList();
		
		if(result.isEmpty()){
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
	
		return result;
		
	}
	
	public List<String> saveArquivo(Integer idEmpresa, Integer idContaSel, String conteudoArq){
		
		String importSucess = "S";
		List<String> result = new ArrayList<String>();
		try{
			Session session = entityManager.unwrap(Session.class);

			session.doWork(new Work() {

				@Override
				public void execute(Connection conn) throws SQLException {
					CallableStatement stmt = conn.prepareCall("{call financeiro.importararquivoconciliacaobancaria(?,?,?,?)}");
					stmt.setInt(1, idEmpresa);
					stmt.setString(2, conteudoArq);
					
					stmt.registerOutParameter(3, Types.VARCHAR);
					stmt.registerOutParameter(4, Types.VARCHAR);
					
					stmt.execute();
					
					String importou = stmt.getString(3);
					String motivo = stmt.getString(4);
					
					stmt.close();
					
					if(importou.equals(importSucess)){
						result.add(importou);
					}else{
						result.add(importou);
						result.add(motivo);
					}
				
				}
			});

		}catch(Exception e){
			e.printStackTrace();
		}
		   return result;
	}
	
	public List<String> gerarConciliacao(Integer idTransBancaria, String idsDespesa){
		
		String importSucess = "S";
		List<String> result = new ArrayList<String>();
		
		try{
			Session session = entityManager.unwrap(Session.class);
			
			session.doWork(new Work(){
				public void execute(Connection conn) throws SQLException{
					CallableStatement stmt = conn.prepareCall("{call financeiro.fazerconciliacaobancaria(?,?,?,?)}");
					
					stmt.setInt(1, idTransBancaria);
					stmt.setString(2, idsDespesa);
					
					stmt.registerOutParameter(3, Types.VARCHAR);
					stmt.registerOutParameter(4, Types.VARCHAR);
					stmt.execute();
					
					String pfez = stmt.getString(3);
					String motivo = stmt.getString(4);
					
					stmt.close();
					
					if(pfez.equals(importSucess)){
						result.add(pfez);
					}else{
						result.add(pfez);
						result.add(motivo);
					}
				}
			});
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("result"+ result);
		return result;
	}
	
	public List<String> desconciliarDados(Integer idTransBancaria){
		
		String importSucess = "S";
		List<String> result = new ArrayList<String>();
				
		try{
			Session session = entityManager.unwrap(Session.class);
			
			session.doWork(new Work(){
				public void execute(Connection conn) throws SQLException{
					CallableStatement stmt = conn.prepareCall("{call financeiro.desfazerconciliacaobancaria(?,?,?)}");
					
					stmt.setInt(1, idTransBancaria);
					
					stmt.registerOutParameter(2, Types.VARCHAR);
					stmt.registerOutParameter(3, Types.VARCHAR);
					
					stmt.execute();
					
					String pdesfez = stmt.getString(2);
					String motivo = stmt.getString(3);
					
					stmt.close();
					
					if(pdesfez.equals(importSucess)){
						result.add(pdesfez);
					}else{
						result.add(pdesfez);
						result.add(motivo);
					}
				}
			});
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<String> prepararSugestao(Integer idEmpresa, Long idUsuario, Integer idBanco){
		String importSucess = "S";
		List<String> result = new ArrayList<String>();
		
		try {
			Session session = entityManager.unwrap(Session.class);
			session.doWork(new Work(){
				public void execute(Connection conn) throws SQLException{
					CallableStatement stmt = conn.prepareCall("{call financeiro.prepararsugestaodeconciliacaobancaria(?,?,?,?,?)}");
					
					stmt.setInt(1, idUsuario.intValue());
					stmt.setInt(2, idBanco);
					stmt.setInt(3, idEmpresa);
					
					stmt.registerOutParameter(4, Types.VARCHAR);
					stmt.registerOutParameter(5, Types.VARCHAR);
					
					stmt.execute();
					
					String pdesfez = stmt.getString(4);
					String motivo = stmt.getString(5);
					
					stmt.close();
					
					if(pdesfez.equals(importSucess)){
						result.add(pdesfez);
					}else{
						result.add(pdesfez);
						result.add(motivo);
					}
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*if(result.size() == 1){
			TypedQuery<SujestaoConciliacaoBancaria> query = entityManager.createQuery(
					"SELECT d FROM SujestaoConciliacaoBancaria d ", SujestaoConciliacaoBancaria.class);

			resultado = query.getResultList();
			
		}else if(resultado.isEmpty()){
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}*/
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<SugestaoConciliacaoBancaria> getSugestao(Long idUsuario, int offset, int limit){
		
		/*TypedQuery<SujestaoConciliacaoBancaria> query = entityManager.createQuery(
				"SELECT d FROM SujestaoConciliacaoBancaria d "
				+ " WHERE d.idUsuario = "+ idUsuario, SujestaoConciliacaoBancaria.class);
		
		List<SujestaoConciliacaoBancaria> resultado = query.getResultList();
		if(resultado.isEmpty()){
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}else{
			return resultado;
		}*/
		
		
		String queryStr = "SELECT d.* FROM util.tmp_sugestaodeconciliacaobancaria d WHERE d.idusuario = " + idUsuario
				+ " ORDER BY transacao_data ASC";
		
		try {
			Query query = entityManager.createNativeQuery(queryStr, SugestaoConciliacaoBancaria.class);
			//return query.getResultList();
			query.setMaxResults(limit).setFirstResult((offset-1)*limit);
			
			List<SugestaoConciliacaoBancaria> result = query.getResultList();
			return result;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	
	}
	
	public Long getTotal(Long idUsuario) {
		Query query = entityManager.createQuery("SELECT count(d) from SugestaoConciliacaoBancaria d where d.idUsuario = " + idUsuario);
		try {
			query.setMaxResults(1);
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
