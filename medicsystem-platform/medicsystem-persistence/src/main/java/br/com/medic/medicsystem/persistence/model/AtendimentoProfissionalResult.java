package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(catalog = "estatistica", name = "tbatendimentoprofissional_result")
public class AtendimentoProfissionalResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATENDIMENTOPROFISSIONAL_RESULT")
	@SequenceGenerator(name = "ATENDIMENTOPROFISSIONAL_RESULT", catalog = "estatistica", sequenceName = "atendimentoprofissional_result", allocationSize = 1)
	@Column(name = "idatendimentoprofissional")
	private Long id;
	
	@Basic
	@Column(name = "idprofissional")
	private Long idProfissional;
	
	@Basic
	@Column(name = "nmprofissional")
	private String nmProfissional;
	
	@Basic
	@Column(name = "qtdatendido")
	private Integer qtdAtendido;
	
	@Basic
	@Column(name = "qtdatendimento")
	private Integer qtdAtendimento;
		
	@Basic
	@Column(name = "qtdatendimentocontratado")
	private Integer qtdAtendimentoContratado;	
	
	@Basic
	@Column(name = "dtinclusao ")
	private Date dtInclusao;
	
	@Basic
	@Column(name = "idespecialidade ")
	private Long idEspecialidade;
	
	@Basic
	@Column(name = "idunidade")
	private Long idUnidade;	
	@Basic
	@Column(name = "qtdfaltoso")
	private Integer qtdFaltoso;
	
	@Basic
	@Column(name = "qtdbloqueado")
	private Integer qtdBloqueado;	
	
	@Basic
	@Column(name = "qtdagendamento")
	private Integer qtdAgendamento;
	
	
	
	@Transient
	@JsonProperty
	public String getDtInclusaoFormatado() {
		if (getDtInclusao() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			return sdf.format(getDtInclusao());
			
		}
		return null;
	}
	
//	@Transient
//	@JsonProperty
//	private Integer performance;

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getIdProfissional() {
		return idProfissional;
	}


	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}


	public String getNmProfissional() {
		return nmProfissional;
	}


	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}


	public Integer getQtdAtendido() {
		return qtdAtendido;
	}


	public void setQtdAtendido(Integer qtdAtendido) {
		this.qtdAtendido = qtdAtendido;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}


	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}


	public Long getIdEspecialidade() {
		return idEspecialidade;
	}


	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}


	public Integer getQtdFaltoso() {
		return qtdFaltoso;
	}


	public void setQtdFaltoso(Integer qtdFaltoso) {
		this.qtdFaltoso = qtdFaltoso;
	}


	public Integer getQtdBloqueado() {
		return qtdBloqueado;
	}


	public void setQtdBloqueado(Integer qtdBloqueado) {
		this.qtdBloqueado = qtdBloqueado;
	}


	public Integer getQtdAgendamento() {
		return qtdAgendamento;
	}


	public void setQtdAgendamento(Integer qtdAgendamento) {
		this.qtdAgendamento = qtdAgendamento;
	}
	
	
	public Integer getQtdAtendimento() {
		return qtdAtendimento;
	}

	
	public void setQtdAtendimento(Integer qtdAtendimento) {
		this.qtdAtendimento = qtdAtendimento;
	}

	
	public Integer getQtdAtendimentoContratado() {
		return qtdAtendimentoContratado;
	}

	
	public void setQtdAtendimentoContratado(Integer qtdAtendimentoContratado) {
		this.qtdAtendimentoContratado = qtdAtendimentoContratado;
	}	
	
}
