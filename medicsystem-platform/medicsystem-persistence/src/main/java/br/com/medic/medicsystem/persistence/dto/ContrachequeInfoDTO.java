package br.com.medic.medicsystem.persistence.dto;

public class ContrachequeInfoDTO {
	private String nrMatricula;
	
	private String nmOrgao;
	
	private String nmSenha;

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public String getNmOrgao() {
		return nmOrgao;
	}

	public void setNmOrgao(String nmOrgao) {
		this.nmOrgao = nmOrgao;
	}

	public String getNmSenha() {
		return nmSenha;
	}

	public void setNmSenha(String nmSenha) {
		this.nmSenha = nmSenha;
	}
}
