package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.Receituario;
import br.com.medic.medicsystem.persistence.model.ReceituarioOftalmologico;


@Named("receituario-dao")
@ApplicationScoped
public class ReceituarioDAO  extends RelationalDataAccessObject<Receituario>  {
	
	
	public Receituario getReceituarioPorAtendimento (Long idAtendimento){
		
		String query = "SELECT rec FROM Receituario rec WHERE rec.atendimento.id = "+ idAtendimento;
		
		TypedQuery<Receituario> queryStr = entityManager.createQuery(query,
				Receituario.class);
		
		queryStr.setFirstResult(0);
		queryStr.setMaxResults(1);
		
		try {
			return queryStr.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	
		
	}
	


}
