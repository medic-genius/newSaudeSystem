package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbenviosms")
public class EnvioSMS implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENVIOSMS_ID_SEQ")
	@SequenceGenerator(name = "ENVIOSMS_ID_SEQ", sequenceName = "realvida.tbenviosms_idenvio_seq", allocationSize = 1)
	@Column(name = "idenvio")
	private Long idEnvio;

	@Basic
	@Column(name = "idcampanha")
	private String idCampanha;

	@Basic
	@Column(name = "idagendamento")
	private Long idAgendamento;

	@Basic
	@Column(name = "dtenvio")
	private Date dtEnvio;

	@Basic
	@Column(name = "boEnviado")
	private Boolean boEnviado;

	@Basic
	@Column(name = "intipoenvio")
	private Integer inTipoEnvio;

	@Basic
	@Column(name = "intipomensagem")
	private Integer inTipoMensagem;

	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;

	@Basic
	@Column(name = "mensagem")
	private String mensagem;

	@Basic
	@Column(name = "resposta")
	private String resposta;

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public String getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(String idCampanha) {
		this.idCampanha = idCampanha;
	}

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Date getDtEnvio() {
		return dtEnvio;
	}

	public void setDtEnvio(Date dtEnvio) {
		this.dtEnvio = dtEnvio;
	}

	public Boolean getBoEnviado() {
		return boEnviado;
	}

	public void setBoEnviado(Boolean boEnviado) {
		this.boEnviado = boEnviado;
	}

	public Integer getInTipoEnvio() {
		return inTipoEnvio;
	}

	public void setInTipoEnvio(Integer inTipoEnvio) {
		this.inTipoEnvio = inTipoEnvio;
	}

	public Integer getInTipoMensagem() {
		return inTipoMensagem;
	}

	public void setInTipoMensagem(Integer inTipoMensagem) {
		this.inTipoMensagem = inTipoMensagem;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
}
