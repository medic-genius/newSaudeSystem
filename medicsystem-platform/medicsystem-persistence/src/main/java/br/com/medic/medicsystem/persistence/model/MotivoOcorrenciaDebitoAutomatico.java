package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


@Entity
@Table(catalog = "realvida", name = "tbmotivoocorrenciadebitoautomatico")
public class MotivoOcorrenciaDebitoAutomatico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOTIVOOCORRENCIADEBITOAUTOMATICO_ID_SEQ")
	@SequenceGenerator(name = "MOTIVOOCORRENCIADEBITOAUTOMATICO_ID_SEQ", sequenceName = "realvida.motivoocorrencia_id_seq")
	@Column(name = "idmotivoocorrencia")
	private Long id;
	
	@Basic
	@Column(name = "cdmotivoocorrencia")
	private String cdMotivoOcorrencia;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtexclusao;
	
	@Basic
	@Column(name = "nmmotivoocorrencia")
	private String nmMotivoOcorrencia;

	//bi-directional many-to-one association to Tbbanco
	@ManyToOne
	@JoinColumn(name="idbanco")
	private Banco banco;
	
	public MotivoOcorrenciaDebitoAutomatico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCdMotivoOcorrencia() {
		return cdMotivoOcorrencia;
	}

	public void setCdMotivoOcorrencia(String cdMotivoOcorrencia) {
		this.cdMotivoOcorrencia = cdMotivoOcorrencia;
	}

	public Date getDtexclusao() {
		return dtexclusao;
	}

	public void setDtexclusao(Date dtexclusao) {
		this.dtexclusao = dtexclusao;
	}

	public String getNmMotivoOcorrencia() {
		return nmMotivoOcorrencia;
	}

	public void setNmMotivoOcorrencia(String nmMotivoOcorrencia) {
		this.nmMotivoOcorrencia = nmMotivoOcorrencia;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
}