package br.com.medic.medicsystem.persistence.model.enums;

public enum TipoOperacaoCaixa {

	ABERTURA(0), 
	TROCA_OPERADOR(1), 
	FECHAMENTO(2), 
	RECEBIMENTO(3), 
	ABASTECIMENTO(4), 
	RETIRADA(5), 
	ESTORNO_PAGAMENTO(6),
	DEVOLUCAO(7);
	
	private Integer id;
	
	private TipoOperacaoCaixa(Integer id){
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public static TipoOperacaoCaixa getTipoOperacaoCaixaById(int id){
		for (TipoOperacaoCaixa t : TipoOperacaoCaixa.values()) {
			if(id == t.id)
				return t;
		}
		return null;
	}
}
