package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AtendimentoProfissionalUnidadeResultDTO {
	
	private Date dtInclusao;
	
	private Long idUnidade;
	
	private Integer qtdAtendido;
	
	private Integer qtdAtendimento;
	
	private Integer qtdFaltoso;
	
	private Integer qtdBloqueado;
	
	private Integer qtdAgendamento;
	
	private Integer qtdAtendimentoContratado;
	
	@Transient
	@JsonProperty
	public String getDtInclusaoFormatado() {
		if (getDtInclusao() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			return sdf.format(getDtInclusao());
			
		}
		return null;
	}
	
	
	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Integer getQtdAtendido() {
		if(qtdAtendido == null)
			qtdAtendido = 0;
		
		return qtdAtendido;
	}

	public void setQtdAtendido(Integer qtdAtendido) {
		this.qtdAtendido = qtdAtendido;
	}

	public Integer getQtdAtendimento() {
		if(qtdAtendimento == null)
			qtdAtendimento = 0;
		
		return qtdAtendimento;
	}

	public void setQtdAtendimento(Integer qtdAtendimento) {
		this.qtdAtendimento = qtdAtendimento;
	}

	public Integer getQtdFaltoso() {
		if(qtdFaltoso == null)
			qtdFaltoso = 0;
		
		return qtdFaltoso;
	}

	public void setQtdFaltoso(Integer qtdFaltoso) {
		this.qtdFaltoso = qtdFaltoso;
	}

	public Integer getQtdBloqueado() {
		if(qtdBloqueado == null)
			qtdBloqueado = 0;
		
		return qtdBloqueado;
	}

	public void setQtdBloqueado(Integer qtdBloqueado) {
		this.qtdBloqueado = qtdBloqueado;
	}

	public Integer getQtdAgendamento() {
		if(qtdAgendamento == null)
			qtdAgendamento = 0;
		
		return qtdAgendamento;
	}

	public void setQtdAgendamento(Integer qtdAgendamento) {
		this.qtdAgendamento = qtdAgendamento;
	}

	public Integer getQtdAtendimentoContratado() {
		if(qtdAtendimentoContratado == null)
			qtdAtendimentoContratado = 0;
		
		return qtdAtendimentoContratado;
	}

	public void setQtdAtendimentoContratado(Integer qtdAtendimentoContratado) {
		this.qtdAtendimentoContratado = qtdAtendimentoContratado;
	}

	
	

}
