package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbpedidocompra")
public class PedidoCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PEDIDOCOMPRA_ID_SEQ")
	@SequenceGenerator(name = "PEDIDOCOMPRA_ID_SEQ", sequenceName = "realvida.pedidocompra_id_seq")
	@Column(name = "idpedidocompra")
	private Long id;
	
	@Basic
	@Column(name = "boRecebido")
	private Boolean boRecebido;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtpedido")
	private Date dtPedido;
	
	@Basic
	@Column(name = "dtrecebimento")
	private Date dtRecebimento;
	
	@Basic
	@Column(name = "nrnotafiscal")
	private String nrNotaFiscal;
	
	@Basic
	@Column(name = "nrpedidocompra")
	private String nrPedidoCompra;
	
	@Basic
	@Column(name = "vlcustototal")
	private float vlCustoTotal;

	//bi-directional many-to-one association to Tbitemsolicitado
	@OneToMany(mappedBy="pedidoCompra")
	private List<ItemSolicitado> itensSolicitados;

	//bi-directional many-to-one association to Tbfornecedor
	@ManyToOne
	@JoinColumn(name="idfornecedor")
	private Fornecedor fornecedor;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idrecebedor")
	private Funcionario funcionario1;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idsolicitador")
	private Funcionario funcionario2;

	public PedidoCompra() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoRecebido() {
		return boRecebido;
	}

	public void setBoRecebido(Boolean boRecebido) {
		this.boRecebido = boRecebido;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtPedido() {
		return dtPedido;
	}

	public void setDtPedido(Date dtPedido) {
		this.dtPedido = dtPedido;
	}

	public Date getDtRecebimento() {
		return dtRecebimento;
	}

	public void setDtRecebimento(Date dtRecebimento) {
		this.dtRecebimento = dtRecebimento;
	}

	public String getNrNotaFiscal() {
		return nrNotaFiscal;
	}

	public void setNrNotaFiscal(String nrNotaFiscal) {
		this.nrNotaFiscal = nrNotaFiscal;
	}

	public String getNrPedidoCompra() {
		return nrPedidoCompra;
	}

	public void setNrPedidoCompra(String nrPedidoCompra) {
		this.nrPedidoCompra = nrPedidoCompra;
	}

	public float getVlCustoTotal() {
		return vlCustoTotal;
	}

	public void setVlCustoTotal(float vlCustoTotal) {
		this.vlCustoTotal = vlCustoTotal;
	}

	public List<ItemSolicitado> getItensSolicitados() {
		return itensSolicitados;
	}

	public void setItensSolicitados(List<ItemSolicitado> itensSolicitados) {
		this.itensSolicitados = itensSolicitados;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Funcionario getFuncionario1() {
		return funcionario1;
	}

	public void setFuncionario1(Funcionario funcionario1) {
		this.funcionario1 = funcionario1;
	}

	public Funcionario getFuncionario2() {
		return funcionario2;
	}

	public void setFuncionario2(Funcionario funcionario2) {
		this.funcionario2 = funcionario2;
	}

	public ItemSolicitado addItemSolicitado(ItemSolicitado itemSolicitado) {
		getItensSolicitados().add(itemSolicitado);
		itemSolicitado.setPedidoCompra(this);

		return itemSolicitado;
	}

	public ItemSolicitado removeItemSolicitado(ItemSolicitado itemSolicitado) {
		getItensSolicitados().remove(itemSolicitado);
		itemSolicitado.setPedidoCompra(null);

		return itemSolicitado;
	}
}