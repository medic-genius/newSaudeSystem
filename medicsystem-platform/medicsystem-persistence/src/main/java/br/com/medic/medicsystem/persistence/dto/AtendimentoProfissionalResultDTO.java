package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.AtendimentoProfissionalResult;

public class AtendimentoProfissionalResultDTO {
	
	private List<AtendimentoProfissionalResult> AtendimentoProfissionalResults;
	
	private List<AtendimentoProfissionalUnidadeResultDTO> atendimentoProfissionalUnidadeResult;
	
	private Integer performanceClinica;
	
	private Integer performanceProfissional;
	
	private Integer indiceAgendNaoRealizados;
	
	private String nmUnidade;

	
	
	public List<AtendimentoProfissionalResult> getAtendimentoProfissionalResults() {
		return AtendimentoProfissionalResults;
	}

	
	public void setAtendimentoProfissionalResults(
			List<AtendimentoProfissionalResult> atendimentoProfissionalResults) {
		AtendimentoProfissionalResults = atendimentoProfissionalResults;
	}

	
	public Integer getPerformanceClinica() {
		return performanceClinica;
	}

	
	public void setPerformanceClinica(Integer performanceClinica) {
		this.performanceClinica = performanceClinica;
	}

	
	public Integer getPerformanceProfissional() {
		return performanceProfissional;
	}

	
	public void setPerformanceProfissional(Integer performanceProfissional) {
		this.performanceProfissional = performanceProfissional;
	}


	public String getNmUnidade() {
		return nmUnidade;
	}


	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}


	public List<AtendimentoProfissionalUnidadeResultDTO> getAtendimentoProfissionalUnidadeResult() {
		return atendimentoProfissionalUnidadeResult;
	}


	public void setAtendimentoProfissionalUnidadeResult(
			List<AtendimentoProfissionalUnidadeResultDTO> atendimentoProfissionalUnidadeResult) {
		this.atendimentoProfissionalUnidadeResult = atendimentoProfissionalUnidadeResult;
	}


	public Integer getIndiceAgendNaoRealizados() {
		return indiceAgendNaoRealizados;
	}


	public void setIndiceAgendNaoRealizados(Integer indiceAgendNaoRealizados) {
		this.indiceAgendNaoRealizados = indiceAgendNaoRealizados;
	}


}
