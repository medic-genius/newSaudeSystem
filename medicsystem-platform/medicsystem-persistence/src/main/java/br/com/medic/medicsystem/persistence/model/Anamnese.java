package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbanamnese")
public class Anamnese implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANAMNESE_ID_SEQ")
	@SequenceGenerator(name = "ANAMNESE_ID_SEQ", sequenceName = "realvida.anamnese_id_seq", allocationSize = 1)
	@Column(name = "idanamnese")
	private Long id;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "nmdoencaanterior")
	private String nmDoencaAnterior;
	
	@Basic
	@Column(name = "nmdoencafamilia")
	private String nmDoencaFamilia;

	@Basic
	@Column(name = "nmtipoalergia")
	private String nmTipoAlergia;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	public Anamnese() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getNmDoencaAnterior() {
		return nmDoencaAnterior;
	}

	public void setNmDoencaAnterior(String nmDoencaAnterior) {
		this.nmDoencaAnterior = nmDoencaAnterior;
	}

	public String getNmDoencaFamilia() {
		return nmDoencaFamilia;
	}

	public void setNmDoencaFamilia(String nmDoencaFamilia) {
		this.nmDoencaFamilia = nmDoencaFamilia;
	}

	public String getNmTipoAlergia() {
		return nmTipoAlergia;
	}

	public void setNmTipoAlergia(String nmTipoAlergia) {
		this.nmTipoAlergia = nmTipoAlergia;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

}