package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ImportaFolhaFuncFluxo;

/**
 * Entidade que representa o persistence
 * da entidade ImportaFolhaFuncFluxo
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

@Named("importafolhafuncfluxo-dao")
@ApplicationScoped
public class ImportaFolhaFuncFluxoDAO extends RelationalDataAccessObject<ImportaFolhaFuncFluxo> {

}
