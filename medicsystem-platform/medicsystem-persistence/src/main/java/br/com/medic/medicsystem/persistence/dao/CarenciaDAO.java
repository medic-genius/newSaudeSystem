package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Carencia;

@Named("carencia-dao")
@ApplicationScoped
public class CarenciaDAO extends RelationalDataAccessObject<Carencia> {

	public List<Carencia> getCarencia(Long idContrato, Long idServico) {
	    String queryStr = "SELECT c FROM Carencia c WHERE "
		        + " c.contrato.id = " + idContrato 
		        + " AND c.servico.id = " + idServico;

		TypedQuery<Carencia> query = entityManager.createQuery(queryStr,
				Carencia.class);
		List<Carencia> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
    }

}
