package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbencaminhamento")
public class Encaminhamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENCAMINHAMENTO_ID_SEQ")
	@SequenceGenerator(name = "ENCAMINHAMENTO_ID_SEQ", sequenceName = "realvida.encaminhamento_id_seq", allocationSize = 1)
	@Column(name = "idencaminhamento")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtencaminhamento")
	private Date dtEncaminhamento;

	@Basic
	@Column(name = "idencaminhamentoaux")
	private Long idEncaminhamentoAux;

	@Basic
	@Column(name = "idencaminhamentoodonto")
	private Long idEncaminhamentoOdonto;

	@Basic
	@Column(name = "nmobservacao")
	private String nmObservacao;

	@Basic
	@Column(name = "nrencaminhamento")
	private String nrEncaminhamento;

	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Profissional profissional;

	@ManyToOne
	@JoinColumn(name = "idcredenciada")
	private Credenciada credenciada;

	public Encaminhamento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtEncaminhamento() {
		return dtEncaminhamento;
	}

	public void setDtEncaminhamento(Date dtEncaminhamento) {
		this.dtEncaminhamento = dtEncaminhamento;
	}

	public Long getIdEncaminhamentoAux() {
		return idEncaminhamentoAux;
	}

	public void setIdEncaminhamentoAux(Long idEncaminhamentoAux) {
		this.idEncaminhamentoAux = idEncaminhamentoAux;
	}

	public Long getIdEncaminhamentoOdonto() {
		return idEncaminhamentoOdonto;
	}

	public void setIdEncaminhamentoOdonto(Long idEncaminhamentoOdonto) {
		this.idEncaminhamentoOdonto = idEncaminhamentoOdonto;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public String getNrEncaminhamento() {
		return nrEncaminhamento;
	}

	public void setNrEncaminhamento(String nrEncaminhamento) {
		this.nrEncaminhamento = nrEncaminhamento;
	}

	public Credenciada getCredenciada() {
		return credenciada;
	}

	public void setCredenciada(Credenciada credenciada) {
		this.credenciada = credenciada;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

}