package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbservicoprofissional")
public class ServicoProfissional implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICOPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "SERVICOPROFISSIONAL_ID_SEQ", sequenceName = "realvida.servicoprofissional_id_seq")
	@Column(name = "idservicoprofissional")
	private Long id;

	@Basic
	@Column(name = "boativo")
	private Boolean boAtivo;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "vlcomissao")
	private Double vlComissao;

	@Basic
	@Column(name = "vlservico")
	private Double vlServico;

	@Basic
	@Column(name = "vlservicoassociado")
	private Double vlServicoAssociado;

	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Profissional profissional;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	public ServicoProfissional() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoAtivo() {
		return boAtivo;
	}

	public void setBoAtivo(Boolean boAtivo) {
		this.boAtivo = boAtivo;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Double getVlComissao() {
		return vlComissao;
	}

	public void setVlComissao(Double vlComissao) {
		this.vlComissao = vlComissao;
	}

	public Double getVlServico() {
		return vlServico;
	}

	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}

	public Double getVlServicoAssociado() {
		return vlServicoAssociado;
	}

	public void setVlServicoAssociado(Double vlServicoAssociado) {
		this.vlServicoAssociado = vlServicoAssociado;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}