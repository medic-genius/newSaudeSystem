package br.com.medic.medicsystem.persistence.model.enums;

public enum FormaPagamentoFinanceiroEnum {
	CHEQUE(0, "Cheque"), DEBITO_ONLINE(1, "Débito Online"), DINHEIRO(2, "Dinheiro");
	private Integer id;
	private String description;
	
	private FormaPagamentoFinanceiroEnum(Integer id, String description){
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
