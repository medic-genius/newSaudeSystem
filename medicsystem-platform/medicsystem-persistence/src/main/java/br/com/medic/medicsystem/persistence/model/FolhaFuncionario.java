package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbfolhafuncionario database table.
 * 
 */
@Entity
public class FolhaFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBFOLHAFUNCIONARIO_IDFOLHAFUNCIONARIO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBFOLHAFUNCIONARIO_IDFOLHAFUNCIONARIO_GENERATOR")
	private Long idfolhafuncionario;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private float vldesconto;

	private float vldescontofalta;

	private float vldespesa;

	private float vldrogaria;

	private float vlhoraextra;

	private float vlliquido;

	//bi-directional many-to-one association to Tbfolhapagamento
	@ManyToOne
	@JoinColumn(name="idfolhapagamento")
	private FolhaPagamento tbfolhapagamento;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	//bi-directional many-to-one association to Tbparcela
	@ManyToOne
	@JoinColumn(name="idparcela")
	private Parcela tbparcela;


	public FolhaFuncionario() {
	}

	public Long getIdfolhafuncionario() {
		return this.idfolhafuncionario;
	}

	public void setIdfolhafuncionario(Long idfolhafuncionario) {
		this.idfolhafuncionario = idfolhafuncionario;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public float getVldesconto() {
		return this.vldesconto;
	}

	public void setVldesconto(float vldesconto) {
		this.vldesconto = vldesconto;
	}

	public float getVldescontofalta() {
		return this.vldescontofalta;
	}

	public void setVldescontofalta(float vldescontofalta) {
		this.vldescontofalta = vldescontofalta;
	}

	public float getVldespesa() {
		return this.vldespesa;
	}

	public void setVldespesa(float vldespesa) {
		this.vldespesa = vldespesa;
	}

	public float getVldrogaria() {
		return this.vldrogaria;
	}

	public void setVldrogaria(float vldrogaria) {
		this.vldrogaria = vldrogaria;
	}

	public float getVlhoraextra() {
		return this.vlhoraextra;
	}

	public void setVlhoraextra(float vlhoraextra) {
		this.vlhoraextra = vlhoraextra;
	}

	public float getVlliquido() {
		return this.vlliquido;
	}

	public void setVlliquido(float vlliquido) {
		this.vlliquido = vlliquido;
	}

	public FolhaPagamento getTbfolhapagamento() {
		return this.tbfolhapagamento;
	}

	public void setTbfolhapagamento(FolhaPagamento tbfolhapagamento) {
		this.tbfolhapagamento = tbfolhapagamento;
	}

	public Funcionario getTbfuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario tbfuncionario) {
		this.funcionario = tbfuncionario;
	}

	public Parcela getTbparcela() {
		return this.tbparcela;
	}

	public void setParcela(Parcela tbparcela) {
		this.tbparcela = tbparcela;
	}


}