package br.com.medic.medicsystem.persistence.appmobile.views;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "app_mobile", name = "historico_agendamentos_view")
public class HistoricoAgendamentosCliente {
	
	@Id
	@Column(name="idagendamento")
	private Long idAgendamento;
	
	@Basic
	@Column(name="dtagendamento")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dtAgendamento;
	
	@Basic
	@Column(name="instatus")
    private Integer inStatus;
	
	@Basic
	@Column(name="idcliente")
    private Long idCliente;
	
	@Basic
	@Column(name="iddependente")
    private Long idDependente;
	
	@Basic
	@Column(name="nmunidade")
    private String nmUnidade;
	
	@Basic
	@Column(name="nmcliente")
    private String nmCliente;
	
	@Basic
	@Column(name="nmdependente")
    private String nmDependente;
	
	@Basic
	@Column(name="nmfuncionario")
    private String nmFuncionario;
	
	@Basic
	@Column(name="nmservico")
    private String nmServico;
	
	@Basic
	@Column(name="podeavaliar")
	private Boolean podeAvaliar;
	
	@Basic
	@Column(name="nota")
	private Integer nota;
	
	@JsonProperty
	@Transient
	private String dtAgendamentoFormatada;

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Date getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdDependente() {
		return idDependente;
	}

	public void setIdDependente(Long idDependente) {
		this.idDependente = idDependente;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}
	
	public Boolean getPodeAvaliar() {
		return podeAvaliar;
	}

	public void setPodeAvaliar(Boolean podeAvaliar) {
		this.podeAvaliar = podeAvaliar;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getDtAgendamentoFormatada() {
		if (getDtAgendamento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.dtAgendamentoFormatada = sdf.format(getDtAgendamento());
			return this.dtAgendamentoFormatada;
		}
		return null;
	}
}
