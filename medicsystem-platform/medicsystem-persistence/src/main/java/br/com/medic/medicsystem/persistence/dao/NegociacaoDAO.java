package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Negociacao;


@Named("negociacao-dao")
@ApplicationScoped
public class NegociacaoDAO  extends RelationalDataAccessObject<Negociacao> {
	
	public Negociacao getNegociacao(Long idNegociacao) {
		return searchByKey(Negociacao.class, idNegociacao);
	}
	
	public List<Negociacao> getNegociacaoByDespesa(Long idDespesa) {
		String queryStr = "SELECT n FROM Negociacao n WHERE "
		        + " n.despesa.id = " + idDespesa;	        

		TypedQuery<Negociacao> query = entityManager.createQuery(queryStr, Negociacao.class);
		List<Negociacao> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

}
