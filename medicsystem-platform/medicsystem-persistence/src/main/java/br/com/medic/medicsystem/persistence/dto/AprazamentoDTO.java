package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;

public class AprazamentoDTO {

	private String nmEspecialidade;
	
	private Date dtDisponivel;
	
	private String nmUnidade;

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public Date getDtDisponivel() {
		return dtDisponivel;
	}

	public void setDtDisponivel(Date dtDisponivel) {
		this.dtDisponivel = dtDisponivel;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}
}
