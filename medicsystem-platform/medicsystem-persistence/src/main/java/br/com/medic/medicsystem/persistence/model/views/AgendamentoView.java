package br.com.medic.medicsystem.persistence.model.views;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;

import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EnvioSMS;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.SubEspecialidade;
import br.com.medic.medicsystem.persistence.model.Unidade;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbagendamento_vw")
public class AgendamentoView {

	@Id
	@Column(name = "idagendamento")
	private Long id;

	@Basic
	@Column(name = "bodespesaexamegerada")
	private Boolean boDespesaExameGerada;

	@Basic
	@Column(name = "bodespesagerada")
	private Boolean boDespesaGerada;

	@Basic
	@Column(name = "boreagendamento")
	private Boolean boReagendamento;

	@Basic
	@Column(name = "boremanejamento")
	private Boolean boRemanejamento;

	@Basic
	@Column(name = "boretorno")
	private Boolean boRetorno;
	
	@Basic
	@Column(name = "boreturnultimate")
	private Boolean boReturnUltimate;

	@Basic
	@Column(name = "boretornogerado")
	private Boolean boRetornoGerado;

	@Basic
	@Column(name = "boservicorealizado")
	private Boolean boServicoRealizado;

	@Basic
	@Column(name = "bosolicitouretorno")
	private Boolean boSolicitouRetorno;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtagendamento")
	private Date dtAgendamento;

	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "hragendamento")
	private String hrAgendamento;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "intipo")
	private Integer inTipo;

	@Basic
	@Column(name = "intipoagendamento")
	private Integer inTipoAgendamento;

	@Basic
	@Column(name = "intipoatendimento")
	private Integer inTipoAtendimento;

	@Basic
	@Column(name = "intipoconsulta")
	private Integer inTipoConsulta;

	@Basic
	@Column(name = "inturno")
	private Integer inTurno;

	@Basic
	@Column(name = "nragendamento")
	private String nrAgendamento;
	
	@Basic
	@Column(name = "nrdespesa")
	private String nrDespesa;
	
	@Basic
	@Column(name = "informapagamentodespesa")
	private Integer formaPagamentoDespesa;

	@Basic
	@Column(name = "bodespesaexc")
	private Boolean boDespesaExc;

	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name = "idagendamento")
	private List<EnvioSMS> envioSms;

	@ManyToOne
	@JoinColumn(name = "iddependente")
	private Dependente dependente;

	@ManyToOne
	@JoinColumn(name = "idespecialidade")
	private Especialidade especialidade;

	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Profissional profissional;

	@Basic
	@Column(name = "boauxiliarexameorcamento")
	private Boolean boAuxiliarExameOrcamento;

	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;

	@ManyToOne
	@JoinColumn(name = "idsubespecialidade")
	private SubEspecialidade subEspecialidade;

	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;
	
	@Basic
	@Column(name = "operadorcadastro")
	private String operadorCadastro;
	
	@Basic
	@Column(name = "operadoralteracao")
	private String operadorAlteracao;
	
	@Basic
	@Column(name = "hrficha")
	private Time hrFicha;
	
	@Basic
	@Column(name = "horapresenca")
	private Time horaPresenca;
	
	@Basic
	@Column(name = "boparticular")
	private Boolean boParticular;
	
	@Basic
	@Column(name="localizacao")
	private String localizacao;
	
	@Basic
	@Column(name="bostatustriagem")
	private Boolean boStatusTriagem;
	
	@Basic
	@Column(name="boprioridade")
	private Boolean boPrioridade;
	
	@Basic
	@Column(name = "dtfaturamentolaudo")
	private Date dtFaturamentoLaudo;
	
	@Basic
	@Column(name="bolaudo")
	private Boolean boLaudo;
	
	@Basic
	@Column(name="idguia")
	private Long idGuia;

	@JsonProperty
	@Transient
	private Boolean boAtualizarFoto;
	
	/*add 12/04/2018*/
	
	@Basic
	@Column(name= "nrcodcliente")
	private String codCliente;
	
	@Basic
	@Column(name="idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Basic
	@Column(name="ingrupofuncionario")
	private Long inGrupoFuncionario;

	public AgendamentoView() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoDespesaExameGerada() {
		return boDespesaExameGerada;
	}

	public void setBoDespesaExameGerada(Boolean boDespesaExameGerada) {
		this.boDespesaExameGerada = boDespesaExameGerada;
	}

	public Boolean getBoDespesaGerada() {
		return boDespesaGerada;
	}

	public void setBoDespesaGerada(Boolean boDespesaGerada) {
		this.boDespesaGerada = boDespesaGerada;
	}

	public Boolean getBoReagendamento() {
		return boReagendamento;
	}

	public void setBoReagendamento(Boolean boReagendamento) {
		this.boReagendamento = boReagendamento;
	}

	public Boolean getBoRemanejamento() {
		return boRemanejamento;
	}

	public void setBoRemanejamento(Boolean boRemanejamento) {
		this.boRemanejamento = boRemanejamento;
	}

	public Boolean getBoRetorno() {
		return boRetorno;
	}

	public void setBoRetorno(Boolean boRetorno) {
		this.boRetorno = boRetorno;
	}

	public Boolean getBoRetornoGerado() {
		return boRetornoGerado;
	}

	public void setBoRetornoGerado(Boolean boRetornoGerado) {
		this.boRetornoGerado = boRetornoGerado;
	}

	public Boolean getBoServicoRealizado() {
		return boServicoRealizado;
	}

	public void setBoServicoRealizado(Boolean boServicoRealizado) {
		this.boServicoRealizado = boServicoRealizado;
	}

	public Boolean getBoSolicitouRetorno() {
		return boSolicitouRetorno;
	}

	public void setBoSolicitouRetorno(Boolean boSolicitouRetorno) {
		this.boSolicitouRetorno = boSolicitouRetorno;
	}

	public Date getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getHrAgendamento() {
		return hrAgendamento;
	}

	public void setHrAgendamento(String hrAgendamento) {
		this.hrAgendamento = hrAgendamento;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public Integer getInTipoAgendamento() {
		return inTipoAgendamento;
	}

	public void setInTipoAgendamento(Integer inTipoAgendamento) {
		this.inTipoAgendamento = inTipoAgendamento;
	}

	public Integer getInTipoAtendimento() {
		return inTipoAtendimento;
	}

	public void setInTipoAtendimento(Integer inTipoAtendimento) {
		this.inTipoAtendimento = inTipoAtendimento;
	}

	public Integer getInTipoConsulta() {
		return inTipoConsulta;
	}

	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}

	public Integer getInTurno() {
		return inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public String getNrAgendamento() {
		return nrAgendamento;
	}

	public void setNrAgendamento(String nrAgendamento) {
		this.nrAgendamento = nrAgendamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public List<EnvioSMS> getEnvioSms() {
		return this.envioSms;
	}
	
	public void setEnvioSms(List<EnvioSMS> envioSms) {
		if(envioSms != null && envioSms.size() > 0) {
			this.envioSms = envioSms;
		} else {
			this.envioSms = null;
		}
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Boolean getBoAuxiliarExameOrcamento() {
		return boAuxiliarExameOrcamento;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public SubEspecialidade getSubEspecialidade() {
		return subEspecialidade;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setBoAuxiliarExameOrcamento(Boolean boAuxiliarExameOrcamento) {
		this.boAuxiliarExameOrcamento = boAuxiliarExameOrcamento;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setSubEspecialidade(SubEspecialidade subEspecialidade) {
		this.subEspecialidade = subEspecialidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public String getTipoAtendimentoFormatado() {
		if (getInTipoAtendimento() != null) {
			if (getInTipoAtendimento() == 0) {
				return "C";
			} else if (getInTipoAtendimento() == 1) {
				return "P";
			}
		}
		return "";
	}

	public String getDtAgendamentoFormatado() {
		if (getDtAgendamento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtAgendamento());
		}
		return null;
	}

	public String getPaciente() {
		if (getDependente() != null) {
			return getDependente().getNmDependente();
		}
		return getCliente().getNmCliente();
	}

	public String getRetornoFormatado() {
		if (getInTipoAgendamento() == 0
		        && (getBoSolicitouRetorno() != null && getBoSolicitouRetorno())) {
			return "SIM";
		} else if ((getBoSolicitouRetorno() == null || !getBoSolicitouRetorno())
		        && getInTipoAgendamento() == 0) {
			return "NAO";
		}

		return "-";
	}
	
	public String getRetornoAgendamentoFormatado() {
		if (getInTipoAgendamento() == 0
		        && (getBoReturnUltimate() != null && getBoReturnUltimate())) {
			return "SIM";
		} else if ((getBoReturnUltimate() == null || !getBoReturnUltimate())
		        && getInTipoAgendamento() == 0) {
			return "NAO";
		}

		return "-";
	}
	
	public String getDtFaturamentoLaudoFormatado() {
		if (getDtFaturamentoLaudo() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtFaturamentoLaudo());
		}
		return null;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Boolean getBoDespesaExc() {
		return boDespesaExc;
	}

	public void setBoDespesaExc(Boolean boDespesaExc) {
		this.boDespesaExc = boDespesaExc;
	}

	public String getNrDespesa() {
		return nrDespesa;
	}

	public void setNrDespesa(String nrDespesa) {
		this.nrDespesa = nrDespesa;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getOperadorCadastro() {
		return operadorCadastro;
	}

	public void setOperadorCadastro(String operadorCadastro) {
		this.operadorCadastro = operadorCadastro;
	}

	public String getOperadorAlteracao() {
		return operadorAlteracao;
	}

	public void setOperadorAlteracao(String operadorAlteracao) {
		this.operadorAlteracao = operadorAlteracao;
	}

	public Time getHrFicha() {
		return hrFicha;
	}

	public void setHrFicha(Time hrFicha) {
		this.hrFicha = hrFicha;
	}

	public Time getHoraPresenca() {
		return horaPresenca;
	}

	public void setHoraPresenca(Time horaPresenca) {
		this.horaPresenca = horaPresenca;
	}

	public Boolean getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}

	public Boolean getBoAtualizarFoto() {
		
		if(dependente != null && dependente.getId() != null && dependente.getFotoDependente() == null){
			return new Boolean(true);
		}else if(dependente == null && cliente != null && cliente.getId() != null && cliente.getFotoCliente() == null){
			return new Boolean(true);
		}
		return new Boolean(false);
	}

	public void setBoAtualizarFoto(Boolean boAtualizarFoto) {
		this.boAtualizarFoto = boAtualizarFoto;
	}

	public Integer getFormaPagamentoDespesa() {
		return formaPagamentoDespesa;
	}

	public void setFormaPagamentoDespesa(Integer formaPagamentoDespesa) {
		this.formaPagamentoDespesa = formaPagamentoDespesa;
	}

	public Boolean getBoReturnUltimate() {
		return boReturnUltimate;
	}

	public void setBoReturnUltimate(Boolean boReturnUltimate) {
		this.boReturnUltimate = boReturnUltimate;
	}
	
	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Boolean getBoStatusTriagem() {
		return boStatusTriagem;
	}

	public void setBoStatusTriagem(Boolean boStatusTriagem) {
		this.boStatusTriagem = boStatusTriagem;
	}

	public Boolean getBoPrioridade() {
		return boPrioridade;
	}

	public void setBoPrioridade(Boolean boPrioridade) {
		this.boPrioridade = boPrioridade;
	}
	
	public Date getDtFaturamentoLaudo() {
		return dtFaturamentoLaudo;
	}

	public void setDtFaturamentoLaudo(Date dtFaturamentoLaudo) {
		this.dtFaturamentoLaudo = dtFaturamentoLaudo;
	}
		
	public Boolean getBoLaudo() {
		return boLaudo;
	}

	public void setBoLaudo(Boolean boLaudo) {
		this.boLaudo = boLaudo;
	}

	public Long getIdGuia() {
		return idGuia;
	}

	public void setIdGuia(Long idGuia) {
		this.idGuia = idGuia;
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getInGrupoFuncionario() {
		return inGrupoFuncionario;
	}

	public void setInGrupoFuncionario(Long inGrupoFuncionario) {
		this.inGrupoFuncionario = inGrupoFuncionario;
	}

}
