package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class PacienteSearchDTO {
	
	private String nome;
	
	private Integer tipo;
	
	private Date dtNascimento;
	
	private Integer inSexo;
	
	private String nrCelular;
	
	private String nmEmail;
	
	private List<PacienteCadastroDTO> cadastrosPaciente;
	
	private List<DependenteCadastroDTO> dependentes;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public List<PacienteCadastroDTO> getCadastrosPaciente() {
		return cadastrosPaciente;
	}

	public void setCadastrosPaciente(List<PacienteCadastroDTO> cadastrosPaciente) {
		this.cadastrosPaciente = cadastrosPaciente;
	}
	
	public void addCadastroPaciente(PacienteCadastroDTO cadastro) {
		if(this.cadastrosPaciente == null) {
			this.cadastrosPaciente = new ArrayList<PacienteCadastroDTO>();
		}
		if(!this.cadastrosPaciente.contains(cadastro)) {
			this.cadastrosPaciente.add(cadastro);
		}
	}

	public List<DependenteCadastroDTO> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<DependenteCadastroDTO> dependentes) {
		this.dependentes = dependentes;
	}
	
	public void addDependente(DependenteCadastroDTO dependente) {
		if(this.dependentes == null) {
			this.dependentes = new ArrayList<DependenteCadastroDTO>();
		}
		if(!this.dependentes.contains(dependente)) {
			this.dependentes.add(dependente);
		}
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Integer getInSexo() {
		return inSexo;
	}

	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}

	public String getNrCelular() {
		 return Utils.removeSpecialChars(nrCelular);
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}	
	
	public String getDtNascimentoFormatado(){
		if (this.dtNascimento != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(this.dtNascimento);
		}
		return null;
	}
	
	
}
