package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.medic.medicsystem.persistence.model.enums.AcaoMovimentacao;
import br.com.medic.medicsystem.persistence.model.enums.TipoMovimentacao;

@Entity
@Table(name = "tbmovimentacaoconta", catalog = "financeiro")
public class MovimentacaoConta implements Serializable{

	private static final long serialVersionUID = 5220015880555371549L;

	@Id
	@Column(name = "idmovimentacaoconta")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOV_CONTA_ID_SEQ")
	@SequenceGenerator(name = "MOV_CONTA_ID_SEQ", sequenceName = "financeiro.mov_conta_id_seq", allocationSize = 1)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dtmovimentacao")
	private Date dtMovimentacao;

	@Enumerated(EnumType.ORDINAL)
	private AcaoMovimentacao acaoMovimentacao;
	
	@Column(name = "nrvalor")
	private Double nrValor;

	@Column(name = "nmdescricao")
	private String nmDescricao;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "intipo")
	private TipoMovimentacao inTipo;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idfuncionariomovimentacao")
	private Funcionario funcionarioMovimentacao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcontabancaria")
	private ContaBancaria contaBancaria;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idfuncionarioexclusao")
	private Funcionario funcionarioExclusao;
	
	@OneToMany(mappedBy = "movimentacaoConta", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<MovimentacaoItemConta> itemsConta;
	
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@JsonIgnore
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@JsonIgnore
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@JsonIgnore
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@JsonIgnore
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Column(name = "idcaixaconferido")
	private Long idCaixaConferido;
	
	@Column(name = "desccaixa")
	private String descCaixa;

	@Column(name = "haveestorno")
	private Boolean haveEstorno; // movimentacoes de acao Pagamento cuja conta foi excluida tem essa flag inicializada
	
	@Column(name = "infoestorno")
	private String infoEstorno; // complemento sob a acao do campo acima
	
	
	public MovimentacaoConta() {

	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtMovimentacao() {
		return dtMovimentacao;
	}
	public void setDtMovimentacao(Date dtMovimentacao) {
		this.dtMovimentacao = dtMovimentacao;
	}
	public AcaoMovimentacao getAcaoMovimentacao() {
		return acaoMovimentacao;
	}
	public void setAcaoMovimentacao(AcaoMovimentacao acaoMovimentacao) {
		this.acaoMovimentacao = acaoMovimentacao;
	}
	public Double getNrValor() {
		return nrValor;
	}
	public void setNrValor(Double nrValor) {
		this.nrValor = nrValor;
	}
	public Funcionario getFuncionarioMovimentacao() {
		return funcionarioMovimentacao;
	}
	public void setFuncionarioMovimentacao(Funcionario funcionarioMovimentacao) {
		this.funcionarioMovimentacao = funcionarioMovimentacao;
	}
	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}
	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}
	public Funcionario getFuncionarioExclusao() {
		return funcionarioExclusao;
	}
	public void setFuncionarioExclusao(Funcionario funcionarioExclusao) {
		this.funcionarioExclusao = funcionarioExclusao;
	}
	public List<MovimentacaoItemConta> getItemsConta() {
		return itemsConta;
	}
	public void setItemsConta(List<MovimentacaoItemConta> itemsConta) {
		this.itemsConta = itemsConta;
	}
	public Date getDtExclusao() {
		return dtExclusao;
	}
	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}
	public String getNmDescricao() {
		return nmDescricao;
	}
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	public TipoMovimentacao getInTipo() {
		return inTipo;
	}
	public void setInTipo(TipoMovimentacao inTipo) {
		this.inTipo = inTipo;
	}
	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}
	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}
	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}
	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}
	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}
	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}
	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}
	public MovimentacaoItemConta addMovimentacaoItemConta(MovimentacaoItemConta itemConta){
		itemConta.setMovimentacaoConta(this);
		getItemsConta().add(itemConta);
		return itemConta;
	}
	public MovimentacaoItemConta removeMovimentacaoItemConta(MovimentacaoItemConta itemConta){
		getItemsConta().remove(itemConta);
		itemConta.setMovimentacaoConta(null);
		return itemConta;
	}
	public Long getIdCaixaConferido() {
		return idCaixaConferido;
	}
	public void setIdCaixaConferido(Long idCaixaConferido) {
		this.idCaixaConferido = idCaixaConferido;
	}
	public String getDescCaixa() {
		return descCaixa;
	}
	public void setDescCaixa(String descCaixa) {
		this.descCaixa = descCaixa;
	}
	
	public Boolean getHaveEstorno() {
		return haveEstorno;
	}
	
	public void setHaveEstorno(Boolean haveEstorno) {
		this.haveEstorno = haveEstorno;
	}
	
	public String getInfoEstorno() {
		return infoEstorno;
	}
	
	public void setInfoEstorno(String infoEstorno) {
		this.infoEstorno = infoEstorno;
	}
}
