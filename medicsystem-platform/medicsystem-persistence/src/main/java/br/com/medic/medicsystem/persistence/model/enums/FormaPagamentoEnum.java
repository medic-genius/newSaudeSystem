package br.com.medic.medicsystem.persistence.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FormaPagamentoEnum {

			CONTRA_CHEQUE("CONTRA-CHEQUE"), 
			DEBITO_AUTOMATICO(1,"DÉBITO AUTOMÁTICO"), 
			DINHEIRO(2,"DINHEIRO"),			
			CHEQUE_A_VISTA(3, "CHEQUE À VISTA"), 
			CARTAO_DE_CREDITO("CARTÃO DE CRÉDITO"), 
			BOLETO_BANCARIO("BOLETO BANCÁRIO"), 
			CHEQUE_PRE_DATADO("CHEQUE PRÉ-DATADO"), 
			GRATUITO(7,"GRATUITO"), 
			CREDITO_PESSOAL("CRÉDITO PESSOAL"), 
			CARTAO_DE_DEBITO("CARTÃO DE DÉBITO"), 
			COBERTURA_DO_PLANO(10,"COBERTURA DO PLANO"), 
			CARNE("CARNÊ"), 
			DESCONTO("DESCONTO"), 
			DEPOSITO_EM_CONTA("DEPÓSITO EM CONTA"), 
			BAIXA_NO_SISTEMA("BAIXA NO SISTEMA"), 
			CONVENIO(15,"CONVÊNIO"), 
			RETORNO(16,"RETORNO"), 
			FOLHA_DE_PAGAMENTO(17,"FOLHA DE PAGAMENTO"),						
			
			CONTRA_CHEQUE_LOWER("Contra-Cheque"),
			BOLETO_BANCARIO_LOWER("Boleto Bancário"), 
			GRATUITO_LOWER(7,"Gratuito"), 
			CREDITO_PESSOAL_LOWER("Crédito Pessoal"), 
			COBERTURA_DO_PLANO_LOWER(10,"Cobertura de Plano"), 
			COBERTURA_LOWER("Cobertura"), 
			CARNE_LOWER("Carnê"), 
			DESCONTO_LOWER("Desconto"), 
			DEPOSITO_EM_CONTA_LOWER("Depósito em Conta"), 
			BAIXA_NO_SISTEMA_LOWER("Baixa no Sistema"), 
			RETORNO_LOWER(16,"Retorno"),
			FOLHA_DE_PAGAMENTO_LOWER(17,"Folha de Pagamento"),	
			
			DEBITO_AUTOMATICO_LOWER(1,"Débito Automático"), 
			DINHEIRO_LOWER(2, "Dinheiro"), 
			CHEQUE_A_VISTA_LOWER(3, "Cheque à vista"), 			
			CARTAO_DE_CREDITO_LOWER(4, "Cartão de Crédito"), 
			CHEQUE_PRE_DATADO_LOWER(6, "Cheque Pré-Datado"), 			
			CARTAO_DE_DEBITO_LOWER(9,"Cartão de Débito"), 
			CONVENIO_LOWER(15,"Convênio"), 
			
			
			DINNERS_CLUB("DINNERS CLUB"),
			AMERICAN_EXPRESS(0, "AMERICAN EXPRESS"),
			MASTERCARD_ELETRONIC(0, "MASTERCARD ELETRONIC"), 
			REDE_SHOP_DEBITO(1, "REDE SHOP"), 
			DINNERS_CLUB_INTERNACIONAL(1, "DINNERS CLUB INTERNACIONAL"),					
			MAESTRO(2, "MAESTRO"), 
			VISA_ELECTRON(2, "VISA ELECTRON"), 
			MASTERCARD(3,"MASTERCARD"),
			ELO_DEBITO(3, "ELO"),
			REDE_SHOP(4,"REDE SHOP"),			
			VISA(5, "VISA"), 			
			ELO(6,"ELO"),
					
			
			ELO_LOWER("Elo"),
			AMERICAN_EXPRESS_LOWER("American Express"), 
			DINNERS_CLUB_LOWER("Dinners Club"), 
			DINNERS_CLUB_INTERNACIONAL_LOWER("Dinners Club Internacional"), 
			MAESTRO_LOWER("Maestro"), 
			MASTERCARD_LOWER("MasterCard"), 
			MASTERCARD_ELETRONIC_LOWER("Mastercard Eletronic"), 
			REDE_SHOP_LOWER("Rede Shop"), 
			VISA_LOWER("Visa"), 
			VISA_ELECTRON_LOWER(" Visa Electron"), 
								
			NAO_INFORMADO("NÃO INFORMADO"),
			NAO_INFORMADO_LOWER("Não Informado"), 
			NAO_DEFINIDA_LOWER("Não Definida");
			
			
	
	private Integer id;
	
	private String description;
	
	FormaPagamentoEnum(Integer id, String description) {
		
		this.id = id;
		this.description = description;
	}
	
	FormaPagamentoEnum(String description) {

		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
	    return getDescription();
	}
}
