package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;

@Named("clienteview-dao")
@ApplicationScoped
public class ClienteViewDAO extends RelationalDataAccessObject<ClienteView>{
		
	public ClienteView getClienteView(Long idCliente) {
		String queryStr = "SELECT cv FROM ClienteView cv " + "WHERE cv.id = "
				+ idCliente;

		TypedQuery<ClienteView> query = entityManager.createQuery(queryStr,
				ClienteView.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
