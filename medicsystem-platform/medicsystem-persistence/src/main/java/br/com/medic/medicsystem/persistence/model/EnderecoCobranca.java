package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(catalog = "realvida", name = "tbenderecocobranca")
public class EnderecoCobranca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENDERECOCOBRANCA_ID_SEQ")
	@SequenceGenerator(name = "ENDERECOCOBRANCA_ID_SEQ", sequenceName = "realvida.enderecocobranca_id_seq", allocationSize = 1)
	@Column(name = "idenderecocobranca")
	private Long id;

	@Basic
	@Column(name = "boenderecoigual")
	private Boolean boEnderecoIgual;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	// bi-directional many-to-one association to Bairro
	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	// bi-directional many-to-one association to Cidade
	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	public EnderecoCobranca() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoEnderecoIgual() {
		return boEnderecoIgual;
	}

	public void setBoEnderecoIgual(Boolean boEnderecoIgual) {
		this.boEnderecoIgual = boEnderecoIgual;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}