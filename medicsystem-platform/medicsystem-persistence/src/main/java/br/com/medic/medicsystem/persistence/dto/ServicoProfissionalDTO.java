package br.com.medic.medicsystem.persistence.dto;

import br.com.medic.medicsystem.persistence.model.Profissional;

public class ServicoProfissionalDTO {

	private Profissional profissional;

	private String descricao;

	private Double valor;

	private Boolean cobertura;

	public ServicoProfissionalDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Boolean getCobertura() {
		return cobertura;
	}

	public void setCobertura(Boolean cobertura) {
		this.cobertura = cobertura;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}
