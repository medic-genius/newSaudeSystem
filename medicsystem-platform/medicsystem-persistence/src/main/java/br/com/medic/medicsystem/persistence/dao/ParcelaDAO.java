package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.TituloDa;

@Named("parcela-dao")
@ApplicationScoped
public class ParcelaDAO extends RelationalDataAccessObject<Parcela> {

	public Parcela getParcela(Long id) {

		return searchByKey(Parcela.class, id);
	}

	public Parcela getParcelaPorDespesa(Long idDespesa) {
		String queryStr = "SELECT p FROM Parcela p " + " WHERE "
		        + " p.despesa.id = " + idDespesa;

		TypedQuery<Parcela> query = entityManager.createQuery(queryStr,
		        Parcela.class);
		query.setMaxResults(1);
		Parcela result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<Parcela> getParcelaAbertasPorDespesa(Long idDespesa) {
		String queryStr = "SELECT p FROM Parcela p " + " WHERE "
		        + " p.despesa.id = " + idDespesa
		        + " and (p.boExcluida is null or p.boExcluida=false) "
		        + " and dtPagamento is null ";

		TypedQuery<Parcela> query = entityManager.createQuery(queryStr,
		        Parcela.class);
		List<Parcela> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<TituloDa> getTitulos(Long idParcela) {

		String queryStr = "SELECT p FROM TituloDa p " + " WHERE "
		        + " p.parcela.id = " + idParcela;

		TypedQuery<TituloDa> query = entityManager.createQuery(queryStr,
		        TituloDa.class);
		List<TituloDa> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<Parcela> getParcelaPagasPorDespesa(Long idDespesa) {

		String queryStr = "SELECT p FROM Parcela p " + " WHERE "
		        + " p.despesa.id = " + idDespesa
		        + " and (p.boExcluida is null or p.boExcluida=false) "
		        + " and dtPagamento is not null ";

		TypedQuery<Parcela> query = entityManager.createQuery(queryStr,
		        Parcela.class);
		List<Parcela> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Parcela getParcelaByIdCobrancaSL(Long idCobrancaSL) {
				
		Parcela result;
		
		String queryStr = "From Parcela Where (boExcluida is null or boExcluida = false) and idCobrancaBoleto_sl = " + idCobrancaSL;		
			
		TypedQuery<Parcela> query = entityManager.createQuery(queryStr, Parcela.class);
		query.setMaxResults(1);
				
		try {

			result = query.getSingleResult();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return null;
		}
				
	}
}
