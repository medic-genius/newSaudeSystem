package br.com.medic.medicsystem.persistence.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VagaDTO {
	
	private Long idUnidade;
	private String nmUnidade;
	private String data;
	private String nmLogradouro;
	private String nrNumero;
	private String nmBairro;
	private String nmCidade;
	private Boolean boParticular;
	
	
	
	public VagaDTO(Long idUnidade, String nmUnidade, String data, String nmLogradouro,
			String nrNumero, String nmBairro, String nmCidade, Boolean boParticular) {
		this.idUnidade = idUnidade;
		this.nmUnidade = nmUnidade;
		this.data = data;
		this.nrNumero = nrNumero;
		this.nmBairro = nmBairro;
		this.nmCidade = nmCidade;
		this.nmLogradouro = nmLogradouro;
		this.boParticular = boParticular;
	}



	public VagaDTO() {
	}
	
	
	public Long getIdUnidade() {
		return idUnidade;
	}
	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}
	public String getNmUnidade() {
		return nmUnidade;
	}
	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	@JsonProperty
	public String getComboVaga(){
		return data+" - MAIS CONSULTA - "+nmUnidade;
	}



	public String getNrNumero() {
		return nrNumero;
	}



	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}



	public String getNmBairro() {
		return nmBairro;
	}



	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}



	public String getNmCidade() {
		return nmCidade;
	}



	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}



	public String getNmLogradouro() {
		return nmLogradouro;
	}



	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}



	public Boolean getBoParticular() {
		return boParticular;
	}



	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}
	
	
	

}
