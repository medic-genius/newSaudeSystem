package br.com.medic.medicsystem.persistence.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ContaBancaria;

@Named("contabancaria-dao")
@ApplicationScoped
public class ContaBancariaDAO extends RelationalDataAccessObject<ContaBancaria> {

	public List<ContaBancaria> getContasBancariaByEmpresaFinanceiro(Long idEmpresaFinanceiro) {
		TypedQuery<ContaBancaria> query = entityManager.createQuery(
		        "SELECT cb FROM ContaBancaria cb WHERE cb.dtExclusao is null AND cb.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro
		        + " ORDER BY cb.nmContaBancaria", ContaBancaria.class);

		List<ContaBancaria> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	public List<ContaBancaria> getContasBancariaByEmpresaFinanceiroTipo(Long idEmpresaFinanceiro, Integer tipo) {
		TypedQuery<ContaBancaria> query = entityManager.createQuery(
		        "SELECT cb FROM ContaBancaria cb WHERE cb.dtExclusao is null AND cb.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro
		        + " AND cb.inTipo = " + tipo + " ORDER BY cb.nmContaBancaria", ContaBancaria.class);
		List<ContaBancaria> result = query.getResultList();
		return result;

	}
	
	public List<ContaBancaria> getContasByEmpresaConciliacao(Long idEmpresaFinanceiro){
		TypedQuery<ContaBancaria> query = entityManager.createQuery("SELECT cb FROM ContaBancaria cb WHERE cb.dtExclusao is null AND cb.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro
				+ " AND cb.inTipo <> 2 ORDER BY cb.nmContaBancaria", ContaBancaria.class);
		
		List<ContaBancaria> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public String getcdbBancoByIdConta(Integer idConta){
		
		TypedQuery<String> query=entityManager.createQuery("SELECT cbf.banco.cdbanco FROM ContaBancaria cbf"
				+ " WHERE cbf.id =" + idConta, String.class);
	
		String result = query.getSingleResult();
		
		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
}
