package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;

public class RegistroAtaDTO {
	
	private String nmPaciente;
	
	private Date dtRegistro;
	
	private Long idUnidade;
	
	private String nmUnidade;
	
	
	private String hrAtendimento;
	
	private String hrPonto;
	
	private String hrPontoFinal;	
	
	private Double vlHora;
	
	private String hrTotal;
	
	
	private Double vlTotal;
	
	private Double vlDiaria;
	
	private Integer statusDesconto;
	
	private String statusTurno;
	
	private Boolean boReposicao;
		
	
	private Integer idServico;
	
	private String nmServico;	

	private Boolean boConsulta;
	
	private Double vlComissao;
	
	private Boolean boRetorno;
	

	public Date getDtRegistro() {
		return dtRegistro;
	}

	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}

	public String getHrAtendimento() {
		return hrAtendimento;
	}

	public void setHrAtendimento(String hrAtendimento) {
		this.hrAtendimento = hrAtendimento;
	}

	public String getHrPonto() {
		return hrPonto;
	}

	public void setHrPonto(String hrPonto) {
		this.hrPonto = hrPonto;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public String getHrPontoFinal() {
		return hrPontoFinal;
	}

	public void setHrPontoFinal(String hrPontoFinal) {
		this.hrPontoFinal = hrPontoFinal;
	}

	public Double getVlHora() {
		return vlHora;
	}

	public void setVlHora(Double vlHora) {
		this.vlHora = vlHora;
	}

	public String getHrTotal() {
		return hrTotal;
	}

	public void setHrTotal(String hrTotal) {
		this.hrTotal = hrTotal;
	}

	public Double getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Double vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Boolean getBoConsulta() {
		return boConsulta;
	}

	public void setBoConsulta(Boolean boConsulta) {
		this.boConsulta = boConsulta;
	}
	public Integer getIdServico() {
		return idServico;
	}

	public void setIdServico(Integer idServico) {
		this.idServico = idServico;
	}

	public Double getVlDiaria() {
		return vlDiaria;
	}

	public void setVlDiaria(Double vlDiaria) {
		this.vlDiaria = vlDiaria;
	}

	public Integer getStatusDesconto() {
		return statusDesconto;
	}

	public void setStatusDesconto(Integer statusDesconto) {
		this.statusDesconto = statusDesconto;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Double getVlComissao() {
		return vlComissao;
	}

	public void setVlComissao(Double vlComissao) {
		this.vlComissao = vlComissao;
	}

	public String getStatusTurno() {
		return statusTurno;
	}

	public void setStatusTurno(String statusTurno) {
		this.statusTurno = statusTurno;
	}
	
	public Boolean getBoRetorno() {
		return boRetorno;
	}

	public void setBoRetorno(Boolean boRetorno) {
		this.boRetorno = boRetorno;
	}

	public Boolean getBoReposicao() {
		return boReposicao;
	}

	public void setBoReposicao(Boolean boReposicao) {
		this.boReposicao = boReposicao;
	}

}
