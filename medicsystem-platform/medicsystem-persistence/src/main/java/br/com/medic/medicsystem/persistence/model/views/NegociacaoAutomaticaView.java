package br.com.medic.medicsystem.persistence.model.views;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.medic.medicsystem.persistence.utils.Utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbnegociacao_automatica_view")
public class NegociacaoAutomaticaView {
	
	@Id
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name = "idplano")
	private Long idPlano;
	
	@Basic
	@Column(name = "idempresagrupo")
	private Long idEmpresaGrupo;
	
	@Basic
	@Column(name = "idcontratocliente")
	private Long idContratoCliente;
	
	@Basic
	@Column(name = "idcliente")
	private Long idCliente;
	
	@Basic
	@Column(name = "idparcela")
	private Long idParcela;
	
	@Basic
	@Column(name = "insituacaocontrato")
	private Integer inSituacaoContrato;
	
	@Basic
	@Column(name = "qtdmensalidade")
	private Integer qtdMensalidade;
	
	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;	
	
	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name = "nrcodcliente")
	private String nrCodCliente;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;
	
	@Basic
	@Column(name = "nrcpf")
	private String nrCPF;
	
	@Basic
	@Column(name = "nrrg")
	private String nrRG;
	
	@Basic
	@Column(name = "nrcep")
	private String nrCEP;
	
	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;
	
	@Basic
	@Column(name = "nmemail")
	private String nmEmail;
	
	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;
	
	@Basic
	@Column(name = "nmbairro")
	private String nmBairro;
	
	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;
	
	@Basic
	@Column(name = "nmcidade")
	private String nmCidade;
	
	@Basic
	@Column(name = "nmuf")
	private String nmUF;
		
	@Basic
	@Column(name = "nmplano")
	private String nmPlano;
	
	@Basic
	@Column(name = "endereco")
	private String endereco;
	
	@Basic
	@Column(name = "endereco1")
	private String enderecoComplemento;
		
	@Column(name = "dtnegociadoauto")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "America/Manaus")
	private Date dtNegociadoAuto;
		
	@Column(name = "dtcontrato")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "America/Manaus")
	private Date dtContrato;
	
	@Basic
	@Column(name = "valor")
	private Float vlTotal;
	
	@Basic
	@Column(name = "bonegociadoauto")
	private Boolean boNegociadoAuto;
	
	@Basic
	@Column(name = "idclienteboleto_sl")
	private Long idClienteSl;
	
	@Basic
	@Column(name = "linkcobrancaboleto_sl")
	private String linkBoleto_sl;
	
	@JsonProperty
	@Transient
	private BigDecimal vlDesconto;
	
	
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdParcela() {
		return idParcela;
	}

	public void setIdParcela(Long idParcela) {
		this.idParcela = idParcela;
	}

	public Integer getInSituacaoContrato() {
		return inSituacaoContrato;
	}

	public void setInSituacaoContrato(Integer inSituacaoContrato) {
		this.inSituacaoContrato = inSituacaoContrato;
	}

	public Integer getQtdMensalidade() {
		return qtdMensalidade;
	}

	public void setQtdMensalidade(Integer qtdMensalidade) {
		this.qtdMensalidade = qtdMensalidade;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Date getDtNegociadoAuto() {
		return dtNegociadoAuto;
	}

	public void setDtNegociadoAuto(Date dtNegociadoAuto) {
		this.dtNegociadoAuto = dtNegociadoAuto;
	}

	public Date getDtContrato() {
		return dtContrato;
	}

	public void setDtContrato(Date dtContrato) {
		this.dtContrato = dtContrato;
	}
	
	public Float getVlTotal() {
		return Utils.getFloat2DecimalPlaces(vlTotal);
	}

	public void setVlTotal(Float vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Boolean getBoNegociadoAuto() {
		return boNegociadoAuto;
	}

	public void setBoNegociadoAuto(Boolean boNegociadoAuto) {
		this.boNegociadoAuto = boNegociadoAuto;
	}
	
	public String getEnderecoComplemento() {
		return enderecoComplemento;
	}

	public void setEnderecoComplemento(String enderecoComplemento) {
		this.enderecoComplemento = enderecoComplemento;
	}
	
	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public String getNrRG() {
		return nrRG;
	}

	public void setNrRG(String nrRG) {
		this.nrRG = nrRG;
	}

	public String getNrCEP() {
		return nrCEP;
	}

	public void setNrCEP(String nrCEP) {
		this.nrCEP = nrCEP;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmCidade() {
		return nmCidade;
	}

	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}

	public String getNmUF() {
		return nmUF;
	}

	public void setNmUF(String nmUF) {
		this.nmUF = nmUF;
	}

	public Long getIdClienteSl() {
		return idClienteSl;
	}

	public void setIdClienteSl(Long idClienteSl) {
		this.idClienteSl = idClienteSl;
	}
	
	public Long getIdContratoCliente() {
		return idContratoCliente;
	}

	public void setIdContratoCliente(Long idContratoCliente) {
		this.idContratoCliente = idContratoCliente;
	}
	
	public String getLinkBoleto_sl() {
		return linkBoleto_sl;
	}

	public void setLinkBoleto_sl(String linkBoleto_sl) {
		this.linkBoleto_sl = linkBoleto_sl;
	}
    
}
