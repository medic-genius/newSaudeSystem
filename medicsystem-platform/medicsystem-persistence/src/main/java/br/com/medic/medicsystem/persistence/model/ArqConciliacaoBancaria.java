package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(catalog = "financeiro", name="tbarquivoconciliacaobancariadetalhe")
public class ArqConciliacaoBancaria implements Serializable{

	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="idtransacaobancaria")
	private Long idTransacaobancaria;
	
	
	@Basic
	@NotNull
	@Column(name = "idempresa")
	private Long idEmpresa;
	
	@Basic
	@NotNull
	@Column(name="idcontabancaria")
	private Long idContaBancaria;
	
	@Basic
	@NotNull
	@Column(name="tipo")
	private String tipo;
	
	
	@Basic
	@NotNull
	@Column(name="data")
	private Date data;
	
	@Basic
	@NotNull
	@Column(name="valor")
	private Float valor;
	
	@Basic
	@NotNull
	@Column(name="fitid")
	private String fitId;
	
	@Basic
	@NotNull
	@Column(name="checknum")
	private String checkNum;
	
	@Basic
	@NotNull
	@Column(name="memo")
	private String memo;
	
	@Basic
	@Column(name="conciliado")
	private Boolean conciliado;

	public Long getIdTransacaobancaria() {
		return idTransacaobancaria;
	}

	public void setIdTransacaobancaria(Long idTransacaobancaria) {
		this.idTransacaobancaria = idTransacaobancaria;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdContaBancaria() {
		return idContaBancaria;
	}

	public void setIdContaBancaria(Long idContaBancaria) {
		this.idContaBancaria = idContaBancaria;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {		
		this.data = data;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public String getFitId() {
		return fitId;
	}

	public void setFitId(String fitId) {
		this.fitId = fitId;
	}

	public String getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(String checkNum) {
		this.checkNum = checkNum;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Boolean getConciliado() {
		return conciliado;
	}

	public void setConciliado(Boolean conciliado) {
		this.conciliado = conciliado;
	}

	
	
}
