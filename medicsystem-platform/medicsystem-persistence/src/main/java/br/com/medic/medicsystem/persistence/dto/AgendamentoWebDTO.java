package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.Cliente;

public class AgendamentoWebDTO {
	
	private List<PacienteCadastroDTO> cadastrosPaciente;
	
	private Long idServico;
	
	private Long idEspecialidade;
	
	private Long idProfissional;
	
	private Long idUnidade;
	
	private String dtAgendamento;
	
	private List<horarioAtendimentoDTO> horarios;
	
	private Cliente cliente;
	
	private String horarioPrevisto;

	public List<PacienteCadastroDTO> getCadastrosPaciente() {
		return cadastrosPaciente;
	}

	public void setCadastrosPaciente(List<PacienteCadastroDTO> cadastrosPaciente) {
		this.cadastrosPaciente = cadastrosPaciente;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}

	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public List<horarioAtendimentoDTO> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<horarioAtendimentoDTO> horarios) {
		this.horarios = horarios;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getHorarioPrevisto() {
		return horarioPrevisto;
	}

	public void setHorarioPrevisto(String horarioPrevisto) {
		this.horarioPrevisto = horarioPrevisto;
	}
	
	
}
