package br.com.medic.dashboard.persistence.interfaces;

import java.util.Collection;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;
import javax.persistence.Query;

/**
 * Interface to represent the DAO contract.
 * 
 */
public interface IDataAccessObject<T> {

	/**
	 * Save an entity
	 * 
	 * @param bean
	 * @throws ObjectAlreadyExistsException
	 */
	Object persist(T bean);

	/**
	 * Update an entity
	 * 
	 * @param bean
	 */
	Object update(T bean);

	/**
	 * Search an entity by its key
	 * 
	 * @param entityId
	 * @throws ObjectNotFoundException
	 * @return The found object
	 */
	<E> E searchByKey(Class<E> clazz, Object object)
	        throws ObjectNotFoundException;

	/**
	 * Remove an entity
	 * 
	 * @param bean
	 */
	void remove(T bean);

	/**
	 * Find an entity using an specific query
	 * 
	 * @param hql
	 * @throws ObjectNotFoundException
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Collection findByQuery(String query);

	/**
	 * Find an entity using an specific query and matching the specified
	 * parameters
	 * 
	 * @param query
	 * @param ordened
	 *            parameters
	 * @return Collection
	 */
	Collection<?> findByQuery(String query, Object... parameters);

	/**
	 * 
	 * Find an entity using an specific query
	 * 
	 * @param query
	 * @throws ObjectNotFoundException
	 * @return
	 */
	<E> Collection<E> findByNativeQuery(String query, Class<E> clazz);

	/**
	 * 
	 * Find an entity using an specific Named Native Query and return Query
	 * Object
	 * 
	 * @param query
	 * @throws ObjectNotFoundException
	 * @return
	 */
	Query findByNamedNativeQuery(String nnqName);

	/**
	 * Load an object by query
	 * 
	 * @param query
	 * @throws ObjectNotFoundException
	 * @return
	 */
	Object loadByQuery(String query);

	/**
	 * Find an entity using an specific query defining a range for the result
	 * 
	 * @param hql
	 * @throws ObjectNotFoundException
	 * @return The result list
	 */
	@SuppressWarnings("rawtypes")
	Collection findByQuery(String query, Map<String, ? extends Object> map,
	        Integer firstResult, Integer maxResults);

	/**
	 * Load an object by query
	 * 
	 * @param query
	 * @param parameters
	 * @return Object
	 */
	Object loadByQuery(String query, Object... parameters);

	Query findByNativeQuery(String query);

}
