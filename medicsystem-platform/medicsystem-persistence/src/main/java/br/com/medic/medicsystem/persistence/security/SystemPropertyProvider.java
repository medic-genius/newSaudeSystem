package br.com.medic.medicsystem.persistence.security;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import org.jboss.logging.Logger;

public class SystemPropertyProvider {

    @Inject
    private Logger logger;
    
    @Produces
    @SystemProperty("")
    String findProperty(InjectionPoint ip) {
        SystemProperty annotation = ip.getAnnotated()
                .getAnnotation(SystemProperty.class);

        String name = annotation.value();
        String found = System.getProperty(name);
        if (found == null) {
            logger.warn(
                    "Propriedade '" + name + "' não encontrado!");
        }
        return found;
    }

}
