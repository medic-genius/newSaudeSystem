package br.com.medic.medicsystem.persistence.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.AgendamentoTotemDTO;
import br.com.medic.medicsystem.persistence.dto.DigitalClienteDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteViewDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.Consultorio;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.enums.TipoPessoa;


@Named("totem-dao")
@ApplicationScoped
public class TotemDAO extends RelationalDataAccessObject<Object>{
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;
	
	private Despesa getDespesaPorAgendamento(Long idAgendamento){
		
		TypedQuery<Despesa> result = null;
		
		String query = "SELECT d "
				+ " FROM Despesa d WHERE d.agendamento.id = :idAgendamento AND (d.boExcluida is false OR d.boExcluida IS NULL) ";
		
		 result = entityManager
			        .createQuery(query, Despesa.class).setParameter("idAgendamento", idAgendamento).setMaxResults(1);
		 
		 try {
			return  result.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	private List<AgendamentoTotemDTO> formatarAgendamentoTotem(List<Agendamento> agendamentos){
		
		List<AgendamentoTotemDTO> ListagDTO = new ArrayList<AgendamentoTotemDTO>();
		
		for (Agendamento ag : agendamentos) {
			AgendamentoTotemDTO agDTO = new AgendamentoTotemDTO();
			
			agDTO.setIdAgendamento(ag.getId());
			agDTO.setIdEspecialidade(ag.getEspecialidade().getId());
			agDTO.setNmEspecialidade(ag.getEspecialidade().getNmEspecialidade());
			if(ag.getDependente() == null){
				agDTO.setIdPaciente(ag.getCliente().getId());
				agDTO.setNmPaciente(ag.getCliente().getNmCliente());
				agDTO.setIdade(ag.getCliente().getIdade());
			}else {
				agDTO.setIdPaciente(ag.getDependente().getId());
				agDTO.setNmPaciente(ag.getDependente().getNmDependente());
				agDTO.setIdade(ag.getDependente().getIdade());
			}
			agDTO.setIdProfissional(ag.getProfissional().getId());
			agDTO.setNmProfissional(ag.getProfissional().getNmFuncionario());
			agDTO.setIdServico(ag.getServico().getId());
			agDTO.setNmServico(ag.getServico().getNmServico());
			agDTO.setInStatusAgendamento(ag.getInStatus());
			agDTO.setNmUnidade(ag.getUnidade().getNmUnidade());
			agDTO.setNmTitular(ag.getCliente().getNmCliente());
			agDTO.setTriado(ag.getServico().getBoTriagem() != null && ag.getServico().getBoTriagem().booleanValue() 
					? new Boolean(true) : new Boolean(false));
			
			agDTO.setNmRetorno(ag.getBoRetorno() != null && ag.getBoRetorno().booleanValue() 
					? "SIM" : "NÃO");
			
			agDTO.setDespesa(getDespesaPorAgendamento(agDTO.getIdAgendamento()));
			
			Consultorio consultorio = getConsultorioPorEspecialidadeProfissional(ag.getProfissional().getId(),
					ag.getEspecialidade().getId(),ag.getUnidade().getId(),ag.getHrAgendamentoFormatado());
			if(consultorio != null){
				agDTO.setNmLocal(consultorio.getLocalizacao());
				agDTO.setNmConsultorio(consultorio.getNmConsultorio());
			}
			
			ListagDTO.add(agDTO);
			
		}
		
		return ListagDTO;
		
	}

	public List<AgendamentoTotemDTO> getAgendamentosPorIdPaciente (String inTipoPaciente, Long idPaciente){
		
		TypedQuery<Agendamento> result = null;
		
		if(inTipoPaciente.equals("2")){	//dependente
			
			String query = "SELECT ag "
					+ " FROM Agendamento ag WHERE ag.dtAgendamento = current_date AND ag.dependente.id = :idPaciente"
					+ " AND ag.inStatus IN (0,1) AND dtExclusao IS NULL";
			
			
			 result = entityManager
			        .createQuery(query, Agendamento.class).setParameter("idPaciente", idPaciente);
			
		
		}else if(inTipoPaciente.equals("1")){//cliente
			
			String query = "SELECT ag "
					+ " FROM Agendamento ag WHERE dtAgendamento = current_date  AND ag.cliente.id = :idPaciente "
					+ " AND ag.dependente IS NULL AND ag.inStatus IN (0,1) AND dtExclusao IS NULL";
			
			result = entityManager
			        .createQuery(query, Agendamento.class).setParameter("idPaciente", idPaciente);
		}
		
		try {
			List<Agendamento> agendamentos = result.getResultList();
			List<AgendamentoTotemDTO> listagDTO = new ArrayList<AgendamentoTotemDTO>();
			
			listagDTO = formatarAgendamentoTotem(agendamentos);
			
			return listagDTO;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PacienteViewDTO> pacienteByCpfCod(String cpf, String cod) {
		
		Query q = null;
		String query = "";
		
		if(cpf != null && cpf.length() > 0){
			 query = "SELECT * FROM realvida.pacientecpf where nrcpf = :cpf"; 
			 q = findByNativeQuery(query);
			 q.setParameter("cpf", cpf);
		}else{
			 query = "SELECT * FROM realvida.pacientecpf where nrcodpaciente = :cod"
			 		+ " UNION "
			 		+" SELECT * FROM realvida.pacientecpf where "
			 		+ "nrcpf = (SELECT nrcpf FROM realvida.pacientecpf where nrcodpaciente = :cod)"
			 		+ "AND realvida.verify_cpf(nrcpf) = TRUE"; 
			 q = findByNativeQuery(query);
			 q.setParameter("cod", cod);
		}

		List<PacienteViewDTO> result = q
				.unwrap(SQLQuery.class)
				.addScalar("nrUniqueId", StringType.INSTANCE)
				.addScalar("nmPaciente", StringType.INSTANCE)
				.addScalar("nrCPF", StringType.INSTANCE)
				.addScalar("idDedoDigital", IntegerType.INSTANCE)
				.addScalar("digitalPaciente", StringType.INSTANCE)
				.addScalar("hasFotoPaciente", BooleanType.INSTANCE)
				.addScalar("nrCodPaciente", StringType.INSTANCE)
				.addScalar("dtNascimento", DateType.INSTANCE)
				.addScalar("inSexo", IntegerType.INSTANCE)
				.addScalar("nrCelular", StringType.INSTANCE)
				.addScalar("nmEmail", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(PacienteViewDTO.class)).list();

		try {

			if (result != null && result.size() > 0) {
				return result;
			} else{
				return null;
			}

		} catch (NoResultException e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<PacienteViewDTO> pacienteByIdPaciente(String idPaciente, String tipoPaciente) {
		String q = null;
		String query = "";

		if(tipoPaciente.equals("1") ) {
			q = "SELECT cast('1' as text) || cast(cli.idcliente as text) AS nruniqueid,"	 
					+" cli.nmcliente AS nmpaciente,"
					+" cli.nrcpf,"
					+" cli.iddedodigital,"
					+" cli.digitalcliente AS digitalpaciente,"
					+" CASE"
					+" WHEN cli.fotocliente IS NULL THEN false"
					+" ELSE true"
					+" END AS hasfotopaciente,"
					+" regexp_replace(cast(cli.nrcodcliente as text), cast('/' as text), cast('' as text)) AS nrcodpaciente, "
					+ " cli.dtnascimento, cli.insexo, cli.nrcelular, cli.nmemail "
					+" FROM realvida.tbcliente cli"
					+" WHERE cli.idcliente = :idPaciente AND dtexclusao IS NULL";
		} else {
			q = "SELECT cast('2' as text) || cast(dep.iddependente as text) AS nruniqueid, "
					+" dep.nmdependente AS nmpaciente, "
					+" dep.nrcpf,"
					+" dep.iddedodigital,"
					+" dep.digitaldependente AS digitalpaciente,"
					+"  CASE"
					+"  WHEN dep.fotodependente IS NULL THEN false"
					+" ELSE true"
					+" END AS hasfotopaciente,"
					+" regexp_replace(cast(dep.nrcodcliente as text), cast('/' as text), cast('' as text)) AS nrcodpaciente, "
					+ " dep.dtnascimento, dep.insexo, dep.nrcelular, dep.nmemail "
					+" FROM realvida.tbdependente dep"
					+" WHERE dep.iddependente = :idPaciente AND dtexclusao IS NULL";
		}

		query = q 
				+" UNION"
				+" (SELECT * FROM realvida.pacientecpf "
				+" where nrcpf = (SELECT nrcpf FROM realvida.pacientecpf where nruniqueid = :nrUniqueId) "
				+" AND realvida.verify_cpf(nrcpf) = TRUE)";

		List<PacienteViewDTO> result = findByNativeQuery(query)
				.setParameter("idPaciente", Integer.parseInt(idPaciente))
				.setParameter("nrUniqueId", tipoPaciente + idPaciente)
				.unwrap(SQLQuery.class)
				.addScalar("nrUniqueId", StringType.INSTANCE)
				.addScalar("nmPaciente", StringType.INSTANCE)
				.addScalar("nrCPF", StringType.INSTANCE)
				.addScalar("idDedoDigital", IntegerType.INSTANCE)
				.addScalar("digitalPaciente", StringType.INSTANCE)
				.addScalar("hasFotoPaciente", BooleanType.INSTANCE)
				.addScalar("nrCodPaciente", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(PacienteViewDTO.class)).list();

		try {

			if (result != null && result.size() > 0) {
				return result;
			} else {
				return null;
			}

		} catch (NoResultException e) {
			return null;
		}

	}
	
	public List<PacienteViewDTO> getDependentesByIdsTitular(List<Long> idsTitulares) {
		String queryStr = "SELECT cast('2' as text) || cast(dep.iddependente as text) AS nruniqueid, "
				+" dep.nmdependente AS nmpaciente, "
				+" dep.nrcpf,"
				+" dep.iddedodigital,"
				+" dep.digitaldependente AS digitalpaciente,"
				+"  CASE"
				+"  WHEN dep.fotodependente IS NULL THEN false"
				+" ELSE true"
				+" END AS hasfotopaciente,"
				+" regexp_replace(cast(dep.nrcodcliente as text), cast('/' as text), cast('' as text)) AS nrcodpaciente, "
				+ " dep.dtnascimento, dep.insexo, dep.nrcelular, dep.nmemail "
				+" FROM realvida.tbdependente dep"
				+" WHERE dtexclusao IS NULL AND dep.idcliente IN (:listIds)";
		
//		List<PacienteViewDTO> result = findByNativeQuery(queryStr)
//				.setParameter("idPaciente", Integer.parseInt(idPaciente))
//				.setParameter("nrUniqueId", tipoPaciente + idPaciente)
//				.unwrap(SQLQuery.class)
//				.addScalar("nrUniqueId", StringType.INSTANCE)
//				.addScalar("nmPaciente", StringType.INSTANCE)
//				.addScalar("nrCPF", StringType.INSTANCE)
//				.addScalar("idDedoDigital", IntegerType.INSTANCE)
//				.addScalar("digitalPaciente", StringType.INSTANCE)
//				.addScalar("hasFotoPaciente", BooleanType.INSTANCE)
//				.addScalar("nrCodPaciente", StringType.INSTANCE)
//				.setResultTransformer(
//						Transformers.aliasToBean(PacienteViewDTO.class)).list();

		try {
			@SuppressWarnings("unchecked")
			List<PacienteViewDTO> result = findByNativeQuery(queryStr)
					.setParameter("listIds", idsTitulares)
					.unwrap(SQLQuery.class)
					.addScalar("nrUniqueId", StringType.INSTANCE)
					.addScalar("nmPaciente", StringType.INSTANCE)
					.addScalar("nrCPF", StringType.INSTANCE)
					.addScalar("idDedoDigital", IntegerType.INSTANCE)
					.addScalar("digitalPaciente", StringType.INSTANCE)
					.addScalar("hasFotoPaciente", BooleanType.INSTANCE)
					.addScalar("nrCodPaciente", StringType.INSTANCE)
					.setResultTransformer(
							Transformers.aliasToBean(PacienteViewDTO.class))
					.list();

			if(result != null && result.size() > 0) {
				return result;
			} else {
				return null;
			}
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<AgendamentoTotemDTO> agendamentoByPaciente(List<String> ids) {
		
		String listaIdCliente = "";
		String listaIdDependente = "";
		String query = "";
		
		for (String id : ids) {
			if(id.substring(0, 1).equals("1")){
				listaIdCliente += id.substring(1,id.length())+",";
			}else if(id.substring(0, 1).equals("2")){
				listaIdDependente += id.substring(1,id.length())+",";
			}
		}
		
		if(listaIdCliente != null && listaIdCliente.length() > 0 ){
			
			query = " SELECT ag.* "
					+ " FROM realvida.tbagendamento ag WHERE dtAgendamento = current_date  AND ag.idcliente in "
					+ " ("+listaIdCliente.substring(0, listaIdCliente.length() -1)+") "
					+ " AND ag.iddependente IS NULL AND ag.inStatus IN (0,1) AND dtExclusao IS NULL";
		}
		
		if(listaIdDependente != null && listaIdDependente.length() > 0){
			if(listaIdCliente != null && listaIdCliente.length() > 0){
				query += " UNION ALL ";
			}
			
			query += "SELECT ag.* "
					+ " FROM realvida.tbagendamento ag WHERE ag.dtAgendamento = current_date AND ag.iddependente in "
					+ " ("+listaIdDependente.substring(0, listaIdDependente.length() -1)+") "
					+ " AND ag.inStatus IN (0,1) AND dtExclusao IS NULL";
		}
		
		

		try {
			
			List<Agendamento> agendamentos = (List<Agendamento>) findByNativeQuery(query, Agendamento.class);
			if(agendamentos != null && agendamentos.size() > 0 ){
				
				List<AgendamentoTotemDTO> agendamentoFormatado = formatarAgendamentoTotem(agendamentos);
				
				return agendamentoFormatado;
			}
			
			return null;
			
		} catch (NoResultException e) {
			return null;
		}

	}
	
	public AgendamentoTotemDTO getAgendamentosPorIdAgendamento (Long idAgendamento){
		
		TypedQuery<Agendamento> result = null;
			
			String query = "SELECT ag "
					+ " FROM Agendamento ag WHERE ag.dtAgendamento = current_date AND ag.id = :idAgendamento AND dtExclusao IS NULL";
			
			
			 result = entityManager
			        .createQuery(query, Agendamento.class).setParameter("idAgendamento", idAgendamento);
		try {
				Agendamento ag = result.getSingleResult();
			
				AgendamentoTotemDTO agDTO = new AgendamentoTotemDTO();
				
				List<Agendamento> listAgendamentos = new ArrayList<Agendamento>();
				
				listAgendamentos.add(ag);
				
				agDTO = formatarAgendamentoTotem(listAgendamentos).get(0);
			
			return agDTO;
			
		} catch (NoResultException e) {
			return null;
		}
		
		
		
	}
	
	public Consultorio getConsultorioPorEspecialidadeProfissional(Long idProfissional, Long idEspecialidade, Long idUnidade, String hrAgendamento){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( new Date());
		
		Integer diaSemana = new Integer(calendar.get(Calendar.DAY_OF_WEEK) - 1);
		
		String query = "SELECT atend "
				+" FROM AtendimentoProfissional atend WHERE atend.profissional.id = "+idProfissional
				+" AND atend.espProfissional.especialidade.id = "+idEspecialidade+" AND atend.unidade.id ="+idUnidade
				+" AND atend.inDiaSemana = "+diaSemana+" AND '"+hrAgendamento+"' BETWEEN atend.hrInicio AND atend.hrFim";
		
		TypedQuery<AtendimentoProfissional> result = entityManager
		        .createQuery(query, AtendimentoProfissional.class);
		
		try {
			AtendimentoProfissional listresult = result.getSingleResult();
			if(listresult != null && listresult.getConsultorio() != null)
				return listresult.getConsultorio();
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<DigitalClienteDTO> getDigitalPorCpf(String cpf) {

		String query = "SELECT cast('1'as text) || cast(tbcliente.idcliente as text) AS id, "
				+ " tbcliente.digitalcliente AS digitalCliente "
				+ " FROM realvida.tbcliente "
				+ " WHERE tbcliente.digitalcliente IS NOT NULL AND nrcpf = :cpf "
				+ " UNION ALL "
				+ " SELECT cast('2' as text) || cast(tbdependente.iddependente as text) AS id, "
				+ " tbdependente.digitaldependente AS digitalCliente "
				+ " FROM realvida.tbdependente "
				+ " WHERE tbdependente.digitaldependente IS NOT NULL AND nrcpf = :cpf "
				+ " UNION ALL "
				+ " SELECT cast('3' as text) || cast(tbfuncionario.idfuncionario as text) AS id, "
				+ " tbfuncionario.digitalfuncionario AS digitalCliente "
				+ " FROM realvida.tbfuncionario "
				+ " WHERE tbfuncionario.digitalfuncionario IS NOT NULL AND nrcpf = :cpf "; 

		List<DigitalClienteDTO> result = findByNativeQuery(query)
				.setParameter("cpf", cpf)
				.unwrap(SQLQuery.class)
				.addScalar("id", StringType.INSTANCE)
				.addScalar("digitalCliente", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(DigitalClienteDTO.class))
				.list();

		try {

			if (result != null && result.size() > 0) {
				return result;
			} else{
				return null;
			}
				

		} catch (NoResultException e) {
			return null;
		}

	}
	
	
	public List<DigitalClienteDTO> getDigitalPorCpf(String cpf, boolean isFuncionario) {
		String query = null;
		if(isFuncionario) {
			query = "SELECT cast('3' as text) || cast(tbfuncionario.idfuncionario as text) AS id, "
					+ " tbfuncionario.digitalfuncionario AS digitalCliente "
					+ " FROM realvida.tbfuncionario "
					+ " WHERE tbfuncionario.digitalfuncionario IS NOT NULL AND nrcpf = :cpf"; 
		} else {
			query = "SELECT cast('1'as text) || cast(tbcliente.idcliente as text) AS id, "
					+ " tbcliente.digitalcliente AS digitalCliente "
					+ " FROM realvida.tbcliente "
					+ " WHERE tbcliente.digitalcliente IS NOT NULL AND nrcpf = :cpf "
					+ " UNION ALL "
					+ " SELECT cast('2' as text) || cast(tbdependente.iddependente as text) AS id, "
					+ " tbdependente.digitaldependente AS digitalCliente "
					+ " FROM realvida.tbdependente "
					+ " WHERE tbdependente.digitaldependente IS NOT NULL AND nrcpf = :cpf";
		}

		@SuppressWarnings("unchecked")
		List<DigitalClienteDTO> result = findByNativeQuery(query)
		.setParameter("cpf", cpf)
		.unwrap(SQLQuery.class)
		.addScalar("id", StringType.INSTANCE)
		.addScalar("digitalCliente", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(DigitalClienteDTO.class))
		.list();
		try {
			if (result != null && result.size() > 0) {
				return result;
			} else{
				return null;
			}
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<DigitalClienteDTO> getDigitalAll(TipoPessoa tipoPessoa) {
		String query = null;
		String fromStr = null;
		if(tipoPessoa.equals(TipoPessoa.CLIENTE) || tipoPessoa.equals(TipoPessoa.DEPENDENTE) ) {
			fromStr = "FROM realvida.clientesdigital_only";
		} else {
			fromStr = "FROM realvida.funcionariosdigital_only";
		}
		if(tipoPessoa.equals(TipoPessoa.FUNCIONARIOAUTORIZADO)){
			fromStr += " WHERE boautorizacaototem is true";
		}
			
		query = "SELECT nruniqueid as id, digitalpaciente as digitalcliente " + fromStr;
		@SuppressWarnings("unchecked")
		List<DigitalClienteDTO> result = findByNativeQuery(query)
		.unwrap(SQLQuery.class)
		.addScalar("id", StringType.INSTANCE)
		.addScalar("digitalCliente", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(DigitalClienteDTO.class))
		.list();
		try {
			if (result != null && result.size() > 0) {
				return result;
			} else{
				return null;
			}
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<FuncionarioDTO> getFuncionarioByCelular(String nrCelular) {


			
		String query = "select idfuncionario, nmfuncionario, nrcelular, imeicelular from realvida.tbfuncionario" 
						+" where nrcelular is not null"
						+" and nrcelular != ''"
						+" and length(nrcelular) > 7"
						+" and regexp_replace(nrcelular, cast('[^0-9]' as TEXT), cast('' as TEXT), 'g') like '%"+nrCelular+"%'"
						+" and dtexclusao is null";
		@SuppressWarnings("unchecked")
		List<FuncionarioDTO> result = findByNativeQuery(query)
		.unwrap(SQLQuery.class)
		.addScalar("idFuncionario", LongType.INSTANCE)
		.addScalar("nmFuncionario", StringType.INSTANCE)
		.addScalar("nrCelular", StringType.INSTANCE)
		.addScalar("imeiCelular", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(FuncionarioDTO.class))
		.list();
		try {
			if (result != null && result.size() > 0) {
				return result;
			} else{
				return null;
			}
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Unidade getUnidadeByLatitudeAndLongitude(Double latitude, Double longitude) {
		
		String query = " select uni.idunidade FROM REALVIDA.TBUNIDADE uni WHERE  (SELECT point '("+latitude+", "+longitude+")' <@ localizacaounidade)";
		
//		Query result = entityManager.createNativeQuery(query);
//		result.setMaxResults(1);
//		TypedQuery<Unidade> result =  entityManager.createQuery(query, Unidade.class);
		try {
			Long idUnidade =  (Long) findByNativeQuery(query).unwrap(SQLQuery.class)
			        .addScalar("idunidade", LongType.INSTANCE).uniqueResult();
			
			if(idUnidade != null ){
				Unidade unidade = unidadeDAO.searchByKey(Unidade.class, idUnidade);
				return unidade;
			}
				
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return null;
	}
	
}
