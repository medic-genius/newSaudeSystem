package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tborgao")
public class Orgao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORGAO_ID_SEQ")
	@SequenceGenerator(name = "ORGAO_ID_SEQ", sequenceName = "realvida.orgao_id_seq", allocationSize=1)
	@Column(name = "idorgao")
	private Long id;
	
	@Basic
	@Column(name = "cdorgao")
	private String cdOrgao;
	
	@Basic
	@Column(name = "cdorgaosiape")
	private String cdOrgaoSiape;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idorgaoaux")
	private String idOrgaoAux;
	
	@Basic
	@Column(name = "idorgaoodonto")
	private String idOrgaoOdonto;
	
	@Basic
	@Column(name = "inesfera")
	private Integer inEsfera;
	
	@Basic
	@Column(name = "nmorgao")
	private String nmOrgao;

	@ManyToOne
	@JoinColumn(name="idgrupoorgao")
	private GrupoOrgao grupoOrgao;

	public Orgao() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCdOrgao() {
		return cdOrgao;
	}

	public void setCdOrgao(String cdOrgao) {
		this.cdOrgao = cdOrgao;
	}

	public String getCdOrgaoSiape() {
		return cdOrgaoSiape;
	}

	public void setCdOrgaoSiape(String cdOrgaoSiape) {
		this.cdOrgaoSiape = cdOrgaoSiape;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getIdOrgaoAux() {
		return idOrgaoAux;
	}

	public void setIdOrgaoAux(String idOrgaoAux) {
		this.idOrgaoAux = idOrgaoAux;
	}

	public String getIdOrgaoOdonto() {
		return idOrgaoOdonto;
	}

	public void setIdOrgaoOdonto(String idOrgaoOdonto) {
		this.idOrgaoOdonto = idOrgaoOdonto;
	}

	public Integer getInEsfera() {
		return inEsfera;
	}

	public void setInEsfera(Integer inEsfera) {
		this.inEsfera = inEsfera;
	}

	public String getNmOrgao() {
		return nmOrgao;
	}

	public void setNmOrgao(String nmOrgao) {
		this.nmOrgao = nmOrgao;
	}

	public GrupoOrgao getGrupoOrgao() {
		return grupoOrgao;
	}

	public void setGrupoOrgao(GrupoOrgao grupoOrgao) {
		this.grupoOrgao = grupoOrgao;
	}

}