package br.com.medic.medicsystem.persistence.model.views;



import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "realvida", name = "atendimentomedico_historico")
public class AtendimentoMedico {
	
	@Id
	@Column(name = "idatendimento")
	private Long idAtendimento;
	
	@Basic
	@Column(name = "idagendamento")
	private Long idAgendamento;
	
	
	@Basic
	@Column(name = "intipopaciente")
	private Character inTipoPaciente;
	
	@Basic
	@Column(name = "nmmedico")
	private String nmMedico;
	
	@Basic
	@Column(name = "dtagendamento")
	private Date dtAtendimento;
	
	
	@Basic
	@Column(name = "nmqueixapaciente")
	private String nmQueixaPaciente;
	
	@Basic
	@Column(name = "nmdiagnostico")
	private String nmDiagnostico;
	
	@Basic
	@Column(name = "nmcondutamedica")
	private String nmCondutaMedica;
	
	@Basic
	@Column(name = "bosolicitouretorno")
	private Boolean boSolicitouRetorno;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "nmservico")
	private String nmServico;
	
	
	@Basic
	@Column(name = "idpaciente")
	private Long idPaciente;
	
	@Basic
	@Column(name = "nmreceituario")
	private String nmReceituario;
	
	@Basic
	@Column(name = "idespecialidade")
	private String idEspecialidade;

	public Long getIdAgendamento() {
		return idAgendamento;
	}


	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}


	public Date getDtAtendimento() {
		return dtAtendimento;
	}


	public void setDtAtendimento(Date dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}


	public String getNmQueixaPaciente() {
		return nmQueixaPaciente;
	}


	public void setNmQueixaPaciente(String nmQueixaPaciente) {
		this.nmQueixaPaciente = nmQueixaPaciente;
	}


	public String getNmDiagnostico() {
		return nmDiagnostico;
	}


	public void setNmDiagnostico(String nmDiagnostico) {
		this.nmDiagnostico = nmDiagnostico;
	}


	public String getNmCondutaMedica() {
		return nmCondutaMedica;
	}


	public void setNmCondutaMedica(String nmCondutaMedica) {
		this.nmCondutaMedica = nmCondutaMedica;
	}


	public Boolean getBoSolicitouRetorno() {
		return boSolicitouRetorno;
	}


	public void setBoSolicitouRetorno(Boolean boSolicitouRetorno) {
		this.boSolicitouRetorno = boSolicitouRetorno;
	}


	public String getNmEspecialidade() {
		return nmEspecialidade;
	}


	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}


	public String getNmServico() {
		return nmServico;
	}


	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}


	public Long getIdPaciente() {
		return idPaciente;
	}


	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}


	public Character getInTipoPaciente() {
		return inTipoPaciente;
	}


	public void setInTipoPaciente(Character inTipoPaciente) {
		this.inTipoPaciente = inTipoPaciente;
	}


	public String getNmMedico() {
		return nmMedico;
	}


	public void setNmMedico(String nmMedico) {
		this.nmMedico = nmMedico;
	}


	public String getNmReceituario() {
		return nmReceituario;
	}


	public void setNmReceituario(String nmReceituario) {
		this.nmReceituario = nmReceituario;
	}


	public Long getIdAtendimento() {
		return idAtendimento;
	}


	public void setIdAtendimento(Long idAtendimento) {
		this.idAtendimento = idAtendimento;
	}
	
	
	
	

}
