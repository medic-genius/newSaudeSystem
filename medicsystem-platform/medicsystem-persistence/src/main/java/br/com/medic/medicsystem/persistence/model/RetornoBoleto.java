package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the tbretornoboleto database table.
 * 
 */
@Entity
public class RetornoBoleto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBRETORNOBOLETO_IDRETORNOBOLETO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBRETORNOBOLETO_IDRETORNOBOLETO_GENERATOR")
	private Long idretornoboleto;

	private String cdbaixadosistema;

	private String cdbancorecebedor;

	private String cdcedente;

	private Timestamp dtatualizacaolog;

	@Temporal(TemporalType.DATE)
	private Date dtcredito;

	@Temporal(TemporalType.DATE)
	private Date dtdebito;

	@Temporal(TemporalType.DATE)
	private Date dtexportacao;

	private Timestamp dtinclusaolog;

	@Temporal(TemporalType.DATE)
	private Date dtpagamento;

	@Temporal(TemporalType.DATE)
	private Date dtvencimento;

	private Integer incarteira;

	private Integer inmovimentacao;

	private Integer intipocanal;

	private Integer intipopagamento;

	private String nmcanal;

	private String nmmovimentacao;

	private String nragencia;

	private String nrnossonumero;

	private String nrnumeroboleto;

	private String nrremessa;

	private double vlabatimento;

	private double vldesconto;

	private double vljurosmoraencargo;

	private double vlliquidocredito;

	private double vlnominal;

	private double vloutrasdespesas;

	private double vloutroscreditos;

	private double vlpagosacado;

	private double vltarifas;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public RetornoBoleto() {
	}

	public Long getIdretornoboleto() {
		return this.idretornoboleto;
	}

	public void setIdretornoboleto(Long idretornoboleto) {
		this.idretornoboleto = idretornoboleto;
	}

	public String getCdbaixadosistema() {
		return this.cdbaixadosistema;
	}

	public void setCdbaixadosistema(String cdbaixadosistema) {
		this.cdbaixadosistema = cdbaixadosistema;
	}

	public String getCdbancorecebedor() {
		return this.cdbancorecebedor;
	}

	public void setCdbancorecebedor(String cdbancorecebedor) {
		this.cdbancorecebedor = cdbancorecebedor;
	}

	public String getCdcedente() {
		return this.cdcedente;
	}

	public void setCdcedente(String cdcedente) {
		this.cdcedente = cdcedente;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtcredito() {
		return this.dtcredito;
	}

	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}

	public Date getDtdebito() {
		return this.dtdebito;
	}

	public void setDtdebito(Date dtdebito) {
		this.dtdebito = dtdebito;
	}

	public Date getDtexportacao() {
		return this.dtexportacao;
	}

	public void setDtexportacao(Date dtexportacao) {
		this.dtexportacao = dtexportacao;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Date getDtpagamento() {
		return this.dtpagamento;
	}

	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}

	public Date getDtvencimento() {
		return this.dtvencimento;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public Integer getIncarteira() {
		return this.incarteira;
	}

	public void setIncarteira(Integer incarteira) {
		this.incarteira = incarteira;
	}

	public Integer getInmovimentacao() {
		return this.inmovimentacao;
	}

	public void setInmovimentacao(Integer inmovimentacao) {
		this.inmovimentacao = inmovimentacao;
	}

	public Integer getIntipocanal() {
		return this.intipocanal;
	}

	public void setIntipocanal(Integer intipocanal) {
		this.intipocanal = intipocanal;
	}

	public Integer getIntipopagamento() {
		return this.intipopagamento;
	}

	public void setIntipopagamento(Integer intipopagamento) {
		this.intipopagamento = intipopagamento;
	}

	public String getNmcanal() {
		return this.nmcanal;
	}

	public void setNmcanal(String nmcanal) {
		this.nmcanal = nmcanal;
	}

	public String getNmmovimentacao() {
		return this.nmmovimentacao;
	}

	public void setNmmovimentacao(String nmmovimentacao) {
		this.nmmovimentacao = nmmovimentacao;
	}

	public String getNragencia() {
		return this.nragencia;
	}

	public void setNragencia(String nragencia) {
		this.nragencia = nragencia;
	}

	public String getNrnossonumero() {
		return this.nrnossonumero;
	}

	public void setNrnossonumero(String nrnossonumero) {
		this.nrnossonumero = nrnossonumero;
	}

	public String getNrnumeroboleto() {
		return this.nrnumeroboleto;
	}

	public void setNrnumeroboleto(String nrnumeroboleto) {
		this.nrnumeroboleto = nrnumeroboleto;
	}

	public String getNrremessa() {
		return this.nrremessa;
	}

	public void setNrremessa(String nrremessa) {
		this.nrremessa = nrremessa;
	}

	public double getVlabatimento() {
		return this.vlabatimento;
	}

	public void setVlabatimento(double vlabatimento) {
		this.vlabatimento = vlabatimento;
	}

	public double getVldesconto() {
		return this.vldesconto;
	}

	public void setVldesconto(double vldesconto) {
		this.vldesconto = vldesconto;
	}

	public double getVljurosmoraencargo() {
		return this.vljurosmoraencargo;
	}

	public void setVljurosmoraencargo(double vljurosmoraencargo) {
		this.vljurosmoraencargo = vljurosmoraencargo;
	}

	public double getVlliquidocredito() {
		return this.vlliquidocredito;
	}

	public void setVlliquidocredito(double vlliquidocredito) {
		this.vlliquidocredito = vlliquidocredito;
	}

	public double getVlnominal() {
		return this.vlnominal;
	}

	public void setVlnominal(double vlnominal) {
		this.vlnominal = vlnominal;
	}

	public double getVloutrasdespesas() {
		return this.vloutrasdespesas;
	}

	public void setVloutrasdespesas(double vloutrasdespesas) {
		this.vloutrasdespesas = vloutrasdespesas;
	}

	public double getVloutroscreditos() {
		return this.vloutroscreditos;
	}

	public void setVloutroscreditos(double vloutroscreditos) {
		this.vloutroscreditos = vloutroscreditos;
	}

	public double getVlpagosacado() {
		return this.vlpagosacado;
	}

	public void setVlpagosacado(double vlpagosacado) {
		this.vlpagosacado = vlpagosacado;
	}

	public double getVltarifas() {
		return this.vltarifas;
	}

	public void setVltarifas(double vltarifas) {
		this.vltarifas = vltarifas;
	}

	public Funcionario getTbfuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario tbfuncionario) {
		this.funcionario = tbfuncionario;
	}

}