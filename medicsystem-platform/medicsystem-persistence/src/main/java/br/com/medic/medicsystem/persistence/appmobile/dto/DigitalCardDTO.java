package br.com.medic.medicsystem.persistence.appmobile.dto;

public class DigitalCardDTO {
	
	private Long idCliente;
	private String nmCliente;
	private String nrCodCliente;
	private String nrCPF;
	private String nrRG;
	private String dtNascimento;
	private Boolean isAssociado;
	private byte[] QRCode;
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getNmCliente() {
		return nmCliente;
	}
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	public String getNrCodCliente() {
		return nrCodCliente;
	}
	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}
	public String getNrCPF() {
		return nrCPF;
	}
	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}
	public String getNrRG() {
		return nrRG;
	}
	public void setNrRG(String nrRG) {
		this.nrRG = nrRG;
	}
	public String getDtNascimento() {
		return dtNascimento;
	}
	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public Boolean getIsAssociado() {
		return isAssociado;
	}
	public void setIsAssociado(Boolean isAssociado) {
		this.isAssociado = isAssociado;
	}
	public byte[] getQRCode() {
		return QRCode;
	}
	public void setQRCode(byte[] qRCode) {
		QRCode = qRCode;
	}
}
