package br.com.medic.medicsystem.persistence.dto;

public class UnidadeDTO {
	
	private Long idUnidade;
	
	private String nmApelido;

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getNmApelido() {
		return nmApelido;
	}

	public void setNmApelido(String nmApelido) {
		this.nmApelido = nmApelido;
	}

}
