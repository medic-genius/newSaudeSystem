package br.com.medic.medicsystem.persistence.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(catalog = "realvida", name = "tbinformativo")
public class Informativo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INFORMATIVO_ID_SEQ")
	@SequenceGenerator(name = "INFORMATIVO_ID_SEQ", catalog = "realvida", sequenceName = "informativo_id_seq", allocationSize = 1)
	@Column(name = "idinformativo")
	private Long idInformativo;
	
	@Basic
	@Column(name = "nminformativosms")
	private String nmInformativo;
	
	@Basic
	@Column(name = "intipo")
	private Integer inTipo;
	
	@Basic
	@Column(name = "idexterno")
	private Long idExterno;
	
	@Basic
	@Column(name = "intipoexterno")
	private Long inTipoExterno;
	
	@Basic
	@Column(name = "nmreferencia")
	private String nmReferencia;
	
	@JsonIgnoreProperties
	@Transient
	String nmDescricao;
	
	public String getNmReferencia() {
		return nmReferencia;
	}

	public void setNmNome(String nmReferencia) {
		this.nmReferencia = nmReferencia;
	}

	public String getNmDescricao() {
		return nmDescricao;
	}

	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}

	
	public Long getIdInformativo() {
		return idInformativo;
	}

	public void setIdInformativo(Long idInformativo) {
		this.idInformativo = idInformativo;
	}

	public String getNmInformativo() {
		return nmInformativo;
	}

	public void setNmInformativo(String nmInformativo) {
		this.nmInformativo = nmInformativo;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public Long getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(Long idExterno) {
		this.idExterno = idExterno;
	}

	public Long getInTipoExterno() {
		return inTipoExterno;
	}

	public void setInTipoExterno(Long inTipoExterno) {
		this.inTipoExterno = inTipoExterno;
	}
	
	
	
}
	