package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.OrcamentoExame;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoDespesaOrcamentoExameView;



@Named("orcamentoexame-dao")
@ApplicationScoped
public class OrcamentoExameDAO extends RelationalDataAccessObject<OrcamentoExame>{
	
	public OrcamentoExame getOrcamentoPorAtendimento(Long idAtendimento){
		
	String query = "SELECT orc FROM OrcamentoExame orc WHERE orc.atendimento.id = "+ idAtendimento;
		
		TypedQuery<OrcamentoExame> queryStr = entityManager.createQuery(query,
				OrcamentoExame.class);

		queryStr.setFirstResult(0);
		queryStr.setMaxResults(1);
		try {
			return queryStr.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
	
	
	public List<AtendimentomedicoDespesaOrcamentoExameView> getOrcamentoPorIdPaciente(Long idPaciente){
		
		String query = "SELECT aten FROM AtendimentomedicoDespesaOrcamentoExameView aten WHERE aten.idPaciente = "+ idPaciente;
			
			TypedQuery<AtendimentomedicoDespesaOrcamentoExameView> queryStr = entityManager.createQuery(query,
					AtendimentomedicoDespesaOrcamentoExameView.class);

			try {
				return queryStr.getResultList();
			} catch (NoResultException e) {
				return null;
			}
		}
	
	public AtendimentomedicoDespesaOrcamentoExameView getOrcamentoPorIdOrcamento(Long idOrcamento){
		
		String query = "SELECT aten FROM AtendimentomedicoDespesaOrcamentoExameView aten WHERE aten.idOrcamentoExame = "+ idOrcamento;
			
			TypedQuery<AtendimentomedicoDespesaOrcamentoExameView> queryStr = entityManager.createQuery(query,
					AtendimentomedicoDespesaOrcamentoExameView.class);
			
			queryStr.setFirstResult(0);
			queryStr.setMaxResults(1);
			
			try {
				return queryStr.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}

}
