package br.com.medic.medicsystem.persistence.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="idfuncionario")
@Table(catalog = "realvida", name = "tbprofissional")
public class Profissional extends Funcionario {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "intipoprofissional")
	private Integer inTipoProfissional;

	@Basic
	@Column(name = "nrcrmcro")
	private String nrCrmCro;
	
	
//	@Basic
//	@Column(name = "dtalteracao")
//	private Date dtAlteracao;
//	
//	@Basic
//	@Column(name = "dtcadastro")
//	private Date dtCadastro;
//
//	@Basic
//	@Column(name = "dtatualizacaolog")
//	private Timestamp dtAtualizacaoLog;
//
//	@Basic
//	@Column(name = "dtinclusaolog")
//	private Timestamp dtInclusaoLog;
//
//	@Basic
//	@Column(name = "idoperadoralteracao")
//	private Long idOperadorAlteracao;
//
//	@Basic
//	@Column(name = "idoperadorcadastro")
//	private Long idOperadorCadastro;
//
//	@Basic
//	@Column(name = "idoperadorexclusao")
//	private Long idOperadorExclusao;

	
	public Profissional() {
	}
	
//	@Override
//	public Long getId() {
//		// TODO Auto-generated method stub
//		return super.getId();
//	}
	
	public Integer getInTipoProfissional() {
		return inTipoProfissional;
	}

	public void setInTipoProfissional(Integer inTipoProfissional) {
		this.inTipoProfissional = inTipoProfissional;
	}

	public String getNrCrmCro() {
		return nrCrmCro;
	}

	public void setNrCrmCro(String nrCrmCro) {
		this.nrCrmCro = nrCrmCro;
	}

}