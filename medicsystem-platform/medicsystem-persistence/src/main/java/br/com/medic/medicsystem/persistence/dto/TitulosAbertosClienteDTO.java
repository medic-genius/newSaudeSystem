package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.medic.medicsystem.persistence.model.CobrancaFinanceiroView;

public class TitulosAbertosClienteDTO implements Serializable{
	
	private static final long serialVersionUID = -3193636701239834279L;
	private String tipo;
	private String nrContrato;
	private String formaPagamento;
	private Double valor;
	private Double valorComJuros;
	private String nrTitulo;
	private Long idTitulo;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dtVencimento;

	public TitulosAbertosClienteDTO() {

	}
	
	public TitulosAbertosClienteDTO(CobrancaFinanceiroView view) {

		this.tipo = view.getInTipo();
		this.nrContrato = view.getNrContrato();
		this.formaPagamento = view.getNmFormaPagamentoTitulo();
		this.valor = view.getVlTitulo();
		this.nrTitulo = view.getNrTitulo();
		this.idTitulo = view.getIdTitulo();
		
		Double totalDivida = view.getVlTitulo() != null ? view.getVlTitulo() : 0.0;
		Double totalJuros = view.getValorJuros() != null ? view.getValorJuros() : 0.0; 
		Double totalMultas = view.getValorMultaAtraso() != null ? view.getValorMultaAtraso() : 0.0;
	
		this.valorComJuros = totalDivida + totalJuros + totalMultas;
		this.dtVencimento = view.getDataVencimento();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getValorComJuros() {
		return valorComJuros;
	}

	public void setValorComJuros(Double valorComJuros) {
		this.valorComJuros = valorComJuros;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public Long getIdTitulo() {
		return idTitulo;
	}

	public void setIdTitulo(Long idTitulo) {
		this.idTitulo = idTitulo;
	}
}
