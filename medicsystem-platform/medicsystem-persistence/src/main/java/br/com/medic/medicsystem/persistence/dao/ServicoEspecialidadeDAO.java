package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ServicoEspecialidade;

@Named("servicoespecialidade-dao")
@ApplicationScoped
public class ServicoEspecialidadeDAO extends RelationalDataAccessObject<ServicoEspecialidade> {

	public ServicoEspecialidade getServicoEspecialidade(Long idServico, Long idEspecialidade){
		
		String queryStr = "SELECT se FROM ServicoEspecialidade se "				
				+ " WHERE se.servico.id = " + idServico
				+ " AND se.especialidade.id = " + idEspecialidade;				
		
		ServicoEspecialidade result;
		
		try {
			TypedQuery<ServicoEspecialidade> query = createQuery(queryStr, ServicoEspecialidade.class);
			query.setMaxResults(1);
			result = query.getSingleResult();
			return result;
		} catch(Exception e) {
			e.printStackTrace();			
		}
		
		return null;
	}

}
