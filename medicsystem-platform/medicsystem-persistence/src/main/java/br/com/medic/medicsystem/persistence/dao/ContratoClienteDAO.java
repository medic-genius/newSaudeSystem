package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;

@Named("contratocliente-dao")
@ApplicationScoped
public class ContratoClienteDAO extends RelationalDataAccessObject<ContratoCliente>{
	
	public ContratoCliente getContratoCliente(Long idContratoCliente) {
		return searchByKey(ContratoCliente.class, idContratoCliente);
	}
	
	public ContratoCliente getPrivateContratoByCliente(Long idCliente) {
			try {
			TypedQuery<ContratoCliente> query = entityManager.createQuery("SELECT cc "
					+ " FROM ContratoCliente cc "
					+ " where cc.contrato.plano is null and cc.cliente.id = " + idCliente
					, ContratoCliente.class);
			query.setMaxResults(1);
			ContratoCliente result = query.getSingleResult();
			return result;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}	

	public ContratoCliente getContratoClienteByContrato(Long idContrato) {
		try {
		
			TypedQuery<ContratoCliente> query = entityManager.createQuery(
					"SELECT cc "
					+ " FROM ContratoCliente cc "
					+ " where cc.contrato.id = " + idContrato, ContratoCliente.class);
			query.setMaxResults(1);
			ContratoCliente result = query.getSingleResult();
			return result;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	public ContratoDependente getByContratoAndDependente(Long idContrato, Long idDependente) {
		try {
			TypedQuery<ContratoDependente> query = entityManager.createQuery(
					"SELECT cd "
					+ " FROM ContratoDependente cd "
					+ " where cd.contrato.id = " + idContrato 
					+ " and cd.dependente.id = " + idDependente, ContratoDependente.class);
			
			query.setMaxResults(1);
			ContratoDependente contratoDependente = query.getSingleResult();
			return contratoDependente;
			
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}	
}
