package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tbsetor database table.
 * 
 */
@Entity
public class Setor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBSETOR_IDSETOR_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBSETOR_IDSETOR_GENERATOR")
	private Long idsetor;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private String nmsetor;

	//bi-directional many-to-one association to Tbmovimentacaoproduto
	@OneToMany(mappedBy="tbsetor1")
	private List<MovimentacaoProduto> tbmovimentacaoprodutos1;

	//bi-directional many-to-one association to Tbmovimentacaoproduto
	@OneToMany(mappedBy="tbsetor2")
	private List<MovimentacaoProduto> tbmovimentacaoprodutos2;

	//bi-directional many-to-one association to Tbdepartamento
	@ManyToOne
	@JoinColumn(name="iddepartamento")
	private Departamento departamento;

	public Setor() {
	}

	public Long getIdsetor() {
		return this.idsetor;
	}

	public void setIdsetor(Long idsetor) {
		this.idsetor = idsetor;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public String getNmsetor() {
		return this.nmsetor;
	}

	public void setNmsetor(String nmsetor) {
		this.nmsetor = nmsetor;
	}

	public List<MovimentacaoProduto> getTbmovimentacaoprodutos1() {
		return this.tbmovimentacaoprodutos1;
	}

	public void setTbmovimentacaoprodutos1(List<MovimentacaoProduto> tbmovimentacaoprodutos1) {
		this.tbmovimentacaoprodutos1 = tbmovimentacaoprodutos1;
	}

	public MovimentacaoProduto addTbmovimentacaoprodutos1(MovimentacaoProduto tbmovimentacaoprodutos1) {
		getTbmovimentacaoprodutos1().add(tbmovimentacaoprodutos1);
		tbmovimentacaoprodutos1.setTbsetor1(this);

		return tbmovimentacaoprodutos1;
	}

	public MovimentacaoProduto removeTbmovimentacaoprodutos1(MovimentacaoProduto tbmovimentacaoprodutos1) {
		getTbmovimentacaoprodutos1().remove(tbmovimentacaoprodutos1);
		tbmovimentacaoprodutos1.setTbsetor1(null);

		return tbmovimentacaoprodutos1;
	}

	public List<MovimentacaoProduto> getTbmovimentacaoprodutos2() {
		return this.tbmovimentacaoprodutos2;
	}

	public void setTbmovimentacaoprodutos2(List<MovimentacaoProduto> tbmovimentacaoprodutos2) {
		this.tbmovimentacaoprodutos2 = tbmovimentacaoprodutos2;
	}

	public MovimentacaoProduto addTbmovimentacaoprodutos2(MovimentacaoProduto tbmovimentacaoprodutos2) {
		getTbmovimentacaoprodutos2().add(tbmovimentacaoprodutos2);
		tbmovimentacaoprodutos2.setTbsetor2(this);

		return tbmovimentacaoprodutos2;
	}

	public MovimentacaoProduto removeTbmovimentacaoprodutos2(MovimentacaoProduto tbmovimentacaoprodutos2) {
		getTbmovimentacaoprodutos2().remove(tbmovimentacaoprodutos2);
		tbmovimentacaoprodutos2.setTbsetor2(null);

		return tbmovimentacaoprodutos2;
	}

	public Departamento getTbdepartamento() {
		return this.departamento;
	}

	public void setTbdepartamento(Departamento tbdepartamento) {
		this.departamento = tbdepartamento;
	}

}