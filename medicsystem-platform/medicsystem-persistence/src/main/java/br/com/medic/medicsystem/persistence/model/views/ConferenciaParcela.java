package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbconferenciaparcela_view")
public class ConferenciaParcela {
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Basic
	@Column(name = "idparcela")
	private String idParcela;
	
	@Basic
	@Column(name = "idcaixa")
	private Long idCaixa;
	
	@Basic
	@Column(name = "nrparcela")
	private String nrParcela;
	
	@Basic
	@Column(name = "nrcodcliente")
	private String nrCodCliente;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;
	
	@Basic
	@Column(name = "nmdependente")
	private String nmDependente;
	
	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name = "nmplano")
	private String nmPlano;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtvencimento")
	private String dtVencimento;
	
	@Basic
	@Column(name = "vlpago")
	private Float vlPago;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtmovimentacaocaixa")
	private Date dtMovimentacaoCaixa;
	
	@Basic
	@Column(name = "nmformapagamento")
	private String nmFormaPagamento;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdParcela() {
		return idParcela;
	}

	public void setIdParcela(String idParcela) {
		this.idParcela = idParcela;
	}

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	}

	public String getNrParcela() {
		return nrParcela;
	}

	public void setNrParcela(String nrParcela) {
		this.nrParcela = nrParcela;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}
	
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Float getVlPago() {
		return vlPago;
	}

	public void setVlPago(Float vlPago) {
		this.vlPago = vlPago;
	}

	public Date getDtMovimentacaoCaixa() {
		return dtMovimentacaoCaixa;
	}

	public void setDtMovimentacaoCaixa(Date dtMovimentacaoCaixa) {
		this.dtMovimentacaoCaixa = dtMovimentacaoCaixa;
	}

	public String getNmFormaPagamento() {
		return nmFormaPagamento;
	}

	public void setNmFormaPagamento(String nmFormaPagamento) {
		this.nmFormaPagamento = nmFormaPagamento;
	}

}
