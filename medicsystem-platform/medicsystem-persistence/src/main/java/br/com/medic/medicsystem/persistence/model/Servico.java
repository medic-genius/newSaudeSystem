package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbservico")
public class Servico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICO_ID_SEQ")
	@SequenceGenerator(name = "SERVICO_ID_SEQ", sequenceName = "realvida.servico_id_seq", allocationSize = 1)
	@Column(name = "idservico")
	private Long id;

	@Basic
	@Column(name = "bobloqueio")
	private Boolean boBloqueio;

	@Basic
	@Column(name = "bogratuito")
	private Boolean boGratuito;

	@Basic
	@Column(name = "bovaloraberto")
	private Boolean boValorAberto;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtcadastro")
	private Date dtCadastro;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "idservicoaux")
	private Long idServicoAux;

	@Basic
	@Column(name = "idservicoodonto")
	private Long idServicoOdonto;

	@Basic
	@Column(name = "intiposervico")
	private Integer inTipoServico;

	@Basic
	@Column(name = "nmservico")
	private String nmServico;

	@Basic
	@Column(name = "nrcodservico")
	private String nrCodServico;

	@Basic
	@Column(name = "validade")
	private Integer validade;

	@Basic
	@Column(name = "vlcusto")
	private Double vlCusto;

	@Basic
	@Column(name = "vlmargemassociado")
	private Double vlMargemAssociado;

	@Basic
	@Column(name = "vlmargemnaoassociado")
	private Double vlMargemNaoAssociado;

	@Basic
	@Column(name = "vlservico")
	private Double vlServico;

	@Basic
	@Column(name = "vlservicoassociado")
	private Double vlServicoAssociado;
	
	@JsonProperty
	@Transient
	private Integer quantidade;

	@JsonProperty
	@Transient
	private String tipoServicoHumanized;
	
	@Basic
	@Column(name = "botriagem")
	private Boolean boTriagem;
	
	@Basic
	@Column(name = "bogeraretornoagendamento")
	private Boolean boGeraRetornoAgendamento;
	
	@Basic
	@Column(name = "validadecontratual")
	private Integer validadeContratual;
	
	@Basic
	@Column(name = "nmpreparatorio")
	private String nmPreparatorio;
	
	@Basic
	@Column(name = "bosite")
	private Boolean boSite;
	
	@Basic
	@Column(name = "bolaudo")
	private Boolean boLaudo;

	public Servico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoBloqueio() {
		return boBloqueio;
	}

	public void setBoBloqueio(Boolean boBloqueio) {
		this.boBloqueio = boBloqueio;
	}

	public Boolean getBoGratuito() {
		return boGratuito;
	}

	public void setBoGratuito(Boolean boGratuito) {
		this.boGratuito = boGratuito;
	}

	public Boolean getBoValorAberto() {
		return boValorAberto;
	}

	public void setBoValorAberto(Boolean boValorAberto) {
		this.boValorAberto = boValorAberto;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Long getIdServicoAux() {
		return idServicoAux;
	}

	public void setIdServicoAux(Long idServicoAux) {
		this.idServicoAux = idServicoAux;
	}

	public Long getIdServicoOdonto() {
		return idServicoOdonto;
	}

	public void setIdServicoOdonto(Long idServicoOdonto) {
		this.idServicoOdonto = idServicoOdonto;
	}

	public Integer getInTipoServico() {
		return inTipoServico;
	}
	
	public String getTipoServicoHumanized() {
		if(null != this.inTipoServico)
		{
			switch (this.inTipoServico) {
			case 0:
				this.tipoServicoHumanized = "MÉDICO";
				break;
			case 1:
				this.tipoServicoHumanized = "ODONTOLÓGICO";
				break;
			case 2:
				this.tipoServicoHumanized = "LABORATORIAL";
				break;
			default:
				this.tipoServicoHumanized = "Desconhecido";
				break;
			}
		} else
		{
			this.tipoServicoHumanized = "Desconhecido";
		}
		return this.tipoServicoHumanized;
	}

	public void setInTipoServico(Integer inTipoServico) {
		this.inTipoServico = inTipoServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public String getNrCodServico() {
		return nrCodServico;
	}

	public void setNrCodServico(String nrCodServico) {
		this.nrCodServico = nrCodServico;
	}

	public Integer getValidade() {
		if (validade == null)
			return 0;
		
		return validade;
	}

	public void setValidade(Integer validade) {
		this.validade = validade;
	}

	public Double getVlCusto() {
		return vlCusto;
	}

	public void setVlCusto(Double vlCusto) {
		this.vlCusto = vlCusto;
	}

	public Double getVlMargemAssociado() {
		return vlMargemAssociado;
	}

	public void setVlMargemAssociado(Double vlMargemAssociado) {
		this.vlMargemAssociado = vlMargemAssociado;
	}

	public Double getVlMargemNaoAssociado() {
		return vlMargemNaoAssociado;
	}

	public void setVlMargemNaoAssociado(Double vlMargemNaoAssociado) {
		this.vlMargemNaoAssociado = vlMargemNaoAssociado;
	}

	public Double getVlServico() {
		return vlServico;
	}

	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}

	public Double getVlServicoAssociado() {
		return vlServicoAssociado;
	}

	public void setVlServicoAssociado(Double vlServicoAssociado) {
		this.vlServicoAssociado = vlServicoAssociado;
	}
	
	@JsonProperty
	@Transient
	public Integer getQuantidade() {
		return quantidade;
	}
	
	@JsonProperty
	@Transient
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Boolean getBoTriagem() {
		if (boTriagem == null)
			return false;
		
		return boTriagem;
	}

	public void setBoTriagem(Boolean boTriagem) {
		this.boTriagem = boTriagem;
	}
	
	public Boolean getBoGeraRetornoAgendamento() {
		if (boGeraRetornoAgendamento == null)
			return false;
		
		return boGeraRetornoAgendamento;
	}

	public void setBoGeraRetornoAgendamento(Boolean boGeraRetornoAgendamento) {
		this.boGeraRetornoAgendamento = boGeraRetornoAgendamento;
	}

	public Integer getValidadeContratual() {
		if (validadeContratual == null)
			return 0;
		
		return validadeContratual;
	}

	public void setValidadeContratual(Integer validadeContratual) {
		this.validadeContratual = validadeContratual;
	}

	public String getNmPreparatorio() {
		return nmPreparatorio;
	}

	public void setNmPreparatorio(String nmPreparatorio) {
		this.nmPreparatorio = nmPreparatorio;
	}	
	
	public Boolean getBoSite() {
		if (boSite == null)
			return false;
		
		return boSite;
	}

	public void setBoSite(Boolean boSite) {
		this.boSite = boSite;
	}
	
	public Boolean getBoLaudo() {
		return boLaudo;
	}

	public void setBoLaudo(Boolean boLaudo) {
		this.boLaudo = boLaudo;
	}

}