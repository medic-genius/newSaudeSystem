package br.com.medic.medicsystem.persistence.model;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbbairro")
public class Bairro {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BAIRRO_ID_SEQ")
	@SequenceGenerator(name = "BAIRRO_ID_SEQ", sequenceName = "realvida.bairro_id_seq", allocationSize = 1)
	@Column(name = "idbairro")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idbairroaux")
	private Long idBairroAux;

	@Basic
	@Column(name = "idbairroodonto")
	private Long idBairroOdonto;

	@Basic
	@Column(name = "nmbairro")
	private String nmBairro;

	// bi-directional many-to-one association to Cidade
	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	public Bairro() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdBairroAux() {
		return idBairroAux;
	}

	public void setIdBairroAux(Long idBairroAux) {
		this.idBairroAux = idBairroAux;
	}

	public Long getIdBairroOdonto() {
		return idBairroOdonto;
	}

	public void setIdBairroOdonto(Long idBairroOdonto) {
		this.idBairroOdonto = idBairroOdonto;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
