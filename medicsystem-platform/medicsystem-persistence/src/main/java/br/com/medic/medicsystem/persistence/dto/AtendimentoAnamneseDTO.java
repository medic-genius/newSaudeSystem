package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedicoAnamneseView;

public class AtendimentoAnamneseDTO {
	
	AnamneseDTO anamnese;
	
	List<AtendimentoMedicoAnamneseView> atendimentomedicoanamneseview;

	public AnamneseDTO getAnamnese() {
		return anamnese;
	}

	public void setAnamnese(AnamneseDTO anamnese) {
		this.anamnese = anamnese;
	}

	public List<AtendimentoMedicoAnamneseView> getAtendimentomedicoanamneseview() {
		return atendimentomedicoanamneseview;
	}

	public void setAtendimentomedicoanamneseview(
			List<AtendimentoMedicoAnamneseView> atendimentomedicoanamneseview) {
		this.atendimentomedicoanamneseview = atendimentomedicoanamneseview;
	}
	
	

}
