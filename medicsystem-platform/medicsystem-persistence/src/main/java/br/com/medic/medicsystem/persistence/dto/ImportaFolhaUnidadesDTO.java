package br.com.medic.medicsystem.persistence.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Entidade que fornece informacoes de um
 * conjunto de folhas de pagamento (ImportaFolhaDTO).
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

public class ImportaFolhaUnidadesDTO {

	private String mesanoreferencia;

	private Integer nroimportados;

	private double somaacrescimos;

	private double somadescontos;

	private double somasalariobase;

	private double somavalorapagar;
	
	private double somasalariofolha;

	private List<ImportaFolhaDTO> folhas;
	
	public ImportaFolhaUnidadesDTO() {
		folhas = new ArrayList<>();
	}

	public String getMesanoreferencia() {
		return mesanoreferencia;
	}

	public void setMesanoreferencia(String mesanoreferencia) {
		this.mesanoreferencia = mesanoreferencia;
	}

	public Integer getNroimportados() {
		return nroimportados;
	}

	public void setNroimportados(Integer nroimportados) {
		this.nroimportados = nroimportados;
	}

	public double getSomaacrescimos() {
		return somaacrescimos;
	}

	public void setSomaacrescimos(double somaacrescimos) {
		this.somaacrescimos = somaacrescimos;
	}

	public double getSomadescontos() {
		return somadescontos;
	}

	public void setSomadescontos(double somadescontos) {
		this.somadescontos = somadescontos;
	}

	public double getSomasalariobase() {
		return somasalariobase;
	}

	public void setSomasalariobase(double somasalariobase) {
		this.somasalariobase = somasalariobase;
	}

	public double getSomavalorapagar() {
		return somavalorapagar;
	}

	public void setSomavalorapagar(double somavalorapagar) {
		this.somavalorapagar = somavalorapagar;
	}

	public List<ImportaFolhaDTO> getFolhas() {
		return folhas;
	}

	public void setFolhas(List<ImportaFolhaDTO> folhas) {
		this.folhas = folhas;
	}

	public double getSomasalariofolha() {
		return somasalariofolha;
	}
	
	public void setSomasalariofolha(double somasalariofolha) {
		this.somasalariofolha = somasalariofolha;
	}
}
