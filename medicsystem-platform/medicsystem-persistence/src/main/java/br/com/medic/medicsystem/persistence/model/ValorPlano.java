package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbvalorplano database table.
 * 
 */
@Entity
public class ValorPlano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBVALORPLANO_IDVALORPLANO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBVALORPLANO_IDVALORPLANO_GENERATOR")
	private Long idvalorplano;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private float vlplano;


	//bi-directional many-to-one association to Tbplano
	@ManyToOne
	@JoinColumn(name="idplano")
	private Plano plano;

	public ValorPlano() {
	}

	public Long getIdvalorplano() {
		return this.idvalorplano;
	}

	public void setIdvalorplano(Long idvalorplano) {
		this.idvalorplano = idvalorplano;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public float getVlplano() {
		return this.vlplano;
	}

	public void setVlplano(float vlplano) {
		this.vlplano = vlplano;
	}

	public Plano getPlano() {
		return this.plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

}