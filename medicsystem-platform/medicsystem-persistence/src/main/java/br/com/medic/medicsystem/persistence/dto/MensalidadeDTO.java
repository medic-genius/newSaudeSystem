package br.com.medic.medicsystem.persistence.dto;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MensalidadeDTO {
	
	private Long idMensalidade;
	
	private String nrMensalidade;
	
	@JsonIgnore
	private Date dtVencimento;
	
	@JsonIgnore
	private Double vlMensalidade;
	
	private String dtVencimentoFormatado;
	
	private String vlMensalidadeFormatado;

	public String getNrMensalidade() {
		return nrMensalidade;
	}

	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}

	public Double getVlMensalidade() {
		return vlMensalidade;
	}

	public void setVlMensalidade(Double vlMensalidade) {
		this.vlMensalidade = vlMensalidade;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public String getVlMensalidadeFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVlMensalidade() != null)
			vlMensalidadeFormatado = currencyFormatter.format(getVlMensalidade());
		return vlMensalidadeFormatado;
	}

	public void setVlMensalidadeFormatado(String vlMensalidadeFormatado) {
		this.vlMensalidadeFormatado = vlMensalidadeFormatado;
	}

	public String getDtVencimentoFormatado() {
		
		SimpleDateFormat formatar = new SimpleDateFormat("dd/MM/yyyy");
		
		if(getDtVencimento() != null)
			dtVencimentoFormatado = formatar.format(getDtVencimento());
			
		return dtVencimentoFormatado;
	}

	public void setDtVencimentoFormatado(String dtVencimentoFormatado) {
		this.dtVencimentoFormatado = dtVencimentoFormatado;
	}

	public Long getIdMensalidade() {
		return idMensalidade;
	}

	public void setIdMensalidade(Long idMensalidade) {
		this.idMensalidade = idMensalidade;
	} 
	

}
