package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tbmovimentacaoitemconta", catalog = "financeiro")
public class MovimentacaoItemConta implements Serializable{

	private static final long serialVersionUID = 8409970616656165542L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOV_ITEM_CONTA_ID_SEQ")
	@SequenceGenerator(name = "MOV_ITEM_CONTA_ID_SEQ", sequenceName = "financeiro.mov_item_conta_id_seq", allocationSize = 1)
	@Column(name = "idmovimentacaoconta")
	private Long id;
	
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	//bi-directional many-to-one association to tbdespesafinanceiro
	@ManyToOne
	@JoinColumn(name="iddespesa")
	private DespesaFinanceiro despesaFinanceiro;

	//bi-directional many-to-one association to tbmovimentacaoconta
	@ManyToOne
	@JoinColumn(name="idmovimentacao")
	private MovimentacaoConta movimentacaoConta;
	
	@JsonIgnore
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@JsonIgnore
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@JsonIgnore
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@JsonIgnore
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtExclusao() {
		return dtExclusao;
	}
	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
	public DespesaFinanceiro getDespesaFinanceiro() {
		return despesaFinanceiro;
	}
	public void setDespesaFinanceiro(DespesaFinanceiro despesaFinanceiro) {
		this.despesaFinanceiro = despesaFinanceiro;
	}
	public MovimentacaoConta getMovimentacaoConta() {
		return movimentacaoConta;
	}
	public void setMovimentacaoConta(MovimentacaoConta movimentacaoConta) {
		this.movimentacaoConta = movimentacaoConta;
	}
	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}
	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}
	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}
	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}
	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}
	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}
	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}
	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}
	
}
