package br.com.medic.medicsystem.persistence.model.superlogica;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class ClienteSP implements Serializable {
	
	@NotNull
	private String st_nome_sac;
	
	@NotNull
	private String st_nomeref_sac;
	
	@NotNull
	private Integer st_diavencimento_sac;
	
	/**
	 *  CNPJ ou CPF
	 */
	private String st_cgc_sac;
	
	/**
	 *  RG
	 */
	private String st_rg_sac;
	
	/**
	 *  CEP
	 */
	private String st_cep_sac;
	
	/**
	 *  ENDEREÇO
	 */
	private String st_endereco_sac;
	
	/**
	 *  NUMERO CASA
	 */
	private String st_numero_sac;
	
	private String st_bairro_sac;
	
	private String st_complemento_sac;
	
	private String st_cidade_sac;
	
	private String st_estado_sac;
	
	private Integer fl_mesmoend_sac;
	
	private Integer in_idCliente;
	
	private String st_CodCliente;
	
	private String st_email_sac;
	
	private Long idContrato;

	

	public String getSt_nome_sac() {
		return st_nome_sac;
	}

	public void setSt_nome_sac(String st_nome_sac) {
		this.st_nome_sac = st_nome_sac;
	}

	public String getSt_nomeref_sac() {
		return st_nomeref_sac;
	}

	public void setSt_nomeref_sac(String st_nomeref_sac) {
		this.st_nomeref_sac = st_nomeref_sac;
	}

	public Integer getSt_diavencimento_sac() {
		return st_diavencimento_sac;
	}

	public void setSt_diavencimento_sac(Integer st_diavencimento_sac) {
		this.st_diavencimento_sac = st_diavencimento_sac;
	}

	public String getSt_cgc_sac() {
		return st_cgc_sac;
	}

	public void setSt_cgc_sac(String st_cgc_sac) {
		this.st_cgc_sac = st_cgc_sac;
	}

	public String getSt_rg_sac() {
		return st_rg_sac;
	}

	public void setSt_rg_sac(String st_rg_sac) {
		this.st_rg_sac = st_rg_sac;
	}

	public String getSt_cep_sac() {
		return st_cep_sac;
	}

	public void setSt_cep_sac(String st_cep_sac) {
		this.st_cep_sac = st_cep_sac;
	}

	public String getSt_endereco_sac() {
		return st_endereco_sac;
	}

	public void setSt_endereco_sac(String st_endereco_sac) {
		this.st_endereco_sac = st_endereco_sac;
	}

	public String getSt_numero_sac() {
		return st_numero_sac;
	}

	public void setSt_numero_sac(String st_numero_sac) {
		this.st_numero_sac = st_numero_sac;
	}

	public String getSt_bairro_sac() {
		return st_bairro_sac;
	}

	public void setSt_bairro_sac(String st_bairro_sac) {
		this.st_bairro_sac = st_bairro_sac;
	}

	public String getSt_complemento_sac() {
		return st_complemento_sac;
	}

	public void setSt_complemento_sac(String st_complemento_sac) {
		this.st_complemento_sac = st_complemento_sac;
	}

	public String getSt_cidade_sac() {
		return st_cidade_sac;
	}

	public void setSt_cidade_sac(String st_cidade_sac) {
		this.st_cidade_sac = st_cidade_sac;
	}

	public String getSt_estado_sac() {
		return st_estado_sac;
	}

	public void setSt_estado_sac(String st_estado_sac) {
		this.st_estado_sac = st_estado_sac;
	}

	public Integer getFl_mesmoend_sac() {
		return fl_mesmoend_sac;
	}

	public void setFl_mesmoend_sac(Integer fl_mesmoend_sac) {
		this.fl_mesmoend_sac = fl_mesmoend_sac;
	}

	public String getSt_CodCliente() {
		return st_CodCliente;
	}

	public void setSt_CodCliente(String st_CodCliente) {
		this.st_CodCliente = st_CodCliente;
	}

	public Integer getIn_idCliente() {
		return in_idCliente;
	}

	public void setIn_idCliente(Integer in_idCliente) {
		this.in_idCliente = in_idCliente;
	}

	public String getSt_email_sac() {
		return st_email_sac;
	}

	public void setSt_email_sac(String st_email_sac) {
		this.st_email_sac = st_email_sac;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	


	

	
	

}
