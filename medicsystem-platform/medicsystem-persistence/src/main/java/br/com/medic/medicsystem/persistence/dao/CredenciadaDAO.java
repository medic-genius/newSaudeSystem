package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Credenciada;

@Named("credenciada-dao")
@ApplicationScoped
public class CredenciadaDAO extends RelationalDataAccessObject<Credenciada> {

	
}
