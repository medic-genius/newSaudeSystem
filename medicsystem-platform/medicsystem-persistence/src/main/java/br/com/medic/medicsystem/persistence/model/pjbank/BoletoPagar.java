package br.com.medic.medicsystem.persistence.model.pjbank;

public class BoletoPagar {
	
	/**
	 * Número da credencial que foi fornecida no credenciamento, length (40).
	 */
	private String credencial;
	
	/**
	 * Vencimento da cobrança no formato MM/DD/AAAA, length (10-10).
	 */
	private String vencimento;
	  
	/**
	 * Valor a ser cobrado em reais. Casas decimais devem ser separadas por ponto, 
	 * máximo de 2 casas decimais, não enviar caracteres diferentes de número ou ponto. 
	 * Não usar separadores de milhares. Exemplo: 1000.98.
	 */
	private Number valor;
	
	/**
	 * Taxa de juros ao dia por atraso. Casas decimais devem ser separadas por ponto, 
	 * máximo de 2 casas decimais, não enviar caracteres diferentes de número ou ponto. 
	 * Não usar separadores de milhares. Exemplo: 0.98, length (1-2).
	 */
	private String juros;
	
	/**
	 * Taxa de multa por atraso. Casas decimais devem ser separadas por ponto, 
	 * máximo de 2 casas decimais, não enviar caracteres diferentes de número ou ponto. 
	 * Não usar separadores de milhares. Exemplo: 0.98, length (1-2).
	 */
	private String multa;
	
	/**
	 * Valor do desconto por pontualidade, em Reais. Casas decimais devem ser separadas por ponto, 
	 * máximo de 2 casas decimais, não enviar caracteres diferentes de número ou ponto. 
	 * Não usar separadores de milhares. Exemplo: 9.58.
	 */
	private String desconto;
	
	/**
	 * Nome completo do pagador. Exemplo: Pagador. length (3-80).
	 */
	private String nome_cliente;
	
	/**
	 * CPF ou CNPJ do pagador. Por enquanto não é obrigatório, porém por determinação da Febraban será obrigatório em breve.
	 */
	private String cpf_cliente;
	
	/**
	 * Endereço do pagador, length (3-80).
	 */
	private String endereco_cliente;
	
	/**
	 * Número do endereço do pagador, length (1-10).
	 */
	private String numero_cliente;
	
	/**
	 * Complemento do endereço do pagador, length (1-80).
	 */
	private String complemento_cliente;
	
	/**
	 * Bairro do endereço do pagador, length (3-80).
	 */
	private String bairro_cliente;
	
	/**
	 * Cidade do endereço do pagador, length (3-80).
	 */
	private String cidade_cliente;
	
	/**
	 * Estado do endereço do pagador, com 2 caracteres. Exemplo: SP, length (2-2).
	 */
	private String estado_cliente;
	
	/**
	 * CEP do endereço do pagador. Apenas números, length (8-10).
	 */
	private Number cep_cliente;
	
	/**
	 * URL do logo da empresa. Será cacheado de forma agressiva, portanto, para mudar o logo altere a url, length (264).
	 */
	private String logo_url;
	
	/**
	 * Numero do pedido da cobrança. Este número é importante se você precisar editar o boleto sem necessidade de duplica-lo. 
	 * O sistema não vai gerar outro boleto se o número do pedido existir. 
	 * Importante: para relacionar os boletos pagos não use o pedido e sim o campo “nosso número”, que será retornado nesta requisição. 
	 * ( Obrigatório em caso de alterações de dados da cobrança. ), length (0-20).
	 */
	private String pedido_numero;
	
	/**
	 * Texto que ficará no topo dos boletos. Será impresso com fonte fixa. Limite de 120 linhas e 65 colunas, length (0-680).
	 */
	private String texto;
	
	/**
	 * Identificação do grupo. É uma string que identifica um grupo de boletos. 
	 * Quando um valor é passado neste campo, é retornado um link adicional para impressão de todos os boletos do mesmo grupo de uma vez. 
	 * Recomendado para imprimir carnês, por exemplo, length (0-20).
	 */
	private String grupo;
	
	

	public String getCredencial() {
		return credencial;
	}

	public void setCredencial(String credencial) {
		this.credencial = credencial;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public Number getValor() {
		return valor;
	}

	public void setValor(Number valor) {
		this.valor = valor;
	}

	public String getJuros() {
		return juros;
	}

	public void setJuros(String juros) {
		this.juros = juros;
	}

	public String getMulta() {
		return multa;
	}

	public void setMulta(String multa) {
		this.multa = multa;
	}

	public String getDesconto() {
		return desconto;
	}

	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}

	public String getCpf_cliente() {
		return cpf_cliente;
	}

	public void setCpf_cliente(String cpf_cliente) {
		this.cpf_cliente = cpf_cliente;
	}

	public String getEndereco_cliente() {
		return endereco_cliente;
	}

	public void setEndereco_cliente(String endereco_cliente) {
		this.endereco_cliente = endereco_cliente;
	}

	public String getNumero_cliente() {
		return numero_cliente;
	}

	public void setNumero_cliente(String numero_cliente) {
		this.numero_cliente = numero_cliente;
	}

	public String getComplemento_cliente() {
		return complemento_cliente;
	}

	public void setComplemento_cliente(String complemento_cliente) {
		this.complemento_cliente = complemento_cliente;
	}

	public String getBairro_cliente() {
		return bairro_cliente;
	}

	public void setBairro_cliente(String bairro_cliente) {
		this.bairro_cliente = bairro_cliente;
	}

	public String getCidade_cliente() {
		return cidade_cliente;
	}

	public void setCidade_cliente(String cidade_cliente) {
		this.cidade_cliente = cidade_cliente;
	}

	public String getEstado_cliente() {
		return estado_cliente;
	}

	public void setEstado_cliente(String estado_cliente) {
		this.estado_cliente = estado_cliente;
	}

	public Number getCep_cliente() {
		return cep_cliente;
	}

	public void setCep_cliente(Number cep_cliente) {
		this.cep_cliente = cep_cliente;
	}

	public String getLogo_url() {
		return logo_url;
	}

	public void setLogo_url(String logo_url) {
		this.logo_url = logo_url;
	}

	public String getPedido_numero() {
		return pedido_numero;
	}

	public void setPedido_numero(String pedido_numero) {
		this.pedido_numero = pedido_numero;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

}
