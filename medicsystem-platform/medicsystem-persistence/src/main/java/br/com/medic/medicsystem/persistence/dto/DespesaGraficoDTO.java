package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DespesaGraficoDTO implements Serializable{

	private static final long serialVersionUID = 4690090110997989444L;

	private Long nrQuantidade;
	private Double nrValorTotal;
	private String nmMes;
	private Double nrAno;
	private Double nrMes;
	
	public DespesaGraficoDTO() {

	}

	public DespesaGraficoDTO(Long nrQuantidade, Double nrValorTotal, String nmMes, Double nrAno) {
		this.nrQuantidade = nrQuantidade;
		this.nrValorTotal = nrValorTotal;
		this.nmMes = nmMes;
		this.nrAno = nrAno;
	}

	public Long getNrQuantidade() {
		return nrQuantidade;
	}
	public void setNrQuantidade(Long nrQuantidade) {
		this.nrQuantidade = nrQuantidade;
	}
	public Double getNrValorTotal() {
		return nrValorTotal;
	}
	public void setNrValorTotal(Double nrValorTotal) {
		this.nrValorTotal = nrValorTotal;
	}
	public String getNmMes() {
		return nmMes;
	}
	public void setNmMes(String nmMes) {
		this.nmMes = nmMes;
	}
	public Double getNrAno() {
		return nrAno;
	}
	public void setNrAno(Double nrAno) {
		this.nrAno = nrAno;
	}
	public Double getNrMes() {
		return nrMes;
	}
	public void setNrMes(Double nrMes) {
		this.nrMes = nrMes;
	}

	@JsonProperty	
	public String getNrValorTotalFormatado() {
		if(nrValorTotal != null){
			return String.format("%.2f", nrValorTotal);
		}
		return "0.00";

	}
	
	@JsonIgnore
	public void setNrValorTotalFormatado(String nrValorTotalFormatado) {
	}
	
}
