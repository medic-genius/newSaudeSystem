package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbdespesa")
public class Despesa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESPESA_ID_SEQ")
	@SequenceGenerator(name = "DESPESA_ID_SEQ", sequenceName = "realvida.despesa_id_seq", allocationSize = 1)
	@Column(name = "iddespesa")
	private Long id;

	@Basic
	@Column(name = "boexcluida")
	private Boolean boExcluida;

	@Basic
	@Column(name = "boparcelada")
	private Boolean boParcelaDa;

	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtdespesa")
	private Date dtDespesa;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "iddespesaaux")
	private Long idDespesaAux;

	@Basic
	@Column(name = "iddespesaodonto")
	private Long idDespesaOdonto;

	@Basic
	@Column(name = "iddespesapai")
	private Integer idDespesaPai;

	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;

	@Basic
	@Column(name = "nmobservacao")
	private String nmObservacao;

	@Basic
	@Column(name = "nrdespesa")
	private String nrDespesa;

	@Basic
	@Column(name = "vldespesaaberto")
	private Double vlDespesaAberto;

	@Basic
	@Column(name = "vldespesacoberta")
	private Double vlDespesaCoberta;

	@Basic
	@Column(name = "vldespesatotal")
	private Double vlDespesaTotal;

	@ManyToOne
	@JoinColumn(name = "idagendamento")
	private Agendamento agendamento;

//	@ManyToOne
//	@JoinColumn(name = "idcartaoimpresso")
//	private CartaoImpresso cartaoImpresso;

	@ManyToOne
	@JoinColumn(name = "idcontratocliente")
	private ContratoCliente contratoCliente;

	@ManyToOne
	@JoinColumn(name = "idcontratodependente")
	private ContratoDependente contratoDependente;

	@Basic
	@Column(name = "idoperadorinclusao")
	private Long idOperadorCadastro;

	@JsonIgnore
	@Transient
	private Funcionario operadorCadastro;

	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;

	
	@OneToMany(mappedBy = "despesa", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<DespesaServico> despesaServicos;

	@JsonIgnore
	@OneToMany(mappedBy = "despesa", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Parcela> parcelas;

	@Transient
	private String dtDespesaFormatado;

	@JsonIgnore
	@Transient
	private String vlDespesaTotalFormatado;

	@JsonIgnore
	@Transient
	private String nomePaciente;

	@JsonIgnore
	@Transient
	private Dependente dependente;

	public Despesa() {
		this.despesaServicos = new ArrayList<DespesaServico>();
		this.parcelas = new ArrayList<Parcela>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoExcluida() {
		return boExcluida;
	}

	public void setBoExcluida(Boolean boExcluida) {
		this.boExcluida = boExcluida;
	}

	public Boolean getBoParcelaDa() {
		return boParcelaDa;
	}

	public void setBoParcelaDa(Boolean boParcelaDa) {
		this.boParcelaDa = boParcelaDa;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Date getDtDespesa() {
		return dtDespesa;
	}

	public void setDtDespesa(Date dtDespesa) {
		this.dtDespesa = dtDespesa;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdDespesaAux() {
		return idDespesaAux;
	}

	public void setIdDespesaAux(Long idDespesaAux) {
		this.idDespesaAux = idDespesaAux;
	}

	public Long getIdDespesaOdonto() {
		return idDespesaOdonto;
	}

	public void setIdDespesaOdonto(Long idDespesaOdonto) {
		this.idDespesaOdonto = idDespesaOdonto;
	}

	public Integer getIdDespesaPai() {
		return idDespesaPai;
	}

	public void setIdDespesaPai(Integer idDespesaPai) {
		this.idDespesaPai = idDespesaPai;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public String getNrDespesa() {
		return nrDespesa;
	}

	public void setNrDespesa(String nrDespesa) {
		this.nrDespesa = nrDespesa;
	}

	public Double getVlDespesaAberto() {
		return vlDespesaAberto;
	}

	public void setVlDespesaAberto(Double vlDespesaAberto) {
		this.vlDespesaAberto = vlDespesaAberto;
	}

	public Double getVlDespesaCoberta() {
		return vlDespesaCoberta;
	}

	public void setVlDespesaCoberta(Double vlDespesaCoberta) {
		this.vlDespesaCoberta = vlDespesaCoberta;
	}

	public Double getVlDespesaTotal() {
		return vlDespesaTotal;
	}

	public void setVlDespesaTotal(Double vlDespesaTotal) {
		this.vlDespesaTotal = vlDespesaTotal;
	}

	public Agendamento getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}

//	public CartaoImpresso getCartaoImpresso() {
//		return cartaoImpresso;
//	}
//
//	public void setCartaoImpresso(CartaoImpresso cartaoImpresso) {
//		this.cartaoImpresso = cartaoImpresso;
//	}

	public ContratoCliente getContratoCliente() {
		return contratoCliente;
	}

	public void setContratoCliente(ContratoCliente contratoCliente) {
		this.contratoCliente = contratoCliente;
	}

	public ContratoDependente getContratoDependente() {
		return contratoDependente;
	}

	public void setContratoDependente(ContratoDependente contratoDependente) {
		this.contratoDependente = contratoDependente;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public List<DespesaServico> getDespesaServicos() {
		return despesaServicos;
	}

	public void setDespesaServicos(List<DespesaServico> despesaServicos) {
		this.despesaServicos = despesaServicos;
	}

	public DespesaServico addDespesaservico(DespesaServico despesaServico) {
		despesaServico.setDespesa(this);
		getDespesaServicos().add(despesaServico);

		return despesaServico;
	}

	public DespesaServico removeDespesaservico(DespesaServico despesaServico) {
		getDespesaServicos().remove(despesaServico);
		despesaServico.setDespesa(null);

		return despesaServico;
	}

	@JsonIgnore
	public List<Parcela> getParcelas() {
		return parcelas;
	}

	@JsonIgnore
	public void setParcelas(List<Parcela> parcelas) {
		this.parcelas = parcelas;
	}

	public Parcela addParcela(Parcela parcela) {
		parcela.setDespesa(this);
		getParcelas().add(parcela);

		return parcela;
	}

	public Parcela removeParcela(Parcela parcela) {
		getParcelas().remove(parcela);
		parcela.setDespesa(null);

		return parcela;
	}

	@JsonProperty
	public String getDtDespesaFormatado() {
		if (getDtDespesa() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtDespesa());
		}
		return null;
	}

	public void setDtDespesaFormatado(String dtDespesaFormatado) {
		this.dtDespesaFormatado = dtDespesaFormatado;
	}

	@JsonProperty
	public String getVlDespesaTotalFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);

		if (getVlDespesaTotal() != null)
			return currencyFormatter.format(getVlDespesaTotal());
		return null;
	}

	@JsonIgnore
	public void setOperadorCadastro(Funcionario operadorCadastro) {
		this.operadorCadastro = operadorCadastro;
	}

	@JsonProperty
	public Funcionario getOperadorCadastro() {
		return operadorCadastro;
	}

	@JsonProperty
	public String getNomePaciente() {
		if (getContratoDependente() != null) {
			return getContratoDependente().getDependente().getNmDependente();
		}

		return getContratoCliente().getCliente().getNmCliente();
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Dependente getDependente() {
		return dependente;
	}

}