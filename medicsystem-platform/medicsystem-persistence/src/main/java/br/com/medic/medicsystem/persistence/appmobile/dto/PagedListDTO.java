package br.com.medic.medicsystem.persistence.appmobile.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class PagedListDTO<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Collection<T> list;
	private Boolean hasMoreData;
	
	public PagedListDTO(List<T> list, Boolean hasMore) {
		this.list = list;
		this.hasMoreData = hasMore;
	}
	
	public Collection<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public Boolean getHasMoreData() {
		return hasMoreData;
	}
	public void setHasMoreData(Boolean hasMoreData) {
		this.hasMoreData = hasMoreData;
	}
}