package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbcarencia")
public class Carencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARENCIA_ID_SEQ")
	@SequenceGenerator(name = "CARENCIA_ID_SEQ", sequenceName = "realvida.carencia_id_seq", allocationSize = 1)
	@Column(name = "idcarencia")
	private Long id;

	@Basic
	@Column(name = "carencia")
	private Integer carencia;

	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;

	@ManyToOne
	@JoinColumn(name = "idplano")
	private Plano plano;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	@Basic
	@Column(name = "totalconsulta")
	private Integer totalConsulta;

	public Carencia() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCarencia() {
		return carencia;
	}

	public void setCarencia(Integer carencia) {
		this.carencia = carencia;
	}

	public Integer getTotalConsulta() {
		return totalConsulta;
	}

	public void setTotalConsulta(Integer totalConsulta) {
		this.totalConsulta = totalConsulta;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}