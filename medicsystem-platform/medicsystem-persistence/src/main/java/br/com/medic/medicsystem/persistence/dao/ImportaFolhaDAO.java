package br.com.medic.medicsystem.persistence.dao;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioFolha;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaDescontosTotalDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaGraficoDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaTotaisGrupoDTO;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;
import br.com.medic.medicsystem.persistence.model.ImportaFolha;
import br.com.medic.medicsystem.persistence.model.UnidadeFinanceiro;

/**
 * @author Phillip
 * @since 01/2016
 * @lastModification 29/05/2016
 * @version 1.4
 */

@Named("importafolha-dao")
@ApplicationScoped
public class ImportaFolhaDAO extends RelationalDataAccessObject<ImportaFolha> {

	public UnidadeFinanceiro getUnidadeFinanceiroByNickName(String unidadeNickName, String empresaNickName) {
	
		unidadeNickName = normalizarTexto(unidadeNickName);
		empresaNickName = normalizarTexto(empresaNickName);
		
		TypedQuery<UnidadeFinanceiro> query = entityManager.createQuery(
				"SELECT und FROM UnidadeFinanceiro und where und.nmNickName = '" + unidadeNickName.toUpperCase() + "'"
				+ " and und.empresaFinanceiro.nmNickName = '"+empresaNickName+"'",
		        UnidadeFinanceiro.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ImportaFolhaGraficoDTO> getImportaFolhaUltimos12Meses(Long idEmpresaFinanceiro,String data) {
		
		String queryStr =
				"SELECT mesanoreferencia as mes, sum(somavlunidades) as folha FROM (" 
						+ "select oif.mesanoreferencia, sum(oif.somavalorapagar) as somavlunidades "
						+"from realvida.tbimportaFolha oif "
						+"join financeiro.tbunidadefinanceiro uf on oif.idunidadefinanceiro = uf.idunidadefinanceiro "
						+"join financeiro.tbempresafinanceiro ef on ef.idempresafinanceiro = uf.idempresafinanceiro "
						+"where oif.mesanoreferencia in (" + data + ") "
						+"and uf.idEmpresaFinanceiro = "+ idEmpresaFinanceiro + " "
						+"group by oif.mesanoreferencia, oif.somavalorapagar "
						+") as financeiromes "
				+"group by mesanoreferencia "
				+"order by  SUBSTR(mesanoreferencia,4,4), SUBSTR(mesanoreferencia,1,2)";
		
		List<ImportaFolhaGraficoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("mes", StringType.INSTANCE)
		        .addScalar("folha", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ImportaFolhaGraficoDTO.class)).list();
		
		return result;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ImportaFolhaGraficoDTO> getImportaGrupoUltimos12Meses(Long idGrupoFinanceiro,String data) {
		
		String queryStr =
				"SELECT mesanoreferencia as mes, sum(somavlunidades) as folha FROM (" 
						+ "select oif.mesanoreferencia, sum(oif.somavalorapagar) as somavlunidades "
						+"from realvida.tbimportaFolha oif "
						+"join financeiro.tbunidadefinanceiro uf on oif.idunidadefinanceiro = uf.idunidadefinanceiro "
						+"join financeiro.tbempresafinanceiro ef on ef.idempresafinanceiro = uf.idempresafinanceiro "
						+"where oif.mesanoreferencia in (" + data + ") "
						+"and ef.intipogrupo = "+ idGrupoFinanceiro + " "
						+"group by oif.mesanoreferencia, oif.somavalorapagar "
						+") as financeiromes "
				+"group by mesanoreferencia "
				+"order by  SUBSTR(mesanoreferencia,4,4), SUBSTR(mesanoreferencia,1,2)";
		
		List<ImportaFolhaGraficoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("mes", StringType.INSTANCE)
		        .addScalar("folha", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ImportaFolhaGraficoDTO.class)).list();
		
		return result;
		
	}
	
	/**
	 * 
	 * Normaliza uma string, retirando
	 * acentuação e caracteres que dificultam
	 * a comparação
	 * 
	 * @author Joelton
	 *
	 */
	private static String normalizarTexto( String texto ){
		
		String textoNormalizado = Normalizer.normalize( texto.replaceAll("\\s", "").toUpperCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		
		return  textoNormalizado;
	}

	/**
	 * @author Joelton
	 * 
	 * @param nmEmpesaFinanceiro
	 * 
	 * @return
	 */
	public EmpresaFinanceiro getEmpresaFinanceiroByNickName(String empresaNickName) {
		
		empresaNickName = normalizarTexto(empresaNickName);
		
		TypedQuery<EmpresaFinanceiro> query = entityManager.createQuery(
				"SELECT ef FROM EmpresaFinanceiro ef where ef.nmNickName = '" + empresaNickName.toUpperCase() + "'", EmpresaFinanceiro.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ImportaFolhaDescontosTotalDTO> getDescontosTotais(Long idEmpresaFinanceiro, String data) {
		
		String queryStr =
				"SELECT  ifff.codigo as nrCodigo ,ifff.descricao as descricao, sum(vltotal) as total from " 
						+"realvida.tbimportafolha oif "
						+"join realvida.tbimportafolhafunc iff on oif.idfolha = iff.idfolha "
						+"join realvida.tbimportafolhafuncfluxo ifff on iff.idfunc = ifff.idfunc "
						+"join financeiro.tbunidadefinanceiro uf on uf.idunidadefinanceiro = oif.idunidadefinanceiro "
						+"join financeiro.tbempresafinanceiro ef on ef.idempresafinanceiro = uf.idempresafinanceiro "
						+"where "
						+"oif.mesanoreferencia = '"+ data + "' "
						+"and ef.idempresafinanceiro = " + idEmpresaFinanceiro + " "
						+"and ifff.tipo = '" + "descontos" +"' "
						+"group by ifff.descricao,ifff.codigo" + " "
						+"order by ifff.descricao ";
					
		
		List<ImportaFolhaDescontosTotalDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("nrCodigo", StringType.INSTANCE)
		        .addScalar("descricao", StringType.INSTANCE)
		        .addScalar("total", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ImportaFolhaDescontosTotalDTO.class)).list();
		
		return result;
		
	}

	@SuppressWarnings("unchecked")
	public ImportaFolhaTotaisGrupoDTO getTotaisGrupoByPeriodo(Integer inTipoGrupo, String data) {
				
		String queryStr =
				"select sum(somasalariobase) as salarioBaseTotal, sum(somaacrescimos) as somaAcrescimoTotal, " 
						+"sum(somadescontos) as somaDescontoTotal,sum(somavalorapagar) as somaValorPagarTotal, "
						+"sum(somasalariofolha) as somaSalarioFolhaTotal " 
						+"from realvida.tbimportafolha oif  "
						+" join financeiro.tbunidadefinanceiro uf on oif.idunidadefinanceiro = uf.idunidadefinanceiro "
						+"join financeiro.tbempresafinanceiro ef on ef.idempresafinanceiro = uf.idempresafinanceiro "
						+"where "
						+"oif.mesanoreferencia = '" + data  + "' "
						+"and ef.intipogrupo = " + inTipoGrupo;
					
		List<ImportaFolhaTotaisGrupoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("salarioBaseTotal", DoubleType.INSTANCE)
		        .addScalar("somaAcrescimoTotal", DoubleType.INSTANCE)
		        .addScalar("somaDescontoTotal", DoubleType.INSTANCE)
		        .addScalar("somaValorPagarTotal", DoubleType.INSTANCE)
		        .addScalar("somaSalarioFolhaTotal", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ImportaFolhaTotaisGrupoDTO.class)).list();
		
		return result.get(0);
		
	}

	public List<BaseDTO> verificarFuncionarioMesmoCodigo(Long idEmpresaFinanceiro, String mesAnoReferencia, List<FuncionarioFolha> list) {
		
		List<BaseDTO> resultadoList = new ArrayList<BaseDTO>();
		
		for (FuncionarioFolha funcionarioFolha : list) {
			TypedQuery<Long> query;
			Long result = 0L;
			
			String querySql = " select count(iff) from ImportaFolhaFunc iff "
					+ " where iff.codigo  = '" + funcionarioFolha.getCod() + "' "
					+ " and iff.tbimportafolha.mesanoreferencia = '" + mesAnoReferencia +"' "
					+ " and iff.empresafinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro
					+ " and trim(iff.nome) != '" + funcionarioFolha.getName().trim() + "' " ;
			
			query = entityManager.createQuery(querySql, Long.class);
			
			try{
				
				result = query.getSingleResult();
				
				if( result > 0L ) {
					BaseDTO base = new BaseDTO();
					base.setDescription( funcionarioFolha.getCod() );
					base.setOpcionalInfo( funcionarioFolha. getName() );
					
					resultadoList.add(base);
				}
					
			}catch(NoResultException ex){
				result = 0L;
			}
		}
		
		return resultadoList;
	}

}
