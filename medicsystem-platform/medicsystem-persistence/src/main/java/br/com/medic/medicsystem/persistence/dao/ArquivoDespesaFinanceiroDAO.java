package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ArquivoDespesaFinanceiro;

@ApplicationScoped
@Named("arquivodespesafinanceiro-dao")
public class ArquivoDespesaFinanceiroDAO extends RelationalDataAccessObject<ArquivoDespesaFinanceiro>{

	public List<ArquivoDespesaFinanceiro> getArquivosByDespesa(Long idDespesa){
		String queryStr = "SELECT a FROM ArquivoDespesaFinanceiro a WHERE a.dtExclusao is null AND a.despesaFinanceiro.id = " + idDespesa + " ORDER BY a.nmNome";
		TypedQuery<ArquivoDespesaFinanceiro> query = entityManager.createQuery(queryStr, ArquivoDespesaFinanceiro.class);
		List<ArquivoDespesaFinanceiro> list = query.getResultList();
		return list;
	}
}
