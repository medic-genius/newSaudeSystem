package br.com.medic.medicsystem.persistence.model.views;

import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "conveniadadespesaconvenio_view")
public class DespesaConvenioView {
	
	@Id
	@Column(name = "id")
	private Long 	id;
	
	@Basic
	@Column(name = "idparcela")
	private Long 	idParcela;
	
	@Basic
	@Column(name = "vlparcela")
	private Double 	vlParcela;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtvencimento")
	private Date 	dtVencimento;
	
	@Basic
	@Column(name = "nrparcela")
	private String 	nrParcela;
	
	@Basic
	@Column(name = "iddespesa")
	private Long 	idDespesa;
	
	@Basic
	@Column(name = "nrdespesa")
	private String 	nrDespesa;
		
	@Basic
	@Column(name = "nrcontrato")
	private String 	nrContrato;
	
	@Basic
	@Column(name = "idempresacliente")
	private Long 	idEmpresaCliente;
	
	@Basic
	@Column(name = "nmcliente")
	private String 	nmCliente;
	
	@Basic
	@Column(name = "nmdependente")
	private String 	nmDependente;
	
	@Basic
	@Column(name = "idservico")
	private Long 	idServico;
	
	@Basic
	@Column(name = "nmservico")
	private String 	nmServico;
	
	@Basic
	@Column(name = "vlservico")
	private Double 	vlServico;
	
	@Basic
	@Column(name = "qtservico")
	private Integer	qtServico;
	
	@Basic
	@Column(name = "iddespesaservico")
	private Long 	idDespesaServico;
	
	@Basic
	@Column(name = "idguia")
	private Long 	idGuia;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdParcela() {
		return idParcela;
	}

	public void setIdParcela(Long idParcela) {
		this.idParcela = idParcela;
	}

	public Double getVlParcela() {
		return vlParcela;
	}

	public void setVlParcela(Double vlParcela) {
		this.vlParcela = vlParcela;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public String getNrParcela() {
		return nrParcela;
	}

	public void setNrParcela(String nrParcela) {
		this.nrParcela = nrParcela;
	}

	public Long getIdDespesa() {
		return idDespesa;
	}

	public void setIdDespesa(Long idDespesa) {
		this.idDespesa = idDespesa;
	}

	public String getNrDespesa() {
		return nrDespesa;
	}

	public void setNrDespesa(String nrDespesa) {
		this.nrDespesa = nrDespesa;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public Long getIdEmpresaCliente() {
		return idEmpresaCliente;
	}

	public void setIdEmpresaCliente(Long idEmpresaCliente) {
		this.idEmpresaCliente = idEmpresaCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public Double getVlServico() {
		DecimalFormat formato = new DecimalFormat("#.##");
		return Double.valueOf(formato.format(vlServico).replace(",", "."));
	}

	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}

	public Integer getQtServico() {
		return qtServico;
	}

	public void setQtServico(Integer qtServico) {
		this.qtServico = qtServico;
	}

	public Long getIdDespesaServico() {
		return idDespesaServico;
	}

	public void setIdDespesaServico(Long idDespesaServico) {
		this.idDespesaServico = idDespesaServico;
	}

	public Long getIdGuia() {
		return idGuia;
	}

	public void setIdGuia(Long idGuia) {
		this.idGuia = idGuia;
	}
	
	

}
