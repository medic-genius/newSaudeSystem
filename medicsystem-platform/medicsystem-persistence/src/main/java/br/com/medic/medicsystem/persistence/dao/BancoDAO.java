package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Banco;

@Named("banco-dao")
@ApplicationScoped
public class BancoDAO extends RelationalDataAccessObject<Banco> {

	public List<Banco> getAllBancos() {

		TypedQuery<Banco> query = entityManager.createQuery(
		        "SELECT c FROM Banco c order by c.nmBanco asc", Banco.class);
		List<Banco> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Banco getBancobyCod( String codBanco) {
		
		Integer codigoBanco = Integer.parseInt(codBanco);
		
		String queryStr = "SELECT b FROM Banco b WHERE b.cdbanco like '%" + codigoBanco + "%'"
				+ " order by b.nmBanco asc"; 
		
		TypedQuery<Banco> query = entityManager.createQuery(queryStr , Banco.class);
						
		try {
			Banco result = query.getSingleResult();
			
			if(result != null)
				return result;
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
				
		return null;
	}

	public Banco getBancoById(Long idBanco) {

		TypedQuery<Banco> query = entityManager.createQuery(
		        "SELECT c FROM Banco c where c.id =" + idBanco, Banco.class);
		Banco result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
}
