package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbgrupoorgao")
public class GrupoOrgao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GRUPOORGAO_ID_SEQ")
	@SequenceGenerator(name = "GRUPOORGAO_ID_SEQ", sequenceName = "realvida.grupoorgao_id_seq", allocationSize = 1)
	@Column(name = "idgrupoorgao")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idgrupoorgaoaux")
	private Long idGrupoOrgaoAux;

	@Basic
	@Column(name = "idgrupoorgaoodonto")
	private Long idGrupoOrgaoOdonto;

	@Basic
	@Column(name = "nmgrupoorgao")
	private String nmGrupoOrgao;

	public GrupoOrgao() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdGrupoOrgaoAux() {
		return idGrupoOrgaoAux;
	}

	public void setIdGrupoOrgaoAux(Long idGrupoOrgaoAux) {
		this.idGrupoOrgaoAux = idGrupoOrgaoAux;
	}

	public Long getIdGrupoOrgaoOdonto() {
		return idGrupoOrgaoOdonto;
	}

	public void setIdGrupoOrgaoOdonto(Long idGrupoOrgaoOdonto) {
		this.idGrupoOrgaoOdonto = idGrupoOrgaoOdonto;
	}

	public String getNmGrupoOrgao() {
		return nmGrupoOrgao;
	}

	public void setNmGrupoOrgao(String nmGrupoOrgao) {
		this.nmGrupoOrgao = nmGrupoOrgao;
	}
}