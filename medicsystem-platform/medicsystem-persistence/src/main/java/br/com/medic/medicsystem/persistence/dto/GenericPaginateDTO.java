package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.List;

public class GenericPaginateDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Total items
	 */
	private Long total;

	/**
	 * Limit items per page
	 */
	private int limit = 10;
	
	/**
	 * Reference number page, not offset db
	 */
	private int offset;
	
	private int totalPage;

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	private List<?> data;
	
	public void prepare()
	{
		this.totalPage = (int) Math.ceil((float)this.total / (float)this.limit);
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> lGuia) {
		this.data = lGuia;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
}
