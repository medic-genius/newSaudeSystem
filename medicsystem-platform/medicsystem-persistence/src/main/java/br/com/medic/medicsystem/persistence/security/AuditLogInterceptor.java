package br.com.medic.medicsystem.persistence.security;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.bson.Document;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.security.dto.AuditObject;

@Interceptor
@Log
public class AuditLogInterceptor {

	private static final String OP_PERSIST = "persist";
	private static final String OP_UPDATE = "update";
	private static final String OP_REMOVE = "remove";
	// private static final String OP_FIND = "find";
	// private static final String OP_LOAD = "load";
	// private static final String OP_GET = "get";
	// private static final String OP_SEARCH = "search";
	// private static final String OP_CREATECRITERIA = "createCriteriaQuery";

	@Inject
	@SystemProperty("auditoria")
	private String auditoria;

	@Inject
	private Logger logger;

	@Inject
	private SecurityService securityService;

	@Inject
	private MongoResource mongo;

	DateFormat dataFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

	@SuppressWarnings("unchecked")
	@AroundInvoke
	public Object logMethodEntry(InvocationContext ctx) throws Exception {

		logger.info("Logando operacoes save/update/remove/read.");
		String method = ctx.getMethod().getName();
		// Parameter[] methodParams = ctx.getMethod().getParameters();
		Object[] params = ctx.getParameters();

		AuditObject ao = new AuditObject();
		Funcionario func = securityService.getFuncionarioLogado();
		if (func != null) {
			ao.setUsuario(func.getNmLogin());
			ao.setNome(func.getNmFuncionario());
			ao.setIdUsuario(func.getId());
		} else if(params[0].getClass().getName().
				equals("br.com.medic.medicsystem.persistence.model.OrdemAgendamento") && method.equals(OP_PERSIST)){
			ao.setUsuario("TOTEM");
			ao.setNome("TOTEM");
			ao.setIdUsuario(4467L);
		} else if(params[0].getClass().getName().
				equals("br.com.medic.medicsystem.persistence.model.Agendamento") && method.equals(OP_PERSIST)) {
			Agendamento ag = (Agendamento) params[0];
			if(ag.getIdOperadorCadastro().equals(4733L) ) {
				ao.setUsuario("APLICATIVO");
				ao.setNome("APLICATIVO");
				ao.setIdUsuario(ag.getIdOperadorCadastro());
			} else {
				ao.setUsuario("WEB");
				ao.setNome("WEB");
				ao.setIdUsuario(ag.getIdOperadorCadastro());
			}
		} else {
			logger.warn("Log nao registrado, nenhum funcionario encontrado com o login autenticado____"+securityService.getUserLogin());
			return ctx.proceed();
		}

		//ao.setDataOperacao(dataFormat.format(Calendar.getInstance().getTime()));

		Object log = null;

		if (method.equals(OP_PERSIST)) {
			log = registerPersistOps(params[0], ao);
		} else if (method.equals(OP_UPDATE)) {
			log = registerUpdateOps(params[0], ao);
		} else if (method.equals(OP_REMOVE)) {
			log = registerRemoveOps(params[0], ao);
		}
		// else if (method.startsWith(OP_FIND) || method.startsWith(OP_GET)
		// || method.startsWith(OP_LOAD) || method.startsWith(OP_SEARCH)) {
		// log = registerReadOps(methodParams, params, ao);
		// } else if (method.equals(OP_CREATECRITERIA)) {
		// method.equals(OP_CREATECRITERIA);
		// }
		if ("true".equals(auditoria)) {
			HashMap<String, Object> result;
			try {
				ObjectMapper mapper = new ObjectMapper();
				ao.setObjeto(log);
				// Object to JSON in String
				String jsonInString = mapper.writeValueAsString(ao);

				result = new ObjectMapper().readValue(jsonInString,
						HashMap.class);
				if (ao.getOperacao() != "R") {
					mongo.saveDocument(new Document(result));
				}
				// else {
				// mongo.saveReadDocument(new Document(result));
				// }
			} catch (IOException e) {
				logger.error("Nao foi possivel registrar o log no mongo", e);
			}
		}

		return ctx.proceed();

	}

	// private Object registerReadOps(Parameter[] mParams, Object[] params,
	// AuditObject ao) {
	// ao.setOperacao("R");
	// QueryObject qo = new QueryObject();
	// List<String> pNames = new ArrayList<>();
	// for (Parameter object : mParams) {
	// pNames.add(object.getType().getName());
	// }
	// qo.setNames(pNames);
	// qo.setValues(params);
	// ao.setObjetoNome(QueryObject.class.getName());
	//
	// return qo;
	//
	// }

	private Object registerRemoveOps(Object object, AuditObject ao) {
		ao.setOperacao("D");
		ao.setObjetoNome(object.getClass().getName());
		return object;
	}

	private Object registerUpdateOps(Object object, AuditObject ao) {
		ao.setOperacao("U");
		ao.setObjetoNome(object.getClass().getName());
		try {
			if (ao.getIdUsuario() != null) {
				PropertyUtils.setProperty(object, "idOperadorAlteracao",
						ao.getIdUsuario());
			}			
		} catch (NoSuchMethodException | InvocationTargetException
				| IllegalAccessException et) {
			logger.warn("nao foi possivel registrar o operador que alterou o objeto, atributo idOperadorAlteracao nao encontrado");
		}
		try {
			PropertyUtils.setProperty(object, "dtAtualizacaoLog",
					new Timestamp(new Date().getTime()));
		} catch (NoSuchMethodException | InvocationTargetException
				| IllegalAccessException et) {
			logger.warn("nao foi possivel registrar a hora da alteracao do objeto, atributo dtAtualizacaoLog nao encontrado");
		}
		
		return object;
	}

	private Object registerPersistOps(Object object, AuditObject ao) {
		ao.setOperacao("C");
		ao.setObjetoNome(object.getClass().getName());
		try {
			if (ao.getIdUsuario() != null) {
				PropertyUtils.setProperty(object, "idOperadorCadastro",
						ao.getIdUsuario());
			}
		} catch (NoSuchMethodException | InvocationTargetException
				| IllegalAccessException et) {
			logger.warn("nao foi possivel registrar o operador que cadastrou o objeto, atributo idOperadorCadastro nao encontrado");
		}
		try {
			PropertyUtils.setProperty(object, "dtInclusaoLog", new Timestamp(
					new Date().getTime()));
		} catch (NoSuchMethodException | InvocationTargetException
				| IllegalAccessException et) {
			logger.warn("nao foi possivel registrar a hora da inclusao do objeto, atributo dtInclusaoLog nao encontrado");
		}

		return object;

	}
}
