package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tbtipoentsaicaixa database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbtipoentsaicaixa")
public class TipoEntSaiCaixa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIPOENTSAICAIXA_ID_SEQ")
	@SequenceGenerator(name = "TIPOENTSAICAIXA_ID_SEQ", sequenceName = "realvida.tipoentsaicaixa_id_seq",allocationSize = 1)
	@Column(name = "idtipoentsaicaixa")
	private Long idtipoentsaicaixa;

	private Timestamp dtatualizacaolog;

	@Temporal(TemporalType.DATE)
	private Date dtexclusao;

	private Timestamp dtinclusaolog;

	/** 0 - Entrada 1 - Saida */
	private Integer insaidacaixa;

	/**
	 *0 - Abertura
	 *1 - Troca de operador
	 *2 - Fechamento
	 *3 - Recebimento
	 *4 - Abastecimento
	 *5 - Retirada
	 *6 - Estorno de Pagamento
	 */
	private Integer intipooperacao;

	private String nmtipoentsaicaixa;

	//bi-directional many-to-one association to Tbmovimentacaocaixa
	@OneToMany(mappedBy="tipoEntSaiCaixa")
	private List<MovimentacaoCaixa> tbmovimentacaocaixas;

	public TipoEntSaiCaixa() {
	}

	public Long getIdtipoentsaicaixa() {
		return this.idtipoentsaicaixa;
	}

	public void setIdtipoentsaicaixa(Long idtipoentsaicaixa) {
		this.idtipoentsaicaixa = idtipoentsaicaixa;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtexclusao() {
		return this.dtexclusao;
	}

	public void setDtexclusao(Date dtexclusao) {
		this.dtexclusao = dtexclusao;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Integer getInsaidacaixa() {
		return this.insaidacaixa;
	}

	public void setInsaidacaixa(Integer insaidacaixa) {
		this.insaidacaixa = insaidacaixa;
	}

	public Integer getIntipooperacao() {
		return this.intipooperacao;
	}

	public void setIntipooperacao(Integer intipooperacao) {
		this.intipooperacao = intipooperacao;
	}

	public String getNmtipoentsaicaixa() {
		return this.nmtipoentsaicaixa;
	}

	public void setNmtipoentsaicaixa(String nmtipoentsaicaixa) {
		this.nmtipoentsaicaixa = nmtipoentsaicaixa;
	}

	public List<MovimentacaoCaixa> getTbmovimentacaocaixas() {
		return this.tbmovimentacaocaixas;
	}

	public void setTbmovimentacaocaixas(List<MovimentacaoCaixa> tbmovimentacaocaixas) {
		this.tbmovimentacaocaixas = tbmovimentacaocaixas;
	}

}