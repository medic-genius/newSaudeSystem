package br.com.medic.medicsystem.persistence.model;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbcontrato")
public class Contrato {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTRATO_ID_SEQ")
	@SequenceGenerator(name = "CONTRATO_ID_SEQ", sequenceName = "realvida.contrato_id_seq", allocationSize = 1)
	@Column(name = "idcontrato")
	private Long id;

	@Basic
	@Column(name = "bobloqueado")
	private Boolean boBloqueado;

	@Basic
	@Column(name = "bocadastradobancobrasil")
	private Boolean boCadastradoBancoBrasil;

	@Basic
	@Column(name = "boempresacliente")
	private Boolean boEmpresaCliente;

	@Basic
	@Column(name = "boisento")
	private Boolean boIsento;

	@Basic
	@Column(name = "bonaofazusoplano")
	private Boolean boNaoFazUsoPlano;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaolog;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtcontrato")
	private Date dtContrato;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtinativacao")
	private Date dtInativacao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtterminocontrato")
	private Date dtTerminoContrato;

	@Basic
	@Column(name = "dtterminovinculo")
	private Date dtTerminoVinculo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtvencimentoinicial")
	private Date dtVencimentoInicial;

	@Basic
	@Column(name = "idcontratoaux")
	private Long idContratoAux;

	@Basic
	@Column(name = "idcontratoodonto")
	private Long idContratoOdonto;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorinativacao")
	private Long idOperadorInativacao;

	@Basic
	@Column(name = "inexcluidoporalteracao")
	private Boolean inExcluidoPorAlteracao;

	@Basic
	@Column(name = "informapagamento")
	private Integer informaPagamento;

	@Basic
	@Column(name = "inmensalidade")
	private Integer inMensalidade;

	@Basic
	@Column(name = "inrenovado")
	private Integer inRenovado;

	@Basic
	@Column(name = "insituacao")
	private Integer situacao;

	@Basic
	@Column(name = "intipoinativacao")
	private Integer inTipoInativacao;

	@Basic
	@Column(name = "motivo")
	private String motivo;

	@Basic
	@Column(name = "nrconta")
	private String nrConta;

	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;

	@Basic
	@Column(name = "nrdigitoverificador")
	private String nrDigitoVerificador;

	@Basic
	@Column(name = "nrparcelaconvenio")
	private Integer nrParcelaConvenio;

	@Basic
	@Column(name = "nrtalao")
	private String nrTalao;

	@Basic
	@Column(name = "vlcontrato")
	private Double vlContrato;

	@Basic
	@Column(name = "vlcontratoantigo")
	private Double vlContratoAntigo;

	@Basic
	@Column(name = "vldesconto")
	private Double vlDesconto;

	@Basic
	@Column(name = "vlmultaatraso")
	private Double vlMultaAtraso;

	@Basic
	@Column(name = "vltaxajuros")
	private Double vlTaxaJuros;

	@Basic
	@Column(name = "vltotal")
	private Double vlTotal;
	
	@Basic
	@Column(name = "bonegociadoauto")
	private Boolean boNegociadoAuto;
	
	@Basic
	@Column(name = "dtnegociadoauto")
	private Date dtNegociadoAuto;

	@ManyToOne
	@JoinColumn(name = "idagencia")
	private Agencia agencia;

	@ManyToOne
	@JoinColumn(name = "idbanco")
	private Banco banco;

	@ManyToOne
	@JoinColumn(name = "idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	@ManyToOne
	@JoinColumn(name = "idplano")
	private Plano plano;

	@ManyToOne
	@JoinColumn(name = "idempresacliente")
	private EmpresaCliente empresaCliente;

	@ManyToOne
	@JoinColumn(name = "idrazao")
	private Razao razao;

	@ManyToOne
	@JoinColumn(name = "idvendedor")
	private Funcionario funcionario;
	
	@JsonIgnore
	@OneToMany(mappedBy = "contrato", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContratoCliente> contratosCliente;
	
	@JsonIgnore
	@OneToMany(mappedBy = "contrato", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContratoDependente> contratosDependente;
	
	@Basic
	@Column(name = "unidadevenda")
	private Integer unidadeVenda;
	
	@Basic
	@Column(name = "boinativadoconveniada")
	private Boolean boInativadoConveniada;
	
	@Basic
	@Column(name = "dtrenovacao")
	private Date dtRenovacao;
	
	@Basic
	@Column(name = "boacaoboleto")
	private Boolean boAcaoBoleto;

	@Transient
	@JsonIgnore
	private String planoFormatado;

	@Transient
	@JsonIgnore
	private String situacaoFormatado;

	@Transient
	@JsonIgnore
	private String tipoInativacaoFormatado;

	@Transient
	@JsonIgnore
	private String fazUsoPlanoFormatado;

	@Transient
	@JsonIgnore
	private String boBloqueadoFormatado;

	@Transient
	@JsonIgnore
	private String dtContratoFormatado;

	@Transient
	@JsonIgnore
	private String dtInativacaoFormatado;

	@Transient
	@JsonIgnore
	private String dtVencimentoInicialFormatado;
	
	@Transient
	@JsonIgnore
	private String vlTotalFormatado;
			
	@Transient
	@JsonIgnore
	private Boolean novo;
	
	@Transient
	@JsonProperty
	private Integer inTipoCliente;
	
	@Transient
	@JsonProperty
	private String nrMatricula;
	
	@Transient
	@JsonProperty
	private Long idOrgao;
	

	public Boolean getNovo() {
		return novo;
	}

	public void setNovo(Boolean novo) {
		this.novo = novo;
	}

	public Contrato() {
		this.contratosCliente = new ArrayList<ContratoCliente>();
		this.contratosDependente = new ArrayList<ContratoDependente>();
	}

	public Boolean getBoBloqueado() {
		return boBloqueado;
	}

	public void setBoBloqueado(Boolean boBloqueado) {
		this.boBloqueado = boBloqueado;
	}

	public Boolean getBoCadastradoBancoBrasil() {
		return boCadastradoBancoBrasil;
	}

	public void setBoCadastradoBancoBrasil(Boolean boCadastradoBancoBrasil) {
		this.boCadastradoBancoBrasil = boCadastradoBancoBrasil;
	}

	public Boolean getBoEmpresaCliente() {
		return boEmpresaCliente;
	}

	public void setBoEmpresaCliente(Boolean boEmpresaCliente) {
		this.boEmpresaCliente = boEmpresaCliente;
	}

	public Boolean getBoIsento() {
		return boIsento;
	}

	public void setBoIsento(Boolean boIsento) {
		this.boIsento = boIsento;
	}

	public Boolean getBoNaoFazUsoPlano() {
		return boNaoFazUsoPlano;
	}

	public void setBoNaoFazUsoPlano(Boolean boNaoFazUsoPlano) {
		this.boNaoFazUsoPlano = boNaoFazUsoPlano;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Timestamp getDtAtualizacaolog() {
		return dtAtualizacaolog;
	}

	public void setDtAtualizacaolog(Timestamp dtAtualizacaolog) {
		this.dtAtualizacaolog = dtAtualizacaolog;
	}

	public Date getDtContrato() {
		return dtContrato;
	}

	public void setDtContrato(Date dtContrato) {
		this.dtContrato = dtContrato;
	}

	public Date getDtInativacao() {
		return dtInativacao;
	}

	public void setDtInativacao(Date dtInativacao) {
		this.dtInativacao = dtInativacao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtTerminoContrato() {
		return dtTerminoContrato;
	}

	public void setDtTerminoContrato(Date dtTerminoContrato) {
		this.dtTerminoContrato = dtTerminoContrato;
	}

	public Date getDtTerminoVinculo() {
		return dtTerminoVinculo;
	}

	public void setDtTerminoVinculo(Date dtTerminoVinculo) {
		this.dtTerminoVinculo = dtTerminoVinculo;
	}

	public Date getDtVencimentoInicial() {
		return dtVencimentoInicial;
	}

	public void setDtVencimentoInicial(Date dtVencimentoInicial) {
		this.dtVencimentoInicial = dtVencimentoInicial;
	}

	public Long getIdContratoAux() {
		return idContratoAux;
	}

	public void setIdContratoAux(Long idContratoAux) {
		this.idContratoAux = idContratoAux;
	}

	public Long getIdContratoOdonto() {
		return idContratoOdonto;
	}

	public void setIdContratoOdonto(Long idContratoOdonto) {
		this.idContratoOdonto = idContratoOdonto;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorInativacao() {
		return idOperadorInativacao;
	}

	public void setIdOperadorInativacao(Long idOperadorInativacao) {
		this.idOperadorInativacao = idOperadorInativacao;
	}

	public Boolean getInExcluidoPorAlteracao() {
		return inExcluidoPorAlteracao;
	}

	public void setInExcluidoPorAlteracao(Boolean inExcluidoPorAlteracao) {
		this.inExcluidoPorAlteracao = inExcluidoPorAlteracao;
	}

	public Integer getInformaPagamento() {
		return informaPagamento;
	}

	public void setInformaPagamento(Integer informaPagamento) {
		this.informaPagamento = informaPagamento;
	}

	public Integer getInMensalidade() {
		return inMensalidade;
	}

	public void setInMensalidade(Integer inMensalidade) {
		this.inMensalidade = inMensalidade;
	}

	public Integer getInRenovado() {
		return inRenovado;
	}

	public void setInRenovado(Integer inRenovado) {
		this.inRenovado = inRenovado;
	}

	public Integer getInTipoInativacao() {
		return inTipoInativacao;
	}

	public void setInTipoInativacao(Integer inTipoInativacao) {
		this.inTipoInativacao = inTipoInativacao;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNrConta() {
		return nrConta;
	}

	public void setNrConta(String nrConta) {
		this.nrConta = nrConta;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNrDigitoVerificador() {
		return nrDigitoVerificador;
	}

	public void setNrDigitoVerificador(String nrDigitoVerificador) {
		this.nrDigitoVerificador = nrDigitoVerificador;
	}

	public Integer getNrParcelaConvenio() {
		return nrParcelaConvenio;
	}

	public void setNrParcelaConvenio(Integer nrParcelaConvenio) {
		this.nrParcelaConvenio = nrParcelaConvenio;
	}

	public String getNrTalao() {
		return nrTalao;
	}

	public void setNrTalao(String nrTalao) {
		this.nrTalao = nrTalao;
	}

	public Double getVlContrato() {
		return vlContrato;
	}

	public void setVlContrato(Double vlContrato) {
		this.vlContrato = vlContrato;
	}

	public Double getVlContratoAntigo() {
		return vlContratoAntigo;
	}

	public void setVlContratoAntigo(Double vlContratoAntigo) {
		this.vlContratoAntigo = vlContratoAntigo;
	}

	public Double getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(Double vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public Double getVlMultaAtraso() {
		return vlMultaAtraso;
	}

	public void setVlMultaAtraso(Double vlMultaAtraso) {
		this.vlMultaAtraso = vlMultaAtraso;
	}

	public Double getVlTaxaJuros() {
		return vlTaxaJuros;
	}

	public void setVlTaxaJuros(Double vlTaxaJuros) {
		this.vlTaxaJuros = vlTaxaJuros;
	}

	public Double getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Double vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public EmpresaCliente getEmpresaCliente() {
		return empresaCliente;
	}

	public void setEmpresaCliente(EmpresaCliente empresaCliente) {
		this.empresaCliente = empresaCliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Razao getRazao() {
		return razao;
	}

	public void setRazao(Razao razao) {
		this.razao = razao;
	}

	@JsonProperty
	public String getPlanoFormatado() {
		if (getPlano() != null) {
			return getPlano().getNmPlano();
		}

		if (getPlano() == null
		        && (getBoNaoFazUsoPlano() != null && getBoNaoFazUsoPlano() == false)) {
			return "PARTICULAR NAO ASSOCIADO";
		}

		return null;
	}

	@JsonProperty
	public String getDtVencimentoInicialFormatado() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (getDtVencimentoInicial() != null) {
			return sdf.format(getDtVencimentoInicial());
		}
		return null;
	}

	@JsonProperty
	public String getSituacaoFormatado() {
		if (getSituacao() != null) {
			if (getSituacao() == 0) {
				return "ATIVO";
			} else {
				return "INATIVO";
			}
		}
		return "-";
	}

	@JsonProperty
	public String getTipoInativacaoFormatado() {
		if (getInTipoInativacao() != null) {
			if (getInTipoInativacao() == 0) {
				return "EMPRESA";
			} else {
				return "CLIENTE";
			}
		}
		return "-";
	}

	@JsonProperty
	public String getFazUsoPlanoFormatado() {
		if (getBoNaoFazUsoPlano() != null) {
			if (getBoNaoFazUsoPlano() == true) {
				return "NAO";
			} else {
				return "SIM";
			}
		}
		return "-";
	}

	@JsonProperty
	public String getBoBloqueadoFormatado() {
		if (getBoBloqueado() != null) {
			if (getBoBloqueado() == true) {
				return "SIM";
			} else {
				return "NAO";
			}
		}
		return "-";
	}

	
	public List<ContratoCliente> getContratosCliente() {
		return contratosCliente;
	}

	public void setContratosCliente(List<ContratoCliente> contratosCliente) {
		this.contratosCliente = contratosCliente;
	}

	public ContratoCliente addContratoCliente(ContratoCliente contratoCliente) {
		contratoCliente.setContrato(this);
		getContratosCliente().add(contratoCliente);

		return contratoCliente;
	}
	
	
	public List<ContratoDependente> getContratosDependente() {
		return contratosDependente;
	}

	public void setContratosDependente(List<ContratoDependente> contratosDependente) {
		this.contratosDependente = contratosDependente;
	}
	
	public ContratoDependente addContratoDependente(ContratoDependente contratoDependente) {
		contratoDependente.setContrato(this);
		getContratosDependente().add(contratoDependente);

		return contratoDependente;
	}

	@JsonProperty
	public String getDtContratoFormatado() {
		if (getDtContrato() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtContrato());
		}
		return null;
	}

	@JsonProperty
	public String getDtInativacaoFormatado() {
		if (getDtInativacao() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtInativacao());
		}
		return null;
	}

	public Boolean getBoNegociadoAuto() {
		return boNegociadoAuto;
	}

	public void setBoNegociadoAuto(Boolean boNegociadoAuto) {
		this.boNegociadoAuto = boNegociadoAuto;
	}

	public Date getDtNegociadoAuto() {
		return dtNegociadoAuto;
	}

	public void setDtNegociadoAuto(Date dtNegociadoAuto) {
		this.dtNegociadoAuto = dtNegociadoAuto;
	}
	
	public String getVlTotalFormatado() {
		
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVlTotal() != null)
			vlTotalFormatado = currencyFormatter.format(getVlTotal());
				
		return vlTotalFormatado;
	}

	public void setVlTotalFormatado(String vlTotalFormatado) {
		this.vlTotalFormatado = vlTotalFormatado;
	}
	
	public Integer getInTipoCliente() {
		return inTipoCliente;
	}

	public void setInTipoCliente(Integer inTipoCliente) {
		this.inTipoCliente = inTipoCliente;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public Long getIdOrgao() {
		return idOrgao;
	}

	public void setIdOrgao(Long idOrgao) {
		this.idOrgao = idOrgao;
	}
	
	public Integer getUnidadeVenda() {
		return unidadeVenda;
	}

	public void setUnidadeVenda(Integer unidadeVenda) {
		this.unidadeVenda = unidadeVenda;
	}

	public Boolean getBoInativadoConveniada() {
		return boInativadoConveniada;
	}

	public void setBoInativadoConveniada(Boolean boInativadoConveniada) {
		this.boInativadoConveniada = boInativadoConveniada;
	}

	public Date getDtRenovacao() {
		return dtRenovacao;
	}

	public void setDtRenovacao(Date dtRenovacao) {
		this.dtRenovacao = dtRenovacao;
	}

	public Boolean getBoAcaoBoleto() {
		return boAcaoBoleto;
	}

	public void setBoAcaoBoleto(Boolean boAcaoBoleto) {
		this.boAcaoBoleto = boAcaoBoleto;
	}
}
