package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * Entidade que representa a folha de pagamento
 * de unidade financeiro
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

@Entity
@Table(catalog = "realvida", name = "tbimportafolha")
public class ImportaFolha implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORTAFOLHA_ID_SEQ")
	@SequenceGenerator(name = "IMPORTAFOLHA_ID_SEQ", catalog = "realvida", sequenceName = "IMPORTAFOLHA_ID_SEQ", allocationSize = 1)
	@Column(name = "idfolha")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "idunidadefinanceiro")
	private UnidadeFinanceiro unidadeFinanceiro;

	private String mesanoreferencia;

	private Integer nroimportados;

	private double somaacrescimos;

	private double somadescontos;

	private double somasalariobase;

	private double somavalorapagar;
	
	// salario real do periodo (sem proventos e descontos)
	@Basic
	@Column(name = "somasalariofolha")
	private double somasalarioFolha;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadOrcadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;
	
	@Basic
	@Column(name = "isfolhaavulsa")
	private Boolean isFolhaAvulsa;

	public ImportaFolha() {
	}

	public UnidadeFinanceiro getUnidadeFinanceiro() {
		return unidadeFinanceiro;
	}
	
	public void setUnidadeFinanceiro(UnidadeFinanceiro unidadeFinanceiro) {
		this.unidadeFinanceiro = unidadeFinanceiro;
	}

	public String getMesanoreferencia() {
		return this.mesanoreferencia;
	}

	public void setMesanoreferencia(String mesanoreferencia) {
		this.mesanoreferencia = mesanoreferencia;
	}

	public Integer getNroimportados() {
		return this.nroimportados;
	}

	public void setNroimportados(Integer nroimportados) {
		this.nroimportados = nroimportados;
	}

	public double getSomaacrescimos() {
		return this.somaacrescimos;
	}

	public void setSomaacrescimos(double somaacrescimos) {
		this.somaacrescimos = somaacrescimos;
	}

	public double getSomadescontos() {
		return this.somadescontos;
	}

	public void setSomadescontos(double somadescontos) {
		this.somadescontos = somadescontos;
	}

	public double getSomasalariobase() {
		return this.somasalariobase;
	}

	public void setSomasalariobase(double somasalariobase) {
		this.somasalariobase = somasalariobase;
	}

	public double getSomavalorapagar() {
		return this.somavalorapagar;
	}

	public void setSomavalorapagar(double somavalorapagar) {
		this.somavalorapagar = somavalorapagar;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadOrcadastro() {
		return idOperadOrcadastro;
	}

	public void setIdOperadOrcadastro(Long idOperadOrcadastro) {
		this.idOperadOrcadastro = idOperadOrcadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}
	
	public double getSomasalarioFolha() {
		return somasalarioFolha;
	}
	
	public void setSomasalarioFolha(double somasalarioFolha) {
		this.somasalarioFolha = somasalarioFolha;
	}
	
	public Boolean getIsFolhaAvulsa() {
		return isFolhaAvulsa;
	}
	
	public void setIsFolhaAvulsa(Boolean isFolhaAvulsa) {
		this.isFolhaAvulsa = isFolhaAvulsa;
	}

}