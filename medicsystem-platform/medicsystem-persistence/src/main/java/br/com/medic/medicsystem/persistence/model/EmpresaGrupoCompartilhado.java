package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "compartilhado", name = "empresagrupocompartilhado")
public class EmpresaGrupoCompartilhado implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDEMPRESAGRUPOCOMPARTILHADO_ID_SEQ")
	@SequenceGenerator(name = "IDEMPRESAGRUPOCOMPARTILHADO_ID_SEQ", sequenceName = "realvida.idempresagrupocompartilhado_id_seq", allocationSize = 1)
	@Column(name = "idempresagrupocompartilhado")
	private Long id;
	
	@Basic
	@Column(name = "idempresagrupoexterno")
	private Long idEmpresaGrupoExterno;
	
	@Basic
	@Column(name = "nmsistemaexterno")
	private String nmSistemaExterno;
	
	@Basic
	@Column(name = "insistemaexterno")
	private Integer inSistemaExterno; // 0 - saudesystem; 1 - elevesystem; 2 - dentalsystem	
	
	@ManyToOne
	@JoinColumn(name = "idempresafinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpresaGrupoExterno() {
		return idEmpresaGrupoExterno;
	}

	public void setIdEmpresaGrupoExterno(Long idEmpresaGrupoExterno) {
		this.idEmpresaGrupoExterno = idEmpresaGrupoExterno;
	}

	public String getNmSistemaExterno() {
		return nmSistemaExterno;
	}

	public void setNmSistemaExterno(String nmSistemaExterno) {
		this.nmSistemaExterno = nmSistemaExterno;
	}

	public EmpresaFinanceiro getEmpresaFinanceiro() {
		return empresaFinanceiro;
	}

	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Integer getInSistemaExterno() {
		return inSistemaExterno;
	}
	
	public void setInSistemaExterno(Integer inSistemaExterno) {
		this.inSistemaExterno = inSistemaExterno;
	}

}
