package br.com.medic.dashboard.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import br.com.medic.dashboard.persistence.dataaccess.MLRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.MonitoramentoAtendimento;

@Named("ml-monitoramento-atendimento-dao")
@ApplicationScoped
public class MonitoramentoAtendimentoDAO extends MLRelationalDataAccessObject<MonitoramentoAtendimento> {
	
	@SuppressWarnings("unchecked")
	public List<MonitoramentoAtendimento> getMonitorAtendimento(Integer tolerancia) {
		
		String query =
			"select *,"
			+ " case"
			+ " when (t1.hrpresenca + interval '1 hour') < current_time then 0"
			+ " else 1"
			+ " end as status"
			+ " from("
			+ " select cli.nmcliente as nmtitular,"
			+ " case"
			+ " when ag.iddependente is null then cli.nmcliente"
			+ " when ag.iddependente is not null then dep.nmdependente"
			+ " end as nmPaciente,"
			+ " ag.nragendamento, ag.dtagendamento, ag.hragendamento, ag.horapresenca as hrpresenca, "
			+ " atprof.hrordeminicio as hrchegadainicio, atprof.hrordemfim as hrchegadafim,"
			+ " f.nmfuncionario as nmprofissional, esp.nmespecialidade, u.nmapelido as nmunidade"
			+ " from realvida.tbagendamento ag"
			+ " inner join realvida.tbfuncionario f on f.idfuncionario = ag.idfuncionario"
			+ " inner join realvida.tbespecialidade esp on esp.idespecialidade = ag.idespecialidade"
			+ " inner join realvida.tbunidade u on u.idunidade = ag.idunidade"
			+ " left join realvida.tbatendimentoprofissional atprof on f.idfuncionario = atprof.idfuncionario"
			+ " and atprof.indiasemana = EXTRACT( DOW FROM ag.dtagendamento) and ag.hragendamento between atprof.hrinicio and atprof.hrfim + interval '1 minute'"
			+ " inner join realvida.tbcliente cli on cli.idcliente = ag.idcliente"
			+ " left join realvida.tbdependente dep on dep.iddependente = ag.iddependente"
			+ " where ag.dtagendamento = current_date"
			+ " and ag.dtexclusao is null"
			+ " and ag.instatus = 4" //Presente
			+ " and f.nmfuncionario not like 'DR.%'"
			+ " and esp.idespecialidade <> 83" //Estetica
			+ " order by ag.horapresenca"
			+ " )t1";
				
		
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmTitular", StringType.INSTANCE)
		        .addScalar("nmPaciente", StringType.INSTANCE)
		        .addScalar("nrAgendamento", StringType.INSTANCE)
		        .addScalar("dtAgendamento", DateType.INSTANCE)
		        .addScalar("nmProfissional", StringType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("nmUnidade", StringType.INSTANCE)
		        .addScalar("hrAgendamento", TimestampType.INSTANCE)
		        .addScalar("hrPresenca", TimestampType.INSTANCE)
		        .addScalar("hrChegadaInicio", TimestampType.INSTANCE)
		        .addScalar("hrChegadaFim", TimestampType.INSTANCE)
		        .addScalar("status", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(MonitoramentoAtendimento.class))
		        .list();
	}
	
}
