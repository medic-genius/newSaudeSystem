package br.com.medic.medicsystem.persistence.security.dto;

import br.com.medic.medicsystem.persistence.model.Funcionario;

public class AuditObject {

	private Long idUsuario;

	private String usuario;

	private String nome;

	private String dataOperacao;

	private String operacao;

	private Object objeto;

	private String objetoNome;
	
	private Funcionario funcionario;

	public AuditObject() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(String dataOperacao) {
		this.dataOperacao = dataOperacao;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public String getObjetoNome() {
		return objetoNome;
	}

	public void setObjetoNome(String objetoNome) {
		this.objetoNome = objetoNome;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	

}
