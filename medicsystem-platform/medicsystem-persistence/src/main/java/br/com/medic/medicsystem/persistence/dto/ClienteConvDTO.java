package br.com.medic.medicsystem.persistence.dto;

public class ClienteConvDTO {
	private Long idCliente;
	
	private String nmCliente;
	
	private String nrCPF;
	
	private Integer inSituacao;
	
	private String nrContrato;
	
	private Long idContrato;
	
	private Long idContratoCliente;
	
	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdContratoCliente() {
		return idContratoCliente;
	}

	public void setIdContratoCliente(Long idContratoCliente) {
		this.idContratoCliente = idContratoCliente;
	}

	public ClienteConvDTO() {
	}

	public ClienteConvDTO(Long idCliente, String nmCliente, String nrCPF) {
		this.idCliente = idCliente;
		this.nmCliente = nmCliente;
		this.nrCPF = nrCPF;
	}
	

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public Integer getInsituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	
	
	
	

}
