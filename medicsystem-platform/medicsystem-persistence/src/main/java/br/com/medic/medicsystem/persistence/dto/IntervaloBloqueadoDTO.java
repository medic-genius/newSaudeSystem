package br.com.medic.medicsystem.persistence.dto;

import java.util.Calendar;
import java.util.Date;

public class IntervaloBloqueadoDTO {

	private Date dtinicio;

	private Date dtfim;

	 public IntervaloBloqueadoDTO(Date dtinicio, Date dtfim) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtinicio);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		this.dtinicio = cal.getTime();
		
		cal.setTime(dtfim);
		
		this.dtfim = cal.getTime();
		
	}
	
	public IntervaloBloqueadoDTO() {
	}

	public Date getDtfim() {
		
		return dtfim;
	}

	public Date getDtinicio() {
		
		return dtinicio;
	}

	public void setDtfim(Date dtfim) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtfim);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.dtfim = cal.getTime();
	}

	public void setDtinicio(Date dtinicio) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtinicio);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.dtinicio = cal.getTime();
	}
}
