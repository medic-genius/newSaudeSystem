package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbservicoespecialidade")
public class ServicoEspecialidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "servicoespecialidade_id_seq")
	@SequenceGenerator(name = "servicoespecialidade_id_seq", sequenceName = "realvida.servicoespecialidade_id_seq")
	@Column(name = "idservicoespecialidade")
	private Long id;

	@Basic
	@Column(name = "boconsulta")
	private Boolean boConsulta;

	@Basic
	@Column(name = "bopadrao")
	private Boolean boPadrao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@ManyToOne
	@JoinColumn(name = "idespecialidade")
	private Especialidade especialidade;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	public ServicoEspecialidade() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoConsulta() {
		return boConsulta;
	}

	public void setBoConsulta(Boolean boConsulta) {
		this.boConsulta = boConsulta;
	}

	public Boolean getBoPadrao() {
		return boPadrao;
	}

	public void setBoPadrao(Boolean boPadrao) {
		this.boPadrao = boPadrao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}