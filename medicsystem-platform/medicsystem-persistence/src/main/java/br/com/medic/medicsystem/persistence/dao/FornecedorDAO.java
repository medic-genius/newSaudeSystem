package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Fornecedor;

@Named("fornecedor-dao")
@ApplicationScoped
public class FornecedorDAO extends RelationalDataAccessObject<Fornecedor> {

	public List<Fornecedor> getAllFornecedores(Boolean isInativo){
		
		if( isInativo == null )
			isInativo = false;
		
		String queryString = "SELECT f FROM Fornecedor f WHERE ";
		
		if( isInativo )
			queryString =  queryString + " f.dtExclusao is not null ";
		else
			queryString =  queryString + " f.dtExclusao is null ";
		
		queryString = queryString + " ORDER BY  f.nmNomeFantasia ASC";
			
		TypedQuery<Fornecedor> query = entityManager.createQuery(queryString, Fornecedor.class);
		List<Fornecedor> result = query.getResultList();
		return result;
	}

	public List<Fornecedor> getFornecedorByCPF(String nrCpf) {
		
		TypedQuery<Fornecedor> query = entityManager.createQuery( "SELECT f FROM Fornecedor f where f.nrCpf = '" + nrCpf + "'" , Fornecedor.class);

		List<Fornecedor> result = query.getResultList();

		if (result.isEmpty()) {
			return null;
		}

		return result;
		
		
	}
	
	private List<Fornecedor> getFornecedorByNrCNPJ(String nrCnpj) {
		
		TypedQuery<Fornecedor> query = entityManager.createQuery( "SELECT f FROM Fornecedor f where f.nrCnpj = '" + nrCnpj + "'" , Fornecedor.class);

		List<Fornecedor> result = query.getResultList();

		if (result.isEmpty()) {
			return null;
		}

		return result;
	}
	
	private List<Fornecedor> getFornecedorByNmRazaoSocial(String nmRazaoSocial) {
		TypedQuery<Fornecedor> query = entityManager.createQuery( "SELECT f FROM Fornecedor f where TRIM(f.nmRazaoSocial) = '" + nmRazaoSocial.trim() + "'" , Fornecedor.class);

		List<Fornecedor> result = query.getResultList();

		if (result.isEmpty()) {
			return null;
		}

		return result;
	}

	public boolean verificarCpfExistente(String nrCpf) {
		List<Fornecedor> fornecedores;
		
		fornecedores = getFornecedorByCPF( nrCpf );
		
		if( fornecedores == null )
			return false;
		else
			return true;
	}

	public boolean verificarCnjpExistente(String nrCnpj) {
		
		List<Fornecedor> fornecedores;
		
		fornecedores = getFornecedorByNrCNPJ( nrCnpj );
		
		if( fornecedores == null )
			return false;
		else
			return true;
		
	}

	public boolean verificarNmRazaoSocialExistente(String nmRazaoSocial) {
		
		List<Fornecedor> fornecedores;
		
		fornecedores = getFornecedorByNmRazaoSocial( nmRazaoSocial );
		
		if( fornecedores == null )
			return false;
		else
			return true;
	}

	
}
