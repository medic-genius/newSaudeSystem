package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;



@Entity
@Table(catalog = "realvida", name = "tbocorrenciadebitoautomatico")
public class OcorrenciaDebitoAutomatico implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCORRENCIADEBITOAUTOMATICO_ID_SEQ")
	@SequenceGenerator(name = "OCORRENCIADEBITOAUTOMATICO_ID_SEQ", sequenceName = "realvida.ocorrencia_id_seq")
	@Column(name = "idocorrencia")
	private Long id;
	
	@Basic
	@Column(name = "cdocorrencia")
	private String cdOcorrencia;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@Basic
	@Column(name = "intipo")
	private Integer inTipo;
	
	@Basic
	@Column(name = "nmocorrencia")
	private String nmOcorrencia;

	//bi-directional many-to-one association to Tbbanco
	@ManyToOne
	@JoinColumn(name="idbanco")
	private Banco banco;

	public OcorrenciaDebitoAutomatico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCdOcorrencia() {
		return cdOcorrencia;
	}

	public void setCdOcorrencia(String cdOcorrencia) {
		this.cdOcorrencia = cdOcorrencia;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public String getNmOcorrencia() {
		return nmOcorrencia;
	}

	public void setNmOcorrencia(String nmOcorrencia) {
		this.nmOcorrencia = nmOcorrencia;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}


}