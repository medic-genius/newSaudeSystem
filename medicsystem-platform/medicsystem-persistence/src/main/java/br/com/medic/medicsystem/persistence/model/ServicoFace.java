package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbservicoface database table.
 * 
 */
@Entity
public class ServicoFace implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBSERVICOFACE_IDSERVICOFACE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBSERVICOFACE_IDSERVICOFACE_GENERATOR")
	private Long idservicoface;

	private Boolean boarcada00;

	private Boolean bocervical;

	private Boolean bodente;

	private Boolean bodistal;

	private Boolean boinferior00;

	private Boolean boinferiordireito;

	private Boolean boinferioresquerdo;

	private Boolean bomesial;

	private Boolean booclusal;

	private Boolean bopalatina;

	private Boolean boraiz;

	private Boolean bosuperior00;

	private Boolean bosuperiordireito;

	private Boolean bosuperioresquerdo;

	private Boolean bovestibular;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	//bi-directional many-to-one association to Tbservico
	@ManyToOne
	@JoinColumn(name="idservico")
	private Servico servico;

	public ServicoFace() {
	}

	public Long getIdservicoface() {
		return this.idservicoface;
	}

	public void setIdservicoface(Long idservicoface) {
		this.idservicoface = idservicoface;
	}

	public Boolean getBoarcada00() {
		return this.boarcada00;
	}

	public void setBoarcada00(Boolean boarcada00) {
		this.boarcada00 = boarcada00;
	}

	public Boolean getBocervical() {
		return this.bocervical;
	}

	public void setBocervical(Boolean bocervical) {
		this.bocervical = bocervical;
	}

	public Boolean getBodente() {
		return this.bodente;
	}

	public void setBodente(Boolean bodente) {
		this.bodente = bodente;
	}

	public Boolean getBodistal() {
		return this.bodistal;
	}

	public void setBodistal(Boolean bodistal) {
		this.bodistal = bodistal;
	}

	public Boolean getBoinferior00() {
		return this.boinferior00;
	}

	public void setBoinferior00(Boolean boinferior00) {
		this.boinferior00 = boinferior00;
	}

	public Boolean getBoinferiordireito() {
		return this.boinferiordireito;
	}

	public void setBoinferiordireito(Boolean boinferiordireito) {
		this.boinferiordireito = boinferiordireito;
	}

	public Boolean getBoinferioresquerdo() {
		return this.boinferioresquerdo;
	}

	public void setBoinferioresquerdo(Boolean boinferioresquerdo) {
		this.boinferioresquerdo = boinferioresquerdo;
	}

	public Boolean getBomesial() {
		return this.bomesial;
	}

	public void setBomesial(Boolean bomesial) {
		this.bomesial = bomesial;
	}

	public Boolean getBooclusal() {
		return this.booclusal;
	}

	public void setBooclusal(Boolean booclusal) {
		this.booclusal = booclusal;
	}

	public Boolean getBopalatina() {
		return this.bopalatina;
	}

	public void setBopalatina(Boolean bopalatina) {
		this.bopalatina = bopalatina;
	}

	public Boolean getBoraiz() {
		return this.boraiz;
	}

	public void setBoraiz(Boolean boraiz) {
		this.boraiz = boraiz;
	}

	public Boolean getBosuperior00() {
		return this.bosuperior00;
	}

	public void setBosuperior00(Boolean bosuperior00) {
		this.bosuperior00 = bosuperior00;
	}

	public Boolean getBosuperiordireito() {
		return this.bosuperiordireito;
	}

	public void setBosuperiordireito(Boolean bosuperiordireito) {
		this.bosuperiordireito = bosuperiordireito;
	}

	public Boolean getBosuperioresquerdo() {
		return this.bosuperioresquerdo;
	}

	public void setBosuperioresquerdo(Boolean bosuperioresquerdo) {
		this.bosuperioresquerdo = bosuperioresquerdo;
	}

	public Boolean getBovestibular() {
		return this.bovestibular;
	}

	public void setBovestibular(Boolean bovestibular) {
		this.bovestibular = bovestibular;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Servico getTbservico() {
		return this.servico;
	}

	public void setServico(Servico tbservico) {
		this.servico = tbservico;
	}

}