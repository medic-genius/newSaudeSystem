package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ImportaFolhaFunc;

/**
 * Entidade que representa o persistence
 * da Entidade ImportaFolhaFun
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

@Named("importafolhafunc-dao")
@ApplicationScoped
public class ImportaFolhaFuncDAO extends RelationalDataAccessObject<ImportaFolhaFunc> {
	
	
//	public String getFileName( MultivaluedMap<String, String> header ) {	
//	
//		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
//		for (String filename : contentDisposition) {
//			if( filename.trim().startsWith("filename") ) {
//				
//				String[] name = filename.split("=");	
//				String finalFilename = name[1].trim().replaceAll("\"", "");
//				
//				return finalFilename;
//			}
//		}
//		
//		return "unknown";
//	}
//	
//	public void writeFile(byte[] content, String fileName) throws IOException {
//		
//		File file = new File( fileName );
//		
//		if( !file.exists() ) {
//			file.createNewFile();
//		}
//		
//		FileOutputStream fop = new FileOutputStream(file);
//		
//		fop.write(content);
//		fop.flush();
//		fop.close();
//		
//	}

}
