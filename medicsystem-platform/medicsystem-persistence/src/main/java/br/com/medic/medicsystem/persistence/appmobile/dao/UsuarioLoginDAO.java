package br.com.medic.medicsystem.persistence.appmobile.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioLogin;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioLoginPK;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;

@Named("usuario-login-dao")
@ApplicationScoped
public class UsuarioLoginDAO extends RelationalDataAccessObject<UsuarioLogin> {

	public UsuarioLogin getUsuarioLoginByPk(Long idUsuario, Integer tipoUsuario, String deviceId) {
		UsuarioLoginPK pk = new UsuarioLoginPK();
		pk.setIdUsuario(idUsuario);
		pk.setTipoUsuario(tipoUsuario);
		pk.setIdDevice(deviceId);
		try {
			return searchByKey(UsuarioLogin.class, pk);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<String> getUsuarioRegistrationIds(Long idUsuario, Integer tipoUsuario) {
		String queryStr = "SELECT u.notificationRegId FROM UsuarioLogin u WHERE u.idUsuario = :idUsuario "
				+ "AND u.tipoUsuario = :tipoUsuario";
		TypedQuery<String> query = entityManager.createQuery(queryStr, String.class)
				.setParameter("idUsuario", idUsuario)
				.setParameter("tipoUsuario", tipoUsuario);
		try {
			return query.getResultList();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
