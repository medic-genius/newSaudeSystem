package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.SMSDTO;
import br.com.medic.medicsystem.persistence.model.EnvioSMS;

@Named("enviosms-dao")
@ApplicationScoped
public class EnvioSmsDAO extends RelationalDataAccessObject<EnvioSMS> {
	
	public void persistSmsResponse(String idCampanha, String response) {
		String queryStr = "SELECT env FROM EnvioSMS env WHERE env.idCampanha = '" + idCampanha + "'";
		try {
			TypedQuery<EnvioSMS> query = entityManager.createQuery(queryStr, EnvioSMS.class);
			EnvioSMS envio = query.getSingleResult();
			if(envio != null) {
				String nResp = null;
				if(envio.getResposta() != null && !envio.getResposta().trim().isEmpty()) {
					nResp = envio.getResposta().trim() + ";" + response;
				} else {
					nResp = response;
				}
				envio.setResposta(nResp);
				this.update(envio);
			}
		} catch(NoResultException e) {
			System.out.println("---> Envio SMS não encontrado - idCampanha: " + idCampanha);
			System.out.println("");
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SMSDTO> getAgendamentosAtendidosProfissional(Long idProfissional, 
			Long idEspecialidade, Long idUnidade) {
		
//		Calendar calendar = Calendar.getInstance();
//		Date dataFim = calendar.getTime();
//		calendar.set(Calendar.MONTH, -1);
//		Date dataInicio = calendar.getTime();
		
//		String queryStr = "SELECT ag FROM AgendamentoView ag WHERE ag.envioSms IS NOT EMPTY"
//				+ " AND ag.profissional.id = " + idProfissional
//				+ " AND ag.inStatus = 2"
//				+ " AND ag.dtAgendamento BETWEEN :dataInicio AND :dataFim";
//		
//		if(idUnidade != null){
//			queryStr+= " AND ag.unidade.id = " + idUnidade;
//		}
//		
//		if(idEspecialidade != null){
//			queryStr+= " AND ag.especialidade.id = " + idEspecialidade;
//		}
		
		String queryStr = "select 	cli.idcliente, cli.nrcodcliente, cli.nmcliente, "
						+" dep.iddependente, dep.nrcodcliente as nrcoddependente, dep.nmdependente," 
						+" sms.resposta, sms.dtenvio"
					+" FROM realvida.tbagendamento ag"
					+" inner join realvida.tbenviosms sms on ag.idagendamento = sms.idagendamento "
					+" inner join realvida.tbcliente cli on cli.idcliente = ag.idcliente"
					+" left join realvida.tbdependente dep on dep.iddependente = ag.iddependente"
					+" WHERE ag.instatus = 2 "// Atendido
					+" and ag.dtexclusao is null"
					+" and sms.resposta is not null"
					+" and sms.intipomensagem = 7 "// Avaliacao de atendimento
					+" and ag.idfuncionario = "+idProfissional
					+" and ag.idespecialidade = "+idEspecialidade
					+" and ag.idunidade = "+idUnidade
					+" and ag.dtagendamento BETWEEN current_date - interval '1 month' AND current_date";
		
		List<SMSDTO> list = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
		        .addScalar("idCliente", LongType.INSTANCE)
		        .addScalar("nrCodCliente", StringType.INSTANCE)
		        .addScalar("nmCliente", StringType.INSTANCE)
		        .addScalar("idDependente", LongType.INSTANCE)
		        .addScalar("nrCodDependente", StringType.INSTANCE)
		        .addScalar("nmDependente", StringType.INSTANCE)
		        .addScalar("resposta", StringType.INSTANCE)
		        .addScalar("dtEnvio", DateType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(SMSDTO.class)).list();
		if(list != null && list.size() > 0) {
			return list;
		}
		return null;
	}
}
