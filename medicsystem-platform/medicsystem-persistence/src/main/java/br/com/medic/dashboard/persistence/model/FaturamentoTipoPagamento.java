package br.com.medic.dashboard.persistence.model;

import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FaturamentoTipoPagamento implements GraphWrapper {
	
	private Long idUnidade;
	
	private String nmUnidade;

	private String formapagamento;

	private Integer numerocontratos;

	private Double faturamento;

	private String faturamentoFormatado;

	public FaturamentoTipoPagamento() {
		// TODO Auto-generated constructor stub
	}

	public String getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}

	public Integer getNumerocontratos() {
		return numerocontratos;
	}

	public void setNumerocontratos(Integer numerocontratos) {
		this.numerocontratos = numerocontratos;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public void setFaturamentoFormatado(String faturamentoFormatado) {
		this.faturamentoFormatado = faturamentoFormatado;
	}

	public String getFaturamentoFormatado() {

		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getFaturamento() != null)
			faturamentoFormatado = currencyFormatter.format(getFaturamento());
		return faturamentoFormatado;

	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public String getGraphLabel() {
		return formapagamento;
	}

	@Override
	public Double getGraphValue() {
		return faturamento;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

}
