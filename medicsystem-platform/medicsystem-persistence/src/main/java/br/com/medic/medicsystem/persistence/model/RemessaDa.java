package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbremessada")
public class RemessaDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REMESSADA_ID_SEQ")
	@SequenceGenerator(name = "REMESSADA_ID_SEQ", sequenceName = "realvida.remessada_id_seq")
	@Column(name = "idremessada")
	private Long id;
	
	@Basic
	@Column(name = "boremessaantiga")
	private Boolean boRemessaAntiga;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtgeracao")
	private Date dtGeracao;
	
	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name = "idremessadaaux")
	private Long idRemessaDaAux;
	
	@Basic
	@Column(name = "nmarquivo")
	private String nmArquivo;
	
	@Basic
	@Column(name = "nmreferencia")
	private String nmReferencia;
	
	@Basic
	@Column(name = "nrsequencial")
	private String nrSequencial;
	
	@Basic
	@Column(name = "vltotal")
	private float vlTotal;

	//bi-directional many-to-one association to Tbmovremessada
	@OneToMany(mappedBy="remessaDa")
	private List<MovRemessaDa> movRemessaDas;

	//bi-directional many-to-one association to Tbbanco
	@ManyToOne
	@JoinColumn(name="idbanco")
	private Banco banco;

	//bi-directional many-to-one association to Tbempresagrupo
	@ManyToOne
	@JoinColumn(name="idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public RemessaDa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoRemessaAntiga() {
		return boRemessaAntiga;
	}

	public void setBoRemessaAntiga(Boolean boRemessaAntiga) {
		this.boRemessaAntiga = boRemessaAntiga;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtGeracao() {
		return dtGeracao;
	}

	public void setDtGeracao(Date dtGeracao) {
		this.dtGeracao = dtGeracao;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Long getIdRemessaDaAux() {
		return idRemessaDaAux;
	}

	public void setIdRemessaDaAux(Long idRemessaDaAux) {
		this.idRemessaDaAux = idRemessaDaAux;
	}

	public String getNmArquivo() {
		return nmArquivo;
	}

	public void setNmArquivo(String nmArquivo) {
		this.nmArquivo = nmArquivo;
	}

	public String getNmReferencia() {
		return nmReferencia;
	}

	public void setNmReferencia(String nmReferencia) {
		this.nmReferencia = nmReferencia;
	}

	public String getNrSequencial() {
		return nrSequencial;
	}

	public void setNrSequencial(String nrSequencial) {
		this.nrSequencial = nrSequencial;
	}

	public float getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(float vlTotal) {
		this.vlTotal = vlTotal;
	}

	public List<MovRemessaDa> getMovRemessaDas() {
		return movRemessaDas;
	}

	public void setMovRemessaDas(List<MovRemessaDa> movRemessaDas) {
		this.movRemessaDas = movRemessaDas;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public MovRemessaDa addMovRemessaDa(MovRemessaDa movRemessaDa) {
		getMovRemessaDas().add(movRemessaDa);
		movRemessaDa.setRemessaDa(this);

		return movRemessaDa;
	}

	public MovRemessaDa removeMovRemessaDa(MovRemessaDa movRemessaDa) {
		getMovRemessaDas().remove(movRemessaDa);
		movRemessaDa.setRemessaDa(null);

		return movRemessaDa;
	}

}