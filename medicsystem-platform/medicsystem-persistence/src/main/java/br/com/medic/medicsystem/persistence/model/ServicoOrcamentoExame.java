package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;





@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(catalog = "realvida", name = "tbservicoorcamentoexame")
public class ServicoOrcamentoExame implements Serializable {
	


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICOORCAMENTOEXAME_ID_SEQ")
	@SequenceGenerator(name = "SERVICOORCAMENTOEXAME_ID_SEQ", sequenceName = "realvida.servicoorcamentoexame_id_seq", allocationSize = 1)
	@Column(name = "idservicoorcamentoexame")
	private Long id;
	

	
	@ManyToOne
	@JoinColumn(name = "idorcamentoexame")
	private OrcamentoExame orcamentoExame;
	
	
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico Servico;
	
	
	@Basic
	@JoinColumn(name = "quantidade")
	private Integer quantidade;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrcamentoExame getOrcamentoExame() {
		return orcamentoExame;
	}

	public void setOrcamentoExame(OrcamentoExame orcamentoExame) {
		this.orcamentoExame = orcamentoExame;
	}

	public Servico getServico() {
		return Servico;
	}

	public void setServico(Servico servico) {
		Servico = servico;
	}


	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}


	

}
