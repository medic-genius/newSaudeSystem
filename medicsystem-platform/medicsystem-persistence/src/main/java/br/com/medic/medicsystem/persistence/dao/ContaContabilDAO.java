package br.com.medic.medicsystem.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.ContaContabilDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ContaContabil;


@Named("contacontabil-dao")
@ApplicationScoped
public class ContaContabilDAO extends RelationalDataAccessObject<ContaContabil>{

	public ArrayList<ContaContabil> getAllContasContabil(Long idContaContabil){
		String queryStr = "SELECT c FROM ContaContabil c ";
				if(idContaContabil != null && idContaContabil != 0) {
					queryStr += " WHERE c.dtExclusao is null ";
				} else {
					queryStr += " WHERE c.contaContabil is null "
							+ "AND c.dtExclusao is null ";
				}
				
				if(idContaContabil != null && idContaContabil != 0 ) {
					queryStr+= " AND c.id = :idContaContabil " ;
				}
				
				queryStr+= "ORDER BY c.nmContaContabil ASC";
				if(idContaContabil != null && idContaContabil !=0 ) {
					TypedQuery<ContaContabil> query = entityManager.createQuery(queryStr, ContaContabil.class)
							.setParameter("idContaContabil", idContaContabil);
					ArrayList<ContaContabil> list = (ArrayList<ContaContabil>) query.getResultList();
					return list;
				} else {
					TypedQuery<ContaContabil> query = entityManager.createQuery(queryStr, ContaContabil.class);
					ArrayList<ContaContabil> list = (ArrayList<ContaContabil>) query.getResultList();
					return list;
				}
		
		
		
	}
	
	//inStatus -> 0-Aberto 1-Pago 2-Atrasado 3-NãoPagas(0 e 2)
	@SuppressWarnings({ "unchecked" })
	public List<ContaContabilDTO> getRelatorioTodasContaContabil(int mesSplit, int anoSplit, Long idContaContabil, Long idContaPai, String idEmpresa, Integer tipoStatus) {
		String queryStr ="";
		if(idEmpresa != null && !idEmpresa.equals("0") && tipoStatus == 1) { // pago
			queryStr += " SELECT d.idempresafinanceirostart,cc.nmdescricao,cc.idcontacontabil, coalesce (SUM(d.vlpago),0) as totaldespesa ";
		}else if(idEmpresa != null && !idEmpresa.equals("0") && (tipoStatus == 0  || tipoStatus == 2) ){ // aberto,
			queryStr += "SELECT d.idempresafinanceirostart, cc.nmdescricao, cc.idcontacontabil, coalesce(SUM(d.vldespesa), 0) as totaldespesa";
		} else if(tipoStatus == 1){ //pago
			queryStr += "select cc.idcontacontabil, cc.nmdescricao, cc.contacontabil_idcontacontabil as idContaFilho, coalesce(sum(d.vlpago), 0) as totaldespesa ";
		} else if(tipoStatus == 0 || tipoStatus == 2){ // aberto,  todos
			queryStr += "select cc.idcontacontabil, cc.nmdescricao, cc.contacontabil_idcontacontabil as idContaFilho, coalesce(SUM(d.vldespesa), 0) as totaldespesa";
		} 
		
		queryStr += " from financeiro.tbcontacontabil cc "
					+ " left join financeiro.tbdespesa d on cc.idcontacontabil = d.idcontacontabil"
					+ " and d.dtexclusao is null ";
				if(tipoStatus == 0) {
					queryStr += " and d.instatus in (0,2)";
				}else if(tipoStatus == 1) {
					queryStr += " and d.instatus = 1";
				}else if( tipoStatus == 2) {
					queryStr += " and d.instatus in (0,1,2)";
				}
				
				if(idEmpresa != null && !idEmpresa.equals("0")) {
					queryStr += " and d.idempresafinanceirostart in (" + idEmpresa + ")";
				}
				
				if(tipoStatus == 1) {
					queryStr += " and Extract('Month' From d.dtpagamento  ) = " + mesSplit
							+ " and extract('Year' from d.dtpagamento) = " + anoSplit
							+ " where cc.dtexclusao is null";
				} else {
					queryStr += " and Extract('Month' From d.dtvencimento  ) = " + mesSplit 
							 + " and extract('Year' from d.dtvencimento) = " + anoSplit
							 + " where cc.dtexclusao is null";
				}
					
				if(idContaContabil != null && idContaContabil != 0) {
					queryStr += " and cc.idcontacontabil = " + idContaContabil; 
				}
				
				if(idEmpresa != null && !idEmpresa.equals("0")) {
					queryStr += " group by d.idempresafinanceirostart, cc.idcontacontabil ";
				} else {
					queryStr += " group by cc.idcontacontabil";
				}
				
							
			
				if( idEmpresa != null && !idEmpresa.equals("0")) {
					List<ContaContabilDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
							.addScalar("nmDescricao", StringType.INSTANCE)
							.addScalar("idContaContabil", LongType.INSTANCE)
							.addScalar("totalDespesa", DoubleType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(ContaContabilDTO.class)).list();
					return result;
				} else {
					List<ContaContabilDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("nmDescricao", StringType.INSTANCE)
							.addScalar("idContaContabil", LongType.INSTANCE)
							.addScalar("totalDespesa", DoubleType.INSTANCE)
							.addScalar("idContaFilho", LongType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(ContaContabilDTO.class)).list();
					return result;
				}
			
	}
	
	public List<ContaContabil> getAllContaContabil() {
		String queryStr = "SELECT c FROM ContaContabil c WHERE c.contaContabil is null AND c.dtExclusao is null ORDER BY c.nmContaContabil ASC";
		TypedQuery<ContaContabil> query = entityManager.createQuery(queryStr, ContaContabil.class);
		List<ContaContabil> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Double> getAnosDespesas() {

		
		String queryStr = "SELECT EXTRACT(YEAR FROM dtpagamento)"
				+ " FROM financeiro.tbdespesa "
				+ " WHERE dtexclusao is null "
				+ " AND instatus = 1 "
				+ " GROUP BY EXTRACT(YEAR FROM dtpagamento)"
				+ " ORDER BY EXTRACT(YEAR FROM dtpagamento) ASC";
		
		
		Query Tquery = entityManager.createNativeQuery(queryStr);
		
		
		try {
			List<Double> result = (List<Double>) Tquery.getResultList();

			return result;
		}catch(NoResultException e) {
			return null;
		}

	}
	
	public Double getAnoBaseDespesa() {
		String queryStr = " select extract( year from min(dtvencimento)) from financeiro.tbdespesa";
		
		Query Tquery = entityManager.createNativeQuery(queryStr);

		try {
			Double result = (Double) Tquery.getSingleResult();
			return result;
		} catch(NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings({ "unchecked"})
	public List<ContaContabilDTO> getRelatorioAnaliseHorizontal(Long idCC, String idEmpresa, String anoForm) {

		String queryStr = "";
		if(idEmpresa != null && !idEmpresa.equals("0")) {
			queryStr = " SELECT d.idempresafinanceirostart, cc.idcontacontabil, cc.nmdescricao, COALESCE(SUM(d.vlpago), 0) as totalDespesa ";
		} else {
			queryStr += "SELECT cc.idcontacontabil, cc.nmdescricao, coalesce(sum(d.vlpago),0) as totalDespesa ";
		}
		 queryStr += " FROM financeiro.tbcontacontabil cc "
				+ " LEFT JOIN financeiro.tbdespesa d on cc.idcontacontabil = d.idcontacontabil "
				+ " AND d.dtexclusao is null "
				+ "	AND d.instatus = 1";
				
			if(idEmpresa!= null && !idEmpresa.equals("0")) {
				queryStr += " AND d.idempresafinanceirostart in ( " + idEmpresa +" )";
			}
				queryStr += " AND extract('Year' from d.dtpagamento) = " + anoForm
				+ " WHERE cc.dtexclusao IS NULL ";
			if(idCC != null && idCC != 0) {
				queryStr += " and cc.idcontacontabil = " + idCC; 
			}
				
			if(idEmpresa !=null && !idEmpresa.equals("0")) {
				queryStr += "GROUP BY cc.idcontacontabil, d.idempresafinanceirostart ";
			}else {
				queryStr +=  " GROUP BY cc.idcontacontabil";
			}
				
		if(idEmpresa != null && !idEmpresa.equals("0")) {
			List<ContaContabilDTO> result = findByNativeQuery(queryStr)
					.unwrap(SQLQuery.class)
					.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
					.addScalar("nmDescricao", StringType.INSTANCE)
					.addScalar("idContaContabil", LongType.INSTANCE)
					.addScalar("totalDespesa", DoubleType.INSTANCE)
					.setResultTransformer(Transformers.aliasToBean(ContaContabilDTO.class)).list();
			
			
			return result;
		} else {
			List<ContaContabilDTO> result = findByNativeQuery(queryStr)
					.unwrap(SQLQuery.class)
					.addScalar("nmDescricao", StringType.INSTANCE)
					.addScalar("idContaContabil", LongType.INSTANCE)
					.addScalar("totalDespesa", DoubleType.INSTANCE)
					.setResultTransformer(Transformers.aliasToBean(ContaContabilDTO.class)).list();
			
			
			return result;
		}
		
	}


	public List<ContaContabil> getBuscaContasContabil(String  nmContaContabil, Integer startPosition, Integer maxResults) {
				CriteriaBuilder cb = entityManager.getCriteriaBuilder();
				CriteriaQuery<ContaContabil> criteria = cb.createQuery(ContaContabil.class);

				Root<ContaContabil> contaContabilRoot = criteria.from(ContaContabil.class);
				criteria.select(contaContabilRoot);

				Predicate p = cb.conjunction();
				
				if (nmContaContabil != null) {
					String where_cc ;

					if(nmContaContabil.startsWith("%") || nmContaContabil.endsWith("%")) {
						where_cc = nmContaContabil.toUpperCase();

					} else {
						where_cc = "%" + nmContaContabil.toLowerCase() + "%";
						
					}

					p = cb.and(p, cb.like(contaContabilRoot.get("nmContaContabil"), where_cc));
					
				}

				p = cb.and(p, cb.isNull(contaContabilRoot.get("dtExclusao")));
				criteria.where(p);
				criteria.orderBy(cb.asc(contaContabilRoot.get("nmContaContabil")));

				TypedQuery<ContaContabil> q = entityManager.createQuery(criteria);
				startPosition = startPosition == null ? 0 : startPosition;
				if(maxResults != null) {
					q.setMaxResults(maxResults);
				}
				q.setFirstResult(startPosition);
				
				List<ContaContabil> result = q.getResultList();

				if (result.isEmpty()) {
					throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
				}

				return result;
	}

	
	
}
