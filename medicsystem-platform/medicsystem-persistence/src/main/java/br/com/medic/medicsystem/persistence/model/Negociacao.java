package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(catalog = "realvida", name = "tbnegociacao")
public class Negociacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEGOCIACAO_ID_SEQ")
	@SequenceGenerator(name = "NEGOCIACAO_ID_SEQ", sequenceName = "realvida.negociacao_id_seq", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	// bi-directional many-to-one association to Tbdespesa
	@ManyToOne
	@JoinColumn(name = "iddespesa")
	private Despesa despesa;

	// bi-directional many-to-one association to Tbmensalidade
	@ManyToOne
	@JoinColumn(name = "idmensalidade")
	private Mensalidade mensalidade;

	// bi-directional many-to-one association to Tbparcela
	@ManyToOne
	@JoinColumn(name = "idparcela")
	private Parcela parcela;
	
	@Basic
	@Column(name = "bonegociadaauto")
	private Boolean boNegociadaAuto;

	public Negociacao() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Mensalidade getMensalidade() {
		return mensalidade;
	}

	public void setMensalidade(Mensalidade mensalidade) {
		this.mensalidade = mensalidade;
	}

	public Parcela getParcela() {
		return parcela;
	}

	public void setParcela(Parcela parcela) {
		this.parcela = parcela;
	}

	public Boolean getBoNegociadaAuto() {
		return boNegociadaAuto;
	}

	public void setBoNegociadaAuto(Boolean boNegociadaAuto) {
		this.boNegociadaAuto = boNegociadaAuto;
	}
	
	

}