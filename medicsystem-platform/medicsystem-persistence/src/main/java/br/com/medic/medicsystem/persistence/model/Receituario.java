package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbreceituario")
public class Receituario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECEITUARIO_ID_SEQ")
	@SequenceGenerator(name = "RECEITUARIO_ID_SEQ", sequenceName = "realvida.receituario_id_seq" , allocationSize = 1)
	@Column(name = "idreceituario")
	private Long id;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtreceituario")
	private Date dtReceituario;
	
	@Basic
	@Column(name = "nmreceituario")
	private String nmReceituario;

	//bi-directional many-to-one association to Tbatendimento
	@ManyToOne
	@JoinColumn(name="idatendimento")
	private Atendimento atendimento;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="idreceituario", insertable=true, updatable=true)
	private List<MedicamentoReceituario> medicamentos;
	
	private String recomendacoes;
	
	public Receituario() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtReceituario() {
		return dtReceituario;
	}

	public void setDtReceituario(Date dtReceituario) {
		this.dtReceituario = dtReceituario;
	}

	public String getNmReceituario() {
		return nmReceituario;
	}

	public void setNmReceituario(String nmReceituario) {
		this.nmReceituario = nmReceituario;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public List<MedicamentoReceituario> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<MedicamentoReceituario> medicamentos) {
		this.medicamentos = medicamentos;
		if(this.medicamentos != null) {
			for(MedicamentoReceituario med : this.medicamentos) {
				med.setIdReceituario(this.id);
			}
		}
	}
	
	public void updateMedicamentosList(List<MedicamentoReceituario> medicamentos) {
		this.medicamentos.clear();
		if(medicamentos != null) {
			this.medicamentos.addAll(medicamentos);
		}
	}

	public String getRecomendacoes() {
		return recomendacoes;
	}

	public void setRecomendacoes(String recomendacoes) {
		this.recomendacoes = recomendacoes;
	}
}