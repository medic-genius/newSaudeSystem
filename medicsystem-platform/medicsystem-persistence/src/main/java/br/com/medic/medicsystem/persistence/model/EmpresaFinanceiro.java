package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * Entidade que representa a empresa financeiro
 * que possui um conjunto de unidades financeiras, com
 * suas respectivas folhas de pagamento
 * 
 * @author Joelton
 * @since 28/03/2016
 * @lastoModification 30/05/2016
 * @version 1.4
 */

@Entity
@Table(catalog = "financeiro", name = "tbempresafinanceiro")
public class EmpresaFinanceiro implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPRESAFINANCEIRO_ID_SEQ")
	@SequenceGenerator(name = "EMPRESAFINANCEIRO_ID_SEQ", sequenceName = "financeiro.empresafinanceiro_id_seq", allocationSize = 1)
	@Column(name = "idempresafinanceiro")
	private Long idEmpresaFinanceiro;
	
	@Basic
	@Column(name = "nmfantasia")
	private String nmFantasia;
	
	@NotNull
	@Basic
	@Column(name = "nmrazaosocial")
	private String nmRazaoSocial;
	
	@Basic
	@Column(name = "nmnickname")
	private String nmNickName;
	
	@Basic
	@Column(name = "intipogrupo")
	private Integer inTipoGrupo;
	
	/*@OneToMany(mappedBy = "empresaFinanceiro", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<ContaBancaria> contasBancarias;*/
	
	@Basic
	private Boolean isEmpresaOficial;
	
	public Long getIdEmpresaFinanceiro() {
		return idEmpresaFinanceiro;
	}
	
	public void setIdEmpresaFinanceiro(Long idEmpresaFinanceiro) {
		this.idEmpresaFinanceiro = idEmpresaFinanceiro;
	}
	
	public String getNmFantasia() {
		return nmFantasia;
	}
	
	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}
	
	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}
	
	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}
	
	public String getNmNickName() {
		return nmNickName;
	}
	
	public void setNmNickName(String nmNickName) {
		this.nmNickName = nmNickName;
	}
	
	public Integer getInTipoGrupo() {
		return inTipoGrupo;
	}
	
	public void setInTipoGrupo(Integer inTipoGrupo) {
		this.inTipoGrupo = inTipoGrupo;
	}

	/*public List<ContaBancaria> getContasBancarias() {
		return contasBancarias;
	}
	
	public void setContasBancarias(List<ContaBancaria> contasBancarias) {
		this.contasBancarias = contasBancarias;
	}*/
	
	public Boolean getIsEmpresaOficial() {
		return isEmpresaOficial;
	}
	
	public void setIsEmpresaOficial(Boolean isEmpresaOficial) {
		this.isEmpresaOficial = isEmpresaOficial;
	}
	
}
