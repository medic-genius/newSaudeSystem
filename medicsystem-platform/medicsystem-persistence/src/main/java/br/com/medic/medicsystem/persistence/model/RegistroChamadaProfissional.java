package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tbregistrochamadaprofissional")
public class RegistroChamadaProfissional implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGISTROCHAMADAPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "REGISTROCHAMADAPROFISSIONAL_ID_SEQ", sequenceName = "realvida.registrochamadaprofissional_id_seq", allocationSize = 1)
	@Column(name = "idregistrochamadaprofissional")
	private Long id;
	
	
	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;
	
	
	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Funcionario funcionario;
	
	@Basic
	@Column(name = "dtchamada")
	private Date dataChamada;
	
	@Basic
	@Column(name = "hrchamada")
	private Time horaChamada;
	

	@ManyToOne
	@JoinColumn(name = "idagendamento")
	private Agendamento agendamento;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Unidade getUnidade() {
		return unidade;
	}
	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Date getDataChamada() {
		return dataChamada;
	}
	public void setDataChamada(Date dataChamada) {
		this.dataChamada = dataChamada;
	}
	public Time getHoraChamada() {
		return horaChamada;
	}
	public void setHoraChamada(Time horaChamada) {
		this.horaChamada = horaChamada;
	}
	public Agendamento getAgendamento() {
		return agendamento;
	}
	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	
	
	
	

}
