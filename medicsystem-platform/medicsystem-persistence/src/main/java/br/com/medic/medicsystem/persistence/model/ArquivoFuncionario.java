package br.com.medic.medicsystem.persistence.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Joelton
 * @since  19/04/2016
 * @version 1.0
 * 
 * last modification  09/05/2016
 * by Joelton Matos
 */
@Entity
@Table(catalog = "financeiro", name = "tbarquivofuncionario")
public class ArquivoFuncionario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARQUIVOFUNCIONARIO_ID_SEQ")
	@SequenceGenerator(name = "ARQUIVOFUNCIONARIO_ID_SEQ", sequenceName = "financeiro.arquivofuncionario_id_seq",allocationSize = 1)
	@Column(name = "idarquivofuncionario")
	private Long idArquivoFuncionario;
		
	@Basic
	@Column(name = "nome")
	private String nome;
		
	@Basic
	@Column(name = "extensao")
	private String extensao;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dataUpload")
	private Date dataUpload;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;
	
	@Basic
	@Column(name = "url")
	private String url;

	
//GETTERS AND SETTERS
	public Long getIdArquivoFuncionario() {
		return idArquivoFuncionario;
	}
	public void setIdArquivoFuncionario(Long idArquivoFuncionario) {
		this.idArquivoFuncionario = idArquivoFuncionario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public Date getDataUpload() {
		return dataUpload;
	}
	public void setDataUpload(Date dataUpload) {
		this.dataUpload = dataUpload;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}	
}
