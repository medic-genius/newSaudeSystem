package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog="realvida", name="renovacaocontrato_view")
public class ViewRenovacaoContrato {
	@Id
	@Column(name="idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name="nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name="dtterminocontrato")
	private Date dtTerminoContrato;
	
	@Basic
	@Column(name="informapagamento")
	private Long inFormaPagamento;
	
	@Basic
	@Column(name="idcliente")
	private Long idCliente;
	
	@Basic
	@Column(name="nmcliente")
	private String nmCliente;
	
	@Basic
	@Column(name="idempresagrupo")
	private Long idEmpresaGrupo;
	
	@Basic
	@Column(name="nmfantasia")
	private String nmFantasia;
	
	@Basic
	@Column(name="idplano")
	private Long idPlano;
	
	@Basic
	@Column(name="nmplano")
	private String nmPlano;
	
	@Basic
	@Column(name="nrqtdemensalidade")
	private Integer nrqtdemensalidade;
	
	@Basic
	@Column(name="qtdcliente")
	private Integer qtdCliente;
	
	@Basic
	@Column(name="qtddependente")
	private Integer qtdDependente;
	
	@Basic
	@Column(name="vlplano")
	private Double vlPlano;

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public Long getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Long inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	
	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Integer getNrqtdemensalidade() {
		return nrqtdemensalidade;
	}

	public void setNrqtdemensalidade(Integer nrqtdemensalidade) {
		this.nrqtdemensalidade = nrqtdemensalidade;
	}

	public Date getDtTerminoContrato() {
		return dtTerminoContrato;
	}

	public void setDtTerminoContrato(Date dtTerminoContrato) {
		this.dtTerminoContrato = dtTerminoContrato;
	}

	public Long getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}

	public Integer getQtdCliente() {
		return qtdCliente;
	}

	public void setQtdCliente(Integer qtdCliente) {
		this.qtdCliente = qtdCliente;
	}

	public Integer getQtdDependente() {
		return qtdDependente;
	}

	public void setQtdDependente(Integer qtdDependente) {
		this.qtdDependente = qtdDependente;
	}

	public Double getVlPlano() {
		return vlPlano;
	}

	public void setVlPlano(Double vlPlano) {
		this.vlPlano = vlPlano;
	}
}
