package br.com.medic.medicsystem.persistence.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = false)
public class ObjectAlreadyExistsException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 312395602966689439L;

	public ObjectAlreadyExistsException(String message) {
		super(message);
	}
	
	public ObjectAlreadyExistsException(RuntimeException rte) {
		super(rte);
	}
	
	public ObjectAlreadyExistsException(String message, RuntimeException rte) {
		super(message, rte);
	}
}
