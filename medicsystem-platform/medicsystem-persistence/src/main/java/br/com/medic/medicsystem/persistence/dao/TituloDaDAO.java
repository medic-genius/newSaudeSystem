package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.TituloDa;

@Named("tituloda-dao")
@ApplicationScoped
public class TituloDaDAO extends RelationalDataAccessObject<TituloDa> {

	public TituloDa getTituloDa(Long id) {

		return searchByKey(TituloDa.class, id);
	}

	public TituloDa getTituloPorParcela(Long idparcela) {
		String queryStr = "SELECT t FROM TituloDa t " + " WHERE "
		        + " t.parcela.id = " + idparcela;

		TypedQuery<TituloDa> query = entityManager.createQuery(queryStr,
		        TituloDa.class);
		query.setMaxResults(1);
		TituloDa result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public String getNumeracaoTitulo() {

		String query = "select lpad(cast((max(cast(nrtitulo as int8))+1) as varchar), 10, '0') as nrtitulo from realvida.tbtituloda;";

		return (String) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .addScalar("nrtitulo", StringType.INSTANCE).uniqueResult();
	}
}
