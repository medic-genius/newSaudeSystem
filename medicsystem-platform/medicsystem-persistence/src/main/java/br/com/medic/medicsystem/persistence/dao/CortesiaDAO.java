package br.com.medic.medicsystem.persistence.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Cortesia;


@Named("cortesia-dao")
@ApplicationScoped
public class CortesiaDAO extends RelationalDataAccessObject<Cortesia>{
	
	public List<Cortesia> getAllCortesia(Integer inStatusCortesia, Boolean boUtilizada, Date dtInicio, Date dtFim){
		String query = "SELECT c FROM Cortesia c"
				+ " WHERE 1=1 ";
		
		if(inStatusCortesia != null){
			query += " AND c.inStatusCortesia = :inStatusCortesia";
		}
		
		if(boUtilizada != null){
			query += " AND c.boUtilizada IS "+ boUtilizada;
		}
		
		if(dtInicio != null) {
			query += " AND c.dtInclusaolog >= :dtInicio ";
		}
		if(dtFim != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(dtFim);
			c.add(Calendar.DAY_OF_MONTH, 1);
			dtFim = c.getTime();
			query += " AND c.dtInclusaolog < :dtFim ";
		}
		
		TypedQuery<Cortesia> result = entityManager
		        .createQuery(query, Cortesia.class);
		
		if(inStatusCortesia != null){
			result.setParameter("inStatusCortesia", inStatusCortesia);
		}
		if(dtInicio != null) {
			result.setParameter("dtInicio", dtInicio);
		}
		if(dtFim != null) {
			result.setParameter("dtFim", dtFim);
		}
		
		try {
			List<Cortesia> listResult = result.getResultList();
			return listResult;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
	}
	
}
