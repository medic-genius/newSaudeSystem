package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.DespesaGuiaServico;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.GuiaServico;

@Named("despesaguiaservico-dao")
@ApplicationScoped
public class DespesaGuiaServicoDAO extends RelationalDataAccessObject<DespesaGuiaServico> {
	
	public GuiaServico getGuiaServicobyDespesa(Long idDespesa){
				
		String strQuery = "SELECT dgs.guiaServico FROM DespesaGuiaServico dgs "
				+ " WHERE dgs.dtExclusao is null "
				+ " and dgs.despesa.id = " + idDespesa;		
		try {
			
			TypedQuery<GuiaServico> query = entityManager.createQuery(strQuery, GuiaServico.class);
			
			query.setMaxResults(1);
			
			GuiaServico result = query.getSingleResult();
			
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<DespesaGuiaServico> getDespesaGuiaServicobyDespesa(Long idDespesa){
		
		String strQuery = "SELECT dgs FROM DespesaGuiaServico dgs "
				+ " WHERE dgs.despesa.id = " + idDespesa;		
		try {
			
			TypedQuery<DespesaGuiaServico> query = entityManager.createQuery(strQuery, DespesaGuiaServico.class);
									
			List<DespesaGuiaServico> result = query.getResultList();
			
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}

	public DespesaGuiaServico getDespesaGuiaServicoByGuiaServico(Long idGuiaServico)
	{
		String strQuery = "SELECT despesaGuiaServico FROM DespesaGuiaServico despesaGuiaServico"
				+ " WHERE despesaGuiaServico.dtExclusao is null "
				+ " and despesaGuiaServico.guiaServico.id = " + idGuiaServico;		
		try {
			TypedQuery<DespesaGuiaServico> query = entityManager.createQuery(strQuery, DespesaGuiaServico.class);
			query.setMaxResults(1);
			DespesaGuiaServico despesaGuiaServico = query.getSingleResult();
			
			return despesaGuiaServico;
		} catch (Exception exception) {
//			System.out.println(exception.getMessage());
		}
		
		return null;
	}
	
	public Guia getGuiaByDespesaServico(DespesaServico despesaServico)
	{
		String strQuery = "SELECT despesaGuiaServico.guiaServico.guia FROM DespesaGuiaServico despesaGuiaServico "
				+ " WHERE despesaGuiaServico.despesa.id = " + despesaServico.getDespesa().getId() + ""
				+ " and despesaGuiaServico.guiaServico.servico.id = " + despesaServico.getServico().getId();		
		try {
			TypedQuery<Guia> query = entityManager.createQuery(strQuery, Guia.class);
			query.setMaxResults(1);
			Guia guia = query.getSingleResult();
			
			return guia;
		} catch (Exception exception) {
//			exception.printStackTrace();
		}
		
		return null;
	
	}

}
