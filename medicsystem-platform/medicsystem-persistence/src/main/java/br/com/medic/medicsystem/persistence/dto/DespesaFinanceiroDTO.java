package br.com.medic.medicsystem.persistence.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DespesaFinanceiroDTO {
	
	private Long idEmpresaFinanceiroStart;
	private Long idDespesa;
	private String nmObservacao;
	private String nrDocumento;
	private BigDecimal valorAP;
	private String nmDescricao;
	private Long idContaContabil;
	private Long idCentroCusto;
	private String dtPagamento;
	private Integer inStatus;
	private String dtVencimento;
	private String nomeFornecedor;
	private String nmCentroCusto;
	private Integer nrQuantidadeParcelas;
	private Integer nrOrdemParcela;
	private String nmDespesa;
	private String dtExclusao;
	private String nmFantasia;
	private Integer inFormaPagamento;
	
	public String getNmFantasia() {
		return nmFantasia;
	}
	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}
	public Integer getNrQuantidadeParcelas() {
		return nrQuantidadeParcelas;
	}
	public void setNrQuantidadeParcelas(Integer nrQuantidadeParcelas) {
		this.nrQuantidadeParcelas = nrQuantidadeParcelas;
	}
	public Integer getNrOrdemParcela() {
		return nrOrdemParcela;
	}
	public void setNrOrdemParcela(Integer nrOrdemParcela) {
		this.nrOrdemParcela = nrOrdemParcela;
	}
	public String getNmCentroCusto() {
		return nmCentroCusto;
	}
	public void setNmCentroCusto(String nmCentroCusto) {
		this.nmCentroCusto = nmCentroCusto;
	}
	public String getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public Integer getInStatus() {
		return inStatus;
	}
	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}
	public String getDtPagamento() {
		return dtPagamento;
	}
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public Long getIdEmpresaFinanceiroStart() {
		return idEmpresaFinanceiroStart;
	}
	public void setIdEmpresaFinanceiroStart(Long idEmpresaFinanceiroStart) {
		this.idEmpresaFinanceiroStart = idEmpresaFinanceiroStart;
	}
	public Long getIdDespesa() {
		return idDespesa;
	}
	public void setIdDespesa(Long idDespesa) {
		this.idDespesa = idDespesa;
	}
	public String getNmObservacao() {
		return nmObservacao;
	}
	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}
	public String getNrDocumento() {
		return nrDocumento;
	}
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	public BigDecimal getValorAP() {
		return valorAP;
	}
	public void setValorAP(BigDecimal valorAP) {
		this.valorAP = valorAP.setScale(2, RoundingMode.HALF_EVEN);
	}
	public String getNmDescricao() {
		return nmDescricao;
	}
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	public Long getIdContaContabil() {
		return idContaContabil;
	}
	public void setIdContaContabil(Long idContaContabil) {
		this.idContaContabil = idContaContabil;
	}
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}
	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}
	public Long getIdCentroCusto() {
		return idCentroCusto;
	}
	public void setIdCentroCusto(Long idCentroCusto) {
		this.idCentroCusto = idCentroCusto;
	}
	public String getNmDespesa() {
		return nmDespesa;
	}
	public void setNmDespesa(String nmDespesa) {
		this.nmDespesa = nmDespesa;
	}
	public Object getDtExclusao() {
		return dtExclusao;
	}
	public void setDtExclusao(String dtExclusao) {
		this.dtExclusao = dtExclusao;
		
	}
	
	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}
	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}
	


}
