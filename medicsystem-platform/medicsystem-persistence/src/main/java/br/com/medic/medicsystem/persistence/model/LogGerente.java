package br.com.medic.medicsystem.persistence.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbloggerente")
public class LogGerente {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOGGERENTE_ID_SEQ")
	@SequenceGenerator(name = "LOGGERENTE_ID_SEQ", sequenceName = "realvida.loggerente_id_seq", allocationSize = 1)
	@Column(name = "idloggerente")
	private Long id;

	@Basic
	@Column(name = "idcliente", updatable = false)
	private Long idCliente;

	@Basic
	@Column(name = "idcontrato")
	private Long idContrato;

	@Basic
	@Column(name = "idplano")
	private Long idPlano;

	@Basic
	@Column(name = "idfuncionario")
	private Long idFuncionario;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;

	@Basic
	@Column(name = "observacao")
	private String observacao;

	@Basic
	@Column(name = "dtacao")
	private Date dtAcao;

	@Basic
	@Column(name = "hracao")
	private Time hrAcao;

	@Basic
	@Column(name = "acao")
	private Integer acao; // 1-Encaixe; 2-Desbloqueio; 3-Exclusao Despesa;
	                      // 4-Exclusao Mensalidade; 5- Quitar Despesa; 6-Quitar
	                      // Mensalidade; 7-Retorno; 8-Liberação por quantidade diária

	@Basic
	@Column(name = "iddocumento")
	private Long idDocumento;

	public LogGerente() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDtAcao() {
		return dtAcao;
	}

	public void setDtAcao(Date dtAcao) {
		this.dtAcao = dtAcao;
	}

	public Time getHrAcao() {
		return hrAcao;
	}

	public void setHrAcao(Time hrAcao) {
		this.hrAcao = hrAcao;
	}

	public Integer getAcao() {
		return acao;
	}

	public void setAcao(Integer acao) {
		this.acao = acao;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}
}
