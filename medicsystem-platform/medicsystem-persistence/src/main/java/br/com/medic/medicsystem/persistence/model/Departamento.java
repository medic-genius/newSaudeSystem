package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the tbdepartamento database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbdepartamento")
public class Departamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPARTAMENTO_ID_SEQ")
	@SequenceGenerator(name = "DEPARTAMENTO_ID_SEQ", sequenceName = "realvida.departamento_id_seq")
	@Column(name = "iddepartamento")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Date dtatualizacaolog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Date dtinclusaolog;

	@Basic
	@Column(name = "nmdepartamento")
	private String nmdepartamento;

	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;

	public Departamento() {
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public String getNmdepartamento() {
		return this.nmdepartamento;
	}

	public void setNmdepartamento(String nmdepartamento) {
		this.nmdepartamento = nmdepartamento;
	}

	public Unidade getTbunidade() {
		return this.unidade;
	}

	public void setUnidade(Unidade tbunidade) {
		this.unidade = tbunidade;
	}

	public Date getDtatualizacaolog() {
		return dtatualizacaolog;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtinclusaolog() {
		return dtinclusaolog;
	}

	public void setDtinclusaolog(Date dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setDtatualizacaolog(Date dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

}