package br.com.medic.medicsystem.persistence.security;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

/**
 * http://www.mballem.com/
 */
public class MongoConnection {

    private static final String HOST = "45.79.150.191";
    private static final int PORT = 27017;
    private static final String DB_NAME = "saudehistorico";
    private static final String user = "saude";        // the user name
    private static final String database = "saudehistorico";    // the name of the database in which the user is defined
    private static final char[] password = "1cCzEFipHMwR".toCharArray();    // the password as a character array
    private static MongoClient mongoClient;
    private static MongoDatabase mongoDatabase;

    private MongoConnection() {
        //construtor privado
    }

    //garante sempre uma unica instancia
    public static synchronized MongoClient getInstance() {
        if (mongoClient == null) {
          	MongoCredential credential = MongoCredential.createCredential(user,
                    database,
                    password);
    		List<MongoCredential> mongoCredentials = new ArrayList<>();
    		mongoCredentials.add(credential);
    		MongoClientOptions.Builder options = MongoClientOptions.builder();
    		options.socketKeepAlive(true);
    		ServerAddress sad = new ServerAddress(HOST, PORT);
    		
        	mongoClient = new MongoClient(sad,mongoCredentials,options.build());
        	
        	
        }
        return mongoClient;
    }

}