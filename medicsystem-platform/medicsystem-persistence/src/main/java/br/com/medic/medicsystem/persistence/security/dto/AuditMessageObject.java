package br.com.medic.medicsystem.persistence.security.dto;

public class AuditMessageObject {	
	private Long idCliente;
	
	private String nrTelefone;
	
	private Integer tipoEnvio;
	
	private Integer tipoMensagem;
	
	private String conteudoMensagem;
	
	private Boolean enviado;
	
	private String dataOperacao;

	private String objetoNome;
	
	public AuditMessageObject() {
		this.objetoNome = this.getClass().getName();
	}
	

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public Integer getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(Integer tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public Integer getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(Integer tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public String getConteudoMensagem() {
		return conteudoMensagem;
	}

	public void setConteudoMensagem(String conteudoMensagem) {
		this.conteudoMensagem = conteudoMensagem;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	public String getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(String dataOperacao) {
		this.dataOperacao = dataOperacao;
	}

	public String getObjetoNome() {
		return objetoNome;
	}

	public void setObjetoNome(String objetoNome) {
		this.objetoNome = objetoNome;
	}
}
