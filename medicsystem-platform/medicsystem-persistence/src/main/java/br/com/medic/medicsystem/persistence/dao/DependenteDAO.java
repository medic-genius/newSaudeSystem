package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.appmobile.dto.ClienteInfoDTO;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Dependente;

@Named("dependente-dao")
@ApplicationScoped
public class DependenteDAO extends RelationalDataAccessObject<Dependente> {

	public List<Dependente> getDependentesByCliente(Long idCliente){
		
		String queryString = "SELECT d FROM Dependente d "
				+ " WHERE d.dtExclusao is null AND d.cliente.id = " + idCliente;
		
		TypedQuery<Dependente> query = entityManager.createQuery(queryString, Dependente.class);
		List<Dependente> result = query.getResultList();
		
		return result;
	}
	
	public List<Dependente> getDependentesClienteByContrato(Long idCliente, Long idContrato){
		
		String queryString = "SELECT d FROM Dependente d, ContratoDependente c "
				+ " WHERE c.dependente.id = d.id AND d.cliente.id = " + idCliente + " AND c.contrato.id = " + idContrato + " AND c.inSituacao = " + 0;
		
		TypedQuery<Dependente> query = entityManager.createQuery(queryString, Dependente.class);
		List<Dependente> result = query.getResultList();
		
		return result;
	}
	
	public Long getCountDependentesByCliente(Long idCliente){
		
		String queryString = "SELECT count(d) FROM Dependente d"
				+ " WHERE d.cliente.id = " + idCliente;
		
		TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
		Long result = query.getSingleResult();
		return result;
	}
	
	
	public Long getQuantidadeDependentesClienteByContrato(Long idCliente, Long idContrato){
		
		if(idCliente ==  null || idContrato == null)
			return null;
		
		String queryString = "SELECT count(d) FROM Dependente d, ContratoDependente c "
				+ " WHERE c.dependente.id = d.id AND d.cliente.id = " + idCliente + " AND c.contrato.id = " + idContrato + " AND c.inSituacao = " + 0;
		
		TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
		Long result = query.getSingleResult();
		
		return result;
	}
	
	public List<Dependente> getDependentesFromClienteList(List<Long> clienteIds) {
		String queryString = "SELECT d FROM Dependente d "
				+ "WHERE d.dtExclusao IS NULL AND d.cliente.id IN :listIds ";
		try {
			TypedQuery<Dependente> query = entityManager.createQuery(queryString, Dependente.class)
					.setParameter("listIds", clienteIds);
			return query.getResultList();
		} catch(Exception e) {
			return null;
		}
	}
	
	//---------------------------------------------------------------------
	// Aplicativo
	
	public Dependente getDependenteByNameAndDtNascimento(String name, Date dtNascimento) {
		String queryStr = "SELECT dep FROM Dependente dep WHERE dep.nmDependente = :name AND dep.dtNascimento = :dtNasc";
		try {
			TypedQuery<Dependente> query = createQuery(queryStr, Dependente.class)
					.setParameter("name", name)
					.setParameter("dtNasc", dtNascimento, TemporalType.DATE);
			Dependente result = query.getSingleResult();
			return result;
		} catch(Exception e) {
		}
		return null;
	}
	
	public Dependente updateDependenteInfo(Long idDependente, ClienteInfoDTO info) {
		String queryStr = "SELECT d FROM Dependente d WHERE d.id = :idDependente";
		TypedQuery<Dependente> query = entityManager.createQuery(queryStr, Dependente.class);
		query.setParameter("idDependente", idDependente);
		query.setMaxResults(1);
		try {
			Dependente dep = query.getSingleResult();
			if(info.getNmCliente() != null && !info.getNmCliente().equals(dep.getNmDependente()) ) {
				dep.setNmDependente(info.getNmCliente());
			}
			if(info.getNmEmail() != null && !info.getNmEmail().equals(dep.getNmEmail()) ) {
				dep.setNmEmail(info.getNmEmail());
			}
			if(info.getNrCelular() != null && !info.getNrCelular().equals(dep.getNrCelular())) {
				dep.setNrCelular(info.getNrCelular());
			}
			this.update(dep);
			return dep;
		} catch (NoResultException e) {
		}
		return null;
	}
	
	public Dependente getDependenteByCPFOrCode(String cpfStr, String codStr) {
		String queryStr = "SELECT dep FROM Dependente dep WHERE ";
		if(cpfStr != null) {
			queryStr += "dep.nrCpf = :param";
		} else if(codStr != null) {
			queryStr += "dep.nrCodCliente = :param";
		}
		try {
			TypedQuery<Dependente> query = createQuery(queryStr, Dependente.class)
					.setParameter("param", (cpfStr != null ? cpfStr : codStr))
					.setMaxResults(1);
			return query.getSingleResult();
		} catch(Exception e) {
			
		}
		return null;
		
	}
	

	public List<Dependente> getDependentesByContrato(Long idContrato){
		String queryStr = "SELECT contratoDependente.dependente FROM ContratoDependente contratoDependente"
				+ " WHERE contratoDependente.contrato.id = :idContrato"
				+ " and (contratoDependente.boFazUsoPlano is null or contratoDependente.boFazUsoPlano = true)";
		try {
			TypedQuery<Dependente> query = entityManager.createQuery(queryStr, Dependente.class);
			query.setParameter("idContrato", idContrato);
			List<Dependente> lDependentes = query.getResultList();
			return lDependentes;			
		} catch (Exception e) {
			return null;
		}
	}


}
