package br.com.medic.medicsystem.persistence.dto;

public class AtividadeAgendamentoDTO {
	
	private String nmFuncionario;
	private String nmCargo;
	private String nmLogin;
	private String nmPerfil;
	private Integer qtdAgendamento;
	private Double media;
	private Integer qtdAtividade;
	private Long idFuncionario;
	private Long idProfissionalRef;
	private Integer qtdExclusao;
	
	public String getNmFuncionario() {
		return nmFuncionario;
	}
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}
	public String getNmCargo() {
		return nmCargo;
	}
	public void setNmCargo(String nmCargo) {
		this.nmCargo = nmCargo;
	}
	public String getNmLogin() {
		return nmLogin;
	}
	public void setNmLogin(String nmLogin) {
		this.nmLogin = nmLogin;
	}
	public String getNmPerfil() {
		return nmPerfil;
	}
	public void setNmPerfil(String nmPerfil) {
		this.nmPerfil = nmPerfil;
	}
	public Integer getQtdAgendamento() {
		return qtdAgendamento;
	}
	public void setQtdAgendamento(Integer qtdAgendamento) {
		this.qtdAgendamento = qtdAgendamento;
	}
	public Double getMedia() {
		return media;
	}
	public void setMedia(Double media) {
		this.media = media;
	}
	public Integer getQtdAtividade() {
		return qtdAtividade;
	}
	public void setQtdAtividade(Integer qtdAtividade) {
		this.qtdAtividade = qtdAtividade;
	}
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public Integer getQtdExclusao() {
		return qtdExclusao;
	}
	public void setQtdExclusao(Integer qtdExclusao) {
		this.qtdExclusao = qtdExclusao;
	}
	public Long getIdProfissionalRef() {
		return idProfissionalRef;
	}
	public void setIdProfissionalRef(Long idProfissionalRef) {
		this.idProfissionalRef = idProfissionalRef;
	}
	
	
	
	

}
