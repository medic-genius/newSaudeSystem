package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.RegistroChamadaProfissional;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;

@Named("registrochamada-dao")
@ApplicationScoped
public class RegistroChamadaDAO extends RelationalDataAccessObject<RegistroChamadaProfissional>{
	
	public List<RegistroChamadaProfissional> verificaRegistroExistente(Long idFuncionario, Long idAgendamento, Long idUnidade){
		
		String query = "SELECT reg FROM RegistroChamadaProfissional REG WHERE reg.funcionario.id = :idFuncionario"
				+ " AND reg.agendamento.id = :idAgendamento AND reg.unidade.id = :idUnidade";
		TypedQuery<RegistroChamadaProfissional> queryresult = 
				entityManager.createQuery(query ,RegistroChamadaProfissional.class )
				.setParameter("idFuncionario", idFuncionario)
				.setParameter("idAgendamento", idAgendamento)
				.setParameter("idUnidade", idUnidade);
		
		try {
			return queryresult.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

}
