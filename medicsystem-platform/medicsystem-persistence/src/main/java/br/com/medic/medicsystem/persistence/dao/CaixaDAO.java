package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Caixa;

@ApplicationScoped
@Named("caixa-dao")
public class CaixaDAO extends RelationalDataAccessObject<Caixa> {

	@SuppressWarnings("unchecked")
	public List<BigInteger> getRetiradaCaixas(Long idContaBancaria, Long idEmpresa, String dtInicio,
			String dtFim) {
		String queryStr = "SELECT mc.idcaixaconferido"
				+ " FROM  financeiro.tbmovimentacaoconta mc"
				+ " INNER JOIN financeiro.tbcontabancariafinanceiro cbf ON cbf.idcontabancaria = mc.idcontabancaria"
				+ " WHERE mc.acaomovimentacao = 3 "//entrada do caixa(retiradas)
				+ " AND mc.dtExclusao is null"
				+ " AND mc.dtmovimentacao BETWEEN '"+ dtInicio + "' AND '"+ dtFim +"'"
				+ " AND mc.idcontabancaria = " + idContaBancaria
				+ " AND cbf.idempresafinanceiro ="+ idEmpresa
				+ " group by mc.idcaixaconferido";
		
		Query query =  findByNativeQuery(queryStr);
		
		List<BigInteger> result = query.getResultList();

		/*if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}*/

		return result;

	}
	
}
