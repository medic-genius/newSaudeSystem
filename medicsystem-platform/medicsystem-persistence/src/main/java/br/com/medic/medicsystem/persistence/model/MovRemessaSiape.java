package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the tbmovremessasiape database table.
 * 
 */
@Entity
public class MovRemessaSiape implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBMOVREMESSASIAPE_IDMOVREMESSASIAPE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBMOVREMESSASIAPE_IDMOVREMESSASIAPE_GENERATOR")
	private Long idmovremessasiape;

	private Timestamp dtatualizacaolog;

	@Temporal(TemporalType.DATE)
	private Date dtexclusao;

	private Timestamp dtinclusaolog;

	private Long idcliente;

	private Long idorgao;

	private Integer instatus;

	private Integer intiporemessa;

	private String nmcliente;

	private String nmobservacao;

	private String nrmatricula;

	private String nrorgao;

	private float vlmensalidade;

	//bi-directional many-to-one association to Tbremessasiape
	@ManyToOne
	@JoinColumn(name="idremessasiape")
	private RemessaSiape tbremessasiape;

	public MovRemessaSiape() {
	}

	public Long getIdmovremessasiape() {
		return this.idmovremessasiape;
	}

	public void setIdmovremessasiape(Long idmovremessasiape) {
		this.idmovremessasiape = idmovremessasiape;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtexclusao() {
		return this.dtexclusao;
	}

	public void setDtexclusao(Date dtexclusao) {
		this.dtexclusao = dtexclusao;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Long getIdcliente() {
		return this.idcliente;
	}

	public void setIdcliente(Long idcliente) {
		this.idcliente = idcliente;
	}

	public Long getIdorgao() {
		return this.idorgao;
	}

	public void setIdorgao(Long idorgao) {
		this.idorgao = idorgao;
	}

	public Integer getInstatus() {
		return this.instatus;
	}

	public void setInstatus(Integer instatus) {
		this.instatus = instatus;
	}

	public Integer getIntiporemessa() {
		return this.intiporemessa;
	}

	public void setIntiporemessa(Integer intiporemessa) {
		this.intiporemessa = intiporemessa;
	}

	public String getNmcliente() {
		return this.nmcliente;
	}

	public void setNmcliente(String nmcliente) {
		this.nmcliente = nmcliente;
	}

	public String getNmobservacao() {
		return this.nmobservacao;
	}

	public void setNmobservacao(String nmobservacao) {
		this.nmobservacao = nmobservacao;
	}

	public String getNrmatricula() {
		return this.nrmatricula;
	}

	public void setNrmatricula(String nrmatricula) {
		this.nrmatricula = nrmatricula;
	}

	public String getNrorgao() {
		return this.nrorgao;
	}

	public void setNrorgao(String nrorgao) {
		this.nrorgao = nrorgao;
	}

	public float getVlmensalidade() {
		return this.vlmensalidade;
	}

	public void setVlmensalidade(float vlmensalidade) {
		this.vlmensalidade = vlmensalidade;
	}

	public RemessaSiape getTbremessasiape() {
		return this.tbremessasiape;
	}

	public void setTbremessasiape(RemessaSiape tbremessasiape) {
		this.tbremessasiape = tbremessasiape;
	}

}