package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tblaudoatendimento_view")
public class LaudoAtendimentoView {
	
	@Id
	@Column(name = "idlaudoatendimento")
	private Long id;
	
	@Basic
	@Column(name = "nragendamento")
	private String nrAgendamento;
	
	@Basic
	@Column(name = "tipopaciente")
	private Character tipoPaciente;
	
	@Basic
	@Column(name = "idpaciente")
	private Long idPaciente;
	
	@Basic
	@Column(name = "nmpaciente")
	private String nmPaciente;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "nmservico")
	private String nmServico;
	
	@Basic
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",  timezone = "America/Manaus")
	@Column(name = "dtatendimento")
	private Date dtAtendimento;
	
	@Basic
	@Column(name = "arqlaudo")
	private byte[] arqLaudo;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrAgendamento() {
		return nrAgendamento;
	}

	public void setNrAgendamento(String nrAgendamento) {
		this.nrAgendamento = nrAgendamento;
	}

	public Character getTipoPaciente() {
		return tipoPaciente;
	}

	public void setTipoPaciente(Character tipoPaciente) {
		this.tipoPaciente = tipoPaciente;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public Date getDtAtendimento() {
		return dtAtendimento;
	}

	public void setDtAtendimento(Date dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}

	public byte[] getArqLaudo() {
		return arqLaudo;
	}

	public void setArqLaudo(byte[] arqLaudo) {
		this.arqLaudo = arqLaudo;
	}	

}
