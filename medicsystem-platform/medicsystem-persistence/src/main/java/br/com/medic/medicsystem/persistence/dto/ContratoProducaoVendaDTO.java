package br.com.medic.medicsystem.persistence.dto;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoContratoEnum;

public class ContratoProducaoVendaDTO {
	
	private String nmcliente;
	
	private String nrcontrato;
	
	private Date dtcontrato;

	private Integer informapagamento;
		
	private Double vltotal;
	
	private Double vlproducao;
	
	private String vlVendaFormatado;
	
	private String vlProducaoFormatado;
	
	private String formaPagamentoFormatado;
	
	private String dtContratoFormatado;

	private Double totalvenda;
	
	private Double totalcomissao;
	
	private Integer quantcontratos;
	
	private String nmfuncionario;
	
	
	public String getNmcliente() {
		return nmcliente;
	}

	public void setNmcliente(String nmcliente) {
		this.nmcliente = nmcliente;
	}

	public String getNrcontrato() {
		return nrcontrato;
	}

	public void setNrcontrato(String nrcontrato) {
		this.nrcontrato = nrcontrato;
	}

	public Integer getInformapagamento() {
		return informapagamento;
	}

	public void setInformapagamento(Integer informapagamento) {
		this.informapagamento = informapagamento;
	}

	public Double getVltotal() {
		return vltotal;
	}

	public void setVltotal(Double vltotal) {
		this.vltotal = vltotal;
	}

	public Double getVlproducao() {
		return vlproducao;
	}

	public void setVlproducao(Double vlproducao) {
		this.vlproducao = vlproducao;
	}	

	public String getFormaPagamentoFormatado() {
		
		if ( informapagamento == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getId() )
			formaPagamentoFormatado = FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getDescription();
		
		else if ( informapagamento == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId() )
			formaPagamentoFormatado = FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getDescription() ;
		
		else if ( informapagamento == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getId() )
			formaPagamentoFormatado = FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getDescription() ;
		
		else if ( informapagamento == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getId())
			formaPagamentoFormatado = FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getDescription();
		
		return formaPagamentoFormatado;
	}

	public void setFormaPagamentoFormatado(String formaPagamentoFormatado) {
		this.formaPagamentoFormatado = formaPagamentoFormatado;
	}
	
	public String getVlVendaFormatado() {
		
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVltotal() != null)
			vlVendaFormatado = currencyFormatter.format(getVltotal());
		
		return vlVendaFormatado;
	}

	public void setVlVendaFormatado(String vlVendaFormatado) {
		this.vlVendaFormatado = vlVendaFormatado;
	}

	public String getVlProducaoFormatado() {
		
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVlproducao() != null)
			vlProducaoFormatado = currencyFormatter.format(getVlproducao());
		
		return vlProducaoFormatado;
	}

	public void setVlProducaoFormatado(String vlProducaoFormatado) {
		this.vlProducaoFormatado = vlProducaoFormatado;
	}
	
	public Date getDtcontrato() {
				
		return dtcontrato;
	}

	public void setDtcontrato(Date dtcontrato) {
		this.dtcontrato = dtcontrato;
	}
	
	public String getDtContratoFormatado() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		
		if(dtcontrato != null)
			dtContratoFormatado = sdf.format(dtcontrato);
		
		return dtContratoFormatado;		
	}

	public void setDtContratoFormatado(String dtContratoFormatado) {
		this.dtContratoFormatado = dtContratoFormatado;
	}

	public Double getTotalvenda() {
		return totalvenda;
	}

	public void setTotalvenda(Double totalvenda) {
		this.totalvenda = totalvenda;
	}

	public Double getTotalcomissao() {
		return totalcomissao;
	}

	public void setTotalcomissao(Double totalcomissao) {
		this.totalcomissao = totalcomissao;
	}

	public Integer getQuantcontratos() {
		return quantcontratos;
	}

	public void setQuantcontratos(Integer quantcontratos) {
		this.quantcontratos = quantcontratos;
	}

	public String getNmfuncionario() {
		return nmfuncionario;
	}

	public void setNmfuncionario(String nmfuncionario) {
		this.nmfuncionario = nmfuncionario;
	}

	
}
