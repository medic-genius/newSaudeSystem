package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObjectDental;
import br.com.medic.medicsystem.persistence.model.Cliente;

@Named("clientecompartinhadodental-dao")
@ApplicationScoped
public class ClienteCompartinhadoDentalDAO extends RelationalDataAccessObjectDental<Cliente>{

	public Cliente updateCliente(Cliente cliente){
		return entityManager.merge(cliente);
	}
}
