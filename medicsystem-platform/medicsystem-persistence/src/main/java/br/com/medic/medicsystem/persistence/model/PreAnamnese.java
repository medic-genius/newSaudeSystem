package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the tbpreanamnese database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbpreanamnese")
public class PreAnamnese implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBPREANAMNESE_IDPREANAMNESE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBPREANAMNESE_IDPREANAMNESE_GENERATOR")
	@Column(name = "idpreanamnese")
	private Long idPreAnamnese;

	@Basic
	@Column(name = "dtatualizacaolog")
	@JsonIgnore
	private Timestamp dtatualizacaolog;

	@Basic
	@Column(name = "dtinclusaolog")
	@JsonIgnore
	private Timestamp dtinclusaolog;

	@Basic
	@Column(name = "nmpergunta")
	private String nmPergunta;

//	//bi-directional many-to-one association to Tbpreanamnesepaciente
//	@JsonIgnore
//	@OneToMany(mappedBy="preAnamnese")
//	private List<PreAnamnesePaciente> tbpreanamnesepacientes;

	public PreAnamnese() {
	}

	

	

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}





	public Long getIdPreAnamnese() {
		return idPreAnamnese;
	}





	public void setIdPreAnamnese(Long idPreAnamnese) {
		this.idPreAnamnese = idPreAnamnese;
	}





	public String getNmPergunta() {
		return nmPergunta;
	}





	public void setNmPergunta(String nmPergunta) {
		this.nmPergunta = nmPergunta;
	}



	

	

//	public List<PreAnamnesePaciente> getTbpreanamnesepacientes() {
//		return this.tbpreanamnesepacientes;
//	}
//
//	public void setTbpreanamnesepacientes(List<PreAnamnesePaciente> tbpreanamnesepacientes) {
//		this.tbpreanamnesepacientes = tbpreanamnesepacientes;
//	}

//	public PreAnamnesePaciente addTbpreanamnesepaciente(PreAnamnesePaciente tbpreanamnesepaciente) {
//		getTbpreanamnesepacientes().add(tbpreanamnesepaciente);
//		tbpreanamnesepaciente.setPreAnamnese(this);
//
//		return tbpreanamnesepaciente;
//	}
//
//	public PreAnamnesePaciente removeTbpreanamnesepaciente(PreAnamnesePaciente tbpreanamnesepaciente) {
//		getTbpreanamnesepacientes().remove(tbpreanamnesepaciente);
//		tbpreanamnesepaciente.setPreAnamnese(null);
//
//		return tbpreanamnesepaciente;
//	}

}