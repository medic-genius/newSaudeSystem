package br.com.medic.medicsystem.persistence.model.pjbank;

public class EmpresaCredenciar {
	
	/**
	 * Razão social da empresa. Será usado no campo “beneficiário” do boleto, length (3-80).
	 */
	private String nome_empresa;
	
	/**
	 * CNPJ da empresa, somente números. O CNPJ deve ser o mesmo da razão social e da conta de repasse. 
	 * Será exposto no boleto bancário, length (11-14).
	 */
	private String banco_repasse;
	
	/**
	 * Número do banco que será feito o repasse, com 3 dígitos. 
	 * Para uma lista de bancos permitidos, veja item 4, length (3-3).
	 */
	private String agencia_repasse;
	
	/**
	 * Número da agência que será feito o repasse, com ou sem dígito dependendo do banco. 
	 * Formato: 99999, 9999-9, length (2-2).
	 */
	private String conta_repasse;
	
	/**
	 * Número da conta bancaria que será feito o repasse, com dígito. Formato: 99999-9, 9999-99, length (0-2).
	 */
	private String cnpj;
	
	/**
	 * DDD do telefone da empresa, somente números, length (2-2).
	 */
	private String ddd;
	
	/**
	 * Telefone da empresa, sem DDD. Somente números. Para uso interno, length (8-10).
	 */
	private String telefone;
	
	/**
	 * E-mail da empresa. Para uso interno, length (3-80).
	 */
	private String email;
	
	/**
	 * Código de identificação do parceiro, fornecido pelo PJBank, length (4-4).
	 */
	private String agencia;
	

	public String getNome_empresa() {
		return nome_empresa;
	}

	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}

	public String getBanco_repasse() {
		return banco_repasse;
	}

	public void setBanco_repasse(String banco_repasse) {
		this.banco_repasse = banco_repasse;
	}

	public String getAgencia_repasse() {
		return agencia_repasse;
	}

	public void setAgencia_repasse(String agencia_repasse) {
		this.agencia_repasse = agencia_repasse;
	}

	public String getConta_repasse() {
		return conta_repasse;
	}

	public void setConta_repasse(String conta_repasse) {
		this.conta_repasse = conta_repasse;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

}
