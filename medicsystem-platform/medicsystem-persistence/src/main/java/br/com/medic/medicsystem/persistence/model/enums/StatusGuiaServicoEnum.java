package br.com.medic.medicsystem.persistence.model.enums;

public enum StatusGuiaServicoEnum {
		
	CRIADO(0,"Criado - Em aberto"), 
	AGENDADO(1, "Agendado"), 
	CONSUMIDO(2, "Consumido"), 
	INATIVO(4, "Inativo"),
	VENCIDO(5, "Vencido");
	
	private Integer id;
	
	private String description;
	
	StatusGuiaServicoEnum(Integer id, String description) {
		
		this.id = id;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
