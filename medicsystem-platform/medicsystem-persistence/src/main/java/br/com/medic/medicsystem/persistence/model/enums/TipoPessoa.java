package br.com.medic.medicsystem.persistence.model.enums;

public enum TipoPessoa {
	
	CLIENTE(1),
	DEPENDENTE(2),
	FUNCIONARIO(3),
	FUNCIONARIOAUTORIZADO(4);
	
	private Integer id;
	
	private TipoPessoa(Integer id) {
		// TODO Auto-generated constructor stub
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public static TipoPessoa getTipoPessoaById(int id){
		for (TipoPessoa t : TipoPessoa.values()) {
			if(id == t.id)
				return t;
		}
		return null;
	}
	

}
