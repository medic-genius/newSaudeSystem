package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

public class AtendimentoProfissionalResuPagamentoDTO {
	
	private List<AtendimentoProfissionalResuDTO> AtendimentoProfissionalResuDTO;
	private Double SomatotalVlComissaoSalario;
	
	
	
	public List<AtendimentoProfissionalResuDTO> getAtendimentoProfissionalResuDTO() {
		return AtendimentoProfissionalResuDTO;
	}
	public void setAtendimentoProfissionalResuDTO(
			List<AtendimentoProfissionalResuDTO> atendimentoProfissionalResuDTO) {
		AtendimentoProfissionalResuDTO = atendimentoProfissionalResuDTO;
	}
	public Double getSomatotalVlComissaoSalario() {
		return SomatotalVlComissaoSalario;
	}
	public void setSomatotalVlComissaoSalario(Double somatotalVlComissaoSalario) {
		SomatotalVlComissaoSalario = somatotalVlComissaoSalario;
	}
	
	
	
	

}
