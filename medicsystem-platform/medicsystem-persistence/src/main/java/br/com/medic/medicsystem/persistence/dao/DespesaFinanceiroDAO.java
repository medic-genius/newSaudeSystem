package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

//import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaGraficoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.DespesaFinanceiro;
import br.com.medic.medicsystem.persistence.model.views.DespesaFinanceiroView;

@Named("despesafinanceiro-dao")
@ApplicationScoped
public class DespesaFinanceiroDAO extends RelationalDataAccessObject<DespesaFinanceiro>{

	public List<DespesaFinanceiro> getAllDespesasFinanceiro(){
		String queryStr = "SELECT d FROM DespesaFinanceiro d WHERE d.dtExclusao is null ORDER BY d.dtVencimento ASC";
		TypedQuery<DespesaFinanceiro> query = entityManager.createQuery(queryStr, DespesaFinanceiro.class);
		List<DespesaFinanceiro> list = query.getResultList();
		return list;
	}
	
	public List<DespesaFinanceiro> getDespesaFinanceiroByNumeroDocumento(String nrDocumento){
		List<DespesaFinanceiro> despesas = null;
		try{
			String queryStr = "SELECT d FROM DespesaFinanceiro d WHERE d.dtExclusao is null AND d.nrDocumento = '" + nrDocumento + "' ORDER BY d.dtVencimento ASC";
		TypedQuery<DespesaFinanceiro> query = entityManager.createQuery(queryStr, DespesaFinanceiro.class);
			despesas = query.getResultList();
		}catch(ObjectNotFoundException | NoResultException ex){
			return null;
		}
		return despesas;
	}
	
	public List<DespesaFinanceiro> getDespesasFinanceiroByServico(String nrServico, Long idEmpresaFinanceiroPagamente, String dtInicio, String dtFim, Long idContaBancaria){
		String queryStr = "SELECT  DISTINCT(d) FROM DespesaFinanceiro d, MovimentacaoItemConta movitem, MovimentacaoConta mov "
				+ " WHERE movitem.despesaFinanceiro.id = d.id "
				+ " AND movitem.movimentacaoConta.id = mov.id "
				+ " AND d.empresaFinanceiroPagante.idEmpresaFinanceiro = " + idEmpresaFinanceiroPagamente;
				if(idContaBancaria != null){
					queryStr += " AND d.contaBancaria.id = "+ idContaBancaria;
				}
				queryStr += " AND d.nrServico = '" + nrServico + "' "
				+ " AND d.dtExclusao is null";
				if(dtInicio != null && dtFim != null ) {
					queryStr += " AND d.dtPagamento BETWEEN '"+ dtInicio +"' AND '" +dtFim+"'";
				}
				queryStr += " AND mov.acaoMovimentacao = 0  " // pagamento de conta
				+ " AND mov.dtExclusao is null"
		 		+ " ORDER BY d.dtVencimento ASC";
				
		
		TypedQuery<DespesaFinanceiro> query = entityManager.createQuery(queryStr, DespesaFinanceiro.class);

		List<DespesaFinanceiro> list = query.getResultList();
		return list;
	}
	
	public List<DespesaFinanceiro> getDespesasFinanceiroByEmpresaFinanceiro(Long id) {
		String queryStr = "SELECT d FROM DespesaFinanceiro d WHERE d.empresaFinanceiroSolicitante.id = " + id  
				+ " and d.dtExclusao is null ORDER BY d.dtVencimento ASC";
		TypedQuery<DespesaFinanceiro> query = entityManager.createQuery(queryStr, DespesaFinanceiro.class);
		List<DespesaFinanceiro> list = query.getResultList();
		return list;
	}
	
	public Long getQuantidadeDespesas(Long idEmpresaFinanceiro, Date dtInicio, Date dtFim, Integer status){
		TypedQuery<Long> query;
		Long result = null;
		
		String querySql = "SELECT count(d) FROM DespesaFinanceiro d WHERE d.empresaFinanceiroSolicitante.idEmpresaFinanceiro = " + idEmpresaFinanceiro;

		if(status != null){
			querySql += " AND d.inStatus = " + status;
		}
		
		if(dtInicio == null && dtFim != null){
			querySql += " AND d.dtVencimento <= :endDate";
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Long.class);
			query.setParameter("endDate", dtFim);
		} else if(dtInicio != null && dtFim != null){
			querySql += " AND d.dtVencimento BETWEEN :startDate AND :endDate";
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Long.class);
			query.setParameter("startDate", dtInicio);
			query.setParameter("endDate", dtFim);
		}else{
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Long.class);
		}
		
		try{
			result = query.getSingleResult();
		}catch(NoResultException ex){
			result = 0L;
		}
		return result;
	}
	
	public Double getValorTotalDespesas(Long idEmpresaFinanceiro, Date dtInicio, Date dtFim, Integer status){
		TypedQuery<Double> query;
		Double result = null;
		
		String querySql = "SELECT sum(d.vlDespesa) FROM DespesaFinanceiro d WHERE d.empresaFinanceiroSolicitante.idEmpresaFinanceiro = " + idEmpresaFinanceiro;
	
		if(status != null){
			querySql += " AND d.inStatus = " + status;
		}
		
		if(dtInicio == null && dtFim != null){
			querySql += " AND d.dtVencimento <= :endDate";
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Double.class);
			query.setParameter("endDate", dtFim);
		} else if (dtInicio != null && dtFim != null){
			querySql += " AND d.dtVencimento BETWEEN :startDate AND :endDate";
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Double.class);
			query.setParameter("startDate", dtInicio);
			query.setParameter("endDate", dtFim);
		} else{
			querySql += " AND d.dtExclusao is null";
			query = entityManager.createQuery(querySql, Double.class);			
		}
		
		try{
			result = query.getSingleResult();
		}catch(NoResultException ex){
			result = 0.0;
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<DespesaFinanceiro> getDespesas(Long idEmpresaFinanceiro, Date dtInicio, Date dtFim, Integer status, Integer inModalidade){
		Query query;
		List<DespesaFinanceiro> result = null;
		
		String querySql = "SELECT d FROM DespesaFinanceiro d WHERE d.empresaFinanceiroSolicitante.idEmpresaFinanceiro = " + idEmpresaFinanceiro;
	
		if(status != null){
			if (status == 0){
				querySql += " AND d.inStatus in (0,2)";
			}else if(status == 7){
				querySql += " AND d.inStatus = 1 AND d.idTransacaoBancaria IS NOT NULL";
			}else{
				querySql += " AND d.inStatus = " + status;
			}
		}
		
		if(dtInicio == null && dtFim == null){
			querySql += " AND d.dtExclusao is null ORDER BY d.dtVencimento ASC";
			query = entityManager.createQuery(querySql, DespesaFinanceiro.class);
		} else if(dtInicio == null && dtFim != null){
			querySql += " AND d.dtVencimento <= :endDate";
			querySql += " AND d.dtExclusao is null ORDER BY d.dtVencimento ASC";
			query = entityManager.createQuery(querySql, DespesaFinanceiro.class);
			query.setParameter("endDate", dtFim);
		} else if (dtInicio != null && dtFim != null){
			querySql += " AND d.dtVencimento BETWEEN :startDate AND :endDate";
			querySql += " AND d.dtExclusao is null ";
			if(inModalidade != null ) {
				querySql += " AND d.inFormaPagamento = " + inModalidade;
			}
			querySql += " ORDER BY d.dtVencimento, d.contaContabil ASC";
			query = entityManager.createQuery(querySql, DespesaFinanceiro.class);
			query.setParameter("startDate", dtInicio);
			query.setParameter("endDate", dtFim);
		} else{
			querySql += " AND d.dtVencimento >= :startDate";
			querySql += " and d.dtExclusao is null ORDER BY d.dtVencimento ASC";
			query = entityManager.createQuery(querySql, DespesaFinanceiro.class);
			query.setParameter("startDate", dtInicio);
		}
		
		try{
			result = query.getResultList();
		}catch(NoResultException ex){
			result = null;
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<DespesaGraficoDTO> getGraficoDespesa(Long idEmpresa, Date dataInicio, Date dataFim){
		String queryStr = "SELECT to_char(dtvencimento,'Mon') AS nmmes, extract(month from dtvencimento) as nrmes, extract(year from dtvencimento) as nrano, sum(vldespesa) AS nrvalortotal, count(vldespesa) AS nrquantidade"
				+ " FROM financeiro.tbdespesa"
				+ " WHERE idempresafinanceirostart = "  + idEmpresa + " AND dtvencimento BETWEEN :dataInicio AND :dataFim AND dtexclusao IS null AND instatus <> 1"
				+ " GROUP BY nmmes, nrano, nrmes"
				+ " ORDER BY nrano, nrmes";
		
		List<DespesaGraficoDTO> list = findByNativeQuery(queryStr).setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE)
				.unwrap(SQLQuery.class)
				.addScalar("nmMes", StringType.INSTANCE)
				.addScalar("nrMes", DoubleType.INSTANCE)
				.addScalar("nrAno", DoubleType.INSTANCE)
				.addScalar("nrValorTotal", DoubleType.INSTANCE)
				.addScalar("nrQuantidade", LongType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(DespesaGraficoDTO.class)).list();
		return list;
		
	}

	public List<DespesaFinanceiro> verificarNrServicoExistenteMesmaEmpresa(Long idDespesa, String nrServico, Long idEmpresaFinanceiroPagante ) {
		List<DespesaFinanceiro> despesas = null;
		try{
			
			String queryStr = "SELECT d FROM DespesaFinanceiro d WHERE  d.nrServico = '" + nrServico + "'"  
					+ " AND d.empresaFinanceiroPagante.idEmpresaFinanceiro = " + idEmpresaFinanceiroPagante;
					
		   if( idDespesa != null )
			   queryStr = queryStr + " AND d.id != " + idDespesa;
					
			TypedQuery<DespesaFinanceiro> query = entityManager.createQuery(queryStr, DespesaFinanceiro.class);
			despesas = query.getResultList();
		}catch(ObjectNotFoundException | NoResultException ex){
			return null;
		}
		return despesas;
	}
	
	/**/

	@SuppressWarnings("unchecked")
	public List<DespesaFinanceiroDTO> getDespesasByCC(String idEmpresa, Long dtAno, Long dtMes, Integer inStatus,
			Integer idContaContabil) {
		String queryStr ="";
		if(idEmpresa != null && !idEmpresa.equals("0") && inStatus == 1) { // pago
			queryStr += " SELECT d.idempresafinanceirostart,d.iddespesa, d.nmobservacao, cc.nmdescricao,cc.idcontacontabil,d.dtpagamento, d.vlpago as valorAP, d.nrdocumento, d.instatus,"
					+ " d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento ";
		}else if(idEmpresa != null && !idEmpresa.equals("0")  && (inStatus == 0 || inStatus == 3 || inStatus == 2) ){ // aberto, nao pago
			queryStr += "SELECT d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.nmdescricao, cc.idcontacontabil, d.dtpagamento, d.vldespesa as valorAP, d.nrdocumento, d.instatus,"
					+ "	d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento ";
		} else if(inStatus == 1){ //pago
			queryStr += "select d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.idcontacontabil, cc.nmdescricao, d.dtpagamento, d.vlpago as valorAP, d.nrdocumento, d.instatus, d.dtvencimento, f.nmnomefantasia as nomeFornecedor,"
					+ " d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento";
		} else if(inStatus == 0 || inStatus == 3 || inStatus == 2){ // aberto, nao pago
			queryStr += "select d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.idcontacontabil, cc.nmdescricao, d.dtpagamento, d.vldespesa as valorAP, d.nrdocumento, d.instatus, d.dtvencimento, f.nmnomefantasia as nomeFornecedor,"
					+ " d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento";
		}
		
		queryStr += " from financeiro.tbdespesa d "
					+ " left join financeiro.tbcontacontabil cc on cc.idcontacontabil = d.idcontacontabil"
					+ " left join financeiro.tbfornecedor f on d.idfornecedor = f.idfornecedor"
					+ "	left join financeiro.tbempresafinanceiro emp on emp.idempresafinanceiro = d.idempresafinanceirostart"
					+ " and cc.dtexclusao is null ";
		
		if(inStatus == 1) {
			queryStr += " where Extract('Month' from d.dtpagamento) = " + dtMes
					+ " and extract ('Year' from d.dtpagamento) = " + dtAno
					+ " and d.dtexclusao is null";
		} else {
			queryStr += " where Extract('Month' from d.dtvencimento) = " + dtMes
					+ " and extract ('Year' from d.dtvencimento) = " + dtAno
					+ " and d.dtexclusao is null";
		}
		if(idContaContabil != null && idContaContabil != 0) {
			queryStr += " and d.idcontacontabil in ("
					+ " WITH RECURSIVE q AS" 
					+ "	(SELECT idcontacontabil, contacontabil_idcontacontabil FROM financeiro.tbcontacontabil" 
					+ "	 WHERE idcontacontabil = " + idContaContabil 
					+ "	UNION ALL" 
					+ "	 SELECT m.idcontacontabil, m.contacontabil_idcontacontabil FROM financeiro.tbcontacontabil m"  
				 	+ "	 JOIN q ON q.idcontacontabil = m.contacontabil_idcontacontabil)"  
					+ "	SELECT idcontacontabil FROM q"
					+ ")"; 
		}
		
		if(idEmpresa != null && !idEmpresa.equals("0") ) {
			queryStr += " and d.idempresafinanceirostart in ("+ idEmpresa+")";
		}
		
		
		if(inStatus == 1) {
			queryStr += " and d.instatus = 1";
		} else if(inStatus == 0) {
			queryStr += " and d.instatus in (0,2)";
		} else if (inStatus == 2) {
			queryStr += " and d.instatus in (0,1,2)";
		}

		queryStr += "order by d.dtvencimento, nomeFornecedor";
				if( idEmpresa != null && !idEmpresa.equals("0") ) {
					List<DespesaFinanceiroDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
							.addScalar("idDespesa", LongType.INSTANCE)
							.addScalar("nmDescricao", StringType.INSTANCE)
							.addScalar("nmObservacao", StringType.INSTANCE)
							.addScalar("idContaContabil", LongType.INSTANCE)
							.addScalar("valorAP", BigDecimalType.INSTANCE)
							.addScalar("nrDocumento", StringType.INSTANCE)
							.addScalar("dtPagamento", StringType.INSTANCE)
							.addScalar("inStatus", IntegerType.INSTANCE)
							.addScalar("dtVencimento", StringType.INSTANCE)
							.addScalar("nomeFornecedor", StringType.INSTANCE)
							.addScalar("nrQuantidadeParcelas", IntegerType.INSTANCE)
							.addScalar("nrOrdemParcela", IntegerType.INSTANCE)
							.addScalar("nmDespesa", StringType.INSTANCE)
							.addScalar("nmFantasia", StringType.INSTANCE)
							.addScalar("inFormaPagamento", IntegerType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(DespesaFinanceiroDTO.class)).list();
					return result;
				} else {
					List<DespesaFinanceiroDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
							.addScalar("idDespesa", LongType.INSTANCE)
							.addScalar("nmDescricao", StringType.INSTANCE)
							.addScalar("nmObservacao", StringType.INSTANCE)
							.addScalar("idContaContabil", LongType.INSTANCE)
							.addScalar("valorAP", BigDecimalType.INSTANCE)
							.addScalar("nrDocumento", StringType.INSTANCE)
							.addScalar("dtPagamento", StringType.INSTANCE)
							.addScalar("inStatus", IntegerType.INSTANCE)
							.addScalar("dtVencimento", StringType.INSTANCE)
							.addScalar("nomeFornecedor", StringType.INSTANCE)
							.addScalar("nrQuantidadeParcelas", IntegerType.INSTANCE)
							.addScalar("nrOrdemParcela", IntegerType.INSTANCE)
							.addScalar("nmDespesa", StringType.INSTANCE)
							.addScalar("nmFantasia", StringType.INSTANCE)
							.addScalar("inFormaPagamento", IntegerType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(DespesaFinanceiroDTO.class)).list();
					return result;
				}
	}
	@SuppressWarnings("unchecked")
	public List<DespesaFinanceiroDTO> getDespesasByCentroCusto(String idEmpresa, Long dtAno, Long dtMes, Integer inStatus,
			Integer idCentroCusto) {
		String queryStr ="";
		if(idEmpresa != null && !idEmpresa.equals("0") && inStatus == 1) { // pago
			queryStr += " SELECT d.idempresafinanceirostart,d.iddespesa, d.nmobservacao, cc.nmcentrocusto,cc.idcentrocusto,d.dtpagamento, d.vlpago as valorAP, d.nrdocumento, d.instatus,"
					+ " d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento";
		}else if(idEmpresa != null && !idEmpresa.equals("0") && (inStatus == 0 || inStatus == 3 || inStatus ==2) ){ // aberto, nao pago
			queryStr += "SELECT d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.nmcentrocusto, cc.idcentrocusto, d.dtpagamento, d.vldespesa as valorAP, d.nrdocumento, d.instatus,"
					+ "	d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas, d.nmdespesa, emp.nmFantasia, d.informapagamento";
		} else if(inStatus == 1){ //pago
			queryStr += "select d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.idcentrocusto, cc.nmcentrocusto, d.dtpagamento, d.vlpago as valorAP, d.nrdocumento, d.instatus, d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas,d.nmdespesa, emp.nmFantasia, d.informapagamento";
		} else if(inStatus == 0 || inStatus == 3|| inStatus == 2){ // aberto, nao pago
			queryStr += "select d.idempresafinanceirostart, d.iddespesa, d.nmobservacao, cc.idcentrocusto, cc.nmcentrocusto, d.dtpagamento, d.vldespesa as valorAP, d.nrdocumento, d.instatus, d.dtvencimento, f.nmnomefantasia as nomeFornecedor, d.nrordemparcela, d.nrquantidadeparcelas,d.nmdespesa, emp.nmFantasia, d.informapagamento ";
		}
		
		queryStr += " from financeiro.tbdespesa d "
					+ " left join financeiro.tbcentrocusto cc on cc.idcentrocusto = d.idcentrocusto"
					+ " left join financeiro.tbfornecedor f on d.idfornecedor = f.idfornecedor"
					+ "	left join financeiro.tbempresafinanceiro emp on emp.idempresafinanceiro = d.idempresafinanceirostart"
					+ " and cc.dtexclusao is null ";
		
		if(inStatus == 1) {
			queryStr += " where Extract('Month' from d.dtpagamento) = " + dtMes
					+ " and extract ('Year' from d.dtpagamento) = " + dtAno
					+ " and d.dtexclusao is null";
		} else {
			queryStr += " where Extract('Month' from d.dtvencimento) = " + dtMes
					+ " and extract ('Year' from d.dtvencimento) = " + dtAno
					+ " and d.dtexclusao is null";
		}
		if(idCentroCusto != null && idCentroCusto != 0) {
			queryStr += " and d.idcentrocusto in ("
					+ " WITH RECURSIVE q AS" 
					+ "	(SELECT idcentrocusto, centrocusto_idcentrocusto FROM financeiro.tbcentrocusto" 
					+ "	 WHERE idcentrocusto = " + idCentroCusto 
					+ "	UNION ALL" 
					+ "	 SELECT m.idcentrocusto, m.centrocusto_idcentrocusto FROM financeiro.tbcentrocusto m"  
				 	+ "	 JOIN q ON q.idcentrocusto = m.centrocusto_idcentrocusto)"  
					+ "	SELECT idcentrocusto FROM q"
					+ ")"; 
		}
		
		if(idEmpresa != null && !idEmpresa.equals("0")) {
			queryStr += " and d.idempresafinanceirostart in ( "+ idEmpresa + ")";
		}
		
		
		if(inStatus == 1 || inStatus == 0) {
			queryStr += " and d.instatus =" + inStatus;
		} else if(inStatus == 3){
			queryStr += " and d.instatus in (0,2) ";
		}
		
		queryStr += "order by d.dtvencimento, nomeFornecedor";

		
				if( idEmpresa != null && !idEmpresa.equals("0")) {
					List<DespesaFinanceiroDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
							.addScalar("idDespesa", LongType.INSTANCE)
							.addScalar("nmObservacao", StringType.INSTANCE)
							.addScalar("nmCentroCusto", StringType.INSTANCE)
							.addScalar("idCentroCusto", LongType.INSTANCE)
							.addScalar("valorAP", BigDecimalType.INSTANCE)
							.addScalar("nrDocumento", StringType.INSTANCE)
							.addScalar("dtPagamento", StringType.INSTANCE)
							.addScalar("inStatus", IntegerType.INSTANCE)
							.addScalar("dtVencimento", StringType.INSTANCE)
							.addScalar("nomeFornecedor", StringType.INSTANCE)
							.addScalar("nrQuantidadeParcelas", IntegerType.INSTANCE)
							.addScalar("nrOrdemParcela", IntegerType.INSTANCE)
							.addScalar("nmFantasia", StringType.INSTANCE)
							.addScalar("nmDespesa", StringType.INSTANCE)
							.addScalar("inFormaPagamento", IntegerType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(DespesaFinanceiroDTO.class)).list();
					return result;
				} else {
					List<DespesaFinanceiroDTO> result = findByNativeQuery(queryStr)
							.unwrap(SQLQuery.class)
							.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
							.addScalar("idDespesa", LongType.INSTANCE)
							.addScalar("nmObservacao", StringType.INSTANCE)
							.addScalar("nmCentroCusto", StringType.INSTANCE)
							.addScalar("idCentroCusto", LongType.INSTANCE)
							.addScalar("valorAP", BigDecimalType.INSTANCE)
							.addScalar("nrDocumento", StringType.INSTANCE)
							.addScalar("dtPagamento", StringType.INSTANCE)
							.addScalar("inStatus", IntegerType.INSTANCE)
							.addScalar("dtVencimento", StringType.INSTANCE)
							.addScalar("nomeFornecedor", StringType.INSTANCE)
							.addScalar("nrQuantidadeParcelas", IntegerType.INSTANCE)
							.addScalar("nrOrdemParcela", IntegerType.INSTANCE)
							.addScalar("nmFantasia", StringType.INSTANCE)
							.addScalar("inFormaPagamento", IntegerType.INSTANCE)
							.setResultTransformer(Transformers.aliasToBean(DespesaFinanceiroDTO.class)).list();
					return result;
				}
			

	}

	public List<DespesaFinanceiroView> getBuscaDespesaFinanceiro(String nmDespesa, Long idEmpresa, Date dtInicio, Date dtFim, String nrDocumento,Long idContaContabil, Integer startPosition,
			Integer maxResults, Long idContaBancaria) {
				
				String querySql = "SELECT d FROM DespesaFinanceiroView d"
						+ " WHERE d.dtExclusao IS NULL"
						+ " AND d.inTipo = 0";
						if(nrDocumento != null){
							querySql += " AND d.nrDocumento like '%"+ nrDocumento+"%'";
						}
						if(nmDespesa != null){
							querySql += " AND d.nmDespesa like UPPER('%"+nmDespesa+"%')";
						}
						if(idContaContabil != null){
							querySql += " AND d.contaContabil.id = " +idContaContabil;
						}
						querySql += " AND d.dtVencimento BETWEEN :dtInicio AND :dtFim "
						+ " AND d.idTransacaoBancaria IS NULL"
						+ " AND (d.contaBancaria.id = "+ idContaBancaria
						+ " OR d.contaBancaria IS NULL)"
						+ " ORDER BY d.dtVencimento, d.contaContabil.id ASC";
				
				/*String querySql = "SELECT d FROM DespesaFinanceiro d "
						+ " WHERE d.dtExclusao is null"
						+ " AND d.inTipo = 0";
				if(nrDocumento != null){
					querySql += " AND d.nrDocumento like '%"+ nrDocumento +"%'";
				}
				if(nmDespesa != null){
					querySql += " AND d.nmDespesa like UPPER('%"+nmDespesa+"%')";
				}
				if(idContaContabil != null){
					querySql += " AND d.contaContabil.id = " +idContaContabil;
				}
				
				querySql +=" AND d.dtVencimento BETWEEN '"+ dtInicio +"' AND '"+ dtFim+"' "
						+ " ORDER BY d.dtVencimento, d.contaContabil.id ASC";
				*/
				
				
				TypedQuery<DespesaFinanceiroView> query = entityManager.createQuery(querySql, DespesaFinanceiroView.class).setParameter("dtInicio", dtInicio).setParameter("dtFim", dtFim);
				startPosition = startPosition == null ? 0 : startPosition;
				if(maxResults != null) {
					query.setMaxResults(maxResults);
				}
				query.setFirstResult(startPosition);
			
				List<DespesaFinanceiroView> result = query.getResultList();
	
				if (result.isEmpty()) {
					throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
				}

			return result;
			
	}



}
