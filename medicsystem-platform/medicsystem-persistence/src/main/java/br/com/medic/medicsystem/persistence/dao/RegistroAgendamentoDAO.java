package br.com.medic.medicsystem.persistence.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.RegistroAtaDTO;
import br.com.medic.medicsystem.persistence.dto.UnidadeDTO;
import br.com.medic.medicsystem.persistence.model.RegistroAgendamento;
import br.com.medic.medicsystem.persistence.model.SalarioMd;

@Named("registroagendamento-dao")
@ApplicationScoped
public class RegistroAgendamentoDAO extends RelationalDataAccessObject<RegistroAgendamento> {
	
	
	@SuppressWarnings("unchecked")
	public List<RegistroAtaDTO> getRegistrosAgendamentoRealizados(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){

		String queryStr = " (select ag.dtagendamento as dtregistro, " 
						+ " case "
						+ " when ag.iddependente is null then cli.nmcliente "
						+ "	when ag.iddependente is not null then dep.nmdependente "
						+ " end as nmpaciente, "
						+ " serv.nmservico, serv.idservico,  coalesce(se.boconsulta, false) as boconsulta, rag.vlcomissao, u.nmapelido as nmunidade, u.idunidade, ag.boreturnultimate as boretorno, ag.idagendamento "
						+ " from estatistica.tbregistroagendamento rag " 
						+ " inner join realvida.tbagendamento ag on rag.idagendamento = ag.idagendamento "
						+ " and rag.idregistroagendamento = (select max(idregistroagendamento) from estatistica.tbregistroagendamento where idagendamento = ag.idagendamento) "
						+ " inner join realvida.tbdespesa d on ag.idagendamento = d.idagendamento and (d.boexcluida is false or d.boexcluida is null) and d.iddespesa = (select max(iddespesa) from realvida.tbdespesa where idagendamento = ag.idagendamento) "
						+ " inner join realvida.tbservico serv on ag.idservico = serv.idservico "
						+ " inner join realvida.tbunidade u on ag.idunidade = u.idunidade "
						+ " inner join realvida.tbcliente cli on ag.idcliente = cli.idcliente "
						+ " left join realvida.tbservicoespecialidade se on ag.idespecialidade = se.idespecialidade and ag.idservico = se.idservico "
						+ " left join realvida.tbdependente dep on ag.iddependente = dep.iddependente "
						+ " where ag.dtexclusao is null "
						+ " and ag.instatus = 2 "
						+ " and rag.tipoatendimento = " + tipoAtendimento
						+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
						+ " and ag.idfuncionario = " + idFuncionario
						+ " and (serv.bolaudo is false or serv.bolaudo is null) "
						+ " order by ag.dtagendamento) "
						+ " union "
						+ " (select ag.dtagendamento as dtregistro, "
						+ " case "
						+ " when ag.iddependente is null then cli.nmcliente "
						+ " when ag.iddependente is not null then dep.nmdependente "
						+ " end as nmpaciente, "
						+ " serv.nmservico, serv.idservico,  coalesce(se.boconsulta, false) as boconsulta, rag.vlcomissao, u.nmapelido as nmunidade, u.idunidade, ag.boreturnultimate as boretorno, ag.idagendamento "
						+ " from estatistica.tbregistroagendamento rag  "
						+ " inner join realvida.tbagendamento ag on rag.idagendamento = ag.idagendamento "
						+ " and rag.idregistroagendamento = (select max(idregistroagendamento) from estatistica.tbregistroagendamento where idagendamento = ag.idagendamento) "
						+ " inner join realvida.tbdespesa d on ag.idagendamento = d.idagendamento and (d.boexcluida is false or d.boexcluida is null) "
						+ " and d.iddespesa = (select max(iddespesa) from realvida.tbdespesa where idagendamento = ag.idagendamento and (boexcluida is false or boexcluida is null)) "
						+ " inner join realvida.tbservico serv on ag.idservico = serv.idservico "
						+ " inner join realvida.tbunidade u on ag.idunidade = u.idunidade "
						+ " inner join realvida.tbcliente cli on ag.idcliente = cli.idcliente "
						+ " left join realvida.tbservicoespecialidade se on ag.idespecialidade = se.idespecialidade and ag.idservico = se.idservico "
						+ " left join realvida.tbdependente dep on ag.iddependente = dep.iddependente "
						+ " where ag.dtexclusao is null "
						+ " and ag.instatus = 2 "
						+ " and rag.tipoatendimento = " + tipoAtendimento
						+ " and rag.dtfaturamentolaudo between '" + dtInicio + "' and '" + dtFim + "'"
						+ " and ag.idfuncionario = " + idFuncionario
						+ " and serv.bolaudo is true "
						+ " order by ag.dtagendamento) ";	  
		  

		List<RegistroAtaDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
		        .addScalar("dtRegistro", DateType.INSTANCE)
		        .addScalar("nmPaciente", StringType.INSTANCE)
		        .addScalar("nmServico", StringType.INSTANCE)
		        .addScalar("idServico", IntegerType.INSTANCE)
		        .addScalar("boConsulta", BooleanType.INSTANCE)
		        .addScalar("vlComissao", DoubleType.INSTANCE)		        
		        .addScalar("nmUnidade", StringType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("boRetorno", BooleanType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(RegistroAtaDTO.class)).list();
		
//		if (result == null || result.isEmpty()) {
//			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//		}
		
		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<RegistroAtaDTO> getRegistrosAgendamentoHora(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){

		String queryStr = "select ag.dtagendamento as dtregistro, rag.vlhora, " 
						+ " (SELECT array_to_string "
						+ " ( ARRAY " 
						+ "	( SELECT ra.hratendimentoinicio ||'-'|| ra.hratendimentofim as hratendimento " 
						+ "	from estatistica.tbregistroagendamento ra "
						+ "	inner join realvida.tbagendamento agend on ra.idagendamento = agend.idagendamento "
						+ "	where agend.dtexclusao is null "
						+ "	and agend.instatus = 2 "
						+ "	and agend.dtagendamento = ag.dtagendamento "
						+ "	and agend.idfuncionario = ag.idfuncionario "
//						+ " and agend.idespecialidade = ag.idespecialidade "
						+ " and agend.idunidade = ag.idunidade "						
						+ "	and ra.tipoatendimento = " + tipoAtendimento 
						+ "	group by ra.hratendimentoinicio ||'-'|| ra.hratendimentofim "
						+ "	order by ra.hratendimentoinicio ||'-'|| ra.hratendimentofim "
						+ "	), cast('-' as text) "
						+ "	) AS array_to_string "
						+ " ) AS hratendimento, "
						+ " (SELECT DISTINCT  "
						+ " ( SELECT array_to_string " 
						+ "	( ARRAY " 
						+ " ( SELECT to_char(hrregistro, 'HH24:MI:SS') " 
						+ "	FROM realvida.tbregistroponto " 
						+ "	WHERE idpontounidade = ag.idunidade "
						+ "	and idfuncionario = ag.idfuncionario "
						+ "	and dtregistro = ag.dtagendamento "
						+ "	order by hrregistro "
						+ "	), cast('-' as text) "
						+ "	) AS array_to_string " 
						+ "	) AS hrponto "
						+ " ), ag.idunidade "
						+ " from estatistica.tbregistroagendamento rag "
						+ " inner join realvida.tbagendamento ag on rag.idagendamento = ag.idagendamento "
						+ " where ag.dtexclusao is null "
						+ " and ag.instatus = 2 "
						+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim +"'"
						+ " and ag.idfuncionario = " + idFuncionario
						+ " and rag.tipoatendimento = " + tipoAtendimento
						+ " group by ag.dtagendamento, ag.idunidade, ag.idfuncionario, rag.vlhora "
						+ " order by ag.dtagendamento"; 

		List<RegistroAtaDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
		        .addScalar("dtRegistro", DateType.INSTANCE)
		        .addScalar("hrAtendimento", StringType.INSTANCE)
		        .addScalar("hrPonto", StringType.INSTANCE)	
		        .addScalar("vlHora", DoubleType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(RegistroAtaDTO.class)).list();
		
//		if (result == null || result.isEmpty()) {
//			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//		}
		
		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<RegistroAtaDTO> getRegistrosAgendamentoFixo(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento, Long idUnidade){
		
		String queryStr =
			"select to_char(dtregistro, 'yyyy-mm-dd') as dtregistro, "
			+ " case "
			+ " when (select count(idbloqueio) from realvida.tbbloqueio "
			+ " where inturno in (0,1,2) and idfuncionario = " + idFuncionario + " and dtexclusaobloqueio is null and dtregistro between dtinicio and dtfim and bodesconto is true and idunidade = " + idUnidade + ") > 0 then 2 "
			+ " when (select count(idbloqueio) from realvida.tbbloqueio "
			+ " where idfuncionario = " + idFuncionario + " and dtexclusaobloqueio is null and dtregistro between dtinicio and dtfim and (bodesconto is null or bodesconto is false) and idunidade = " + idUnidade + ") > 0 then 0"
			+ " when (select count(idagendamento) from realvida.tbagendamento "
			+ " where dtexclusao is null and idfuncionario = " + idFuncionario + " and instatus = 2 and dtagendamento = dtregistro and idunidade = " + idUnidade + ") > 0 then 0"				
			+ " else 1 "
			+ " end as statusdesconto, "
			+ " coalesce( cast((select vlsalario from realvida.tbsalariomd where idfuncionario = " + idFuncionario + " and idunidade = " + idUnidade + " and "
			+ " mes = Extract('Month' From cast('" + dtInicio + "' as date)) and ano = Extract('Year' From cast('" + dtInicio + "'  as date))) / "
			+ " ( select sum(turno.qtdturno) from ( "
			+ " select dtregistro "
			+ " from generate_series(date '" + dtInicio + "', date '" + dtFim + "', interval '1 days') as dtregistro "
			+ " where EXTRACT(DOW FROM dtregistro) in "
			+ " (select EXTRACT( DOW FROM ag.dtagendamento) from estatistica.tbregistroagendamento ra "
			+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
			+ " where ag.dtexclusao is null "
			+ " and ag.instatus = 2 "
			+ " and ag.idfuncionario = " + idFuncionario
			+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "' "
			+ " and ag.idunidade = " + idUnidade
			+ " and ra.tipoatendimento = " + tipoAtendimento		
			+ " and ra.boreposicao is false "
			+ " group by ag.dtagendamento) )t1"
			+ " inner join ( select EXTRACT(DOW FROM ag.dtagendamento) as diasemana, ra.qtdturno from estatistica.tbregistroagendamento ra "
			+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
			+ " where ag.dtexclusao is null "
			+ " and ag.instatus = 2 "
			+ " and ag.idfuncionario = " + idFuncionario
			+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "' "
			+ " and ag.idunidade = " + idUnidade
			+ " and ra.tipoatendimento = " + tipoAtendimento			
			+ " and ra.boreposicao is false "
			+ " group by EXTRACT(DOW FROM ag.dtagendamento), ra.qtdturno "
			+ " ) turno on EXTRACT(DOW FROM t1.dtregistro) = turno.diasemana "
			+ " ) as numeric(10,2)), 0) as vldiaria, "			
			+ " cast(" + idUnidade + " as bigint) as idunidade, "
			+ " (SELECT array_to_string(ARRAY "
			+ " ( select case "
			+ " when (select count(idbloqueio) from realvida.tbbloqueio "
			+ " where dtexclusaobloqueio is null and inturno = t2.inturno and idfuncionario = " + idFuncionario
			+ " and dtregistro between dtinicio and dtfim and bodesconto is true and idunidade = " + idUnidade + ") > 0 then t2.inturno || '-' || 1 "
			+ " else t2.inturno || '-' || 0 end as status"
			+ " from ( select EXTRACT(DOW FROM ag.dtagendamento) as diasemana, ra.inturno "
			+ " from estatistica.tbregistroagendamento ra"
			+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento"
			+ " where ag.dtexclusao is null and ag.instatus = 2 "
			+ " and ag.idfuncionario = " + idFuncionario
			+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "' "
			+ " and ra.tipoatendimento = " + tipoAtendimento
			+ " and ag.idunidade = " + idUnidade
			+ " and ra.boreposicao is false "
			+ " group by EXTRACT(DOW FROM ag.dtagendamento), ra.inturno "
			+ " )t2 where t2.diasemana = EXTRACT( DOW FROM dtregistro) "			
			+ " ), ',')) as statusturno, false as boreposicao  "
			+ " from generate_series(date '" + dtInicio + "', date '" + dtFim + "', interval '1 days') as dtregistro "
			+ " where EXTRACT(DOW FROM dtregistro) in "
			+ " (select EXTRACT( DOW FROM ag.dtagendamento) from estatistica.tbregistroagendamento ra "
			+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
			+ " where ag.dtexclusao is null and ag.instatus = 2 and ag.idfuncionario = " + idFuncionario + " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
			+ " and ag.idunidade = " + idUnidade
			+ " and ra.tipoatendimento = " + tipoAtendimento			
			+ " and ra.boreposicao is false "
			+ " group by ag.dtagendamento) "
			+ ""
			+ " union all "
			+ ""
			+ " (select cast(ag.dtagendamento as text) as dtregistro, 0 as statusdesconto, 0 as vldiaria, ag.idunidade, cast('0-0' as text) as statusturno, true as boreposicao "
			+ " from estatistica.tbregistroagendamento ra "
			+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
			+ " where ag.dtexclusao is null and ag.instatus = 2 and ag.idfuncionario = " + idFuncionario + " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
			+ " and ag.idunidade = " + idUnidade
			+ " and ra.tipoatendimento = " + tipoAtendimento
			+ " and ra.boreposicao is true "
			+ " group by ag.dtagendamento, ag.idunidade "
			+ " order by ag.dtagendamento)"; 
				  		   
						
		List<RegistroAtaDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
		        .addScalar("dtRegistro", DateType.INSTANCE)
		        .addScalar("statusDesconto", IntegerType.INSTANCE)		        	
		        .addScalar("vlDiaria", DoubleType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("statusTurno", StringType.INSTANCE)
		        .addScalar("boReposicao", BooleanType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(RegistroAtaDTO.class)).list();
		
//		if (result == null || result.isEmpty()) {
//			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//		}
		
		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<UnidadeDTO> getUnidadeTrabalhadaByTipoAtendimento(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){
		
		String queryStr = "select ag.idunidade, u.nmapelido  from estatistica.tbregistroagendamento ra "
				+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
				+ " inner join realvida.tbunidade u on ag.idunidade = u.idunidade "
				+ " where ag.dtexclusao is null "
				+ " and ag.instatus = 2 "
				+ " and ag.idfuncionario = " + idFuncionario  
				+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
				+ (tipoAtendimento != null ? " and ra.tipoatendimento = " + tipoAtendimento : "")
				+ " group by ag.idunidade, u.nmapelido"
				+ " order by ag.idunidade";
		
		
		List<UnidadeDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("idUnidade", LongType.INSTANCE)
				.addScalar("nmApelido", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(UnidadeDTO.class)).list();
		
//		if (result == null || result.isEmpty()) {
//			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//		}
		
		return result;	
			
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> getUnidadeTrabalhadaByTA(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){
		
		String queryStr = "select ag.idunidade from estatistica.tbregistroagendamento ra "
				+ " inner join realvida.tbagendamento ag on ra.idagendamento = ag.idagendamento "
				+ " inner join realvida.tbunidade u on ag.idunidade = u.idunidade "
				+ " where ag.dtexclusao is null "
				+ " and ag.instatus = 2 "
				+ " and ag.idfuncionario = " + idFuncionario  
				+ " and ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
				+ " and ra.tipoatendimento = " + tipoAtendimento
				+ " group by ag.idunidade, u.nmapelido "
				+ " order by ag.idunidade";
		
		
		List<Long> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("idUnidade", LongType.INSTANCE)
				.addScalar("nmUnidade", StringType.INSTANCE).list();
		
//		if (result == null || result.isEmpty()) {
//			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//		}
		
		return result;	
			
	}
	
	public List<SalarioMd> getSalarioMd(Long idFuncionario, String dtInicio){
		
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date dateIni = null;
		
		Calendar c = Calendar.getInstance();
		try {
			dateIni = format.parse(dtInicio);
			c.setTime(dateIni);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		TypedQuery<SalarioMd> query = entityManager.createQuery(				
		        "SELECT smd from SalarioMd smd where smd.funcionario.id = " + idFuncionario		        
                + " and smd.dtExclusao is null "
                + " and smd.mes = " + (c.get(Calendar.MONTH) + 1)
                + " and smd.ano = " + c.get(Calendar.YEAR)
                + " and smd.BoResponsavelTecnico is true",
                SalarioMd.class);

		List<SalarioMd> result = query.getResultList();

		return result;
		
	}
	
	public List<RegistroAgendamento> getRegistroAgendamentoByAgendamento(Long idAgendamento){
		
		String StrQuery= "SELECT ra from RegistroAgendamento ra "
				+ " where ra.idAgendamento = " + idAgendamento;
		
		try {
			
			TypedQuery<RegistroAgendamento> query = 
					entityManager.createQuery(StrQuery, RegistroAgendamento.class);			
			
			List<RegistroAgendamento> result = query.getResultList();

			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		
		return null;
	}

}
