package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbsubespecialidade")
public class SubEspecialidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUBESPECIALIDADE_ID_SEQ")
	@SequenceGenerator(name = "SUBESPECIALIDADE_ID_SEQ", sequenceName = "realvida.subespecialidade_id_seq", allocationSize = 1)
	@Column(name = "idsubespecialidade")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idsubespecialidadeaux")
	private Long idSubEspecialidadeAux;

	@Basic
	@Column(name = "idsubespecialidadeodonto")
	private Long idSubEspecialidadeOdonto;

	@Basic
	@Column(name = "nmsubespecialidade")
	private String nmSubEspecialidade;

	@ManyToOne
	@JoinColumn(name = "idespecialidade")
	private Especialidade especialidade;

	public SubEspecialidade() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdSubEspecialidadeAux() {
		return idSubEspecialidadeAux;
	}

	public void setIdSubEspecialidadeAux(Long idSubEspecialidadeAux) {
		this.idSubEspecialidadeAux = idSubEspecialidadeAux;
	}

	public Long getIdSubEspecialidadeOdonto() {
		return idSubEspecialidadeOdonto;
	}

	public void setIdSubEspecialidadeOdonto(Long idSubEspecialidadeOdonto) {
		this.idSubEspecialidadeOdonto = idSubEspecialidadeOdonto;
	}

	public String getNmSubEspecialidade() {
		return nmSubEspecialidade;
	}

	public void setNmSubEspecialidade(String nmSubEspecialidade) {
		this.nmSubEspecialidade = nmSubEspecialidade;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}
}