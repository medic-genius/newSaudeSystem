package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(catalog = "realvida", name = "atendimentomedico_anamnesepaciente")
public class AtendimentoMedicoAnamneseView {

	@Id
	@Column(name = "idpreanamnese")
	private Long idPreAnamnese;
	
	@Basic
	@Column(name = "idcliente")
	private Long idCliente;

	@Basic
	@Column(name = "iddependente")
	private Long idDependente;

	@Basic
	@Column(name = "nmpergunta")
	private String nmPergunta;
	
	@Transient
	@Basic
	@Column(name = "inrespostaformatado")
	private String inRespostaFormatado;
	
	@Basic
	@Column(name = "inresposta")
	private Integer inResposta;
	
	@Basic
	@Column(name = "idpreanamnesepaciente")
	private Long idPreanamnesePaciente;



	public Long getIdPreAnamnese() {
		return idPreAnamnese;
	}

	public void setIdPreAnamnese(Long idPreAnamnese) {
		this.idPreAnamnese = idPreAnamnese;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdDependente() {
		return idDependente;
	}

	public void setIdDependente(Long idDependente) {
		this.idDependente = idDependente;
	}

	public String getNmPergunta() {
		return nmPergunta;
	}

	public void setNmPergunta(String nmPergunta) {
		this.nmPergunta = nmPergunta;
	}
	@Transient
	public String getInRespostaFormatado() {
		return inRespostaFormatado;
	}
	@Transient
	public void setInRespostaFormatado(String inRespostaFormatado) {
		this.inRespostaFormatado = inRespostaFormatado;
	}

	

	public Integer getInResposta() {
		return inResposta;
	}

	public void setInResposta(Integer inResposta) {
		this.inResposta = inResposta;
	}

	public Long getIdPreanamnesePaciente() {
		return idPreanamnesePaciente;
	}

	public void setIdPreanamnesePaciente(Long idPreanamnesePaciente) {
		this.idPreanamnesePaciente = idPreanamnesePaciente;
	}

}
