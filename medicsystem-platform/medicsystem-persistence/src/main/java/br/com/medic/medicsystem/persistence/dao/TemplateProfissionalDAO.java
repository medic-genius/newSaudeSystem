package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.TemplateProfissional;

@Named("templateprofissional-dao")
@ApplicationScoped
public class TemplateProfissionalDAO extends RelationalDataAccessObject<TemplateProfissional>{
	public TemplateProfissional getTemplateProfissional(Long id) {
		return searchByKey(TemplateProfissional.class, id);
	}
	
	public List<TemplateProfissional> getTemplateProfissionalPorFuncionario (Long idFuncionario) {
		String query = "SELECT tempprof FROM TemplateProfissonal tempprof WHERE tempprof.dtExclusao IS NULL "
				+ "AND tempprof.espProfissional.funcionario.id = :idFuncionario";
		
		TypedQuery<TemplateProfissional> queryStr = entityManager.createQuery(query, TemplateProfissional.class)
				.setParameter("idFuncionario", idFuncionario);
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public List<TemplateProfissional> getTemplateProfissionalPorEspecialidade (Long idEspecialidade) {
		String query = "SELECT tempprof FROM TemplateProfissonal tempprof WHERE tempprof.dtExclusao IS NULL "
				+ " AND tempprof.espProfissional.especialidade.id = :idEspecialidade";

		TypedQuery<TemplateProfissional> queryStr = entityManager.createQuery(query, TemplateProfissional.class)
				.setParameter("idEspecialidade", idEspecialidade);
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<TemplateProfissional> getTemplateProfissionalPorEspProf (Long idEspecialidade, Long idFuncionario) {
		String query = "SELECT tempprof FROM TemplateProfissional tempprof "
				+ " WHERE tempprof.dtExclusao IS NULL "
				+ " AND tempprof.espProfissional.especialidade.id = :idEspecialidade"
				+ " AND tempprof.espProfissional.funcionario.id = :idFuncionario";

		TypedQuery<TemplateProfissional> queryStr = entityManager.createQuery(query, TemplateProfissional.class)
				.setParameter("idEspecialidade", idEspecialidade)
				.setParameter("idFuncionario", idFuncionario);
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}
