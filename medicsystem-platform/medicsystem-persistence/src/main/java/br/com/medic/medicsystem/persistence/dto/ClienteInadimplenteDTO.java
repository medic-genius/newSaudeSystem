package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.medic.medicsystem.persistence.model.CobrancaContatoView;
import br.com.medic.medicsystem.persistence.model.CobrancaView;
import br.com.medic.medicsystem.persistence.model.views.CobrancaCompartilhadoView;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ClienteInadimplenteDTO implements Serializable{

	private static final long serialVersionUID = 7429028047176843520L;
	
	private Long idCliente;
	private String nmCliente;
	private String nrCPF;
	private String nrTelefone;
	private String nrCelular;
	private String nrRg;
	private String nrCodCliente;
	private String nmEmail;
	private String nmUF;
	private Long idClienteBoleto_sl;
	private String linkCobrancaBoleto_sl;
	private Integer totalDocumentoAberto;
	private Integer qtdeMensalidade;
	private Integer qtdeDespesa;
	private Double totalDivida;
	private Double totalDividaComJuros;
	
	private Double somaTotalDivida;
	private Double somaTotalDividaComJuros;
	
	private String nmCidade;
	private String nmBairro;
	private String nmLogradouro;
	private String nrNumero;
	private String nmComplemento;
	private String nrCEP;
	private String nmPontoReferencia;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date prevPagamento;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dtProximaLigacao;
	
	private String ultimoContato;
	
	private String ultimoNegociador;
	private String ultimaDataContato;	
	
	public ClienteInadimplenteDTO() {

	}
	
	public ClienteInadimplenteDTO(CobrancaView view) {
		this.idCliente = view.getId();
		this.nmCliente = view.getNmCliente();
		this.nrCPF = view.getNrCpf();
		this.nrTelefone = view.getNrTelefone();
		this.nrCelular = view.getNrCelular();
		this.totalDocumentoAberto = (int)(long) view.getQdteDevido();
		this.qtdeDespesa = (int)(long) view.getQtdeDespesa();
		this.qtdeMensalidade = (int)(long) view.getQtdeMensalidade();
		this.totalDivida = view.getSomaTotalDividas();
		Double totalDivida = view.getSomaTotalDividas() != null ? view.getSomaTotalDividas() : 0.0;
		Double totalJuros = view.getSomaTotalJuros() != null ? view.getSomaTotalJuros() : 0.0; 
		Double totalMultas = view.getValorMultaAtraso() != null ? view.getValorMultaAtraso() : 0.0;
		this.totalDividaComJuros =   totalDivida + totalJuros + totalMultas;
		this.prevPagamento = view.getDtPrevisaoPagamento();
		this.dtProximaLigacao = view.getDtProximaLigacao();	
		this.ultimoContato = view.getUltimoContato();
	}
	
	public ClienteInadimplenteDTO(CobrancaContatoView view) {
		this.idCliente = view.getId();
		this.nmCliente = view.getNmCliente();
		this.nrCPF = view.getNrCpf();
		this.nrTelefone = view.getNrTelefone();
		this.nrCelular = view.getNrCelular();
		this.totalDocumentoAberto = (int)(long) view.getQdteDevido();
		this.qtdeDespesa = (int)(long) view.getQtdeDespesa();
		this.qtdeMensalidade = (int)(long) view.getQtdeMensalidade();
		this.totalDivida = view.getSomaTotalDividas();
		Double totalDivida = view.getSomaTotalDividas() != null ? view.getSomaTotalDividas() : 0.0;
		Double totalJuros = view.getSomaTotalJuros() != null ? view.getSomaTotalJuros() : 0.0; 
		Double totalMultas = view.getValorMultaAtraso() != null ? view.getValorMultaAtraso() : 0.0;
		this.totalDividaComJuros =   totalDivida + totalJuros + totalMultas;
		this.prevPagamento = view.getDtPrevisaoPagamento();
		this.dtProximaLigacao = view.getDtProximaLigacao();
		this.ultimoContato = view.getUltimoContato();
	}
	
	public ClienteInadimplenteDTO(CobrancaCompartilhadoView view) {
		this.idCliente = view.getId();
		this.nmCliente = view.getNmCliente();
		this.nrCPF = view.getNrCpf();
		this.nrTelefone = view.getNrTelefone();
		this.nrCelular = view.getNrCelular();
		this.nrRg = view.getNrRg() != null ? view.getNrRg() : "";
		this.nrCodCliente = view.getNrCodCliente();
		this.nmEmail = view.getNmEmail() != null ? view.getNmEmail() : "";
		this.nmLogradouro = view.getNmLogradouro() != null ? view.getNmLogradouro() : "";
		this.nrCEP = view.getNrCEP() != null ? view.getNrCEP() : "";
		this.idClienteBoleto_sl = view.getIdClienteBoleto_sl() != null ? view.getIdClienteBoleto_sl() : 0L;
		this.linkCobrancaBoleto_sl = view.getLinkCobrancaBoleto_sl() != null ? view.getLinkCobrancaBoleto_sl() : "";
		this.nrNumero = view.getNrNumero() != null ? view.getNrNumero() : "";
		this.nmBairro = view.getNmBairro() != null ? view.getNmBairro() : "";
		this.nmCidade = view.getNmCidade() != null ? view.getNmCidade() : "";
		this.nmUF = view.getNmUF() != null ? view.getNmUF() : "";
		this.totalDocumentoAberto = (int)(long) view.getQdteDevido();
		this.qtdeDespesa = (int)(long) view.getQtdeDespesa();
		this.qtdeMensalidade = (int)(long) view.getQtdeMensalidade();
		this.totalDivida = view.getSomaTotalDividas();
		Double totalDivida = view.getSomaTotalDividas() != null ? view.getSomaTotalDividas() : 0.0;
		Double totalJuros = view.getSomaTotalJuros() != null ? view.getSomaTotalJuros() : 0.0; 
		Double totalMultas = view.getValorMultaAtraso() != null ? view.getValorMultaAtraso() : 0.0;
		this.totalDividaComJuros =   totalDivida + totalJuros + totalMultas;
		this.prevPagamento = view.getDtPrevisaoPagamento();
		this.dtProximaLigacao = view.getDtProximaLigacao();
	}
	
	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCPF() {
		if (nrCPF != null && nrCPF.length() == 11) {
			Pattern pattern = Pattern
			        .compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCPF);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3-$4");
		}
		
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Integer getTotalDocumentoAberto() {
		return totalDocumentoAberto;
	}

	public void setTotalDocumentoAberto(Integer totalDocumentoAberto) {
		this.totalDocumentoAberto = totalDocumentoAberto;
	}

	public Double getTotalDivida() {
		return totalDivida;
	}

	public void setTotalDivida(Double totalDivida) {
		this.totalDivida = totalDivida;
	}

	public Double getTotalDividaComJuros() {
		return totalDividaComJuros;
	}

	public void setTotalDividaComJuros(Double totalDividaComJuros) {
		this.totalDividaComJuros = totalDividaComJuros;
	}

	public Date getPrevPagamento() {
		return prevPagamento;
	}

	public void setPrevPagamento(Date prevPagamento) {
		this.prevPagamento = prevPagamento;
	}

	public Date getDtProximaLigacao() {
		return dtProximaLigacao;
	}

	public void setDtProximaLigacao(Date dtProximaLigacao) {
		this.dtProximaLigacao = dtProximaLigacao;
	}

	public Integer getQtdeMensalidade() {
		return qtdeMensalidade;
	}

	public void setQtdeMensalidade(Integer qtdeMensalidade) {
		this.qtdeMensalidade = qtdeMensalidade;
	}

	public Integer getQtdeDespesa() {
		return qtdeDespesa;
	}

	public void setQtdeDespesa(Integer qtdeDespesa) {
		this.qtdeDespesa = qtdeDespesa;
	}

	public String getNmCidade() {
		return nmCidade;
	}

	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNrCEP() {
		return nrCEP;
	}

	public void setNrCEP(String nrCEP) {
		this.nrCEP = nrCEP;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNrRg() {
		return nrRg;
	}

	public void setNrRg(String nrRg) {
		this.nrRg = nrRg;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmUF() {
		return nmUF;
	}

	public void setNmUF(String nmUF) {
		this.nmUF = nmUF;
	}

	public Long getIdClienteBoleto_sl() {
		return idClienteBoleto_sl;
	}

	public void setIdClienteBoleto_sl(Long idClienteBoleto_sl) {
		this.idClienteBoleto_sl = idClienteBoleto_sl;
	}

	public String getLinkCobrancaBoleto_sl() {
		return linkCobrancaBoleto_sl;
	}

	public void setLinkCobrancaBoleto_sl(String linkCobrancaBoleto_sl) {
		this.linkCobrancaBoleto_sl = linkCobrancaBoleto_sl;
	}

	public Double getSomaTotalDivida() {
		return somaTotalDivida;
	}

	public void setSomaTotalDivida(Double somaTotalDivida) {
		this.somaTotalDivida = somaTotalDivida;
	}

	public Double getSomaTotalDividaComJuros() {
		return somaTotalDividaComJuros;
	}

	public void setSomaTotalDividaComJuros(Double somaTotalDividaComJuros) {
		this.somaTotalDividaComJuros = somaTotalDividaComJuros;
	}

	public String getUltimoContato() {
		return ultimoContato;
	}

	public void setUltimoContato(String ultimoContato) {
		this.ultimoContato = ultimoContato;
	}

	public String getUltimoNegociador() {
				
		if(ultimoContato != null)		
			return ultimoContato.split("-")[0];				
				
		return ultimoNegociador;
	}

	public String getUltimaDataContato() {
		
		if(ultimoContato != null)		
			return ultimoContato.split("-")[1] + " " + ultimoContato.split("-")[2];
		
		return ultimaDataContato;
	}
	
}