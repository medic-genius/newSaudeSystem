package br.com.medic.medicsystem.persistence.security.dto;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class KeycloakUserRolesDTO {

	private ArrayList<?> realmMappings;

	private LinkedHashMap<String, LinkedHashMap<String, ?>> clientMappings;

	public ArrayList<?> getRealmMappings() {
		return realmMappings;
	}

	public void setRealmMappings(ArrayList<?> realmMappings) {
		this.realmMappings = realmMappings;
	}

	public LinkedHashMap<String, LinkedHashMap<String, ?>> getClientMappings() {
		return clientMappings;
	}

	public void setClientMappings(
	        LinkedHashMap<String, LinkedHashMap<String, ?>> clientMappings) {
		this.clientMappings = clientMappings;
	}

	public class KeycloakMappingDTO {
		private String id;
		private String name;
		private String composite;

		public String getComposite() {
			return composite;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public void setComposite(String composite) {
			this.composite = composite;
		}

		public void setId(String id) {
			this.id = id;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

}
