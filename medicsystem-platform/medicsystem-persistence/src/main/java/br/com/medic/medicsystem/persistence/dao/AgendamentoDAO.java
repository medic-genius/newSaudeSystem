package br.com.medic.medicsystem.persistence.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimeType;

import br.com.medic.medicsystem.persistence.appmobile.dto.AgendamentoLembreteDTO;
import br.com.medic.medicsystem.persistence.appmobile.dto.PagedListDTO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.views.HistoricoAgendamentosCliente;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.CalAgendaMedicoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.Carencia;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;
import br.com.medic.medicsystem.persistence.model.enums.TipoPessoa;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoProfissionalView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoView;

@Named("agendamento-dao")
@ApplicationScoped
public class AgendamentoDAO extends RelationalDataAccessObject<Agendamento> {

	public Despesa getDespesaAgendamento(Long idAgendamento) {
		String queryStr = "SELECT d FROM Despesa d " + " WHERE "
		        + " d.agendamento.id = " + idAgendamento
		        + " AND (d.boExcluida is null or d.boExcluida=false)";

		TypedQuery<Despesa> query = entityManager.createQuery(queryStr,
		        Despesa.class);

		Despesa result = null;

		query.setMaxResults(1);

		try {

			result = query.getSingleResult();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return result;
		}
	}

	public CoberturaPlano getCoberturaPlano(Long idPlano, Long idServico) {

		String queryStr = "SELECT cp FROM CoberturaPlano cp where cp.plano.id = "
		        + idPlano + " and cp.servico.id = " + idServico;

		TypedQuery<CoberturaPlano> query = entityManager.createQuery(queryStr,
		        CoberturaPlano.class);

		query.setMaxResults(1);
		CoberturaPlano result = query.getSingleResult();

		return result;
	}

	public List<Mensalidade> getMensalidadesPagas(Long idContrato) {

		TypedQuery<Mensalidade> query = entityManager.createQuery(
		        "SELECT m from Mensalidade m where m.contrato.id = "
		                + idContrato + " and m.contrato.situacao = 0 "
		                + " and m.dataExclusao is null and m.inStatus = 1",
		        Mensalidade.class);

		List<Mensalidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Contrato getContratoPorEmpresaCliente(Long idEmpresaCliente) {

		TypedQuery<Contrato> query = entityManager
		        .createQuery(
		                "SELECT c FROM Contrato c where c.boEmpresaCliente = true and c.situacao = 0 and c.empresaCliente.id = "
		                        + idEmpresaCliente, Contrato.class);

		query.setMaxResults(1);
		Contrato result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Carencia getCarencia(Long idContrato, Long idServico) {

		String queryStr = "SELECT c FROM Carencia c where c.contrato.id = "
		        + idContrato + " and c.servico.id = " + idServico;
		
		try {			
		
			TypedQuery<Carencia> query = entityManager.createQuery(queryStr,Carencia.class);

			query.setMaxResults(1);
			Carencia result = query.getSingleResult();
			
			return result;

		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}		
		
	}

	public List<ContratoCliente> getContratoCliente(Long idContrato) {
		TypedQuery<ContratoCliente> query = entityManager.createQuery(
		        "SELECT cc FROM ContratoCliente cc where cc.contrato.id = "
		                + idContrato, ContratoCliente.class);

		List<ContratoCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<ContratoCliente> getContratoClientePorCliente(Long idCliente) {
		TypedQuery<ContratoCliente> query = entityManager.createQuery(
		        "SELECT cc FROM ContratoCliente cc where cc.cliente.id = "
		                + idCliente, ContratoCliente.class);

		List<ContratoCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public ContratoCliente getContratoClientePorClienteId(Long idCliente) {
		TypedQuery<ContratoCliente> query = entityManager.createQuery(
		        "SELECT cc FROM ContratoCliente cc where cc.cliente.id = "
		                + idCliente, ContratoCliente.class);

		ContratoCliente result = null;

		query.setMaxResults(1);

		try {

			result = query.getSingleResult();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return result;
		}
	}

	public List<ContratoDependente> getContratosDependentes(Long idContrato) {
		TypedQuery<ContratoDependente> query = entityManager.createQuery(
		        "SELECT cd FROM ContratoDependente cd where cd.contrato.id = "
		                + idContrato, ContratoDependente.class);

		List<ContratoDependente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Cliente getCliente(Long idCliente) {

		return searchByKey(Cliente.class, idCliente);
	}

	public List<Despesa> getDespesasPorAgendamento(Long idAgendamento) {
		TypedQuery<Despesa> query = entityManager.createQuery(
		        "SELECT d FROM Despesa d where d.agendamento.id = "
		                + idAgendamento
		                + " and (d.boExcluida is null or d.boExcluida=false) ",
		        Despesa.class);

		List<Despesa> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public String getNovoCodigoAgendamento() {

		String queryStr = "select coalesce(nragendamento, '0') as nragendamento from realvida.tbagendamento order by idagendamento desc limit 1";
		
		try {
			
			Query q = findByNativeQuery(queryStr);
			String result = (String) q.getSingleResult();

			if (result == null) {
				throw new ObjectNotFoundException(
				        "Um novo codigo de agendamento nao pode ser gerado");
			}
			
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return "0";
		}
		


	}

	public List<Parcela> getParcelas(Long idCliente) {

		TypedQuery<Parcela> query = entityManager.createQuery(
		        "from Parcela p where p.dtPagamento is null "
		                + " and p.despesa.inSituacao = 0"
		                + " and p.inFormaPagamento IN (1,5)"
		                + " and p.despesa.contratoCliente.cliente.id = "
		                + idCliente, Parcela.class);

		List<Parcela> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<Parcela> getParcelasDespesa(Long idDespesa) {

		TypedQuery<Parcela> query = entityManager.createQuery(
		        "from Parcela p where (p.boExcluida is false or p.boExcluida is null) "
		                + " and p.despesa.id = "
		                + idDespesa, Parcela.class);

		
		try {
			if(query.getResultList() != null && query.getResultList().size() > 0)
				return query.getResultList();
			else 
				return null;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

	public Double getSomaValorDebitoBoleto(Integer inFormaPagamento,
	        String idCliente) {

		String query = "select coalesce(sum(vlparcela), 0.0) as vlsoma from realvida.tbparcela as parc "
		        + " join realvida.tbdespesa as desp on desp.iddespesa = parc.iddespesa "
		        + " join realvida.tbcontratocliente as cc on cc.idcontratocliente = desp.idcontratocliente "
		        + " join realvida.tbcontrato as c on c.idcontrato = cc.idcontrato "
		        + " where c.insituacao = 0 and parc.informapagamento = "
		        + inFormaPagamento.toString()
		        + " and parc.dtpagamento is null and (parc.boExcluida is null or parc.boExcluida is false) and (parc.boReparcelada is null or parc.boReparcelada is false) "
		        + " and cc.idcliente = " + idCliente;

		return (Double) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .addScalar("vlsoma", DoubleType.INSTANCE).uniqueResult();
	}

	public List<Contrato> getContratosPorCliente(Long idCliente) {

		TypedQuery<Contrato> query = entityManager
		        .createQuery(
		                "Select distinct(cc.contrato) from ContratoCliente cc where "
		                        + "(cc.contrato.situacao is null or cc.contrato.situacao = 0) "
		                        + "and cc.cliente.id = " + idCliente,
		                Contrato.class);

		List<Contrato> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<Dependente> getDependentesPorCliente(Long idCliente) {

		TypedQuery<Dependente> query = entityManager
		        .createQuery(
		                "Select distinct(cd.dependente) from ContratoDependente cd where "
		                        + "cd.blAutorizaDebAut is true and cd.dependente.cliente.id = "
		                        + idCliente, Dependente.class);

		List<Dependente> result = query.getResultList();

		return result;
	}

	public List<ContratoCliente> getMatriculasPorCliente(Long idCliente) {

		TypedQuery<ContratoCliente> query = entityManager
		        .createQuery(
		                "from ContratoCliente cc where (cc.contrato.situacao is null or  cc.contrato.situacao = 0) "
		                        + "and cc.orgao is not null and cc.nrMatricula is not null and cc.cliente.id = "
		                        + idCliente, ContratoCliente.class);

		List<ContratoCliente> result = query.getResultList();

		if (result.isEmpty()) {
			return null;
		}

		return result;
	}

	public ServicoProfissional getListaProfissionais(Long idServico,
	        Long idProfissional) {

		String queryStr = " from ServicoProfissional sp"
		        + " where sp.servico.id = " + idServico
		        + " and sp.profissional.id = " + idProfissional
		        + " and sp.profissional.dtExclusao is null ";

		TypedQuery<ServicoProfissional> query = entityManager.createQuery(
		        queryStr, ServicoProfissional.class);

		query.setMaxResults(1);
		ServicoProfissional result = query.getSingleResult();
		
		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		
		return result;
	}

	public List<CoberturaPlano> getListCoberturaPlano(Long idPlano,
	        Long idServico) {

		TypedQuery<CoberturaPlano> query = entityManager.createQuery(
		        "SELECT cp FROM CoberturaPlano cp where cp.plano.id = "
		                + idPlano + " and cp.servico.id = " + idServico,
		        CoberturaPlano.class);

		List<CoberturaPlano> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public String getNumeracaoTitulo() {

		String query = "select lpad(cast((max(cast(nrtitulo as int8))+1) as varchar), 10, '0') as nrtitulo from realvida.tbtituloda;";

		return (String) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .addScalar("nrtitulo", StringType.INSTANCE).uniqueResult();
	}

	public Agendamento getAgendamento(Long id) {
		return searchByKey(Agendamento.class, id);
	}
	
	public Despesa getDespesa(Long id) {
		return searchByKey(Despesa.class, id);
	}
	
	
	public List<AgendamentoView> getAgendamentosMedico(Long idProfissional, String data, Long idUnidade) {
		String queryStr = "SELECT av FROM AgendamentoView av "
		        + "WHERE av.dtExclusao is null " + "AND av.profissional.id = "
		        + idProfissional;
		queryStr += " AND av.dtAgendamento = '" + data +"'";
		
		if(idUnidade != null)
			queryStr += " AND av.unidade.id = " + idUnidade;
//		if (!todos) {
//			queryStr += " AND av.dtAgendamento >= current_date ";
//		}
		
		queryStr += " ORDER BY av.dtAgendamento DESC, av.hrAgendamento ASC";
		TypedQuery<AgendamentoView> query = entityManager.createQuery(queryStr,
		        AgendamentoView.class);
		List<AgendamentoView> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<AgendamentoView> getAgendamentosMedicoPorPeriodo(String dtInicio, String dtFim, Long idUnidade) {
		String queryStr = "SELECT av FROM AgendamentoView av "
		        + "WHERE av.dtExclusao is null "; 
		
		if(idUnidade != null)
			queryStr += " AND av.unidade.id = " + idUnidade;
		
		queryStr += " AND av.dtAgendamento between '" + dtInicio +"' and '" + dtFim +"'";	
				
		queryStr += " ORDER BY av.unidade.id, av.especialidade.nmEspecialidade, av.profissional.nmFuncionario, av.dtAgendamento DESC, av.hrAgendamento ASC";
		
		try {
			TypedQuery<AgendamentoView> query = entityManager.createQuery(queryStr, AgendamentoView.class);
			List<AgendamentoView> result = query.getResultList();
			return result;
		} catch(ObjectNotFoundException e) {
			return null;
		}
		
	}
	
	public AgendamentoView getAgendamentoViewById(Long idAgendamento) {
		String queryStr = "SELECT av FROM AgendamentoView av WHERE av.id = :idAgendamento";
		try {
			TypedQuery<AgendamentoView> query = entityManager.createQuery(queryStr,
			        AgendamentoView.class)
					.setParameter("idAgendamento", idAgendamento);
			AgendamentoView result = query.getSingleResult();
			return result;
		} catch(ObjectNotFoundException e) {
		}
		return null;
	}
	
	
	public List<AtendimentomedicoView> getAtendimentoMedico(Long idProfissional, Long idEspecialidade) {
		
		String queryStr = "SELECT ag FROM AtendimentomedicoView ag "
				+ " , OrdemAgendamento ord "
				+ "  WHERE ord.idAgendamento = ag.idAgendamento AND (ord.boExcluido IS FALSE OR ord.boExcluido IS NULL) AND ag.idmedico = "
                + idProfissional;
		
		if(idEspecialidade != null )
			queryStr +=" and ag.idEspecialidade = " + idEspecialidade;
		
		queryStr += " ORDER BY ag.hrInicio ASC, ord.nrOrdem ASC";
		
		TypedQuery<AtendimentomedicoView> query = entityManager.createQuery(queryStr,
		                AtendimentomedicoView.class);
		
		
		List<AtendimentomedicoView> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
		
	}
	private String formatarIds (List<Long> listIds){
		String ids = ""; 
		
		if (listIds != null && listIds.size() > 0 ){
			if (listIds.size() == 1 ){
				return listIds.get(0).toString();
			}
		for (Long id : listIds) {
			ids += id + ",";
		}
		return ids.substring(0, ids.length() - 1);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List <CalAgendaMedicoDTO> getAgendaMedico(List<Long> ids, String dtInicio, String dtFim,List<Long> idsUnidade,List<Long> idsFuncionario){
		
		String listIds = formatarIds(ids);
		String listUnidades = formatarIds(idsUnidade);
		String listFuncionario = formatarIds(idsFuncionario);
	
		

	String query =	
			
	" SELECT * from ( "
	+ " SELECT dta.idunidade, dta.nmapelido, dta.idfuncionario, dta.nmfuncionario, dta.idespecialidade, dta.nmespecialidade, dta.intipoconsulta,"
			+ "	dta.indiasemana, dta.hrinicio, dta.hrfim, dta.hrtempoconsulta, dta.dtatend, (dta.qtpacienteatender - "
			+ "	(select count(idagendamento) from realvida.tbagendamento agend where agend.dtagendamento = dta.dtatend and agend.idfuncionario = dta.idfuncionario and agend.hragendamento between dta.hrinicio and dta.hrfim and agend.dtexclusao is null)) as qtdatend, dta.boparticular  "
	+ "	FROM "
	+ " ( "
		+ "SELECT fun.idfuncionario, fun.nmfuncionario, atdprof.idunidade, un.nmapelido, esppro.idespecialidade, esp.nmespecialidade, atdprof.intipoconsulta, "
				+ "	atdprof.indiasemana, atdprof.hrinicio, atdprof.hrfim, atdprof.hrtempoconsulta, atdprof.qtpacienteatender, "
				+ "	cast (to_char(generate_series('"+dtInicio+"', '"+dtFim+"', INTERVAL '1 day'), 'YYYY-MM-DD') as date) as dtatend, atdprof.boparticular, "
				+ " atdprof.inturno "
		+ "FROM realvida.tbfuncionario fun "
		+ "inner join realvida.tbprofissional prof on fun.idfuncionario = prof.idfuncionario and fun.dtexclusao is null and intipoprofissional = 0 "
		+ "inner join realvida.tbatendimentoprofissional atdprof on prof.idfuncionario = atdprof.idfuncionario "
		+ "inner join realvida.tbespprofissional esppro on prof.idfuncionario = esppro.idfuncionario and atdprof.idespprofissional = esppro.idespprofissional "
		+ "inner join realvida.tbespecialidade esp on esppro.idespecialidade = esp.idespecialidade "
		+ "inner join realvida.tbunidade un on atdprof.idunidade = un.idunidade "
		+ "where esppro.idespecialidade in ("+listIds+") "
		+ "ORDER BY fun.nmfuncionario, atdprof.indiasemana "
	+ " ) as dta "
	+ "	WHERE dta.indiasemana = ( SELECT EXTRACT(ISODOW FROM dta.dtatend) ) "
		+ " AND dta.idunidade in " + (listUnidades != null && listUnidades.length() > 0 ? "("+listUnidades+") " : "(1, 2, 7) ")
		+ (listFuncionario != null && listFuncionario.length() > 0 ? "and dta.idfuncionario in ("+listFuncionario+") ": "")
		+ "	and dta.dtatend not in (select cast(to_char(generate_series(bloq.dtinicio, bloq.dtfim, INTERVAL '1 day'), 'YYYY-MM-DD') as date) as dtbloqueio"
		+ " from realvida.tbbloqueio bloq where bloq.idfuncionario = dta.idfuncionario and bloq.dtexclusaobloqueio is null and bloq.idunidade = dta.idunidade and bloq.bonaoativo is false and bloq.dtfim >= current_date"
		+ " AND ( (bloq.dtinicio between '"+dtInicio+"' and '"+dtFim+"' OR bloq.dtfim between '"+dtInicio+"' and '"+dtFim+"')"
		+ " OR ('"+dtInicio+"' between bloq.dtinicio and bloq.dtfim and ('"+dtFim+"' between bloq.dtinicio and bloq.dtfim )) )"
		+ " AND (bloq.inturno = -1 OR bloq.inturno = dta.inturno) ORDER BY bloq.dtinicio) "
		+ "	and (select sum(qtpacienteatender) from realvida.tbatendimentoprofissional where idfuncionario = dta.idfuncionario and indiasemana = ( SELECT EXTRACT(ISODOW FROM dta.dtatend) )) "
		+ " > (select count(idagendamento) from realvida.tbagendamento agend where agend.dtagendamento = dta.dtatend and agend.idfuncionario = dta.idfuncionario and agend.dtexclusao is null) "
		+ "	and dta.dtatend >= current_date and dta.dtatend < current_date + interval '1 year' "
	+ "	ORDER BY dta.dtatend, dta.hrinicio, dta.nmfuncionario, dta.indiasemana"
	+ ") as t1 "
	+" where t1.qtdatend > 0 ";
			
		
		List<CalAgendaMedicoDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("nmApelido", StringType.INSTANCE)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)
		        .addScalar("inDiaSemana", IntegerType.INSTANCE)
		        .addScalar("hrInicio", TimeType.INSTANCE)
		        .addScalar("hrFim", TimeType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("qtdAtend", IntegerType.INSTANCE)
		        .addScalar("inTipoConsulta", IntegerType.INSTANCE)
		        .addScalar("boParticular", BooleanType.INSTANCE)
		        .addScalar("dtAtend", DateType.INSTANCE)
		        
		        .setResultTransformer(
		                Transformers.aliasToBean(CalAgendaMedicoDTO.class)).list();
		
		try {
			
			if(result != null && result.size() > 0){
				System.out.println("result "+ result.get(0));
			}else 
				System.out.println("sem resultados");
			return result;
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
//	public boolean verificarHorarioDuplicado(Agendamento agendamento){
//		
//			DateFormat dataformat = new SimpleDateFormat( "dd/MM/yyyy" );
//		
//		
//			String queryMesmoHorario = "FROM Agendamento ag "
//									  + " WHERE ag.dtExclusao IS NULL AND ag.dtAgendamento = "+dataformat.format(agendamento.getDtAgendamento())
//									  + " AND ag.unidade.id = "+ agendamento.getUnidade().getId()
//									  + " AND ag.especialidade.id = "+ agendamento.getEspecialidade().getId()
//									  +  (agendamento.getSubEspecialidade() != null && agendamento.getSubEspecialidade().getId() != null ? " AND ag.subEspecialidade.id = "+agendamento.getSubEspecialidade().getId() : "")
//									  +  (agendamento.getProfissional() != null && agendamento.getProfissional().getId() != null ? " AND ag.profissional.id = "+ agendamento.getProfissional().getId() : "")
//									  + " AND ag.inTipoAtendimento = "+ agendamento.getInTipoAtendimento()
//									  + " AND ag.inTipoConsulta = "+ agendamento.getInTipoConsulta()
//									  + " AND ag.hrAgendamento = '"+ agendamento.getHrAgendamentoFormatado() +"'";		
//			
//			TypedQuery<Agendamento> query = entityManager.createQuery(queryMesmoHorario, Agendamento.class);
//			
//			
//			try {
//				
//				List<Agendamento> result = query.getResultList();
//				
//				if(result != null && result.size() > 0)
//					return true;
//				else 
//					return false;
//				
//			} catch (Exception e) {
//				System.out.println(e);
//				return false;
//			}
//
//			
//	}
	
	public boolean getHorariosConsultasMarcadasNoDia(Agendamento agendamento) {
		
		Locale locale = new Locale("pt", "BR");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy", locale);
		
		Calendar dtagendamento = Calendar.getInstance();

		try {
			dtagendamento.setTime(df.parse(agendamento.getDtAgendamentoFormatado()));

		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
		
		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
		

		String queryStr = "SELECT agv.hragendamento "
		        + " FROM realvida.tbagendamento_vw agv "
		        + " WHERE agv.dtexclusao is null "
		        + " AND agv.inTipoConsulta = " + agendamento.getInTipoConsulta()
		        + " AND agv.idfuncionario = " + agendamento.getProfissional().getId()
		        + " AND agv.idunidade = " + agendamento.getUnidade().getId()
		        + " AND agv.idespecialidade = " + agendamento.getEspecialidade().getId()
		        + " AND agv.hragendamento = '" + agendamento.getHrAgendamentoFormatado()+"'";

		if (agendamento.getSubEspecialidade() != null && agendamento.getSubEspecialidade().getId() != null ) {
			queryStr += " AND agv.idsubespecialidade = " + agendamento.getSubEspecialidade().getId();
		}
		
		if (agendamento.getInTipoAgendamento() != null) {
			queryStr += " AND agv.intipoagendamento = " + agendamento.getInTipoAgendamento();
		}
		
		if (agendamento.getInTipoAtendimento() != null) {
			queryStr += " AND agv.intipoatendimento = " + agendamento.getInTipoAtendimento();
		}
		
		
			
		queryStr += " AND agv.intipo = 0 " ;
		
		
		queryStr += " AND agv.dtAgendamento = '" +   sqlFormatter.format(dtagendamento.getTime())
		        + "' ORDER BY agv.dtagendamento";

		Query q = findByNativeQuery(queryStr);
		@SuppressWarnings("unchecked")
		List<String> result = (List<String>) q.getResultList();

		if(result != null && result.size() > 0)
			return true;
		else 
			return false;
	}
	
	
public List<AtendimentomedicoView> getAtendimentoEnfermagem(List<Long> idEspecialidade, Long idUnidade, List<Long> idProfissional) {
		
		String listIds = "";

		if (idEspecialidade != null && idEspecialidade.size() == 1) {
			listIds = idEspecialidade.get(0).toString();
		} else {
			for (Long id : idEspecialidade) {
				listIds += id + ",";
			}
			listIds = listIds.substring(0, listIds.length() - 1);
		}
		
		String listIdProfissional = "";
		if(idProfissional != null && idProfissional.size() > 0 ){
			if (idProfissional.size() == 1) {
				listIdProfissional = idProfissional.get(0).toString();
			} else {
				for (Long id : idProfissional) {
					listIdProfissional += id + ",";
				}
				listIdProfissional = listIdProfissional.substring(0, listIdProfissional.length() - 1);

			}
		}
		
		
		String queryStr = "SELECT ag FROM AtendimentomedicoView ag WHERE ag.idEspecialidade IN ( "+listIds+") AND ag.boTriagem IS TRUE";
		
		if(idUnidade != null )
			queryStr+=" AND idUnidade = "+idUnidade;
		
		if(idProfissional != null && idProfissional.size() > 0)
			queryStr+=" AND idmedico IN ("+listIdProfissional+")";
		
		queryStr += " ORDER BY ag.horaPresenca desc";
		
		TypedQuery<AtendimentomedicoView> query = entityManager.createQuery(queryStr,
		                AtendimentomedicoView.class);
		
		
		List<AtendimentomedicoView> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
		
	}

	public Date getUltimoAgendamentoFaltoso(Long idPaciente, String tipoPaciente){
		
		String queryStr ="SELECT MAX(ag.dtagendamento) AS maxdate FROM realvida.tbagendamento ag where ag.instatus = 3 AND ag.dtexclusao IS NULL AND ag.intipo = 0 ";
		
		if(tipoPaciente.equals("C"))
			queryStr += " AND ag.idcliente = "+ idPaciente;
		else
			queryStr += " AND ag.iddependente = "+ idPaciente;
		
		Date resultDate = (Date) findByNativeQuery(queryStr).unwrap(SQLQuery.class)
        .addScalar("maxdate", DateType.INSTANCE).uniqueResult();
		
		if(resultDate != null)
			return resultDate;
		else 
			return null;
		

		
	}
	
	public AtendimentoProfissional getConsultorioPorEspecialidadeProfissional(Long idProfissional, Long idEspecialidade, Long idUnidade, String hrAgendamento, Long idServico, Integer diaSemana){
		
		
		String query = "SELECT atend "
				+" FROM AtendimentoProfissional atend, AtendimentoServProfissional atserv WHERE atend.profissional.id = "+idProfissional
				+" AND atend.espProfissional.especialidade.id = "+idEspecialidade+" AND atend.unidade.id ="+idUnidade
				+" AND atend.inDiaSemana = "+diaSemana+" AND '"+hrAgendamento+"' BETWEEN atend.hrInicio AND atend.hrFim"
				+" AND atend.id = atserv.idAtendimentoProfissional"
				+" AND atserv.idServico = "+idServico
				+" AND atserv.dtExclusao IS NULL";
		
		TypedQuery<AtendimentoProfissional> result = entityManager
		        .createQuery(query, AtendimentoProfissional.class);
		
		try {
			AtendimentoProfissional listresult = result.getSingleResult();
			if(listresult != null && listresult.getId() != null)
				return listresult;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return null;
	}
	
public Date getUltimaDespesaCoberta(Long idServico, Long idContrato, Integer validadeContratual, Long idCliente, Long idDependente, Long idPlano){
		
		String queryStr = "";
		
		if(idDependente != null){
			
			queryStr = " select t1.dtdespesa from ("
					+" select desp.* from realvida.tbdespesa desp"
					+" inner join realvida.tbdespesaservico despserv on desp.iddespesa = despserv.iddespesa"
					+" inner join realvida.tbcontratodependente cd on desp.idcontratodependente = cd.idcontratodependente"
					+" inner join realvida.tbcontrato c on c.idcontrato = cd.idcontrato"
					+" left join realvida.tbagendamento ag on ag.idagendamento = desp.idagendamento and (ag.instatus = 2 or ag.instatus = 1 or ag.instatus = 4) and ag.dtexclusao is null"
					+" where desp.informapagamento = 10 and desp.insituacao = 1"
					+" and (desp.boexcluida is null or desp.boexcluida = false)"
					+" and despserv.idservico = :idServico"
					+" and cd.idcontrato = :idContrato"
					+" and cd.iddependente = :idPaciente"
					+" and c.idplano = :idPlano"
					+" and ((desp.idagendamento is not null and ag.idagendamento is not null) or (desp.idagendamento is null and ag.idagendamento is null))"
					+" order by desp.dtdespesa desc"
					+" limit 1"
					+" )t1"
					+" where current_date between t1.dtdespesa and (t1.dtdespesa + interval '"
					+(validadeContratual != null ?
							validadeContratual : 0)+" days')";
			
		}else {
			queryStr = " select t1.dtdespesa from ("
					+" select desp.* from realvida.tbdespesa desp"
					+" inner join realvida.tbdespesaservico despserv on desp.iddespesa = despserv.iddespesa"
					+" inner join realvida.tbcontratocliente cc on desp.idcontratocliente = cc.idcontratocliente"
					+" inner join realvida.tbcontrato c on c.idcontrato = cc.idcontrato"
					+" left  join realvida.tbagendamento ag on ag.idagendamento = desp.idagendamento and (ag.instatus = 2 or ag.instatus = 1 or ag.instatus = 4) and ag.dtexclusao is null and ag.iddependente is null "
					+" where desp.informapagamento = 10 and desp.insituacao = 1"
					+" and (desp.boexcluida is null or desp.boexcluida = false)"
					+" and despserv.idservico = :idServico"
					+" and cc.idcontrato = :idContrato"
					+" and cc.idcliente = :idPaciente"
					+" and c.idplano = :idPlano"
					+" and ((desp.idagendamento is not null and ag.idagendamento is not null) or (desp.idagendamento is null and ag.idagendamento is null))"
					+" order by desp.dtdespesa desc"
					+" limit 1"
					+" )t1"
					+" where current_date between t1.dtdespesa and (t1.dtdespesa + interval '"
					+(validadeContratual != null ?
							validadeContratual : 0)+" days') and t1.idcontratodependente is null";
		}
		
		
		
		Query query = entityManager.createNativeQuery(queryStr)
				.setParameter("idServico", idServico)
				.setParameter("idPlano", idPlano)
				.setParameter("idPaciente", (idDependente != null ? idDependente : idCliente))
				.setParameter("idContrato", idContrato);

		query.setMaxResults(1);
		
		try {
		Date result = (Date) query.getSingleResult();

			return result;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Agendamento> getAgendamentosNovosCliente(Long idCliente){
		
		String queryStr = "SELECT ag FROM Agendamento ag "
				+ "WHERE ag.dtExclusao IS NULL "
				+ "AND ag.inStatus IN (0,1,4) "
				+ "AND ag.dtAgendamento >= CURRENT_DATE "
				+ "AND ag.contrato.plano IS NULL "
				+ "AND ag.cliente.id = " + idCliente;
		
		TypedQuery<Agendamento> query = entityManager.createQuery(queryStr,
				Agendamento.class);
		
		try {
			List<Agendamento> listresult = query.getResultList();
			if(listresult != null && listresult.size() > 0)
				return listresult;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return null;
		
	}
	
	public PagedListDTO<HistoricoAgendamentosCliente> getHistoricoAgendamentos(Long idCliente,
			Integer tipoUsuario, Integer offsetFact, Integer tipoListagem) {
		Integer dataSetLen = 10;
		String queryStr = "SELECT ags FROM HistoricoAgendamentosCliente ags ";
		if(tipoUsuario.intValue() == TipoUsuario.CLIENTE.valorTipo) {
			queryStr += "WHERE ags.idCliente = :idCliente AND ags.idDependente IS NULL ";
		} else {
			queryStr += "WHERE ags.idDependente = :idCliente ";
		}
		if(tipoListagem.equals(1)) {
			queryStr += " AND (ags.dtAgendamento < :today OR "
					+ " (ags.dtAgendamento = :today AND (ags.inStatus = 2 OR ags.inStatus = 3)) ) ";
		} else {
			queryStr += " AND (ags.dtAgendamento > :today OR "
					+ " (ags.dtAgendamento = :today AND (ags.inStatus = 0 OR ags.inStatus = 1 "
					+ "OR ags.inStatus = 4)) ) ";
		}
		queryStr += " ORDER BY ags.dtAgendamento DESC";
		Integer offsetV = (dataSetLen * offsetFact);
		TypedQuery<HistoricoAgendamentosCliente> query = entityManager.createQuery(
				queryStr, HistoricoAgendamentosCliente.class)
				.setParameter("idCliente", idCliente)
				.setParameter("today", new Date())
				.setMaxResults(dataSetLen)
				.setFirstResult(offsetV);
		
		try {
			List<HistoricoAgendamentosCliente> result = query.getResultList();
			PagedListDTO<HistoricoAgendamentosCliente> response = new PagedListDTO<>(result, (result.size() == dataSetLen));
			return response;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<AgendamentoLembreteDTO> getAgendamentosParaLembrete(Integer daysAhead) {
		String queryStr = "SELECT ag.idagendamento, ag.dtagendamento, prof.nmfuncionario, "
				+ "esp.nmespecialidade, un.nmapelido AS nmunidade, reg.id_usuario AS idpaciente, "
				+ "reg.tipo_usuario AS tipopaciente, "
				+ "(CASE WHEN reg.tipo_usuario = 1 THEN cli.nmcliente ELSE dep.nmdependente END) AS nmpaciente, "
				+ "(CASE WHEN reg.tipo_usuario = 1 THEN cli.nmemail ELSE dep.nmemail END) AS emailpaciente, "
				+ "(CASE WHEN reg.tipo_usuario = 1 THEN cli.nrcelular ELSE dep.nrcelular END) AS celularpaciente, "
				+ "lgn.notification_reg_id as notificationregid "
				+ "FROM app_mobile.tbusuario_register reg "
				+ "INNER JOIN app_mobile.tbusuario_login lgn ON lgn.id_usuario = reg.id_usuario AND lgn.tipo_usuario = reg.tipo_usuario "
				+ "INNER JOIN realvida.tbagendamento ag ON ag.dtexclusao IS NULL AND "
				+ "(reg.tipo_usuario = 1 AND ag.idcliente = reg.id_usuario AND ag.iddependente is null) "
				+ "OR (reg.tipo_usuario = 2 AND ag.iddependente = reg.id_usuario) "
				+ "INNER JOIN realvida.tbespecialidade esp ON esp.idespecialidade = ag.idespecialidade "
				+ "INNER JOIN realvida.tbfuncionario prof ON prof.idfuncionario = ag.idfuncionario "
				+ "INNER JOIN realvida.tbunidade un ON un.idunidade = ag.idunidade "
				+ "LEFT JOIN realvida.tbcliente cli ON cli.idcliente = reg.id_usuario "
				+ "LEFT JOIN realvida.tbdependente dep ON dep.iddependente = reg.id_usuario "
				+ "WHERE ag.dtagendamento = current_date + :nDaysAhead "
				+ "GROUP BY ag.idagendamento, ag.dtagendamento, prof.nmfuncionario, esp.nmespecialidade, "
				+ "un.nmapelido, reg.id_usuario, reg.tipo_usuario, nmpaciente, emailpaciente, "
				+ "celularpaciente, notificationregid ";
		try {
			@SuppressWarnings("unchecked")
			List<AgendamentoLembreteDTO> result = this.findByNativeQuery(queryStr)
					.setParameter("nDaysAhead", daysAhead)
					.unwrap(SQLQuery.class)
					.addScalar("idAgendamento", LongType.INSTANCE)
					.addScalar("dtAgendamento", DateType.INSTANCE)
					.addScalar("nmFuncionario", StringType.INSTANCE)
					.addScalar("nmEspecialidade", StringType.INSTANCE)
					.addScalar("nmUnidade", StringType.INSTANCE)
					.addScalar("idPaciente", LongType.INSTANCE)
					.addScalar("tipoPaciente", IntegerType.INSTANCE)
					.addScalar("nmPaciente", StringType.INSTANCE)
					.addScalar("emailPaciente", StringType.INSTANCE)
					.addScalar("celularPaciente", StringType.INSTANCE)
					.addScalar("notificationRegId", StringType.INSTANCE)
					.setResultTransformer(
							Transformers.aliasToBean(AgendamentoLembreteDTO.class)).list();
			return result;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Agendamento> getAgendamentosFromSiteExcluir() {
		String queryStr = "SELECT ag.* FROM realvida.tbagendamento ag "
				+ " INNER JOIN realvida.tbdespesa desp ON desp.idagendamento = ag.idagendamento "
				+ " AND desp.insituacao = 0 "
				+ " INNER JOIN realvida.tbparcela p ON p.iddespesa = desp.iddespesa "
				+ " INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = ag.idcontrato "
				+ " LEFT JOIN realvida.tbboletobancario bol ON bol.idparcela = p.idparcela "
				+ " WHERE (ag.idoperadorcadastro = 4733 OR ag.idoperadorcadastro = 4734) "
				+ " AND ag.dtexclusao IS NULL "
				+ " AND ag.instatus = 0 AND cont.idempresacliente IS NULL "
				+ " AND date_part('dow', CURRENT_DATE) <> 0 "
				+ " AND ("
				+ " (bol.idboletobancario IS NOT NULL AND "
				+ " p.dtvencimento <= (CURRENT_DATE - (CASE WHEN date_part('dow', current_date) < 3 THEN 3 ELSE 2 END)) ) "
				+ " OR (bol.idboletobancario IS NULL AND p.dtvencimento <= current_date) ) "
				+ " ORDER BY p.dtvencimento DESC";
		try {
			Query query = entityManager.createNativeQuery(queryStr, Agendamento.class);
			return query.getResultList();
		} catch(Exception e) {
			
		}
		return null;
	}
	

	public Integer countPacienteAgendamentosConsulta(Long idPaciente, Integer tipoPaciente, Date dia, 
			Long idEspecialidade, Long idServico) {
		String queryStr = "SELECT ag.* FROM realvida.tbagendamento ag "
				+ "INNER JOIN realvida.tbservicoespecialidade se ON se.idespecialidade = :idEsp "
				+ "AND se.idservico = :idServ AND se.boconsulta IS TRUE "
				+ "WHERE ag.dtexclusao IS NULL AND ag.dtagendamento = :dtAg ";
		if(tipoPaciente.equals(TipoPessoa.CLIENTE.getId())) {
			queryStr += "AND ag.idcliente = :idPaciente AND ag.iddependente IS NULL ";
		} else if(tipoPaciente.equals(TipoPessoa.DEPENDENTE.getId())){
			queryStr += "AND ag.iddependente = :idPaciente ";
		} else {
			return null;
		}
		queryStr += "ORDER BY ag.idagendamento";
		try {
			List<?> result = entityManager.createNativeQuery(queryStr, Agendamento.class)
					.setParameter("idEsp" , idEspecialidade)
					.setParameter("idServ", idServico)
					.setParameter("dtAg", dia)
					.setParameter("idPaciente", idPaciente)
					.getResultList();
			return result.size();
		} catch(Exception e) {
			
		}
		return null;
}
	
	
	public Integer getQtdAgendamentosAtendProf(AtendimentoProfissionalView ap, List<String> datas){
		
//		SimpleDateFormat sdfData = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");
//		
		String horainicio;
		String horaFim;
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTimeInMillis(ap.getHrInicio().getTime());
		horainicio = sdfHora.format(cal.getTime());
		
		cal.setTimeInMillis(ap.getHrFim().getTime());
		horaFim = sdfHora.format(cal.getTime());
		 
		
		String query = " SELECT CAST(COALESCE(count(*),0) AS int) FROM realvida.tbagendamento "
//						 +" WHERE dtagendamento between current_date and current_date + interval '"+StaticValuesGlobal.QDTDIASATENDIMENTOFIXO+" day'"
						 +" where dtagendamento in ("+ datas.toString().substring(1, datas.toString().length() -1)+")"
						 +" AND hragendamento BETWEEN '"+horainicio+"' AND '"+horaFim+"'"
						 +" AND idfuncionario = "+ap.getIdProfissional()
						 +" AND idespecialidade ="+ap.getIdEspProfissional()
						 +" AND idunidade ="+ap.getIdUnidade()
					     +" AND dtexclusao IS NULL"
					     +" AND intipoconsulta = "+ap.getInTipoConsulta()
					     +" AND intipo = 0"
					     + "AND ( SELECT EXTRACT(ISODOW FROM dtagendamento) ) = "+ap.getInDiaSemana();
		

		Query Tquery = entityManager.createNativeQuery(query);

		Tquery.setMaxResults(1);
		
		try {
		Integer result = (Integer) Tquery.getSingleResult();

			return result;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	public Agendamento getUltimoAgendamentoConsultaByProfissionalEspecialidade(Long idCliente, Long idDependente, Long idFuncionario, Long idEspecialidade){
		
		String queryStr = "SELECT ag FROM Agendamento ag, ServicoEspecialidade se INNER JOIN se.servico as serv INNER JOIN se.especialidade as esp "				
				+ " WHERE ag.servico = serv AND ag.especialidade = esp "
				+ " AND ag.dtExclusao IS NULL AND ag.inStatus = 2 AND se.boConsulta IS TRUE "
				+ " AND ag.especialidade.id = " + idEspecialidade
				+ " AND ag.profissional.id = " + idFuncionario
				+ " AND ag.cliente.id = " + idCliente				
				+ (idDependente != null ? " AND ag.dependente.id = " + idDependente : "AND ag.dependente.id IS NULL ")				
				+ " ORDER BY ag.dtAgendamento DESC" ;
		
		try {
			TypedQuery<Agendamento> query = createQuery(queryStr, Agendamento.class);
			query.setMaxResults(1);
			Agendamento result = query.getSingleResult();
			return result;

		} catch(Exception e) {
			//e.printStackTrace();			
		}
		
		return null;
	}
	
	public void updateRetornoAgendamento(Long idCliente, Long idDependente, Long idFuncionario, Long idEspecialidade){
		
		
		try {
			StoredProcedureQuery q = entityManager.createStoredProcedureQuery("realvida.update_returnagendamento");
			q.registerStoredProcedureParameter("par_idcliente", Long.class, ParameterMode.IN);
			q.registerStoredProcedureParameter("par_iddependente", Long.class, ParameterMode.IN);
			q.registerStoredProcedureParameter("par_idfuncionario", Long.class, ParameterMode.IN);
			q.registerStoredProcedureParameter("par_idespecialidade", Long.class, ParameterMode.IN);
			
			q.setParameter("par_idcliente", idCliente == null ? 0L : idCliente);
			q.setParameter("par_iddependente", idDependente == null ? 0L : idDependente);
			q.setParameter("par_idfuncionario", idFuncionario);
			q.setParameter("par_idespecialidade", idEspecialidade);
			q.execute();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Long getTotalRelatorioAgendamentosMedico(String dataInicio, String dataFim, Integer statusAg,
			String idOperadorCadastro, Long inGrupoFuncionario, Long idUnidade, Long idEspecialidade, Long idProfissional){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 String dtFim = dataFim + " 23:59:59";
		 String dtInicio = dataInicio + " 00:00:00";
		 Date dateInicio;
		 Date dateFim;
		 Timestamp timestampFim;
		 Timestamp timestampInicio;
		try {
			dateInicio = sdf.parse(dtInicio);
			dateFim = sdf.parse(dtFim);

			timestampFim = new Timestamp(dateFim.getTime());
			timestampInicio  = new Timestamp(dateInicio.getTime());
			String queryStr = "SELECT COUNT(av.*) AS counter FROM realvida.tbagendamento_vw av "
			+ " WHERE av.dtexclusao is null";
			
			if(statusAg != -1){
			 queryStr += " AND av.dtagendamento BETWEEN '"+dataInicio+"' AND '"+ dataFim+"' "
			 		+ " AND av.instatus = " + statusAg; 
			}else{
				queryStr += " AND av.dtinclusaolog BETWEEN '"+timestampInicio+"' AND '"+timestampFim+"'";
			}
			if(inGrupoFuncionario != null){
				queryStr += " AND av.ingrupofuncionario = "+ inGrupoFuncionario;
			}
			
			if(idOperadorCadastro != null){
				queryStr += " AND av.idoperadorcadastro in (" +idOperadorCadastro+")";
			}
			
			if(idUnidade != null){
				queryStr += " AND av.idunidade = "+ idUnidade;
			}
			
			if(idEspecialidade != null){
				queryStr += " AND av.idespecialidade = "+ idEspecialidade;
			}
			
			if(idProfissional != null){
				queryStr += " AND av.idprofissional = "+ idProfissional;
			}

			Query query = entityManager.createNativeQuery(queryStr/*,
			        AgendamentoView.class*/);
			Long quantidade = (Long) query.unwrap(SQLQuery.class)
	        .addScalar("counter", LongType.INSTANCE).uniqueResult();
			
			
			 return quantidade;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

	public List<AgendamentoView> getRelatorioAgendamentosMedico(String dataInicio, String dataFim, Integer statusAg,
			String idOperadorCadastro, Long inGrupoFuncionario, Long idUnidade, Long idEspecialidade, Long idProfissional, Integer offset, Integer limit) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 String dtFim = dataFim + " 23:59:59";
		 String dtInicio = dataInicio + " 00:00:00";
		 Date dateInicio;
		 Date dateFim;
		 Timestamp timestampFim;
		 Timestamp timestampInicio;
		 List<AgendamentoView> result = new ArrayList<AgendamentoView>();
		try {
			dateInicio = sdf.parse(dtInicio);
			dateFim = sdf.parse(dtFim);

			timestampFim = new Timestamp(dateFim.getTime());
			timestampInicio  = new Timestamp(dateInicio.getTime());
			String queryStr = "SELECT av FROM AgendamentoView av "
			+ " WHERE av.dtExclusao is null";
			String orderStr = "";
			if(statusAg != -1){
			 queryStr += " AND av.dtAgendamento BETWEEN '"+dataInicio+"' AND '"+ dataFim+"' "
			 		+ " AND av.inStatus = " + statusAg; 
			 orderStr += " ORDER BY av.dtAgendamento ASC";
			}else{
				queryStr += " AND av.dtInclusaoLog BETWEEN '"+timestampInicio+"' AND '"+timestampFim+"'";
				orderStr += " ORDER BY av.dtInclusaoLog ASC";
			}
			if(inGrupoFuncionario != null){
				queryStr += " AND av.inGrupoFuncionario = "+ inGrupoFuncionario;
			}
			
			if(idOperadorCadastro != null){
				queryStr += " AND av.idOperadorCadastro in (" +idOperadorCadastro+")";
			}
			
			if(idUnidade != null){
				queryStr += " AND av.unidade.id = "+ idUnidade;
			}
			
			if(idEspecialidade != null){
				queryStr += " AND av.especialidade.id = "+ idEspecialidade;
			}
			
			if(idProfissional != null){
				queryStr += " AND av.profissional.id = "+ idProfissional;
			}
			String  queryFinal= queryStr += orderStr;
			TypedQuery<AgendamentoView> query = entityManager.createQuery(queryFinal,
			        AgendamentoView.class);
			query.setMaxResults(limit).setFirstResult((offset-1)*limit);
			result = query.getResultList();
			return result;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	
	}

}
