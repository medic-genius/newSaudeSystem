package br.com.medic.medicsystem.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SummaryServicoByStatusDTO {

	private Integer qdtServico;
	
	private Integer status;

	private Double custoServicos;

	private String sqlFieldsSelect =
			"	count(guia.idguia) as qdt_servicos" + 
			"	, guia_servico.status" + 
			"	, sum(cp.vlservicoassociado) as custo_servicos" ;

	private String sqlFrom = "realvida.tbguiaservico guia_servico";

	private String sqlJoins =
			"inner join realvida.tbservico as servico on servico.idservico = guia_servico.idservico"
			+ " inner join realvida.tbguia as guia on guia.idguia = guia_servico.idguia"
			+ " inner join realvida.tbcliente as cliente on cliente.idcliente = guia.idcliente"
			+ " left join realvida.tbdependente as dependente on dependente.iddependente = guia.iddependente"
			+ " inner join realvida.tbcontrato as contrato on contrato.idcontrato = guia.idcontrato"
			+ " inner join realvida.tbplano as plano on plano.idplano = contrato.idplano"
			+ " inner join realvida.tbcoberturaplano as cp on plano.idplano = cp.idplano and cp.idservico = servico.idservico"
			+ " inner join realvida.tbempresacliente as empresacliente on empresacliente.idempresacliente = contrato.idempresacliente";

	private String sqlWhere = null;

	public Integer getQdtServico() {
		return qdtServico;
	}

	public void setQdtServico(Integer qdtServico) {
		this.qdtServico = qdtServico;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getCustoServicos() {
		return custoServicos;
	}

	public void setCustoServicos(Double custoServicos) {
		this.custoServicos = custoServicos;
	}

	private String sqlOrderBy = "guia_servico.idguiaservico desc";

	@JsonIgnore
	public String getSqlSelect() {
		String sqlQuery =
				"select "
				+ this.sqlFieldsSelect
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		sqlQuery += " group by status";
		return sqlQuery;
	}

	@JsonIgnore
	public String getSqlCount() {
		String sqlQuery =
				"select "
				+ " count(*)"
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		return sqlQuery;
	}

	public void addSqlWhereConditionAnd(String condition) {
		if(null == this.sqlWhere) {
			this.sqlWhere = condition;
		} else {
			this.sqlWhere += " and " + condition;
		}
	}

	@JsonIgnore
	public String getSqlOrderBy() {
		return this.sqlOrderBy;
	}
	
	public void setSqlOrderBy(String sqlOrderBy) {
		this.sqlOrderBy = sqlOrderBy;
	}

}
