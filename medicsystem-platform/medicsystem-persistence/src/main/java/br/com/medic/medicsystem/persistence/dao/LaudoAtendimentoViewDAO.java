package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.views.LaudoAtendimentoView;

@Named("laudoatendimentoview-dao")
@ApplicationScoped
public class LaudoAtendimentoViewDAO extends RelationalDataAccessObject<LaudoAtendimentoViewDAO> {
	
	public List<LaudoAtendimentoView> getLaudoAtendimentoPorCliente(List<Long>listIdPaciente, String tipoPaciente) {
		
		String idPacientes = "";
		for(Long listIds: listIdPaciente){
			idPacientes = idPacientes + ", " + listIds;
		}
		
		String query = "SELECT lav FROM LaudoAtendimentoView lav "
				+ " WHERE lav.idPaciente in ("+ idPacientes.substring(2, idPacientes.length()) + ")"
				+ " and lav.tipoPaciente = '" + tipoPaciente + "'";
		
		TypedQuery<LaudoAtendimentoView> queryStr = entityManager.createQuery(query, LaudoAtendimentoView.class);
						
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}
