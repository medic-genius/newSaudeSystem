package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class AssinaturaRecorrenteDataDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cartaoNomePortador;
	private String cartaoNumero;
	private String cartaoCvv;
	private String cartaoValidade;
	private Boolean cartaoIsElo;
	
	private Integer customerId;
	private Long idEmpresaGrupo;
	private Integer planoIdRecorrencia;
	private Integer quantidade;
	private BigDecimal valor;
	private String diaVencimentoInicial;
	
	public String getCartaoNomePortador() {
		return cartaoNomePortador;
	}
	public void setCartaoNomePortador(String cartaoNomePortador) {
		this.cartaoNomePortador = cartaoNomePortador;
	}
	public String getCartaoNumero() {
		return cartaoNumero;
	}
	public void setCartaoNumero(String cartaoNumero) {
		this.cartaoNumero = cartaoNumero;
	}
	public String getCartaoCvv() {
		return cartaoCvv;
	}
	public void setCartaoCvv(String cartaoCvv) {
		this.cartaoCvv = cartaoCvv;
	}
	public String getCartaoValidade() {
		return cartaoValidade;
	}
	public void setCartaoValidade(String cartaoValidade) {
		this.cartaoValidade = cartaoValidade;
	}
	public Boolean getCartaoIsElo() {
		return cartaoIsElo;
	}
	public void setCartaoIsElo(Boolean cartaoIsElo) {
		this.cartaoIsElo = cartaoIsElo;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}
	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}
	public Integer getPlanoIdRecorrencia() {
		return planoIdRecorrencia;
	}
	public void setPlanoIdRecorrencia(Integer planoIdRecorrencia) {
		this.planoIdRecorrencia = planoIdRecorrencia;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getDiaVencimentoInicial() {
		return diaVencimentoInicial;
	}
	public void setDiaVencimentoInicial(String diaVencimentoInicial) {
		this.diaVencimentoInicial = diaVencimentoInicial;
	}
}
