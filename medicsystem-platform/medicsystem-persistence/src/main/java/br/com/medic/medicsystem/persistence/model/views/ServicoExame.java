package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(catalog = "realvida", name = "tbservicoexame")
public class ServicoExame {
	
	@Id
	@Column(name = "idservicoexame")
	private Long idServicoExame;
	
	
	@Basic
	@Column(name = "idespecialidade")
	private Long idEspecialidade;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "idservico")
	private Long idServico;
	
	@Basic
	@Column(name = "nmservico")
	private String nmServico;
	
	
	@Basic
	@Column(name = "ranking")
	private Long ranking;
	
	@Basic
	@Column(name = "tipo")
	private String tipo;



	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public Long getRanking() {
		return ranking;
	}

	public void setRanking(Long ranking) {
		this.ranking = ranking;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
	
	
	
}
