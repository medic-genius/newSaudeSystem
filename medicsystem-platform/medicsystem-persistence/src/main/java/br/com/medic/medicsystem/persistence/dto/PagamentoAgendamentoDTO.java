package br.com.medic.medicsystem.persistence.dto;

import java.math.BigDecimal;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class PagamentoAgendamentoDTO {
	// id agendamento
	private Long idAgendamento;
	
	// valor a ser pago
	private BigDecimal valor;
	
	// 1-boleto; 2-cartao de credito
	private Integer formaPagamento;
		
	// dados de cartão de crédito
	private String cartaoNomePortador;
	private String cartaoNumero;
	private String cartaoCvv;
	private String cartaoValidade;
	private Boolean cartaoIsElo;
	// vindi customer id
	private Integer customerId;
	
	public Long getIdAgendamento() {
		return idAgendamento;
	}
	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public Integer getFormaPagamento() {
		return formaPagamento;
	}
	public void setFormaPagamento(Integer formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public String getCartaoNomePortador() {
		return cartaoNomePortador;
	}
	public void setCartaoNomePortador(String cartaoNomePortador) {
		this.cartaoNomePortador = cartaoNomePortador;
	}
	public String getCartaoNumero() {
		return Utils.removeSpecialChars(cartaoNumero);
	}
	public void setCartaoNumero(String cartaoNumero) {
		this.cartaoNumero = cartaoNumero;
	}
	public String getCartaoCvv() {
		return cartaoCvv;
	}
	public void setCartaoCvv(String cartaoCvv) {
		this.cartaoCvv = cartaoCvv;
	}
	public String getCartaoValidade() {
		return cartaoValidade;
	}
	public void setCartaoValidade(String cartaoValidade) {
		this.cartaoValidade = cartaoValidade;
	}
	public Boolean getCartaoIsElo() {
		return cartaoIsElo;
	}
	public void setCartaoIsElo(Boolean cartaoIsElo) {
		this.cartaoIsElo = cartaoIsElo;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}
