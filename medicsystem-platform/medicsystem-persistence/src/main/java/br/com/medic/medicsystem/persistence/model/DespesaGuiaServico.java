package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "realvida", name = "tbdespesaguiaservico")
public class DespesaGuiaServico implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESPESAGUIASERVICO_ID_SEQ")
	@SequenceGenerator(name = "DESPESAGUIASERVICO_ID_SEQ", sequenceName = "realvida.despesaguiaservico_id_seq", allocationSize = 1)
	@Column(name = "iddespesaguiaservico")
	private Long id;
	
	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "iddespesa")
	private Despesa despesa;
		
	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idguiaservico")
	private GuiaServico guiaServico;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtexclusao")
	private Timestamp dtExclusao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public GuiaServico getGuiaServico() {
		return guiaServico;
	}

	public void setGuiaServico(GuiaServico guiaServico) {
		this.guiaServico = guiaServico;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}
	
	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Timestamp dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
	

}
