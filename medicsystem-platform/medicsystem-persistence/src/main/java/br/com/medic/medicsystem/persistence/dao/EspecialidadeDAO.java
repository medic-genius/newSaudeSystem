package br.com.medic.medicsystem.persistence.dao;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimeType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.CalAgendaMedicoDTO;
import br.com.medic.medicsystem.persistence.dto.CalEspecialidadeDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.EspProfissional;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.ServicoEspecialidade;
import br.com.medic.medicsystem.persistence.model.ServicoSubEspecialidade;
import br.com.medic.medicsystem.persistence.model.SubEspecialidade;

@Named("especialidade-dao")
@ApplicationScoped
public class EspecialidadeDAO extends RelationalDataAccessObject<Especialidade> {

	public List<Especialidade> getEspecialidades() {
		TypedQuery<Especialidade> query = entityManager.createQuery(
		        "SELECT e FROM EspProfissional esp, Funcionario f, Especialidade e"
		        + " WHERE esp.funcionario.id = f.id"
		        + " AND esp.especialidade.id = e.id"
		        + " AND e.inTipoEspecialidade = 0"
		        + " AND e.dtExclusao is null"
		        + " AND f.dtExclusao is null"
		        + " Group By e.id"
		        + " ORDER By e.nmEspecialidade",
		        Especialidade.class);

		List<Especialidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<SubEspecialidade> getSubEspecialidades(Long idEspecialidade) {
		TypedQuery<SubEspecialidade> query = entityManager.createQuery(
		        "SELECT u FROM SubEspecialidade u WHERE u.especialidade.id = "
		                + idEspecialidade + " ORDER BY u.nmSubEspecialidade ASC",
		        SubEspecialidade.class);

		List<SubEspecialidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public ServicoSubEspecialidade getServicoSubEspecialidadePorSubEspecialidade(
	        Long idSubEspecialidade) {

		TypedQuery<ServicoSubEspecialidade> query = entityManager
		        .createQuery(
		                "SELECT sse FROM ServicoSubEspecialidade sse "
		                        + " where sse.subEspecialidade.id = "
		                        + idSubEspecialidade
		                        + " and sse.boConsulta is not null and sse.boConsulta is true",
		                ServicoSubEspecialidade.class);

		query.setMaxResults(1);
		return query.getSingleResult();

	}

	public ServicoEspecialidade getServicoEspecialidadePorEspecialidade(
	        Long idEspecialidade) {
		
		try {
			
			TypedQuery<ServicoEspecialidade> query = entityManager
			        .createQuery(
			                "SELECT se from ServicoEspecialidade se where se.especialidade.id = "
			                        + idEspecialidade
			                        + " and se.boConsulta is not null and se.boConsulta is true",
			                ServicoEspecialidade.class);

			query.setMaxResults(1);
			return query.getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;

		
	}
	
	
	@SuppressWarnings("unchecked")
	public List <CalEspecialidadeDTO> getCalEspecialidadeDTO(){
		
		String query = " SELECT esp.idespecialidade, " 
           +"esp.nmespecialidade, "
           +"serv.idservico, "
           +"serv.nmservico, "
           +"serv.vlservico ";

		query +=" FROM realvida.tbespecialidade esp ";

		query +="INNER JOIN "
				+" realvida.tbservicoespecialidade se "
				+" ON esp.idespecialidade = se.idespecialidade ";

		query +="INNER JOIN "
          + " realvida.tbservico serv "
          + " ON  se.idservico = serv.idservico" 
          + " AND  serv.dtexclusao is null ";

		query +=" WHERE "
            +"esp.dtexclusao is null "
            +"AND esp.intipoespecialidade = 0 --Tipo Médico "
            +"AND se.boconsulta = true ";

		query +=" ORDER BY esp.nmespecialidade ";
		
		List<CalEspecialidadeDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("idServico", LongType.INSTANCE)
		        .addScalar("nmServico", StringType.INSTANCE)
		        .addScalar("vlServico", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(CalEspecialidadeDTO.class)).list();
		
		try {
			return result;
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List <CalAgendaMedicoDTO> getAgendaMedico(List<Long> ids){
		
		String query = " SELECT "
		   +"esp.idespecialidade, " 
           +"esp.nmespecialidade, "
           +"fun.idfuncionario, "
           +"fun.nmfuncionario, "
           +"trim(substring(fun.nmfuncionario, 0, POSITION(' ' in fun.nmfuncionario) )) as nmdoutor, "
           +"pro.nrcrmcro, "
           +"atendpro.indiasemana, "
           +"atendpro.inturno, "
           +"atendpro.hrinicio, "
           +"atendpro.hrfim, "
           +"atendpro.hrtempoconsulta, "
           +"to_char(atendpro.hrtempoconsulta, 'mi')\\:\\:\\integer as minuto, "
           +"unid.idunidade, "
           +"unid.nmlogradouro, "
           +"unid.nrnumero, "
           +"unid.nrcep, "
           +"bairro.nmbairro, "
           +"cidade.nmcidade ";

		query +=" FROM realvida.tbfuncionario fun ";

		query +="INNER JOIN realvida.tbprofissional pro ON fun.idfuncionario = pro.idfuncionario "
			   +"INNER JOIN realvida.tbatendimentoprofissional atendpro  ON fun.idfuncionario = atendpro.idfuncionario "
			   +"INNER JOIN realvida.tbespprofissional esppro            ON fun.idfuncionario = esppro.idfuncionario "
			   +"INNER JOIN realvida.tbespecialidade esp                 ON esp.idespecialidade = esppro.idespecialidade "
			   +"INNER JOIN realvida.tbunidade unid                      ON atendpro.idunidade = unid.idunidade "
			   +"INNER JOIN realvida.tbbairro bairro                     ON unid.idbairro = bairro.idbairro "
			   +"INNER JOIN realvida.tbcidade cidade                     ON unid.idcidade = cidade.idcidade ";


		query +=" WHERE "
            +"fun.dtexclusao IS null "
            +"AND pro.intipoprofissional = 0  ";
            
           String listIds = "";
           
           if(ids != null && ids.size() == 1){
        	   
        	   listIds = ids.get(0).toString();
           }else{
        	   
        	   for (Long id : ids) {
               	
               	listIds += id+",";
               	
               	
               }
        	   
        	   listIds = listIds.substring(0, listIds.length() -1);
        	   
           }
            
            
            
            query +=" AND esp.idespecialidade IN ("+listIds+")";

		query +=" ORDER BY "
				+ " esp.nmespecialidade ,"
				+ " fun.nmfuncionario ,"
				+ " atendpro.indiasemana, "
				+ " atendpro.hrinicio ";
		
		List<CalAgendaMedicoDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)
		        .addScalar("nmDoutor", StringType.INSTANCE)
		        .addScalar("nrCRMCRO", StringType.INSTANCE)
		        .addScalar("inDiaSemana", IntegerType.INSTANCE)
		        .addScalar("inTurno", IntegerType.INSTANCE)
		        .addScalar("hrInicio", TimeType.INSTANCE)
		        .addScalar("hrFim", TimeType.INSTANCE)
		        .addScalar("hrTempoConsulta", TimeType.INSTANCE)
		        .addScalar("minuto", IntegerType.INSTANCE)
		        .addScalar("idUnidade", LongType.INSTANCE)
		        .addScalar("nmLogradouro", StringType.INSTANCE)
		        .addScalar("nrNumero", IntegerType.INSTANCE)
		        .addScalar("nrCep", StringType.INSTANCE)
		        .addScalar("nmBairro", StringType.INSTANCE)
		        .addScalar("nmCidade", StringType.INSTANCE)
		        
		        .setResultTransformer(
		                Transformers.aliasToBean(CalAgendaMedicoDTO.class)).list();
		
		try {
			return result;
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
	public Especialidade getEspecialidadeById(Long idEspecialidade) {
		return entityManager.find(Especialidade.class, idEspecialidade);
	}
	
	public Collection<Especialidade> getEspecialidadesAtivas(){
		
		String queryStr = "SELECT DISTINCT esp.*"
		        + " FROM realvida.tbAtendimentoProfissional_View ap"
		        + " INNER JOIN realvida.tbespecialidade esp ON esp.idespecialidade = ap.idespprofissional"
		        + " INNER JOIN realvida.tbatendimentoservprofissional espserv ON espserv.idatendimentoprofissional = ap.idatendimentoprofissional"
		        + " INNER JOIN realvida.tbservico serv ON serv.idservico = espserv.idservico and serv.bosite IS TRUE"
		        + " WHERE espserv.dtexclusao is null"
		        + " ORDER BY esp.nmespecialidade";

		Collection<Especialidade> result = findByNativeQuery(queryStr,
		        Especialidade.class);

		if (result.isEmpty()) {
			return null;
		}

		return result;
	}
	
	public List<Especialidade> getEspecialidadesByProfissional(Long idFuncionario) {
		String queryStr = "SELECT ep.especialidade FROM EspProfissional ep WHERE ep.funcionario.id = :idFunc";
		try {
			TypedQuery<Especialidade> query = createQuery(queryStr, Especialidade.class)
					.setParameter("idFunc", idFuncionario);
			return query.getResultList();
		} catch(Exception e) {
			
		}
		return null;
	}
	
	public EspProfissional getEspProfissional(Long idMedico, Long idEspecialidade) {
		String queryStr = "SELECT ep FROM EspProfissional ep WHERE "
				+ "ep.funcionario.id = :idMedico AND ep.especialidade.id = :idEspecialidade";
		try {
			TypedQuery<EspProfissional> query = this.entityManager.createQuery(queryStr, EspProfissional.class)
					.setParameter("idMedico", idMedico)
					.setParameter("idEspecialidade", idEspecialidade)
					.setMaxResults(1);
			return query.getSingleResult();

		} catch(Exception e) {
			
		}
		return null;
	}
}
