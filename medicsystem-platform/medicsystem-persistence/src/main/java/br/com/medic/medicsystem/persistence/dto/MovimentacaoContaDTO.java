package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class MovimentacaoContaDTO implements Serializable{
	
	private static final long serialVersionUID = 6709663970696042506L;

	private Double nrSaldo;
	
	private Double totalEntrada;
	
	private Double totalSaida;
	
	private Double totalEstorno;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "America/Manaus")
	private Date data;

	private List<MovimentacaoItensContaDTO> itens;

	public Double getNrSaldo() {
		return nrSaldo;
	}
	public void setNrSaldo(Double nrSaldo) {
		this.nrSaldo = nrSaldo;
	}
	public List<MovimentacaoItensContaDTO> getItens() {
		return itens;
	}
	public void setItens(List<MovimentacaoItensContaDTO> itens) {
		this.itens = itens;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	public Double getTotalEntrada() {
		return totalEntrada;
	}
	public void setTotalEntrada(Double totalEntrada) {
		this.totalEntrada = totalEntrada;
	}
	public Double getTotalSaida() {
		return totalSaida;
	}
	public void setTotalSaida(Double totalSaida) {
		this.totalSaida = totalSaida;
	}
	public Double getTotalEstorno() {
		return totalEstorno;
	}
	public void setTotalEstorno(Double totalEstorno) {
		this.totalEstorno = totalEstorno;
	}

	@JsonProperty	
	public String getNrSaldoFormatado() {
		if(nrSaldo != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(nrSaldo);
		}
		return "0,00";

	}
	
	@JsonIgnore
	public void setNrSaldoFormatado(String nrSaldoFormatado) {
	}
	
	@JsonProperty	
	public String getTotalEntradaFormatado() {
		if(this.totalEntrada != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(this.totalEntrada);
		}
		return "0,00";
	}
	
	@JsonProperty	
	public String getTotalSaidaFormatado() {
		if(this.totalSaida != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(this.totalSaida);
		}
		return "0,00";

	}
	
	@JsonProperty	
	public String getTotalEstornoFormatado() {
		if(this.totalEstorno != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(this.totalEstorno);
		}
		return "0,00";

	}
	
}

