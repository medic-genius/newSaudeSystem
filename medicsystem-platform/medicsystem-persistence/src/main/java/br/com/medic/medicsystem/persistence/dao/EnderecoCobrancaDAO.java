package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.EnderecoCobranca;

@Named("endereco-cobranca-dao")
@ApplicationScoped
public class EnderecoCobrancaDAO extends RelationalDataAccessObject<EnderecoCobranca> {

}
