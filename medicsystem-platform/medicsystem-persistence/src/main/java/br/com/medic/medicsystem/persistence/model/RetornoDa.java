package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbretornoda")
public class RetornoDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RETORNODA_ID_SEQ")
	@SequenceGenerator(name = "RETORNODA_ID_SEQ", sequenceName = "realvida.retornoda_id_seq")
	@Column(name = "idretornoda")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtcredito")
	private Date dtCredito;
	
	@Basic
	@Column(name = "dtgravacao")
	private Date dtGravacao;
	
	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;
	
	@Basic
	@Column(name = "nravisobancario")
	private Long nrAvisoBancario;
	
	@Basic
	@Column(name = "nrcodigoempresa")
	private Long nrCodigoEmpresa;

	//bi-directional many-to-one association to Tbmovretornoda
	@OneToMany(mappedBy="tbretornoda")
	private List<MovRetornoDa> movRetornoDas;

	//bi-directional many-to-one association to Tbbanco
	@ManyToOne
	@JoinColumn(name="idbanco")
	private Banco banco;

	//bi-directional many-to-one association to Tbempresagrupo
	@ManyToOne
	@JoinColumn(name="idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public RetornoDa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtCredito() {
		return dtCredito;
	}

	public void setDtCredito(Date dtCredito) {
		this.dtCredito = dtCredito;
	}

	public Date getDtGravacao() {
		return dtGravacao;
	}

	public void setDtGravacao(Date dtGravacao) {
		this.dtGravacao = dtGravacao;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Long getNrAvisoBancario() {
		return nrAvisoBancario;
	}

	public void setNrAvisoBancario(Long nrAvisoBancario) {
		this.nrAvisoBancario = nrAvisoBancario;
	}

	public Long getNrCodigoEmpresa() {
		return nrCodigoEmpresa;
	}

	public void setNrCodigoEmpresa(Long nrCodigoEmpresa) {
		this.nrCodigoEmpresa = nrCodigoEmpresa;
	}

	public List<MovRetornoDa> getMovRetornoDas() {
		return movRetornoDas;
	}

	public void setMovRetornoDas(List<MovRetornoDa> movRetornoDas) {
		this.movRetornoDas = movRetornoDas;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public MovRetornoDa addMovRetornoDa(MovRetornoDa movRetornoDa) {
		getMovRetornoDas().add(movRetornoDa);
		movRetornoDa.setRetornoDa(this);

		return movRetornoDa;
	}

	public MovRetornoDa removeMovRetornoDa(MovRetornoDa movRetornoDa) {
		getMovRetornoDas().remove(movRetornoDa);
		movRetornoDa.setRetornoDa(null);

		return movRetornoDa;
	}

}