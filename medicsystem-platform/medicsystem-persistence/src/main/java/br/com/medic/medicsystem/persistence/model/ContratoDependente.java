package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbcontratodependente")
public class ContratoDependente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTRATODEPENDENTE_ID_SEQ")
	@SequenceGenerator(name = "CONTRATODEPENDENTE_ID_SEQ", sequenceName = "realvida.contratodependente_id_seq", allocationSize = 1)
	@Column(name = "idcontratodependente")
	private Long id;

	@Basic
	@Column(name = "blautorizadebaut")
	private Boolean blAutorizaDebAut;

	@Basic
	@Column(name = "creditoconveniada")
	private Integer creditoConveniada;

	@Basic
	@Column(name = "dtinativacao")
	private Date dtInativacao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;
	
	@Basic
	@Column(name = "bofazusoplano")
	private Boolean boFazUsoPlano;

	// bi-directional many-to-one association to Tbcontrato
	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;

	// bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name = "iddependente")
	private Dependente dependente;
	
	@Basic
	@Column(name = "boinativadoconveniada")
	private Boolean boInativadoConveniada;

	@Transient
	@JsonIgnore
	private String inSituacaoFormatado;

	public ContratoDependente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBlAutorizaDebAut() {
		return blAutorizaDebAut;
	}

	public void setBlAutorizaDebAut(Boolean blAutorizaDebAut) {
		this.blAutorizaDebAut = blAutorizaDebAut;
	}

	public Integer getCreditoConveniada() {
		return creditoConveniada;
	}

	public void setCreditoConveniada(Integer creditoConveniada) {
		this.creditoConveniada = creditoConveniada;
	}

	public Date getDtInativacao() {
		return dtInativacao;
	}

	public void setDtInativacao(Date dtInativacao) {
		this.dtInativacao = dtInativacao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	@JsonProperty
	public String getInSituacaoFormatado() {
		if (getInSituacao() != null) {
			if (getInSituacao() == 0) {
				return "SIM";
			} else {
				return "NAO";
			}
		}
		return "-";
	}

	public Boolean getBoFazUsoPlano() {
		if (boFazUsoPlano == null)
			return true;
					
		return boFazUsoPlano;
	}

	public void setBoFazUsoPlano(Boolean boFazUsoPlano) {
		this.boFazUsoPlano = boFazUsoPlano;
	}

	public Boolean getBoInativadoConveniada() {
		return boInativadoConveniada;
	}

	public void setBoInativadoConveniada(Boolean boInativadoConveniada) {
		this.boInativadoConveniada = boInativadoConveniada;
	}
	
	
	
}