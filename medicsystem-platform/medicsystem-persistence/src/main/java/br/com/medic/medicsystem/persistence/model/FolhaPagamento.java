package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tbfolhapagamento database table.
 * 
 */
@Entity
public class FolhaPagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBFOLHAPAGAMENTO_IDFOLHAPAGAMENTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBFOLHAPAGAMENTO_IDFOLHAPAGAMENTO_GENERATOR")
	private Long idfolhapagamento;

	private Timestamp dtatualizacaolog;

	@Temporal(TemporalType.DATE)
	private Date dtexclusao;

	@Temporal(TemporalType.DATE)
	private Date dtfinal;

	private Timestamp dtinclusaolog;

	@Temporal(TemporalType.DATE)
	private Date dtinicial;

	private Integer ingrupofuncionario;

	private Integer instatus;

	private Integer nranofolha;

	private Integer nrmesfolha;

	//bi-directional many-to-one association to Tbfolhafuncionario
	@OneToMany(mappedBy="tbfolhapagamento")
	private List<FolhaFuncionario> tbfolhafuncionarios;

	//bi-directional many-to-one association to Tbunidade
	@ManyToOne
	@JoinColumn(name="idunidade")
	private Unidade unidade;

	public FolhaPagamento() {
	}

	public Long getIdfolhapagamento() {
		return this.idfolhapagamento;
	}

	public void setIdfolhapagamento(Long idfolhapagamento) {
		this.idfolhapagamento = idfolhapagamento;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtexclusao() {
		return this.dtexclusao;
	}

	public void setDtexclusao(Date dtexclusao) {
		this.dtexclusao = dtexclusao;
	}

	public Date getDtfinal() {
		return this.dtfinal;
	}

	public void setDtfinal(Date dtfinal) {
		this.dtfinal = dtfinal;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Date getDtinicial() {
		return this.dtinicial;
	}

	public void setDtinicial(Date dtinicial) {
		this.dtinicial = dtinicial;
	}

	public Integer getIngrupofuncionario() {
		return this.ingrupofuncionario;
	}

	public void setIngrupofuncionario(Integer ingrupofuncionario) {
		this.ingrupofuncionario = ingrupofuncionario;
	}

	public Integer getInstatus() {
		return this.instatus;
	}

	public void setInstatus(Integer instatus) {
		this.instatus = instatus;
	}

	public Integer getNranofolha() {
		return this.nranofolha;
	}

	public void setNranofolha(Integer nranofolha) {
		this.nranofolha = nranofolha;
	}

	public Integer getNrmesfolha() {
		return this.nrmesfolha;
	}

	public void setNrmesfolha(Integer nrmesfolha) {
		this.nrmesfolha = nrmesfolha;
	}

	public List<FolhaFuncionario> getTbfolhafuncionarios() {
		return this.tbfolhafuncionarios;
	}

	public void setTbfolhafuncionarios(List<FolhaFuncionario> tbfolhafuncionarios) {
		this.tbfolhafuncionarios = tbfolhafuncionarios;
	}

	public FolhaFuncionario addTbfolhafuncionario(FolhaFuncionario tbfolhafuncionario) {
		getTbfolhafuncionarios().add(tbfolhafuncionario);
		tbfolhafuncionario.setTbfolhapagamento(this);

		return tbfolhafuncionario;
	}

	public FolhaFuncionario removeTbfolhafuncionario(FolhaFuncionario tbfolhafuncionario) {
		getTbfolhafuncionarios().remove(tbfolhafuncionario);
		tbfolhafuncionario.setTbfolhapagamento(null);

		return tbfolhafuncionario;
	}

	public Unidade getTbunidade() {
		return this.unidade;
	}

	public void setUnidade(Unidade tbunidade) {
		this.unidade = tbunidade;
	}

}