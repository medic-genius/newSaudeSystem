package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CobrancaDTO {
	
	private String nmTipo;
	private String nrDocumento;
	private String nmFormaCombinada;
	private String nmFormaQuitacao;
	private Date dtVencimento;
	private Double vlDocumento;
	private Double vlPago;
	private Date dtPagamento;
	
//	private String dtPagamentoFormatada;
//	private String dtVencimentoFormatada;
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
	
	
	public String getNmTipo() {
		return nmTipo;
	}
	public void setNmTipo(String nmTipo) {
		this.nmTipo = nmTipo;
	}
	public String getNrDocumento() {
		return nrDocumento;
	}
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	public String getNmFormaCombinada() {
		return nmFormaCombinada;
	}
	public void setNmFormaCombinada(String nmFormaCombinada) {
		this.nmFormaCombinada = nmFormaCombinada;
	}
	public String getNmFormaQuitacao() {
		return nmFormaQuitacao;
	}
	public void setNmFormaQuitacao(String nmFormaQuitacao) {
		this.nmFormaQuitacao = nmFormaQuitacao;
	}
	public Date getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public Double getVlDocumento() {
		return vlDocumento;
	}
	public void setVlDocumento(Double vlDocumento) {
		this.vlDocumento = vlDocumento;
	}
	public Double getVlPago() {
		return vlPago;
	}
	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	
	@JsonProperty
	public String getDtPagamentoFormatada() {
		if(dtPagamento != null)
			return sdf.format(dtPagamento);
		
		return null;
	}
	
	@JsonProperty
	public String getDtVencimentoFormatada() {
		if(dtVencimento != null)
			return sdf.format(dtVencimento);
		
		return null;
	}
	
	
	
	
	 

}
