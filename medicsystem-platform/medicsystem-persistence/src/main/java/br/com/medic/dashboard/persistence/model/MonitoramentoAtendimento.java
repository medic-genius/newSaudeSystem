package br.com.medic.dashboard.persistence.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MonitoramentoAtendimento {
	
	private String nmTitular;
	
	private String nmPaciente;
	
	private String nrAgendamento;
	
	private Date dtAgendamento;
	
	private String nmProfissional;
	
	private String nmEspecialidade;
	
	private String nmUnidade;
	
	private Integer status;
	
	@JsonIgnore
	private Timestamp hrAgendamento;
	
	@JsonIgnore
	private Timestamp hrPresenca;
	
	@JsonIgnore
	private Timestamp hrChegadaInicio;
	
	@JsonIgnore
	private Timestamp hrChegadaFim;
	
	private String horaAgendamento;
	
	private String horaPresenca;
	
	private String horaChegadaInicio;
	
	private String horaChegadaFim;
	
	
	public String getNmTitular() {
		return nmTitular;
	}

	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNrAgendamento() {
		return nrAgendamento;
	}

	public void setNrAgendamento(String nrAgendamento) {
		this.nrAgendamento = nrAgendamento;
	}

	public Date getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public Timestamp getHrAgendamento() {
		return hrAgendamento;
	}

	public void setHrAgendamento(Timestamp hrAgendamento) {
		this.hrAgendamento = hrAgendamento;
	}

	public Timestamp getHrPresenca() {
		return hrPresenca;
	}

	public void setHrPresenca(Timestamp hrPresenca) {
		this.hrPresenca = hrPresenca;
	}

	public Timestamp getHrChegadaInicio() {
		return hrChegadaInicio;
	}

	public void setHrChegadaInicio(Timestamp hrChegadaInicio) {
		this.hrChegadaInicio = hrChegadaInicio;
	}

	public Timestamp getHrChegadaFim() {
		return hrChegadaFim;
	}

	public void setHrChegadaFim(Timestamp hrChegadaFim) {
		this.hrChegadaFim = hrChegadaFim;
	}

	public String getNmProfissional() {
		return nmProfissional;
	}

	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getHoraAgendamento() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrAgendamento != null)
			return df.format(hrAgendamento);
		
		return horaAgendamento;
	}

	public void setHoraAgendamento(String horaAgendamento) {
		this.horaAgendamento = horaAgendamento;
	}

	public String getHoraPresenca() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrPresenca != null)
			return df.format(hrPresenca);
		
		return horaPresenca;
	}

	public void setHoraPresenca(String horaPresenca) {
		this.horaPresenca = horaPresenca;
	}

	public String getHoraChegadaInicio() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrChegadaInicio != null)
			return df.format(hrChegadaInicio);
		
		return horaChegadaInicio;
	}

	public void setHoraChegadaInicio(String horaChegadaInicio) {
		this.horaChegadaInicio = horaChegadaInicio;
	}

	public String getHoraChegadaFim() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrChegadaFim != null)
			return df.format(hrChegadaFim);
		
		return horaChegadaFim;
	}

	public void setHoraChegadaFim(String horaChegadaFim) {
		this.horaChegadaFim = horaChegadaFim;
	}

}
