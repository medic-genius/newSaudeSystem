package br.com.medic.medicsystem.persistence.model;

import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbmedicamento")
public class Medicamento {
	
	@Id
	@Column(name="codigo")
	private Integer codigo;
	
	@Basic
	@Column(name="descricao")
	private String descricao;
	
	@Basic
	@Column(name = "valor")
	private BigDecimal vlMedicamento;
	
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setVlMedicamento(BigDecimal vlMedicamento) {
		this.vlMedicamento = vlMedicamento;
	}
	
	public BigDecimal getVlMedicamento() {
		return vlMedicamento;
	}
}