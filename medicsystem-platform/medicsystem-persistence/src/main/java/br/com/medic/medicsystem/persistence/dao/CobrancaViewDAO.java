package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.CobrancaView;

@ApplicationScoped
@Named("cobrancaview-dao")
public class CobrancaViewDAO extends RelationalDataAccessObject<CobrancaView> {

	public Long getQuantidadeClientesInadimplentesByInadimplenciaRecente(Long idEmpresa, Long idFormaPagamento, Integer dias){
		//A abordagem de dividir a quantidade de dias por 28 e multiplicar seu piso por 30 é utilizada para evitar
		//o problema que acontece quando as datas possuem meses com 28, 29, 31 dias (não múltiplos de 30), pois a query
		//foi preparar para receber apenas os múltiplos de 30 (30 dias - 1 mês, 60 dias - meses....)
		Integer qtdeCobrancasEmAberto = (int) Math.floor(dias/28);
		Integer diasTotal = qtdeCobrancasEmAberto * 30;
		String query = "";
		
		if( idFormaPagamento != null && (idFormaPagamento.equals(1L) || idFormaPagamento.equals(2L)) )
			qtdeCobrancasEmAberto = qtdeCobrancasEmAberto - 1;

		query =	
		"select count(tb) from ( select t1.idcliente as id, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular, sum(inadimplente) as qdtedevido, sum(vltotal) as somatotaldividas, sum(vljuros) as somatotaljuros, sum(vlmultaatraso) as somatotalmultas "
		+ "from realvida.tbcobranca_mp_view t1 "
		+ "where intipo = 'M' and inadimplente = 1 and idcontrato IN ( "
		+ "select idcontrato from ( "
		+ "select c.idcontrato, count(idmensalidade) "
		+ "from realvida.tbmensalidade m inner join realvida.tbcontrato c on c.idcontrato = m.idcontrato and c.insituacao = 0 "
		+ "where dtvencimento between current_date - interval '" + diasTotal + " days' and current_date AND m.dtexclusao IS NULL AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) AND m.instatus = 0 "
		+ "AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5 "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) "
		+ "group by c.idcontrato "		
		+ "having count(*) = " + qtdeCobrancasEmAberto + ") t1 "
		+ "where ( select count(*) "
		+ "from realvida.tbmensalidade m "
		+ "where m.idcontrato = t1.idcontrato "
		+ "AND m.dtvencimento between '2006-01-01' "
		+ "AND current_date - interval '" + diasTotal + " days' "
		+ "AND m.dtexclusao IS NULL "
		+ "AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) "
		+ "AND m.instatus = 0 AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5 "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) ) < 1) ";
	
		if(idEmpresa != null){
			query +=	"AND t1.idempresagrupo = " + idEmpresa +" ";
		}
		
		if(idFormaPagamento != null){
			query +=	"AND t1.informapagamento = " + idFormaPagamento +" ";
		}
		
		query+= "group by t1.idcliente, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular) tb;";
				
		Query q = findByNativeQuery(query);
		
		BigInteger r = (BigInteger) q.getSingleResult();

		return r.longValueExact();
	}
	

	@SuppressWarnings("unchecked")
	public List <CobrancaView> getClientesInadimplentesByInadimplenciaRecente(Long idEmpresa, Long idFormaPagamento, Integer startPosition, Integer maxResults, Integer dias, Boolean boPaginar){
		//A abordagem de dividir a quantidade de dias por 28 e multiplicar seu piso por 30 é utilizada para evitar
		//o problema que acontece quando as data possuem meses com 28, 29, 31 dias (não múltiplos de 30), pois a query
		//foi preparar para receber apenas os múltiplos de 30 (30 dias - 1 mês, 60 dias - meses....)
		Integer qtdeCobrancasEmAberto = (int) Math.floor(dias/28);
		Integer diasTotal = qtdeCobrancasEmAberto * 30;		
		String query = "";
		
		if( idFormaPagamento != null && (idFormaPagamento.equals(1L) || idFormaPagamento.equals(2L)) )	
			qtdeCobrancasEmAberto = qtdeCobrancasEmAberto - 1;	
		
		query =	
		"select t1.idcliente as id, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular, sum(inadimplente) as qdtedevido, sum(vltotal) as somatotaldividas, sum(vljuros) as somatotaljuros, sum(vlmultaatraso) as somatotalmultas, sum (case when intipo = 'M' then 1 else 0 end) as qtdeMensalidade, sum (case when intipo = 'D' then 1 else 0 end) as qtdeDespesa "
		+ "from realvida.tbcobranca_mp_view t1 "
		+ "where intipo = 'M' and inadimplente = 1 and idcontrato IN ( "
		+ "select idcontrato from ( "
		+ "select c.idcontrato, count(idmensalidade) "
		+ "from realvida.tbmensalidade m inner join realvida.tbcontrato c on c.idcontrato = m.idcontrato and c.insituacao = 0 "
		+ "where dtvencimento between current_date - interval '" + diasTotal + " days' and current_date AND m.dtexclusao IS NULL AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) AND m.instatus = 0 "
		+ "AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5"
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) "
		+ "group by c.idcontrato "
		+ "having count(*) = " + qtdeCobrancasEmAberto + ") t1 "
		+ "where ( select count(*) "
		+ "from realvida.tbmensalidade m "
		+ "where m.idcontrato = t1.idcontrato "
		+ "AND m.dtvencimento between '2006-01-01' "
		+ "AND current_date - interval '" + diasTotal + " days' "
		+ "AND m.dtexclusao IS NULL "
		+ "AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) "
		+ "AND m.instatus = 0 AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5"
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) ) < 1) ";
	
		if(idEmpresa != null){
			query +=	"AND t1.idempresagrupo = " + idEmpresa +" ";
		}
		
		if(idFormaPagamento != null){
			query +=	"AND t1.informapagamento = " + idFormaPagamento +" ";
		}
		
		query+= "group by t1.idcliente, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular "
		+ "order by t1.nmcliente ";
		
		if(boPaginar != null && boPaginar){
			query+=" limit "+ maxResults +" offset " + startPosition + ";"; 
		}
		
			
			
		List<CobrancaView> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("id", LongType.INSTANCE)
		        .addScalar("nmCliente", StringType.INSTANCE)
		        .addScalar("nrCpf", StringType.INSTANCE)
		        .addScalar("nrTelefone", StringType.INSTANCE)
		        .addScalar("nrCelular", StringType.INSTANCE)
		        .addScalar("qdteDevido", LongType.INSTANCE)
		        .addScalar("somaTotalDividas", DoubleType.INSTANCE)
		        .addScalar("somaTotalJuros", DoubleType.INSTANCE)
		        .addScalar("somaTotalMultas", DoubleType.INSTANCE)
		        .addScalar("qtdeMensalidade", LongType.INSTANCE)
		        .addScalar("qtdeDespesa", LongType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(CobrancaView.class)).list();
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List <CobrancaView> getClientesInadimplentesByInadimplenciaRecenteXLS(Long idEmpresa, Long idFormaPagamento, Integer startPosition, Integer maxResults, Integer dias){
		//A abordagem de dividir a quantidade de dias por 28 e multiplicar seu piso por 30 é utilizada para evitar
		//o problema que acontece quando as data possuem meses com 28, 29, 31 dias (não múltiplos de 30), pois a query
		//foi preparar para receber apenas os múltiplos de 30 (30 dias - 1 mês, 60 dias - meses....)
		Integer qtdeCobrancasEmAberto = (int) Math.floor(dias/28);
		Integer diasTotal = qtdeCobrancasEmAberto * 30;		
		String query = "";
		
		if( idFormaPagamento != null && (idFormaPagamento.equals(1L) || idFormaPagamento.equals(2L)) )	
			qtdeCobrancasEmAberto = qtdeCobrancasEmAberto - 1;	
		
		query =	
		"select t1.idcliente as id, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular, sum(inadimplente) as qdtedevido, sum(vltotal) as somatotaldividas, sum(vljuros) as somatotaljuros, sum(vlmultaatraso) as somatotalmultas, sum (case when intipo = 'M' then 1 else 0 end) as qtdeMensalidade, sum (case when intipo = 'D' then 1 else 0 end) as qtdeDespesa, "
		+ "t1.nmcidade, t1.nmbairro, t1.nmlogradouro, t1.nrnumero, t1.nmcomplemento, t1.nrcep, t1.nmpontoreferencia "
		+ "from realvida.tbcobranca_mp_view t1 "
		+ "where intipo = 'M' and inadimplente = 1 and idcontrato IN ( "
		+ "select idcontrato from ( "
		+ "select c.idcontrato, count(idmensalidade) "
		+ "from realvida.tbmensalidade m inner join realvida.tbcontrato c on c.idcontrato = m.idcontrato and c.insituacao = 0 "
		+ "where dtvencimento between current_date - interval '" + diasTotal + " days' and current_date AND m.dtexclusao IS NULL AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) AND m.instatus = 0 "
		+ "AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5"
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) "
		+ "group by c.idcontrato "
		+ "having count(*) = " + qtdeCobrancasEmAberto + ") t1 "
		+ "where ( select count(*) "
		+ "from realvida.tbmensalidade m "
		+ "where m.idcontrato = t1.idcontrato "
		+ "AND m.dtvencimento between '2006-01-01' "
		+ "AND current_date - interval '" + diasTotal + " days' "
		+ "AND m.dtexclusao IS NULL "
		+ "AND m.dtvencimento <= cast (cast('now' as text) as date) "
		+ "AND (m.bonegociada IS NULL OR m.bonegociada = false) "
		+ "AND m.instatus = 0 AND (m.dtvencimento < (cast (cast('now' as text) as date) - cast ('35 days'as interval)) "
		+ "AND (m.informapagamento = ANY (ARRAY[0, 1])) "
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('10 days' as interval)) AND m.informapagamento = 5"
		+ "OR m.dtvencimento < (cast (cast('now' as text) as date) - cast('4 days' as interval)) AND m.informapagamento = 30) ) < 1) ";
	
		if(idEmpresa != null){
			query +=	"AND t1.idempresagrupo = " + idEmpresa +" ";
		}
		
		if(idFormaPagamento != null){
			query +=	"AND t1.informapagamento = " + idFormaPagamento +" ";
		}
		
		query+= "group by t1.idcliente, t1.nmcliente, t1.nrcpf, t1.nrtelefone, t1.nrcelular, t1.nmcidade, t1.nmbairro, t1.nmlogradouro, t1.nrnumero, t1.nmcomplemento, t1.nrcep, t1.nmpontoreferencia "
		+ "order by t1.nmcliente ";
//		+ "limit "+ maxResults +" offset " + startPosition + ";"; 
			
		List<CobrancaView> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("id", LongType.INSTANCE)
		        .addScalar("nmCliente", StringType.INSTANCE)
		        .addScalar("nrCpf", StringType.INSTANCE)
		        .addScalar("nrTelefone", StringType.INSTANCE)
		        .addScalar("nrCelular", StringType.INSTANCE)
		        .addScalar("qdteDevido", LongType.INSTANCE)
		        .addScalar("somaTotalDividas", DoubleType.INSTANCE)
		        .addScalar("somaTotalJuros", DoubleType.INSTANCE)
		        .addScalar("somaTotalMultas", DoubleType.INSTANCE)
		        .addScalar("qtdeMensalidade", LongType.INSTANCE)
		        .addScalar("qtdeDespesa", LongType.INSTANCE)
		        .addScalar("nmCidade", StringType.INSTANCE)
		        .addScalar("nmBairro", StringType.INSTANCE)
		        .addScalar("nmLogradouro", StringType.INSTANCE)
		        .addScalar("nrNumero", StringType.INSTANCE)
		        .addScalar("nmComplemento", StringType.INSTANCE)
		        .addScalar("nrCEP", StringType.INSTANCE)
		        .addScalar("nmPontoReferencia", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(CobrancaView.class)).list();
			
		return result;
	}

	public List<CobrancaView> getClientesInadimplentesFromCobrancaView(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Integer startPosition, Integer maxResults, Boolean boAdimplente, Boolean boPaginar){	
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CobrancaView> criteria = cb.createQuery(CobrancaView.class);
		
		Root<CobrancaView> cobrancaRoot = criteria.from(CobrancaView.class);
		criteria.multiselect(cobrancaRoot.get("id"), 
							cobrancaRoot.get("nmCliente"),
							cobrancaRoot.get("nrCpf"),
							cobrancaRoot.get("nrTelefone"),
							cobrancaRoot.get("nrCelular"),
							cobrancaRoot.get("ultimoContato"),
							cb.sum(cobrancaRoot.get("inInadimplente")),
							cb.sum(cobrancaRoot.get("valorDivida")),
							cb.sum(cobrancaRoot.get("valorJuros")),
							cb.sum(cobrancaRoot.get("valorMultaAtraso")),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "M"), 1).otherwise(0)),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "D"), 1).otherwise(0))
				);
		
		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteria.where(predicate);
		criteria.groupBy(cobrancaRoot.get("id"), cobrancaRoot.get("nmCliente"),	cobrancaRoot.get("nrCpf"), 
				cobrancaRoot.get("nrTelefone"), cobrancaRoot.get("nrCelular"), cobrancaRoot.get("ultimoContato"));

		criteria.orderBy(cb.asc(cobrancaRoot.get("nmCliente")));
		
		TypedQuery<CobrancaView> q = entityManager.createQuery(criteria);
		if(boPaginar != null && boPaginar){
			
			startPosition = startPosition == null ? 0 : startPosition;
			maxResults = maxResults == null ? 10 : maxResults;
			q.setFirstResult(startPosition);
			q.setMaxResults(maxResults);
		}

		List<CobrancaView> result = q.getResultList();
		
		return result;
	}
	
	//para gerar arquivo xls
	
		public List<CobrancaView> getClientesInadimplentesFromCobrancaViewXLS(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
				String nomeCliente, Date dataInicio, Date dataFim, Integer startPosition, Integer maxResults, Boolean boAdimplente){	
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<CobrancaView> criteria = cb.createQuery(CobrancaView.class);
			
			Root<CobrancaView> cobrancaRoot = criteria.from(CobrancaView.class);
			criteria.multiselect(cobrancaRoot.get("id"), 
								cobrancaRoot.get("nmCliente"),
								cobrancaRoot.get("nrCpf"),
								cobrancaRoot.get("nrTelefone"),
								cobrancaRoot.get("nrCelular"),
								cobrancaRoot.get("nmCidade"),
								cobrancaRoot.get("nmBairro"),
								cobrancaRoot.get("nmLogradouro"),
								cobrancaRoot.get("nrNumero"),
								cobrancaRoot.get("nmComplemento"),
								cobrancaRoot.get("nrCEP"),
								cobrancaRoot.get("nmPontoReferencia"),
								cb.sum(cobrancaRoot.get("inInadimplente")),
								cb.sum(cobrancaRoot.get("valorDivida")),
								cb.sum(cobrancaRoot.get("valorJuros")),
								cb.sum(cobrancaRoot.get("valorMultaAtraso")),
								cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "M"), 1).otherwise(0)),
								cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "D"), 1).otherwise(0))
					);
			
			Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
			
			criteria.where(predicate);
			criteria.groupBy(cobrancaRoot.get("id"), cobrancaRoot.get("nmCliente"), cobrancaRoot.get("nmCidade"),
					cobrancaRoot.get("nmBairro"),cobrancaRoot.get("nmLogradouro"), cobrancaRoot.get("nrCpf"), 
					cobrancaRoot.get("nrTelefone"), cobrancaRoot.get("nrCelular"),cobrancaRoot.get("nrNumero"),
					cobrancaRoot.get("nmComplemento"),cobrancaRoot.get("nrCEP"),cobrancaRoot.get("nmPontoReferencia"));

			criteria.orderBy(cb.asc(cobrancaRoot.get("nmCliente")));
			
			TypedQuery<CobrancaView> q = entityManager.createQuery(criteria);
//			startPosition = startPosition == null ? 0 : startPosition;
//			maxResults = maxResults == null ? 10 : maxResults;
//			q.setFirstResult(startPosition);
//			q.setMaxResults(maxResults);
			List<CobrancaView> result = q.getResultList();
			
			return result;
		}
	
	
	/**
	 * Retorna a quantidade de clientes dentro dos filtros especificados.
	 * */
	
	public Long getQuantidadeClientesInadimplentes(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Boolean boAdimplente){	

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = cb.createQuery();
		
		Root<CobrancaView> cobrancaRoot = criteriaQuery.from(CobrancaView.class);
		criteriaQuery.select(cobrancaRoot.get("id"));
		criteriaQuery.distinct(true);

		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteriaQuery.where(predicate);
		
		CriteriaQuery<Long> criteriaQueryCount = criteriaQuery.select(cb.countDistinct(cobrancaRoot));
		TypedQuery<Long> typedQueryCount = entityManager.createQuery(criteriaQueryCount);
		Long totalLinhas = typedQueryCount.getSingleResult();
		
		return totalLinhas;
	}
	
	private Predicate getWhereClause(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, 
			Date dataInicio, Date dataFim, CriteriaBuilder cb, Root<CobrancaView> cobrancaRoot, Boolean boAdimplente){
		Predicate predicate = cb.conjunction();
		
		if(idEmpresa != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("idEmpresaGrupo"), idEmpresa));
		}
		
		if(idFormaPagamento != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inFormaDePagamento"), idFormaPagamento));
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			predicate = cb.and(predicate, cb.between(cobrancaRoot.get("dataVencimento"), dataInicio, dataFim));
		}
		
		if (nomeCliente != null) {
			String where_cliente;

			if(nomeCliente.startsWith("%") || nomeCliente.endsWith("%")) {
				where_cliente = nomeCliente.toUpperCase();

			} else {
				where_cliente = nomeCliente.toUpperCase() + "%";
			}

			predicate = cb.and(predicate, cb.like(cobrancaRoot.get("nmCliente"), where_cliente));
		}
				
		if(boAdimplente == null || !boAdimplente){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inInadimplente"), 1));
		}
		
		return predicate;
	}
	
}
