package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Negociacao;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;

@Named("despesa-dao")
@ApplicationScoped
public class DespesaDAO extends RelationalDataAccessObject<Despesa> {

	public List<ServicoProfissional> getServicosPorProfissional(
	        Long idProfissional) {
		String queryStr = "SELECT sp FROM ServicoProfissional sp "
		        + "WHERE sp.profissional.id = " + idProfissional
		        + " AND sp.servico.boBloqueio = false "
		        + "ORDER BY sp.servico.nmServico ASC";
		TypedQuery<ServicoProfissional> query = entityManager.createQuery(
		        queryStr, ServicoProfissional.class);

		List<ServicoProfissional> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<DespesaServico> getDespesaServicos(Long idDespesa) {

		String queryStr = "SELECT ds FROM DespesaServico ds "
		        + "WHERE ds.despesa.id = " + idDespesa;
		TypedQuery<DespesaServico> query = entityManager.createQuery(queryStr,
		        DespesaServico.class);

		List<DespesaServico> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}

	public List<Parcela> getDespesaParcelas(Long idDespesa) {
		String queryStr = "SELECT p FROM Parcela p WHERE p.despesa.id = "
		        + idDespesa;
		TypedQuery<Parcela> query = entityManager.createQuery(queryStr,
		        Parcela.class);

		List<Parcela> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Long getNovoCodigoDespesa() {

		String queryStr = "select coalesce(max(cast(coalesce(iddespesa, '0') as int8)), 0) as nrdesp from realvida.tbdespesa";

		Query q = findByNativeQuery(queryStr);

		BigInteger result = (BigInteger) q.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(
			        "Um novo codigo de despesa nao pode ser gerado");
		}

		return result.longValue();

	}

	public Despesa getDespesa(Long id) {
		return searchByKey(Despesa.class, id);
	}

	public List<DespesaServico> getDespesaServicosConnectedToConferencia(
	        Long idDespesa) {
		String queryStr = "SELECT ds FROM DespesaServico ds WHERE ds.despesa.id = "
		        + idDespesa + " AND ds.conferencia is not null";
		TypedQuery<DespesaServico> query = entityManager.createQuery(queryStr,
		        DespesaServico.class);

		List<DespesaServico> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<Negociacao> getNegociacao(Long idDespesa) {
		String queryStr = "SELECT d FROM Negociacao d " + " WHERE "
		        + " d.despesa.id = " + idDespesa;

		TypedQuery<Negociacao> query = entityManager.createQuery(queryStr,
		        Negociacao.class);

		List<Negociacao> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<Despesa> getDespesaByContratoClienteMesAno(ContratoCliente contratoCliente, String mesAno){
		
		String nrDespesa = contratoCliente.getContrato().getEmpresaCliente().getId() + "/" + mesAno;
		
		String queryStr = "From Despesa "
				+ " where (boExcluida is null or boExcluida is false) "
				+ " and contratoCliente.id = " + contratoCliente.getId()
				+ " and nrDespesa = '" + nrDespesa + "'";
//				+ " and month(dtDespesa) = " + Integer.valueOf(mesAnoSplit[0]) 
//                + " and year(dtDespesa) = " + Integer.valueOf(mesAnoSplit[1]);
		
		TypedQuery<Despesa> query = entityManager.createQuery(queryStr, Despesa.class);
		
		try {

			List<Despesa> result = query.getResultList();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return null;
		}
		
	}
}
