package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbplano")
public class Plano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLANO_ID_SEQ")
	@SequenceGenerator(name = "PLANO_ID_SEQ", sequenceName = "realvida.plano_id_seq", allocationSize = 1)
	@Column(name = "idplano")
	private Long id;

	@Basic
	@Column(name = "bolivrebloqueio")
	private Boolean boLivreBloqueio;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtcadastro")
	private Date dtCadastro;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;

	@Basic
	@Column(name = "idempresagrupo")
	private Long idEmpresaGrupo;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "idplanoaux")
	private Long idPlanoAux;

	@Basic
	@Column(name = "idplanoodonto")
	private Long idPlanoOdonto;

	@Basic
	@Column(name = "intipocobranca")
	private Integer inTipoCobranca;

	@Basic
	@Column(name = "intipoplano")
	private Integer inTipoPlano;

	@Basic
	@Column(name = "nmplano")
	private String nmPlano;

	@Basic
	@Column(name = "nrqtdemensalidade")
	private Integer nrQtdeMensalidade;

	@Basic
	@Column(name = "observacao")
	private String observacao;

	@Basic
	@Column(name = "totalconsulta")
	private Integer totalConsulta;

	@Basic
	@Column(name = "vlplano")
	private Double vlPlano;
	
	@Basic
	@Column(name="idrecorrenciaplano")
	private Integer idRecorrenciaPlano;
	
	@Basic
	@Column(name = "bovenda")
	private Boolean boVenda;
	
	@ManyToOne
	@JoinColumn(name = "idempresacliente")
	private EmpresaCliente empresaCliente;
			

	public Plano() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoLivreBloqueio() {
		return boLivreBloqueio;
	}

	public void setBoLivreBloqueio(Boolean boLivreBloqueio) {
		this.boLivreBloqueio = boLivreBloqueio;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Long getIdPlanoAux() {
		return idPlanoAux;
	}

	public void setIdPlanoAux(Long idPlanoAux) {
		this.idPlanoAux = idPlanoAux;
	}

	public Long getIdPlanoOdonto() {
		return idPlanoOdonto;
	}

	public void setIdPlanoOdonto(Long idPlanoOdonto) {
		this.idPlanoOdonto = idPlanoOdonto;
	}

	public Integer getInTipoCobranca() {
		return inTipoCobranca;
	}

	public void setInTipoCobranca(Integer inTipoCobranca) {
		this.inTipoCobranca = inTipoCobranca;
	}

	public Integer getInTipoPlano() {
		return inTipoPlano;
	}

	public void setInTipoPlano(Integer inTipoPlano) {
		this.inTipoPlano = inTipoPlano;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Integer getNrQtdeMensalidade() {
		return nrQtdeMensalidade;
	}

	public void setNrQtdeMensalidade(Integer nrQtdeMensalidade) {
		this.nrQtdeMensalidade = nrQtdeMensalidade;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getTotalConsulta() {
		return totalConsulta;
	}

	public void setTotalConsulta(Integer totalConsulta) {
		this.totalConsulta = totalConsulta;
	}

	public Double getVlPlano() {
		return vlPlano;
	}

	public void setVlPlano(Double vlPlano) {
		this.vlPlano = vlPlano;
	}

	public Integer getIdRecorrenciaPlano() {
		return idRecorrenciaPlano;
	}

	public void setIdRecorrenciaPlano(Integer idRecorrenciaPlano) {
		this.idRecorrenciaPlano = idRecorrenciaPlano;
	}

	public EmpresaCliente getEmpresaCliente() {
		return empresaCliente;
	}

	public void setEmpresaCliente(EmpresaCliente empresaCliente) {
		this.empresaCliente = empresaCliente;
	}
	
	public Boolean getBoVenda() {
		return boVenda;
	}

	public void setBoVenda(Boolean boVenda) {
		this.boVenda = boVenda;
	}

}
