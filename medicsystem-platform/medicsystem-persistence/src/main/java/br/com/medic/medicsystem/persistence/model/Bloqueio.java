package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(catalog = "realvida", name = "tbbloqueio")
public class Bloqueio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLOQUEIO_ID_SEQ")
	@SequenceGenerator(name = "BLOQUEIO_ID_SEQ", sequenceName = "realvida.bloqueio_id_seq")
	@Column(name = "idbloqueio")
	private Long id;

	@Basic
	@Column(name = "bonaoativo")
	private Boolean boNaoAtivo;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Temporal(TemporalType.DATE)
	@Basic
	@Column(name = "dtexclusaobloqueio")
	private Date dtExclusaoBloqueio;

	@Temporal(TemporalType.DATE)
	@Basic
	@Column(name = "dtfim")
	private Date dtFim;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Temporal(TemporalType.DATE)
	@Basic
	@Column(name = "dtinicio")
	private Date dtInicio;

	@Basic
	@Column(name = "intipoatendimento")
	private Integer inTipoAtendimento;

	@Basic
	@Column(name = "inturno")
	private Integer inTurno;

	@Basic
	@Column(name = "nmbloqueio")
	private String nmBloqueio;

	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Profissional profissional;

	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;

	public Bloqueio() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoNaoAtivo() {
		return boNaoAtivo;
	}

	public void setBoNaoAtivo(Boolean boNaoAtivo) {
		this.boNaoAtivo = boNaoAtivo;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusaoBloqueio() {
		return dtExclusaoBloqueio;
	}

	public void setDtExclusaoBloqueio(Date dtExclusaoBloqueio) {
		this.dtExclusaoBloqueio = dtExclusaoBloqueio;
	}

	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Integer getInTipoAtendimento() {
		return inTipoAtendimento;
	}

	public void setInTipoAtendimento(Integer inTipoAtendimento) {
		this.inTipoAtendimento = inTipoAtendimento;
	}

	public Integer getInTurno() {
		return inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public String getNmBloqueio() {
		return nmBloqueio;
	}

	public void setNmBloqueio(String nmBloqueio) {
		this.nmBloqueio = nmBloqueio;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}