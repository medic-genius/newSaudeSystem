package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.List;

public class ClientesPaginadosDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long quantidadeClientesTotal;
	private List<ClienteInadimplenteDTO> clientes;

	public Long getQuantidadeClientesTotal() {
		return quantidadeClientesTotal;
	}
	public void setQuantidadeClientesTotal(Long quantidadeClientesTotal) {
		this.quantidadeClientesTotal = quantidadeClientesTotal;
	}
	public List<ClienteInadimplenteDTO> getClientes() {
		return clientes;
	}
	public void setClientes(List<ClienteInadimplenteDTO> clientes) {
		this.clientes = clientes;
	}
	
}
