package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "view_aniversariantes_dia")
public class AniversariantesView {

	@Id
	@Column(name = "idcliente")
	private Long id;

	@Basic
	@Column(name = "cliente")
	private String nmCliente;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "nascimento")
	private Date dtNascimento;

	@Basic
	@Column(name = "telefone")
	private String nrTelefone;

	@Basic
	@Column(name = "celular")
	private String nrCelular;
	
	@Basic
	@Column(name = "dia")
	private String diaNascimento;
	
	@Basic
	@Column(name = "mes")
	private String mesNascimento;
	
	@Basic
	@Column(name = "ano")
	private String anoNascimento;
	
	@Basic
	@Column(name = "idade")
	private String idadeCliente;

	public AniversariantesView() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getDiaNascimento() {
		return diaNascimento;
	}

	public void setDiaNascimento(String diaNascimento) {
		this.diaNascimento = diaNascimento;
	}

	public String getMesNascimento() {
		return mesNascimento;
	}

	public void setMesNascimento(String mesNascimento) {
		this.mesNascimento = mesNascimento;
	}

	public String getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(String anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getIdadeCliente() {
		return idadeCliente;
	}

	public void setIdadeCliente(String idadeCliente) {
		this.idadeCliente = idadeCliente;
	}

}

