package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.NewProspect;

@Named("newprospect-dao")
@ApplicationScoped
public class NewProspectDAO extends RelationalDataAccessObject<NewProspect>{
	
	public NewProspect getNewProspectbyCpf( String nrCpf){
		
		String queryStr = "SELECT np FROM NewProspect np WHERE np.nrCpf = '" + nrCpf + "'";
		
		TypedQuery<NewProspect> result = entityManager
		        .createQuery(queryStr, NewProspect.class);
		
		try {
			NewProspect resultNp = result.getSingleResult();
			
			if(resultNp != null)
				return resultNp;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return null;
	}

}
