package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.TemplateReceituario;

@Named("template-receituario-dao")
@ApplicationScoped
public class TemplateReceituarioDAO extends RelationalDataAccessObject<TemplateReceituario> {
	
	public List<TemplateReceituario> getTemplatesReceituarioForEspecialidadeProfissional(
			Long idMedico, Long idEspecialidade) {
		String queryStr = "SELECT tpl FROM TemplateReceituario tpl "
				+ "WHERE tpl.dtExclusao IS NULL AND tpl.espProfissional.funcionario.id = :idMedico "
				+ "AND tpl.espProfissional.especialidade.id = :idEspecialidade";
		try {
			TypedQuery<TemplateReceituario> query = createQuery(queryStr, TemplateReceituario.class)
					.setParameter("idMedico", idMedico)
					.setParameter("idEspecialidade", idEspecialidade);
			return query.getResultList();
		} catch(Exception e) {
			
		}
		return null;
	}
	
	public TemplateReceituario getTemplateReceituario(Long idTemplate) {
		try {
			return this.searchByKey(TemplateReceituario.class, idTemplate);
		} catch(Exception e) {
			
		}
		return null;
	}
}
