package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tbretornosiape database table.
 * 
 */
@Entity
public class RetornoSiape implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBRETORNOSIAPE_IDRETORNOSIAPE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBRETORNOSIAPE_IDRETORNOSIAPE_GENERATOR")
	private Long idretornosiape;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private String dtreferencia;

	@Temporal(TemporalType.DATE)
	private Date dtretornosiape;

	private Integer insituacaoarquivo;

	private Integer intipoarquivo;

	//bi-directional many-to-one association to Tbempresagrupo
	@ManyToOne
	@JoinColumn(name="idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public RetornoSiape() {
	}

	public Long getIdretornosiape() {
		return this.idretornosiape;
	}

	public void setIdretornosiape(Long idretornosiape) {
		this.idretornosiape = idretornosiape;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public String getDtreferencia() {
		return this.dtreferencia;
	}

	public void setDtreferencia(String dtreferencia) {
		this.dtreferencia = dtreferencia;
	}

	public Date getDtretornosiape() {
		return this.dtretornosiape;
	}

	public void setDtretornosiape(Date dtretornosiape) {
		this.dtretornosiape = dtretornosiape;
	}

	public Integer getInsituacaoarquivo() {
		return this.insituacaoarquivo;
	}

	public void setInsituacaoarquivo(Integer insituacaoarquivo) {
		this.insituacaoarquivo = insituacaoarquivo;
	}

	public Integer getIntipoarquivo() {
		return this.intipoarquivo;
	}

	public void setIntipoarquivo(Integer intipoarquivo) {
		this.intipoarquivo = intipoarquivo;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return this.empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public Funcionario getTbfuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario tbfuncionario) {
		this.funcionario = tbfuncionario;
	}

}