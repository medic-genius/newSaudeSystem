package br.com.medic.dashboard.persistence.model;

public class FaturamentoMesAMes {

	private String mes;
	private Double contratosnovos;
	private Double contratosinativados;

	public FaturamentoMesAMes() {
		// TODO Auto-generated constructor stub
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Double getContratosnovos() {
		return contratosnovos;
	}

	public void setContratosnovos(Double contratosnovos) {
		this.contratosnovos = contratosnovos;
	}

	public Double getContratosinativados() {
		return contratosinativados;
	}

	public void setContratosinativados(Double contratosinativados) {
		this.contratosinativados = contratosinativados;
	}

}
