package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbatendimentoprofissional")
public class AtendimentoProfissional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATENDIMENTOPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "ATENDIMENTOPROFISSIONAL_ID_SEQ", sequenceName = "realvida.atendimentoprofissional_id_seq")
	@Column(name = "idatendimentoprofissional")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Date dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Date dtInclusaoLog;
	
	@Basic
	@Column(name = "hrfim")
	private Time hrFim;
	
	@Basic
	@Column(name = "hrinicio")
	private Time hrInicio;
	
	@Basic
	@Column(name = "hrtempoconsulta")
	private Time hrTempoConsulta;
	
	@Basic
	@Column(name = "indiasemana")
	private Integer inDiaSemana;
	
	@Basic
	@Column(name = "intipoatendimento")
	private Integer inTipoAtendimento;
	
	@Basic
	@Column(name = "intipoconsulta")
	private Integer inTipoConsulta;
	
	@Basic
	@Column(name = "inturno")
	private Integer inTurno;
	
	@Basic
	@Column(name = "qtpacienteatender")
	private Integer qtPacienteAtender;

	@ManyToOne
	@JoinColumn(name="idespprofissional")
	private EspProfissional espProfissional;

	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Profissional profissional;

//	@ManyToOne
//	@JoinColumn(name="idsubespprofissional")
//	private SubEspProfissional subEspProfissional;

	@ManyToOne
	@JoinColumn(name="idunidade")
	private Unidade unidade;
	
	@ManyToOne
	@JoinColumn(name="idconsultorio")
	private Consultorio consultorio;
	
	@Basic
	@Column(name = "boordemproducao")
	private Boolean boOrdemProducao;
	
	@Basic
	@Column(name = "hrordeminicio")
	private Time hrOrdemInicio;
	
	@Basic
	@Column(name = "hrordemfim")
	private Time hrOrdemFim;
	
	@Basic
	@Column(name = "boparticular")
	private Boolean boParticular;
	
	@Basic
	@Column(name = "bohorista")
	private Boolean boHorista;
	
	@Basic
	@Column(name = "boproducao")
	private Boolean boProducao;
	
	@Basic
	@Column(name = "bofixo")
	private Boolean boFixo;
	
	@Basic
	@Column(name = "vlhora")
	private Double vlHora;
	
	@Basic
	@Column(name = "hrfimcontratado")
	private Time hrFimContratado;
	
	@Basic
	@Column(name = "hriniciocontratado")
	private Time hrInicioContratado;
	
	@Basic
	@Column(name = "boaceitaencaixe")
	private Boolean boAceitaEncaixe;

	@Basic
	@Column(name = "bobarcode")
	private Boolean boBarcode;

	public AtendimentoProfissional() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Date dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Date dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Time getHrFim() {
		return hrFim;
	}

	public void setHrFim(Time hrFim) {
		this.hrFim = hrFim;
	}

	public Time getHrInicio() {
		return hrInicio;
	}

	public void setHrInicio(Time hrInicio) {
		this.hrInicio = hrInicio;
	}

	public Time getHrTempoConsulta() {
		return hrTempoConsulta;
	}

	public void setHrTempoConsulta(Time hrTempoConsulta) {
		this.hrTempoConsulta = hrTempoConsulta;
	}

	public Integer getInDiaSemana() {
		return inDiaSemana;
	}

	public void setInDiaSemana(Integer inDiaSemana) {
		this.inDiaSemana = inDiaSemana;
	}

	public Integer getInTipoAtendimento() {
		return inTipoAtendimento;
	}

	public void setInTipoAtendimento(Integer inTipoAtendimento) {
		this.inTipoAtendimento = inTipoAtendimento;
	}

	public Integer getInTipoConsulta() {
		return inTipoConsulta;
	}

	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}

	public Integer getInTurno() {
		return inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public Integer getQtPacienteAtender() {
		return qtPacienteAtender;
	}

	public void setQtPacienteAtender(Integer qtPacienteAtender) {
		this.qtPacienteAtender = qtPacienteAtender;
	}

	public EspProfissional getEspProfissional() {
		return espProfissional;
	}

	public void setEspProfissional(EspProfissional espProfissional) {
		this.espProfissional = espProfissional;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

//	public SubEspProfissional getSubEspProfissional() {
//		return subEspProfissional;
//	}
//
//	public void setSubEspProfissional(SubEspProfissional subEspProfissional) {
//		this.subEspProfissional = subEspProfissional;
//	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Consultorio getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(Consultorio consultorio) {
		this.consultorio = consultorio;
	}

	public Boolean getBoOrdemProducao() {
		return boOrdemProducao;
	}

	public void setBoOrdemProducao(Boolean boOrdemProducao) {
		this.boOrdemProducao = boOrdemProducao;
	}

	public Time getHrOrdemInicio() {
		return hrOrdemInicio;
	}

	public void setHrOrdemInicio(Time hrOrdemInicio) {
		this.hrOrdemInicio = hrOrdemInicio;
	}

	public Time getHrOrdemFim() {
		return hrOrdemFim;
	}

	public void setHrOrdemFim(Time hrOrdemFim) {
		this.hrOrdemFim = hrOrdemFim;
	}

	public Boolean getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}

	public Boolean getBoHorista() {
		return boHorista;
	}

	public void setBoHorista(Boolean boHorista) {
		this.boHorista = boHorista;
	}

	public Boolean getBoProducao() {
		return boProducao;
	}

	public void setBoProducao(Boolean boProducao) {
		this.boProducao = boProducao;
	}

	public Boolean getBoFixo() {
		return boFixo;
	}

	public void setBoFixo(Boolean boFixo) {
		this.boFixo = boFixo;
	}

	public Double getVlHora() {
		return vlHora;
	}

	public void setVlHora(Double vlHora) {
		this.vlHora = vlHora;
	}
	
	public Time getHrFimContratado() {
		return hrFimContratado;
	}

	public void setHrFimContratado(Time hrFimContratado) {
		this.hrFimContratado = hrFimContratado;
	}

	public Time getHrInicioContratado() {
		return hrInicioContratado;
	}

	public void setHrInicioContratado(Time hrInicioContratado) {
		this.hrInicioContratado = hrInicioContratado;
	}
	
	public Boolean getBoAceitaEncaixe() {
		return boAceitaEncaixe;
	}

	public void setBoAceitaEncaixe(Boolean boAceitaEncaixe) {
		this.boAceitaEncaixe = boAceitaEncaixe;
	}

	public Boolean getBoBarcode() {
		return boBarcode;
	}

	public void setBoBarcode(Boolean boBarcode) {
		this.boBarcode = boBarcode;
	}
}