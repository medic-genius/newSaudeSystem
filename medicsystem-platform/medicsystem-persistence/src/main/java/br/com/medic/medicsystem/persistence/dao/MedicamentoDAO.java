package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Medicamento;

@Named("medicamento-dao")
@ApplicationScoped
public class MedicamentoDAO extends RelationalDataAccessObject<Medicamento> {
	
	public List<Medicamento> searchMedicamentos(String nmMedicamento) {
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Medicamento> criteria = cb.createQuery(Medicamento.class);

			Root<Medicamento> medicamentoRoot = criteria.from(Medicamento.class);
			criteria.select(medicamentoRoot);

			String where_med = "%" + nmMedicamento.toLowerCase() + "%";
			criteria.where(cb.like(cb.lower(medicamentoRoot.get("descricao")), where_med));

			TypedQuery<Medicamento> q = entityManager.createQuery(criteria);
			List<Medicamento> result = q.getResultList();

			return result;
		} catch(Exception e) {
			return null;
		}
	}
}