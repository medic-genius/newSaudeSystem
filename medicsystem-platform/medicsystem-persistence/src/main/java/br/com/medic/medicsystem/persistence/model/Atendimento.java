package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(catalog = "realvida", name = "tbatendimento")
public class Atendimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATENDIMENTO_ID_SEQ")
	@SequenceGenerator(name = "ATENDIMENTO_ID_SEQ", sequenceName = "realvida.atendimento_id_seq", allocationSize=1)
	@Column(name = "idatendimento")
	private Long id;
	
	@Basic
	@Column(name = "bofinalizado")
	private Boolean boFinalizado;

	@Basic
	@Column(name = "bosolicitouretorno")
	private Boolean boSolicitouRetorno;

	@Basic
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",  timezone = "America/Manaus")
	@Column(name = "dtatendimento")
	private Date dtAtendimento;

	@Basic
	@Column(name = "dtatualizacaolog")
	@JsonIgnore
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	@JsonIgnore
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "nmcondutamedica")
	private String nmCondutaMedica;

	@Basic
	@Column(name = "nmdiagnostico")
	private String nmDiagnostico;

	@Basic
	@Column(name = "nmqueixapaciente")
	private String nmQueixaPaciente;

	//bi-directional many-to-one association to Tbagendamento
	@ManyToOne
	@JoinColumn(name="idagendamento")
	private Agendamento agendamento;

//	//bi-directional many-to-one association to Tbatestadomedico
//	@OneToMany(mappedBy="atendimento")
//	private List<AtestadoMedico> atestadoMedicos;
//
//	//bi-directional many-to-one association to Tbreceituario
//	@OneToMany(mappedBy="atendimento")
//	private List<Receituario> receituarios;
//
//	//bi-directional many-to-one association to Tbreceituariooftalmologico
//	@OneToMany(mappedBy="atendimento")
//	private List<ReceituarioOftalmologico> receituarioOftalmologicos;
//
//	//bi-directional many-to-one association to Tbservicocliente
//	@OneToMany(mappedBy="atendimento")
//	private List<ServicoCliente> servicoClientes;

	public Atendimento() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoFinalizado() {
		return boFinalizado;
	}

	public void setBoFinalizado(Boolean boFinalizado) {
		this.boFinalizado = boFinalizado;
	}

	public Boolean getBoSolicitouRetorno() {
		return boSolicitouRetorno;
	}

	public void setBoSolicitouRetorno(Boolean boSolicitouRetorno) {
		this.boSolicitouRetorno = boSolicitouRetorno;
	}

	public Date getDtAtendimento() {
		return dtAtendimento;
	}

	public void setDtAtendimento(Date dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getNmCondutaMedica() {
		return nmCondutaMedica;
	}

	public void setNmCondutaMedica(String nmCondutaMedica) {
		this.nmCondutaMedica = nmCondutaMedica;
	}

	public String getNmDiagnostico() {
		return nmDiagnostico;
	}

	public void setNmDiagnostico(String nmDiagnostico) {
		this.nmDiagnostico = nmDiagnostico;
	}

	public String getNmQueixaPaciente() {
		return nmQueixaPaciente;
	}

	public void setNmQueixaPaciente(String nmQueixaPaciente) {
		this.nmQueixaPaciente = nmQueixaPaciente;
	}

	public Agendamento getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}

//	public void setReceituarios(List<Receituario> receituarios) {
//		this.receituarios = receituarios;
//	}
//
//	public void setReceituarioOftalmologicos(
//			List<ReceituarioOftalmologico> receituarioOftalmologicos) {
//		this.receituarioOftalmologicos = receituarioOftalmologicos;
//	}
//
//	public void setServicoClientes(List<ServicoCliente> servicoClientes) {
//		this.servicoClientes = servicoClientes;
//	}
//
//	public List<AtestadoMedico> getAtestadoMedicos() {
//		return this.atestadoMedicos;
//	}
//
//	public void setAtestadoMedicos(List<AtestadoMedico> atestadoMedicos) {
//		this.atestadoMedicos = atestadoMedicos;
//	}
//
//	public AtestadoMedico addAtestadoMedico(AtestadoMedico atestadoMedico) {
//		getAtestadoMedicos().add(atestadoMedico);
//		atestadoMedico.setAtendimento(this);
//
//		return atestadoMedico;
//	}
//
//	public AtestadoMedico removeAtestadomedico(AtestadoMedico atestadomedico) {
//		getAtestadoMedicos().remove(atestadomedico);
//		atestadomedico.setAtendimento(null);
//
//		return atestadomedico;
//	}
//
//	public List<Receituario> getReceituarios() {
//		return this.receituarios;
//	}
//
//	public void setTbreceituarios(List<Receituario> receituarios) {
//		this.receituarios = receituarios;
//	}

//	public Receituario addReceituario(Receituario receituario) {
//		getReceituarios().add(receituario);
//		receituario.setAtendimento(this);
//
//		return receituario;
//	}
//
//	public Receituario removeReceituario(Receituario receituario) {
//		getReceituarios().remove(receituario);
//		receituario.setAtendimento(null);
//
//		return receituario;
//	}
//
//	public List<ReceituarioOftalmologico> getReceituarioOftalmologicos() {
//		return this.receituarioOftalmologicos;
//	}
//
//	public void setTbreceituariooftalmologicos(List<ReceituarioOftalmologico> receituarioOftalmologicos) {
//		this.receituarioOftalmologicos = receituarioOftalmologicos;
//	}

//	public ReceituarioOftalmologico addReceituariooftalmologico(ReceituarioOftalmologico receituarioOftalmologico) {
//		getReceituarioOftalmologicos().add(receituarioOftalmologico);
//		receituarioOftalmologico.setAtendimento(this);
//
//		return receituarioOftalmologico;
//	}
//
//	public ReceituarioOftalmologico removeReceituariooftalmologico(ReceituarioOftalmologico receituarioOftalmologico) {
//		getReceituarioOftalmologicos().remove(receituarioOftalmologico);
//		receituarioOftalmologico.setAtendimento(null);
//
//		return receituarioOftalmologico;
//	}
//
//	public List<ServicoCliente> getServicoClientes() {
//		return this.servicoClientes;
//	}
//
//	public void setTbservicoclientes(List<ServicoCliente> servicoClientes) {
//		this.servicoClientes = servicoClientes;
//	}

//	public ServicoCliente addServicocliente(ServicoCliente servicoCliente) {
//		getServicoClientes().add(servicoCliente);
//		servicoCliente.setAtendimento(this);
//
//		return servicoCliente;
//	}
//
//	public ServicoCliente removeServicocliente(ServicoCliente servicoCliente) {
//		getServicoClientes().remove(servicoCliente);
//		servicoCliente.setAtendimento(null);
//
//		return servicoCliente;
//	}

}