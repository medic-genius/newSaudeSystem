package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Phillip Furtado
 * @since 01-01-2016
 * @version 1.0
 * 
 * last modification 09/05/2016
 * by Joelton Matos
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(catalog = "realvida", name = "tbfuncionario")
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCIONARIO_ID_SEQ")
	@SequenceGenerator(name = "FUNCIONARIO_ID_SEQ", sequenceName = "realvida.funcionario_id_seq", allocationSize = 1)
	@Column(name = "idfuncionario")
	private Long id;

	@Basic
	@Column(name = "bodebitainss")
	private Boolean boDebitaInss;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtcadastro")
	private Date dtCadastro;

	@Basic
	@Column(name = "dtcontrato")
	private Date dtContrato;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtnascimento")
	private Date dtNascimento;

	@Basic
	@Column(name = "idclienteaux")
	private Long idClienteAux;

	@Basic
	@Column(name = "idclienteodonto")
	private Long idClienteOdonto;

	@Basic
	@Column(name = "idfuncionarioaux")
	private Long idFuncionarioAux;

	@Basic
	@Column(name = "idfuncionarioodonto")
	private Long idFuncionarioOdonto;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "inestadocivil")
	private Integer inEstadoCivil;

	@Basic
	@Column(name = "ingrupofuncionario")
	private Integer inGrupoFuncionario;

	@Basic
	@Column(name = "insexo")
	private Integer inSexo;

	@Basic
	@Column(name = "limitedesconto")
	private Double limiteDesconto;

	@Basic
	@Column(name = "nmcargo")
	private String nmCargo;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nmfuncionario")
	private String nmFuncionario;

	@Basic
	@Column(name = "nmlogin")
	private String nmLogin;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nmsenha")
	private String nmSenha;

	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;

	@Basic
	@Column(name = "nrcpf")
	private String nrCpf;

	@Basic
	@Column(name = "nrmatricula")
	private String nrMatricula;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nrrg")
	private String nrRg;

	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;

	@Basic
	@Column(name = "vlacrescimo")
	private Double vlAcrescimo;

	@Basic
	@Column(name = "vlajudacusto")
	private Double vlAjudaCusto;

	@Basic
	@Column(name = "vlbasecalcfalta")
	private Double vlBaseCalcFalta;

	@Basic
	@Column(name = "vlcargahoraria")
	private Double vlCargaHoraria;

	@Basic
	@Column(name = "vlinss")
	private Double vlInss;

	@Basic
	@Column(name = "vlrefeicao")
	private Double vlRefeicao;

	@Basic
	@Column(name = "vlsalario")
	private Double vlSalario;

	@Basic
	@Column(name = "vltransporte")
	private Double vlTransporte;

	@Basic
	@Column(name = "digitalfuncionario")
	private String digitalFuncionario;
	
	@Basic
	@Column(name = "iddedodigital")
	private Integer idDedoDigital;
	
	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "iddepartamento")
	private Departamento departamento;

	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;
	
	@Basic
	@Column(name = "boautorizacaototem")
	private Boolean boAutorizacaoTotem;
	
	@Basic
	@Column(name = "imeicelular")
	private String imeiCelular;
	
	@Basic
	@Column(name = "assinaturadigital")
	private String assinaturaDigital;
	
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "America/Manaus")
	@Basic
	@Column(name = "hrtoleranciaponto")
	private Timestamp hrToleranciaPonto;
	
	@Basic
	@Column(name = "idfuncionarioreferencia")
	private Long idProfissionalRef;

	public Funcionario() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoDebitaInss() {
		return boDebitaInss;
	}

	public void setBoDebitaInss(Boolean boDebitaInss) {
		this.boDebitaInss = boDebitaInss;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Date getDtContrato() {
		return dtContrato;
	}

	public void setDtContrato(Date dtContrato) {
		this.dtContrato = dtContrato;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Long getIdClienteAux() {
		return idClienteAux;
	}

	public void setIdClienteAux(Long idClienteAux) {
		this.idClienteAux = idClienteAux;
	}

	public Long getIdClienteOdonto() {
		return idClienteOdonto;
	}

	public void setIdClienteOdonto(Long idClienteOdonto) {
		this.idClienteOdonto = idClienteOdonto;
	}

	public Long getIdFuncionarioAux() {
		return idFuncionarioAux;
	}

	public void setIdFuncionarioAux(Long idFuncionarioAux) {
		this.idFuncionarioAux = idFuncionarioAux;
	}

	public Long getIdFuncionarioOdonto() {
		return idFuncionarioOdonto;
	}

	public void setIdFuncionarioOdonto(Long idFuncionarioOdonto) {
		this.idFuncionarioOdonto = idFuncionarioOdonto;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Integer getInEstadoCivil() {
		return inEstadoCivil;
	}

	public void setInEstadoCivil(Integer inEstadoCivil) {
		this.inEstadoCivil = inEstadoCivil;
	}

	public Integer getInGrupoFuncionario() {
		return inGrupoFuncionario;
	}

	public void setInGrupoFuncionario(Integer inGrupoFuncionario) {
		this.inGrupoFuncionario = inGrupoFuncionario;
	}

	public Integer getInSexo() {
		return inSexo;
	}

	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}

	public Double getLimiteDesconto() {
		return limiteDesconto;
	}

	public void setLimiteDesconto(Double limiteDesconto) {
		this.limiteDesconto = limiteDesconto;
	}

	public String getNmCargo() {
		return nmCargo;
	}

	public void setNmCargo(String nmCargo) {
		this.nmCargo = nmCargo;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public String getNmLogin() {
		return nmLogin;
	}

	public void setNmLogin(String nmLogin) {
		this.nmLogin = nmLogin;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNmSenha() {
		return nmSenha;
	}

	public void setNmSenha(String nmSenha) {
		this.nmSenha = nmSenha;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrCpf() {
		if (nrCpf != null && nrCpf.length() == 11) {
			Pattern pattern = Pattern
			        .compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCpf);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3-$4");
		}

		return null;
	}
	
	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNrRg() {
		return nrRg;
	}

	public void setNrRg(String nrRg) {
		this.nrRg = nrRg;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public Double getVlAcrescimo() {
		return vlAcrescimo;
	}

	public void setVlAcrescimo(Double vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	public Double getVlAjudaCusto() {
		return vlAjudaCusto;
	}

	public void setVlAjudaCusto(Double vlAjudaCusto) {
		this.vlAjudaCusto = vlAjudaCusto;
	}

	public Double getVlBaseCalcFalta() {
		return vlBaseCalcFalta;
	}

	public void setVlBaseCalcFalta(Double vlBaseCalcFalta) {
		this.vlBaseCalcFalta = vlBaseCalcFalta;
	}

	public Double getVlCargaHoraria() {
		return vlCargaHoraria;
	}

	public void setVlCargaHoraria(Double vlCargaHoraria) {
		this.vlCargaHoraria = vlCargaHoraria;
	}

	public Double getVlInss() {
		return vlInss;
	}

	public void setVlInss(Double vlInss) {
		this.vlInss = vlInss;
	}

	public Double getVlRefeicao() {
		return vlRefeicao;
	}

	public void setVlRefeicao(Double vlRefeicao) {
		this.vlRefeicao = vlRefeicao;
	}

	public Double getVlSalario() {
		return vlSalario;
	}

	public void setVlSalario(Double vlSalario) {
		this.vlSalario = vlSalario;
	}

	public Double getVlTransporte() {
		return vlTransporte;
	}

	public void setVlTransporte(Double vlTransporte) {
		this.vlTransporte = vlTransporte;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}
	
	public String getDigitalFuncionario() {
		return digitalFuncionario;
	}

	public void setDigitalFuncionario(String digitalFuncionario) {
		this.digitalFuncionario = digitalFuncionario;
	}
	
	public Integer getIdDedoDigital() {
		return idDedoDigital;
	}

	public void setIdDedoDigital(Integer idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}

	public Boolean getBoAutorizacaoTotem() {
		return boAutorizacaoTotem;
	}

	public void setBoAutorizacaoTotem(Boolean boAutorizacaoTotem) {
		this.boAutorizacaoTotem = boAutorizacaoTotem;
	}

	public String getImeiCelular() {
		return imeiCelular;
	}

	public void setImeiCelular(String imeiCelular) {
		this.imeiCelular = imeiCelular;
	}
	
	public String getAssinaturaDigital() {
		return assinaturaDigital;
	}

	public void setAssinaturaDigital(String assinaturaDigital) {
		this.assinaturaDigital = assinaturaDigital;
	}

	public Timestamp getHrToleranciaPonto() {
		return hrToleranciaPonto;
	}

	public void setHrToleranciaPonto(Timestamp hrToleranciaPonto) {
		this.hrToleranciaPonto = hrToleranciaPonto;
	}
	
	public Long getIdProfissionalRef() {
		return idProfissionalRef;
	}

	public void setIdProfissionalRef(Long idProfissionalRef) {
		this.idProfissionalRef = idProfissionalRef;
	}
	

}