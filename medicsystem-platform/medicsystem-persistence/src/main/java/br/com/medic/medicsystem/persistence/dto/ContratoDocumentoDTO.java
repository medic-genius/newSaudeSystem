package br.com.medic.medicsystem.persistence.dto;

public class ContratoDocumentoDTO {
	
	String nrMatricula; 
	String tipoContrato; // NOVO PLANO, RESGATE OU INCLUSAO
	String estadoCivil;
	String sexo; // "F" ou "M"
	String nmTitular; 
	//String mensalidadeTitular; // MENSALIDADE TITULAR R$
	String ocupacaoTitular; // CARGO
	String rgTitular;
	String cpfTitular;
	String logradouroTitular;
	String nrLogradouro;
	String aptoLogradouro;
	String blocoLogradouro;
	String complementoLogradouro;
	String bairro;
	String cep;
	String nmCidade;
	String uf;
	String telefoneResidencial;
	String telefoneComercial;
	String ramalTelComercial;
	String telefoneCelular;
	String tipoPlano; // INDIVIDUAL ou FAMILIAR
	String grupoConveniado; // ESTADUAL, MUNICIPAL, FEDERAL, EMPRESARIAL ou PARTICULAR
	Boolean titularnaofazuso;
	
	// DEPENDENTES
	String nmDependente1;
	String nmDependente2;
	String nmDependente3;
	String nmDependente4;
	String dtNascimento1; // DATA DE NASCIMENTO DEPENDENTE 1
	String dtNascimento2; // DATA DE NASCIMENTO DEPENDENTE 2
	String dtNascimento3; // DATA DE NASCIMENTO DEPENDENTE 3
	String dtNascimento4; // DATA DE NASCIMENTO DEPENDENTE 4
	String parentescoDept1; // PARENTESCO DEPENDENTE 1
 	String parentescoDept2; // PARENTESCO DEPENDENTE 2
	String parentescoDept3; // PARENTESCO DEPENDENTE 3
	String parentescoDept4; // PARENTESCO DEPENDENTE 4
	//String mensalidadeDept1; // MENSALIDADE DEPENDENTE 1
	//String mensalidadeDept2; // MENSALIDADE DEPENDENTE 2
	//String mensalidadeDept3; // MENSALIDADE DEPENDENTE 3
	//String mensalidadeDept4; // MENSALIDADE DEPENDENTE 4
	
	String nmVendedor;
	String formaPagamento; // CONTRA CHEQUE, DEBITO ou OUTROS
	String diaPagamento; // DATA PAGT DE CADA MES
	String totalPlano; // TOTAL PLANO R$
	String nmBanco;
	String nrAgencia;
	String nrConta;
	
	// MOMENTO DA OPERACAO
	String dia; 
	String mes; 
	String ano;
	
	public Boolean getTitularnaofazuso() {
		return titularnaofazuso;
	}
	public void setTitularnaofazuso(Boolean titularnaofazuso) {
		this.titularnaofazuso = titularnaofazuso;
	}
	
	public String getNrMatricula() {
		return nrMatricula;
	}
	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}
	public String getTipoContrato() {
		return tipoContrato;
	}
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNmTitular() {
		return nmTitular;
	}
	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}
	public String getOcupacaoTitular() {
		return ocupacaoTitular;
	}
	public void setOcupacaoTitular(String ocupacaoTitular) {
		this.ocupacaoTitular = ocupacaoTitular;
	}
	public String getRgTitular() {
		return rgTitular;
	}
	public void setRgTitular(String rgTitular) {
		this.rgTitular = rgTitular;
	}
	public String getCpfTitular() {
		return cpfTitular;
	}
	public void setCpfTitular(String cpfTitular) {
		this.cpfTitular = cpfTitular;
	}
	public String getLogradouroTitular() {
		return logradouroTitular;
	}
	public void setLogradouroTitular(String logradouroTitular) {
		this.logradouroTitular = logradouroTitular;
	}
	public String getNrLogradouro() {
		return nrLogradouro;
	}
	public void setNrLogradouro(String nrLogradouro) {
		this.nrLogradouro = nrLogradouro;
	}
	public String getAptoLogradouro() {
		return aptoLogradouro;
	}
	public void setAptoLogradouro(String aptoLogradouro) {
		this.aptoLogradouro = aptoLogradouro;
	}
	public String getBlocoLogradouro() {
		return blocoLogradouro;
	}
	public void setBlocoLogradouro(String blocoLogradouro) {
		this.blocoLogradouro = blocoLogradouro;
	}
	public String getComplementoLogradouro() {
		return complementoLogradouro;
	}
	public void setComplementoLogradouro(String complementoLogradouro) {
		this.complementoLogradouro = complementoLogradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getNmCidade() {
		return nmCidade;
	}
	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}
	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}
	public String getTelefoneComercial() {
		return telefoneComercial;
	}
	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}
	public String getRamalTelComercial() {
		return ramalTelComercial;
	}
	public void setRamalTelComercial(String ramalTelComercial) {
		this.ramalTelComercial = ramalTelComercial;
	}
	public String getTelefoneCelular() {
		return telefoneCelular;
	}
	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	public String getTipoPlano() {
		return tipoPlano;
	}
	public void setTipoPlano(String tipoPlano) {
		this.tipoPlano = tipoPlano;
	}
	public String getGrupoConveniado() {
		return grupoConveniado;
	}
	public void setGrupoConveniado(String grupoConveniado) {
		this.grupoConveniado = grupoConveniado;
	}
	public String getNmDependente1() {
		return nmDependente1;
	}
	public void setNmDependente1(String nmDependente1) {
		this.nmDependente1 = nmDependente1;
	}
	public String getNmDependente2() {
		return nmDependente2;
	}
	public void setNmDependente2(String nmDependente2) {
		this.nmDependente2 = nmDependente2;
	}
	public String getNmDependente3() {
		return nmDependente3;
	}
	public void setNmDependente3(String nmDependente3) {
		this.nmDependente3 = nmDependente3;
	}
	public String getNmDependente4() {
		return nmDependente4;
	}
	public void setNmDependente4(String nmDependente4) {
		this.nmDependente4 = nmDependente4;
	}
	public String getDtNascimento1() {
		return dtNascimento1;
	}
	public void setDtNascimento1(String dtNascimento1) {
		this.dtNascimento1 = dtNascimento1;
	}
	public String getDtNascimento2() {
		return dtNascimento2;
	}
	public void setDtNascimento2(String dtNascimento2) {
		this.dtNascimento2 = dtNascimento2;
	}
	public String getDtNascimento3() {
		return dtNascimento3;
	}
	public void setDtNascimento3(String dtNascimento3) {
		this.dtNascimento3 = dtNascimento3;
	}
	public String getDtNascimento4() {
		return dtNascimento4;
	}
	public void setDtNascimento4(String dtNascimento4) {
		this.dtNascimento4 = dtNascimento4;
	}
	public String getParentescoDept1() {
		return parentescoDept1;
	}
	public void setParentescoDept1(String parentescoDept1) {
		this.parentescoDept1 = parentescoDept1;
	}
	public String getParentescoDept2() {
		return parentescoDept2;
	}
	public void setParentescoDept2(String parentescoDept2) {
		this.parentescoDept2 = parentescoDept2;
	}
	public String getParentescoDept3() {
		return parentescoDept3;
	}
	public void setParentescoDept3(String parentescoDept3) {
		this.parentescoDept3 = parentescoDept3;
	}
	public String getParentescoDept4() {
		return parentescoDept4;
	}
	public void setParentescoDept4(String parentescoDept4) {
		this.parentescoDept4 = parentescoDept4;
	}
	public String getNmVendedor() {
		return nmVendedor;
	}
	public void setNmVendedor(String nmVendedor) {
		this.nmVendedor = nmVendedor;
	}
	public String getFormaPagamento() {
		return formaPagamento;
	}
	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public String getDiaPagamento() {
		return diaPagamento;
	}
	public void setDiaPagamento(String diaPagamento) {
		this.diaPagamento = diaPagamento;
	}
	public String getTotalPlano() {
		return totalPlano;
	}
	public void setTotalPlano(String totalPlano) {
		this.totalPlano = totalPlano;
	}
	public String getNmBanco() {
		return nmBanco;
	}
	public void setNmBanco(String nmBanco) {
		this.nmBanco = nmBanco;
	}
	public String getNrAgencia() {
		return nrAgencia;
	}
	public void setNrAgencia(String nrAgencia) {
		this.nrAgencia = nrAgencia;
	}
	public String getNrConta() {
		return nrConta;
	}
	public void setNrConta(String nrConta) {
		this.nrConta = nrConta;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	} 
	
	
}
