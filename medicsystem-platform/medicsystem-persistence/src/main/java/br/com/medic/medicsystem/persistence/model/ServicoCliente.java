package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * The persistent class for the tbservicocliente database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbservicocliente")
public class ServicoCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICOCLIENTE_ID_SEQ")
	@SequenceGenerator(name = "SERVICOCLIENTE_ID_SEQ", sequenceName = "realvida.servicocliente_id_seq")
	@Column(name = "idservicocliente")
	private Long id;
	
	@Basic
	@Column(name = "bocoberturaplano")
	private Boolean boCoberturaPlano;
	
	@Basic
	@Column(name = "borealizado")
	private Boolean boRealizado;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "inliberado")
	private Boolean inLiberado;
	
	@Basic
	@Column(name = "qtexame")
	private Integer qtExame;

	// bi-directional many-to-one association to Tbatendimento
	@ManyToOne
	@JoinColumn(name = "idatendimento")
	private Atendimento atendimento;

	// bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	// bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name = "iddependente")
	private Dependente dependente;

	// bi-directional many-to-one association to Tbservico
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	public ServicoCliente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoCoberturaPlano() {
		return boCoberturaPlano;
	}

	public void setBoCoberturaPlano(Boolean boCoberturaPlano) {
		this.boCoberturaPlano = boCoberturaPlano;
	}

	public Boolean getBoRealizado() {
		return boRealizado;
	}

	public void setBoRealizado(Boolean boRealizado) {
		this.boRealizado = boRealizado;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Boolean getInLiberado() {
		return inLiberado;
	}

	public void setInLiberado(Boolean inLiberado) {
		this.inLiberado = inLiberado;
	}

	public Integer getQtExame() {
		return qtExame;
	}

	public void setQtExame(Integer qtExame) {
		this.qtExame = qtExame;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}