package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbguia")
public class Guia implements Serializable {

	private static final long serialVersionUID = 1L; // unknows

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUIA_ID_SEQ")
	@SequenceGenerator(name = "GUIA_ID_SEQ", sequenceName = "realvida.guia_id_seq", allocationSize = 1)
	@Column(name = "idguia")
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "iddependente")
	private Dependente dependente;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtinicio")
	private Date dtInicio;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtvalidade")
	private Date dtValidade;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtexclusao")
	private Timestamp dtExclusao;

	@OneToMany(mappedBy = "guia", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<GuiaServico> guiaServicos;

	public Integer getCanBeInactivated() {
		if(this.dtValidade != null) {
			if(this.getDtValidade().after(new Date()) && null == this.dtExclusao) {
				return 1;
			} else {
				return 0;
			}
		}
		return null;
	}

	public Guia() {
		this.guiaServicos = new ArrayList<GuiaServico>();
	}

    public List<GuiaServico> getGuiaServicos() {
        return guiaServicos;
    }

	public void setGuiaServicos(List<GuiaServico> guiaServicos) {
		this.guiaServicos = guiaServicos;
	}

	public GuiaServico addGuiaServico(GuiaServico guiaServico) {
		guiaServico.setGuia(this);
		getGuiaServicos().add(guiaServico);

		return guiaServico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Date getDtValidade() {
		return dtValidade;
	}

	public void setDtValidade(Date dtValidade) {
		this.dtValidade = dtValidade;
	}

	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Timestamp dtExclusao) {
		this.dtExclusao = dtExclusao;

	}

	@SuppressWarnings("deprecation")
	public Boolean getExpired() {
			Date todayZero = new Date();
			todayZero.setHours(0);
			todayZero.setMinutes(0);
			todayZero.setSeconds(0);
			if(todayZero.getYear() > this.getDtValidade().getYear() ||
	             (todayZero.getYear() == this.getDtValidade().getYear() &&
	            		 todayZero.getMonth() > this.getDtValidade().getMonth()) ||
	             (todayZero.getYear() == this.getDtValidade().getYear() &&
	            		 todayZero.getMonth() == this.getDtValidade().getMonth() &&
	            				 todayZero.getDate() > this.getDtValidade().getDate()))
			{
				return true;
			}

		return false;
	}
}
