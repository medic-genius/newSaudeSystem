package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ImagemCliente;

@Named("imagemcliente-dao")
@ApplicationScoped
public class ImagemClienteDAO extends RelationalDataAccessObject<ImagemCliente> {

	@SuppressWarnings("unchecked")
	public List<ImagemCliente> getImagensPorCliente(Long id) {
	    Query query = entityManager.createQuery("SELECT ic FROM ImagemCliente ic where ic.cliente.id = " + id);
		List<ImagemCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
}
