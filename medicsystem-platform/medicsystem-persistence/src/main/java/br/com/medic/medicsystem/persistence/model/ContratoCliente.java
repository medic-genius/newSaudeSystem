package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(catalog = "realvida", name = "tbcontratocliente")
public class ContratoCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTRATOCLIENTE_ID_SEQ")
	@SequenceGenerator(name = "CONTRATOCLIENTE_ID_SEQ", sequenceName = "realvida.contratocliente_id_seq", allocationSize = 1)
	@Column(name = "idcontratocliente")
	private Long id;

	@Basic
	@Column(name = "boativo")
	private Boolean boAtivo;

	@Basic
	@Column(name = "creditoconveniada")
	private Integer creditoConveniada;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "intipocliente")
	private Integer inTipoCliente;

	@Basic
	@Column(name = "invinculo")
	private Integer inVinculo;

	@Basic
	@Column(name = "nrmatricula")
	private String nrMatricula;

	@Transient
	private Integer credito;

	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;

	@ManyToOne
	@JoinColumn(name = "idorgao")
	private Orgao orgao;

	@ManyToOne
	@JoinColumn(name = "idplano")
	private Plano plano;

	public ContratoCliente() {
		this.credito = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoAtivo() {
		return boAtivo;
	}

	public void setBoAtivo(Boolean boAtivo) {
		this.boAtivo = boAtivo;
	}

	public Integer getCreditoConveniada() {
		return creditoConveniada;
	}

	public void setCreditoConveniada(Integer creditoConveniada) {
		this.creditoConveniada = creditoConveniada;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getInTipoCliente() {
		return inTipoCliente;
	}

	public void setInTipoCliente(Integer inTipoCliente) {
		this.inTipoCliente = inTipoCliente;
	}

	public Integer getInVinculo() {
		return inVinculo;
	}

	public void setInVinculo(Integer inVinculo) {
		this.inVinculo = inVinculo;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public Integer getCredito() {
		return credito;
	}

	public void setCredito(Integer credito) {
		this.credito = credito;
	}

}
