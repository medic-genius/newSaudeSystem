package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(catalog = "financeiro", name = "tbcontabancariafinanceiro")
public class ContaBancaria implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTABANCARIAFINANCEIRO_ID_SEQ")
	@SequenceGenerator(name = "CONTABANCARIAFINANCEIRO_ID_SEQ", sequenceName = "financeiro.contabancariafinanceiro_id_seq", allocationSize = 1)
	@Column(name = "idcontabancaria")
	private Long id;
	 
	@Basic
	@NotNull
	@Column(name = "nmcontabancaria", length=50)
	private String nmContaBancaria;
	
	@Basic
	@NotNull
	@Column(name = "intipocontabancaria", length=50)
	private Short inTipo;  // 0 - Conta Corrente 1 - Poupanca  2 - Dinheiro 
	 	
	@Basic
	@NotNull
	@Column(name = "nrcontabancaria", length=15)
	private String nrContaBancaria;
	
	@Basic
	@NotNull
	@Column(name = "nrdigitoverificador", length=2)
	private String nrDigitoVerificador;
	
//	@JsonIgnore
//	@NotNull
	@ManyToOne
	@JoinColumn(name="idempresafinanceiro")	
	private EmpresaFinanceiro empresaFinanceiro;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idagencia")
	private Agencia agencia;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idbanco")
	private Banco banco;
		
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@Basic
	@Column(name = "nrcodcedente", length=10)
	private String nrCodCedente;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmContaBancaria() {
		return nmContaBancaria;
	}
	
	public void setNmContaBancaria(String nmContaBancaria) {
		this.nmContaBancaria = nmContaBancaria;
	}

	public Short getInTipo() {
		return inTipo;
	}

	public void setInTipo(Short inTipo) {
		this.inTipo = inTipo;
	}

	public String getNrContaBancaria() {
		return nrContaBancaria;
	}

	public void setNrContaBancaria(String nrContaBancaria) {
		this.nrContaBancaria = nrContaBancaria;
	}

	public String getNrDigitoVerificador() {
		return nrDigitoVerificador;
	}

	public void setNrDigitoVerificador(String nrDigitoVerificador) {
		this.nrDigitoVerificador = nrDigitoVerificador;
	}

	public EmpresaFinanceiro getEmpresaFinanceiro() {
		return empresaFinanceiro;
	}

	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getNrCodCedente() {
		return nrCodCedente;
	}

	public void setNrCodCedente(String nrCodCedente) {
		this.nrCodCedente = nrCodCedente;
	}
}
