package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbparcela")
public class Parcela implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARCELA_ID_SEQ")
	@SequenceGenerator(name = "PARCELA_ID_SEQ", sequenceName = "realvida.parcela_id_seq", allocationSize = 1)
	@Column(name = "idparcela")
	private Long id;

	@Basic
	@Column(name = "boautorizadagerente")
	private Boolean boAutorizadaGerente;

	@Basic
	@Column(name = "boexcluida")
	private Boolean boExcluida;

	@Basic
	@Column(name = "bonegociacaoautomatica")
	private Boolean boNegociacaoAutomatica;

	@Basic
	@Column(name = "bonegociada")
	private Boolean boNegociada;

	@Basic
	@Column(name = "boreparcelada")
	private Boolean boReparcelaDa;

	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtcarta1")
	private Date dtCarta1;

	@Basic
	@Column(name = "dtcarta2")
	private Date dtCarta2;

	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;

	@Basic
	@Column(name = "dtnegociacao")
	private Date dtNegociacao;

	@Basic
	@Column(name = "dtpagamento")
	private Date dtPagamento;

	@Basic
	@Column(name = "dtprevisaopagto")
	private Date dtPrevisaoPagto;

	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;

	@Basic
	@Column(name = "formapagamentoefetuada")
	private String formaPagamentoEfetuada;

	@Basic
	@Column(name = "iddespesapai")
	private Integer idDespesaPai;

	@Basic
	@Column(name = "idparcelaaux")
	private Long idParcelaAux;

	@Basic
	@Column(name = "idparcelaodonto")
	private Long idParcelaOdonto;

	@Basic
	@Column(name = "nrparcela")
	private String nrParcela;

	@Basic
	@Column(name = "observacao")
	private String observacao;

	@Basic
	@Column(name = "vljuros")
	private Double vlJuros;

	@Basic
	@Column(name = "vlmulta")
	private Double vlMulta;

	@Basic
	@Column(name = "vlpago")
	private Double vlPago;

	@Basic
	@Column(name = "vlparcela")
	private Double vlParcela;

	@ManyToOne
	@JoinColumn(name = "iddespesa")
	private Despesa despesa;
	
//	@Basic
//	@Column(name = "informapagamento", insertable = false, updatable = false)
//	private Integer informaPagamento;

	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;

	@Transient
	@JsonIgnore
	private String dtVencimentoFormatado;

	@Transient
	@JsonIgnore
	private String vlParcelaFormatado;

	@Transient
	@JsonIgnore
	private String dtPagamentoFormatado;

	@Transient
	@JsonIgnore
	private String dtInclusaoFormatado;

	@Transient
	@JsonIgnore
	private String formaPagamento;
	
	@Basic
	@Column(name = "idcobrancaboleto_sl")
	private Long idCobrancaBoleto_sl;
	
	@Basic
	@Column(name = "linkcobrancaboleto_sl")
	private String linkCobrancaBoleto_sl;

	public Parcela() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoAutorizadaGerente() {
		return boAutorizadaGerente;
	}

	public void setBoAutorizadaGerente(Boolean boAutorizadaGerente) {
		this.boAutorizadaGerente = boAutorizadaGerente;
	}

	public Boolean getBoExcluida() {
		return boExcluida;
	}

	public void setBoExcluida(Boolean boExcluida) {
		this.boExcluida = boExcluida;
	}

	public Boolean getBoNegociacaoAutomatica() {
		return boNegociacaoAutomatica;
	}

	public void setBoNegociacaoAutomatica(Boolean boNegociacaoAutomatica) {
		this.boNegociacaoAutomatica = boNegociacaoAutomatica;
	}

	public Boolean getBoNegociada() {
		return boNegociada;
	}

	public void setBoNegociada(Boolean boNegociada) {
		this.boNegociada = boNegociada;
	}

	public Boolean getBoReparcelaDa() {
		return boReparcelaDa;
	}

	public void setBoReparcelaDa(Boolean boReparcelaDa) {
		this.boReparcelaDa = boReparcelaDa;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtCarta1() {
		return dtCarta1;
	}

	public void setDtCarta1(Date dtCarta1) {
		this.dtCarta1 = dtCarta1;
	}

	public Date getDtCarta2() {
		return dtCarta2;
	}

	public void setDtCarta2(Date dtCarta2) {
		this.dtCarta2 = dtCarta2;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Date getDtNegociacao() {
		return dtNegociacao;
	}

	public void setDtNegociacao(Date dtNegociacao) {
		this.dtNegociacao = dtNegociacao;
	}

	public Date getDtPagamento() {
		return dtPagamento;
	}

	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	public Date getDtPrevisaoPagto() {
		return dtPrevisaoPagto;
	}

	public void setDtPrevisaoPagto(Date dtPrevisaoPagto) {
		this.dtPrevisaoPagto = dtPrevisaoPagto;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public String getFormaPagamentoEfetuada() {
		return formaPagamentoEfetuada;
	}

	public void setFormaPagamentoEfetuada(String formaPagamentoEfetuada) {
		this.formaPagamentoEfetuada = formaPagamentoEfetuada;
	}

	public Integer getIdDespesaPai() {
		return idDespesaPai;
	}

	public void setIdDespesaPai(Integer idDespesaPai) {
		this.idDespesaPai = idDespesaPai;
	}

	public Long getIdParcelaAux() {
		return idParcelaAux;
	}

	public void setIdParcelaAux(Long idParcelaAux) {
		this.idParcelaAux = idParcelaAux;
	}

	public Long getIdParcelaOdonto() {
		return idParcelaOdonto;
	}

	public void setIdParcelaOdonto(Long idParcelaOdonto) {
		this.idParcelaOdonto = idParcelaOdonto;
	}

	public String getNrParcela() {
		return nrParcela;
	}

	public void setNrParcela(String nrParcela) {
		this.nrParcela = nrParcela;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Double getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(Double vlJuros) {
		this.vlJuros = vlJuros;
	}

	public Double getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(Double vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Double getVlPago() {
		return vlPago;
	}

	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}

	public Double getVlParcela() {
		return vlParcela;
	}

	public void setVlParcela(Double vlParcela) {
		this.vlParcela = vlParcela;
	}

	@JsonIgnore
	public Despesa getDespesa() {
		return despesa;
	}

	@JsonProperty
	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	@JsonProperty
	public String getFormaPagamento() {
		switch (this.getInFormaPagamento()) {

		case 0:
			return FormaPagamentoEnum.CONTRA_CHEQUE_LOWER.getDescription();
		case 1:
			return FormaPagamentoEnum.DEBITO_AUTOMATICO_LOWER.getDescription();
		case 2:
			return FormaPagamentoEnum.DINHEIRO_LOWER.getDescription();
		case 3:
			return FormaPagamentoEnum.CHEQUE_A_VISTA_LOWER.getDescription();
		case 4:
			return FormaPagamentoEnum.CARTAO_DE_CREDITO_LOWER.getDescription();
		case 5:
			return FormaPagamentoEnum.BOLETO_BANCARIO_LOWER.getDescription();
		case 6:
			return FormaPagamentoEnum.CHEQUE_PRE_DATADO_LOWER.getDescription();
		case 7:
			return FormaPagamentoEnum.GRATUITO_LOWER.getDescription();
		case 8:
			return FormaPagamentoEnum.CREDITO_PESSOAL_LOWER.getDescription();
		case 9:
			return FormaPagamentoEnum.CARTAO_DE_DEBITO_LOWER.getDescription();
		case 10:
			return FormaPagamentoEnum.COBERTURA_DO_PLANO_LOWER.getDescription();
		case 11:
			return FormaPagamentoEnum.CARNE_LOWER.getDescription();
		case 12:
			return FormaPagamentoEnum.DESCONTO_LOWER.getDescription();
		case 13:
			return FormaPagamentoEnum.DEPOSITO_EM_CONTA_LOWER.getDescription();
		case 14:
			return FormaPagamentoEnum.BAIXA_NO_SISTEMA_LOWER.getDescription();
		case 15:
			return FormaPagamentoEnum.CONVENIO_LOWER.getDescription();
		case 16:
			return FormaPagamentoEnum.RETORNO_LOWER.getDescription();
		case 17:
			return FormaPagamentoEnum.FOLHA_DE_PAGAMENTO_LOWER.getDescription();
		default:
			break;
		}

		return "";

	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	@JsonProperty
	public String getDtVencimentoFormatado() {
		if (getDtVencimento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtVencimento());
		}
		return null;
	}

	@JsonProperty
	public String getDtInclusaoFormatado() {
		if (getDtInclusao() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtInclusao());
		}
		return null;
	}

	@JsonProperty
	public String getDtPagamentoFormatado() {
		if (getDtPagamento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtPagamento());
		}
		return null;
	}

	@JsonProperty
	public String getVlParcelaFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVlParcela() != null)
			return currencyFormatter.format(getVlParcela());
		return null;
	}

	public Long getIdCobrancaBoleto_sl() {
		return idCobrancaBoleto_sl;
	}

	public void setIdCobrancaBoleto_sl(Long idCobrancaBoleto_sl) {
		this.idCobrancaBoleto_sl = idCobrancaBoleto_sl;
	}

	public String getLinkCobrancaBoleto_sl() {
		return linkCobrancaBoleto_sl;
	}

	public void setLinkCobrancaBoleto_sl(String linkCobrancaBoleto_sl) {
		this.linkCobrancaBoleto_sl = linkCobrancaBoleto_sl;
	}
	
	@JsonProperty
	@Transient
	public Long getIdDespesa(){
		if(despesa != null && despesa.getId() != null)
			return despesa.getId();
		
		return null;
	}
	
	public String getNmCliente(){
		return despesa.getContratoCliente().getCliente().getNmCliente();
	}
	
	public String getNmEndereco(){
		Cliente cliente =  despesa.getContratoCliente().getCliente();
		return cliente.getNmLogradouro()+", "+cliente.getNrNumero() +" - "+cliente.getBairro().getNmBairro();
	}
	
	public String getNmEnderecoComplemento(){
		Cliente cliente =  despesa.getContratoCliente().getCliente();
		return cliente.getNrCEP() + " - "+cliente.getCidade().getNmCidade()+"-"+cliente.getCidade().getNmUF();
	}
}