package br.com.medic.medicsystem.persistence.appmobile.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UsuarioRegisterPK.class)
@Table(schema="app_mobile", name="tbusuario_register")
public class UsuarioRegister {
	
	@Id
	@Column(name="id_usuario")
	private Long idUsuario;
	
	@Id
	@Column(name="tipo_usuario")
	private Integer tipoUsuario;
	
	@Basic
	@Column(name="login")
	private String login;
	
	@Basic
	@Column(name = "kc_userid")
	private String kcUserId;
	
	@Basic
	@Column(name="dtexclusao")
	private Date dtExclusao;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getKcUserId() {
		return kcUserId;
	}

	public void setKcUserId(String kcUserId) {
		this.kcUserId = kcUserId;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
}
