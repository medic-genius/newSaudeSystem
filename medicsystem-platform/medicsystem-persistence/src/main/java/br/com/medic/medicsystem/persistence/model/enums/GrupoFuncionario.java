package br.com.medic.medicsystem.persistence.model.enums;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GrupoFuncionario {

	FUNCIONARIO(0, "FUNCIONÁRIOS"), MEDICO_DENTISTAS(1, "MEDICOS/DENTISTAS"), PRESTADORES(2, "PRESTADORES DE SERVIÇOS"), 
	VENDEDORES(3, "VENDEDORES"), ESTAGIARIOS(4, "ESTAGIÁRIOS"), FUNCGOSAT(5,"FUNCIONÁRIOS GOSAT");
	
	private Integer id;
	private String description;

	private GrupoFuncionario(Integer id, String description){
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static List<GrupoFuncionario> getGruposFunc(){
		List<GrupoFuncionario> grupoList = new ArrayList<GrupoFuncionario>();
		for (GrupoFuncionario grupoFuncionario : GrupoFuncionario.values()) {
			grupoList.add(grupoFuncionario);
		}
		return grupoList;
	}
}
