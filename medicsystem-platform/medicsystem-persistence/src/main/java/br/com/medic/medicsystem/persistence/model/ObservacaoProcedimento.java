package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbobservacaoprocedimento database table.
 * 
 */
@Entity
public class ObservacaoProcedimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBOBSERVACAOPROCEDIMENTO_IDOBSERVACAOPROCEDIMENTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBOBSERVACAOPROCEDIMENTO_IDOBSERVACAOPROCEDIMENTO_GENERATOR")
	private Long idobservacaoprocedimento;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private Timestamp dtobservacao;

	private String nmobservacao;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	//bi-directional many-to-one association to Tbprocedimento
	@ManyToOne
	@JoinColumn(name="idprocedimento")
	private Procedimento procedimento;

	public ObservacaoProcedimento() {
	}

	public Long getIdobservacaoprocedimento() {
		return this.idobservacaoprocedimento;
	}

	public void setIdobservacaoprocedimento(Long idobservacaoprocedimento) {
		this.idobservacaoprocedimento = idobservacaoprocedimento;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Timestamp getDtobservacao() {
		return this.dtobservacao;
	}

	public void setDtobservacao(Timestamp dtobservacao) {
		this.dtobservacao = dtobservacao;
	}

	public String getNmobservacao() {
		return this.nmobservacao;
	}

	public void setNmobservacao(String nmobservacao) {
		this.nmobservacao = nmobservacao;
	}

	public Funcionario getTbfuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario tbfuncionario) {
		this.funcionario = tbfuncionario;
	}

	public Procedimento getTbprocedimento() {
		return this.procedimento;
	}

	public void setTbprocedimento(Procedimento tbprocedimento) {
		this.procedimento = tbprocedimento;
	}

}