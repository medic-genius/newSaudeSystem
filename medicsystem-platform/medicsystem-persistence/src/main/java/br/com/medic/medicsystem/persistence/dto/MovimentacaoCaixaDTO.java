package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.views.ConferenciaMensalidade;
import br.com.medic.medicsystem.persistence.model.views.ConferenciaParcela;
import br.com.medic.medicsystem.persistence.model.views.MovimentacaoCaixaComportilhadoView;

public class MovimentacaoCaixaDTO {
	
	private List<ConferenciaMensalidade> conferenciaMensalidadeList;
	
	private List<ConferenciaParcela> conferenciaParcelaList;
	
	private List<MovimentacaoCaixaComportilhadoView> movimentacaoCaixaList;
	
	private Float vlAbertura ;
	
	private Float vlAbastecimento ;
	
	private Float vlRetirada ;
	
	private Float vlDevolucao ;
	
	private Float vlFechamento ;
	
	private Float totalEspecie ;
	
	private Float totalDebitoAutomatico ;
	
	private Float totalDinheiro ;
	
	private Float totalCheque ;
	
	private Float totalCartaoCredito ;
	
	private Float totalCartaoDebito ;
	
	private Float totalBaixa ;
	
	private Float totalConvenio ;
	
	private Float totalPagamentos ;
	
	private Float total ;
	
	private Float totalCreditoVisa ;
	
	private Float totalCreditoMasterCard ;
	
	private Float totalCreditoAmericanExpress ;
	
	private Float totalCreditoElo ;
	
	private Float totalCreditoDiners ;
	
	private Float totalCreditoMaestro ;
	
	private Float totalCreditoRedShop ;
	
	private Float totalDebitoVisa ;
	
	private Float totalDebitoMasterCard ;
	
	private Float totalDebitoElo ;
	
	private Float totalDebitoRedShop ;
	
	
	public MovimentacaoCaixaDTO( ) {
		
	}
	
	
	public List<ConferenciaMensalidade> getConferenciaMensalidadeList() {
		return conferenciaMensalidadeList;
	}

	public void setConferenciaMensalidadeList(List<ConferenciaMensalidade> conferenciaMensalidadeList) {
		this.conferenciaMensalidadeList = conferenciaMensalidadeList;
	}

	public List<ConferenciaParcela> getConferenciaParcelaList() {
		return conferenciaParcelaList;
	}

	public void setConferenciaParcelaList(List<ConferenciaParcela> conferenciaParcelaList) {
		this.conferenciaParcelaList = conferenciaParcelaList;
	}


	public Float getVlAbertura() {
		return vlAbertura;
	}


	public void setVlAbertura(Float vlAbertura) {
		this.vlAbertura = vlAbertura;
	}


	public Float getVlAbastecimento() {
		return vlAbastecimento;
	}


	public void setVlAbastecimento(Float vlAbastecimento) {
		this.vlAbastecimento = vlAbastecimento;
	}


	public Float getVlRetirada() {
		return vlRetirada;
	}


	public void setVlRetirada(Float vlRetirada) {
		this.vlRetirada = vlRetirada;
	}


	public Float getVlFechamento() {
		return vlFechamento;
	}


	public void setVlFechamento(Float vlFechamento) {
		this.vlFechamento = vlFechamento;
	}


	public Float getTotalEspecie() {
		return totalEspecie;
	}


	public void setTotalEspecie(Float totalEspecie) {
		this.totalEspecie = totalEspecie;
	}


	public Float getTotalDebitoAutomatico() {
		return totalDebitoAutomatico;
	}


	public void setTotalDebitoAutomatico(Float totalDebitoAutomatico) {
		this.totalDebitoAutomatico = totalDebitoAutomatico;
	}


	public Float getTotalDinheiro() {
		return totalDinheiro;
	}


	public void setTotalDinheiro(Float totalDinheiro) {
		this.totalDinheiro = totalDinheiro;
	}


	public Float getTotalCartaoCredito() {
		return totalCartaoCredito;
	}


	public void setTotalCartaoCredito(Float totalCartaoCredito) {
		this.totalCartaoCredito = totalCartaoCredito;
	}


	public Float getTotalCartaoDebito() {
		return totalCartaoDebito;
	}


	public void setTotalCartaoDebito(Float totalCartaoDebito) {
		this.totalCartaoDebito = totalCartaoDebito;
	}


	public Float getTotalBaixa() {
		return totalBaixa;
	}


	public void setTotalBaixa(Float totalBaixa) {
		this.totalBaixa = totalBaixa;
	}


	public Float getTotalConvenio() {
		return totalConvenio;
	}


	public void setTotalConvenio(Float totalConvenio) {
		this.totalConvenio = totalConvenio;
	}


	public Float getTotalPagamentos() {
		return totalPagamentos;
	}


	public void setTotalPagamentos(Float totalPagamentos) {
		this.totalPagamentos = totalPagamentos;
	}


	public Float getTotal() {
		return total;
	}


	public void setTotal(Float total) {
		this.total = total;
	}


	public Float getTotalCreditoVisa() {
		return totalCreditoVisa;
	}


	public void setTotalCreditoVisa(Float totalCreditoVisa) {
		this.totalCreditoVisa = totalCreditoVisa;
	}


	public Float getTotalCreditoMasterCard() {
		return totalCreditoMasterCard;
	}


	public void setTotalCreditoMasterCard(Float totalCreditoMasterCard) {
		this.totalCreditoMasterCard = totalCreditoMasterCard;
	}


	public Float getTotalCreditoAmericanExpress() {
		return totalCreditoAmericanExpress;
	}


	public void setTotalCreditoAmericanExpress(Float totalCreditoAmericanExpress) {
		this.totalCreditoAmericanExpress = totalCreditoAmericanExpress;
	}


	public Float getTotalCreditoElo() {
		return totalCreditoElo;
	}


	public void setTotalCreditoElo(Float totalCreditoElo) {
		this.totalCreditoElo = totalCreditoElo;
	}


	public Float getTotalCreditoDiners() {
		return totalCreditoDiners;
	}


	public void setTotalCreditoDiners(Float totalCreditoDiners) {
		this.totalCreditoDiners = totalCreditoDiners;
	}


	public Float getTotalCreditoMaestro() {
		return totalCreditoMaestro;
	}


	public void setTotalCreditoMaestro(Float totalCreditoMaestro) {
		this.totalCreditoMaestro = totalCreditoMaestro;
	}


	public Float getTotalCreditoRedShop() {
		return totalCreditoRedShop;
	}


	public void setTotalCreditoRedShop(Float totalCreditoRedShop) {
		this.totalCreditoRedShop = totalCreditoRedShop;
	}


	public Float getTotalDebitoVisa() {
		return totalDebitoVisa;
	}


	public void setTotalDebitoVisa(Float totalDebitoVisa) {
		this.totalDebitoVisa = totalDebitoVisa;
	}


	public Float getTotalDebitoMasterCard() {
		return totalDebitoMasterCard;
	}


	public void setTotalDebitoMasterCard(Float totalDebitoMasterCard) {
		this.totalDebitoMasterCard = totalDebitoMasterCard;
	}


	public Float getTotalDebitoElo() {
		return totalDebitoElo;
	}


	public void setTotalDebitoElo(Float totalDebitoElo) {
		this.totalDebitoElo = totalDebitoElo;
	}


	public Float getTotalDebitoRedShop() {
		return totalDebitoRedShop;
	}


	public void setTotalDebitoRedShop(Float totalDebitoRedShop) {
		this.totalDebitoRedShop = totalDebitoRedShop;
	}
	
	public List<MovimentacaoCaixaComportilhadoView> getMovimentacaoCaixaList() {
		return movimentacaoCaixaList;
	}
	
	public void setMovimentacaoCaixaList(List<MovimentacaoCaixaComportilhadoView> movimentacaoCaixaList) {
		this.movimentacaoCaixaList = movimentacaoCaixaList;
	}
	
	public Float getTotalCheque() {
		return totalCheque;
	}
	
	public void setTotalCheque(Float totalCheque) {
		this.totalCheque = totalCheque;
	}


	public Float getVlDevolucao() {
		return vlDevolucao;
	}


	public void setVlDevolucao(Float vlDevolucao) {
		this.vlDevolucao = vlDevolucao;
	}
	
	

}
