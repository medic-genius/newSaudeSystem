package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the tbsalariomd database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbsalariomd")
public class SalarioMd implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SALARIOMD_ID_SEQ")	
	@SequenceGenerator(name = "SALARIOMD_ID_SEQ", catalog = "realvida", sequenceName = "salariomd_id_seq", allocationSize = 1)
	@Column(name = "idsalariomd")
	private Long id;

	@Basic
	@Column(name="mes")
	private Integer mes;
	
	@Basic
	@Column(name="ano")
	private Integer ano;
	
	@Basic
	@Column(name="vlsalario")
	private Double vlSalario;
	
	@Basic
	@Column(name="intipoprofissional")
	private Integer inTipoProfissional;

	@Basic
	@Column(name="dtcadastro")
	@Temporal(TemporalType.DATE)
	private Date dtcadastro;

	@Basic
	@Column(name="dtexclusao")
	@Temporal(TemporalType.DATE)
	private Date dtExclusao;

	@Basic
	@Column(name="idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name="idoperadorexclusao")
	private Long idOperadorExclusao;	

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;
	
	@Basic
	@Column(name="idunidade")
	private Long idUnidade;
	
	@Basic
	@Column(name="boresponsaveltecnico")
	private Boolean BoResponsavelTecnico;

	public SalarioMd() {
	}

	
	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Date getDtcadastro() {
		return this.dtcadastro;
	}

	public void setDtcadastro(Date dtcadastro) {
		this.dtcadastro = dtcadastro;
	}

	public Integer getMes() {
		return this.mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Funcionario getTbfuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario tbfuncionario) {
		this.funcionario = tbfuncionario;
	}


	public Long getIdUnidade() {
		return idUnidade;
	}


	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}


	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}


	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}


	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}


	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}


	public Date getDtExclusao() {
		return dtExclusao;
	}


	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}


	public Integer getInTipoProfissional() {
		return inTipoProfissional;
	}


	public void setInTipoProfissional(Integer inTipoProfissional) {
		this.inTipoProfissional = inTipoProfissional;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Double getVlSalario() {
		return vlSalario;
	}


	public void setVlSalario(Double vlSalario) {
		this.vlSalario = vlSalario;
	}


	public Boolean getBoResponsavelTecnico() {
		return BoResponsavelTecnico;
	}


	public void setBoResponsavelTecnico(Boolean boResponsavelTecnico) {
		BoResponsavelTecnico = boResponsavelTecnico;
	}

}