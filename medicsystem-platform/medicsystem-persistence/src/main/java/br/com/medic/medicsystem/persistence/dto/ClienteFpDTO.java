package br.com.medic.medicsystem.persistence.dto;

public class ClienteFpDTO {
	
	private String nrMatricula;
	
	private String nmCliente;

	private String nmOrgao;
	
	private String nrCpf;
	
	private String nmUF;
	
	private String dtNacimento;
	
	private String nmEndereco;
	
	private String nrCep;
	
	private String nrFone;
	
	private String nmBanco;
	
	private String nrAgencia;
	
	private String nrDigAgencia;
	
	private String nrConta;
	
	private String nrDigConta;
	
	private String nmCargo;
	

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmOrgao() {
		return nmOrgao;
	}

	public void setNmOrgao(String nmOrgao) {
		this.nmOrgao = nmOrgao;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNmUF() {
		return nmUF;
	}

	public void setNmUF(String nmUF) {
		this.nmUF = nmUF;
	}

	public String getDtNacimento() {
		return dtNacimento;
	}

	public void setDtNacimento(String dtNacimento) {
		this.dtNacimento = dtNacimento;
	}

	public String getNmEndereco() {
		return nmEndereco;
	}

	public void setNmEndereco(String nmEndereco) {
		this.nmEndereco = nmEndereco;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrFone() {
		return nrFone;
	}

	public void setNrFone(String nrFone) {
		this.nrFone = nrFone;
	}

	public String getNmBanco() {
		return nmBanco;
	}

	public void setNmBanco(String nmBanco) {
		this.nmBanco = nmBanco;
	}

	public String getNrAgencia() {
		return nrAgencia;
	}

	public void setNrAgencia(String nrAgencia) {
		this.nrAgencia = nrAgencia;
	}

	public String getNrDigAgencia() {
		return nrDigAgencia;
	}

	public void setNrDigAgencia(String nrDigAgencia) {
		this.nrDigAgencia = nrDigAgencia;
	}

	public String getNrConta() {
		return nrConta;
	}

	public void setNrConta(String nrConta) {
		this.nrConta = nrConta;
	}

	public String getNrDigConta() {
		return nrDigConta;
	}

	public void setNrDigConta(String nrDigConta) {
		this.nrDigConta = nrDigConta;
	}

	public String getNmCargo() {
		return nmCargo;
	}

	public void setNmCargo(String nmCargo) {
		this.nmCargo = nmCargo;
	}
	
}
