package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tbcobranca", catalog = "realvida")
public class Cobranca implements Serializable{

	private static final long serialVersionUID = -6397291240477714876L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_COBRANCA_SEQ")
	@SequenceGenerator(name = "ID_COBRANCA_SEQ", sequenceName = "realvida.cobranca_id_seq", allocationSize = 1)
	@Column(name = "idcobranca")
	private Long id;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "idsac")
	private Sac sac;
	
	@Column(name = "descricao")
	private String descricao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "prevpagamento")
	private Date previsaoPagamento;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "proxligacao")
	private Date proximaLigacao;

	@Column(name = "instatus")
	private Integer inStatus;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sac getSac() {
		return sac;
	}

	public void setSac(Sac sac) {
		this.sac = sac;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getPrevisaoPagamento() {
		return previsaoPagamento;
	}

	public void setPrevisaoPagamento(Date previsaoPagamento) {
		this.previsaoPagamento = previsaoPagamento;
	}

	public Date getProximaLigacao() {
		return proximaLigacao;
	}

	public void setProximaLigacao(Date proximaLigacao) {
		this.proximaLigacao = proximaLigacao;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}
	
}
