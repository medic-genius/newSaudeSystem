package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;



@Entity
@Table(catalog = "realvida", name = "tbpreanamnesepaciente")
public class PreAnamnesePaciente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREANANMESEPACIENTE_ID_SEQ")
	@SequenceGenerator(name = "PREANANMESEPACIENTE_ID_SEQ", sequenceName = "realvida.preanamnesepaciente_id_seq", allocationSize=1)
	@Column(name = "idpreanamnesepaciente")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "inresposta")
	private Integer inResposta;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	//bi-directional many-to-one association to Tbpreanamnese
	@ManyToOne
	@JoinColumn(name="idpreanamnese")
	private PreAnamnese preAnamnese;

	public PreAnamnesePaciente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getInResposta() {
		return inResposta;
	}

	public void setInResposta(Integer inResposta) {
		this.inResposta = inResposta;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public PreAnamnese getPreAnamnese() {
		return preAnamnese;
	}

	public void setPreAnamnese(PreAnamnese preAnamnese) {
		this.preAnamnese = preAnamnese;
	}

}