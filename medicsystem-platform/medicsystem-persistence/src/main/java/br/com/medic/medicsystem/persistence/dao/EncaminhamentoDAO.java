package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Encaminhamento;

@Named("encaminhamento-dao")
@ApplicationScoped
public class EncaminhamentoDAO extends
        RelationalDataAccessObject<Encaminhamento> {
	
	public String geraCodigoEncaminhamento() {
		return geraCodigoEncaminhamento(1);
	}

	public String getNovoCodigoEncaminhamento() {
		String queryStr = "select coalesce(nrencaminhamento, '0') as nrencaminhamento from realvida.tbencaminhamento order by idencaminhamento desc limit 1";

		try {
			
			Query q = findByNativeQuery(queryStr);
			String result = (String) q.getSingleResult();

			if (result == null) {
				throw new ObjectNotFoundException(
				        "Um novo codigo de encaminhamento nao pode ser gerado");
			}
			
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return "0";
		}
		

		
	}
	
	public String geraCodigoEncaminhamento(Integer inc) {
		String codigoRecuperado = null;

		try {
			//codigoRecuperado = getObject(Encaminhamento.class, String.class,
			//        "nrEncaminhamento", "desc");
			codigoRecuperado = getNovoCodigoEncaminhamento();
		} catch (ObjectNotFoundException e) {
			codigoRecuperado = "000001";
		}

		Integer novoSeq = Integer.parseInt(codigoRecuperado) + inc;
		String nrNovoSeq = String.valueOf(novoSeq);

		while (nrNovoSeq.trim().length() < 12) {
			nrNovoSeq = "0".concat(nrNovoSeq);
		}

		return nrNovoSeq;
	}
	
	public Encaminhamento save(Encaminhamento encaminhamento){
		
		return persist(encaminhamento);
	}
	

}
