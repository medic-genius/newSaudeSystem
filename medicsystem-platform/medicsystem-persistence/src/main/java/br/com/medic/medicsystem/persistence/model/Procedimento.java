package br.com.medic.medicsystem.persistence.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the tbprocedimento database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbprocedimento")
public class Procedimento {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROCEDIMENTO_ID_SEQ")
	@SequenceGenerator(name = "PROCEDIMENTO_ID_SEQ", sequenceName = "realvida.procedimento_id_seq", allocationSize = 1)
	@Column(name = "idprocedimento")
	private Long id;

	@Basic
	@Column(name = "boautorizado")
	private Boolean boAutorizado;

	@Basic
	@Column(name = "bocobertura")
	private Boolean boCobertura;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtconclusao")
	@Temporal(TemporalType.DATE)
	private Date dtConclusao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idorcamento")
	private Long idOrcamento;

	@Basic
	@Column(name = "idprocedimentoaux")
	private Long idProcedimentoAux;

	@Basic
	@Column(name = "idprocedimentoodonto")
	private Long idProcedimentoOdonto;

	@Basic
	@Column(name = "infacedente")
	private Integer inFaceDente;

	@Basic
	@Column(name = "insituacao")
	private Integer inSituacao;

	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "nmfuncionarioexecutor")
	private String nmFuncionarioExecutor;

	@Basic
	@Column(name = "nmfuncionariosolicitante")
	private String nmFuncionarioSolicitante;

	@Basic
	@Column(name = "qtprocedimento")
	private Integer qtProcedimento;

	@Basic
	@Column(name = "vlprocedimento")
	private Double vlProcedimento;

	@Basic
	@Column(name = "idsolicitante")
	private Long idsolicitante;

	@Basic
	@Column(name = "idexecutor")
	private Long idexecutor;

	// bi-directional many-to-one association to Tbservico
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	public Procedimento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoAutorizado() {
		return boAutorizado;
	}

	public void setBoAutorizado(Boolean boAutorizado) {
		this.boAutorizado = boAutorizado;
	}

	public Boolean getBoCobertura() {
		return boCobertura;
	}

	public void setBoCobertura(Boolean boCobertura) {
		this.boCobertura = boCobertura;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtConclusao() {
		return dtConclusao;
	}

	public void setDtConclusao(Date dtConclusao) {
		this.dtConclusao = dtConclusao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdOrcamento() {
		return idOrcamento;
	}

	public void setIdOrcamento(Long idOrcamento) {
		this.idOrcamento = idOrcamento;
	}

	public Long getIdProcedimentoAux() {
		return idProcedimentoAux;
	}

	public void setIdProcedimentoAux(Long idProcedimentoAux) {
		this.idProcedimentoAux = idProcedimentoAux;
	}

	public Long getIdProcedimentoOdonto() {
		return idProcedimentoOdonto;
	}

	public void setIdProcedimentoOdonto(Long idProcedimentoOdonto) {
		this.idProcedimentoOdonto = idProcedimentoOdonto;
	}

	public Integer getInFaceDente() {
		return inFaceDente;
	}

	public void setInFaceDente(Integer inFaceDente) {
		this.inFaceDente = inFaceDente;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public String getNmFuncionarioExecutor() {
		return nmFuncionarioExecutor;
	}

	public void setNmFuncionarioExecutor(String nmFuncionarioExecutor) {
		this.nmFuncionarioExecutor = nmFuncionarioExecutor;
	}

	public String getNmFuncionarioSolicitante() {
		return nmFuncionarioSolicitante;
	}

	public void setNmFuncionarioSolicitante(String nmFuncionarioSolicitante) {
		this.nmFuncionarioSolicitante = nmFuncionarioSolicitante;
	}

	public Integer getQtProcedimento() {
		return qtProcedimento;
	}

	public void setQtProcedimento(Integer qtProcedimento) {
		this.qtProcedimento = qtProcedimento;
	}

	public Double getVlProcedimento() {
		return vlProcedimento;
	}

	public void setVlProcedimento(Double vlProcedimento) {
		this.vlProcedimento = vlProcedimento;
	}

	public Long getIdsolicitante() {
		return idsolicitante;
	}

	public void setIdsolicitante(Long idsolicitante) {
		this.idsolicitante = idsolicitante;
	}

	public Long getIdexecutor() {
		return idexecutor;
	}

	public void setIdexecutor(Long idexecutor) {
		this.idexecutor = idexecutor;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}