package br.com.medic.medicsystem.persistence.security.dto;

public class LoginDTO {

	private String username;

	private String password;
	
	private String perfilNecessario;

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPerfilNecessario() {
	    return perfilNecessario;
    }
	
	public void setPerfilNecessario(String perfilNecessario) {
	    this.perfilNecessario = perfilNecessario;
    }

}
