package br.com.medic.medicsystem.persistence.dataaccess.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

import br.com.medic.medicsystem.persistence.dataaccess.IDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectAlreadyExistsException;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.security.Log;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class RelationalDataAccessObject<T> implements IDataAccessObject<T> {

    @PersistenceContext(unitName = "saudesystem")
    protected EntityManager entityManager;

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Collection<?> findByQuery(String query) {
        Collection<?> result = entityManager.createQuery(query).getResultList();

        if (result.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return result;
    }

    public Collection<T> findByTypedQuery(String query, Class<T> clazz) {
        Collection<T> result = entityManager.createQuery(query, clazz)
                .getResultList();

        if (result.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return result;
    }

    @Override
    public Collection<T> findByTypedQueryWithParams(String query,
            Class<T> clazz, Object... parameters) {

        TypedQuery<T> sql = entityManager.createQuery(query, clazz);

        for (int i = 0; i < parameters.length; i++) {
            sql.setParameter(i + 1, parameters[i]);
        }

        Collection<T> result = sql.getResultList();

        if (result.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> Collection<E> findByNativeQuery(String query, Class<E> clazz) {
        Collection<E> result = entityManager.createNativeQuery(query, clazz)
                .getResultList();

        if (result.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return result;
    }

    @Override
    public Query findByNativeQuery(String query) {
        return entityManager.createNativeQuery(query);

    }

    @Override
    public Query findByNamedNativeQuery(String nnqName) {
        return entityManager.createNamedQuery(nnqName);

    }

    @Override
    public <E> Query findByNamedQuery(String nnqName, Class<E> clazz) {
        return entityManager.createNamedQuery(nnqName, clazz);
    }

    @Override
    public Object loadByQuery(String query) {
        Object result;

        try {
            Query q = entityManager.createQuery(query);
            q.setMaxResults(1);
            result = q.getSingleResult();
        } catch (NoResultException nre) {
            throw new ObjectNotFoundException("Load by Query failed", nre);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object loadByQuery(String query, Object... parameters) {

        Query sql = entityManager.createQuery(query);

        for (int i = 0; i < parameters.length; i++) {
            sql.setParameter(i + 1, parameters[i]);
        }

        // getSingleResult() uses pagination and can generate memory problems
        // when the query uses join fetch
        List<Object> results = sql.getResultList();

        if (results == null || results.isEmpty()) {
            throw new ObjectNotFoundException("Entidade nao encontrada.");
        }

        return results.get(0);
    }

    @Override
    public Collection<?> findByQuery(String query, Object... parameters) {

        Query sql = entityManager.createQuery(query);

        for (int i = 0; i < parameters.length; i++) {
            sql.setParameter(i + 1, parameters[i]);
        }

        Collection<?> result = sql.getResultList();

        if (result.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return result;
    }

    @Override
    public Collection<?> findByQuery(String query,
            Map<String, ? extends Object> map, Integer firstResult,
            Integer maxResults) {
        Query result = entityManager.createQuery(query);

        if (map != null) {
            Set<? extends Map.Entry<String, ? extends Object>> set = map
                    .entrySet();
            for (Map.Entry<?, ?> me : set) {
                result.setParameter((String) me.getKey(), me.getValue());
            }
        }
        if (firstResult != null) {
            result.setFirstResult(firstResult);
        }
        if (maxResults != null) {
            result.setMaxResults(maxResults);
        }

        Collection<?> resultList = result.getResultList();

        if (resultList.isEmpty()) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
        }

        return resultList;
    }

    @Log
    @Override
    public void remove(T bean) {
        entityManager.remove(entityManager.merge(bean));
    }

    @Log
    @Override
    public T persist(T bean) {
        try {
            entityManager.persist(bean);
        } catch (PersistenceException eee) {
            if (eee.getCause() instanceof ConstraintViolationException) {
                throw new ObjectAlreadyExistsException(
                        "O objeto ja existe no sistema.", eee);
            } else {
                throw eee;
            }
        }

        return bean;
    }

    @Log
    @Override
    public T update(T bean) {
        return entityManager.merge(bean);
    }

    public <E> E searchByKey(Class<E> clazz, Object object) {
        E result = entityManager.find(clazz, object);

        if (result == null) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE
                    + clazz.getCanonicalName() + " with id " + object);
        }

        return result;
    }

    public <R> R getObject(Class<T> clazz, Class<R> returnType, String field,
            String order) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<R> criteria = builder.createQuery(returnType);

        Root<T> root = criteria.from(clazz);
        criteria.select(root.get(field));

        TypedQuery<R> q = entityManager.createQuery(criteria);

        q.setFirstResult(0);
        q.setMaxResults(1);

        R result = q.getSingleResult();

        if (order != null && order.equals("asc")) {
            criteria.orderBy(builder.asc(root.get(field)));
        } else if (order != null && order.equals("desc")) {
            criteria.orderBy(builder.desc(root.get(field)));
        }

        if (result == null) {
            throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
        }

        return result;
    }

    @Override

    public TypedQuery<?> createCriteriaQuery(CriteriaQuery<?> criteriaQuery) {
        return entityManager.createQuery(criteriaQuery);
    }

    @Override
    public TypedQuery<T> createQuery(String query, Class<T> clazz) {
        return entityManager.createQuery(query, clazz);
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return entityManager.getCriteriaBuilder();
    }
}