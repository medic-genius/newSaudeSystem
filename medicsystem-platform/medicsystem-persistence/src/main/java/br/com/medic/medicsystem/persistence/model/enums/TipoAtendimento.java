package br.com.medic.medicsystem.persistence.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum TipoAtendimento {
	
	PARTICULAR(0, "Particular"), HORISTA(1, "Horista"), PRODUCAO(2, "Producao"), FIXO(3, "Fixo");
	
	private Integer id;
	private String description;
	
	private TipoAtendimento(int ordinal, String name) {
		this.id = ordinal;
		this.description = name;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
