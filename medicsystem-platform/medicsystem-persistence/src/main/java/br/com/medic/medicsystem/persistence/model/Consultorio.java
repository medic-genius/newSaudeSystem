package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tbconsultorio")
public class Consultorio implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONSULTORIO_ID_SEQ")
	@SequenceGenerator(name = "CONSULTORIO_ID_SEQ", sequenceName = "realvida.consultorio_id_seq")
	@Column(name = "idconsultorio")
	private Long id;
	
	@Basic
	@Column(name = "nmconsultorio")
	private String nmConsultorio;
	
	
	@Basic
	@Column(name = "localizacao")
	private String localizacao;
	
	
	@ManyToOne
	@JoinColumn(name="idunidade")
	private Unidade unidade;
	
	@Basic
	@Column(name="dtexclusao")
	private Date dtExclusao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmConsultorio() {
		return nmConsultorio;
	}

	public void setNmConsultorio(String nmConsultorio) {
		this.nmConsultorio = nmConsultorio;
	}



	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
	
	
	
	

}
