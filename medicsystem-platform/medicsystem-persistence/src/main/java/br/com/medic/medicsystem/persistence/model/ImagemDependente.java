package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbimagemdependente")
public class ImagemDependente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idimagemdependente")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "nmimagemdependente")
	private String nmImagemDependente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	public ImagemDependente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getNmImagemDependente() {
		return nmImagemDependente;
	}

	public void setNmImagemDependente(String nmImagemDependente) {
		this.nmImagemDependente = nmImagemDependente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente tbdependente) {
		this.dependente = tbdependente;
	}

}