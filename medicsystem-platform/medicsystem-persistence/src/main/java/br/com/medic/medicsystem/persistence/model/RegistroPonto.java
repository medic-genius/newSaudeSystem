package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbregistroponto")
public class RegistroPonto implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGISTROPONTO_ID_SEQ")
	@SequenceGenerator(name = "REGISTROPONTO_ID_SEQ", catalog = "realvida", sequenceName = "registroponto_id_seq", allocationSize = 1)
	@Column(name = "idregistroponto")
	private Long id;
		
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idpontounidade")
	private Unidade unidade;
		
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Funcionario funcionario;
	
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtregistro")
	private Date dtRegistro;
	
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "America/Manaus")
	@Basic
	@Column(name = "hrregistro")
	private Timestamp hrRegistro;
	
	@Basic
	@Column(name = "nmjustificativa")
	private String nmJustificativa;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getDtRegistro() {
		return dtRegistro;
	}

	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}

	public Timestamp getHrRegistro() {
		return hrRegistro;
	}

	public void setHrRegistro(Timestamp hrRegistro) {
		this.hrRegistro = hrRegistro;
	}
	
	public String getNmJustificativa() {
		return nmJustificativa;
	}

	public void setNmJustificativa(String nmJustificativa) {
		this.nmJustificativa = nmJustificativa;
	}
	
	@JsonProperty
	@Transient
	public String getRegistroFormatado (){
		if(dtRegistro != null && hrRegistro != null){
			String dataFormatada = "";
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(hrRegistro.getTime());
			dataFormatada+= sdf.format(dtRegistro);
			sdf = new SimpleDateFormat("HH:mm");
			return dataFormatada+=" "+sdf.format(calendar.getTime());
			
		}
		
		return null;
	}
	
}



