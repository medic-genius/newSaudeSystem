package br.com.medic.dashboard.persistence.model;

import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FaturamentoMovimentacaoContratos implements GraphWrapper {

	private String situacaocontrato;

	private String formapagamento;

	private Integer numerocontratos;

	private Double faturamento;

	private String faturamentoFormatado;

	public FaturamentoMovimentacaoContratos() {
		// TODO Auto-generated constructor stub
	}

	public String getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}

	public Integer getNumerocontratos() {
		return numerocontratos;
	}

	public void setNumerocontratos(Integer numerocontratos) {
		this.numerocontratos = numerocontratos;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public void setFaturamentoFormatado(String faturamentoFormatado) {
		this.faturamentoFormatado = faturamentoFormatado;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFaturamentoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getFaturamento() != null)
			faturamentoFormatado = currencyFormatter.format(getFaturamento());
		return faturamentoFormatado;
	}

	public void setSituacaocontrato(String situacaocontrato) {
		this.situacaocontrato = situacaocontrato;
	}

	public String getSituacaocontrato() {
		return situacaocontrato;
	}

	@Override
	public String getGraphLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getGraphValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
