package br.com.medic.medicsystem.persistence.model.enums;

public enum StatusPagamentoDespesa {

	ABERTO(0, "ABERTO"), 
	PAGO(1, "PAGO"), 
	VENCIDO;
	
	private Integer id;
	
	private String description;
	
	StatusPagamentoDespesa() {
	}
	
	StatusPagamentoDespesa(Integer id, String description) {
		
		this.setId(id);
		this.setDescription(description);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
