package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(catalog = "realvida", name = "tbatendimentoservprofissional")
public class AtendimentoServProfissional  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id															
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATENDIMENTOSERVPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "ATENDIMENTOSERVPROFISSIONAL_ID_SEQ", sequenceName = "realvida.atendimentoservprofissional_id_seq")
	@Column(name = "idatendimentoservprofissional")
	private Long id;
	
	@Basic
	@Column(name = "idatendimentoprofissional")
	private Long idAtendimentoProfissional;
	
	@Basic
	@Column(name = "idservico")
	private Long idServico;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Long idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	

}
