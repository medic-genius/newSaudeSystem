package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.LogVendedor;

@Named("log-vendedor-dao")
@ApplicationScoped
public class LogVendedorDAO extends RelationalDataAccessObject<LogVendedor>{
	public LogVendedor persistLogVendedor(LogVendedor logObject) {
		try {
			return this.persist(logObject);
		} catch(Exception e) {
			
		}
		return null;
	}
	
	public LogVendedor checkCliente(String cardNumber, Integer idEmpresa) {
		String queryStr = "SELECT l FROM LogVendedor l "
				+ "WHERE l.cartaoNumero = :cardNumber "
				+ "AND l.idEmpresa = :idEmpresa ORDER BY l.id DESC";
		TypedQuery<LogVendedor> query = createQuery(queryStr, LogVendedor.class)
				.setParameter("cardNumber", cardNumber)
				.setParameter("idEmpresa", idEmpresa)
				.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch(Exception e) {
		}
		return null;
	}
}
