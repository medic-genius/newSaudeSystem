package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Bairro;

@Named("bairro-dao")
@ApplicationScoped
public class BairroDAO extends RelationalDataAccessObject<Bairro> {

	@SuppressWarnings("unchecked")
	public List<Bairro> getBairrosPorCidade(Long idCidade) {
		Query query = entityManager
				.createQuery("SELECT b FROM Bairro b where b.cidade.id = " + idCidade + " Order by b.nmBairro");

		List<Bairro> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Bairro getBairro(Long idBairro) {
		return searchByKey(Bairro.class, idBairro);
	}

	public Bairro getBairro(String nmBairro, Long idCidade) {
		Query query = entityManager
				.createQuery("SELECT bairro FROM Bairro bairro" + " WHERE lower(bairro.nmBairro) = lower(:nmBairro)"
						+ " and bairro.cidade.id = :idCidade");
		try {
			query.setMaxResults(1);
			query.setParameter("nmBairro", nmBairro);
			query.setParameter("idCidade", idCidade);
			Bairro bairro = (Bairro) query.getSingleResult();
			return bairro;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
