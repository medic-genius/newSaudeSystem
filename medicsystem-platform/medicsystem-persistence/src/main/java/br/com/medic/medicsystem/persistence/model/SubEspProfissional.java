package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbsubespprofissional")
public class SubEspProfissional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUBESPPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "SUBESPPROFISSIONAL_ID_SEQ", sequenceName = "realvida.subespprofissional_id_seq", allocationSize = 1)
	@Column(name = "idsubespprofissional")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@ManyToOne
	@JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name = "idprofissional", referencedColumnName = "idfuncionario")
	private Profissional profissional;

	@ManyToOne
	@JoinColumn(name = "idsubespecialidade")
	private SubEspecialidade subEspecialidade;

	public SubEspProfissional() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public SubEspecialidade getSubEspecialidade() {
		return subEspecialidade;
	}

	public void setSubEspecialidade(SubEspecialidade subEspecialidade) {
		this.subEspecialidade = subEspecialidade;
	}

}