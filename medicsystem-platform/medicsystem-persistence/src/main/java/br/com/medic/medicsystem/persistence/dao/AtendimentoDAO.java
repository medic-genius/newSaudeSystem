package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Atendimento;

@Named("atendimento-dao")
@ApplicationScoped
public class AtendimentoDAO  extends RelationalDataAccessObject<Atendimento> {
	
	public Atendimento getAtendimentoPorIdAgendamento(Long idAgendamento){

		String query = "SELECT aten FROM Atendimento aten WHERE aten.agendamento.id = "+ idAgendamento;

		TypedQuery<Atendimento> queryStr = entityManager.createQuery(query,
				Atendimento.class);

		//		Atendimento atendimento = queryStr.getSingleResult();
		queryStr.setFirstResult(0);
		queryStr.setMaxResults(1);
		try {
			return queryStr.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Boolean checkFinalizacaoAtendimento(Long idAgendamento) {
		String query = "SELECT atp.bobarcode FROM realvida.tbagendamento ag "
				+ "INNER JOIN realvida.tbespprofissional espprof "
				+ "ON espprof.idespecialidade = ag.idespecialidade "
				+ "AND espprof.idfuncionario = ag.idfuncionario "
				+ "LEFT JOIN realvida.tbatendimentoprofissional atp "
				+ "ON atp.idfuncionario = ag.idfuncionario "
				+ "AND atp.idespprofissional = espprof.idespprofissional "
				+ "AND atp.indiasemana = EXTRACT(DOW FROM ag.dtagendamento) "
				+ "AND DATE_TRUNC('seconds', ag.hragendamento) BETWEEN atp.hrinicio AND atp.hrfim "
				+ "LEFT JOIN realvida.tbconsultorio consul ON consul.idconsultorio = atp.idconsultorio "
				+ "WHERE ag.idagendamento = :idAgendamento";
		try {
			Object result = findByNativeQuery(query)
					.setParameter("idAgendamento", idAgendamento)
					.getSingleResult();
			
			if(result != null) {
				return (Boolean) result;
			}
		} catch(ObjectNotFoundException e) {
		}
		return null;
	}
}


