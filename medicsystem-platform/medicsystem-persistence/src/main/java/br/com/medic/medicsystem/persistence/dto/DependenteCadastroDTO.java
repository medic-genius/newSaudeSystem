package br.com.medic.medicsystem.persistence.dto;

public class DependenteCadastroDTO extends PacienteCadastroDTO {
	
	private String nome;
	
	public DependenteCadastroDTO() {
		super();
		this.setTipo(2);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
