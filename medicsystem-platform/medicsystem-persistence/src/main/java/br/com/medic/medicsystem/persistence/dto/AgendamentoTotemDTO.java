package br.com.medic.medicsystem.persistence.dto;

import br.com.medic.medicsystem.persistence.model.Despesa;

public class AgendamentoTotemDTO {
	
	private String nmPaciente;
	
	private String nmProfissional;
	
	private String nmServico;
	
	private String nmConsultorio;
	
	private String nmLocal;
	
	private String nmEspecialidade;
	
	private Integer inStatusAgendamento;
	
	private Long idPaciente;
	
	private Long idProfissional;
	
	private Long idServico;
	
	private Long idEspecialidade;
	
	private Long idAgendamento;
	
	private Boolean triado;
	
	private String nmUnidade;
	
	private String nmRetorno;
	
	private String nmTitular;
	
	private Despesa despesa;
	
	private Integer idade;

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNmProfissional() {
		return nmProfissional;
	}

	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public String getNmLocal() {
		return nmLocal;
	}

	public void setNmLocal(String nmLocal) {
		this.nmLocal = nmLocal;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}

	public Integer getInStatusAgendamento() {
		return inStatusAgendamento;
	}

	public void setInStatusAgendamento(Integer inStatusAgendamento) {
		this.inStatusAgendamento = inStatusAgendamento;
	}

	public String getNmConsultorio() {
		return nmConsultorio;
	}

	public void setNmConsultorio(String nmConsultorio) {
		this.nmConsultorio = nmConsultorio;
	}

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Boolean getTriado() {
		return triado;
	}

	public void setTriado(Boolean triado) {
		this.triado = triado;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public String getNmRetorno() {
		return nmRetorno;
	}

	public void setNmRetorno(String nmRetorno) {
		this.nmRetorno = nmRetorno;
	}

	public String getNmTitular() {
		return nmTitular;
	}

	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}


	
	

}
