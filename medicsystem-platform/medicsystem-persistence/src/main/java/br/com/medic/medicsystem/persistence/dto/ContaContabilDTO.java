package br.com.medic.medicsystem.persistence.dto;



public class ContaContabilDTO {
	
	private Long idEmpresaFinanceiroStart;
	private Long idContaFilho;
	private String nmDescricao;
	private Long idContaContabil;
	private Double totalDespesa;
	private String mesAno;
	
	private Integer ano;
	
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getIdEmpresaFinanceiroStart() {
		return idEmpresaFinanceiroStart;
	}
	public void setIdEmpresaFinanceiroStart(Long idEmpresaFinanceiroStart) {
		this.idEmpresaFinanceiroStart = idEmpresaFinanceiroStart;
	}
	public String getNmDescricao() {
		return nmDescricao;
	}
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	public Long getIdContaContabil() {
		return idContaContabil;
	}
	public void setIdContaContabil(Long idContaContabil) {
		this.idContaContabil = idContaContabil;
	}
	public Double getTotalDespesa() {
		return totalDespesa;
	}
	public void setTotalDespesa(Double totalDespesa) {
		this.totalDespesa = totalDespesa;
	}
	public String getMesAno() {
		return mesAno;
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}
	public Long getIdContaFilho() {
		return idContaFilho;
	}
	public void setIdContaFilho(Long idContaFilho) {
		this.idContaFilho = idContaFilho;
	}

}