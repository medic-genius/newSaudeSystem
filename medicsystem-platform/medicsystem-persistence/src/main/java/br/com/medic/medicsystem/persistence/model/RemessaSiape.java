package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbmovremessasiape")
public class RemessaSiape implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOVREMESSASIAPE_ID_SEQ")
	@SequenceGenerator(name = "MOVREMESSASIAPE_ID_SEQ", sequenceName = "realvida.movremessasiape_id_seq")
	@Column(name = "idmovremessasiape")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtremessasiape")
	private Date dtRemessaSiape;

	//bi-directional many-to-one association to Tbmovremessasiape
	@OneToMany(mappedBy="tbremessasiape")
	private List<MovRemessaSiape> movRemessaSiapes;

	//bi-directional many-to-one association to Tbempresagrupo
	@ManyToOne
	@JoinColumn(name="idempresagrupo")
	private EmpresaGrupo empresaGrupo;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public RemessaSiape() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtRemessaSiape() {
		return dtRemessaSiape;
	}

	public void setDtRemessaSiape(Date dtRemessaSiape) {
		this.dtRemessaSiape = dtRemessaSiape;
	}

	public List<MovRemessaSiape> getMovRemessaSiapes() {
		return movRemessaSiapes;
	}

	public void setMovRemessaSiapes(List<MovRemessaSiape> movRemessaSiapes) {
		this.movRemessaSiapes = movRemessaSiapes;
	}

	public EmpresaGrupo getEmpresaGrupo() {
		return empresaGrupo;
	}

	public void setEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}