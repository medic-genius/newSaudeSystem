package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbcreditopessoa;")
public class CreditoPessoal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CREDITOPESSOAL_ID_SEQ")
	@SequenceGenerator(name = "CREDITOPESSOAL_ID_SEQ", sequenceName = "realvida.creditopessoal_id_seq")
	@Column(name = "idcreditopessoal")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtcreditopessoal")
	private Date dtCreditoPessoal;
	
	@Basic
	@Column(name = "nmmotivo")
	private String nmMotivo;
	
	@Basic
	@Column(name = "vlcreditopessoal")
	private float vlCreditoPessoal;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	public CreditoPessoal() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtCreditoPessoal() {
		return dtCreditoPessoal;
	}

	public void setDtCreditoPessoal(Date dtCreditoPessoal) {
		this.dtCreditoPessoal = dtCreditoPessoal;
	}

	public String getNmMotivo() {
		return nmMotivo;
	}

	public void setNmMotivo(String nmMotivo) {
		this.nmMotivo = nmMotivo;
	}

	public float getVlCreditoPessoal() {
		return vlCreditoPessoal;
	}

	public void setVlCreditoPessoal(float vlCreditoPessoal) {
		this.vlCreditoPessoal = vlCreditoPessoal;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}