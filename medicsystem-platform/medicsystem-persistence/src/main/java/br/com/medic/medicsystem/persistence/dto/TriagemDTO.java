package br.com.medic.medicsystem.persistence.dto;



public class TriagemDTO {
	

	private Double vlPeso;


	private Double vlAltura;

	
	private Integer txGlicose;


	private Integer vlTemperatura;


	private String nmTipoSanguineo;


	private String nmPressaoArterial;


	private String nmPressaoArterialExcelente;


	public Double getVlPeso() {
		return vlPeso;
	}


	public void setVlPeso(Double vlPeso) {
		this.vlPeso = vlPeso;
	}


	public Double getVlAltura() {
		return vlAltura;
	}


	public void setVlAltura(Double vlAltura) {
		this.vlAltura = vlAltura;
	}


	public Integer getTxGlicose() {
		return txGlicose;
	}


	public void setTxGlicose(Integer txGlicose) {
		this.txGlicose = txGlicose;
	}


	public Integer getVlTemperatura() {
		return vlTemperatura;
	}


	public void setVlTemperatura(Integer vlTemperatura) {
		this.vlTemperatura = vlTemperatura;
	}


	public String getNmTipoSanguineo() {
		return nmTipoSanguineo;
	}


	public void setNmTipoSanguineo(String nmTipoSanguineo) {
		this.nmTipoSanguineo = nmTipoSanguineo;
	}


	public String getNmPressaoArterial() {
		return nmPressaoArterial;
	}


	public void setNmPressaoArterial(String nmPressaoArterial) {
		this.nmPressaoArterial = nmPressaoArterial;
	}


	public String getNmPressaoArterialExcelente() {
		return nmPressaoArterialExcelente;
	}


	public void setNmPressaoArterialExcelente(String nmPressaoArterialExcelente) {
		this.nmPressaoArterialExcelente = nmPressaoArterialExcelente;
	}
	
	
	
	

}
