package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(catalog="util", name="tmp_sugestaodeconciliacaobancaria")
public class SugestaoConciliacaoBancaria implements Serializable{

	private static final long serialVersionUID = 1L;

	
	@Column(name="idusuario")
	private Long idUsuario;
	
	@Id
	@Column(name="idtransacao")
	private Long idTransacao;
	
	@ManyToOne
	@JoinColumn(name="iddespesa")
	private DespesaFinanceiro lancamentos;
	
	@Basic
	@Column(name="transacao_data")
	private Date dataTransacao;
	
	@Basic
	@Column(name="transacao_valor")
	private Double valor;
	
	@Basic
	@Column(name="transacao_nome")
	private String transacaoNome;

	@Basic
	@Column(name="despesa_data")
	private Date despesaData;
	
	@Basic
	@Column(name="despesa_descricao")
	private String nmDespesa;
	
	@ManyToOne
	@JoinColumn(name="despesa_idfornecedor")
	private Fornecedor fornecedor;
	
	@Basic
	@Column(name="despesa_descricaofornecedor")
	private String nmFornecedor;
	
	@ManyToOne
	@JoinColumn(name="despesa_idcontacontabil")
	private ContaContabil contaContabil;
	
	@Basic
	@Column(name="despesa_descricaocontacontabil")
	private String nmContaContabil;
	
	@ManyToOne
	@JoinColumn(name="despesa_idcentrodecusto")
	private CentroCusto centroCusto;
	
	@Basic
	@Column(name="despesa_descricaocentrodecusto")
	private String nmCentroCusto;
	
	@Basic
	@Column(name="despesa_status")
	private Integer inStatus;
	
	@Basic
	@Column(name="despesa_valor")
	private Double valorDespesa;
	
	@Basic
	@Column(name="despesa_vldesconto")
	private Double vlDesconto;
	
	@Basic
	@Column(name="despesa_vljuros")
	private Double vlJuros;
	
	@Basic
	@Column(name="despesa_vlMulta")
	private Double vlMulta;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdTransacao() {
		return idTransacao;
	}

	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}
	

	public Date getDataTransacao() {
		return dataTransacao;
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getTransacaoNome() {
		return transacaoNome;
	}

	public void setTransacaoNome(String transacaoNome) {
		this.transacaoNome = transacaoNome;
	}

	public Date getDespesaData() {
		return despesaData;
	}

	public void setDespesaData(Date despesaData) {
		this.despesaData = despesaData;
	}

	public String getNmDespesa() {
		return nmDespesa;
	}

	public void setNmDespesa(String nmDespesa) {
		this.nmDespesa = nmDespesa;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getNmFornecedor() {
		return nmFornecedor;
	}

	public void setNmFornecedor(String nmFornecedor) {
		this.nmFornecedor = nmFornecedor;
	}

	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	public String getNmContaContabil() {
		return nmContaContabil;
	}

	public void setNmContaContabil(String nmContaContabil) {
		this.nmContaContabil = nmContaContabil;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public String getNmCentroCusto() {
		return nmCentroCusto;
	}

	public void setNmCentroCusto(String nmCentroCusto) {
		this.nmCentroCusto = nmCentroCusto;
	}

	public Double getValorDespesa() {
		return valorDespesa;
	}

	public void setValorDespesa(Double valorDespesa) {
		this.valorDespesa = valorDespesa;
	}

	public Double getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(Double vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public Double getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(Double vlJuros) {
		this.vlJuros = vlJuros;
	}

	public Double getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(Double vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public DespesaFinanceiro getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(DespesaFinanceiro lancamentos) {
		this.lancamentos = lancamentos;
	}

	
	
}
