package br.com.medic.medicsystem.persistence.dto;

import java.util.HashMap;

/**
 * Entidade que representa um funcionario
 * obtido do arquivo .txt da folha de pagamento
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

public class FuncionarioFolha {

	private String cod;
	private String name;
	private String cargo;
	private String admissao;
	private Long nrodep;
	private String salario;
	@SuppressWarnings("rawtypes")
	private HashMap fluxo;
	private String liquido;
	private String base_irrf;
	private String contr_inss;
	private String inss_segurado;
	private String base_fgts;
	private String fgts;

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getAdmissao() {
		return admissao;
	}

	public void setAdmissao(String admissao) {
		this.admissao = admissao;
	}

	public Long getNrodep() {
		return nrodep;
	}

	public void setNrodep(Long nrodep) {
		this.nrodep = nrodep;
	}

	public String getSalario() {
		return salario;
	}

	public void setSalario(String salario) {
		this.salario = salario;
	}

	@SuppressWarnings("rawtypes")
	public HashMap getFluxo() {
		return fluxo;
	}

	public void setFluxo(@SuppressWarnings("rawtypes") HashMap fluxo) {
		this.fluxo = fluxo;
	}

	public String getLiquido() {
		return liquido;
	}

	public void setLiquido(String liquido) {
		this.liquido = liquido;
	}

	public String getBase_irrf() {
		return base_irrf;
	}

	public void setBase_irrf(String base_irrf) {
		this.base_irrf = base_irrf;
	}

	public String getContr_inss() {
		return contr_inss;
	}

	public void setContr_inss(String contr_inss) {
		this.contr_inss = contr_inss;
	}

	public String getInss_segurado() {
		return inss_segurado;
	}

	public void setInss_segurado(String inss_segurado) {
		this.inss_segurado = inss_segurado;
	}

	public String getBase_fgts() {
		return base_fgts;
	}

	public void setBase_fgts(String base_fgts) {
		this.base_fgts = base_fgts;
	}

	public String getFgts() {
		return fgts;
	}

	public void setFgts(String fgts) {
		this.fgts = fgts;
	}

}
