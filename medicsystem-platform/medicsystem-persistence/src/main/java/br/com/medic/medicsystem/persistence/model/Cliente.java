package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@NamedQuery(name = "mNovosClientes", query = "SELECT cli "
        + "FROM Cliente cli, ContratoCliente concli LEFT JOIN concli.contrato as c "
        + "WHERE concli.contrato = c  AND c.empresaGrupo.id = 9 "
        + "AND concli.cliente = cli "
        + "AND c.plano is not null  AND c.situacao = 0 AND cli.dtExclusao is null "
        + "AND cli.dtInclusao = :dtincl GROUP BY cli ")
@Entity
@Table(catalog = "realvida", name = "tbcliente")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENTE_ID_SEQ")
	@SequenceGenerator(name = "CLIENTE_ID_SEQ", sequenceName = "realvida.cliente_id_seq", allocationSize = 1)
	@Column(name = "idcliente")
	private Long id;

	@Basic
	@Column(name = "nrcodcliente", nullable = false, unique = true)
	private String nrCodCliente;

	@NotNull
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;

	
	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;

	@Basic
	@Column(name = "longitude")
	private Float longitude;
	
	@Basic
	@Column(name = "latitude")
	private Float latitude;

	@NotNull
	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@NotNull
	@Basic
	@Column(name = "nrcpf")
	private String nrCPF;

	@Basic
	@Column(name = "nrrg")
	private String nrRG;

	// TODO Port to Enum
	@Basic
	@Column(name = "inestadocivil")
	private Integer inEstadoCivil;

	@Basic
	@Column(name = "insexo")
	private Integer inSexo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtnascimento")
	private Date dtNascimento;

	@Basic
	@Column(name = "btfotocliente")
	private byte[] btFotoCliente;

	@Basic
	@Column(name = "vlcreditopessoaltotal")
	private Double vlCreditoPessoalTotal;

	@Basic
	@Column(name = "vlsalario")
	private Double vlSalario;

	@Basic
	@Column(name = "nrpercentual")
	private Integer nrPercentual;

	@Basic
	@Column(name = "vllimite")
	private Double vlLimite;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nrcep")
	private String nrCEP;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nmemail")
	private String nmEmail;

	@Basic
	@Column(name = "bobloqueado")
	private Boolean boBloqueado;

	@Basic
	@Column(name = "bonaoassociado")
	private Boolean boNaoAssociado;

	@Basic
	@Column(name = "nmocupacao")
	private String nmOcupacao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "boestrangeiro")
	private Boolean boEstrangeiro;

	@Basic
	@Column(name = "vlpeso")
	private Double vlPeso;

	@Basic
	@Column(name = "vlaltura")
	private Double vlAltura;

	@Basic
	@Column(name = "txglicose")
	private Integer txGlicose;

	@Basic
	@Column(name = "vltemperatura")
	private Integer vlTemperatura;

	@Basic
	@Column(name = "nmtiposanguineo")
	private String nmTipoSanguineo;

	@Basic
	@Column(name = "nmpressaoarterial")
	private String nmPressaoArterial;

	@Basic
	@Column(name = "nmpressaoarterialexcelente")
	private String nmPressaoArterialExcelente;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idclienteaux")
	private Long idClienteAux;

	@Basic
	@Column(name = "idclienteodonto")
	private Long idClienteOdonto;

	@ManyToOne
	@JoinColumn(name = "idenderecocobranca")
	private EnderecoCobranca enderecoCobranca;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "inarquivosiapegerado")
	private Integer inArquivoSiapeGerado;

	@Basic
	@Column(name = "instatusretorno")
	private Integer inStatusRetorno;

	@Basic
	@Column(name = "instatusretornoodonto")
	private Integer inStatusRetornoOdonto;

	@Basic
	@Column(name = "instatussiape")
	private Integer inStatusSiape;

	@Basic
	@Column(name = "instatussiapeodonto")
	private Integer inStatusSiapeOdonto;

	@Basic
	@Column(name = "nmobservacaoaux")
	private String nmObservacaoAux;

	@Basic
	@Column(name = "observacao")
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	@Transient
	@JsonIgnore
	private String sexoFormatado;

	@Basic
	@Column(name = "digitalcliente")
	private String digitalCliente;

	@Basic
	@Column(name = "fotocliente")
	@JsonIgnore
	private byte[] fotoCliente;

	@Transient
	@JsonIgnore
	private String fotoClienteDecoded;
	
	@Transient
	private String fotoClienteEncoded;
	
	@Transient
	@JsonIgnore
	private Integer idade;
	
	@Basic
	@Column(name = "iddedodigital")
	private Integer idDedoDigital;
	
	@Basic
	@Column(name = "idclienteboleto_sl")
	private Long idClienteBoleto_sl;
	
	@Basic
	@Column(name = "idrecorrencia")
	private Long idRecorrencia;

	
	@JsonIgnore
	public void setFotoClienteDecoded(String fotoClienteDecoded) {
		this.fotoClienteDecoded = fotoClienteDecoded;
	}
	@JsonIgnore
	public byte[] getFotoCliente() {
		return fotoCliente;
	}

	public void setFotoCliente(byte[] fotoCliente) {
		this.fotoCliente = fotoCliente;
	}

	public Cliente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public String getNrRG() {
		return nrRG;
	}

	public void setNrRG(String nrRG) {
		this.nrRG = nrRG;
	}

	public Integer getInEstadoCivil() {
		return inEstadoCivil;
	}

	public void setInEstadoCivil(Integer inEstadoCivil) {
		this.inEstadoCivil = inEstadoCivil;
	}

	public Integer getInSexo() {
		return inSexo;
	}

	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public byte[] getBtFotoCliente() {
		return btFotoCliente;
	}

	public void setBtFotoCliente(byte[] btFotoCliente) {
		this.btFotoCliente = btFotoCliente;
	}

	public Double getVlCreditoPessoalTotal() {
		return vlCreditoPessoalTotal;
	}

	public void setVlCreditoPessoalTotal(Double vlCreditoPessoalTotal) {
		this.vlCreditoPessoalTotal = vlCreditoPessoalTotal;
	}

	public Double getVlSalario() {
		return vlSalario;
	}

	public void setVlSalario(Double vlSalario) {
		this.vlSalario = vlSalario;
	}

	public Integer getNrPercentual() {
		return nrPercentual;
	}

	public void setNrPercentual(Integer nrPercentual) {
		this.nrPercentual = nrPercentual;
	}

	public Double getVlLimite() {
		return vlLimite;
	}

	public void setVlLimite(Double vlLimite) {
		this.vlLimite = vlLimite;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNrCEP() {
		return nrCEP;
	}

	public void setNrCEP(String nrCEP) {
		this.nrCEP = nrCEP;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public Boolean getBoBloqueado() {
		return boBloqueado;
	}

	public void setBoBloqueado(Boolean boBloqueado) {
		this.boBloqueado = boBloqueado;
	}

	public Boolean getBoNaoAssociado() {
		return boNaoAssociado;
	}

	public void setBoNaoAssociado(Boolean boNaoAssociado) {
		this.boNaoAssociado = boNaoAssociado;
	}

	public String getNmOcupacao() {
		return nmOcupacao;
	}

	public void setNmOcupacao(String nmOcupacao) {
		this.nmOcupacao = nmOcupacao;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Boolean getBoEstrangeiro() {
		return boEstrangeiro;
	}

	public void setBoEstrangeiro(Boolean boEstrangeiro) {
		this.boEstrangeiro = boEstrangeiro;
	}

	public Double getVlPeso() {
		return vlPeso;
	}

	public void setVlPeso(Double vlPeso) {
		this.vlPeso = vlPeso;
	}

	public Double getVlAltura() {
		return vlAltura;
	}

	public void setVlAltura(Double vlAltura) {
		this.vlAltura = vlAltura;
	}

	public Integer getTxGlicose() {
		return txGlicose;
	}

	public void setTxGlicose(Integer txGlicose) {
		this.txGlicose = txGlicose;
	}

	public Integer getVlTemperatura() {
		return vlTemperatura;
	}

	public void setVlTemperatura(Integer vlTemperatura) {
		this.vlTemperatura = vlTemperatura;
	}

	public String getNmTipoSanguineo() {
		return nmTipoSanguineo;
	}

	public void setNmTipoSanguineo(String nmTipoSanguineo) {
		this.nmTipoSanguineo = nmTipoSanguineo;
	}

	public String getNmPressaoArterial() {
		return nmPressaoArterial;
	}

	public void setNmPressaoArterial(String nmPressaoArterial) {
		this.nmPressaoArterial = nmPressaoArterial;
	}

	public String getNmPressaoArterialExcelente() {
		return nmPressaoArterialExcelente;
	}

	public void setNmPressaoArterialExcelente(String nmPressaoArterialExcelente) {
		this.nmPressaoArterialExcelente = nmPressaoArterialExcelente;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdClienteAux() {
		return idClienteAux;
	}

	public void setIdClienteAux(Long idClienteAux) {
		this.idClienteAux = idClienteAux;
	}

	public Long getIdClienteOdonto() {
		return idClienteOdonto;
	}

	public void setIdClienteOdonto(Long idClienteOdonto) {
		this.idClienteOdonto = idClienteOdonto;
	}

	public EnderecoCobranca getEnderecoCobranca() {
		return enderecoCobranca;
	}

	public void setEnderecoCobranca(EnderecoCobranca enderecoCobranca) {
		this.enderecoCobranca = enderecoCobranca;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Integer getInArquivoSiapeGerado() {
		return inArquivoSiapeGerado;
	}

	public void setInArquivoSiapeGerado(Integer inArquivoSiapeGerado) {
		this.inArquivoSiapeGerado = inArquivoSiapeGerado;
	}

	public Integer getInStatusRetorno() {
		return inStatusRetorno;
	}

	public void setInStatusRetorno(Integer inStatusRetorno) {
		this.inStatusRetorno = inStatusRetorno;
	}

	public Integer getInStatusRetornoOdonto() {
		return inStatusRetornoOdonto;
	}

	public void setInStatusRetornoOdonto(Integer inStatusRetornoOdonto) {
		this.inStatusRetornoOdonto = inStatusRetornoOdonto;
	}

	public Integer getInStatusSiape() {
		return inStatusSiape;
	}

	public void setInStatusSiape(Integer inStatusSiape) {
		this.inStatusSiape = inStatusSiape;
	}

	public Integer getInStatusSiapeOdonto() {
		return inStatusSiapeOdonto;
	}

	public void setInStatusSiapeOdonto(Integer inStatusSiapeOdonto) {
		this.inStatusSiapeOdonto = inStatusSiapeOdonto;
	}

	public String getNmObservacaoAux() {
		return nmObservacaoAux;
	}

	public void setNmObservacaoAux(String nmObservacaoAux) {
		this.nmObservacaoAux = nmObservacaoAux;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonProperty
	public String getSexoFormatado() {
		if (inSexo != null) {
			if (inSexo == 0) {
				return "Masculino";
			}
			return "Feminino";
		}
		return null;
	}

	@JsonIgnore
	public void setSexoFormatado(String sexoFormatado) {

	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getDigitalCliente() {
		return digitalCliente;
	}

	public void setDigitalCliente(String digitalCliente) {
		this.digitalCliente = digitalCliente;
	}


	@JsonIgnore
	public String getFotoClienteDecoded() {
		if (getFotoCliente() != null) {
			// Base64.encodeBase64String(getFotoCliente());
			return new String(getFotoCliente());
		}
		return null;
	}
	
	@JsonProperty
	public String getFotoClienteEncoded() {
		return fotoClienteEncoded;
	}
	
	@JsonProperty
	public void setFotoClienteEncoded(String fotoClienteEncoded) {
		this.fotoClienteEncoded = fotoClienteEncoded;
	}
	
	public Integer getIdDedoDigital() {
		return idDedoDigital;
	}
	
	public void setIdDedoDigital(Integer idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}
	
	@JsonProperty
	public Integer getIdade() {
		GregorianCalendar hj=new GregorianCalendar();
		GregorianCalendar nascimento=new GregorianCalendar();
		
		if(getDtNascimento() != null){
				nascimento.setTime(getDtNascimento());
						
				int anohj=hj.get(Calendar.YEAR);
				int anoNascimento=nascimento.get(Calendar.YEAR);
				
				
				 idade = anohj-anoNascimento;
				
				 
				 if ( hj.get(Calendar.MONTH) < nascimento.get(Calendar.MONTH)) {
					 idade --;  
				    }
				
				return idade;
		
		}
		
		return 0;
	}
	
	public Long getIdClienteBoleto_sl() {
		return idClienteBoleto_sl;
	}
	public void setIdClienteBoleto_sl(Long idClienteBoleto_sl) {
		this.idClienteBoleto_sl = idClienteBoleto_sl;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	
	public Long getIdRecorrencia() {
		return idRecorrencia;
	}
	public void setIdRecorrencia(Long idRecorrencia) {
		this.idRecorrencia = idRecorrencia;
	}
	
	

}
