package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Plano;

@Named("plano-dao")
@ApplicationScoped
public class PlanoDAO extends RelationalDataAccessObject<Plano> {

	public CoberturaPlano getCoberturaPlanoPorServico(Long idPlano,
            Long idServico) {
		
		TypedQuery<CoberturaPlano> query = entityManager.createQuery(
		        "SELECT cp from CoberturaPlano cp where cp.plano.id = "
		                + idPlano + " and cp.servico.id = " + idServico,
		                CoberturaPlano.class);

	    query.setMaxResults(1);
	    
		return query.getSingleResult();

    }

	public List<Plano> gePlanosPorEmpresaGrupo(Long idEmpresa) {
		TypedQuery<Plano> query = entityManager.createQuery(
		        "SELECT p FROM Plano p where p.idEmpresaGrupo = "
		                + idEmpresa + " order by p.nmPlano asc",
		        Plano.class);
		List<Plano> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<Plano> gePlanosByEmpresaGrupo(Long idEmpresa) {
		String queryStr = "SELECT p FROM Plano p ";
		if(idEmpresa != null){
			queryStr +=" WHERE p.idEmpresaGrupo = "+ idEmpresa;
		}
		 queryStr +=" ORDER BY p.nmPlano asc";
		TypedQuery<Plano> query = entityManager.createQuery(queryStr,
		        Plano.class);
		List<Plano> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
}
