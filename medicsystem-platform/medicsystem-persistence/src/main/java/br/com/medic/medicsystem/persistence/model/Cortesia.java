package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbcortesia")
public class Cortesia implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CORTESIA_ID_SEQ")
	@SequenceGenerator(name = "CORTESIA_ID_SEQ", catalog = "realvida", sequenceName = "cortesia_id_seq", allocationSize = 1)
	@Column(name = "idcortesia")
	private Long id;
	
	@Basic
	@Column(name = "nmvendedor")
	private String nmVendedor;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;
	
	@Basic
	@Column(name = "nmdescricao")
	private String nmDescricao;
	
	@Basic
	@Column(name = "boutilizada")
	private Boolean boUtilizada;
	
	/*
	 * indica em que estado está a cortesia
		0 - não averiguado, 1 - aprovado, 2 - reprovado
	 */
	@Basic
	@Column(name = "instatuscortesia")
	private Integer inStatusCortesia;
	
	@ManyToOne
	@JoinColumn(name = "idfuncionarioaprorepro")
	private Funcionario FuncionarioAproRepro;
	
	@ManyToOne
	@JoinColumn(name = "idfuncionarioliberou")
	private Funcionario funcionarioLiberou;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaolog;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "datanascimento")
	private Date dataNascimento;
	
	@Transient
	@JsonProperty
	private String statusCortesiaFormatado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmVendedor() {
		return nmVendedor;
	}

	public void setNmVendedor(String nmVendedor) {
		this.nmVendedor = nmVendedor;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmDescricao() {
		return nmDescricao;
	}

	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}

	public Boolean getBoUtilizada() {
		return boUtilizada;
	}

	public void setBoUtilizada(Boolean boUtilizada) {
		this.boUtilizada = boUtilizada;
	}


	public Integer getInStatusCortesia() {
		return inStatusCortesia;
	}

	public void setInStatusCortesia(Integer inStatusCortesia) {
		this.inStatusCortesia = inStatusCortesia;
	}

	public Funcionario getFuncionarioAproRepro() {
		return FuncionarioAproRepro;
	}

	public void setFuncionarioAproRepro(Funcionario funcionarioAproRepro) {
		FuncionarioAproRepro = funcionarioAproRepro;
	}

	public Funcionario getFuncionarioLiberou() {
		return funcionarioLiberou;
	}

	public void setFuncionarioLiberou(Funcionario funcionarioLiberou) {
		this.funcionarioLiberou = funcionarioLiberou;
	}

	public Timestamp getDtInclusaolog() {
		return dtInclusaolog;
	}

	public void setDtInclusaolog(Timestamp dtInclusaolog) {
		this.dtInclusaolog = dtInclusaolog;
	}
	

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getStatusCortesiaFormatado() {
		if(inStatusCortesia != null){
			if(inStatusCortesia.equals(0))
				return "Não Averiguada";
			if(inStatusCortesia.equals(1))
				return "Aprovada";
			if(inStatusCortesia.equals(2))
				return "Reprovada";
			
			return "Não Definido";
		}
		return "Não Definido";
	}

	public void setStatusCortesiaFormatado(String statusCortesiaFormatado) {
		this.statusCortesiaFormatado = statusCortesiaFormatado;
	}
	
	
	
	
	

}
