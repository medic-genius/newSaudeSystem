package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbimagemcliente")
public class ImagemCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMAGEMCLIENTE_ID_SEQ")
	@SequenceGenerator(name = "IMAGEMCLIENTE_ID_SEQ", sequenceName = "realvida.imagemcliente_id_seq")
	@Column(name = "idimagemcliente")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "nmimagemcliente")
	private String nmImagemCliente;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;
	

	
	@Transient
	@JsonIgnore
	private String urlImagem;

	public ImagemCliente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getNmImagemCliente() {
		return nmImagemCliente;
	}

	public void setNmImagemCliente(String nmImagemCliente) {
		this.nmImagemCliente = nmImagemCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@JsonProperty
	public String getUrlImagem() {
		
		if(id != null){
			return getCliente().getNrCodCliente() + "/" + getNmImagemCliente(); 
		}
		
		return urlImagem;
	}
	
	@JsonIgnore
	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	
	
	
	


}