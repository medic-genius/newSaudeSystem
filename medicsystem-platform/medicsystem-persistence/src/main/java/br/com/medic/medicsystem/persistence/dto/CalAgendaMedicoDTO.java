package br.com.medic.medicsystem.persistence.dto;

import java.sql.Time;
import java.util.Date;

public class CalAgendaMedicoDTO {
	
	
	 private Long idEspecialidade; 
	 private String nmEspecialidade;
	 private Long idFuncionario;  
	 private String nmFuncionario; 
	 private Integer inDiaSemana; 
	 private Time hrInicio; 
	 private Time hrFim;
	 private Long idUnidade; 
	 private Integer qtdAtend;
	 private Integer inTipoConsulta;
	 private Date dtAtend;
	 private String nmApelido;
	 private Boolean boParticular;
	 
	 
	 
	public Long getIdEspecialidade() {
		return idEspecialidade;
	}
	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getNmFuncionario() {
		return nmFuncionario;
	}
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public Integer getInDiaSemana() {
		return inDiaSemana;
	}
	public void setInDiaSemana(Integer inDiaSemana) {
		this.inDiaSemana = inDiaSemana;
	}

	public Time getHrInicio() {
		return hrInicio;
	}
	public void setHrInicio(Time hrInicio) {
		this.hrInicio = hrInicio;
	}
	public Time getHrFim() {
		return hrFim;
	}
	public void setHrFim(Time hrFim) {
		this.hrFim = hrFim;
	}
	
	public Long getIdUnidade() {
		return idUnidade;
	}
	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	public Integer getInTipoConsulta() {
		return inTipoConsulta;
	}
	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}
	public Integer getQtdAtend() {
		return qtdAtend;
	}
	public void setQtdAtend(Integer qtdAtend) {
		this.qtdAtend = qtdAtend;
	}
	public Date getDtAtend() {
		return dtAtend;
	}
	public void setDtAtend(Date dtAtend) {
		this.dtAtend = dtAtend;
	}
	public String getNmApelido() {
		return nmApelido;
	}
	public void setNmApelido(String nmApelido) {
		this.nmApelido = nmApelido;
	}
	public Boolean getBoParticular() {
		return boParticular;
	}
	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}


	
	 
	 
	 
}
	 
	 
	 
	