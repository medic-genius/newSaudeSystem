package br.com.medic.medicsystem.persistence.dto;

import br.com.medic.medicsystem.persistence.model.Credenciada;

public class ServicoCredenciadaDTO {

	private Credenciada credenciada;

	private String descricao;

	private Double valor;

	private Boolean cobertura;

	private Double custo;
	
	private Long idPlano;

	public ServicoCredenciadaDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Credenciada getCredenciada() {
		return credenciada;
	}

	public void setCredenciada(Credenciada credenciada) {
		this.credenciada = credenciada;
	}

	public Boolean getCobertura() {
		return cobertura;
	}

	public void setCobertura(Boolean cobertura) {
		this.cobertura = cobertura;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getCusto() {
		return custo;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}

	public Long getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}
	
	

}
