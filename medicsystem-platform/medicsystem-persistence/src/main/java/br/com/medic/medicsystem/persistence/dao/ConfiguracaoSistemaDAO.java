package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ConfiguracaoSistema;

@Named("configuracaosistema-dao")
@ApplicationScoped
public class ConfiguracaoSistemaDAO extends RelationalDataAccessObject<ConfiguracaoSistema> {

	public ConfiguracaoSistema getConfiguracaoSistema() {
	    Query query = entityManager.createQuery("SELECT c FROM ConfiguracaoSistema c");
	    ConfiguracaoSistema result = (ConfiguracaoSistema) query.getResultList().get(0);

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

}
