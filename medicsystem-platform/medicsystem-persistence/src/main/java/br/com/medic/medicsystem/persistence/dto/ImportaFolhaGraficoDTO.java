package br.com.medic.medicsystem.persistence.dto;

/**
 * Entidade que representa o graficos de 
 * um conjunto de folhas de pagamento, mostrando
 * o periodo e valor pago.
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

public class ImportaFolhaGraficoDTO {

	private String mes;
	private Double folha;

	public Double getFolha() {
		return folha;
	}

	public String getMes() {
		return mes;
	}

	public void setFolha(Double folha) {
		this.folha = folha;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

}
