package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * 
 * @author Phillip Furtando
 *
 */

@Entity
@Table(catalog = "realvida", name = "tbconfiguracaosistema")
public class ConfiguracaoSistema implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONFIGURACAOSISTEMA_ID_SEQ")
	@SequenceGenerator(name = "CONFIGURACAOSISTEMA_ID_SEQ", sequenceName = "realvida.configuracaosistema_id_seq")
	@Column(name = "idconfiguracaosistema")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "idempresamedicasiape")
	private Long idEmpresaMedicaSiape;
	
	@Basic
	@Column(name = "idempresaodontosiape")
	private Long idEmpresaOdontoSiape;
	
	@Basic
	@Column(name = "idoftalmologia")
	private Long idOftalmologia;
	
	@Basic
	@Column(name = "idservicocartao")
	private Long idServicoCartao;
	
	@Basic
	@Column(name = "nmpathimagem")
	private String nmPathImagem;
	
	@Basic
	@Column(name = "nmpathimagemodontologica")
	private String nmPathImagemOdontologica;
	
	@Basic
	@Column(name = "nmraizremessaarquivo")
	private String nmRaizRemessaArquivo;
	
	@Basic
	@Column(name = "nmraizremessasiape")
	private String nmRaizRemessaSiape;
	
	@Basic
	@Column(name = "nmraizretornoarquivo")
	private String nmRaizRetornoArquivo;
	
	@Basic
	@Column(name = "nmseparador")
	private String nmSeparador;
	
	@Basic
	@Column(name = "qtddiasretornobancobrasil")
	private Integer qtdDiasRetornoBancoBrasil;
	
	@Basic
	@Column(name = "vlmultaatraso")
	private float vlMultaAtraso;
	
	@Basic
	@Column(name = "vltaxajuros")
	private float vlTaxaJuros;
	
	@Basic
	@Column(name = "nmpatharquivofuncionario")
	private String nmPathArquivoFuncionario;

	@Basic
	@Column(name = "nmpatharquivodespesa")
	private String nmPathArquivoDespesa;
	
	public ConfiguracaoSistema() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdEmpresaMedicaSiape() {
		return idEmpresaMedicaSiape;
	}

	public void setIdEmpresaMedicaSiape(Long idEmpresaMedicaSiape) {
		this.idEmpresaMedicaSiape = idEmpresaMedicaSiape;
	}

	public Long getIdEmpresaOdontoSiape() {
		return idEmpresaOdontoSiape;
	}

	public void setIdEmpresaOdontoSiape(Long idEmpresaOdontoSiape) {
		this.idEmpresaOdontoSiape = idEmpresaOdontoSiape;
	}

	public Long getIdOftalmologia() {
		return idOftalmologia;
	}

	public void setIdOftalmologia(Long idOftalmologia) {
		this.idOftalmologia = idOftalmologia;
	}

	public Long getIdServicoCartao() {
		return idServicoCartao;
	}

	public void setIdServicoCartao(Long idServicoCartao) {
		this.idServicoCartao = idServicoCartao;
	}

	public String getNmPathImagem() {
		return nmPathImagem;
	}
	public void setNmPathImagem(String nmPathImagem) {
		this.nmPathImagem = nmPathImagem;
	}
	public String getNmPathImagemOdontologica() {
		return nmPathImagemOdontologica;
	}
	public void setNmPathImagemOdontologica(String nmPathImagemOdontologica) {
		this.nmPathImagemOdontologica = nmPathImagemOdontologica;
	}
	public String getNmRaizRemessaArquivo() {
		return nmRaizRemessaArquivo;
	}
	public void setNmRaizRemessaArquivo(String nmRaizRemessaArquivo) {
		this.nmRaizRemessaArquivo = nmRaizRemessaArquivo;
	}
	public String getNmRaizRemessaSiape() {
		return nmRaizRemessaSiape;
	}
	public void setNmRaizRemessaSiape(String nmRaizRemessaSiape) {
		this.nmRaizRemessaSiape = nmRaizRemessaSiape;
	}
	public String getNmRaizRetornoArquivo() {
		return nmRaizRetornoArquivo;
	}
	public void setNmRaizRetornoArquivo(String nmRaizRetornoArquivo) {
		this.nmRaizRetornoArquivo = nmRaizRetornoArquivo;
	}
	public String getNmSeparador() {
		return nmSeparador;
	}
	public void setNmSeparador(String nmSeparador) {
		this.nmSeparador = nmSeparador;
	}
	public Integer getQtdDiasRetornoBancoBrasil() {
		return qtdDiasRetornoBancoBrasil;
	}
	public void setQtdDiasRetornoBancoBrasil(Integer qtdDiasRetornoBancoBrasil) {
		this.qtdDiasRetornoBancoBrasil = qtdDiasRetornoBancoBrasil;
	}
	public float getVlMultaAtraso() {
		return vlMultaAtraso;
	}
	public void setVlMultaAtraso(float vlMultaAtraso) {
		this.vlMultaAtraso = vlMultaAtraso;
	}
	public float getVlTaxaJuros() {
		return vlTaxaJuros;
	}
	public void setVlTaxaJuros(float vlTaxaJuros) {
		this.vlTaxaJuros = vlTaxaJuros;
	}
	
	public String getNmPathArquivoFuncionario() {
		return nmPathArquivoFuncionario;
	}
	
	public void setNmPathArquivoFuncionario(String nmPathArquivoFuncionario) {
		this.nmPathArquivoFuncionario = nmPathArquivoFuncionario;
	}
	public String getNmPathArquivoDespesa() {
		return nmPathArquivoDespesa;
	}
	public void setNmPathArquivoDespesa(String nmPathArquivoDespesa) {
		this.nmPathArquivoDespesa = nmPathArquivoDespesa;
	}
}