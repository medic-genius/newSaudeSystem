package br.com.medic.medicsystem.persistence.model;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

public class BoletoVencido {

	private Date dtvencimento;

	private Double vlmensalidade;

	private String nmcliente;

	private String nrtelefone;

	private String nrcelular;
	
	private Long idCliente;
	
	private Long idContrato;
	
	private Long idMensalidade;

	public BoletoVencido() {
		// TODO Auto-generated constructor stub
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public Double getVlmensalidade() {
		return vlmensalidade;
	}

	public void setVlmensalidade(Double vlmensalidade) {
		this.vlmensalidade = vlmensalidade;
	}

	public String getNmcliente() {
		return nmcliente;
	}

	public void setNmcliente(String nmcliente) {
		this.nmcliente = nmcliente;
	}

	public String getNrtelefone() {
		return nrtelefone;
	}

	public void setNrtelefone(String nrtelefone) {
		this.nrtelefone = nrtelefone;
	}

	public String getNrcelular() {
		return nrcelular;
	}

	public void setNrcelular(String nrcelular) {
		this.nrcelular = nrcelular;
	}

	public String getValorMensalidadeFormatado() {

		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		return currencyFormatter.format(getVlmensalidade());

	}

	public String getDataVencimentoFormatado() {

		Locale locale = new Locale("pt", "BR");
		DateFormat dateFormatter = DateFormat.getDateInstance(
		        DateFormat.MEDIUM, locale);

		return dateFormatter.format(getDtvencimento());

	}
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdMensalidade() {
		return idMensalidade;
	}

	public void setIdMensalidade(Long idMensalidade) {
		this.idMensalidade = idMensalidade;
	}
}
