package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.RegistroPonto;

public class RegistroPontoDTO {

	Long idFuncionario;
	String nmFuncionario;
	Date dtRegistro;
	List<RegistroPonto> listRegistros;
	
	public List<RegistroPonto> getListRegistros() {
		return listRegistros;
	}

	public void setListRegistros(List<RegistroPonto> listRegistros) {
		this.listRegistros = listRegistros;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}
	
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}
	
	public Date getDtRegistro() {
		return dtRegistro;
	}
	
	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}
			
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	
}
