package br.com.medic.medicsystem.persistence.security;

import java.security.Principal;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.security.auth.Subject;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;

import org.jboss.logging.Logger;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.model.Funcionario;

@Stateless
public class SecurityService {

	@Inject
	private Logger logger;

	@Resource
	private SessionContext context;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;

	public String getUserLogin() {

		try {
			Subject userSubject = (Subject) PolicyContext.getContext("javax.security.auth.Subject.container");

			@SuppressWarnings("rawtypes")
			Set<KeycloakPrincipal> principals = userSubject.getPrincipals(KeycloakPrincipal.class);
			for (KeycloakPrincipal<?> keycloakPrincipal : principals) {
				return keycloakPrincipal.getKeycloakSecurityContext().getToken().getPreferredUsername();
			}

		} catch (Exception e) {

			return null;
		}

		return null;

	}

	public String getUserToken() {

		try {
			Subject userSubject = (Subject) PolicyContext.getContext("javax.security.auth.Subject.container");

			@SuppressWarnings("rawtypes")
			Set<KeycloakPrincipal> principals = userSubject.getPrincipals(KeycloakPrincipal.class);
			for (KeycloakPrincipal<?> keycloakPrincipal : principals) {
				return keycloakPrincipal.getKeycloakSecurityContext().getTokenString();
			}

		} catch (PolicyContextException e) {

			return null;
		}

		return null;

	}

	public String getUserId() {
		return context.getCallerPrincipal().getName();
	}

	public Funcionario getFuncionarioByLogin(String login) {
		return funcionarioDAO.getFuncionarioPorLogin(login);
	}

	public boolean userInRole(final String role) {
		return false;// context.isCallerInRole(role);
	}

	public Funcionario getFuncionarioLogado() {
		String user = getUserLogin();
		try {

			return getFuncionarioByLogin(user);
		} catch (NoResultException e) {
			logger.warn("Nao foi possivel encontrar funcionario correspondente ao usuario logado: " + user);
		}
		return null;
	}

}
