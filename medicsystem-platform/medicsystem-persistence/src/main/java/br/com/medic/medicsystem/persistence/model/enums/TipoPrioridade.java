package br.com.medic.medicsystem.persistence.model.enums;

public enum TipoPrioridade {
	NORMAL(0), 
	PRIORIDADE(1),
	PRIORIDADE_80ANOS(2);
	
	private Integer id;

	private TipoPrioridade(Integer id){
		this.id = id;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
