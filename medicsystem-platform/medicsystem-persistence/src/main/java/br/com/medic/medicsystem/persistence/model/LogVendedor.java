package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "vendas", name = "log_vendedor")
public class LogVendedor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOG_VENDEDOR_ID_SEQ")
	@SequenceGenerator(name = "LOG_VENDEDOR_ID_SEQ", sequenceName = "vendas.log_vendedor_id_seq", allocationSize = 1)
	@Column(name = "id")
	private Integer id;
	
	@Basic
	@Column(name="id_vendedor")
	private Integer idVendedor;
	
	@Basic
	@Column(name="data_log")
	private Date dataLog;
	
	@Basic
	@Column(name="json_str")
	private String jsonStr;
	
	@Basic
	@Column(name="vindi_customer_id")
	private Integer vindiCustomerId;
	
	@Basic
	@Column(name="vindi_payment_profile_id")
	private Integer vindiPaymentProfileId;
	
	@Basic
	@Column(name="vindi_product_id")
	private Integer vindiProductId;
	
	@Basic
	@Column(name="vindi_subscription_id")
	private Integer vindiSubscriptionId;
	
	@Basic
	@Column(name="vindi_gateway_response_code")
	private String vindiGatewayResponseCode;
	
	@Basic
	@Column(name="vindi_gateway_message")
	private String vindiGatewayMessage;
	
	@Basic
	@Column(name="type")
	private String type;
	
	@Basic
	@Column(name="movement")
	private String movement;
	
	@Basic
	@Column(name="latitude")
	private String latitude;
	
	@Basic
	@Column(name="longitude")
	private String longitude;
	
	@Basic
	@Column(name="success")
	private Integer success;
	
	@Basic
	@Column(name="cpf_consulta")
	private String cpfConsulta;
	
	@Basic
	@Column(name="id_empresa")
	private Integer idEmpresa;
	
	@Basic
	@Column(name="id_unidade")
	private Integer idUnidade;
	
	@Basic
	@Column(name="id_plano")
	private Integer idPlano;
	
	@Basic
	@Column(name="quantidade")
	private Integer quantidade;
	
	@Basic
	@Column(name="cartao_nome_cliente")
	private String cartaoNomeCliente;
	
	@Basic
	@Column(name="cartao_numero")
	private String cartaoNumero;
	
	@Basic
	@Column(name="cartao_cvv")
	private String cartaoCvv;
	
	@Basic
	@Column(name="cartao_vencimento")
	private String cartaoVencimento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdVendedor() {
		return idVendedor;
	}

	public void setIdVendedor(Integer idVendedor) {
		this.idVendedor = idVendedor;
	}

	public Date getDataLog() {
		return dataLog;
	}

	public void setDataLog(Date dataLog) {
		this.dataLog = dataLog;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public Integer getVindiCustomerId() {
		return vindiCustomerId;
	}

	public void setVindiCustomerId(Integer vindiCustomerId) {
		this.vindiCustomerId = vindiCustomerId;
	}

	public Integer getVindiPaymentProfileId() {
		return vindiPaymentProfileId;
	}

	public void setVindiPaymentProfileId(Integer vindiPaymentProfileId) {
		this.vindiPaymentProfileId = vindiPaymentProfileId;
	}

	public Integer getVindiProductId() {
		return vindiProductId;
	}

	public void setVindiProductId(Integer vindiProductId) {
		this.vindiProductId = vindiProductId;
	}

	public Integer getVindiSubscriptionId() {
		return vindiSubscriptionId;
	}

	public void setVindiSubscriptionId(Integer vindiSubscriptionId) {
		this.vindiSubscriptionId = vindiSubscriptionId;
	}

	public String getVindiGatewayResponseCode() {
		return vindiGatewayResponseCode;
	}

	public void setVindiGatewayResponseCode(String vindiGatewayResponseCode) {
		this.vindiGatewayResponseCode = vindiGatewayResponseCode;
	}

	public String getVindiGatewayMessage() {
		return vindiGatewayMessage;
	}

	public void setVindiGatewayMessage(String vindiGatewayMessage) {
		this.vindiGatewayMessage = vindiGatewayMessage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMovement() {
		return movement;
	}

	public void setMovement(String movement) {
		this.movement = movement;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}

	public String getCpfConsulta() {
		return cpfConsulta;
	}

	public void setCpfConsulta(String cpfConsulta) {
		this.cpfConsulta = cpfConsulta;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Integer getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Integer idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Integer getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Integer idPlano) {
		this.idPlano = idPlano;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getCartaoNomeCliente() {
		return cartaoNomeCliente;
	}

	public void setCartaoNomeCliente(String cartaoNomeCliente) {
		this.cartaoNomeCliente = cartaoNomeCliente;
	}

	public String getCartaoNumero() {
		return cartaoNumero;
	}

	public void setCartaoNumero(String cartaoNumero) {
		this.cartaoNumero = cartaoNumero;
	}

	public String getCartaoCvv() {
		return cartaoCvv;
	}

	public void setCartaoCvv(String cartaoCvv) {
		this.cartaoCvv = cartaoCvv;
	}

	public String getCartaoVencimento() {
		return cartaoVencimento;
	}

	public void setCartaoVencimento(String cartaoVencimento) {
		this.cartaoVencimento = cartaoVencimento;
	}
}
