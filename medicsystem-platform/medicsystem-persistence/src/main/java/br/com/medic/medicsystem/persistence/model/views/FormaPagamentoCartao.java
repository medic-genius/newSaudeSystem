package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "Caixa_PagamentoCartao_View")
public class FormaPagamentoCartao {
	
	@Id
	@Column(name = "idCaixa")
	private Long idCaixa;

	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;

	@Basic
	@Column(name = "inadministradoracartaocredito")
	private Integer inAdministradoraCartaoCredito;
	
	@Basic
	@Column(name = "inadministradoracartaodebito")
	private Integer inAdministradoraCartaoDebito;
	
	@Basic
	@Column(name = "vlmovdetalhamento")
	private Float vlMovdetalhamento;

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Integer getInAdministradoraCartaoCredito() {
		return inAdministradoraCartaoCredito;
	}

	public void setInAdministradoraCartaoCredito(Integer inAdministradoraCartaoCredito) {
		this.inAdministradoraCartaoCredito = inAdministradoraCartaoCredito;
	}

	public Integer getInAdministradoraCartaoDebito() {
		return inAdministradoraCartaoDebito;
	}

	public void setInAdministradoraCartaoDebito(Integer inAdministradoraCartaoDebito) {
		this.inAdministradoraCartaoDebito = inAdministradoraCartaoDebito;
	}

	public Float getVlMovdetalhamento() {
		return vlMovdetalhamento;
	}

	public void setVlMovdetalhamento(Float vlMovdetalhamento) {
		this.vlMovdetalhamento = vlMovdetalhamento;
	}
	
	

}
