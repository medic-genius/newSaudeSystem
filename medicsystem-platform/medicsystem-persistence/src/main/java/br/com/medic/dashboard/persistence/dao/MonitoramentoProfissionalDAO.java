package br.com.medic.dashboard.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import br.com.medic.dashboard.persistence.dataaccess.MLRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.MonitoramentoProfissional;

@Named("ml-monitoramento-profissional-dao")
@ApplicationScoped
public class MonitoramentoProfissionalDAO extends MLRelationalDataAccessObject<MonitoramentoProfissional>{

	@SuppressWarnings("unchecked")
	public List<MonitoramentoProfissional> getMonitorProfissional(Integer tolerancia) {
		
		String query =
		"select t1.nmfuncionario as funcionario, t1.nmespecialidade as especialidade, t1.hrinicio as hrinicioatendimento, t1.hrfim as hrfimatendimento, t1.nrcelular, t1.nmapelido as unidade, t1.nmbloqueio as bloqueio, t1.firsttimeponto, t1.lasttimeponto, t1.firsttimechamada,"
		+ " case"
		+ " when current_time >= hrinicio and current_time > hrinicio + interval '10 min' and firsttimechamada is null and nmbloqueio is null then 0" // variavel
		+ " else 1"
		+ " end as status"
		+ " from ("
		+ " select f.idfuncionario, f.nmfuncionario, replace(f.nrcelular,'-','') as nrcelular, b.nmbloqueio, u.nmapelido, esp.nmespecialidade, atdprof.hrinicio, atdprof.hrfim,"
		+ " min(rp.hrregistro) as firsttimeponto, max(rp.hrregistro) as lasttimeponto, min(rcp.hrchamada) as firsttimechamada"
		+ " from realvida.tbfuncionario f"
		+ " inner join realvida.tbprofissional p on f.idfuncionario = p.idfuncionario"
		+ " inner join realvida.tbatendimentoprofissional atdprof on f.idfuncionario = atdprof.idfuncionario"
		+ " inner join realvida.tbunidade u on atdprof.idunidade = u.idunidade"
		+ " inner join realvida.tbespprofissional espprof on atdprof.idespprofissional = espprof.idespprofissional"
		+ " inner join realvida.tbespecialidade esp on esp.idespecialidade = espprof.idespecialidade"
		+ " left join realvida.tbbloqueio b on f.idfuncionario = b.idfuncionario and current_date between b.dtinicio and b.dtfim and dtexclusaobloqueio is null"
		+ " left join realvida.tbregistroponto rp on f.idfuncionario = rp.idfuncionario and rp.dtregistro = current_date and u.idunidade = rp.idpontounidade"
		+ " left join realvida.tbregistrochamadaprofissional rcp on f.idfuncionario = rcp.idfuncionario and rcp.dtchamada = current_date and u.idunidade = rcp.idunidade"
		+ " where f.dtexclusao is null"
		+ " and atdprof.indiasemana = EXTRACT( DOW FROM current_date)"
		+ " and atdprof.hrinicio between current_time - interval '2 hours' and current_time + interval '2 hours'" //variavel
		+ " and atdprof.hrfim > current_time - interval '15 min'" //variavel
		+ " and f.nmfuncionario not like '%DR.%'"
		+ " and esp.idespecialidade <> 83" //Estetica
		+ " group by f.idfuncionario, b.nmbloqueio, atdprof.hrinicio, atdprof.hrfim, u.nmapelido, esp.nmespecialidade"
		+ " order by f.nmfuncionario, atdprof.hrinicio"
		+ ")t1";
		
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("funcionario", StringType.INSTANCE)
		        .addScalar("especialidade", StringType.INSTANCE)	
		        .addScalar("hrinicioatendimento", TimestampType.INSTANCE)
		        .addScalar("hrfimatendimento", TimestampType.INSTANCE)
		        .addScalar("nrCelular", StringType.INSTANCE)
		        .addScalar("unidade", StringType.INSTANCE)
		        .addScalar("bloqueio", StringType.INSTANCE)
		        .addScalar("firsttimeponto", TimestampType.INSTANCE)
		        .addScalar("lasttimeponto", TimestampType.INSTANCE)
		        .addScalar("firsttimechamada", TimestampType.INSTANCE)
		        .addScalar("status", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(MonitoramentoProfissional.class))
		        .list();
		
				
	}	
	
}
