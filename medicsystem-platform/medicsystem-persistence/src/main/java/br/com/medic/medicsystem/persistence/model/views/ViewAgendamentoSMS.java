package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(catalog = "realvida", name = "viewagendamentossms")
public class ViewAgendamentoSMS {
	
	@Id
	@Column(name = "idagendamento")
	private Long idAgendamento;
	
	@Basic
	@Column(name = "idCliente")
	private Long idCliente;
	
	@Basic
	@Column(name = "nmpaciente")
	private String nmPaciente;
	
	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;
	
	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "nmapelido")
	private String nmapelido;
	
	@Basic
	@Column(name = "dtagendamento")
	private Date dtAgendamento;
	
	@Basic
	@Column(name = "linkpreparatorio")
	private String linkPreparatorio;
	
	@Basic
	@Column(name = "hriniciofim")
	private String hrInicioFim;
	
	@JsonProperty
	@Transient
	private String hrOrdemInicio;
	
	@JsonProperty
	@Transient
	private String hrOrdemFim;
		
	
	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	
	// adicionado para log
	public Long getIdCliente() {
		return idCliente;
	}
	
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmapelido() {
		return nmapelido;
	}

	public void setNmapelido(String nmapelido) {
		this.nmapelido = nmapelido;
	}

	public Date getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public String getLinkPreparatorio() {
		return linkPreparatorio;
	}

	public void setLinkPreparatorio(String linkPreparatorio) {
		this.linkPreparatorio = linkPreparatorio;
	}
	
	public String getHrInicioFim() {
		return hrInicioFim;
	}

	public void setHrInicioFim(String hrInicioFim) {
		this.hrInicioFim = hrInicioFim;
	}

	public String getHrOrdemInicio() {
		
		String[] split;
		if(hrInicioFim != null){
			split = hrInicioFim.split("-");
			hrOrdemInicio = split[0];
		}
		
		return hrOrdemInicio;
	}

	public void setHrOrdemInicio(String hrOrdemInicio) {
		this.hrOrdemInicio = hrOrdemInicio;
	}

	public String getHrOrdemFim() {
		
		String[] split;
		if(hrInicioFim != null){
			split = hrInicioFim.split("-");
			hrOrdemFim = split[1];
		}
		
		return hrOrdemFim;
	}

	public void setHrOrdemFim(String hrOrdemFim) {
		this.hrOrdemFim = hrOrdemFim;
	}

	

}
