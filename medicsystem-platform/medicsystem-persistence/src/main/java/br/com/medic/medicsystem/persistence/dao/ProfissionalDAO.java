package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Profissional;

@Named("profissional-dao")
@ApplicationScoped
public class ProfissionalDAO extends RelationalDataAccessObject<Profissional> {
	
	//@Inject
	//private Logger logger;

}
