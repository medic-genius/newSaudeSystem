package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidade que representa a unidade financeiro
 * pertencente a empresa financeiro
 * 
 * @author Joelton
 * 
 * @since 28/03/2016
 * 
 * @version 1.3
 *
 */

@Entity
@Table(catalog = "financeiro", name = "tbunidadefinanceiro")
public class UnidadeFinanceiro implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UNIDADEFINANCEIRO_ID_SEQ")
	@SequenceGenerator(name = "UNIDADEFINANCEIRO_ID_SEQ", sequenceName = "financeiro.unidadefinanceiro_id_seq")
	@Column(name = "idunidadefinanceiro")
	private Long idUnidadeFinanceiro;
	
	@Basic
	@Column(name = "nmunidade")
	private String nmUnidade;

	@Basic
	@Column(name = "nmnickname")
	private String nmNickName;
	
	@ManyToOne
	@JoinColumn(name = "idempresafinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;

//GETTERS AND SETTERS
	
	public Long getIdUnidadeFinanceiro() {
		return idUnidadeFinanceiro;
	}

	public void setIdUnidadeFinanceiro(Long idUnidadeFinanceiro) {
		this.idUnidadeFinanceiro = idUnidadeFinanceiro;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public String getNmNickName() {
		return nmNickName;
	}

	public void setNmNickName(String nmNickName) {
		this.nmNickName = nmNickName;
	}

	public EmpresaFinanceiro getEmpresaFinanceiro() {
		return empresaFinanceiro;
	}

	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}
	
	
	
	
	
}
