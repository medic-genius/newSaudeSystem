package br.com.medic.medicsystem.persistence.model.superlogica;

import java.io.Serializable;

public class Cobranca  implements Serializable {
	
	private Integer id_sacado_sac;
	
	private ProdutoServicoGeneric produtoServicoGeneric;
	
	private String dt_vencimento_recb ;
	
	private Integer id_conta_cb;
	
	private Integer id_formapagamento_recb;
	
	private String st_nossonumerofixo_recb;
	
	private Integer in_idCliente;
	
	private Integer in_CodCliente;
	
	private String st_NomeCliente;
	
	private Long idContrato;
	
	private String st_observacaoexterna_recb; 
	
	private String endereco; 
	
	private String enderecoComplemento; 

	public Integer getId_sacado_sac() {
		return id_sacado_sac;
	}

	public void setId_sacado_sac(Integer id_sacado_sac) {
		this.id_sacado_sac = id_sacado_sac;
	}

	public ProdutoServicoGeneric getProdutoServicoGeneric() {
		return produtoServicoGeneric;
	}

	public void setProdutoServicoGeneric(ProdutoServicoGeneric produtoServicoGeneric) {
		this.produtoServicoGeneric = produtoServicoGeneric;
	}

	public String getDt_vencimento_recb() {
		return dt_vencimento_recb;
	}

	public void setDt_vencimento_recb(String dt_vencimento_recb) {
		this.dt_vencimento_recb = dt_vencimento_recb;
	}

	public Integer getId_conta_cb() {
		return id_conta_cb;
	}

	public void setId_conta_cb(Integer id_conta_cb) {
		this.id_conta_cb = id_conta_cb;
	}

	public Integer getId_formapagamento_recb() {
		return id_formapagamento_recb;
	}

	public void setId_formapagamento_recb(Integer id_formapagamento_recb) {
		this.id_formapagamento_recb = id_formapagamento_recb;
	}

	public String getSt_nossonumerofixo_recb() {
		return st_nossonumerofixo_recb;
	}

	public void setSt_nossonumerofixo_recb(String st_nossonumerofixo_recb) {
		this.st_nossonumerofixo_recb = st_nossonumerofixo_recb;
	}

	public Integer getIn_idCliente() {
		return in_idCliente;
	}

	public void setIn_idCliente(Integer in_idCliente) {
		this.in_idCliente = in_idCliente;
	}

	public Integer getIn_CodCliente() {
		return in_CodCliente;
	}

	public void setIn_CodCliente(Integer in_CodCliente) {
		this.in_CodCliente = in_CodCliente;
	}

	public String getSt_NomeCliente() {
		return st_NomeCliente;
	}

	public void setSt_NomeCliente(String st_NomeCliente) {
		this.st_NomeCliente = st_NomeCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getSt_observacaoexterna_recb() {
		return st_observacaoexterna_recb;
	}

	public void setSt_observacaoexterna_recb(String st_observacaoexterna_recb) {
		this.st_observacaoexterna_recb = st_observacaoexterna_recb;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEnderecoComplemento() {
		return enderecoComplemento;
	}

	public void setEnderecoComplemento(String enderecoComplemento) {
		this.enderecoComplemento = enderecoComplemento;
	}
	
	
	

}
