package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "mensalidade_boleto_imprimir")
public class MensalidadeBoletoImprimirView {
	
	@Id
	@Column(name = "idmensalidade")
	private Long id;

	@Basic
	@Column(name = "nrmensalidade")
	private String nrMensalidade;
	
	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name = "vlmensalidade")
	private Double vlMensalidade;
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "idcliente")
	private Long idCliente;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;

	@Basic
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name = "dtcontrato")
	private Date dtContrato;
		
	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name = "nrtalao")
	private String nrTalao;
	
	@Basic
	@Column(name = "nmplano")
	private String nmPlano;
	
	@Transient
	@JsonIgnore
	private String situacaoFormatado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Date getDtContrato() {
		return dtContrato;
	}

	public void setDtContrato(Date dtContrato) {
		this.dtContrato = dtContrato;
	}

	public String getNrMensalidade() {
		return nrMensalidade;
	}

	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNrTalao() {
		return nrTalao;
	}

	public void setNrTalao(String nrTalao) {
		this.nrTalao = nrTalao;
	}

	public Double getVlMensalidade() {
		return vlMensalidade;
	}

	public void setVlMensalidade(Double vlMensalidade) {
		this.vlMensalidade = vlMensalidade;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	@JsonProperty
	public String getSituacaoFormatado() {
		if (getInStatus() != null) {
			if (getInStatus() == 0) {
				return "Em Aberto";
			} else {
				if (getInStatus() == 1) {
					return "Quitado";
				} else {
					return "-";
				}
			}
		}
		return "-";
	}

	public void setSituacaoFormatado(String situacaoFormatado) {
		this.situacaoFormatado = situacaoFormatado;
	}

}
