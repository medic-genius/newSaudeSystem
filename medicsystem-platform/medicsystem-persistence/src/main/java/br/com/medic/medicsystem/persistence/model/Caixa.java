package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tbcaixa database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbcaixa")
public class Caixa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAIXA_ID_SEQ")
	@SequenceGenerator(name = "CAIXA_ID_SEQ", sequenceName = "realvida.caixa_id_seq", allocationSize = 1)
	@Column(name = "idcaixa")
	private Long idcaixa;

	@Temporal(TemporalType.DATE)
	private Date dtcaixa;

	@Temporal(TemporalType.DATE)
	private Date dtconferencia;

	private Timestamp dtinclusaolog;
	
	/** 0 - Aberto 1 - Fechado . Pode aceitar Nulo, sendo este seu estado inicial.*/
	private Integer instatus;

	private String nmcaixa;

	private String nmobservacaoconferencia;
	
	private Timestamp dtatualizacaolog;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idconferente")
	private Funcionario tbfuncionario1;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idoperador")
	private Funcionario tbfuncionario2;

	//bi-directional many-to-one association to Tbunidade
	@ManyToOne
	@JoinColumn(name="idunidade")
	private Unidade unidade;

	//bi-directional many-to-one association to Tbmovimentacaocaixa
	@OneToMany(mappedBy="caixa")
	private List<MovimentacaoCaixa> tbmovimentacaocaixas;

	public Caixa() {
	}

	public Long getIdcaixa() {
		return this.idcaixa;
	}

	public void setIdcaixa(Long idcaixa) {
		this.idcaixa = idcaixa;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtcaixa() {
		return this.dtcaixa;
	}

	public void setDtcaixa(Date dtcaixa) {
		this.dtcaixa = dtcaixa;
	}

	public Date getDtconferencia() {
		return this.dtconferencia;
	}

	public void setDtconferencia(Date dtconferencia) {
		this.dtconferencia = dtconferencia;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Integer getInstatus() {
		return this.instatus;
	}

	public void setInstatus(Integer instatus) {
		this.instatus = instatus;
	}

	public String getNmcaixa() {
		return this.nmcaixa;
	}

	public void setNmcaixa(String nmcaixa) {
		this.nmcaixa = nmcaixa;
	}

	public String getNmobservacaoconferencia() {
		return this.nmobservacaoconferencia;
	}

	public void setNmobservacaoconferencia(String nmobservacaoconferencia) {
		this.nmobservacaoconferencia = nmobservacaoconferencia;
	}

	public Funcionario getTbfuncionario1() {
		return this.tbfuncionario1;
	}

	public void setTbfuncionario1(Funcionario tbfuncionario1) {
		this.tbfuncionario1 = tbfuncionario1;
	}

	public Funcionario getTbfuncionario2() {
		return this.tbfuncionario2;
	}

	public void setTbfuncionario2(Funcionario tbfuncionario2) {
		this.tbfuncionario2 = tbfuncionario2;
	}

	public Unidade getTbunidade() {
		return this.unidade;
	}

	public void setUnidade(Unidade tbunidade) {
		this.unidade = tbunidade;
	}

	public List<MovimentacaoCaixa> getTbmovimentacaocaixas() {
		return this.tbmovimentacaocaixas;
	}

	public void setTbmovimentacaocaixas(List<MovimentacaoCaixa> tbmovimentacaocaixas) {
		this.tbmovimentacaocaixas = tbmovimentacaocaixas;
	}

	public MovimentacaoCaixa addTbmovimentacaocaixa(MovimentacaoCaixa tbmovimentacaocaixa) {
		getTbmovimentacaocaixas().add(tbmovimentacaocaixa);
		tbmovimentacaocaixa.setCaixa(this);

		return tbmovimentacaocaixa;
	}

	public MovimentacaoCaixa removeTbmovimentacaocaixa(MovimentacaoCaixa tbmovimentacaocaixa) {
		getTbmovimentacaocaixas().remove(tbmovimentacaocaixa);
		tbmovimentacaocaixa.setCaixa(null);

		return tbmovimentacaocaixa;
	}

}