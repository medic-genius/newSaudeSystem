package br.com.medic.medicsystem.persistence.dto;

public class LaudoAtendimentoDTO {
	
	private String nmMedico;
	
	private String nmPaciente;
	
	private String crm;
	
	private Integer idade;
	
	private String dataExame;
	
	private String especialidade;
	
	private String conteudo;
	
	private String assinaturaDigital;

	public String getNmMedico() {
		return nmMedico;
	}

	public void setNmMedico(String nmMedico) {
		this.nmMedico = nmMedico;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getDataExame() {
		return dataExame;
	}

	public void setDataExame(String dataExame) {
		this.dataExame = dataExame;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public String getAssinaturaDigital() {
		return assinaturaDigital;
	}

	public void setAssinaturaDigital(String assinaturaDigital) {
		this.assinaturaDigital = assinaturaDigital;
	}


}
