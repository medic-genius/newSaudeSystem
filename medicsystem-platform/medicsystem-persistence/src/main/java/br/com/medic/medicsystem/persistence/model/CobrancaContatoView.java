package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(catalog = "realvida", name = "tbcobranca_mp_contato_view")
public class CobrancaContatoView implements Serializable{

	private static final long serialVersionUID = -5099319423870931866L;
	
	@Id
	@Column(name = "idcliente")
	private Long id;
	
	@Column(name = "nmcliente")
	private String nmCliente;
	
	@Column(name = "nrcpf")
	private String nrCpf;
	
	@Column(name = "nrtelefone")
	private String nrTelefone;
	
	@Column(name = "nrcelular")
	private String nrCelular;
	
	@Column(name = "idempresagrupo")
	private Long idEmpresaGrupo;
	
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Column(name = "informapagamento")
	private Integer inFormaDePagamento;

	@Column(name = "intipo")
	private String inTipo;
	
	@Column(name = "nrtitulo")
	private String nrTitulo;

	@Column(name = "dtvencimento")
	private Date dataVencimento;

	@Column(name = "inadimplente")
	private Integer inInadimplente;
	
	@Column(name = "vltotal")
	private Double valorDivida;

	@Column(name = "vljuros")
	private Double valorJuros;
	
	@Column(name = "vlmultaatraso")
	private Double valorMultaAtraso;
	
	@Column(name = "prevpagamento")
	private Date dtPrevisaoPagamento;
	
	@Column(name = "proxligacao")
	private Date dtProximaLigacao;
	
	@Column(name = "ultimocontato")
	private String ultimoContato;
	
	@Transient
	private Long qdteDevido;
	
	@Transient
	private Long qtdeDespesa;
	
	@Transient
	private Long qtdeMensalidade;
	
	@Transient
	private Double somaTotalDividas;

	@Transient
	private Double somaTotalJuros;
	
	@Transient
	private Double somaTotalMultas;
	
	public CobrancaContatoView() {
	}
	
	public CobrancaContatoView(Long id, String nmCliente, String nrCpf, String nrTelefone, String nrCelular, String ultimoContato, 
			Long qdteDevido, Double somaTotalDividas, Double somaTotalJuros, Double somaTotalMultas, Long qtdeMensalidade, 
			Long qtdeDespesa) {
		this.id = id;
		this.nmCliente = nmCliente;
		this.nrCpf = nrCpf;
		this.nrTelefone = nrTelefone;
		this.nrCelular = nrCelular;
		this.ultimoContato = ultimoContato;
		this.qdteDevido = qdteDevido;
		this.somaTotalDividas = somaTotalDividas;
		this.somaTotalJuros = somaTotalJuros;
		this.somaTotalMultas = somaTotalMultas;
		this.qtdeMensalidade = qtdeMensalidade;
		this.qtdeDespesa = qtdeDespesa;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public Integer getInFormaDePagamento() {
		return inFormaDePagamento;
	}

	public void setInFormaDePagamento(Integer inFormaDePagamento) {
		this.inFormaDePagamento = inFormaDePagamento;
	}

	public Double getValorDivida() {
		return valorDivida;
	}

	public void setValorDivida(Double valorDivida) {
		this.valorDivida = valorDivida;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Double getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) {
		this.valorJuros = valorJuros;
	}

	public Double getValorMultaAtraso() {
		return valorMultaAtraso;
	}

	public void setValorMultaAtraso(Double valorMultaAtraso) {
		this.valorMultaAtraso = valorMultaAtraso;
	}

	public Date getDtPrevisaoPagamento() {
		return dtPrevisaoPagamento;
	}

	public void setDtPrevisaoPagamento(Date dtPrevisaoPagamento) {
		this.dtPrevisaoPagamento = dtPrevisaoPagamento;
	}

	public Long getQdteDevido() {
		return qdteDevido;
	}

	public void setQdteDevido(Long qdteDevido) {
		this.qdteDevido = qdteDevido;
	}

	public Double getSomaTotalDividas() {
		return somaTotalDividas;
	}

	public void setSomaTotalDividas(Double somaTotalDividas) {
		this.somaTotalDividas = somaTotalDividas;
	}

	public Double getSomaTotalJuros() {
		return somaTotalJuros;
	}

	public void setSomaTotalJuros(Double somaTotalJuros) {
		this.somaTotalJuros = somaTotalJuros;
	}

	public Double getSomaTotalMultas() {
		return somaTotalMultas;
	}

	public void setSomaTotalMultas(Double somaTotalMultas) {
		this.somaTotalMultas = somaTotalMultas;
	}

	public Date getDtProximaLigacao() {
		return dtProximaLigacao;
	}

	public void setDtProximaLigacao(Date dtProximaLigacao) {
		this.dtProximaLigacao = dtProximaLigacao;
	}

	public Integer getInInadimplente() {
		return inInadimplente;
	}

	public void setInInadimplente(Integer inInadimplente) {
		this.inInadimplente = inInadimplente;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public String getInTipo() {
		return inTipo;
	}

	public void setInTipo(String inTipo) {
		this.inTipo = inTipo;
	}

	public Long getQtdeDespesa() {
		return qtdeDespesa;
	}

	public void setQtdeDespesa(Long qtdeDespesa) {
		this.qtdeDespesa = qtdeDespesa;
	}

	public Long getQtdeMensalidade() {
		return qtdeMensalidade;
	}

	public void setQtdeMensalidade(Long qtdeMensalidade) {
		this.qtdeMensalidade = qtdeMensalidade;
	}
	
	public String getUltimoContato() {
		return ultimoContato;
	}

	public void setUltimoContato(String ultimoContato) {
		this.ultimoContato = ultimoContato;
	}
	
}
