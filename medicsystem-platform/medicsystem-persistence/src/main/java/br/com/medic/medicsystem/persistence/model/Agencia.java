package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbagencia")
public class Agencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGENCIA_ID_SEQ")
	@SequenceGenerator(name = "AGENCIA_ID_SEQ", sequenceName = "realvida.agencia_id_seq", allocationSize = 1)
	@Column(name = "idagencia")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idagenciaaux")
	private Long idAgenciaAux;

	@Basic
	@Column(name = "idagenciaodonto")
	private Long idAgenciaOdonto;

	@Basic
	@Column(name = "nmagencia")
	private String nmAgencia;

	@Basic
	@Column(name = "nragencia")
	private String nrAgencia;

	@Basic
	@Column(name = "nrdigitoverificador")
	private String nrDigitoVerificador;

	@ManyToOne
	@JoinColumn(name = "idbanco")
	private Banco banco;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	public Agencia() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdAgenciaAux() {
		return idAgenciaAux;
	}

	public void setIdAgenciaAux(Long idAgenciaAux) {
		this.idAgenciaAux = idAgenciaAux;
	}

	public Long getIdAgenciaOdonto() {
		return idAgenciaOdonto;
	}

	public void setIdAgenciaOdonto(Long idAgenciaOdonto) {
		this.idAgenciaOdonto = idAgenciaOdonto;
	}

	public String getNmAgencia() {
		return nmAgencia;
	}

	public void setNmAgencia(String nmAgencia) {
		this.nmAgencia = nmAgencia;
	}

	public String getNrAgencia() {
		return nrAgencia;
	}

	public void setNrAgencia(String nrAgencia) {
		this.nrAgencia = nrAgencia;
	}

	public String getNrDigitoVerificador() {
		return nrDigitoVerificador;
	}

	public void setNrDigitoVerificador(String nrDigitoVerificador) {
		this.nrDigitoVerificador = nrDigitoVerificador;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
}