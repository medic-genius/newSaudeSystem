package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Cidade;

@Named("cidade-dao")
@ApplicationScoped
public class CidadeDAO extends RelationalDataAccessObject<Cidade> {

	public List<Cidade> getAllCidades() {

		TypedQuery<Cidade> query = entityManager.createQuery("SELECT c FROM Cidade c order by c.nmCidade asc",
				Cidade.class);
		List<Cidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Cidade getCidade(Long idCidade) {
		return searchByKey(Cidade.class, idCidade);
	}

	public Cidade getCidadeByNmCidadeAndUf(String nmCidade, String nmUF) {
		Query query = entityManager.createQuery("SELECT cidade FROM Cidade cidade"
				+ " WHERE lower(cidade.nmCidade) = lower(:nmCidade)" 
				+ " and lower(cidade.nmUF) = lower(:nmUF)");
		try {
			query.setMaxResults(1);
			query.setParameter("nmCidade", nmCidade);
			query.setParameter("nmUF", nmUF);
			Cidade cidade = (Cidade) query.getSingleResult();
			return cidade;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
