package br.com.medic.medicsystem.persistence.model.enums;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FormaPagamentoContratoEnum {
	FORMA_PAGAMENTO_BOLETO(0, "BOLETO BANCÁRIO"),
	FORMA_PAGAMENTO_DEBITO_AUTOMATICO(1, "DÉBITO AUTOMÁTICO"),
	FORMA_PAGAMENTO_CONTRA_CHEQUE(2, "CONTRA CHEQUE"),
	FORMA_PAGAMENTO_CARTAO_RECORRENTE(30, "CARTÃO RECORRENTE");
	
	private Integer id;
	private String description;

	private FormaPagamentoContratoEnum(int ordinal, String name) {
		this.id = ordinal;
		this.description = name;
	}

	public static FormaPagamentoContratoEnum getTipoContatoById(int id){
		for (FormaPagamentoContratoEnum forma : FormaPagamentoContratoEnum.values()) {
			if(id == forma.id)
				return forma;
		}
		return null;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static List<FormaPagamentoContratoEnum> getFormaPagamento(){
		List<FormaPagamentoContratoEnum> list = new ArrayList<FormaPagamentoContratoEnum>();
		for (FormaPagamentoContratoEnum status : FormaPagamentoContratoEnum.values()) {
			list.add(status);
		}
		return list;
	}
	
}
