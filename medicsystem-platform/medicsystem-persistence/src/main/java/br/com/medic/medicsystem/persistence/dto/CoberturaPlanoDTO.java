package br.com.medic.medicsystem.persistence.dto;

public class CoberturaPlanoDTO {
	
	private String servico;
	
	private String codigo;
	
	private String tipo;
	
	private Double valorAssociado;
	
	private Double valorNaoAssociado;
	
	private Integer carencia;
	
	private String nmPlano;
	
	private String nroContrato;
	
	private Boolean boCobertura;
	
	private Integer validadeContratual;
	
	public CoberturaPlanoDTO() {
		
	}

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Double getValorAssociado() {
		return valorAssociado;
	}

	public void setValorAssociado(Double valorAssociado) {
		this.valorAssociado = valorAssociado;
	}

	public Double getValorNaoAssociado() {
		return valorNaoAssociado;
	}

	public void setValorNaoAssociado(Double valorNaoAssociado) {
		this.valorNaoAssociado = valorNaoAssociado;
	}

	public Integer getCarencia() {
		return carencia;
	}

	public void setCarencia(Integer carencia) {
		this.carencia = carencia;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public Boolean getBoCobertura() {
		return boCobertura;
	}

	public void setBoCobertura(Boolean boCobertura) {
		this.boCobertura = boCobertura;
	}

	
	public String getBoCoberturaFormatada() {
		
		if(getBoCobertura() != null && getBoCobertura())
			return "SIM";
		else
			return "NÃO";
	}

	public Integer getValidadeContratual() {
		return validadeContratual;
	}

	public void setValidadeContratual(Integer validadeContratual) {
		this.validadeContratual = validadeContratual;
	}
	
	
	
}
