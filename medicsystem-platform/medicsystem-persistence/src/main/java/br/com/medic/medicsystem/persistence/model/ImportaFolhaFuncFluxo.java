package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidade que representa os fluxos de descontos/proventos
 * de um funcionario(ImportaFolhaFunc)
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */
@Entity
@Table(catalog = "realvida", name = "tbimportafolhafuncfluxo")
public class ImportaFolhaFuncFluxo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORTAFOLHAFUNCFLX_ID_SEQ")
	@SequenceGenerator(name = "IMPORTAFOLHAFUNCFLX_ID_SEQ", catalog = "realvida", sequenceName = "IMPORTAFOLHAFUNCFLX_ID_SEQ", allocationSize = 1)
	@Column(name = "idfluxo")
	private Long id;

	private String codigo;

	private String descricao;

	private double vltotal;

	private double vlunitario;
	
	private String tipo;

	// bi-directional many-to-one association to ImportaFolhaFunc
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "idfunc")
	private ImportaFolhaFunc tbimportafolhafunc;

	public ImportaFolhaFuncFluxo() {
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getVltotal() {
		return this.vltotal;
	}

	public void setVltotal(double vltotal) {
		this.vltotal = vltotal;
	}

	public double getVlunitario() {
		return this.vlunitario;
	}

	public void setVlunitario(double vlunitario) {
		this.vlunitario = vlunitario;
	}

	public ImportaFolhaFunc getTbimportafolhafunc() {
		return this.tbimportafolhafunc;
	}

	public void setTbimportafolhafunc(ImportaFolhaFunc tbimportafolhafunc) {
		this.tbimportafolhafunc = tbimportafolhafunc;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}