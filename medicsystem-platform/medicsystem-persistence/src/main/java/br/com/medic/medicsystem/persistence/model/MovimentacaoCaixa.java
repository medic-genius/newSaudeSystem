package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(catalog = "realvida", name = "tbmovimentacaocaixa")
public class MovimentacaoCaixa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOVIMENTACAOCAIXA_ID_SEQ")
	@SequenceGenerator(name = "MOVIMENTACAOCAIXA_ID_SEQ", sequenceName = "realvida.movimentacaocaixa_id_seq")
	@Column(name = "idmovimentacaocaixa")
	private Long id;
	
	@Basic
	@Column(name = "boexcluido")
	private Boolean boExcluido;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtmovimentacaocaixa")
	private Date dtMovimentacaoCaixa;
	
	@Basic
	@Column(name = "idmovimentacaocaixaestorno")
	private Long idMovimentacaoCaixaEstorno;
	
	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;
	
	@Basic
	@Column(name = "nmmovimentacaocaixa")
	private String nmMovimentacaoCaixa;
	
	@Basic
	@Column(name = "vljuros")
	private double vlJuros;
	
	@Basic
	@Column(name = "vlmovimentacaocaixa")
	private float vlMovimentacaoCaixa;
	
	@Basic
	@Column(name = "vlmulta")
	private double vlMulta;

	//bi-directional many-to-one association to Tbcaixa
	@ManyToOne
	@JoinColumn(name="idcaixa")
	private Caixa caixa;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionariomovimentacao")
	private Funcionario funcionario;

	//bi-directional many-to-one association to Tbtipoentsaicaixa
	@ManyToOne
	@JoinColumn(name="idtipoentsaicaixa")
	private TipoEntSaiCaixa tipoEntSaiCaixa;

	//bi-directional many-to-one association to Tbmovimentacaoitem
	@OneToMany(mappedBy="tbmovimentacaocaixa")
	private List<MovimentacaoItem> movimentacaoItems;

	//bi-directional many-to-one association to Tbmovimentacaopagto
	@OneToMany(mappedBy="tbmovimentacaocaixa")
	private List<MovimentacaoPagto> movimentacaoPagtos;

	public MovimentacaoCaixa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoExcluido() {
		return boExcluido;
	}

	public void setBoExcluido(Boolean boExcluido) {
		this.boExcluido = boExcluido;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtMovimentacaoCaixa() {
		return dtMovimentacaoCaixa;
	}

	public void setDtMovimentacaoCaixa(Date dtMovimentacaoCaixa) {
		this.dtMovimentacaoCaixa = dtMovimentacaoCaixa;
	}

	public Long getIdMovimentacaoCaixaEstorno() {
		return idMovimentacaoCaixaEstorno;
	}

	public void setIdMovimentacaoCaixaEstorno(Long idMovimentacaoCaixaEstorno) {
		this.idMovimentacaoCaixaEstorno = idMovimentacaoCaixaEstorno;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public String getNmMovimentacaoCaixa() {
		return nmMovimentacaoCaixa;
	}

	public void setNmMovimentacaoCaixa(String nmMovimentacaoCaixa) {
		this.nmMovimentacaoCaixa = nmMovimentacaoCaixa;
	}

	public double getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(double vlJuros) {
		this.vlJuros = vlJuros;
	}

	public float getVlMovimentacaoCaixa() {
		return vlMovimentacaoCaixa;
	}

	public void setVlMovimentacaoCaixa(float vlMovimentacaoCaixa) {
		this.vlMovimentacaoCaixa = vlMovimentacaoCaixa;
	}

	public double getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(double vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public TipoEntSaiCaixa getTipoEntSaiCaixa() {
		return tipoEntSaiCaixa;
	}

	public void setTipoEntSaiCaixa(TipoEntSaiCaixa tipoEntSaiCaixa) {
		this.tipoEntSaiCaixa = tipoEntSaiCaixa;
	}

	public List<MovimentacaoItem> getMovimentacaoItems() {
		return movimentacaoItems;
	}

	public void setMovimentacaoItems(List<MovimentacaoItem> movimentacaoItems) {
		this.movimentacaoItems = movimentacaoItems;
	}

	public List<MovimentacaoPagto> getMovimentacaoPagtos() {
		return movimentacaoPagtos;
	}

	public void setMovimentacaoPagtos(List<MovimentacaoPagto> movimentacaoPagtos) {
		this.movimentacaoPagtos = movimentacaoPagtos;
	}

	public MovimentacaoItem addMovimentacaoItem(MovimentacaoItem movimentacaoItem) {
		getMovimentacaoItems().add(movimentacaoItem);
		movimentacaoItem.setMovimentacaoCaixa(this);

		return movimentacaoItem;
	}

	public MovimentacaoItem removeTbmovimentacaoitem(MovimentacaoItem movimentacaoItem) {
		getMovimentacaoItems().remove(movimentacaoItem);
		movimentacaoItem.setMovimentacaoCaixa(null);

		return movimentacaoItem;
	}

	public MovimentacaoPagto addMovimentacaoPagto(MovimentacaoPagto movimentacaoPagto) {
		getMovimentacaoPagtos().add(movimentacaoPagto);
		movimentacaoPagto.setMovimentacaoCaixa(this);

		return movimentacaoPagto;
	}

	public MovimentacaoPagto removeTbmovimentacaopagto(MovimentacaoPagto movimentacaoPagto) {
		getMovimentacaoPagtos().remove(movimentacaoPagto);
		movimentacaoPagto.setMovimentacaoCaixa(null);

		return movimentacaoPagto;
	}

}