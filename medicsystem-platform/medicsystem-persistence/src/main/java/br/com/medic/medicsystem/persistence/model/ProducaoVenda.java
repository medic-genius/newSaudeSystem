package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the tbproducaovenda database table.
 * 
 */
@Entity
public class ProducaoVenda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUCAOVENDA_ID_SEQ")
	@SequenceGenerator(name = "PRODUCAOVENDA_ID_SEQ", sequenceName = "realvida.producaovenda_id_seq", allocationSize = 1 )
	@Column(name = "idproducaovenda")		
	private Long id;
	
	//@Temporal(TemporalType.DATE)
	@Basic
	@Column(name = "dtproducao")
	private Date dtProducao;
	
	private Integer inOperacao;
	
	private Integer inFormaPagamento;
	
	private Integer inDescontado;
	
	private Integer inSituacao;
	
	private Double vlPlano;
	
	private Double pcProducao;
	
	private Double vlProducao;
		
	//bi-directional many-to-one association to Tbcontrato
	@ManyToOne
	@JoinColumn(name="idcontrato")
	private Contrato tbcontrato;
	
	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idvendedor")
	private Funcionario funcionario1;
	
	//bi-directional many-to-one association to Tbplano
	@ManyToOne
	@JoinColumn(name="idplano")
	private Plano plano;
	
	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idoperadorsituacao")
	private Funcionario funcionario2;

	private String nrContratoAntigo;
	
	private Double vlContratoAntigo;
	
	private String nmPlanoAntigo;
	
	private Timestamp dtAtualizacaoLog;

	private Timestamp dtInclusaoLog;
	

	public ProducaoVenda() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtProducao() {
		return dtProducao;
	}

	public void setDtProducao(Date dtProducao) {
		this.dtProducao = dtProducao;
	}

	public Integer getInOperacao() {
		return inOperacao;
	}

	public void setInOperacao(Integer inOperacao) {
		this.inOperacao = inOperacao;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Integer getInDescontado() {
		return inDescontado;
	}

	public void setInDescontado(Integer inDescontado) {
		this.inDescontado = inDescontado;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public Double getVlPlano() {
		return vlPlano;
	}

	public void setVlPlano(Double vlPlano) {
		this.vlPlano = vlPlano;
	}

	public Double getPcProducao() {
		return pcProducao;
	}

	public void setPcProducao(Double pcProducao) {
		this.pcProducao = pcProducao;
	}

	public Double getVlProducao() {
		return vlProducao;
	}

	public void setVlProducao(Double vlProducao) {
		this.vlProducao = vlProducao;
	}
	
	public String getNrContratoAntigo() {
		return nrContratoAntigo;
	}

	public void setNrContratoAntigo(String nrContratoAntigo) {
		this.nrContratoAntigo = nrContratoAntigo;
	}

	public Double getVlContratoAntigo() {
		return vlContratoAntigo;
	}

	public void setVlContratoAntigo(Double vlContratoAntigo) {
		this.vlContratoAntigo = vlContratoAntigo;
	}

	public String getNmPlanoAntigo() {
		return nmPlanoAntigo;
	}

	public void setNmPlanoAntigo(String nmPlanoAntigo) {
		this.nmPlanoAntigo = nmPlanoAntigo;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	

}