package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbcoberturaplano")
public class CoberturaPlano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COBERTURAPLANO_ID_SEQ")
	@SequenceGenerator(name = "COBERTURAPLANO_ID_SEQ", sequenceName = "realvida.coberturaplano_id_seq")
	@Column(name = "idcoberturaplano")
	private Long id;
	
	@Basic
	@Column(name = "bocobertura")
	private Boolean boCobertura;
	
	@Basic
	@Column(name = "carencia")
	private Integer carencia;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "totalconsulta")
	private Integer totalConsulta;
	
	@Basic
	@Column(name = "vlservico")
	private Double vlServico;
	
	@Basic
	@Column(name = "vlservicoassociado")
	private Double vlServicoAssociado;

	@ManyToOne
	@JoinColumn(name="idplano")
	private Plano plano;

	@ManyToOne
	@JoinColumn(name="idservico")
	private Servico servico;
	
	@Basic
	@Column(name = "validadecontratual")
	private Integer validadeContratual;

	public CoberturaPlano() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoCobertura() {
		return boCobertura;
	}

	public void setBoCobertura(Boolean boCobertura) {
		this.boCobertura = boCobertura;
	}

	public Integer getCarencia() {
		return carencia;
	}

	public void setCarencia(Integer carencia) {
		this.carencia = carencia;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getTotalConsulta() {
		return totalConsulta;
	}

	public void setTotalConsulta(Integer totalConsulta) {
		this.totalConsulta = totalConsulta;
	}

	public Double getVlServico() {
		return vlServico;
	}

	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}

	public Double getVlServicoAssociado() {
		return vlServicoAssociado;
	}

	public void setVlServicoAssociado(Double vlServicoAssociado) {
		this.vlServicoAssociado = vlServicoAssociado;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Integer getValidadeContratual() {
		return validadeContratual;
	}

	public void setValidadeContratual(Integer validadeContratual) {
		this.validadeContratual = validadeContratual;
	}
	
	
}