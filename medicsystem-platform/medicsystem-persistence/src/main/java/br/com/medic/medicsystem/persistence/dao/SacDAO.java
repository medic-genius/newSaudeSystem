package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Sac;
import br.com.medic.medicsystem.persistence.model.enums.SituacaoSacCobranca;

@ApplicationScoped
@Named("sac-dao")
public class SacDAO extends RelationalDataAccessObject<Sac> {

	public List<Sac> getSacCobrancaListByCliente(Long idCliente){
		String querySql = "SELECT s FROM Sac s, Cobranca c where c.sac.id = s.id and s.cliente.id = " + idCliente + " order by s.data desc, s.hrFinal desc" ;
		
		TypedQuery<Sac> query = entityManager.createQuery(querySql,	Sac.class);
		List<Sac> result = query.getResultList();
	
		return result;
	}
	
	public Sac getSacCobrancaEmAbertoByCliente(Long idCliente){
		String querySql = "SELECT s FROM Sac s, Cobranca c where c.sac.id = s.id and s.cliente.id = " + idCliente + " and s.inSituacao = " + SituacaoSacCobranca.ABERTO.getId();
		Sac result = null;
		
		TypedQuery<Sac> query = entityManager.createQuery(querySql,	Sac.class);
		try{
			result = query.getSingleResult();
		}catch(NoResultException ex){
//			ex.printStackTrace();
		}
		return result;
	}
	
}
