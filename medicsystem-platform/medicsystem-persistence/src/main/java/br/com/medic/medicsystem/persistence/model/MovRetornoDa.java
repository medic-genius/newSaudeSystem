package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the tbmovretornoda database table.
 * 
 */
@Entity
public class MovRetornoDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBMOVRETORNODA_IDMOVRETORNODA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBMOVRETORNODA_IDMOVRETORNODA_GENERATOR")
	private Long idmovretornoda;

	private Timestamp dtatualizacaolog;

	@Temporal(TemporalType.DATE)
	private Date dtcredito;

	private Timestamp dtinclusaolog;

	@Temporal(TemporalType.DATE)
	private Date dtocorrencia;

	@Temporal(TemporalType.DATE)
	private Date dtvencimento;

	private Integer instatus;

	private Integer intratado;

	private String nmmotivoocorrencia1;

	private String nmmotivoocorrencia2;

	private String nmmotivoocorrencia3;

	private String nmmotivoocorrencia4;

	private String nmmotivoocorrencia5;

	private String nmocorrencia;

	private String nragencia;

	private String nrdocumento;

	private String nrmotivoocorrencia1;

	private String nrmotivoocorrencia2;

	private String nrmotivoocorrencia3;

	private String nrmotivoocorrencia4;

	private String nrmotivoocorrencia5;

	private String nrnossonumero;

	private String nrocorrencia;

	private Long nrsequencial;

	private String nrtitulobanco;

	private float vldebito;

	private float vljuros;

	private float vlpago;

	private float vltitulo;

	//bi-directional many-to-one association to Tbretornoda
	@ManyToOne
	@JoinColumn(name="idretornoda")
	private RetornoDa tbretornoda;

	public MovRetornoDa() {
	}

	public Long getIdmovretornoda() {
		return this.idmovretornoda;
	}

	public void setIdmovretornoda(Long idmovretornoda) {
		this.idmovretornoda = idmovretornoda;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Date getDtcredito() {
		return this.dtcredito;
	}

	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Date getDtocorrencia() {
		return this.dtocorrencia;
	}

	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}

	public Date getDtvencimento() {
		return this.dtvencimento;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public Integer getInstatus() {
		return this.instatus;
	}

	public void setInstatus(Integer instatus) {
		this.instatus = instatus;
	}

	public Integer getIntratado() {
		return this.intratado;
	}

	public void setIntratado(Integer intratado) {
		this.intratado = intratado;
	}

	public String getNmmotivoocorrencia1() {
		return this.nmmotivoocorrencia1;
	}

	public void setNmmotivoocorrencia1(String nmmotivoocorrencia1) {
		this.nmmotivoocorrencia1 = nmmotivoocorrencia1;
	}

	public String getNmmotivoocorrencia2() {
		return this.nmmotivoocorrencia2;
	}

	public void setNmmotivoocorrencia2(String nmmotivoocorrencia2) {
		this.nmmotivoocorrencia2 = nmmotivoocorrencia2;
	}

	public String getNmmotivoocorrencia3() {
		return this.nmmotivoocorrencia3;
	}

	public void setNmmotivoocorrencia3(String nmmotivoocorrencia3) {
		this.nmmotivoocorrencia3 = nmmotivoocorrencia3;
	}

	public String getNmmotivoocorrencia4() {
		return this.nmmotivoocorrencia4;
	}

	public void setNmmotivoocorrencia4(String nmmotivoocorrencia4) {
		this.nmmotivoocorrencia4 = nmmotivoocorrencia4;
	}

	public String getNmmotivoocorrencia5() {
		return this.nmmotivoocorrencia5;
	}

	public void setNmmotivoocorrencia5(String nmmotivoocorrencia5) {
		this.nmmotivoocorrencia5 = nmmotivoocorrencia5;
	}

	public String getNmocorrencia() {
		return this.nmocorrencia;
	}

	public void setNmocorrencia(String nmocorrencia) {
		this.nmocorrencia = nmocorrencia;
	}

	public String getNragencia() {
		return this.nragencia;
	}

	public void setNragencia(String nragencia) {
		this.nragencia = nragencia;
	}

	public String getNrdocumento() {
		return this.nrdocumento;
	}

	public void setNrdocumento(String nrdocumento) {
		this.nrdocumento = nrdocumento;
	}

	public String getNrmotivoocorrencia1() {
		return this.nrmotivoocorrencia1;
	}

	public void setNrmotivoocorrencia1(String nrmotivoocorrencia1) {
		this.nrmotivoocorrencia1 = nrmotivoocorrencia1;
	}

	public String getNrmotivoocorrencia2() {
		return this.nrmotivoocorrencia2;
	}

	public void setNrmotivoocorrencia2(String nrmotivoocorrencia2) {
		this.nrmotivoocorrencia2 = nrmotivoocorrencia2;
	}

	public String getNrmotivoocorrencia3() {
		return this.nrmotivoocorrencia3;
	}

	public void setNrmotivoocorrencia3(String nrmotivoocorrencia3) {
		this.nrmotivoocorrencia3 = nrmotivoocorrencia3;
	}

	public String getNrmotivoocorrencia4() {
		return this.nrmotivoocorrencia4;
	}

	public void setNrmotivoocorrencia4(String nrmotivoocorrencia4) {
		this.nrmotivoocorrencia4 = nrmotivoocorrencia4;
	}

	public String getNrmotivoocorrencia5() {
		return this.nrmotivoocorrencia5;
	}

	public void setNrmotivoocorrencia5(String nrmotivoocorrencia5) {
		this.nrmotivoocorrencia5 = nrmotivoocorrencia5;
	}

	public String getNrnossonumero() {
		return this.nrnossonumero;
	}

	public void setNrnossonumero(String nrnossonumero) {
		this.nrnossonumero = nrnossonumero;
	}

	public String getNrocorrencia() {
		return this.nrocorrencia;
	}

	public void setNrocorrencia(String nrocorrencia) {
		this.nrocorrencia = nrocorrencia;
	}

	public Long getNrsequencial() {
		return this.nrsequencial;
	}

	public void setNrsequencial(Long nrsequencial) {
		this.nrsequencial = nrsequencial;
	}

	public String getNrtitulobanco() {
		return this.nrtitulobanco;
	}

	public void setNrtitulobanco(String nrtitulobanco) {
		this.nrtitulobanco = nrtitulobanco;
	}

	public float getVldebito() {
		return this.vldebito;
	}

	public void setVldebito(float vldebito) {
		this.vldebito = vldebito;
	}

	public float getVljuros() {
		return this.vljuros;
	}

	public void setVljuros(float vljuros) {
		this.vljuros = vljuros;
	}

	public float getVlpago() {
		return this.vlpago;
	}

	public void setVlpago(float vlpago) {
		this.vlpago = vlpago;
	}

	public float getVltitulo() {
		return this.vltitulo;
	}

	public void setVltitulo(float vltitulo) {
		this.vltitulo = vltitulo;
	}

	public RetornoDa getTbretornoda() {
		return this.tbretornoda;
	}

	public void setRetornoDa(RetornoDa tbretornoda) {
		this.tbretornoda = tbretornoda;
	}

}