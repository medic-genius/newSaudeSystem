package br.com.medic.medicsystem.persistence.dto;

public class FaturamentoProfissionalDTO {
	
	private Double vlSalario;
	
	private String totalHr;
	
	private Integer totalServico; 
	
	private RegistroAgendamentoDTO RegistrosHorista;
	
	private RegistroAgendamentoDTO RegistrosFixo;
	
	private RegistroAgendamentoDTO RegistrosProducao;
	
	private RegistroAgendamentoDTO RespTecnico;

	public Double getVlSalario() {
		return vlSalario;
	}

	public void setVlSalario(Double vlSalario) {
		this.vlSalario = vlSalario;
	}

	public String getTotalHr() {
		return totalHr;
	}

	public void setTotalHr(String totalHr) {
		this.totalHr = totalHr;
	}

	public Integer getTotalServico() {
		return totalServico;
	}

	public void setTotalServico(Integer totalServico) {
		this.totalServico = totalServico;
	}

	public RegistroAgendamentoDTO getRegistrosHorista() {
		return RegistrosHorista;
	}

	public void setRegistrosHorista(RegistroAgendamentoDTO registrosHorista) {
		RegistrosHorista = registrosHorista;
	}

	public RegistroAgendamentoDTO getRegistrosFixo() {
		return RegistrosFixo;
	}

	public void setRegistrosFixo(RegistroAgendamentoDTO registrosFixo) {
		RegistrosFixo = registrosFixo;
	}

	public RegistroAgendamentoDTO getRegistrosProducao() {
		return RegistrosProducao;
	}

	public void setRegistrosProducao(RegistroAgendamentoDTO registrosProducao) {
		RegistrosProducao = registrosProducao;
	}

	public RegistroAgendamentoDTO getRespTecnico() {
		return RespTecnico;
	}

	public void setRespTecnico(RegistroAgendamentoDTO respTecnico) {
		RespTecnico = respTecnico;
	}

}
