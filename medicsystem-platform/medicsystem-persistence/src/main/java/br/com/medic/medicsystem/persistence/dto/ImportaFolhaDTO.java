package br.com.medic.medicsystem.persistence.dto;

import java.util.Collection;

import br.com.medic.medicsystem.persistence.model.ImportaFolha;

/**
 * Entidade que fornece informaçoes de um
 * folha de pagamento, bem como os funcionarios
 * que pertencem a esta.
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

public class ImportaFolhaDTO {
	
	private ImportaFolha oif;
	
	private Collection<?> funcs;
	
	public Collection<?> getFuncs() {
		return funcs;
	}
	
	public ImportaFolha getOif() {
		return oif;
	}
	
	public void setFuncs(Collection<?> funcs) {
		this.funcs = funcs;
	}
	
	public void setOif(ImportaFolha oif) {
		this.oif = oif;
	}

}
