package br.com.medic.dashboard.persistence.model;

public interface GraphWrapper {

	String getGraphLabel();
	
	Double getGraphValue();
}
