package br.com.medic.medicsystem.persistence.dto;


public class BoletosImprimirDTO {
	
	private Long idMensalidade;
	
	private Long idCliente;
	
	private Long idContrato;
	
	private String nmCliente;
	
	private String situacao;
	
	private String nrContrato;
	
	private String nrMensalidade;
	
	private String nmPlano;
	
	private String dtContrato;
	
	private String dtVencimento;
	
	private Double vlMensalidade;
	
	private String nrTalao;
	
	public Long getIdMensalidade() {
		return idMensalidade;
	}

	public void setIdMensalidade(Long idMensalidade) {
		this.idMensalidade = idMensalidade;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNrMensalidade() {
		return nrMensalidade;
	}

	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getDtContrato() {
		
		return dtContrato;
	}

	public void setDtContrato(String dtContrato) {
		this.dtContrato = dtContrato;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Double getVlMensalidade() {
		return vlMensalidade;
	}

	public void setVlMensalidade(Double vlMensalidade) {
		this.vlMensalidade = vlMensalidade;
	}

	public String getNrTalao() {
		return nrTalao;
	}

	public void setNrTalao(String nrTalao) {
		this.nrTalao = nrTalao;
	}

}
