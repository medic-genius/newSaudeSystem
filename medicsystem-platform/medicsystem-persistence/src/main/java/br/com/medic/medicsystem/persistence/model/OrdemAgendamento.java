package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tbordemagendamento")
public class OrdemAgendamento implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDEMAGENDAMENTO_ID_SEQ")
	@SequenceGenerator(name = "ORDEMAGENDAMENTO_ID_SEQ", catalog = "realvida", sequenceName = "ordemagendamento_id_seq", allocationSize = 1)
	@Column(name = "idordemagendamento")
	private Long id;
	
	@Basic
	@Column(name = "idagendamento")
	private Long idAgendamento;
	
	@Basic
	@Column(name = "idespecialidade")
	private Long idEspecialidade;
	
	@Basic
	@Column(name = "idfuncionario")
	private Long idFuncionario;
	
	@Basic
	@Column(name = "boprioridade")
	private Boolean boPrioridade;
	
	@Basic
	@Column(name = "nrordem")
	private Double nrOrdem;
	
	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "boexcluido")
	private Boolean boExcluido;
	
	@Basic
	@Column(name = "nrficha")
	private String nrFicha;
	
	@Basic
	@Column(name = "idatendimentoprofissional")
	private Long idAtendimentoProfissional;
	
	@Basic
	@Column(name = "intipoprioridade")
	private Integer inTipoPrioridade;
	
	@Basic
	@Column(name="instatusprioridade")
	private Integer inStatusPrioridade;
	
	@Basic
	@Column(name="obsurgencia")
	private String obsUrgencia;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Boolean getBoPrioridade() {
		return boPrioridade;
	}

	public void setBoPrioridade(Boolean boPrioridade) {
		this.boPrioridade = boPrioridade;
	}

	public Double getNrOrdem() {
		return nrOrdem;
	}

	public void setNrOrdem(Double nrOrdem) {
		this.nrOrdem = nrOrdem;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Boolean getBoExcluido() {
		return boExcluido;
	}

	public void setBoExcluido(Boolean boExcluido) {
		this.boExcluido = boExcluido;
	}

	public String getNrFicha() {
		return nrFicha;
	}

	public void setNrFicha(String nrFicha) {
		this.nrFicha = nrFicha;
	}

	public Long getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Long idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public Integer getInTipoPrioridade() {
		return inTipoPrioridade;
	}

	public void setInTipoPrioridade(Integer inTipoPrioridade) {
		this.inTipoPrioridade = inTipoPrioridade;
	}

	public Integer getInStatusPrioridade() {
		return inStatusPrioridade;
	}

	public void setInStatusPrioridade(Integer inStatusPrioridade) {
		this.inStatusPrioridade = inStatusPrioridade;
	}

	public String getObsUrgencia() {
		return obsUrgencia;
	}

	public void setObsUrgencia(String obsUrgencia) {
		this.obsUrgencia = obsUrgencia;
	}
	
}
