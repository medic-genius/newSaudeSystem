package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Anamnese;



@Named("anamnese-dao")
@ApplicationScoped
public class AnamneseDAO extends RelationalDataAccessObject<Anamnese>{

}
