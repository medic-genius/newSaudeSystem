package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataCoberturaDTO {
	
	private Date dataCobertura;
	private Date dataProximoServico;
	private Long idDespesa;
	private Integer validadeContratual;
	
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	
	
	public Date getDataCobertura() {
		return dataCobertura;
	}
	
	public void setDataCobertura(Date dataCobertura) {
		this.dataCobertura = dataCobertura;
	}
	
	public Date getDataProximoServico() {
		return dataProximoServico;
	}
	
	public void setDataProximoServico(Date dataProximoServico) {
		this.dataProximoServico = dataProximoServico;
	}
	
	public String getDataCoberturaFormatada() {
		if(dataCobertura != null){
			return dt.format(dataCobertura);
		}
		return null;
	}
	
	public String getDataProximoServicoFormatada() {
		if(dataProximoServico != null){
			return dt.format(dataProximoServico);
		}
		return null;
		
	}

	public Long getIdDespesa() {
		return idDespesa;
	}

	public void setIdDespesa(Long idDespesa) {
		this.idDespesa = idDespesa;
	}

	public Integer getValidadeContratual() {
		return validadeContratual;
	}

	public void setValidadeContratual(Integer validadeContratual) {
		this.validadeContratual = validadeContratual;
	}
	
	
	

}
