package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.ArquivoDespesaFinanceiro;

public class ArquivoDespesaFinanceiroDTO implements Serializable{

	private static final long serialVersionUID = 6966695426513095516L;
	
	private Long idDespesa;
	private List<ArquivoDespesaFinanceiro> arquivos;
	public Long getIdDespesa() {
		return idDespesa;
	}
	public void setIdDespesa(Long idDespesa) {
		this.idDespesa = idDespesa;
	}
	public List<ArquivoDespesaFinanceiro> getArquivos() {
		return arquivos;
	}
	public void setArquivos(List<ArquivoDespesaFinanceiro> arquivos) {
		this.arquivos = arquivos;
	}
}
