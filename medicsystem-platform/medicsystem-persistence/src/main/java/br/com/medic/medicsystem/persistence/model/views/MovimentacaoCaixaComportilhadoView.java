package br.com.medic.medicsystem.persistence.model.views;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.medic.medicsystem.persistence.model.enums.TipoOperacaoCaixa;

@Entity
@Table(catalog = "compartilhado", name = "movimentacao_caixa_compartilhado_view")
public class MovimentacaoCaixaComportilhadoView implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idMovimentacaoCaixa")
	private Long idMovimentacaoCaixa;
	
	@Basic
	@Column(name = "nmmovimentacaocaixa")
	private String nmMovimentacaoCaixa;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtmovimentacaocaixa")
	private Date dtMovimentacaoCaixa;
	
	@Basic
	@Column(name = "vlmovimentacaocaixa")
	private Float vlMovimentacaoCaixa;
	
	@Basic
	@Column(name = "vljuros")
	private Float vlJuros;
	
	@Basic
	@Column(name = "vlmulta")
	private Float vlMulta;
	
	@Basic
	@Column(name = "idfuncionariomovimentacao")
	private Long idFuncionarioMovimentacao;
	
	@Basic
	@Column(name = "nmfuncionariomovimentacao")
	private String nmFuncionarioMovimentacao;
	
	@Basic
	@Column(name = "boexcluido")
	private Boolean boExcluido;
	
	@Basic
	@Column(name = "idmovimentacaocaixaestorno")
	private Long idMovimentacaoCaixaEstorno;
	
	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;
	
	/**
	 *0 - Abertura
	 *1 - Troca de operador
	 *2 - Fechamento
	 *3 - Recebimento
	 *4 - Abastecimento
	 *5 - Retirada
	 *6 - Estorno de Pagamento
	 */
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "intipooperacao")
	private TipoOperacaoCaixa inTipoOperacao; 
	
	@Basic
	@Column(name = "nmtipoentsaicaixa")
	private String nmTipoEntSaiCaixa;
	
	/**
	 *0 - Entrada
	 *1 - Saida
	 */
	@Basic
	@Column(name = "insaidacaixa")
	private Integer inSaidaCaixa;
	
	@Basic
	@Column(name = "idcaixa")
	private Long idCaixa;

	public Long getIdMovimentacaoCaixa() {
		return idMovimentacaoCaixa;
	}

	public void setIdMovimentacaoCaixa(Long idMovimentacaoCaixa) {
		this.idMovimentacaoCaixa = idMovimentacaoCaixa;
	}

	public String getNmMovimentacaoCaixa() {
		return nmMovimentacaoCaixa;
	}

	public void setNmMovimentacaoCaixa(String nmMovimentacaoCaixa) {
		this.nmMovimentacaoCaixa = nmMovimentacaoCaixa;
	}

	public Date getDtMovimentacaoCaixa() {
		return dtMovimentacaoCaixa;
	}

	public void setDtMovimentacaoCaixa(Date dtMovimentacaoCaixa) {
		this.dtMovimentacaoCaixa = dtMovimentacaoCaixa;
	}

	public Float getVlMovimentacaoCaixa() {
		return vlMovimentacaoCaixa;
	}

	public void setVlMovimentacaoCaixa(Float vlMovimentacaoCaixa) {
		this.vlMovimentacaoCaixa = vlMovimentacaoCaixa;
	}

	public Float getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(Float vlJuros) {
		this.vlJuros = vlJuros;
	}

	public Float getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(Float vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Long getIdFuncionarioMovimentacao() {
		return idFuncionarioMovimentacao;
	}

	public void setIdFuncionarioMovimentacao(Long idFuncionarioMovimentacao) {
		this.idFuncionarioMovimentacao = idFuncionarioMovimentacao;
	}

	public String getNmFuncionarioMovimentacao() {
		return nmFuncionarioMovimentacao;
	}

	public void setNmFuncionarioMovimentacao(String nmFuncionarioMovimentacao) {
		this.nmFuncionarioMovimentacao = nmFuncionarioMovimentacao;
	}

	/*public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}
*/
	public Boolean getBoExcluido() {
		return boExcluido;
	}

	public void setBoExcluido(Boolean boExcluido) {
		this.boExcluido = boExcluido;
	}

	public Long getIdMovimentacaoCaixaEstorno() {
		return idMovimentacaoCaixaEstorno;
	}

	public void setIdMovimentacaoCaixaEstorno(Long idMovimentacaoCaixaEstorno) {
		this.idMovimentacaoCaixaEstorno = idMovimentacaoCaixaEstorno;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public TipoOperacaoCaixa getInTipoOperacao() {
		return inTipoOperacao;
	}

	public void setInTipoOperacao(TipoOperacaoCaixa inTipoOperacao) {
		this.inTipoOperacao = inTipoOperacao;
	}

	public String getNmTipoEntSaiCaixa() {
		return nmTipoEntSaiCaixa;
	}

	public void setNmTipoEntSaiCaixa(String nmTipoEntSaiCaixa) {
		this.nmTipoEntSaiCaixa = nmTipoEntSaiCaixa;
	}

	public Integer getInSaidaCaixa() {
		return inSaidaCaixa;
	}

	public void setInSaidaCaixa(Integer inSaidaCaixa) {
		this.inSaidaCaixa = inSaidaCaixa;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	} 

}
