package br.com.medic.medicsystem.persistence.model;



public class Foto  {
	
	private Long idCliente;
	private byte[] btFoto;
	private byte[] foto;
	private String fotoDecoded;
	private String fotoEncoded;
	private String nmPaciente;
	
	
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public byte[] getBtFoto() {
		return btFoto;
	}
	public void setBtFoto(byte[] btFoto) {
		this.btFoto = btFoto;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	
	public String getFotoDecoded() {
		if (getFoto() != null) {
			// Base64.encodeBase64String(getFotoCliente());
			return new String(getFoto());
		}
		return null;
	}
	
	public void setFotoDecoded(String fotoDecoded) {
		this.fotoDecoded = fotoDecoded;
	}
	public String getFotoEncoded() {
		return fotoEncoded;
	}
	public void setFotoEncoded(String fotoEncoded) {
		this.fotoEncoded = fotoEncoded;
	}
	public String getNmPaciente() {
		return nmPaciente;
	}
	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	
	
	
}
