package br.com.medic.medicsystem.persistence.security.keycloak;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provê métodos de acesso às funcionalidades básicas oferecidas pelo Keycloak (servidor de autenticação)
 * para gerenciamento de usuários.
 * 
 * @author Patrick Lima
 *
 */
public class KeycloakUsers {
	@Inject
	private KeycloakAccessProvider kcAccess;

	public String createUser(String username, String firstName, 
			String lastName, String email, String password) {
		if(email != null && email.isEmpty()) {
			email = null;
		}

		Map<String, Object> newUser = new HashMap<String, Object>();
		newUser.put("username", username);
		newUser.put("firstName", firstName);
		newUser.put("lastName", lastName);
		newUser.put("email", email);
		newUser.put("enabled", true);

		try {
			ObjectMapper mapper = new ObjectMapper();
			String entityStr = mapper.writeValueAsString(newUser);

			String realmName = kcAccess.getRealmName();
			String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users";

			HttpPost req = new HttpPost(uri);
			req.setEntity(new StringEntity(entityStr));
			CloseableHttpResponse response = kcAccess.sendRequest(req);
			if(response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
				Header[] headers = response.getHeaders("Location");
				String location = headers[0].getValue();
				String[] locationArr = location.split("/");
				String newId = locationArr[locationArr.length - 1];
				
				// definir senha
				boolean isComplete = password == null || this.setUserPassword(newId, password);
				if(isComplete) {
					return newId;
				} else {
					this.deleteUserById(newId);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean deleteUserById(String userId) {
		String realmName = kcAccess.getRealmName();
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users/" + userId;
		HttpDelete req = new HttpDelete(uri);
		try {
			kcAccess.sendRequest(req);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> searchCliente(String username, String email) {
		String realmName = kcAccess.getRealmName();
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users";
		URIBuilder uriBuilder = new URIBuilder();
		uriBuilder.setPath(uri);
		if(username != null) {
			uriBuilder.setParameter("username", username);
		}
		if(email != null) {
			uriBuilder.setParameter("email", email);
		}
		try {
			HttpGet request = new HttpGet();
			request.setURI(uriBuilder.build());
			
			CloseableHttpResponse response = kcAccess.sendRequest(request);
			ObjectMapper mapper = new ObjectMapper();
			List<Object> list = mapper.readValue(response.getEntity().getContent(), List.class);
			if(list != null && !list.isEmpty()) {
				String strObj = mapper.writeValueAsString(list.get(0));
				return mapper.readValue(strObj, Map.class);
			}
		} catch (Exception e) {
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String searchClienteString(String username, String email) {
		String realmName = kcAccess.getRealmName();
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users";
		URIBuilder uriBuilder = new URIBuilder();
		uriBuilder.setPath(uri);
		if(username != null) {
			uriBuilder.setParameter("username", username);
		}
		if(email != null) {
			uriBuilder.setParameter("email", email);
		}
		try {
			HttpGet request = new HttpGet();
			request.setURI(uriBuilder.build());
			
			CloseableHttpResponse response = kcAccess.sendRequest(request);
			ObjectMapper mapper = new ObjectMapper();
			List<Object> list = mapper.readValue(response.getEntity().getContent(), List.class);
			if(list != null && !list.isEmpty()) {
				String strObj = mapper.writeValueAsString(list);
				return strObj;
			}
		} catch (Exception e) {
			
		}
		return null;
	}
	/*
	 * unificar com a função searchCliente
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> searchCliente(String username, String email, String id) {
		String realmName = kcAccess.getRealmName();
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users"+(id != null ? "/"+id : "");
		URIBuilder uriBuilder = new URIBuilder();
		uriBuilder.setPath(uri);
		
		if(id == null){
			if(username != null) {
				uriBuilder.setParameter("username", username);
			}
			if(email != null) {
				uriBuilder.setParameter("email", email);
			}
		}

		try {
			HttpGet request = new HttpGet();
			request.setURI(uriBuilder.build());
			
			CloseableHttpResponse response = kcAccess.sendRequest(request);
			ObjectMapper mapper = new ObjectMapper();
			List<Object> list = mapper.readValue(response.getEntity().getContent(), List.class);
			if(list != null && !list.isEmpty()) {
				String strObj = mapper.writeValueAsString(list.get(0));
				return mapper.readValue(strObj, Map.class);
			}
		} catch (Exception e) {
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String updateClienteString(String id, String email) {
		/*
		 * Caso queira alterar mais campos, criar uma classe.
		 */
		
		String realmName = kcAccess.getRealmName();
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users/"+id;
		URIBuilder uriBuilder = new URIBuilder();
		uriBuilder.setPath(uri);
		
		try {
			
		Map<String, Object> newUser = new HashMap<String, Object>();
		newUser.put("email", email);
		ObjectMapper mapperObj = new ObjectMapper();
		String entityStr = mapperObj.writeValueAsString(newUser);
		
		
	
			HttpPut request = new HttpPut(uri);
			request.setURI(uriBuilder.build());
			request.setEntity(new StringEntity(entityStr));
			
			CloseableHttpResponse response = kcAccess.sendRequest(request);
			ObjectMapper mapper = new ObjectMapper();
			List<Object> list = mapper.readValue(response.getEntity().getContent(), List.class);
			if(list != null && !list.isEmpty()) {
				String strObj = mapper.writeValueAsString(list);
				return strObj;
			}
		} catch (Exception e) {
			
		}
		return null;
	}
	
	public Boolean requestResetPasswordEmail(String userId) {
		String realmName = kcAccess.getRealmName();
		
		// uri versão atual
//		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users/" + 
//		userId + "/execute-actions-email";
		
		// uri versão legada (medic)
		String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users/" + 
				userId + "/reset-password-email";
		HttpPut request = new HttpPut(uri);
		try {
			String action = "[\"UPDATE_PASSWORD\"]";
			request.setEntity(new StringEntity(action));
			CloseableHttpResponse response = kcAccess.sendRequest(request);
			if(response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return true;
			}
		} catch(Exception e) {
			
		}
		return false;
	}
	
	/**
	 * Atualiza senha de usuário no servidor Keycloak
	 * 
	 * @param kcUserId identificador do usuário no servidor keycloak
	 * @param password senha que será definida para o usuário
	 * @return true em caso de sucesso. Do contrário false.
	 */
	private Boolean setUserPassword(String kcUserId, String password) {
		Map<String, Object> credential = new HashMap<String, Object>();
		credential.put("type", "password");
		credential.put("value", password);
		credential.put("temporary", false);
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String entityStr = mapper.writeValueAsString(credential);

			String realmName = kcAccess.getRealmName();
			String uri = kcAccess.getAuthServerBaseUrl() + "/admin/realms/" + realmName + "/users/" + kcUserId + "/reset-password";

			HttpPut req = new HttpPut(uri);
			req.setEntity(new StringEntity(entityStr));
			CloseableHttpResponse response = kcAccess.sendRequest(req);
			return (response != null) && (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
