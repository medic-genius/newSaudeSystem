package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Receituario;
import br.com.medic.medicsystem.persistence.model.ServicoOrcamentoExame;



@Named("servicoorcamentoexame-dao")
@ApplicationScoped
public class ServicoOrcamentoExameDAO  extends RelationalDataAccessObject<ServicoOrcamentoExame>{
	
	public List<ServicoOrcamentoExame> getServicoExamePorOrcamento(Long idOrcamento){
		
		
		String query = "SELECT soe FROM ServicoOrcamentoExame soe WHERE soe.orcamentoExame.id = "+idOrcamento;
		
		TypedQuery<ServicoOrcamentoExame> queryStr = entityManager.createQuery(query,
				ServicoOrcamentoExame.class);
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
public boolean deleteServicoOrcamentoExame(List<ServicoOrcamentoExame> listServOrcExameOriginal){
	boolean removido = false;
		
	for (ServicoOrcamentoExame servicoOrc : listServOrcExameOriginal) {
		try {
			this.remove(servicoOrc);
			removido = true;
		} catch (Exception e) {
			throw new ObjectNotFoundException(
					"Erro ao remover servicoOrçamento");
		}
		
	}
	
	entityManager.flush();
	
	return removido;
		
	}
	
	
	
	
	
//	public void removeServicoExamePorAtendimento(){
//		
//		
//	}

}
