package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbconferenciamensalidade_view")
public class ConferenciaMensalidade {
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Basic
	@Column(name = "idmensalidade")
	private String idMensalidade;
	
	@Basic
	@Column(name = "idcaixa")
	private Long idCaixa;
	
	@Basic
	@Column(name = "nrmensalidade")
	private String nrMensalidade;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;
		
	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name = "nmplano")
	private String nmPlano;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtvencimento")
	private String dtVencimento;
	
	@Basic
	@Column(name = "vlpago")
	private Float vlPago;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtmovimentacaocaixa")
	private Date dtMovimentacaoCaixa;
	
	@Basic
	@Column(name = "nmformapagamento")
	private String nmFormaPagamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdMensalidade() {
		return idMensalidade;
	}

	public void setIdMensalidade(String idMensalidade) {
		this.idMensalidade = idMensalidade;
	}

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	}

	public String getNrMensalidade() {
		return nrMensalidade;
	}

	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}
	
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Float getVlPago() {
		return vlPago;
	}

	public void setVlPago(Float vlPago) {
		this.vlPago = vlPago;
	}

	public Date getDtMovimentacaoCaixa() {
		return dtMovimentacaoCaixa;
	}

	public void setDtMovimentacaoCaixa(Date dtMovimentacaoCaixa) {
		this.dtMovimentacaoCaixa = dtMovimentacaoCaixa;
	}

	public String getNmFormaPagamento() {
		return nmFormaPagamento;
	}

	public void setNmFormaPagamento(String nmFormaPagamento) {
		this.nmFormaPagamento = nmFormaPagamento;
	}

}
