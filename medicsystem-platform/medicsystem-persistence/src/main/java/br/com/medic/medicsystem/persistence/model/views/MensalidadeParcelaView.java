package br.com.medic.medicsystem.persistence.model.views;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbmensalidades_tbparcelas_view")
public class MensalidadeParcelaView {
	
	@Id
	@Column(name = "iddespesa")
	private Long idDocumento;
		
	@Basic
	@Column(name = "txtipo")
	private String inTipo;
	
	@Basic
	@Column(name = "nrdespesa")
	private String nrDocumento;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;
	
	@Basic
	@Column(name = "txformapagamentocombinada")
	private String formaPagamentoCombinada;
	
	@Basic
	@Column(name = "bonegociada")
	private Boolean boNegociada;	
	
	@Basic
	@Column(name = "vltotal")
	private Double vlTotal;
	
	@Basic
	@Column(name = "vlcomjuros")
	private Double vlTotalJuros;
		
	@Basic
	@Column(name = "vlpago")
	private Double vlPago;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtpagamento")
	private Date dtPagamento;
	
	@Basic
	@Column(name = "txsituacao")
	private String nmSituacao;
	
	@Basic
	@Column(name = "inquitada")
	private Integer inQuitada;
	
	@Basic
	@Column(name = "txquitada")	
	private String nmQuitada;
	
	
	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;
	
	@Basic
	@Column(name = "dtprevisaopagto")
	private Date dtPrevisaoPagto;
	
	@Basic
	@Column(name = "observacao")	
	private String nmObservacao;
	
	
	@Basic
	@Column(name = "dtexclusao")
	private Timestamp dtExclusao;
	
	@Basic
	@Column(name = "boexcluida")
	private Boolean boExcluida;
	
	@Basic
	@Column(name = "boreparcelada")
	private Boolean boReparcelada;	
	
	
	@Basic
	@Column(name = "idcontrato")
	private Long idContrato;

	@Basic
	@Column(name = "idcontratocliente")
	private Long idContratoCliente;
	
	

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getInTipo() {
		return inTipo;
	}

	public void setInTipo(String inTipo) {
		this.inTipo = inTipo;
	}

	public String getNrDocumento() {
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public Date getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public String getFormaPagamentoCombinada() {
		return formaPagamentoCombinada;
	}

	public void setFormaPagamentoCombinada(String formaPagamentoCombinada) {
		this.formaPagamentoCombinada = formaPagamentoCombinada;
	}

	public Boolean getBoNegociada() {
		return boNegociada;
	}

	public void setBoNegociada(Boolean boNegociada) {
		this.boNegociada = boNegociada;
	}

	public Double getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Double vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Double getVlTotalJuros() {
		return vlTotalJuros;
	}

	public void setVlTotalJuros(Double vlTotalJuros) {
		this.vlTotalJuros = vlTotalJuros;
	}

	public Double getVlPago() {
		return vlPago;
	}

	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}

	public Date getDtPagamento() {
		return dtPagamento;
	}

	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	public String getNmSituacao() {
		return nmSituacao;
	}

	public void setNmSituacao(String nmSituacao) {
		this.nmSituacao = nmSituacao;
	}

	public Integer getInQuitada() {
		return inQuitada;
	}

	public void setInQuitada(Integer inQuitada) {
		this.inQuitada = inQuitada;
	}

	public String getNmQuitada() {
		return nmQuitada;
	}

	public void setNmQuitada(String nmQuitada) {
		this.nmQuitada = nmQuitada;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Date getDtPrevisaoPagto() {
		return dtPrevisaoPagto;
	}

	public void setDtPrevisaoPagto(Date dtPrevisaoPagto) {
		this.dtPrevisaoPagto = dtPrevisaoPagto;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Timestamp dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Boolean getBoExcluida() {
		return boExcluida;
	}

	public void setBoExcluida(Boolean boExcluida) {
		this.boExcluida = boExcluida;
	}

	public Boolean getBoReparcelada() {
		return boReparcelada;
	}

	public void setBoReparcelada(Boolean boReparcelada) {
		this.boReparcelada = boReparcelada;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdContratoCliente() {
		return idContratoCliente;
	}

	public void setIdContratoCliente(Long idContratoCliente) {
		this.idContratoCliente = idContratoCliente;
	}
		
}
