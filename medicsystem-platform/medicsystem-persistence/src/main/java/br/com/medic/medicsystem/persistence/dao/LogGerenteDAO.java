package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.LogGerente;

@Named("log-gerente-dao")
@ApplicationScoped
public class LogGerenteDAO extends RelationalDataAccessObject<LogGerente> {

	public LogGerente saveLogGerente(LogGerente lg) {
		return persist(lg);
	}

}
