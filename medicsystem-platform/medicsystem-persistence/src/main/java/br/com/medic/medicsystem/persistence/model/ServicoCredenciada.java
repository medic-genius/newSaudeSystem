package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbservicocredenciada")
public class ServicoCredenciada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICOCREDENCIADA_ID_SEQ")
	@SequenceGenerator(name = "SERVICOCREDENCIADA_ID_SEQ", sequenceName = "realvida.servicocredenciada_id_seq", allocationSize = 1)
	@Column(name = "idservicocredenciada")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "validade")
	private Integer validade;

	@Basic
	@Column(name = "vlcredenciada")
	private Double vlCredenciada;

	@Basic
	@Column(name = "vlcredenciadaassociado")
	private Double vlCredenciadaAssociado;

	@Basic
	@Column(name = "vlcusto")
	private Double vlCusto;

	@Basic
	@Column(name = "vlmargemassociado")
	private Double vlMargemAssociado;

	@Basic
	@Column(name = "vlmargemnaoassociado")
	private Double vlMargemNaoAssociado;

	// bi-directional many-to-one association to Tbcredenciada
	@ManyToOne
	@JoinColumn(name = "idcredenciada")
	private Credenciada credenciada;

	// bi-directional many-to-one association to Tbservico
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	public ServicoCredenciada() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getValidade() {
		return validade;
	}

	public void setValidade(Integer validade) {
		this.validade = validade;
	}

	public Double getVlCredenciada() {
		return vlCredenciada;
	}

	public void setVlCredenciada(Double vlCredenciada) {
		this.vlCredenciada = vlCredenciada;
	}

	public Double getVlCredenciadaAssociado() {
		return vlCredenciadaAssociado;
	}

	public void setVlCredenciadaAssociado(Double vlCredenciadaAssociado) {
		this.vlCredenciadaAssociado = vlCredenciadaAssociado;
	}

	public Double getVlCusto() {
		return vlCusto;
	}

	public void setVlCusto(Double vlCusto) {
		this.vlCusto = vlCusto;
	}

	public Double getVlMargemAssociado() {
		return vlMargemAssociado;
	}

	public void setVlMargemAssociado(Double vlMargemAssociado) {
		this.vlMargemAssociado = vlMargemAssociado;
	}

	public Double getVlMargemNaoAssociado() {
		return vlMargemNaoAssociado;
	}

	public void setVlMargemNaoAssociado(Double vlMargemNaoAssociado) {
		this.vlMargemNaoAssociado = vlMargemNaoAssociado;
	}

	public Credenciada getCredenciada() {
		return credenciada;
	}

	public void setCredenciada(Credenciada credenciada) {
		this.credenciada = credenciada;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

}