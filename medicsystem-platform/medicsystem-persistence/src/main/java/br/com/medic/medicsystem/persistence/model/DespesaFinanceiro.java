package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Entity
@Table(catalog = "financeiro", name = "tbdespesa")
@SecondaryTable(catalog="financeiro", name="vwdespesa", pkJoinColumns=@PrimaryKeyJoinColumn(name="iddespesa"))
public class DespesaFinanceiro implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESPESAFINANCEIRO_ID_SEQ")
	@SequenceGenerator(name = "DESPESAFINANCEIRO_ID_SEQ", sequenceName = "financeiro.despesafinanceiro_id_seq", allocationSize = 1)
	@Column(name = "iddespesa")
	private Long id;
	 
	@Basic
	@NotNull
	@Column(name = "intipodespesa")
	private Short inTipo;
	
	@Basic
	@Column(name = "nrordemparcela")
	private Integer nrOrdemParcela;
	
	@Basic
	@Column(name = "nrquantidadeparcelas")
	private Integer nrQuantidadeParcelas;
	
	@Basic
	@NotNull
	@Column(name = "nmdespesa", length=50)
	private String nmDespesa;
	
	@Basic
	@Column(name = "nrdocumento")
	private String nrDocumento;
	
	@Basic
	@Column(name = "nrServico")
	private String nrServico;
	
	@Basic
	@Column(name = "dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name = "dtpagamento")
	private Date dtPagamento;
	
	@Basic
	@Column(name = "vlPago")
	private Double vlPago;
	
	@Basic
	@Column(name = "vldespesa")
	private Double vlDespesa;
	
	@Basic
	@Column(name = "vlmulta")
	private Double vlMulta;
	
	@Basic
	@Column(name = "vljuros")
	private Double vlJuros;
	
	@Basic
	@Column(name = "vldesconto")
	private Double vlDesconto;
	
	@Basic
	@Column(name = "instatus")
	private Short inStatus;
	
	@Transient
	private Integer inSituacao;
	
	@Basic
	@Column(name = "nmobservacao", length=100)
	private String nmObservacao;
	
	@Basic
	@Column(name = "dtcompetencia")
	private Date dtCompetencia;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idempresafinanceirostart")
	private EmpresaFinanceiro empresaFinanceiroSolicitante;

	@ManyToOne
	@JoinColumn(name = "idempresafinanceiroend")
	private EmpresaFinanceiro empresaFinanceiroPagante;
	
	@ManyToOne
	@JoinColumn(name = "idcontabancaria")
	private ContaBancaria contaBancaria;
	
	@ManyToOne
	@JoinColumn(name = "idfornecedor")
	private Fornecedor fornecedor;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcontacontabil")
	private ContaContabil contaContabil;
	
	@ManyToOne
	@JoinColumn(name = "idcentrocusto")
	private CentroCusto centroCusto;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name ="informapagamento")
	private Integer inFormaPagamento;
	
	@JsonIgnore
	@Transient
	private SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	
	@JsonProperty
	@Transient
	private String dtVencimentoFormatada;
	
	@JsonProperty
	@Transient
	private String dtPagamentoFormatada;
	
	@JsonProperty
	@Transient
	private String inSituacaoFormatado;
	
	@JsonIgnore
	@Transient
	private String vlDespesaFormatado;
	
	@JsonIgnore
	@Transient
	private String vlPagoFormatado;

//	@JsonIgnore
//	@Transient
	@Basic
	@Column(name="idtransacaobancaria", table="vwdespesa")
	private Long idTransacaoBancaria;

	public DespesaFinanceiro() {

	}
	
	public DespesaFinanceiro(DespesaFinanceiro despesa) {
		this.inStatus = despesa.getInStatus();
		this.id = despesa.getId();
		this.inTipo = despesa.getInTipo();
		this.nrOrdemParcela = despesa.getNrOrdemParcela();
		this.nrQuantidadeParcelas = despesa.getNrQuantidadeParcelas();
		this.nmDespesa = despesa.getNmDespesa();
		this.nrDocumento = despesa.getNrDocumento();
		this.nrServico = despesa.getNrServico();
		this.dtVencimento = despesa.getDtVencimento();
		this.dtPagamento = despesa.getDtPagamento();
		this.vlPago = despesa.getVlPago();
		this.vlDespesa = despesa.getVlDespesa();
		this.vlMulta = despesa.getVlMulta();
		this.vlJuros = despesa.getVlJuros();
		this.vlDesconto = despesa.getVlDesconto();
		this.nmObservacao = despesa.getNmObservacao();
		this.dtCompetencia = despesa.getDtCompetencia();
		this.dtExclusao = despesa.getDtExclusao();
		this.empresaFinanceiroSolicitante = despesa.getEmpresaFinanceiroSolicitante();
		this.empresaFinanceiroPagante = despesa.getEmpresaFinanceiroPagante();
		this.contaBancaria = despesa.getContaBancaria();
		this.fornecedor = despesa.getFornecedor();
		this.contaContabil = despesa.getContaContabil();
		this.centroCusto = despesa.getCentroCusto();
		this.idOperadorAlteracao = despesa.getIdOperadorAlteracao();
		this.idOperadorCadastro = despesa.getIdOperadorCadastro();
		this.idOperadorExclusao = despesa.getIdOperadorExclusao();
		this.dtInclusaoLog = despesa.getDtInclusaoLog();
		this.dtAtualizacaoLog = despesa.getDtAtualizacaoLog();
		this.inFormaPagamento = despesa.getInFormaPagamento();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Short getInTipo() {
		return inTipo;
	}

	public void setInTipo(Short inTipo) {
		this.inTipo = inTipo;
	}

	public String getNmDespesa() {
		return nmDespesa.toUpperCase();
	}

	public void setNmDespesa(String nmDespesa) {
		this.nmDespesa = nmDespesa;
	}

	public String getNrDocumento() {
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public String getNrServico() {
		return nrServico;
	}

	public void setNrServico(String nrServico) {
		this.nrServico = nrServico;
	}

	public Date getDtVencimento() {
		setDtVencimentoFormatada(dt.format(dtVencimento));
		return dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Double getVlDespesa() {
		return vlDespesa;
	}

	public void setVlDespesa(Double vlDespesa) {
		this.vlDespesa = vlDespesa;
	}

	public Double getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(Double vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Double getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(Double vlJuros) {
		this.vlJuros = vlJuros;
	}

	public Double getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(Double vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public Short getInStatus() {
		return inStatus;
	}

	public void setInStatus(Short inStatus) {
		this.inStatus = inStatus;
	}
	
	public Integer getInSituacao() {
		if(this.dtVencimento != null){
			Date dataVctoFimDodia = DateUtil.getEndOfDay(this.dtVencimento);
			if(dataVctoFimDodia.before(new Date()) && this.inStatus != 1){
				this.inSituacao = 2;
			}else if( (DateUtil.isIgual(dataVctoFimDodia, new Date()) || dataVctoFimDodia.after(new Date()) ) && this.inStatus != 1){
				this.inSituacao = 0;				
			}else if(this.inStatus == 1){
				this.inSituacao = 1;
			}
		}
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public Date getDtCompetencia() {
		return dtCompetencia;
	}

	public void setDtCompetencia(Date dtCompetencia) {
		this.dtCompetencia = dtCompetencia;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public EmpresaFinanceiro getEmpresaFinanceiroSolicitante() {
		return empresaFinanceiroSolicitante;
	}
	
	public void setEmpresaFinanceiroSolicitante(EmpresaFinanceiro empresaFinanceiroSolicitante) {
		this.empresaFinanceiroSolicitante = empresaFinanceiroSolicitante;
	}

	public EmpresaFinanceiro getEmpresaFinanceiroPagante() {
		return empresaFinanceiroPagante;
	}
	
	public void setEmpresaFinanceiroPagante(EmpresaFinanceiro empresaFinanceiroPagante) {
		this.empresaFinanceiroPagante = empresaFinanceiroPagante;
	}

	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}

	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtPagamento() {
		return dtPagamento;
	}

	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	public Double getVlPago() {
		return vlPago;
	}
	
	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}

	public Integer getNrOrdemParcela() {
		return nrOrdemParcela;
	}

	public void setNrOrdemParcela(Integer nrOrdemParcela) {
		this.nrOrdemParcela = nrOrdemParcela;
	}

	public Integer getNrQuantidadeParcelas() {
		return nrQuantidadeParcelas;
	}

	public void setNrQuantidadeParcelas(Integer nrQuantidadeParcelas) {
		this.nrQuantidadeParcelas = nrQuantidadeParcelas;
	}

	public SimpleDateFormat getDt() {
		return dt;
	}

	public void setDt(SimpleDateFormat dt) {
		this.dt = dt;
	}

	public String getDtVencimentoFormatada() {
		return dtVencimentoFormatada;
	}

	public void setDtVencimentoFormatada(String dtVencimentoFormatada) {
		this.dtVencimentoFormatada = dtVencimentoFormatada;
	}


	public String getDtPagamentoFormatada() {
		return dtPagamentoFormatada;
	}

	public void setDtPagamentoFormatada(String dtPagamentoFormatada) {
		this.dtPagamentoFormatada = dtPagamentoFormatada;
	}

	public String getInSituacaoFormatado() {
		if (this.inSituacao == 0)
			this.inSituacaoFormatado = "EM ABERTO";
		else if (this.inSituacao == 1)
			this.inSituacaoFormatado = "PAGO";
		else if (this.inSituacao == 2)
			this.inSituacaoFormatado = "ATRASADO";
		
		return inSituacaoFormatado;
	}

	public void setInSituacaoFormatado(String inSituacaoFormatado) {
		this.inSituacaoFormatado = inSituacaoFormatado;
	}
	

	@JsonProperty
	public String getVlDespesaFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);

		if (getVlDespesa() != null)
			return currencyFormatter.format(getVlDespesa());
		return null;
	}
	
	@JsonProperty
	public String getVlPagoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);

		if (getVlPago() != null)
			return currencyFormatter.format(getVlPago());
		return null;
	}
	
	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Long getIdTransacaoBancaria() {
		return idTransacaoBancaria;
	}

	public void setIdTransacaoBancaria(Long idTransacaoBancaria) {
		this.idTransacaoBancaria = idTransacaoBancaria;
	}


}
