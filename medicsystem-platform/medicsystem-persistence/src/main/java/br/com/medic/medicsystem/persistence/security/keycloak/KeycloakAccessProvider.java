package br.com.medic.medicsystem.persistence.security.keycloak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.keycloak.RSATokenVerifier;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.representations.AccessToken;

import com.fasterxml.jackson.databind.ObjectMapper;

public class KeycloakAccessProvider {

	private KeycloakDeployment deployment;

	private CloseableHttpClient client = HttpClients.createDefault();

	// ultimo token obtido (medic-secondauth client)
	private Map<String, String> issuedToken;
	private AccessToken issuedTokenInfo;

	public KeycloakAccessProvider() {
		initialize("keycloak-secondauth.json");
	}

	private void initialize(String keycloakJSON) {
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(keycloakJSON);
		this.deployment = KeycloakDeploymentBuilder.build(is);
	}

	private boolean issueToken() {
		String url = deployment.getTokenUrl();

		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("client_id", deployment.getResourceName()));
		nvps.add(new BasicNameValuePair("client_secret", deployment.getResourceCredentials().get("secret")));
		nvps.add(new BasicNameValuePair("grant_type", "client_credentials"));

		try {
			httpPost.setConfig(RequestConfig.copy(RequestConfig.DEFAULT)
					.setConnectionRequestTimeout(15000)
					.setConnectTimeout(15000)
					.build()
					);
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			CloseableHttpResponse response = client.execute(httpPost);

			if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				@SuppressWarnings("unchecked")
				Map<String, String> token = (new ObjectMapper()).readValue(response.getEntity().getContent(), Map.class);
				if(token != null) {
					issuedToken = token;
					issuedTokenInfo = RSATokenVerifier.verifyToken(token.get("access_token"), deployment.getRealmKey(), deployment.getRealmInfoUrl());
					return true;
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean isLoggedIn() {
		return issuedToken != null && issuedTokenInfo.isActive() && !issuedTokenInfo.isExpired();
	}

	public CloseableHttpResponse sendRequest(HttpRequestBase req) throws Exception {
		try {
			String HOST = deployment.getLogoutUrl().getHost();
			HttpHost target = new HttpHost(HOST, 8080, "http");
			if(!isLoggedIn()) {
				boolean hasSuccess = issueToken();
				if(!hasSuccess) {
					return null;
				}
			}
			req.setHeader("Authorization", "bearer " + issuedToken.get("access_token"));
			req.setHeader("Content-Type", "application/json");
			req.setConfig(RequestConfig.copy(RequestConfig.DEFAULT)
					.setConnectionRequestTimeout(15000)
					.setConnectTimeout(15000)
					.build()
					);
			CloseableHttpResponse response = client.execute(target, req);
			logoutSession();
			return response;
		} catch (IOException e) {
			throw new Exception();
		}
	}
	
	private void logoutSession() {
		try {
			String HOST = deployment.getLogoutUrl().getHost();
			HttpHost target = new HttpHost(HOST, 8080, "http");
			
			String uri = this.getAuthServerBaseUrl() + "/realms/" + this.getRealmName() + "/protocol/openid-connect/logout";
			
			HttpPost httpPost = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("refresh_token", this.issuedToken.get("refresh_token")));
			nvps.add(new BasicNameValuePair("client_id", deployment.getResourceName()));
			nvps.add(new BasicNameValuePair("client_secret", deployment.getResourceCredentials().get("secret")));
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

			httpPost.setConfig(RequestConfig.copy(RequestConfig.DEFAULT)
					.setConnectionRequestTimeout(15000)
					.setConnectTimeout(15000)
					.build()
					);
			client.execute(target, httpPost);
			issuedToken = null;
			this.issuedTokenInfo = null;
		} catch(Exception e) {
			
		}
	}

	public String getAuthServerBaseUrl() {
		return this.deployment.getAuthServerBaseUrl();
	}

	public String getRealmName() {
		return this.deployment.getRealm();
	}
}

