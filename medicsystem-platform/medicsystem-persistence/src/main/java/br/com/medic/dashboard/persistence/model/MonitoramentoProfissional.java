package br.com.medic.dashboard.persistence.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MonitoramentoProfissional {
	
	private String funcionario;
	
	private String especialidade;
	
	private String unidade;
	
	private String bloqueio;
	
	private String nrCelular;
	
	@JsonIgnore
	private Timestamp hrinicioatendimento;
	
	@JsonIgnore
	private Timestamp hrfimatendimento;	
	
	@JsonIgnore
	private Timestamp firsttimeponto;
	
	@JsonIgnore
	private Timestamp lasttimeponto;
	
	@JsonIgnore
	private Timestamp firsttimechamada;
	
	private Integer status;
	
	private String horaAtendimentoInicial;
	
	private String horaAtendimentoFinal;
	
	private String horaPontoInicial;
	
	private String horaPontoFinal;
	
	private String horaChamada;
	
	
	public MonitoramentoProfissional(){
		
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public Timestamp getHrinicioatendimento() {
		return hrinicioatendimento;
	}

	public void setHrinicioatendimento(Timestamp hrinicioatendimento) {
		this.hrinicioatendimento = hrinicioatendimento;
	}

	public Timestamp getHrfimatendimento() {
		return hrfimatendimento;
	}

	public void setHrfimatendimento(Timestamp hrfimatendimento) {
		this.hrfimatendimento = hrfimatendimento;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(String bloqueio) {
		this.bloqueio = bloqueio;
	}

	public Timestamp getFirsttimeponto() {
		return firsttimeponto;
	}

	public void setFirsttimeponto(Timestamp firsttimeponto) {
		this.firsttimeponto = firsttimeponto;
	}

	public Timestamp getLasttimeponto() {
		return lasttimeponto;
	}

	public void setLasttimeponto(Timestamp lasttimeponto) {
		this.lasttimeponto = lasttimeponto;
	}

	public Timestamp getFirsttimechamada() {
		return firsttimechamada;
	}

	public void setFirsttimechamada(Timestamp firsttimechamada) {
		this.firsttimechamada = firsttimechamada;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getHoraAtendimentoInicial() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrinicioatendimento != null)
			return df.format(hrinicioatendimento);
		
		return horaAtendimentoInicial;
	}

	public void setHoraAtendimentoInicial(String horaAtendimentoInicial) {
		this.horaAtendimentoInicial = horaAtendimentoInicial;
	}

	public String getHoraAtendimentoFinal() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(hrfimatendimento != null)
			return df.format(hrfimatendimento);
		
		return horaAtendimentoFinal;
	}

	public void setHoraAtendimentoFinal(String horaAtendimentoFinal) {
		this.horaAtendimentoFinal = horaAtendimentoFinal;
	}

	public String getHoraPontoInicial() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(firsttimeponto != null)
			return df.format(firsttimeponto);
			
		return horaPontoInicial;
	}

	public void setHoraPontoInicial(String horaPontoInicial) {
		this.horaPontoInicial = horaPontoInicial;
	}

	public String getHoraPontoFinal() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(lasttimeponto != null)
			return df.format(lasttimeponto);
		
		return horaPontoFinal;
	}

	public void setHoraPontoFinal(String horaPontoFinal) {
		this.horaPontoFinal = horaPontoFinal;
	}

	public String getHoraChamada() {
		
		DateFormat df = new SimpleDateFormat("HH:mm");
		if(firsttimechamada != null)
			return df.format(firsttimechamada);
		
		return horaChamada;
	}

	public void setHoraChamada(String horaChamada) {
		this.horaChamada = horaChamada;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}


}
