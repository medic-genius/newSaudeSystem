package br.com.medic.medicsystem.persistence.security.dto;

public class UsuarioAtividadeDTO {
	
	private String login;
	
	private Integer qtdAtividade;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Integer getQtdAtividade() {
		return qtdAtividade;
	}

	public void setQtdAtividade(Integer qtdAtividade) {
		this.qtdAtividade = qtdAtividade;
	}
	
	
	

}
