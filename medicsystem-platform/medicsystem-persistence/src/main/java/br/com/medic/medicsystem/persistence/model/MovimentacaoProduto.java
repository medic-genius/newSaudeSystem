package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the tbmovimentacaoproduto database table.
 * 
 */
@Entity
public class MovimentacaoProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBMOVIMENTACAOPRODUTO_IDMOVIMENTACAOPRODUTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBMOVIMENTACAOPRODUTO_IDMOVIMENTACAOPRODUTO_GENERATOR")
	private Long idmovimentacaoproduto;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	@Temporal(TemporalType.DATE)
	private Date dtmovimentacao;

	private String nmobservacao;

	private Integer qtmovimentada;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;


	//bi-directional many-to-one association to Tbsetor
	@ManyToOne
	@JoinColumn(name="idsetordestino")
	private Setor tbsetor1;

	//bi-directional many-to-one association to Tbsetor
	@ManyToOne
	@JoinColumn(name="idsetororigem")
	private Setor tbsetor2;

	public MovimentacaoProduto() {
	}

	public Long getIdmovimentacaoproduto() {
		return this.idmovimentacaoproduto;
	}

	public void setIdmovimentacaoproduto(Long idmovimentacaoproduto) {
		this.idmovimentacaoproduto = idmovimentacaoproduto;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Date getDtmovimentacao() {
		return this.dtmovimentacao;
	}

	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}

	public String getNmobservacao() {
		return this.nmobservacao;
	}

	public void setNmobservacao(String nmobservacao) {
		this.nmobservacao = nmobservacao;
	}

	public Integer getQtmovimentada() {
		return this.qtmovimentada;
	}

	public void setQtmovimentada(Integer qtmovimentada) {
		this.qtmovimentada = qtmovimentada;
	}

	public Setor getTbsetor1() {
		return this.tbsetor1;
	}

	public void setTbsetor1(Setor tbsetor1) {
		this.tbsetor1 = tbsetor1;
	}

	public Setor getTbsetor2() {
		return this.tbsetor2;
	}

	public void setTbsetor2(Setor tbsetor2) {
		this.tbsetor2 = tbsetor2;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}