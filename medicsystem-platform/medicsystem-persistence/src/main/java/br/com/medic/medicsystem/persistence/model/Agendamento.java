package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import br.com.medic.medicsystem.persistence.dto.PacienteDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbagendamento")
public class Agendamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGENDAMENTO_ID_SEQ")
	@SequenceGenerator(name = "AGENDAMENTO_ID_SEQ", catalog = "realvida", sequenceName = "agendamento_id_seq", allocationSize = 1)
	@Column(name = "idagendamento")
	private Long id;

	@Basic
	@Column(name = "boDespesaExameGerada")
	private Boolean boDespesaExameGerada;

	@Basic
	@Column(name = "bodespesagerada")
	private Boolean boDespesaGerada;

	@Basic
	@Column(name = "boexames")
	private Boolean boExames;

	@Basic
	@Column(name = "boreagendamento")
	private Boolean boReagendamento;

	@Basic
	@Column(name = "boremanejamento")
	private Boolean boRemanejamento;

	@Basic
	@Column(name = "boretorno")
	private Boolean boRetorno;

	@Basic
	@Column(name = "boretornogerado")
	private Boolean boRetornoGerado;

	@Basic
	@Column(name = "boservicorealizado")
	private Boolean boServicoRealizado;

	@Basic
	@Column(name = "bosolicitouretorno")
	private Boolean boSolicitouRetorno;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  timezone = "UTC")
	@Basic
	@Column(name = "dtagendamento")
	private Date dtAgendamento;

	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Manaus")
	@Basic
	@Column(name = "hragendamento")
	private Date hrAgendamento;

	@Basic
	@Column(name = "idagendamentoaux")
	private Long idAgendamentoAux;

	@Basic
	@Column(name = "idagendamentoodonto")
	private Long idAgendamentoOdonto;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "intipo")
	private Integer inTipo;

	@NotNull
	@Basic
	@Column(name = "intipoagendamento", nullable = false)
	private Integer inTipoAgendamento;

	@Basic
	@Column(name = "intipoatendimento")
	private Integer inTipoAtendimento;

	@NotNull
	@Basic
	@Column(name = "intipoconsulta")
	private Integer inTipoConsulta;

	@Basic
	@Column(name = "inturno")
	private Integer inTurno;

	@Basic
	@Column(name = "nragendamento", nullable = false)
	private String nrAgendamento;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcliente")
	private Cliente cliente;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcontrato")
	private Contrato contrato;

	@ManyToOne
	@JoinColumn(name = "iddependente")
	private Dependente dependente;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idespecialidade")
	private Especialidade especialidade;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idfuncionario")
	private Profissional profissional;

	@ManyToOne
	@JoinColumn(name = "idsubespecialidade")
	private SubEspecialidade subEspecialidade;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idunidade")
	private Unidade unidade;
	
	
	@Basic
	@Column(name = "bostatustriagem")
	private Boolean boStatusTriagem;
	
	@Basic
	@Column(name = "boatendimentobarcode")
	private Boolean boAtendimentoBarcode;

	@JsonProperty
	@Transient
	private PacienteDTO<?> paciente;

	@Transient
	private String motivoEncaixe;

	@JsonProperty
	@Transient
	private String justificativaRetorno;

	@JsonProperty
	@Transient
	private String justificativaLoginGerente;
	
	@JsonProperty
	@Transient
	private String dtAgendamentoFormatado;
	
	@JsonProperty
	@Transient
	private String hrAgendamentoFormatado;
	
	private Time horaPresenca;
	
	@JsonProperty
	@Transient
	private String nmOperadorCadastro;
	
	@JsonProperty
	@Transient
	private String nmOperadorAlteracao;
	
	@JsonProperty
	@Transient
	private String dtInclusaoFormatada;
	
	@JsonProperty
	@Transient
	private String dtModificacaoFormatada;
	
	@JsonIgnore
	@Transient
	private SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
	
	@JsonProperty
	@Transient
	private String nmLocal;
	
	@Basic
	@Column(name = "idagendamentopai")
	private Long idAgendamentoPai;
	
	@Basic
	@Column(name = "boparticular")
	private Boolean boParticular;
	
	@Basic
	@Column(name = "boreturnultimate")
	private Boolean boReturnUltimate;
	
	/*add 27-04-2018*/
	@Basic
	@Column(name="intipoprioridade")
	private Integer inTipoPrioridade;
	
	@Basic
	@Column(name="obsurgencia")
	private String obsUrgencia;
	
	@Basic
	@Column(name="instatusprioridade")
	private Integer inStatusPrioridade;
	
	/**/
		
	/*
	 * quando verdadeiro, indica que o agendamento 
	 * não possui nenhum atendimento medico
	 */
	@JsonProperty
	@Transient
	private boolean boForaHorario;
	
	
	/*
	 * quando verdadeiro, indica que o agendamento 
	 * esta fora do limite para dar a presença
	 */
	@JsonProperty
	@Transient
	private boolean boForaPresenca;

	@JsonProperty
	@Transient
	private Consultorio consultorio;
	
	@JsonProperty
	@Transient
	private String hrInicio;
	
	@JsonProperty
	@Transient
	private String hrFim;
	
	@JsonProperty
	@Transient
	private Boolean isConvenio; // indica se um agendamento é por convênio
	
	@JsonProperty
	@Transient
	private String loginGerenteAutorizacao; // login de gerente autorizando agendamento

	public Agendamento() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoDespesaExameGerada() {
		return this.boDespesaExameGerada;
	}

	public void setBoDespesaExameGerada(Boolean boDespesaExameGerada) {
		this.boDespesaExameGerada = boDespesaExameGerada;
	}

	public Boolean getBoDespesaGerada() {
		return this.boDespesaGerada;
	}

	public void setBoDespesaGerada(Boolean boDespesaGerada) {
		this.boDespesaGerada = boDespesaGerada;
	}

	public Boolean getBoExames() {
		return this.boExames;
	}

	public void setBoExames(Boolean boExames) {
		this.boExames = boExames;
	}

	public Boolean getBoReagendamento() {
		return this.boReagendamento;
	}

	public void setBoReagendamento(Boolean boReagendamento) {
		this.boReagendamento = boReagendamento;
	}

	public Boolean getBoRemanejamento() {
		return this.boRemanejamento;
	}

	public void setBoRemanejamento(Boolean boRemanejamento) {
		this.boRemanejamento = boRemanejamento;
	}

	public Boolean getBoRetorno() {
		return this.boRetorno;
	}

	public void setBoRetorno(Boolean boRetorno) {
		this.boRetorno = boRetorno;
	}

	public Boolean getBoRetornoGerado() {
		return this.boRetornoGerado;
	}

	public void setBoRetornoGerado(Boolean boRetornoGerado) {
		this.boRetornoGerado = boRetornoGerado;
	}

	public Boolean getBoServicoRealizado() {
		return this.boServicoRealizado;
	}

	public void setBoServicoRealizado(Boolean boServicoRealizado) {
		this.boServicoRealizado = boServicoRealizado;
	}

	public Boolean getBoSolicitouRetorno() {
		return this.boSolicitouRetorno;
	}

	public void setBoSolicitouRetorno(Boolean boSolicitouRetorno) {
		this.boSolicitouRetorno = boSolicitouRetorno;
	}

	public Date getDtAgendamento() {
		return this.dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public Date getDtAtualizacao() {
		return this.dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Timestamp getDtAtualizacaoLog() {
		if(dtAtualizacaoLog != null)
			setDtModificacaoFormatada(dt.format(dtAtualizacaoLog));
		return this.dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusao() {
		return this.dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtInclusaoLog() {
		setDtInclusaoFormatada(dt.format(dtInclusaoLog));
		return this.dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getHrAgendamento() {
		return this.hrAgendamento;
	}

	public void setHrAgendamento(Date hrAgendamento) {
		this.hrAgendamento = hrAgendamento;
	}

	public Long getIdAgendamentoAux() {
		return this.idAgendamentoAux;
	}

	public void setIdAgendamentoAux(Long idAgendamentoAux) {
		this.idAgendamentoAux = idAgendamentoAux;
	}

	public Long getidAgendamentoOdonto() {
		return this.idAgendamentoOdonto;
	}

	public void setidAgendamentoOdonto(Long idAgendamentoOdonto) {
		this.idAgendamentoOdonto = idAgendamentoOdonto;
	}

	public Long getIdOperadorAlteracao() {
		return this.idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return this.idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Integer getInStatus() {
		return this.inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Integer getInTipo() {
		return this.inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public Integer getInTipoAgendamento() {
		return inTipoAgendamento;
	}

	public void setInTipoAgendamento(Integer inTipoAgendamento) {
		this.inTipoAgendamento = inTipoAgendamento;
	}

	public Integer getInTipoAtendimento() {
		return this.inTipoAtendimento;
	}

	public void setInTipoAtendimento(Integer inTipoAtendimento) {
		this.inTipoAtendimento = inTipoAtendimento;
	}

	public Integer getInTipoConsulta() {
		return this.inTipoConsulta;
	}

	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}

	public Integer getInTurno() {
		return this.inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public String getNrAgendamento() {
		return this.nrAgendamento;
	}

	public void setNrAgendamento(String nrAgendamento) {
		this.nrAgendamento = nrAgendamento;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Contrato getContrato() {
		return this.contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Dependente getDependente() {
		return this.dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Especialidade getEspecialidade() {
		return this.especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public Profissional getProfissional() {
		return this.profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public SubEspecialidade getSubEspecialidade() {
		return this.subEspecialidade;
	}

	public void setSubEspecialidade(SubEspecialidade subEspecialidade) {
		this.subEspecialidade = subEspecialidade;
	}

	public Unidade getUnidade() {
		return this.unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public PacienteDTO<?> getPaciente() {
		return this.paciente;
	}

	public void setPaciente(PacienteDTO<?> paciente) {
		this.paciente = paciente;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getMotivoEncaixe() {
		return motivoEncaixe;
	}

	public void setMotivoEncaixe(String motivoEncaixe) {
		this.motivoEncaixe = motivoEncaixe;
	}

	public String getJustificativaRetorno() {
		return justificativaRetorno;
	}

	public void setJustificativaRetorno(String justificativaRetorno) {
		this.justificativaRetorno = justificativaRetorno;
	}

	public String getJustificativaLoginGerente() {
		return justificativaLoginGerente;
	}

	public void setJustificativaLoginGerente(String justificativaLoginGerente) {
		this.justificativaLoginGerente = justificativaLoginGerente;
	}

	
	public String getDtAgendamentoFormatado() {
		if (getDtAgendamento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			this.dtAgendamentoFormatado = sdf.format(getDtAgendamento());
			return this.dtAgendamentoFormatado;
		}
		return null;
	}
	
	
	public String getHrAgendamentoFormatado() {
		if (getHrAgendamento() != null) {
			SimpleDateFormat form = new SimpleDateFormat("kk:mm");
			this.hrAgendamentoFormatado = form.format(getHrAgendamento());
			return this.hrAgendamentoFormatado;
		}
		return null;
	}

	public Time getHoraPresenca() {
		return horaPresenca;
	}

	public void setHoraPresenca(Time horaPresenca) {
		this.horaPresenca = horaPresenca;
	}

	public String getNmOperadorCadastro() {
		return nmOperadorCadastro;
	}

	public void setNmOperadorCadastro(String nmOperadorCadastro) {
		this.nmOperadorCadastro = nmOperadorCadastro;
	}

	public String getNmOperadorAlteracao() {
		return nmOperadorAlteracao;
	}

	public void setNmOperadorAlteracao(String nmOperadorAlteracao) {
		this.nmOperadorAlteracao = nmOperadorAlteracao;
	}

	public String getDtInclusaoFormatada() {
		return dtInclusaoFormatada;
	}

	public void setDtInclusaoFormatada(String dtInclusaoFormatada) {
		this.dtInclusaoFormatada = dtInclusaoFormatada;
	}

	public String getDtModificacaoFormatada() {
		return dtModificacaoFormatada;
	}

	public void setDtModificacaoFormatada(String dtModificacaoFormatada) {
		this.dtModificacaoFormatada = dtModificacaoFormatada;
	}

	@JsonProperty
	@Transient
	public String getNmLocal() {
		return nmLocal;
	}
	
	@JsonProperty
	@Transient
	public void setNmLocal(String nmLocal) {
		this.nmLocal = nmLocal;
	}

	public Boolean getBoStatusTriagem() {
		return boStatusTriagem;
	}

	public void setBoStatusTriagem(Boolean boStatusTriagem) {
		this.boStatusTriagem = boStatusTriagem;
	}

	public Boolean getBoAtendimentoBarcode() {
		return boAtendimentoBarcode;
	}

	public void setBoAtendimentoBarcode(Boolean boAtendimentoBarcode) {
		this.boAtendimentoBarcode = boAtendimentoBarcode;
	}

	public Long getIdAgendamentoPai() {
		return idAgendamentoPai;
	}

	public void setIdAgendamentoPai(Long idAgendamentoPai) {
		this.idAgendamentoPai = idAgendamentoPai;
	}
	
	@JsonProperty
	@Transient
	public boolean getBoForaHorario() {
		return boForaHorario;
	}
	
	@JsonProperty
	@Transient
	public void setBoForaHorario(boolean boForaHorario) {
		this.boForaHorario = boForaHorario;
	}
	
	@JsonProperty
	@Transient
	public boolean getBoForaPresenca() {
		return boForaPresenca;
	}
	
	@JsonProperty
	@Transient
	public void setBoForaPresenca(boolean boForaPresenca) {
		this.boForaPresenca = boForaPresenca;
	}

	public Consultorio getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(Consultorio consultorio) {
		this.consultorio = consultorio;
	}

	public String getHrInicio() {
		return hrInicio;
	}

	public void setHrInicio(String hrInicio) {
		this.hrInicio = hrInicio;
	}

	public String getHrFim() {
		return hrFim;
	}

	public void setHrFim(String hrFim) {
		this.hrFim = hrFim;
	}

	public Boolean getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}

	public Boolean getIsConvenio() {
		return isConvenio;
	}

	public void setIsConvenio(Boolean isConvenio) {
		this.isConvenio = isConvenio;
	}

	public String getLoginGerenteAutorizacao() {
		return loginGerenteAutorizacao;
	}

	public void setLoginGerenteAutorizacao(String loginGerenteAutorizacao) {
		this.loginGerenteAutorizacao = loginGerenteAutorizacao;
	}
	
	public Boolean getBoReturnUltimate() {
		return boReturnUltimate;
	}

	public void setBoReturnUltimate(Boolean boReturnUltimate) {
		this.boReturnUltimate = boReturnUltimate;
	}

	public Integer getInTipoPrioridade() {
		return inTipoPrioridade;
	}

	public void setInTipoPrioridade(Integer inTipoPrioridade) {
		this.inTipoPrioridade = inTipoPrioridade;
	}

	public String getObsUrgencia() {
		return obsUrgencia;
	}

	public void setObsUrgencia(String obsUrgencia) {
		this.obsUrgencia = obsUrgencia;
	}

	public Integer getInStatusPrioridade() {
		return inStatusPrioridade;
	}

	public void setInStatusPrioridade(Integer inStatusPrioridade) {
		this.inStatusPrioridade = inStatusPrioridade;
	}
	
	
}