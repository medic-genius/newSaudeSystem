package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.DataCoberturaDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoDTO;
import br.com.medic.medicsystem.persistence.dto.TipoConsultaDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.EspProfissional;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.ServicoCredenciada;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;
import br.com.medic.medicsystem.persistence.model.SubEspProfissional;
import br.com.medic.medicsystem.persistence.model.views.DespesaConvenioView;

@Named("servico-dao")
@ApplicationScoped
public class ServicoDAO extends RelationalDataAccessObject<Servico> {

	@Inject
	private Logger logger;

	public List<ServicoProfissional> getServicosPorProfissional(
	        Long idProfissional) {
		String queryStr = "SELECT sp FROM ServicoProfissional sp "
		        + "WHERE sp.profissional.id = " + idProfissional
		        + " AND sp.servico.boBloqueio = false "
		        + "ORDER BY sp.servico.nmServico ASC";
		TypedQuery<ServicoProfissional> query = entityManager.createQuery(
		        queryStr, ServicoProfissional.class);

		List<ServicoProfissional> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Servico getServico(Long id) {
		return searchByKey(Servico.class, id);
	}

	public List<Servico> getServicos(String nmServico, Integer startPosition, Integer maxResults) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Servico> criteria = cb.createQuery(Servico.class);

		Root<Servico> servicoRoot = criteria.from(Servico.class);
		criteria.select(servicoRoot);

		Predicate p = cb.conjunction();
		if (nmServico != null) {
			String where_servico;

			if(nmServico.startsWith("%") || nmServico.endsWith("%")) {
				where_servico = nmServico.toUpperCase();

			} else {
				where_servico = "%" + nmServico.toUpperCase() + "%";
			}

			p = cb.and(p, cb.like(servicoRoot.get("nmServico"), where_servico));
			p = cb.and(p, cb.isNull(servicoRoot.get("dtExclusao")));
			p = cb.and(p, cb.isFalse(servicoRoot.get("boBloqueio")));
		}

		criteria.where(p);

		criteria.orderBy(cb.asc(servicoRoot.get("nmServico")));

		TypedQuery<Servico> q = entityManager.createQuery(criteria);
		startPosition = startPosition == null ? 0 : startPosition;

		if (maxResults != null) {
			q.setMaxResults(maxResults);
		}

		q.setFirstResult(startPosition);

		List<Servico> result = q.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<ServicoCredenciada> getServicoCredenciadas(String idsServico,
	        Integer tipoCredenciada) {
		// inLocalAtendimento = 1 interna, 0 externa
		String queryCredenciada = "select sc from ServicoCredenciada sc where sc.servico.id in ("
		        + idsServico
		        + " ) and sc.credenciada.dtExclusao is null and sc.credenciada.inLocalAtendimento = "
		        + tipoCredenciada + " order by sc.credenciada.nmFantasia";

		TypedQuery<ServicoCredenciada> query = entityManager.createQuery(
		        queryCredenciada, ServicoCredenciada.class);

		return query.getResultList();

	}

	public List<ServicoCredenciada> getServicoCredenciada(Long idServico,
	        Integer tipoCredenciada) {
		// inLocalAtendimento = 1 interna, 0 externa
		String queryCredenciada = "select sc from ServicoCredenciada sc where sc.servico.id = "
		        + idServico
		        + " and sc.credenciada.dtExclusao is null and sc.credenciada.inLocalAtendimento = "
		        + tipoCredenciada + " order by sc.credenciada.nmFantasia";

		TypedQuery<ServicoCredenciada> query = entityManager.createQuery(
		        queryCredenciada, ServicoCredenciada.class);

		return query.getResultList();

	}

	public List<ServicoProfissional> getServicoProfissionais(Long idServico) {
		// Carrega os profissionais relacionados
		String queryProfissionais = "select sp from ServicoProfissional sp"
		        + " where sp.servico.id = " + idServico
		        + " and sp.profissional.dtExclusao is null ";

		TypedQuery<ServicoProfissional> query = entityManager.createQuery(
		        queryProfissionais, ServicoProfissional.class);

		return query.getResultList();
	}

	public List<EspProfissional> getEspecialidadeProfissionais(Long idServico) {

		String queryEspProf = "select distinct(ep) from ServicoEspecialidade se, EspProfissional ep"
		        + " where se.especialidade.id = ep.especialidade.id "
		        + " and ep.profissional.dtExclusao is null "
		        + " and se.servico.id = "
		        + idServico
		        + " and se.boPadrao is true "
		        + " and ep.profissional.dtExclusao is null ";

		TypedQuery<EspProfissional> query = entityManager.createQuery(
		        queryEspProf, EspProfissional.class);

		return query.getResultList();
	}

	public List<SubEspProfissional> getSubEspecialidadeProfissionais(
	        Long idServico) {

		String querySubEspProf = "select distinct(sep) from ServicoSubEspecialidade sse, SubEspProfissional sep"
		        + " where sse.subEspecialidade.id = sep.subEspecialidade.id "
		        + " and sep.profissional.dtExclusao is null "
		        + " and sse.servico.id = "
		        + idServico
		        + " and sse.boPadrao is true"
		        + " and sep.profissional.dtExclusao is null";

		TypedQuery<SubEspProfissional> query = entityManager.createQuery(
		        querySubEspProf, SubEspProfissional.class);

		return query.getResultList();
	}

	public ServicoProfissional getServicoProfissional(Long idServico,
	        Long idProfissional) {
		
		// Carrega os profissionais relacionados
		String queryProfissionais = "select sp from ServicoProfissional sp"
		        + " where sp.servico.id = " + idServico
		        + " and sp.profissional.id = " + idProfissional
		        + " and sp.profissional.dtExclusao is null ";
		        
		TypedQuery<ServicoProfissional> query = entityManager.createQuery(
		        queryProfissionais, ServicoProfissional.class);
		
		query.setMaxResults(1);
		
		return query.getSingleResult();
	}

	public ServicoCredenciada getServicoCredenciada(Long idServico,
	        Long idCredenciada) {
		// Carrega os profissionais relacionados
		String queryCredenciada = "select sp from ServicoCredenciada sp"
		        + " where sp.servico.id = " + idServico
		        + " and sp.credenciada.id = " + idCredenciada
		        + " and sp.credenciada.dtExclusao is null";

		TypedQuery<ServicoCredenciada> query = entityManager.createQuery(
		        queryCredenciada, ServicoCredenciada.class);

		query.setMaxResults(1);
		return query.getSingleResult();
	}

	public List<ServicoDTO> getValoresServico(String idServico) {
		String query = "SELECT 'PROFISSIONAL' AS TIPO, PROF.NMFUNCIONARIO AS EXECUTOR, SP.vlservico AS VLSERVICO, SP.vlservicoassociado AS VLASSOCIADO,"
				+ " '-' AS LOGRADOURO, '-' AS NUMERO, '-' AS COMPLEMENTO, '-' AS BAIRRO, '-' AS NMPONTOREFERENCIA, '-' AS NRTELEFONE1, '-' AS NRTELEFONE2, '-' AS NMEMAIL "
		        + " FROM REALVIDA.TBSERVICOPROFISSIONAL SP "
		        + " JOIN REALVIDA.TBFUNCIONARIO PROF ON PROF.IDFUNCIONARIO = SP.IDFUNCIONARIO "
		        + " WHERE SP.IDSERVICO =  "
		        + idServico
		        + " AND PROF.DTEXCLUSAO IS NULL "
		        + " UNION ALL "
		        + " SELECT CASE CD.INLOCALATENDIMENTO "
		        + " WHEN 0 THEN 'CRED. EXTERNA' "
		        + " ELSE 'CRED. INTERNA' "
		        + " END AS TIPO, "
		        + " CD.nmfantasia AS EXECUTOR, SC.vlcredenciada AS VLSERVICO, SC.vlcredenciadaassociado AS VLASSOCIADO, "
		        + " CD.NMLOGRADOURO, CD.NRNUMERO AS NUMERO, CD.NMCOMPLEMENTO AS COMPLEMENTO, B.NMBAIRRO AS BAIRRO, CD.NMPONTOREFERENCIA AS NMPONTOREFERENCIA, CD.NRTELEFONE1, CD.NRTELEFONE2, CD.NMEMAIL AS NMEMAIL "
		        + " FROM REALVIDA.TBSERVICOCREDENCIADA SC "
		        + " JOIN REALVIDA.TBCREDENCIADA CD ON CD.IDCREDENCIADA = SC.IDCREDENCIADA "
		        + " LEFT JOIN REALVIDA.TBBAIRRO B ON CD.IDBAIRRO = B.IDBAIRRO "
		        + " WHERE SC.IDSERVICO =  "
		        + idServico
		        + " AND CD.DTEXCLUSAO IS NULL";

		@SuppressWarnings("unchecked")
		List<ServicoDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("tipo", StringType.INSTANCE)
		        .addScalar("executor", StringType.INSTANCE)
		        .addScalar("vlservico", DoubleType.INSTANCE)
		        .addScalar("vlassociado", DoubleType.INSTANCE)
		        .addScalar("logradouro", StringType.INSTANCE)
		        .addScalar("numero", StringType.INSTANCE)
		        .addScalar("complemento", StringType.INSTANCE)
		        .addScalar("bairro", StringType.INSTANCE)
		        .addScalar("nmPontoReferencia", StringType.INSTANCE)
		        .addScalar("nrTelefone1", StringType.INSTANCE)
		        .addScalar("nrTelefone2", StringType.INSTANCE)
		        .addScalar("nmEmail", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ServicoDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}
	public List<TipoConsultaDTO> getTipoConsulta(Long idServico, Long idFuncionario, Long idEspecialidade) {
		
		
		String query = "select intipoconsulta as id, CASE intipoconsulta "
												+"  WHEN 0 THEN cast('Hora Marcada' as text) "
									            +"  WHEN 1 THEN cast('Ordem de Chegada' as text) "
									            +"  WHEN 2 THEN cast('Orçamento' as text ) "
									            +" END AS nmtipoconsulta "
				+" from realvida.tbatendimentoprofissional ap"
				 +" join realvida.tbatendimentoservprofissional asp on ap.idatendimentoprofissional = asp.idatendimentoprofissional and asp.dtexclusao is null " 
				 +" join realvida.tbespprofissional espprof on espprof.idespprofissional = ap.idespprofissional"
				 +" where ap.idfuncionario = :idFuncionario"
				 +" and idservico = :idServico"
				 +" and espprof.idespecialidade = :idEspecialidade "
				 +" group by intipoconsulta";
		
		@SuppressWarnings("unchecked")
		List<TipoConsultaDTO> result =  findByNativeQuery(query)
		        						.unwrap(SQLQuery.class)
		        						.addScalar("id", IntegerType.INSTANCE)
		        						.addScalar("nmTipoConsulta", StringType.INSTANCE)
		        						.setParameter("idFuncionario", idFuncionario).setParameter("idServico", idServico)
		        						.setParameter("idEspecialidade", idEspecialidade)
		        						.setResultTransformer(
									                Transformers.aliasToBean(TipoConsultaDTO.class)).list();
		
		try {
			
			if(result != null )
				return result;
			else 
				return null;
			
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
	}
	
public DataCoberturaDTO getUltimoServicoCobertoValido(Long idServico, Long idContrato, Integer validadeContratual, Long idCliente, Long idDependente){
		
		String queryStr = "";
		
		if(idDependente != null){
			
			queryStr = " select t1.dtdespesa as dataCobertura, t1.iddespesa from ("
					+" select desp.* from realvida.tbdespesa desp"
					+" inner join realvida.tbdespesaservico despserv on desp.iddespesa = despserv.iddespesa"
					+" inner join realvida.tbcontratodependente cd on desp.idcontratodependente = cd.idcontratodependente"
					+" left join realvida.tbagendamento ag on ag.idagendamento = desp.idagendamento and (ag.instatus = 2 or ag.instatus = 1 or ag.instatus = 4) and ag.dtexclusao is null"
					+" where desp.informapagamento = 10 and desp.insituacao = 1"
					+" and (desp.boexcluida is null or desp.boexcluida = false)"
					+" and despserv.idservico = :idServico"
					+" and cd.idcontrato = :idContrato"
					+" and cd.iddependente = :idPaciente"
					+" and ((desp.idagendamento is not null and ag.idagendamento is not null) or (desp.idagendamento is null and ag.idagendamento is null))"
					+" order by desp.dtdespesa desc"
					+" limit 1"
					+" )t1"
					+" where current_date between t1.dtdespesa and (t1.dtdespesa + interval '"
					+(validadeContratual != null ?
							validadeContratual : 0)+" days')";
			
		}else {
			queryStr = " select t1.dtdespesa as dataCobertura, t1.iddespesa from ("
					+" select desp.* from realvida.tbdespesa desp"
					+" inner join realvida.tbdespesaservico despserv on desp.iddespesa = despserv.iddespesa"
					+" inner join realvida.tbcontratocliente cc on desp.idcontratocliente = cc.idcontratocliente"
					+" left join realvida.tbagendamento ag on ag.idagendamento = desp.idagendamento and (ag.instatus = 2 or ag.instatus = 1 or ag.instatus = 4) and ag.dtexclusao is null and ag.iddependente is null "
					+" where desp.informapagamento = 10 and desp.insituacao = 1"
					+" and (desp.boexcluida is null or desp.boexcluida = false)"
					+" and despserv.idservico = :idServico"
					+" and cc.idcontrato = :idContrato"
					+" and cc.idcliente = :idPaciente"
					+" and ((desp.idagendamento is not null and ag.idagendamento is not null) or (desp.idagendamento is null and ag.idagendamento is null))"
					+" order by desp.dtdespesa desc"
					+" limit 1"
					+" )t1"
					+" where current_date between t1.dtdespesa and (t1.dtdespesa + interval '"
					+(validadeContratual != null ?
							validadeContratual : 0)+" days') and t1.idcontratodependente is null";
		}
		
		
		
		try {
			DataCoberturaDTO result = (DataCoberturaDTO) findByNativeQuery(queryStr)
			.setParameter("idServico", idServico)
			.setParameter("idPaciente", (idDependente != null ? idDependente : idCliente))
			.setParameter("idContrato", idContrato)
			.unwrap(SQLQuery.class)
			.addScalar("dataCobertura", DateType.INSTANCE)
		    .addScalar("idDespesa", LongType.INSTANCE)
		    .setResultTransformer(
		                Transformers.aliasToBean(DataCoberturaDTO.class)).uniqueResult();
			if(result == null || result.getIdDespesa() == null){
				return null;
			}
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}

		public CoberturaPlano getCoberturaPlano(Long idPlano, Long idServico) {
		
			String queryStr = "SELECT cp FROM CoberturaPlano cp where cp.plano.id = "
			        + idPlano + " and cp.servico.id = " + idServico;
		
			TypedQuery<CoberturaPlano> query = entityManager.createQuery(queryStr,
			        CoberturaPlano.class);
		try{
			query.setMaxResults(1);
			CoberturaPlano result = query.getSingleResult();
		
			return result;
		}catch (Exception e){
			return null;
		}
			
		}
		
		public List<Servico> getServicosByTipo(Integer inTipo) {
			
			TypedQuery<Servico> query = entityManager
			        .createQuery("SELECT serv FROM Servico serv where serv.dtExclusao is null"
			        		+ " and serv.inTipoServico = " +inTipo+ " order by serv.nmServico asc",
			        Servico.class);

			List<Servico> result = query.getResultList();

			if (result.isEmpty()) {
				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			}

			return result;

		}
		
		
	public List<Servico> getServicosPorPlano(long idPlano, String name){
		
		String queryStr = "SELECT coberturaPlano.servico FROM CoberturaPlano coberturaPlano"
				+ " where coberturaPlano.plano.id = " + idPlano
				+ " and coberturaPlano.servico.nmServico like upper('%" + name + "%')";
	
		TypedQuery<Servico> query = entityManager.createQuery(queryStr, Servico.class);
		try
		{
			List<Servico> result = query.getResultList();
			return result;
		} catch (Exception e)
		{
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DespesaConvenioView> getDespesaServicoConvenio(long idEmpresaCliente, String dtInicio, String dtFim){
		
		String queryStr =
		"SELECT t1.idparcela, t1.iddespesa, t1.nrdespesa, t1.nrparcela, t1.nrcontrato,"
		+ " t1.nmcliente, t1.nmdependente, t1.dtvencimento, t1.idempresacliente, t1.idservico, t1.nmservico, t1.iddespesaservico, t1.vlservico,"
		+ " t1.qtservico, t1.vlparcela, t1.idguia "
		+ " from ( "
		+ " select p.idparcela, d.iddespesa, d.nrdespesa, p.nrparcela, c.nrcontrato, cli.nmcliente, "
		+ " dep.nmdependente, p.dtvencimento, c.idempresacliente, s.idservico, s.nmservico, "
		+ " ds.iddespesaservico, ds.vlservico, ds.qtservico, "
		+ " ( SELECT cast(sum(cast(despserv.vlservico * despserv.qtservico as double precision)) as numeric(10,2)) AS sum "
		+ "  FROM realvida.tbdespesaservico despserv"
		+ "  JOIN realvida.tbdespesa desp ON desp.iddespesa = despserv.iddespesa "
		+ "  JOIN realvida.tbparcela parc ON parc.iddespesa = desp.iddespesa "
		+ "  WHERE despserv.iddespesa = d.iddespesa AND (parc.informapagamento = 15 OR (parc.informapagamento <> ALL (ARRAY[-1, 10, 14, 15, 16])) AND despserv.incoberto IS TRUE)) AS vlparcela, "
		+ " gs.idguia "
		+ " FROM realvida.tbdespesaservico ds "
		+ " JOIN realvida.tbdespesa d ON ds.iddespesa = d.iddespesa "
		+ " JOIN realvida.tbparcela p ON d.iddespesa = p.iddespesa "
		+ " JOIN realvida.tbservico s ON s.idservico = ds.idservico "
		+ " JOIN realvida.tbcontratocliente cc ON cc.idcontratocliente = d.idcontratocliente "
		+ " JOIN realvida.tbcontrato c ON c.idcontrato = cc.idcontrato "
		+ " JOIN realvida.tbcliente cli ON cli.idcliente = cc.idcliente "
		+ " LEFT JOIN realvida.tbcontratodependente cd ON cd.idcontratodependente = d.idcontratodependente "
		+ " LEFT JOIN realvida.tbdependente dep ON dep.iddependente = cd.iddependente "
		+ " LEFT JOIN realvida.tbguiaservico gs on gs.idservico = ds.idservico and gs.idguiaservico = (select idguiaservico from realvida.tbdespesaguiaservico where idguiaservico = gs.idguiaservico and iddespesa = d.iddespesa) "
		+ " WHERE (d.boexcluida IS NULL OR d.boexcluida = false) AND (p.informapagamento = 15 OR (p.informapagamento <> ALL (ARRAY[-1, 10, 14, 15, 16])) AND ds.incoberto IS TRUE) AND d.insituacao = 1 "
		+ " AND c.idempresacliente IS NOT NULL and d.idagendamento is null "
		+ " AND c.idempresacliente = " + idEmpresaCliente + " AND p.dtvencimento between '"+ dtInicio +"' and '" + dtFim +"' "
		+ " GROUP BY d.iddespesa, s.idservico, p.idparcela, c.nrcontrato, cli.nmcliente, dep.nmdependente, c.idempresacliente, ds.iddespesaservico, gs.idguia "
		+ " UNION "
		+ " select p.idparcela, d.iddespesa, d.nrdespesa, p.nrparcela, c.nrcontrato, cli.nmcliente, "
		+ " dep.nmdependente, ag.dtagendamento as dtvencimento, c.idempresacliente, s.idservico, s.nmservico, "
		+ " ds.iddespesaservico, ds.vlservico, ds.qtservico, "
		+ " ( SELECT cast(sum(cast(despserv.vlservico * despserv.qtservico as double precision)) as numeric(10,2)) AS sum "
		+ "  FROM realvida.tbdespesaservico despserv "
		+ "  JOIN realvida.tbdespesa desp ON desp.iddespesa = despserv.iddespesa "
		+ "  JOIN realvida.tbparcela parc ON parc.iddespesa = desp.iddespesa "
		+ "  WHERE despserv.iddespesa = d.iddespesa AND (parc.informapagamento = 15 OR (parc.informapagamento <> ALL (ARRAY[-1, 10, 14, 15, 16])) AND despserv.incoberto IS TRUE)) AS vlparcela,"
		+ " gs.idguia "
		+ " FROM realvida.tbdespesaservico ds "
		+ " JOIN realvida.tbdespesa d ON ds.iddespesa = d.iddespesa "
		+ " JOIN realvida.tbparcela p ON d.iddespesa = p.iddespesa "
		+ " JOIN realvida.tbservico s ON s.idservico = ds.idservico "
		+ " JOIN realvida.tbcontratocliente cc ON cc.idcontratocliente = d.idcontratocliente "
		+ " JOIN realvida.tbcontrato c ON c.idcontrato = cc.idcontrato "
		+ " JOIN realvida.tbcliente cli ON cli.idcliente = cc.idcliente "
		+ " INNER JOIN realvida.tbagendamento ag on d.idagendamento = ag.idagendamento "
		+ " LEFT JOIN realvida.tbcontratodependente cd ON cd.idcontratodependente = d.idcontratodependente "
		+ " LEFT JOIN realvida.tbdependente dep ON dep.iddependente = cd.iddependente "
		+ " LEFT JOIN realvida.tbguiaservico gs on gs.idservico = ds.idservico and gs.idguiaservico = (select idguiaservico from realvida.tbdespesaguiaservico where idguiaservico = gs.idguiaservico and iddespesa = d.iddespesa) "
		+ " WHERE (d.boexcluida IS NULL OR d.boexcluida = false) AND (p.informapagamento = 15 OR (p.informapagamento <> ALL (ARRAY[-1, 10, 14, 15, 16])) AND ds.incoberto IS TRUE) AND d.insituacao = 1 "
		+ " AND c.idempresacliente IS NOT NULL and ag.instatus = 2 "
		+ " AND c.idempresacliente =" + idEmpresaCliente + " AND ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
		+ " GROUP BY d.iddespesa, s.idservico, p.idparcela, c.nrcontrato, cli.nmcliente, dep.nmdependente, c.idempresacliente, ds.iddespesaservico, gs.idguia, ag.dtagendamento "
		+ ") t1 "
		+ " order by t1.iddespesa ";	
		
		
		try {			
			
			List<DespesaConvenioView> result = findByNativeQuery(queryStr)			
			.unwrap(SQLQuery.class)			
		    .addScalar("idParcela", LongType.INSTANCE)
		    .addScalar("idDespesa", LongType.INSTANCE)
		    .addScalar("nrDespesa", StringType.INSTANCE)
		    .addScalar("nrParcela", StringType.INSTANCE)
		    .addScalar("nrContrato", StringType.INSTANCE)
		    .addScalar("nmCliente", StringType.INSTANCE)
		    .addScalar("nmDependente", StringType.INSTANCE)
		    .addScalar("dtVencimento", DateType.INSTANCE)
		    .addScalar("idEmpresaCliente", LongType.INSTANCE)
		    .addScalar("idServico", LongType.INSTANCE)
		    .addScalar("nmServico", StringType.INSTANCE)
		    .addScalar("idDespesaServico", LongType.INSTANCE)
		    .addScalar("vlServico", DoubleType.INSTANCE)
		    .addScalar("qtServico", IntegerType.INSTANCE)
		    .addScalar("vlParcela", DoubleType.INSTANCE)
		    .addScalar("idGuia", LongType.INSTANCE)
		    .setResultTransformer(Transformers.aliasToBean(DespesaConvenioView.class)).list();
			
			if(result == null){
				return null;
			}
			return result;
			
		} catch (NoResultException e) {
			return null;
		}

	}
	
	

}
