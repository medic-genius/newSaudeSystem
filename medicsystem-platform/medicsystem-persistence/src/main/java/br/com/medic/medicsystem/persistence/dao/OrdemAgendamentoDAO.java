package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.OrdemAgendamento;


@Named("ordemagendamento-dao")
@ApplicationScoped
public class OrdemAgendamentoDAO extends RelationalDataAccessObject<OrdemAgendamento>{
	
	public Double maxOrdemPrioridadedaPrioridade(Long idFuncionario, Long idEspecialidade, Long idAtendimentoProfissional){
		String queryStr = "SELECT COALESCE(MAX(nrordem),0) as nrordem FROM realvida.tbordemagendamento "
				+ " WHERE idfuncionario = :idFuncionario AND idespecialidade = :idEspecialidade AND intipoprioridade = 2"
				+ " AND (boexcluido IS FALSE OR boexcluido IS NULL) "
				+ " AND idatendimentoprofissional = :idAtendimentoProfissional";

		Query q = findByNativeQuery(queryStr).setParameter("idFuncionario", idFuncionario).setParameter("idEspecialidade", idEspecialidade)
				.setParameter("idAtendimentoProfissional", idAtendimentoProfissional);
		Double result = (Double) q.getSingleResult();

		if (result == null) {
			return new Double(0);
		}

		return result;
	}
	public Double maxOrdemPrioridade (Long idFuncionario, Long idEspecialidade, Long idAtendimentoProfissional){
		
		String queryStr = "SELECT COALESCE(MAX(nrordem),0) as nrordem FROM realvida.tbordemagendamento "
				+ " WHERE idfuncionario = :idFuncionario AND idespecialidade = :idEspecialidade AND intipoprioridade = 1"
				+ " AND (boexcluido IS FALSE OR boexcluido IS NULL) "
				+ " AND idatendimentoprofissional = :idAtendimentoProfissional";

		Query q = findByNativeQuery(queryStr).setParameter("idFuncionario", idFuncionario).setParameter("idEspecialidade", idEspecialidade)
				.setParameter("idAtendimentoProfissional", idAtendimentoProfissional);
		Double result = (Double) q.getSingleResult();

		if (result == null) {
			return new Double(0);
		}

		return result;
	}	
	
	public Double maxOrdemTodos (Long idFuncionario, Long idEspecialidade, Long idAtendimentoProfissional){
		
		String queryStr = "SELECT COALESCE(MAX(nrordem),0) as nrordem FROM realvida.tbordemagendamento "
				+ " WHERE idfuncionario = :idFuncionario AND idespecialidade = :idEspecialidade " 
				+ " AND (boexcluido IS FALSE OR boexcluido IS NULL)"
				+ " AND idatendimentoprofissional = :idAtendimentoProfissional";

		Query q = findByNativeQuery(queryStr).setParameter("idFuncionario", idFuncionario).setParameter("idEspecialidade", idEspecialidade)
				.setParameter("idAtendimentoProfissional", idAtendimentoProfissional);
		Double result = (Double) q.getSingleResult();

		if (result == null) {
			return new Double(0);
		}

		return result;
	}
	
	public Double maxOrdemNormal (Long idFuncionario, Long idEspecialidade, Long idAtendimentoProfissional){
		
		String queryStr = "SELECT COALESCE(MAX(nrordem),0) as nrordem FROM realvida.tbordemagendamento "
				+ " WHERE idfuncionario = :idFuncionario AND idespecialidade = :idEspecialidade AND intipoprioridade = 0" 
				+ " AND (boexcluido IS FALSE OR boexcluido IS NULL)"
				+ " AND idatendimentoprofissional = :idAtendimentoProfissional";

		Query q = findByNativeQuery(queryStr).setParameter("idFuncionario", idFuncionario).setParameter("idEspecialidade", idEspecialidade)
				.setParameter("idAtendimentoProfissional", idAtendimentoProfissional);
		Double result = (Double) q.getSingleResult();

		if (result == null) {
			return new Double(0);
		}

		return result;
	}
	
	public OrdemAgendamento getOrdemAgendamentoPorAgendamento (Long idAgendamento){
		
		
		String queryString = "SELECT ord FROM OrdemAgendamento ord where ord.idAgendamento = :idAgendamento "
				+ " AND (ord.boExcluido IS FALSE OR ord.boExcluido IS NULL)";
		TypedQuery<OrdemAgendamento> result = entityManager.createQuery(
				queryString, OrdemAgendamento.class).setParameter("idAgendamento", idAgendamento);
		
		
		try {
			return result.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<OrdemAgendamento> getListOrdemAgendamentoPorAgendamento (Long idAgendamento){
		
		String queryString = "SELECT ord FROM OrdemAgendamento ord where ord.idAgendamento = :idAgendamento "
				+ " AND (ord.boExcluido IS FALSE OR ord.boExcluido IS NULL)";
				
		try {
			
			TypedQuery<OrdemAgendamento> query = entityManager.createQuery(
					queryString, OrdemAgendamento.class).setParameter("idAgendamento", idAgendamento);
			
			List<OrdemAgendamento> result = query.getResultList();
			
			return result;
		} catch (Exception e) {
			return null;
		}
	}

}
