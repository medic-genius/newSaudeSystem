package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.CobrancaFinanceiroView;

@ApplicationScoped
@Named("cobrancafinanceiroview-dao")
public class CobrancaFinanceiroViewDAO extends RelationalDataAccessObject<CobrancaFinanceiroView>{

	public List<CobrancaFinanceiroView> getTitulosAbertosByContrato(String nrContrato){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CobrancaFinanceiroView> criteria = cb.createQuery(CobrancaFinanceiroView.class);
		
		Root<CobrancaFinanceiroView> cobrancaRoot = criteria.from(CobrancaFinanceiroView.class);
		criteria.multiselect(
				cobrancaRoot.get("nrContrato"),
				cobrancaRoot.get("nmFormaPagamentoTitulo"),
				cobrancaRoot.get("inTipo"), 
				cobrancaRoot.get("dataVencimento"),
				cobrancaRoot.get("vlTitulo"),
				cobrancaRoot.get("valorJuros"),
				cobrancaRoot.get("valorMultaAtraso"),
				cobrancaRoot.get("nrTitulo"),
				cobrancaRoot.get("idTitulo")
		);
		
		Predicate predicate = cb.conjunction();
		
		if(nrContrato != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("nrContrato"), nrContrato));
		}
		
//		predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inInadimplente"), 1));

		criteria.where(predicate);
		TypedQuery<CobrancaFinanceiroView> q = entityManager.createQuery(criteria);
		List<CobrancaFinanceiroView> result = q.getResultList();	
		return result;
	}
	
	
	public List<CobrancaFinanceiroView> getNegociacaoViewListByIdCliente(Long idCliente){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CobrancaFinanceiroView> criteria = cb.createQuery(CobrancaFinanceiroView.class);
		
		Root<CobrancaFinanceiroView> cobrancaRoot = criteria.from(CobrancaFinanceiroView.class);
		criteria.multiselect(cobrancaRoot.get("idContrato"), cobrancaRoot.get("idCliente"), cobrancaRoot.get("nrContrato"),
				cobrancaRoot.get("inFormaDePagamento"), cobrancaRoot.get("nmPlano"), cobrancaRoot.get("dtContrato"), 
				cobrancaRoot.get("boNaoFazUsoDoPlano"), cobrancaRoot.get("inSituacao"), cb.count(cobrancaRoot.get("idTitulo")),
				cb.sum(cobrancaRoot.get("vlTitulo")), cb.sum(cobrancaRoot.get("valorJuros")), cb.sum(cobrancaRoot.get("valorMultaAtraso")));

		Predicate predicate = cb.conjunction();
		
		predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("idCliente"), idCliente));
		
		criteria.where(predicate);
		criteria.groupBy(cobrancaRoot.get("idContrato"), cobrancaRoot.get("idCliente"), cobrancaRoot.get("nrContrato"),
				cobrancaRoot.get("inFormaDePagamento"), cobrancaRoot.get("nmPlano"), cobrancaRoot.get("dtContrato"), 
				cobrancaRoot.get("boNaoFazUsoDoPlano"), cobrancaRoot.get("inSituacao"));
		criteria.orderBy(cb.asc(cobrancaRoot.get("idCliente")));

		TypedQuery<CobrancaFinanceiroView> q = entityManager.createQuery(criteria);
		List<CobrancaFinanceiroView> result = q.getResultList();
		return result;
	}
	
}
