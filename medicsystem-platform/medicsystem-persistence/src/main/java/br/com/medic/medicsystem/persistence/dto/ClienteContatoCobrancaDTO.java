package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.medic.medicsystem.persistence.model.Sac;
import br.com.medic.medicsystem.persistence.model.enums.TipoContato;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

public class ClienteContatoCobrancaDTO implements Serializable{

	private static final long serialVersionUID = -3754903925139471673L;

	private Long idSac;
	private String nmFuncionario;
	private Long idFuncionario;
	private String nmConversa;
	private TipoContato tipoContato;
	private String tempoDuracao;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Manaus")
	private Date dtContato;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "America/Manaus")
	private Date dtProximaLigacao;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "America/Manaus")
	private Date dtPrevistaPagamento;

	public ClienteContatoCobrancaDTO() {

	}
	
	public ClienteContatoCobrancaDTO(Sac sac) {
		this.idSac = sac.getId();
		this.nmConversa = sac.getObservacao();
		
		if(sac.getFuncionario2()!= null){
			this.nmFuncionario = sac.getFuncionario2().getNmFuncionario();
			this.idFuncionario = sac.getFuncionario2().getId();
		} else if(sac.getFuncionario1()!= null){
			this.nmFuncionario = sac.getFuncionario1().getNmFuncionario();
			this.idFuncionario = sac.getFuncionario1().getId();
		}
		
		if(sac.getCobranca() != null){
			this.dtProximaLigacao = sac.getCobranca().getProximaLigacao();
			this.dtPrevistaPagamento = sac.getCobranca().getPrevisaoPagamento();
		}
		
		if(sac.getData() != null && sac.getHrFinal() != null && sac.getHrInicial() != null){
			Calendar cDate = Calendar.getInstance();
			cDate.setTime(sac.getData());
		
			Calendar cTime = Calendar.getInstance();
			cTime.setTime(sac.getHrInicial());
			
			cDate.set(Calendar.HOUR_OF_DAY, cTime.get(Calendar.HOUR_OF_DAY));
			cDate.set(Calendar.MINUTE, cTime.get(Calendar.MINUTE));
			cDate.set(Calendar.SECOND, cTime.get(Calendar.SECOND));
			
			this.dtContato =  cDate.getTime();
			
			this.tempoDuracao = DateUtil.calculaDiferencaEntreHorariosAsString(sac.getHrInicial(), sac.getHrFinal());
		}
		
		this.tipoContato =  TipoContato.getTipoContatoById(sac.getTipoSac());
	}

	public Long getIdSac() {
		return idSac;
	}

	public void setIdSac(Long idSac) {
		this.idSac = idSac;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getNmConversa() {
		return nmConversa;
	}

	public void setNmConversa(String nmConversa) {
		this.nmConversa = nmConversa;
	}

	public Date getDtContato() {
		return dtContato;
	}

	public void setDtContato(Date dtContato) {
		this.dtContato = dtContato;
	}

	public Date getDtProximaLigacao() {
		return dtProximaLigacao;
	}

	public void setDtProximaLigacao(Date dtProximaLigacao) {
		this.dtProximaLigacao = dtProximaLigacao;
	}

	public Date getDtPrevistaPagamento() {
		return dtPrevistaPagamento;
	}

	public void setDtPrevistaPagamento(Date dtPrevistaPagamento) {
		this.dtPrevistaPagamento = dtPrevistaPagamento;
	}

	public TipoContato getTipoContato() {
		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) {
		this.tipoContato = tipoContato;
	}

	public String getTempoDuracao() {
		return tempoDuracao;
	}

	public void setTempoDuracao(String tempoDuracao) {
		this.tempoDuracao = tempoDuracao;
	}
	
}
