package br.com.medic.medicsystem.persistence.security.keycloak;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.security.auth.Subject;
import javax.security.jacc.PolicyContext;

import org.keycloak.KeycloakPrincipal;

import br.com.medic.medicsystem.persistence.appmobile.dao.UsuarioRegisterDAO;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;

public class KeycloakUtil {
	@Inject
	@Named("usuario-register-dao")
	private UsuarioRegisterDAO usuarioRegisterDAO;
	
	public String getCurrentUserLogin() {
		try {
			Subject userSubject = (Subject) PolicyContext.getContext("javax.security.auth.Subject.container");

			@SuppressWarnings("rawtypes")
			Set<KeycloakPrincipal> principals = userSubject.getPrincipals(KeycloakPrincipal.class);
			for (KeycloakPrincipal<?> keycloakPrincipal : principals) {
				return keycloakPrincipal.getKeycloakSecurityContext().getToken().getPreferredUsername();
			}

		} catch (Exception e) {

			return null;
		}
		return null;
	}
	
	public UsuarioRegister getCurrentUserByContext() {
		String username = this.getCurrentUserLogin();
		if(username != null) {
			return usuarioRegisterDAO.getUsuarioByUsername(username);
		}
		return null;
	}
}

