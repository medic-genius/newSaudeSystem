package br.com.medic.medicsystem.persistence.dto;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;

public class GuiaServicoDTO {

	private Long id;

	private Guia guia;

	private Servico servico;

	private Integer status;
	
	private String statusFormatado;

	private Timestamp dtUtilizacao;

	private Timestamp dtExclusao;

	private Float valorServico;
	
	private Double vlServicoAssociado;

	private GenericPaginateDTO paginator;

	private String sqlFieldsSelect =
			"guia_servico.idguiaservico as id" + ", guia_servico.idguia as id_guia"
			+ ", guia_servico.idservico as id_servico" + ", guia_servico.status"
			+ ", guia_servico.dtutilizacao as dtUtilizacao" + ", guia_servico.dtexclusao as dtExclusao"
			+ ", servico.nmservico as nm_servico" + ", guia.dtvalidade as dtValidade"
			+ ", guia.dtexclusao as guiaDtExclusao" + ", guia.dtinicio as dtInicio"
			+ ", guia.idcliente as id_cliente" // i10
			+ ", guia.iddependente as id_dependente" + ", cliente.nmcliente as nmCliente"
			+ ", dependente.nmdependente as nm_dependente" + ", contrato.idcontrato as id_contrato"
			+ ", contrato.nrcontrato as nr_contrato" + ", contrato.idplano as id_plano"
			+ ", contrato.idempresacliente as id_empresacliente" + ", contrato.bobloqueado as bo_bloqueado"
			+ ", contrato.dtinativacao as dt_inativacao"
			+ ", contrato.boinativadoconveniada as bo_inativado_conveniada" // i20
			+ ", plano.nmplano as nm_plano" + ", empresacliente.nmrazaosocial"
			+ ", cp.vlservico as vlservico" + ", cp.vlservicoassociado as vlservicoassociado" ;

	private String sqlFrom = "realvida.tbguiaservico guia_servico";

	private String sqlJoins =
			"inner join realvida.tbservico as servico on servico.idservico = guia_servico.idservico"
			+ " inner join realvida.tbguia as guia on guia.idguia = guia_servico.idguia"
			+ " inner join realvida.tbcliente as cliente on cliente.idcliente = guia.idcliente"
			+ " left join realvida.tbdependente as dependente on dependente.iddependente = guia.iddependente"
			+ " inner join realvida.tbcontrato as contrato on contrato.idcontrato = guia.idcontrato"
			+ " inner join realvida.tbplano as plano on plano.idplano = contrato.idplano"
			+ " inner join realvida.tbcoberturaplano as cp on plano.idplano = cp.idplano and cp.idservico = servico.idservico"
			+ " inner join realvida.tbempresacliente as empresacliente on empresacliente.idempresacliente = contrato.idempresacliente";

	private String sqlWhere = null;

	private String sqlOrderBy = "guia_servico.idguiaservico desc";

	@JsonIgnore
	public String getSqlSelect() {
		String sqlQuery =
				"select "
				+ this.sqlFieldsSelect
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		if(null != this.sqlOrderBy) {
			sqlQuery += " order by " + this.sqlOrderBy;
		}
		sqlQuery += " limit " + paginator.getLimit();
		sqlQuery += " offset " + (paginator.getOffset() - 1) * paginator.getLimit();
		return sqlQuery;
	}

	@JsonIgnore
	public String getSqlCount() {
		String sqlQuery =
				"select "
				+ " count(*)"
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		return sqlQuery;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Guia getGuia() {
		return guia;
	}

	public void setGuia(Guia guia) {
		this.guia = guia;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	@SuppressWarnings("deprecation")
	public Integer getStatus() {
		if (0 == this.status) {
			Date todayZero = new Date();
			todayZero.setHours(0);
			todayZero.setMinutes(0);
			todayZero.setSeconds(0);
			if(todayZero.getYear() > this.guia.getDtValidade().getYear() ||
	             (todayZero.getYear() == this.guia.getDtValidade().getYear() &&
	            		 todayZero.getMonth() > this.guia.getDtValidade().getMonth()) ||
	             (todayZero.getYear() == this.guia.getDtValidade().getYear() &&
	            		 todayZero.getMonth() == this.guia.getDtValidade().getMonth() &&
	            				 todayZero.getDate() > this.guia.getDtValidade().getDate()))
			{
				return 5;
			}
		}
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getDtUtilizacao() {
		return dtUtilizacao;
	}

	public void setDtUtilizacao(Timestamp dtUtilizacao) {
		this.dtUtilizacao = dtUtilizacao;
	}

	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Timestamp dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Float getValorServico() {
		return valorServico;
	}

	public void setValorServico(Float valorServico) {
		this.valorServico = valorServico;
	}

	public Double getVlServicoAssociado() {
		return vlServicoAssociado;
	}

	public void setVlServicoAssociado(Double vlServicoAssociado) {
		this.vlServicoAssociado = vlServicoAssociado;
	}

	public GenericPaginateDTO getPaginator() {
		return paginator;
	}

	public void setPaginator(GenericPaginateDTO paginator) {
		this.paginator = paginator;
	}

	public void addSqlWhereConditionAnd(String condition) {
		if(null == this.sqlWhere) {
			this.sqlWhere = condition;
		} else {
			this.sqlWhere += " and " + condition;
		}
	}
	
	public void addSqlWhereConditionOr(String condition) {
		if(null == this.sqlWhere) {
			this.sqlWhere = condition;
		} else {
			this.sqlWhere += " or " + condition;
		}
	}

	public void setSqlOrderBy(String sqlOrderBy) {
		this.sqlOrderBy = sqlOrderBy;
	}

	public String getStatusFormatado() {
		if(status != null){
			switch (status) {
			case 0:
				return StatusGuiaServicoEnum.CRIADO.getDescription();
			case 1:
				return StatusGuiaServicoEnum.AGENDADO.getDescription();
			case 2:
				return StatusGuiaServicoEnum.CONSUMIDO.getDescription();
			case 4:
				return StatusGuiaServicoEnum.INATIVO.getDescription();
			case 5:
				return StatusGuiaServicoEnum.VENCIDO.getDescription();
			default:
				return null;
			}			
		}
		
		return null;	
	}

	public void setStatusFormatado(String statusFormatado) {
		this.statusFormatado = statusFormatado;
	}

}
