package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;

public class SMSDTO {
	
	private Long idCliente;
	private String nmCliente;
	private Long idDependente;
	private String nmDependente;
	private String nrCodCliente;
	private Date dtEnvio;
	private String resposta;
	private String nrCodDependente;
	
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getNmCliente() {
		return nmCliente;
	}
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	public Long getIdDependente() {
		return idDependente;
	}
	public void setIdDependente(Long idDependente) {
		this.idDependente = idDependente;
	}
	public String getNmDependente() {
		return nmDependente;
	}
	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}
	public String getNrCodCliente() {
		return nrCodCliente;
	}
	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}
	public Date getDtEnvio() {
		return dtEnvio;
	}
	public void setDtEnvio(Date dtEnvio) {
		this.dtEnvio = dtEnvio;
	}
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public String getNrCodDependente() {
		return nrCodDependente;
	}
	public void setNrCodDependente(String nrCodDependente) {
		this.nrCodDependente = nrCodDependente;
	}
	
	

}
