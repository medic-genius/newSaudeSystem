package br.com.medic.medicsystem.persistence.appmobile.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UsuarioLoginPK.class)
@Table(catalog="app_mobile", name="tbusuario_login")
public class UsuarioLogin implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_usuario")
	private Long idUsuario;
	
	@Id
	@Column(name="tipo_usuario")
	private Integer tipoUsuario;
	
	@Id
	@Column(name="id_device")
	private String idDevice;
	
	@Basic
	@Column(name="device_os")
	private String deviceOS;
	
	@Basic
	@Column(name="notification_reg_id")
	private String notificationRegId;
	
	@Basic
	@Column(name="last_login")
	private Date lastLogin;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(String idDevice) {
		this.idDevice = idDevice;
	}

	public String getDeviceOS() {
		return deviceOS;
	}

	public void setDeviceOS(String deviceOS) {
		this.deviceOS = deviceOS;
	}

	public String getNotificationRegId() {
		return notificationRegId;
	}

	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
}
