package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "financeiro", name = "tbfornecedor")
public class Fornecedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FORNECEDOR_ID_SEQ")
	@SequenceGenerator(name = "FORNECEDOR_ID_SEQ", sequenceName = "financeiro.fornecedor_id_seq", allocationSize = 1)
	@Column(name = "idfornecedor")
	private Long id;

	@Basic
	@Column(name = "nmnomefantasia")
	private String nmNomeFantasia;
	
	@Basic
	@Column(name = "nmrazaosocial")
	private String nmRazaoSocial;

	@Basic
	@Column(name = "nrcnpj")
	private String nrCnpj;
	
	@Basic
	@Column(name = "nrcpf")
	private String nrCpf;

	@Basic
	@Column(name = "nrinscricaoestadual")
	private String nrInscricaoEstadual;

	@Basic
	@Column(name = "nrinscricaomunicipal")
	private String nrInscricaoMunicipal;
	
	@Basic
	@Column(name = "nrtelefone")
	private String nrTelefone;

	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;
	
	@Basic
	@Column(name = "nmsite")
	private String nmSite;	

	@Basic
	@Column(name = "nmemail")
	private String nmEmail;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;
	
	//bi-directional many-to-one association to Bairro
	@ManyToOne
	@JoinColumn(name="idbairro")
	private Bairro bairro;

	//bi-directional many-to-one association to Cidade
	@ManyToOne
	@JoinColumn(name="idcidade")
	private Cidade cidade;	

	@Basic
	@Column(name = "nmobservacao")
	private String nmObservacao;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	public Fornecedor() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmNomeFantasia() {
		return nmNomeFantasia;
	}

	public void setNmNomeFantasia(String nmNomeFantasia) {
		this.nmNomeFantasia = nmNomeFantasia;
	}


	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}

	public String getNmSite() {
		return nmSite;
	}

	public void setNmSite(String nmSite) {
		this.nmSite = nmSite;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrCnpj() {
		return nrCnpj;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public String getNrInscricaoEstadual() {
		return nrInscricaoEstadual;
	}

	public void setNrInscricaoEstadual(String nrInscricaoEstadual) {
		this.nrInscricaoEstadual = nrInscricaoEstadual;
	}

	public String getNrInscricaoMunicipal() {
		return nrInscricaoMunicipal;
	}

	public void setNrInscricaoMunicipal(String nrInscricaoMunicipal) {
		this.nrInscricaoMunicipal = nrInscricaoMunicipal;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}
	
	@JsonProperty
	public String getNrCpfFormatado() {
		if (nrCpf != null && nrCpf.length() == 11) {
			Pattern pattern = Pattern
			        .compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCpf);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3-$4");
		}
		return nrCpf;
	}
	
	@JsonProperty
	public void setNrCpfFormatado(String nrCpf) {
		
	}
	
	@JsonProperty
	public String getNrCnpjFormatado() {
		if (nrCnpj != null && nrCnpj.length() == 14) {
			Pattern pattern = Pattern
			        .compile("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCnpj);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3/$4-$5");
		}
		return nrCnpj;
	}
	
	@JsonProperty
	public void setNrCnpjFormatado(String nrCnpj) {
		
	}
	
}