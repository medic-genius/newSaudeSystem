package br.com.medic.medicsystem.persistence.dao;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObjectAcademia;
import br.com.medic.medicsystem.persistence.dto.MensalidadeDTO;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.performance.MensalidadePerformance;


@ApplicationScoped
@Named("mensalidadecompartilhadopeformance-dao")
public class MensalidadeCompartilhadoPeformanceDAO extends RelationalDataAccessObjectAcademia<MensalidadePerformance>{
	
	public Collection<MensalidadePerformance> getMensalidadeByCliente(Long idCliente, Integer dias) {
		
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentesrecente_getmensalidades("+dias+"," +idCliente+")";
		try {
			
			Collection<MensalidadePerformance> result = findByNativeQuery(queryStr,
					MensalidadePerformance.class);

			if (result.isEmpty()) {
				return null;
			}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public MensalidadePerformance updateMensalidade(MensalidadePerformance mensalidade){
		
		return entityManager.merge(mensalidade);
	}
	
	@SuppressWarnings("unchecked")
	public List<MensalidadeDTO> getMensalidadeByClienteInadimplente(Long idCliente, Integer dias) {
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentesrecente_getmensalidades("+dias+"," +idCliente+")";		
			
		List<MensalidadeDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("nrMensalidade", StringType.INSTANCE)
				.addScalar("dtVencimento", DateType.INSTANCE)
				.addScalar("vlMensalidade", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(MensalidadeDTO.class))
				.list();
		
		
		return result;
		
	}
	
public Collection<Parcela> getCobrancaByMensalidade(Long idMensalidade) {
		
		String querystr = "select parc.* from realvida.tbnegociacao neg "
							+"join realvida.tbdespesa desp on neg.iddespesa = desp.iddespesa "
							+"join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa "
							+"where neg.idmensalidade = "+ idMensalidade;
		try {
			
			Collection<Parcela> result = findByNativeQuery(querystr, Parcela.class);

			if (result.isEmpty()) {
				return null;
			}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		
	}
}
