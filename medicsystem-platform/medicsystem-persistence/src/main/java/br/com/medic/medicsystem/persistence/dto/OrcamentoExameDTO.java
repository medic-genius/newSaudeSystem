package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.ServicoOrcamentoExame;

public class OrcamentoExameDTO {
	
	
	private Long idOrcamentoExame;
	
	private Long idServicoOrcamentoExame;
	
	private Long idAtendimento;
	
	private Long  idServico;
	
	private String nmServico;
	
	private Integer quantidade;
	
	private Long idEspecialidade;
	

	public Long getIdOrcamentoExame() {
		return idOrcamentoExame;
	}

	public void setIdOrcamentoExame(Long idOrcamentoExame) {
		this.idOrcamentoExame = idOrcamentoExame;
	}

	public Long getIdServicoOrcamentoExame() {
		return idServicoOrcamentoExame;
	}

	public void setIdServicoOrcamentoExame(Long idServicoOrcamentoExame) {
		this.idServicoOrcamentoExame = idServicoOrcamentoExame;
	}

	public Long getIdAtendimento() {
		return idAtendimento;
	}

	public void setIdAtendimento(Long idAtendimento) {
		this.idAtendimento = idAtendimento;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}






	
	

}
