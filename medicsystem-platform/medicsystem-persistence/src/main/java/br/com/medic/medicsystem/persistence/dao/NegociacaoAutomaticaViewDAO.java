package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.views.NegociacaoAutomaticaView;

@Named("negociacaoautomaticaview-dao")
@ApplicationScoped
public class NegociacaoAutomaticaViewDAO extends RelationalDataAccessObject<NegociacaoAutomaticaView>{
	
	public List<NegociacaoAutomaticaView> getClientesNegociacaoView(Integer inFormaPagamento, Integer qtdMensalidade, 
			String dtInicio, String dtFim, Boolean boNegociadoAuto) {
		
		String queryStr, orderBy = "";
						
		queryStr = "SELECT nav FROM NegociacaoAutomaticaView nav "
				+ "WHERE nav.dtContrato between '" + dtInicio + "' and '" + dtFim + "' ";
		
		if(qtdMensalidade != null)
			queryStr += " and nav.qtdMensalidade >= " + qtdMensalidade;
		else
			queryStr += " and nav.qtdMensalidade >= 0 ";
		
		if(inFormaPagamento != null)
			queryStr += " and nav.inFormaPagamento = " + inFormaPagamento;				
				
		if(boNegociadoAuto == true){
			queryStr += " and nav.boNegociadoAuto = true";
			
		}else{
			queryStr += " and (nav.boNegociadoAuto is null or nav.boNegociadoAuto = false)";			
		}
		orderBy = " Order by nav.nmCliente"; 

		TypedQuery<NegociacaoAutomaticaView> query = entityManager.createQuery(queryStr + orderBy, NegociacaoAutomaticaView.class);
		
//		if(boNegociadoAuto == false){
//			query.setMaxResults(200);
//		}
		
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}
