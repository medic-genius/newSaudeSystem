package br.com.medic.medicsystem.persistence.dto;

public class ConciliacaoFinanceiroDTO {
	
	private String dtExtrato; // data do lancamento
	private Double extValor; //valor do lancamento
	private String nmDespesa; //descricao do lancamento
	private String dtend; // data do fechamento da fatura
	private String dtasof;
	
	public ConciliacaoFinanceiroDTO(){
		
	}

	public String getDtExtrato() {
		return dtExtrato;
	}

	public void setDtExtrato(String dtExtrato) {
		this.dtExtrato = dtExtrato;
	}



	public Double getExtValor() {
		return extValor;
	}

	public void setExtValor(Double extValor) {
		this.extValor = extValor;
	}

	
	public String getNmDespesa() {
		return nmDespesa;
	}

	public void setNmDespesa(String nmDespesa) {
		this.nmDespesa = nmDespesa;
	}

	public String getDtend() {
		return dtend;
	}

	public void setDtend(String dtend) {
		this.dtend = dtend;
	}

	public String getDtasof() {
		return dtasof;
	}

	public void setDtasof(String dtasof) {
		this.dtasof = dtasof;
	}
	
}
