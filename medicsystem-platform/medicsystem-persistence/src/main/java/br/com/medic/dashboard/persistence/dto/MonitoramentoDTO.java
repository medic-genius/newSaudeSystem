package br.com.medic.dashboard.persistence.dto;

import java.util.List;

public class MonitoramentoDTO {
	
	private Integer valorMonitor;
	
	private Integer nrTotal;
	
	private Integer nrTotalOk;
	
	private List<?> monitoramento;
	
	private List<?> monitoramentoAlert;

	public Integer getValorMonitor() {
		return valorMonitor;
	}

	public void setValorMonitor(Integer valorMonitor) {
		this.valorMonitor = valorMonitor;
	}

	public Integer getNrTotal() {
		return nrTotal;
	}

	public void setNrTotal(Integer nrTotal) {
		this.nrTotal = nrTotal;
	}

	public List<?> getMonitoramento() {
		return monitoramento;
	}

	public void setMonitoramento(List<?> monitoramento) {
		this.monitoramento = monitoramento;
	}

	public List<?> getMonitoramentoAlert() {
		return monitoramentoAlert;
	}

	public void setMonitoramentoAlert(List<?> monitoramentoAlert) {
		this.monitoramentoAlert = monitoramentoAlert;
	}

	public Integer getNrTotalOk() {
		return nrTotalOk;
	}

	public void setNrTotalOk(Integer nrTotalOk) {
		this.nrTotalOk = nrTotalOk;
	}

	

}
