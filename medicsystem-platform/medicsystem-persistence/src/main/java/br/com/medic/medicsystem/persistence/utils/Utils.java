package br.com.medic.medicsystem.persistence.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class Utils {

	public static final String BR_CODE = "55";

	public static final String MAO_CODE = "92";

	public static final String NINE_DIGIT = "9";

	public static String phoneNumberCleanup(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");

			if (numberStr.length() >= 8
			        && Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {

				if (numberStr.length() == 8) {

					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9) {
					return BR_CODE + MAO_CODE + numberStr;
				} else if (numberStr.length() == 11) {
					return BR_CODE + numberStr;
				}

			}
		}

		return null;
	}

	public static String removeSpecialChars(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");

			return numberStr;

		}

		return null;
	}
	
	public static Float getFloat2DecimalPlaces(Float n){
		return (float) (Math.round(n * 100.0) / 100.00) ;
	}
	
	/**
	 * 
	 * @param value - valor a ser convertido
	 * @param places - quantidade de casa decimais
	 * @return numero com places casa decimais
	 */
	public static double getDoubleDecimalPlaces(Double value, int places ){
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		
		return bd.doubleValue();
	}
	
	public static String converterDoubelToCurrencyReal( Double valor ) {
		Locale ptBr = new Locale("pt","BR");
		String valorConvertido = NumberFormat.getCurrencyInstance( ptBr ).format( valor);
		
		return valorConvertido;
	}
	
	public static String converterDoubelToCurrencyRealWithoutSymbol( Double valor ) {
		Locale ptBr = new Locale("pt","BR");
		NumberFormat nf = NumberFormat.getCurrencyInstance( ptBr );
		DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) nf).setDecimalFormatSymbols( decimalFormatSymbols );
		
		String valorConvertido = nf.format( valor);
		
		return valorConvertido;
	}
	
	public static Object getPrimeiroNomeCliente(String nomeCliente) {
		if (nomeCliente != null) {
			nomeCliente = nomeCliente.trim();
			if (nomeCliente.indexOf(" ") != -1) {
				return nomeCliente.split(" ")[0];
			}
		}
		return nomeCliente;
	}
}
