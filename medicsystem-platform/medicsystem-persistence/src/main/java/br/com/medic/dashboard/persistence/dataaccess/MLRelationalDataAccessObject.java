package br.com.medic.dashboard.persistence.dataaccess;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

import br.com.medic.dashboard.persistence.interfaces.IDataAccessObject;
import br.com.medic.dashboard.persistence.exception.ObjectAlreadyExistsException;
import br.com.medic.dashboard.persistence.exception.ObjectNotFoundException;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class MLRelationalDataAccessObject<T> implements IDataAccessObject<T> {

	@PersistenceContext(unitName = "saudesystem")
	private EntityManager entityManager;

	private static final String NOT_RESULT_MESSAGE = "Nenhum resultado encontrado para consulta: ";

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Collection<?> findByQuery(String query) {
		Collection<?> result = entityManager.createQuery(query).getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> Collection<E> findByNativeQuery(String query, Class<E> clazz) {
		Collection<E> result = entityManager.createNativeQuery(query, clazz)
		        .getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
		}

		return result;
	}

	@Override
	public Query findByNamedNativeQuery(String nnqName) {
		return entityManager.createNamedQuery(nnqName);

	}

	@Override
	public Query findByNativeQuery(String query) {
		return entityManager.createNativeQuery(query);

	}

	@Override
	public Object loadByQuery(String query) {
		Object result;

		try {
			result = entityManager.createQuery(query).getSingleResult();
		} catch (NoResultException nre) {
			throw new ObjectNotFoundException("Load by Query failed", nre);
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object loadByQuery(String query, Object... parameters) {

		Query sql = entityManager.createQuery(query);

		for (int i = 0; i < parameters.length; i++) {
			sql.setParameter(i + 1, parameters[i]);
		}

		// getSingleResult() uses pagination and can generate memory problems
		// when the query uses join fetch
		List<Object> results = sql.getResultList();

		if (results == null || results.isEmpty()) {
			throw new ObjectNotFoundException("Entidade nao encontrada.");
		}

		return results.get(0);
	}

	@Override
	public Collection<?> findByQuery(String query, Object... parameters) {

		Query sql = entityManager.createQuery(query);

		for (int i = 0; i < parameters.length; i++) {
			sql.setParameter(i + 1, parameters[i]);
		}

		Collection<?> result = sql.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
		}

		return result;
	}

	@Override
	public Collection<?> findByQuery(String query,
	        Map<String, ? extends Object> map, Integer firstResult,
	        Integer maxResults) {
		Query result = entityManager.createQuery(query);

		if (map != null) {
			Set<? extends Map.Entry<String, ? extends Object>> set = map
			        .entrySet();
			for (Map.Entry<?, ?> me : set) {
				result.setParameter((String) me.getKey(), me.getValue());
			}
		}
		if (firstResult != null) {
			result.setFirstResult(firstResult);
		}
		if (maxResults != null) {
			result.setMaxResults(maxResults);
		}

		Collection<?> resultList = result.getResultList();

		if (resultList.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE + query);
		}

		return resultList;
	}

	@Override
	public void remove(T bean) {
		entityManager.remove(entityManager.merge(bean));
	}

	@Override
	public Object persist(T bean) {
		try {
			entityManager.persist(bean);
		} catch (PersistenceException eee) {
			if (eee.getCause() instanceof ConstraintViolationException) {
				throw new ObjectAlreadyExistsException(
				        "O objeto ja existe no sistema.", eee);
			} else {
				throw eee;
			}
		}

		return bean;
	}

	@Override
	public Object update(T bean) {
		return entityManager.merge(bean);
	}

	public <E> E searchByKey(Class<E> clazz, Object object) {
		E result = entityManager.find(clazz, object);

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE
			        + clazz.getCanonicalName() + " with id " + object);
		}

		return result;
	}
}