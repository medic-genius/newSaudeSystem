package br.com.medic.medicsystem.persistence.security;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.bson.BsonDateTime;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jboss.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.mongodb.Block;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClientSettings;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.WriteModel;
import com.mongodb.connection.ClusterSettings;
import com.mongodb.client.model.Filters.*;

/**
 * Para habilitar o recurso de auditoria, adicione as linhas abaixo no standalone.xml do wildfly.
 * 
 * <system-properties>
 *       <property name="auditoria" value="true"/>
 *       <property name="mongo_url" value="45.79.150.191:27017"/>
 *       <property name="mongo_db" value="saudehistorico"/>
 *       <property name="mongo_coll_cud" value="cud"/>
 *       <property name="mongo_coll_read" value="read"/>
 *       <property name="mongo_user" value="saude"/>
 *       <property name="mongo_pass" value="1cCzEFipHMwR"/>
 *   </system-properties>
 *   
 * @author phillip
 *
 */
@Stateless
public class MongoResource {

	@Inject
	private Logger logger;

	@Inject
	@SystemProperty("mongo_url")
	private String mongoURL;

	@Inject
	@SystemProperty("mongo_db")
	private String mongoDb;

	@Inject
	@SystemProperty("mongo_coll_cud")
	private String mongoCollectionCud;
	
	@Inject
	@SystemProperty("mongo_coll_log_sms")
	private String mongoCollectionLogSms;

	@Inject
	@SystemProperty("mongo_coll_read")
	private String mongoCollectionRead;

	@Inject
	@SystemProperty("mongo_user")
	private String mongoUser;

	@Inject
	@SystemProperty("mongo_pass")
	private String mongoPass;
	
	private final String HOST = "45.79.150.191";
	private final int PORT = 27017;

	public void saveDocument(Document document) {
		
		MongoClient mc = getMongoClient();
		
		Calendar cal = Calendar.getInstance();
		DateFormat dataFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		DateFormat dataFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		dataFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		try {
			cal.setTime(dataFormat.parse(dataFormat2.format(new Date())));
			//System.out.println(document);
			document.append("dataOperacao", dataFormat.parse(dataFormat2.format(new Date())));
			//System.out.println(document);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		MongoDatabase database = mc.getDatabase(mongoDb);
		MongoCollection<Document> collection = database.getCollection(mongoCollectionCud);
		collection.insertOne(document, new SingleResultCallback<Void>() {
			@Override
			public void onResult(final Void result, final Throwable t) {
				if (t != null) {
					logger.error("Ocorreu um erro ao salvar dados no mongo", t);
				} else {
					logger.info("Operacao salva no mongo " + mongoDb + "/" + mongoCollectionCud);
				}
				mc.close();
			}
		});
		
		
		
	}

	public void saveReadDocument(Document document) {

		MongoClient mc = getMongoClient();

		MongoDatabase database = mc.getDatabase(mongoDb);
		MongoCollection<Document> collection = database.getCollection(mongoCollectionRead);
		collection.insertOne(document, new SingleResultCallback<Void>() {
			@Override
			public void onResult(final Void result, final Throwable t) {
				if (t != null) {
					logger.error("Ocorreu um erro ao salvar dados no mongo", t);
				} else {
					logger.info("Operacao salva no mongo " + mongoDb + "/" + mongoCollectionRead);
				}
				mc.close();
			}
		});
		
		
	}
	
	private com.mongodb.MongoClient getInstance(){
       
          	MongoCredential credential = MongoCredential.createCredential(mongoUser,
          			mongoDb,
          			mongoPass.toCharArray());
    		List<MongoCredential> mongoCredentials = new ArrayList<>();
    		mongoCredentials.add(credential);
    		MongoClientOptions.Builder options = MongoClientOptions.builder();
    		options.socketKeepAlive(true);
    		ServerAddress sad = new ServerAddress(HOST, PORT);
    		
        	return  new com.mongodb.MongoClient(sad,mongoCredentials,options.build());
        	
        	
        
	}

	private MongoClient getMongoClient() {
		MongoCredential credential = MongoCredential.createCredential(mongoUser, mongoDb, mongoPass.toCharArray());

		List<MongoCredential> mongoCredentials = new ArrayList<>();
		mongoCredentials.add(credential);

		ClusterSettings clusterSettings = ClusterSettings.builder().hosts(Arrays.asList(new ServerAddress(mongoURL)))
				.description("Linode Mongo Server").build();

		MongoClientSettings settings = MongoClientSettings.builder().credentialList(mongoCredentials)
				.clusterSettings(clusterSettings).build();

		return MongoClients.create(settings);
	}
	
	public HashMap<Long, Integer> getAtividadesUsuarios(Date start, Date end) {
		
		com.mongodb.MongoClient mc = getInstance();

		com.mongodb.client.MongoDatabase database = mc.getDatabase(mongoDb);
		com.mongodb.client.MongoCollection<Document> collection = database.getCollection(mongoCollectionCud);
		HashMap<Long, Integer> results = new HashMap<Long,Integer>();
		try {
			
			System.out.println(configureDateMongo(0,start));
			System.out.println(configureDateMongo(1,end));

			Document inteval = new Document("dataOperacao",new Document("$gte",configureDateMongo(0,start)).append("$lte",configureDateMongo(1,end)));
			
			AggregateIterable<Document> iterable = collection.aggregate(Arrays.asList(
			new Document("$match",inteval),
	        new Document("$group", new Document("_id", "$idUsuario").append("count", new Document("$sum", 1)))));
			
			try{
				iterable.forEach(new Block<Document>() {
				    @Override
				    public void apply(final Document document) {
				    	
				        try {
				        	results.put(Long.parseLong(document.get("_id").toString()), Integer.parseInt(document.get("count").toString()));
				        	
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    }
				});
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		//	System.out.println("passou no finally");
			mc.close();
		}
		System.out.println(results);
		return results;			
	}
	
	// metodo para formatar a data de inicio e fim no mongo
	// 0-inicio   1-fim
	private Timestamp configureDateMongo(int type, Date date){
		Calendar cal = Calendar.getInstance();
		
		if(type == 0){
			cal.setTime(date);
		    cal.set(Calendar.HOUR_OF_DAY, 0);
		    cal.set(Calendar.MINUTE, 0);
		    cal.set(Calendar.SECOND, 0);
		    cal.set(Calendar.MILLISECOND, 0);
		    return new Timestamp(cal.getTimeInMillis());
			
		}else{
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, 23);
		    cal.set(Calendar.MINUTE, 59);
		    cal.set(Calendar.SECOND, 59);
		    cal.set(Calendar.MILLISECOND, 999);
		    return new Timestamp(cal.getTimeInMillis());
		}

	}
	
	public void saveDocumentLogSms(Document document) {
		MongoClient mc = getMongoClient();
		Calendar cal = Calendar.getInstance();
		DateFormat dataFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		DateFormat dataFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		dataFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			cal.setTime(dataFormat.parse(dataFormat2.format(new Date())));
			//System.out.println(document);
			document.append("dataOperacao", dataFormat.parse(dataFormat2.format(new Date())));
			//System.out.println(document);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MongoDatabase database = mc.getDatabase(mongoDb);
		MongoCollection<Document> collection = database.getCollection(mongoCollectionLogSms);
		collection.insertOne(document, new SingleResultCallback<Void>() {
			@Override
			public void onResult(final Void result, final Throwable t) {
				if (t != null) {
					logger.error("Ocorreu um erro ao salvar dados no mongo", t);
				} else {
					logger.info("Operacao salva no mongo " + mongoDb + "/" + mongoCollectionLogSms);
				}
				mc.close();
			}
		});	
	}
}
