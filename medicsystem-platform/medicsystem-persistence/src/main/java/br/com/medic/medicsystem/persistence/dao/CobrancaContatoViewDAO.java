package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.CobrancaContatoView;

@ApplicationScoped
@Named("cobrancacontatoview-dao")
public class CobrancaContatoViewDAO extends RelationalDataAccessObject<CobrancaContatoView> {

	
	public List<CobrancaContatoView> getClientesInadimplentesFromCobrancaView(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Integer startPosition, Integer maxResults, Boolean boAdimplente, Boolean boPaginar){	
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CobrancaContatoView> criteria = cb.createQuery(CobrancaContatoView.class);
		
		Root<CobrancaContatoView> cobrancaRoot = criteria.from(CobrancaContatoView.class);
		criteria.multiselect(cobrancaRoot.get("id"), 
							cobrancaRoot.get("nmCliente"),
							cobrancaRoot.get("nrCpf"),
							cobrancaRoot.get("nrTelefone"),
							cobrancaRoot.get("nrCelular"),
							cobrancaRoot.get("ultimoContato"),
							cb.sum(cobrancaRoot.get("inInadimplente")),
							cb.sum(cobrancaRoot.get("valorDivida")),
							cb.sum(cobrancaRoot.get("valorJuros")),
							cb.sum(cobrancaRoot.get("valorMultaAtraso")),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "M"), 1).otherwise(0)),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "D"), 1).otherwise(0))
				);
		
		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteria.where(predicate);
		criteria.groupBy(cobrancaRoot.get("id"), cobrancaRoot.get("nmCliente"),	cobrancaRoot.get("nrCpf"), 
				cobrancaRoot.get("nrTelefone"), cobrancaRoot.get("nrCelular"), cobrancaRoot.get("ultimoContato"));

		criteria.orderBy(cb.asc(cobrancaRoot.get("nmCliente")));
		
		TypedQuery<CobrancaContatoView> q = entityManager.createQuery(criteria);
		
		if(boPaginar != null && boPaginar){
			
			startPosition = startPosition == null ? 0 : startPosition;
			maxResults = maxResults == null ? 10 : maxResults;
			q.setFirstResult(startPosition);
			q.setMaxResults(maxResults);
		}
		
		List<CobrancaContatoView> result = q.getResultList();
		
		return result;
	}
	
	
	/**
	 * Retorna a quantidade de clientes dentro dos filtros especificados.
	 * */
	
	public Long getQuantidadeClientesInadimplentes(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Boolean boAdimplente){	

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = cb.createQuery();
		
		Root<CobrancaContatoView> cobrancaRoot = criteriaQuery.from(CobrancaContatoView.class);
		criteriaQuery.select(cobrancaRoot.get("id"));
		criteriaQuery.distinct(true);

		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteriaQuery.where(predicate);
		
		CriteriaQuery<Long> criteriaQueryCount = criteriaQuery.select(cb.countDistinct(cobrancaRoot));
		TypedQuery<Long> typedQueryCount = entityManager.createQuery(criteriaQueryCount);
		Long totalLinhas = typedQueryCount.getSingleResult();
		
		return totalLinhas;
	}
	
	private Predicate getWhereClause(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, 
			Date dataInicio, Date dataFim, CriteriaBuilder cb, Root<CobrancaContatoView> cobrancaRoot, Boolean boAdimplente){
		Predicate predicate = cb.conjunction();
		
		if(idEmpresa != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("idEmpresaGrupo"), idEmpresa));
		}
		
		if(idFormaPagamento != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inFormaDePagamento"), idFormaPagamento));
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			switch (opcaoData) {
			//se a opção de data for por previsao de pagamento
			case 0:
				predicate = cb.and(predicate, cb.between(cobrancaRoot.get("dtPrevisaoPagamento"), dataInicio, dataFim));
				break;

			//se a opção de data for por próxima ligação
			case 1:
				predicate = cb.and(predicate, cb.between(cobrancaRoot.get("dtProximaLigacao"), dataInicio, dataFim));
				break;

			default:
				throw new IllegalArgumentException();
			}			
		}
		
		if (nomeCliente != null) {
			String where_cliente;

			if(nomeCliente.startsWith("%") || nomeCliente.endsWith("%")) {
				where_cliente = nomeCliente.toUpperCase();

			} else {
				where_cliente = nomeCliente.toUpperCase() + "%";
			}

			predicate = cb.and(predicate, cb.like(cobrancaRoot.get("nmCliente"), where_cliente));
		}
		
		if(boAdimplente == null || !boAdimplente){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inInadimplente"), 1));
		}
		
		return predicate;
	}
}
