package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(catalog = "realvida", name = "tblaudoatendimento")
public class LaudoAtendimento implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LAUDOATENDIMENTO_ID_SEQ")
	@SequenceGenerator(name = "LAUDOATENDIMENTO_ID_SEQ", sequenceName = "realvida.laudoatendimento_id_seq", allocationSize = 1)
	@Column(name = "idlaudoatendimento")
	private Long id;
	
	@NotNull
	@Basic
	@Column(name = "nmconteudo")
	private String nmConteudo;
	
	@NotNull
	@OneToOne
	@JoinColumn(name = "idatendimento")
	private Atendimento atendimento;
	
	@NotNull
	@Column(name = "arqLaudo")
	private byte[] arqLaudo;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmConteudo() {
		return nmConteudo;
	}

	public void setNmConteudo(String nmConteudo) {
		this.nmConteudo = nmConteudo;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public byte[] getArqLaudo() {
		return arqLaudo;
	}

	public void setArqLaudo(byte[] arqLaudo) {
		this.arqLaudo = arqLaudo;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}
	
}
