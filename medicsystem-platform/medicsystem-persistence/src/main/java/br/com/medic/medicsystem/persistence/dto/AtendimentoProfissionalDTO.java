package br.com.medic.medicsystem.persistence.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AtendimentoProfissionalDTO {
	
	private Long idProfissional;
	
	private Long idAtendimentoProfissional;

	private String nmTipoAtendimento;

	private String nmUnidade;

	private String nmEspecialidade;

	private String nmSubEspecialidade;

	private String nmDiaSemana;

	private String nmTurno;

	private String nmHrInicio;

	private String nmHrFim;

	private String nmTipoConsulta;

	private Integer qtPacienteAtender;

	private Integer inDiaSemana;

	private String nmHrTempoConsulta;

	private Integer inTipoConsulta;
	
	private Boolean boParticular;
	
	private Boolean boProducao;
	
	private Boolean boAceitaEncaixe;

	public AtendimentoProfissionalDTO() {

	}

	public String getNmTipoAtendimento() {
		return nmTipoAtendimento;
	}

	public void setNmTipoAtendimento(String nmTipoAtendimento) {
		this.nmTipoAtendimento = nmTipoAtendimento;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmSubEspecialidade() {
		return nmSubEspecialidade;
	}

	public void setNmSubEspecialidade(String nmSubEspecialidade) {
		this.nmSubEspecialidade = nmSubEspecialidade;
	}

	public String getNmDiaSemana() {
		return nmDiaSemana;
	}

	public void setNmDiaSemana(String nmDiaSemana) {
		this.nmDiaSemana = nmDiaSemana;
	}

	public String getNmTurno() {
		return nmTurno;
	}

	public void setNmTurno(String nmTurno) {
		this.nmTurno = nmTurno;
	}

	public String getNmHrInicio() {
		return nmHrInicio;
	}

	public void setNmHrInicio(String nmHrInicio) {
		this.nmHrInicio = nmHrInicio;
	}

	public String getNmHrFim() {
		return nmHrFim;
	}

	public void setNmHrFim(String nmHrFim) {
		this.nmHrFim = nmHrFim;
	}

	public String getNmTipoConsulta() {
		return nmTipoConsulta;
	}

	public void setNmTipoConsulta(String nmTipoConsulta) {
		this.nmTipoConsulta = nmTipoConsulta;
	}

	public Integer getQtPacienteAtender() {
		return qtPacienteAtender;
	}

	public void setQtPacienteAtender(Integer qtPacienteAtender) {
		this.qtPacienteAtender = qtPacienteAtender;
	}

	public Integer getInDiaSemana() {
		return inDiaSemana;
	}

	public void setInDiaSemana(Integer inDiaSemana) {
		this.inDiaSemana = inDiaSemana;
	}

	public String getNmHrTempoConsulta() {
		return nmHrTempoConsulta;
	}

	public void setNmHrTempoConsulta(String nmHrTempoConsulta) {
		this.nmHrTempoConsulta = nmHrTempoConsulta;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Integer getInTipoConsulta() {
		return inTipoConsulta;
	}

	public void setInTipoConsulta(Integer inTipoConsulta) {
		this.inTipoConsulta = inTipoConsulta;
	}

	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Boolean getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}

	public Long getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Long idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public void setBoProducao(Boolean boProducao) {
		this.boProducao = boProducao;
	}

	public Boolean getBoAceitaEncaixe() {
		return boAceitaEncaixe;
	}

	public void setBoAceitaEncaixe(Boolean boAceitaEncaixe) {
		this.boAceitaEncaixe = boAceitaEncaixe;
	}
	
	
	
	

}
