package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbreceituariooftalmologico")
public class ReceituarioOftalmologico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECEITUARIOOFTALMOLOGICO_ID_SEQ")
	@SequenceGenerator(name = "RECEITUARIOOFTALMOLOGICO_ID_SEQ", catalog = "realvida", sequenceName = "realvida.receituariooftalmologico_id_seq" , allocationSize = 1)
	@Column(name = "idreceituariooftalmologico")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtreceituariooftalmologico")
	private Date dtReceituarioOftalmologico;
	
	@Basic
	@Column(name = "nmobservacao")
	private String nmObservacao;
	
	@Basic
	@Column(name = "nrlongeodcilindrico")
	private String nrLongeOdCilindrico;
	
	@Basic
	@Column(name = "nrlongeodeixo")
	private String nrLongeOdEixo;
	
	@Basic
	@Column(name = "nrlongeodesferico")
	private String nrLongeOdEsferico;
	
	@Basic
	@Column(name = "nrlongeoecilindrico")
	private String nrLongeOeCilindrico;
	
	@Basic
	@Column(name = "nrlongeoeeixo")
	private String nrLongeOeEixo;
	
	@Basic
	@Column(name = "nrlongeoeesferico")
	private String nrLongeOeEsferico;
	
	@Basic
	@Column(name = "nrpertoodcilindrico")
	private String nrPertoOdCilindrico;
	
	@Basic
	@Column(name = "nrpertoodeixo")
	private String nrPertoOdEixo;
	
	@Basic
	@Column(name = "nrpertoodesferico")
	private String nrPertoOdEsferico;
	
	@Basic
	@Column(name = "nrpertooecilindrico")
	private String nrPertoOeCilindrico;
	
	@Basic
	@Column(name = "nrpertooeeixo")
	private String nrPertoOeEixo;
	
	@Basic
	@Column(name = "nrpertooeesferico")
	private String nrPertoOeEsferico;

	//bi-directional many-to-one association to Tbatendimento
	@ManyToOne
	@JoinColumn(name="idatendimento")
	private Atendimento atendimento;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	public ReceituarioOftalmologico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtReceituarioOftalmologico() {
		return dtReceituarioOftalmologico;
	}

	public void setDtReceituarioOftalmologico(Date dtReceituarioOftalmologico) {
		this.dtReceituarioOftalmologico = dtReceituarioOftalmologico;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}

	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}

	public String getNrLongeOdCilindrico() {
		return nrLongeOdCilindrico;
	}

	public void setNrLongeOdCilindrico(String nrLongeOdCilindrico) {
		this.nrLongeOdCilindrico = nrLongeOdCilindrico;
	}

	public String getNrLongeOdEixo() {
		return nrLongeOdEixo;
	}

	public void setNrLongeOdEixo(String nrLongeOdEixo) {
		this.nrLongeOdEixo = nrLongeOdEixo;
	}

	public String getNrLongeOdEsferico() {
		return nrLongeOdEsferico;
	}

	public void setNrLongeOdEsferico(String nrLongeOdEsferico) {
		this.nrLongeOdEsferico = nrLongeOdEsferico;
	}

	public String getNrLongeOeCilindrico() {
		return nrLongeOeCilindrico;
	}

	public void setNrLongeOeCilindrico(String nrLongeOeCilindrico) {
		this.nrLongeOeCilindrico = nrLongeOeCilindrico;
	}

	public String getNrLongeOeEixo() {
		return nrLongeOeEixo;
	}

	public void setNrLongeOeEixo(String nrLongeOeEixo) {
		this.nrLongeOeEixo = nrLongeOeEixo;
	}

	public String getNrLongeOeEsferico() {
		return nrLongeOeEsferico;
	}

	public void setNrLongeOeEsferico(String nrLongeOeEsferico) {
		this.nrLongeOeEsferico = nrLongeOeEsferico;
	}

	public String getNrPertoOdCilindrico() {
		return nrPertoOdCilindrico;
	}

	public void setNrPertoOdCilindrico(String nrPertoOdCilindrico) {
		this.nrPertoOdCilindrico = nrPertoOdCilindrico;
	}

	public String getNrPertoOdEixo() {
		return nrPertoOdEixo;
	}

	public void setNrPertoOdEixo(String nrPertoOdEixo) {
		this.nrPertoOdEixo = nrPertoOdEixo;
	}

	public String getNrPertoOdEsferico() {
		return nrPertoOdEsferico;
	}

	public void setNrPertoOdEsferico(String nrPertoOdEsferico) {
		this.nrPertoOdEsferico = nrPertoOdEsferico;
	}

	public String getNrPertoOeCilindrico() {
		return nrPertoOeCilindrico;
	}

	public void setNrPertoOeCilindrico(String nrPertoOeCilindrico) {
		this.nrPertoOeCilindrico = nrPertoOeCilindrico;
	}

	public String getNrPertoOeEixo() {
		return nrPertoOeEixo;
	}

	public void setNrPertoOeEixo(String nrPertoOeEixo) {
		this.nrPertoOeEixo = nrPertoOeEixo;
	}

	public String getNrPertoOeEsferico() {
		return nrPertoOeEsferico;
	}

	public void setNrPertoOeEsferico(String nrPertoOeEsferico) {
		this.nrPertoOeEsferico = nrPertoOeEsferico;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

}