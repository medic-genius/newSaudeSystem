package br.com.medic.medicsystem.persistence.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog="realvida", name="tbavaliacaoagendamento")
public class AvaliacaoAgendamento {
	@Id
	@Column(name="idagendamento")
	private Long idAgendamento;
	
	@Basic
	@Column(name="nota")
	private Integer nota;
	
	@Basic
	@Column(name="comentario")
	private String comentario;
	
	@Basic
	@Column(name="dtavaliacao")
	private Date dtAvaliacao;

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getDtAvaliacao() {
		return dtAvaliacao;
	}

	public void setDtAvaliacao(Date dtAvaliacao) {
		this.dtAvaliacao = dtAvaliacao;
	}
}
