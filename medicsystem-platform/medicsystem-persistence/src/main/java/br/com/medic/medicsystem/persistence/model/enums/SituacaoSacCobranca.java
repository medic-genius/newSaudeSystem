package br.com.medic.medicsystem.persistence.model.enums;

public enum SituacaoSacCobranca {

	ABERTO(0), FECHADO(1);
	
	private Integer id;
	
	private SituacaoSacCobranca(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
