package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.List;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class DespesaFinanceiroAgrupadaReportDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<DespesaFinanceiroServicoDTO> despesaFinanceiroServico;
	
	private Double vlTotalDespesas;
	
	private Double vlTotalDespesasPagas;
	
	private String vlTotalDespesasFormatado;
	
	private String vlTotalDespesasPagasFormatado;
	
	public DespesaFinanceiroAgrupadaReportDTO( List<DespesaFinanceiroServicoDTO> despesaFinanceiroServico, Double vlTotalDespesas, Double vlTotalDespesasPagas ) {
		this.despesaFinanceiroServico =  despesaFinanceiroServico;
		this.vlTotalDespesas = vlTotalDespesas;
		this.vlTotalDespesasPagas = vlTotalDespesasPagas;
		this.vlTotalDespesasFormatado = Utils.converterDoubelToCurrencyReal( vlTotalDespesas );
		this.vlTotalDespesasPagasFormatado = Utils.converterDoubelToCurrencyReal( vlTotalDespesasPagas );
	}
	

	public List<DespesaFinanceiroServicoDTO> getDespesaFinanceiroServico() {
		return despesaFinanceiroServico;
	}

	public void setDespesaFinanceiroServico(List<DespesaFinanceiroServicoDTO> despesaFinanceiroServico) {
		this.despesaFinanceiroServico = despesaFinanceiroServico;
	}

	public Double getVlTotalDespesas() {
		return vlTotalDespesas;
	}

	public void setVlTotalDespesas(Double vlTotalDespesas) {
		this.vlTotalDespesas = vlTotalDespesas;
	}

	public Double getVlTotalDespesasPagas() {
		return vlTotalDespesasPagas;
	}

	public void setVlTotalDespesasPagas(Double vlTotalDespesasPagas) {
		this.vlTotalDespesasPagas = vlTotalDespesasPagas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getVlTotalDespesasFormatado() {
		return vlTotalDespesasFormatado;
	}
	
	public void setVlTotalDespesasFormatado(String vlTotalDespesasFormatado) {
		this.vlTotalDespesasFormatado = vlTotalDespesasFormatado;
	}
	
	public String getVlTotalDespesasPagasFormatado() {
		return vlTotalDespesasPagasFormatado;
	}
	
	public void setVlTotalDespesasPagasFormatado(String vlTotalDespesasPagasFormatado) {
		this.vlTotalDespesasPagasFormatado = vlTotalDespesasPagasFormatado;
	}
	
	
}
