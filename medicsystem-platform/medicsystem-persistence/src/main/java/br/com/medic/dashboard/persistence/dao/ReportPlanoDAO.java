package br.com.medic.dashboard.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.dashboard.persistence.dataaccess.MLRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.ClientePlano;
import br.com.medic.dashboard.persistence.model.FaturamentoPlano;

@Named("ml-report-plano-dao")
@ApplicationScoped
public class ReportPlanoDAO extends
        MLRelationalDataAccessObject<FaturamentoPlano> {

	@SuppressWarnings("unchecked")
	public List<FaturamentoPlano> getReportFaturamentoPlanos() {

		String query = "SELECT "
		        + "pln.idplano as idplano, pln.nmplano AS nomeplano, pln.vlplano AS valorplano, "
		        + "count(cont.idcontrato) AS contratos, sum(cont.vltotal) AS totalreal, "
		        + "( "
		        + "SELECT SUM(qtd) FROM "
		        + "( "
		        + "SELECT count(*) as qtd FROM "
		        + "( "
		        + "SELECT cli.idcliente as qtd "
		        + "FROM realvida.tbcliente cli "
		        + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
		        + "INNER JOIN realvida.tbcontrato con ON con.idcontrato = contcli.idcontrato "
		        + "INNER JOIN realvida.tbplano pln1 ON pln1.idplano = con.idplano "
		        + "WHERE pln1.idplano = pln.idplano "
		        + "AND con.idempresagrupo = 9 "
		        + "AND con.insituacao = 0 "
		        + "AND con.bonaofazusoplano = false "
		        + "AND con.informapagamento is not null "
		        + "GROUP BY cli.idcliente, con.idcontrato "
		        + ") as qtd "
		        + "UNION ALL "
		        + "SELECT count(cd.iddependente) as qtd "
		        + "FROM realvida.tbcontrato c "
		        + "INNER JOIN realvida.tbcontratodependente cd on c.idcontrato = cd.idcontrato "
		        + "INNER JOIN realvida.tbcontratocliente contcli on c.idcontrato = contcli.idcontrato "
		        + "WHERE c.insituacao = 0 "
		        + "AND cd.insituacao = 0 "
		        + "AND c.idplano = pln.idplano "
		        + "AND c.idempresagrupo = 9 "
		        + "AND c.informapagamento is not null "
		        + ") quantidade "
		        + ") as vidas "
		        + "FROM realvida.tbcontrato cont "
		        + "INNER JOIN realvida.tbcontratocliente cc ON cc.idcontrato = cont.idcontrato "
		        + "INNER JOIN realvida.tbcliente client ON client.idcliente = cc.idcliente "
		        + "INNER JOIN realvida.tbplano pln on  pln.idplano = cont.idplano "
		        + "WHERE cont.insituacao = 0 "
		        + "AND cont.idempresagrupo = 9 "
		        + "AND pln.intipoplano = 0" 
		        + "GROUP BY pln.idplano "
		        + "ORDER BY totalreal DESC";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idplano", LongType.INSTANCE)
		        .addScalar("nomeplano", StringType.INSTANCE)
		        .addScalar("valorplano", DoubleType.INSTANCE)
		        .addScalar("contratos", IntegerType.INSTANCE)
		        .addScalar("totalreal", DoubleType.INSTANCE)
		        .addScalar("vidas", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(FaturamentoPlano.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<FaturamentoPlano> getReportFaturamentoPlanosEmpresarial() {

		String query = "SELECT "
		        + "pln.idplano as idplano, pln.nmplano AS nomeplano, pln.vlplano AS valorplano, "
		        + "count(cont.idcontrato) AS contratos, sum(cont.vltotal) AS totalreal, "
		        + "( "
		        + "SELECT SUM(qtd) FROM "
		        + "( "
		        + "SELECT count(*) as qtd FROM "
		        + "( "
		        + "SELECT cli.idcliente as qtd "
		        + "FROM realvida.tbcliente cli "
		        + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
		        + "INNER JOIN realvida.tbcontrato con ON con.idcontrato = contcli.idcontrato "
		        + "INNER JOIN realvida.tbplano pln1 ON pln1.idplano = con.idplano "
		        + "WHERE pln1.idplano = pln.idplano "
		        + "AND con.idempresagrupo = 9 "
		        + "AND con.insituacao = 0 "
		        + "AND con.bonaofazusoplano = false "
		        + "AND con.informapagamento is not null "
		        + "GROUP BY cli.idcliente, con.idcontrato "
		        + ") as qtd "
		        + "UNION ALL "
		        + "SELECT count(cd.iddependente) as qtd "
		        + "FROM realvida.tbcontrato c "
		        + "INNER JOIN realvida.tbcontratodependente cd on c.idcontrato = cd.idcontrato "
		        + "INNER JOIN realvida.tbcontratocliente contcli on c.idcontrato = contcli.idcontrato "
		        + "WHERE c.insituacao = 0 "
		        + "AND cd.insituacao = 0 "
		        + "AND c.idplano = pln.idplano "
		        + "AND c.idempresagrupo = 9 "
		        + "AND c.informapagamento is not null "
		        + ") quantidade "
		        + ") as vidas "
		        + "FROM realvida.tbcontrato cont "
		        + "INNER JOIN realvida.tbcontratocliente cc ON cc.idcontrato = cont.idcontrato "
		        + "INNER JOIN realvida.tbcliente client ON client.idcliente = cc.idcliente "
		        + "INNER JOIN realvida.tbplano pln on  pln.idplano = cont.idplano "
		        + "WHERE cont.insituacao = 0 "
		        + "AND cont.idempresagrupo = 9 "
		        + "AND pln.intipoplano = 2 " 
		        + "GROUP BY pln.idplano "
		        + "ORDER BY totalreal DESC";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idplano", LongType.INSTANCE)
		        .addScalar("nomeplano", StringType.INSTANCE)
		        .addScalar("valorplano", DoubleType.INSTANCE)
		        .addScalar("contratos", IntegerType.INSTANCE)
		        .addScalar("totalreal", DoubleType.INSTANCE)
		        .addScalar("vidas", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(FaturamentoPlano.class))
		        .list();

	}

	@SuppressWarnings("unchecked")
	public List<ClientePlano> getReportListaClientesPorPlano(Integer idPlano, Integer qtdAtendimento, Integer qtdAtendimentoFim, Float custoVida, Integer inFormaPagamento, Float ultimoPagamento) {
		
		String query, orderBy, where = "";
		
		query = 
				"SELECT * FROM ( "
				+ "SELECT cli.idcliente, cont.nrcontrato AS numerocontrato,  "
		        + "CASE  "
		        + "WHEN cont.bobloqueado = true THEN 'BLOQUEADO'  "
		        + "WHEN cont.bobloqueado = false THEN 'LIBERADO'  "
		        + "END AS situacao,  "
		        + "CASE   "
		        + "WHEN cont.informapagamento = 1 THEN 'DEBITO'   "
		        + "WHEN cont.informapagamento = 0 THEN 'BOLETO'  "
		        + "WHEN cont.informapagamento = 2 THEN 'CONTRA CHEQUE'  "
		        + "WHEN cont.informapagamento = 30 THEN 'RECORRENTE' "
		        + "ELSE 'OUTROS'  "
		        + "END AS FormaPagamento,    "
		        + "cli.nmcliente AS nomecliente,  "
		        + "( "
		        + "SELECT SUM(quantidade)  "
		        + "FROM ( "
		        + "SELECT count(*) as quantidade  "
		        + "FROM (  "
		        + "SELECT clit.idcliente  "
		        + "FROM realvida.tbcliente clit  "
		        + "INNER JOIN realvida.tbcontratocliente contcliente ON contcliente.idcliente = clit.idcliente  "
		        + "INNER JOIN realvida.tbcontrato con ON contcliente.idcontrato = con.idcontrato "
		        + "INNER JOIN realvida.tbplano pln1 ON pln1.idplano = con.idplano  "
		        + "WHERE pln1.idplano = pln.idplano  "
		        + "AND con.idempresagrupo = 9  "
		        + "AND con.insituacao = 0  	 "
		        + "AND con.idcontrato = cont.idcontrato "
		        + "AND con.bonaofazusoplano is false  "
		        + "AND con.informapagamento is not null "
		        + "group by clit.idcliente  "
		        + ") quantidade  "
		        + "UNION ALL  "
		        + "SELECT count(cd.iddependente) AS quantidade "
		        + "FROM realvida.tbcontrato c  "
		        + "INNER JOIN realvida.tbcontratodependente cd on  cd.idcontrato = c.idcontrato "
		        + "INNER JOIN realvida.tbcontratocliente contclit ON contclit.idcontrato = c.idcontrato  "
		        + "WHERE c.insituacao = 0  "
		        + "AND cd.insituacao = 0 "
		        + "AND c.idempresagrupo = 9  "
		        + "AND c.idplano = pln.idplano  "
		        + "AND c.idcontrato = cont.idcontrato "
		        + "AND c.informapagamento is not null "
		        + ") AS vida "
		        + ") as vidas, "
		        + "pln.vlplano AS plano,   "
		        + "cont.vltotal AS cobrado,  "
		        + "men.vlpago as ultimopagamento, "
		        + "cli.nrtelefone as telefone,  "
		        + "cli.nrcelular as celular,  "
		        + "cli.nmlogradouro as endereco,  "
		        + "cli.nrnumero as numeroendereco, "
		        + "(	select count(idagendamento) from realvida.tbagendamento ag " 
	        	+ "		where ag.dtexclusao is null and ag.instatus = 2	"
	        	+ "		and ag.idcontrato = cont.idcontrato "
	        	+ "		and ag.dtagendamento between current_date - interval '12 months' and current_date "
	        	+ ") as qtdatendimento "		        
		        + "FROM realvida.tbcliente cli  "
		        + "INNER JOIN realvida.tbcontratocliente contcli ON cli.idcliente = contcli.idcliente "
		        + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
		        + "INNER JOIN realvida.tbplano pln ON pln.idplano = cont.idplano "
		        + "LEFT JOIN realvida.tbmensalidade men on cont.idcontrato = men.idcontrato " 
		        + " and men.idmensalidade = (select max(idmensalidade) from realvida.tbmensalidade where dtexclusao is null and instatus = 1 and idcontrato = cont.idcontrato) "
		        + "WHERE pln.idplano = " + idPlano + (inFormaPagamento != null &&  inFormaPagamento != 400 ? " and cont.informapagamento = "+inFormaPagamento: "")
		        + " AND cont.insituacao = 0 "
		        + "AND cont.idempresagrupo = 9 "
		        + "AND cont.informapagamento is not null "
		        + "group by pln.idplano, cont.idcontrato, cli.idcliente, men.vlpago	"
		        + ")t1 ";
		
		if(qtdAtendimento != null || qtdAtendimentoFim != null)
			where += " where qtdatendimento between " + qtdAtendimento + " and " + qtdAtendimentoFim;
		
		if(custoVida != null)
			where += where.length() > 0 ? " and ((t1.vidas > 0 and (t1.cobrado/t1.vidas) <= " + custoVida + ") or (t1.vidas = 0))" : " where ((t1.vidas > 0 and (t1.cobrado/t1.vidas) <= " + custoVida + ") or (t1.vidas = 0))";
		
		if(ultimoPagamento != null)
			where += where.length() > 0 ? " and ultimopagamento <= " + ultimoPagamento : " where ultimopagamento <= " + ultimoPagamento;
		
		orderBy = " order by t1.nomecliente";

		return findByNativeQuery(query + where + orderBy)
		        .unwrap(SQLQuery.class)
		        .addScalar("numerocontrato", StringType.INSTANCE)
		        .addScalar("situacao", StringType.INSTANCE)
		        .addScalar("formapagamento", StringType.INSTANCE)
		        .addScalar("nomecliente", StringType.INSTANCE)
		        .addScalar("vidas", IntegerType.INSTANCE)
		        .addScalar("plano", DoubleType.INSTANCE)
		        .addScalar("cobrado", DoubleType.INSTANCE)
		        .addScalar("ultimopagamento", DoubleType.INSTANCE)
		        .addScalar("telefone", StringType.INSTANCE)
		        .addScalar("celular", StringType.INSTANCE)
		        .addScalar("endereco", StringType.INSTANCE)
		        .addScalar("numeroendereco", StringType.INSTANCE)
		        .addScalar("qtdatendimento", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ClientePlano.class)).list();
	}

}
