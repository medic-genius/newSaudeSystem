package br.com.medic.medicsystem.persistence.dto;

public class DespesaSiteDTO {
	
	private Long idAgendamento;
	private Boolean isConvenio;
	private Double vlDespesa;
	private Integer inStatusAgendamento;
	private String nmPlano;
	private String nrContrato;
	
	

	
	
	public DespesaSiteDTO(Long idAgendamento, Boolean isConvenio,
			Double vlDespesa, Integer inStatusAgendamento, String nmPlano,
			String nrContrato) {
		this.idAgendamento = idAgendamento;
		this.isConvenio = isConvenio;
		this.vlDespesa = vlDespesa;
		this.inStatusAgendamento = inStatusAgendamento;
		this.nmPlano = nmPlano;
		this.nrContrato = nrContrato;
		
	}
	
	
	public Long getIdAgendamento() {
		return idAgendamento;
	}
	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	public Boolean getIsConvenio() {
		return isConvenio;
	}
	public void setIsConvenio(Boolean isConvenio) {
		this.isConvenio = isConvenio;
	}
	public Double getVlDespesa() {
		return vlDespesa;
	}
	public void setVlDespesa(Double vlDespesa) {
		this.vlDespesa = vlDespesa;
	}
	public Integer getInStatusAgendamento() {
		return inStatusAgendamento;
	}
	public void setInStatusAgendamento(Integer inStatusAgendamento) {
		this.inStatusAgendamento = inStatusAgendamento;
	}


	public String getNmPlano() {
		return nmPlano;
	}


	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}


	public String getNrContrato() {
		return nrContrato;
	}


	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	

}
