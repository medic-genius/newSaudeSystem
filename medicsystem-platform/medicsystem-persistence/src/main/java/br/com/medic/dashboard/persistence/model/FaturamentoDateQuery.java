package br.com.medic.dashboard.persistence.model;

public class FaturamentoDateQuery {
	
	private String mes;
	
	private String dataInicioMes;
	
	private String dataFimMes;
	
	public FaturamentoDateQuery() {
	    // TODO Auto-generated constructor stub
    }

	public FaturamentoDateQuery(String mes, String dataInicioMes,
            String dataFimMes) {
	    super();
	    this.mes = mes;
	    this.dataInicioMes = dataInicioMes;
	    this.dataFimMes = dataFimMes;
    }

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getDataInicioMes() {
		return dataInicioMes;
	}

	public void setDataInicioMes(String dataInicioMes) {
		this.dataInicioMes = dataInicioMes;
	}

	public String getDataFimMes() {
		return dataFimMes;
	}

	public void setDataFimMes(String dataFimMes) {
		this.dataFimMes = dataFimMes;
	}
	
	

}
