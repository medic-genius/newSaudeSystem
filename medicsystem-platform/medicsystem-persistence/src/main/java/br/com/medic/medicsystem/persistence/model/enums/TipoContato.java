package br.com.medic.medicsystem.persistence.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum TipoContato {

		TELEFONE(0, "Telefone"), WHATSAPP(1, "WhatsApp"), SMS(2, "SMS"), FACEBOOK(3, "Facebook"), INLOCO(4, "In Loco");
		
		private Integer id;
		private String description;

		private TipoContato(int ordinal, String name) {
			this.id = ordinal;
			this.description = name;
		}

		public static TipoContato getTipoContatoById(int id){
			for (TipoContato tipo : TipoContato.values()) {
				if(id == tipo.id)
					return tipo;
			}
			return null;
		}
		
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
		
}
