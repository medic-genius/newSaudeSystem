package br.com.medic.medicsystem.persistence.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="realvida", name="tbmedicamentotemplatereceituario")
public class MedicamentoTemplateReceituario {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEDICAMENTOTEMPLATERECEITUARIO_ID_SEQ")
	@SequenceGenerator(name = "MEDICAMENTOTEMPLATERECEITUARIO_ID_SEQ", sequenceName = "realvida.idmedicamentotemplatereceituario_seq" , allocationSize = 1)
	@Column(name = "idmedicamentotemplatereceituario")
	private Long id;
	
	@Basic
	@Column(name = "idtemplatereceituario")
	private Long idTemplateReceituario;
	
	@ManyToOne
	@JoinColumn(name = "idmedicamento", referencedColumnName="codigo")
	private Medicamento medicamento;
	
	@Basic
	@Column(name = "nmmedicamento")
	private String nmMedicamento;
	
	@Basic
	@Column(name = "nmposologia")
	private String nmPosologia;
	
	@Basic
	@Column(name = "nmorientacoes")
	private String nmOrientacoes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdTemplateReceituario() {
		return idTemplateReceituario;
	}

	public void setIdTemplateReceituario(Long idTemplateReceituario) {
		this.idTemplateReceituario = idTemplateReceituario;
	}

	public Medicamento getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public String getNmMedicamento() {
		return nmMedicamento;
	}

	public void setNmMedicamento(String nmMedicamento) {
		this.nmMedicamento = nmMedicamento;
	}

	public String getNmPosologia() {
		return nmPosologia;
	}

	public void setNmPosologia(String nmPosologia) {
		this.nmPosologia = nmPosologia;
	}

	public String getNmOrientacoes() {
		return nmOrientacoes;
	}

	public void setNmOrientacoes(String nmOrientacoes) {
		this.nmOrientacoes = nmOrientacoes;
	}
}
