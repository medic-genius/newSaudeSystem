package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.DespesaServico;

@Named("despesaservico-dao")
@ApplicationScoped
public class DespesaServicoDAO extends
        RelationalDataAccessObject<DespesaServico> {

	public List<DespesaServico> getDespesaServicoPorEncaminhamento(
	        Long idEncaminhamento) {

		TypedQuery<DespesaServico> query = entityManager.createQuery(
		        "SELECT ds from DespesaServico ds where ds.encaminhamento.id = "
		                + idEncaminhamento, DespesaServico.class);

		List<DespesaServico> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

}
