package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SituacaoDTO {
	
	private String nrcontrato;
	
	private String nmplano;

	private String nmformacombinada;

	private Integer qtdaberto;

	private String nmsituacao;

	private Date dtvencimento;

	public SituacaoDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getNmformacombinada() {
		return nmformacombinada;
	}

	public void setNmformacombinada(String nmformacombinada) {
		this.nmformacombinada = nmformacombinada;
	}

	public Integer getQtdaberto() {
		return qtdaberto;
	}

	public void setQtdaberto(Integer qtdaberto) {
		this.qtdaberto = qtdaberto;
	}

	public String getNmsituacao() {
		return nmsituacao;
	}

	public void setNmsituacao(String nmsituacao) {
		this.nmsituacao = nmsituacao;
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public String getDtvencimentoFormatado() {
		if (getDtvencimento() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtvencimento());
		}
		return null;
	}

	public String getNrcontrato() {
		return nrcontrato;
	}

	public void setNrcontrato(String nrcontrato) {
		this.nrcontrato = nrcontrato;
	}

	public String getNmplano() {
		return nmplano;
	}

	public void setNmplano(String nmplano) {
		this.nmplano = nmplano;
	}
	
	

}
