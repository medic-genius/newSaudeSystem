package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

public class RegistroAgendamentoDTO {
	
	private Double vlSalario;
	
	private String totalHr;
	
	private Integer totalServico; 
		
	private List<RegistroAtaDTO> listAtendimentoCliente;
	
	private List<RegistroAtaDTO> listHorarioAtendimento;
	
	private List<RegistroAtaDTO> listFixoAtendimento;
	
	private List<RegistroAtaDTO> listRespTecnico;
	
	
	public List<RegistroAtaDTO> getListAtendimentoCliente() {
		return listAtendimentoCliente;
	}

	public void setListAtendimentoCliente(List<RegistroAtaDTO> listAtendimentoCliente) {
		this.listAtendimentoCliente = listAtendimentoCliente;
	}

	public List<RegistroAtaDTO> getListHorarioAtendimento() {
		return listHorarioAtendimento;
	}

	public void setListHorarioAtendimento(List<RegistroAtaDTO> listHorarioAtendimento) {
		this.listHorarioAtendimento = listHorarioAtendimento;
	}

	public Double getVlSalario() {
		return vlSalario;
	}

	public void setVlSalario(Double vlSalario) {
		this.vlSalario = vlSalario;
	}
	
	public Integer getTotalServico() {
		return totalServico;
	}

	public void setTotalServico(Integer totalServico) {
		this.totalServico = totalServico;
	}

	public String getTotalHr() {
		return totalHr;
	}

	public void setTotalHr(String totalHr) {
		this.totalHr = totalHr;
	}

	public List<RegistroAtaDTO> getListFixoAtendimento() {
		return listFixoAtendimento;
	}

	public void setListFixoAtendimento(List<RegistroAtaDTO> listFixoAtendimento) {
		this.listFixoAtendimento = listFixoAtendimento;
	}

	public List<RegistroAtaDTO> getListRespTecnico() {
		return listRespTecnico;
	}

	public void setListRespTecnico(List<RegistroAtaDTO> listRespTecnico) {
		this.listRespTecnico = listRespTecnico;
	}


}
