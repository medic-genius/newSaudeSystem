package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tborcamento")
public class Orcamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORCAMENTO_ID_SEQ")
	@SequenceGenerator(name = "ORCAMENTO_ID_SEQ", sequenceName = "realvida.orcamento_id_seq")
	@Column(name = "idorcamento")
	private Long id;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtorcamento")
	private Date dtOrcamento;
	
	@Basic
	@Column(name = "idorcamentoaux")
	private Long idOrcamentoAux;
	
	@Basic
	@Column(name = "idorcamentoodonto")
	private Long idOrcamentoOdonto;
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;
	
	@Basic
	@Column(name = "vltotal")
	private float vlTotal;

	//bi-directional many-to-one association to Tbagendamento
	@ManyToOne
	@JoinColumn(name="idagendamento")
	private Agendamento agendamento;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Tbdependente
	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idfuncionariocancelamento")
	private Funcionario funcionario;

	public Orcamento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtOrcamento() {
		return dtOrcamento;
	}

	public void setDtOrcamento(Date dtOrcamento) {
		this.dtOrcamento = dtOrcamento;
	}

	public Long getIdOrcamentoAux() {
		return idOrcamentoAux;
	}

	public void setIdOrcamentoAux(Long idOrcamentoAux) {
		this.idOrcamentoAux = idOrcamentoAux;
	}

	public Long getIdOrcamentoOdonto() {
		return idOrcamentoOdonto;
	}

	public void setIdOrcamentoOdonto(Long idOrcamentoOdonto) {
		this.idOrcamentoOdonto = idOrcamentoOdonto;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public float getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(float vlTotal) {
		this.vlTotal = vlTotal;
	}

	public Agendamento getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}