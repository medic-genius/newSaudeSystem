package br.com.medic.medicsystem.persistence.dto;

import java.util.Date;

public class PacienteViewDTO {
	
	private String nrUniqueId;
	private String nmPaciente;
	private String nrCPF;
	private Integer idDedoDigital;
	private String digitalPaciente;
	private Boolean hasFotoPaciente;
	private String nrCodPaciente;
	private Date dtNascimento;
	private Integer inSexo;
	private String nrCelular;
	private String nmEmail;
	
	
	public String getNrUniqueId() {
		return nrUniqueId;
	}
	public void setNrUniqueId(String nrUniqueId) {
		this.nrUniqueId = nrUniqueId;
	}
	public String getNmPaciente() {
		return nmPaciente;
	}
	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	public String getNrCPF() {
		return nrCPF;
	}
	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}
	public Integer getIdDedoDigital() {
		return idDedoDigital;
	}
	public void setIdDedoDigital(Integer idDedoDigital) {
		this.idDedoDigital = idDedoDigital;
	}
	public String getDigitalPaciente() {
		return digitalPaciente;
	}
	public void setDigitalPaciente(String digitalPaciente) {
		this.digitalPaciente = digitalPaciente;
	}
	public Boolean getHasFotoPaciente() {
		return hasFotoPaciente;
	}
	public void setHasFotoPaciente(Boolean hasFotoPaciente) {
		this.hasFotoPaciente = hasFotoPaciente;
	}
	public String getNrCodPaciente() {
		return nrCodPaciente;
	}
	public void setNrCodPaciente(String nrCodPaciente) {
		this.nrCodPaciente = nrCodPaciente;
	}
	public Date getDtNascimento() {
		return dtNascimento;
	}
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public Integer getInSexo() {
		return inSexo;
	}
	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}
	public String getNrCelular() {
		return nrCelular;
	}
	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}
	public String getNmEmail() {
		return nmEmail;
	}
	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}
	
	

}
