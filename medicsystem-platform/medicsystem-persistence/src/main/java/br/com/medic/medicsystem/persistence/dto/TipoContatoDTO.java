package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;

public class TipoContatoDTO implements Serializable {

	private static final long serialVersionUID = -6218657991670250687L;
	private int tipo;
	private String nmTipo;

	public TipoContatoDTO() {

	}
	
	public TipoContatoDTO(int tipo, String nmTipo) {
		this.tipo = tipo;
		this.nmTipo = nmTipo;
	}
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getNmTipo() {
		return nmTipo;
	}
	public void setNmTipo(String nmTipo) {
		this.nmTipo = nmTipo;
	}
	
}
