package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(catalog = "realvida", name = "tbtemplateprofissional")
public class TemplateProfissional implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEMPLATEPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "TEMPLATEPROFISSIONAL_ID_SEQ", sequenceName = "realvida.templateprofissional_id_seq" , allocationSize = 1)
	@Column(name = "idtemplateprofissional")
	private Long id;
		
	@Basic
	@NotNull
	@Column(name = "nmtemplate")
	private String nmTemplate;
	
	@Basic
	@NotNull
	@Column(name = "conteudo", length = 5000)
	private String nmConteudo;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name="idespprofissional")
	private EspProfissional espProfissional;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Date dtInclusaoLog;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Date dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmTemplate() {
		return nmTemplate;
	}

	public void setNmTemplate(String nmTemplate) {
		this.nmTemplate = nmTemplate;
	}

	public String getNmConteudo() {
		return nmConteudo;
	}

	public void setNmConteudo(String nmConteudo) {
		this.nmConteudo = nmConteudo;
	}

	public EspProfissional getEspProfissional() {
		return espProfissional;
	}

	public void setEspProfissional(EspProfissional espProfissional) {
		this.espProfissional = espProfissional;
	}

	public Date getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Date dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Date dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}
}
