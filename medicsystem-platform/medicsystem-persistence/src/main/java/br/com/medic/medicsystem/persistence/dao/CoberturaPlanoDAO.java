package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.CoberturaPlanoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;

@Named("coberturaplano-dao")
@ApplicationScoped
public class CoberturaPlanoDAO extends RelationalDataAccessObject<CoberturaPlano> {

	@SuppressWarnings("unchecked")
	public List<CoberturaPlanoDTO> getCoberturaPlanoPorCliente(Long idCliente) {

		String query = "SELECT CPLAN.BOCOBERTURA,"
							+ "SERV.NMSERVICO AS SERVICO, "
							+ "SERV.NRCODSERVICO AS CODIGO, "
							+ "CASE SERV.INTIPOSERVICO "
								+ "WHEN 0 THEN 'MÉDICO' "
								+ "WHEN 1 THEN 'ODONTOLÓGICO' "
								+ "WHEN 2 THEN 'LABORATORIAL' "
							+ "ELSE 'NULL' "
							+ "END AS TIPO, "
							+ "CPLAN.VLSERVICOASSOCIADO AS VALORASSOCIADO, "
							+ "CPLAN.VLSERVICO AS VALORNAOASSOCIADO, "
							+ "CPLAN.CARENCIA AS CARENCIA, "
							+ "COALESCE(CPLAN.VALIDADECONTRATUAL,0) AS VALIDADECONTRATUAL, "
							+ "PLA.NMPLANO AS NMPLANO, "
							+ "CONT.NRCONTRATO AS NROCONTRATO "
							+ "FROM REALVIDA.TBSERVICO SERV "
							+ "INNER JOIN REALVIDA.TBCOBERTURAPLANO CPLAN ON CPLAN.IDSERVICO = SERV.IDSERVICO "
							+ "INNER JOIN REALVIDA.TBPLANO PLA ON PLA.IDPLANO = CPLAN.IDPLANO "
							+ "INNER JOIN REALVIDA.TBCONTRATO CONT on CONT.IDPLANO = PLA.IDPLANO "
							+ "INNER JOIN REALVIDA.TBCONTRATOCLIENTE CCLI ON CCLI.IDCONTRATO = CONT.IDCONTRATO "
							+ "INNER JOIN REALVIDA.TBCLIENTE CLI ON CLI.IDCLIENTE = CCLI.IDCLIENTE "							
							+ "WHERE CLI.IDCLIENTE = " + idCliente + " AND CONT.INSITUACAO = 0 "
							+ "GROUP BY SERV.NMSERVICO," 
									 + "SERV.NRCODSERVICO, "
									 + "SERV.INTIPOSERVICO, " 
									 + "CPLAN.VLSERVICOASSOCIADO, "
									 + "CPLAN.VLSERVICO, " 
									 + "CPLAN.CARENCIA, " 
									 + "PLA.NMPLANO, "
									 + "CONT.NRCONTRATO, "
									 + "CPLAN.BOCOBERTURA, " 
									 + "CPLAN.VALIDADECONTRATUAL "
							+ "ORDER BY PLA.NMPLANO ";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("boCobertura", BooleanType.INSTANCE)
		        .addScalar("servico", StringType.INSTANCE)
   		        .addScalar("codigo", StringType.INSTANCE)
		        .addScalar("tipo", StringType.INSTANCE)
		        .addScalar("valorAssociado", DoubleType.INSTANCE)
   		        .addScalar("valorNaoAssociado", DoubleType.INSTANCE)
		        .addScalar("carencia", IntegerType.INSTANCE)
		        .addScalar("nmPlano", StringType.INSTANCE)
		        .addScalar("nroContrato", StringType.INSTANCE)
		        .addScalar("validadeContratual", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(CoberturaPlanoDTO.class))
		        .list();
	}
	
	public List<CoberturaPlano> getCoberturaPlanoPorPlano(Long idPlano){
		
		String queryStr = "SELECT cp FROM CoberturaPlano cp Where cp.plano.id = " + idPlano;
				
		TypedQuery<CoberturaPlano> query = entityManager.createQuery(queryStr,
				CoberturaPlano.class);
		
		
		List<CoberturaPlano> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
}
