package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;

/**
 * Classe utilizada para criar dtos que precisam apenas de um nome e um id.
 * Pode ser usado de forma genérica por qualquer classe nesta conjectura.
 * */
public class BaseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String description;
	private String opcionalInfo;
	
	public BaseDTO() {

	}

	public BaseDTO(Long id) {
		this.id = id;
	}
	
	public BaseDTO(Long id, String description) {
		this.id = id;
		this.description = description;
	}

	public BaseDTO(Long id, String description, String opcionalInfo) {
		this.id = id;
		this.description = description;
		this.opcionalInfo = opcionalInfo;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOpcionalInfo() {
		return opcionalInfo;
	}

	public void setOpcionalInfo(String opcionalInfo) {
		this.opcionalInfo = opcionalInfo;
	}
	
}
