package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbmovimentacaopagto database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbmovimentacaopagto")
public class MovimentacaoPagto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOVIMENTACAOPAGTO_ID_SEQ")
	@SequenceGenerator(name = "MOVIMENTACAOPAGTO_ID_SEQ", sequenceName = "realvida.movimentacaopagto_id_seq",allocationSize = 1)
	@Column(name = "idmovdetalhamento")
	private Long idmovdetalhamento;

	private Boolean boexcluido;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private Integer inadministradoracartaocredito;

	private Integer inadministradoracartaodebito;

	private Integer informapagamento;

	private Integer qtparcelas;

	private Float vlmovdetalhamento;

	//bi-directional many-to-one association to Tbmovimentacaocaixa
	@ManyToOne
	@JoinColumn(name="idmovimentacaocaixa")
	private MovimentacaoCaixa tbmovimentacaocaixa;

	public MovimentacaoPagto() {
	}

	public Long getIdmovdetalhamento() {
		return this.idmovdetalhamento;
	}

	public void setIdmovdetalhamento(Long idmovdetalhamento) {
		this.idmovdetalhamento = idmovdetalhamento;
	}

	public Boolean getBoexcluido() {
		return this.boexcluido;
	}

	public void setBoexcluido(Boolean boexcluido) {
		this.boexcluido = boexcluido;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Integer getInadministradoracartaocredito() {
		return this.inadministradoracartaocredito;
	}

	public void setInadministradoracartaocredito(Integer inadministradoracartaocredito) {
		this.inadministradoracartaocredito = inadministradoracartaocredito;
	}

	public Integer getInadministradoracartaodebito() {
		return this.inadministradoracartaodebito;
	}

	public void setInadministradoracartaodebito(Integer inadministradoracartaodebito) {
		this.inadministradoracartaodebito = inadministradoracartaodebito;
	}

	public Integer getInformapagamento() {
		return this.informapagamento;
	}

	public void setInformapagamento(Integer informapagamento) {
		this.informapagamento = informapagamento;
	}

	public Integer getQtparcelas() {
		return this.qtparcelas;
	}

	public void setQtparcelas(Integer qtparcelas) {
		this.qtparcelas = qtparcelas;
	}

	public Float getVlmovdetalhamento() {
		return vlmovdetalhamento;
	}
	
	public void setVlmovdetalhamento(Float vlmovdetalhamento) {
		this.vlmovdetalhamento = vlmovdetalhamento;
	}
	
	public MovimentacaoCaixa getTbmovimentacaocaixa() {
		return this.tbmovimentacaocaixa;
	}

	public void setMovimentacaoCaixa(MovimentacaoCaixa tbmovimentacaocaixa) {
		this.tbmovimentacaocaixa = tbmovimentacaocaixa;
	}

}