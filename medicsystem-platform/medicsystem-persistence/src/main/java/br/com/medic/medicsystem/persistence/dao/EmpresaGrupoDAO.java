package br.com.medic.medicsystem.persistence.dao;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;

@Named("empresagrupo-dao")
@ApplicationScoped
public class EmpresaGrupoDAO extends RelationalDataAccessObject<EmpresaGrupo> {

	public static final long ID_DEFAULT = 9L;
	
	public Collection<EmpresaGrupo> getEmpresaGrupos() {
		String queryStr = " select eg from EmpresaGrupo eg";

		return findByTypedQuery(queryStr, EmpresaGrupo.class);

	}

	public EmpresaGrupo getEmpresaGrupoDefault() 
	{
		return searchByKey(EmpresaGrupo.class, ID_DEFAULT);
	}
}
