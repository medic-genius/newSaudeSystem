package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class MovimentacaoItensContaDTO implements Serializable{

	private static final long serialVersionUID = -8116120755424054595L;

	private Date dtMovimentacao;
	private String nrDocumento;
	private Integer inTipo;
	private String nmAcaoMovimentacao;
	private Double nrDebito;
	private Double nrCredito;
	private Double nrSaldo;
	private String fornecedores;
	private String nmDescricao;
	private String descCaixa;
	private Boolean haveEstorno;
	private String infoEstorno;
	private Long idCaixaConferido;
	private Integer acaoMovimentacao;
	
	public MovimentacaoItensContaDTO() {

	}
	
	public MovimentacaoItensContaDTO(Date dtMovimentacao, String nrDocumento, Integer inTipo, String nmAcaoMovimentacao,
			Double nrDebito, Double nrCredito, Double nrSaldo, String fornecedores, String descricao, String descCaixa, Boolean haveEstorno, String infoEstorno, Long idCaixaConferido
			, Integer acaoMovimentacao) {
		this.dtMovimentacao = dtMovimentacao;
		this.nrDocumento = nrDocumento;
		this.inTipo = inTipo;
		this.nmAcaoMovimentacao = nmAcaoMovimentacao;
		this.nrDebito = nrDebito;
		this.nrCredito = nrCredito;
		this.nrSaldo = nrSaldo;
		this.fornecedores = fornecedores;
		this.nmDescricao = descricao;
		this.descCaixa = descCaixa;
		this.haveEstorno = haveEstorno;
		this.infoEstorno = infoEstorno;
		this.idCaixaConferido =idCaixaConferido;
		this.setAcaoMovimentacao(acaoMovimentacao);
	}

	public Date getDtMovimentacao() {
		return dtMovimentacao;
	}
	public void setDtMovimentacao(Date dtMovimentacao) {
		this.dtMovimentacao = dtMovimentacao;
	}
	public String getNrDocumento() {
		return nrDocumento;
	}
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	public Integer getInTipo() {
		return inTipo;
	}
	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}
	public String getNmAcaoMovimentacao() {
		return nmAcaoMovimentacao;
	}
	public void setNmAcaoMovimentacao(String nmAcaoMovimentacao) {
		this.nmAcaoMovimentacao = nmAcaoMovimentacao;
	}
	public Double getNrDebito() {
		return nrDebito;
	}
	public void setNrDebito(Double nrDebito) {
		this.nrDebito = nrDebito;
	}
	public Double getNrCredito() {
		return nrCredito;
	}
	public void setNrCredito(Double nrCredito) {
		this.nrCredito = nrCredito;
	}
	public Double getNrSaldo() {
		return nrSaldo;
	}
	public void setNrSaldo(Double nrSaldo) {
		this.nrSaldo = nrSaldo;
	}
	
	@JsonProperty	
	public String getNrSaldoFormatado() {
		if(nrSaldo != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(nrSaldo);
		}
		return "0,00";

	}
	
	@JsonIgnore
	public void setNrSaldoFormatado(String nrSaldoFormatado) {
	}
	
	
	@JsonProperty	
	public String getNrCreditoFormatado() {
		if(nrCredito != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(nrCredito);
		}
		return "0,00";

	}
	
	@JsonIgnore
	public void setNrCreditoFormatado(String nrCreditoFormatado) {
	}
	
	@JsonProperty	
	public String getNrDebitoFormatado() {
		if(nrDebito != null){
			return Utils.converterDoubelToCurrencyRealWithoutSymbol(nrDebito);
		}
		return "0,00";

	}
	
	@JsonIgnore
	public void setNrDebitoFormatado(String nrDebitoFormatado) {
	}
	
	public String getNmDescricao() {
		return nmDescricao;
	}
	
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	
	public String getFornecedores() {
		return fornecedores;
	}
	
	public void setFornecedores(String fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	public String getDescCaixa() {
		return descCaixa;
	}
	
	public void setDescCaixa(String descCaixa) {
		this.descCaixa = descCaixa;
	}
	
	@JsonProperty
	public String getDtMovimentacaoFormatado() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if ( this.getDtMovimentacao() != null) {
			return sdf.format( this.getDtMovimentacao() );
		}
		return null;
	}
	
	public Boolean getHaveEstorno() {
		return haveEstorno;
	}
	
	public void setHaveEstorno(Boolean haveEstorno) {
		this.haveEstorno = haveEstorno;
	}
	
	public String getInfoEstorno() {
		return infoEstorno;
	}
	
	public void setInfoEstorno(String infoEstorno) {
		this.infoEstorno = infoEstorno;
	}

	public Long getIdCaixaConferido() {
		return idCaixaConferido;
	}

	public void setIdCaixaConferido(Long idCaixaConferido) {
		this.idCaixaConferido = idCaixaConferido;
	}

	public Integer getAcaoMovimentacao() {
		return acaoMovimentacao;
	}

	public void setAcaoMovimentacao(Integer acaoMovimentacao) {
		this.acaoMovimentacao = acaoMovimentacao;
	}
	
}
