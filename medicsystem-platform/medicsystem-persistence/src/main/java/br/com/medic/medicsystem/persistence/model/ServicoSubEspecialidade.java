package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbservicosubespecialidade")
public class ServicoSubEspecialidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICOSUBESPECIALIDADE_ID_SEQ")
	@SequenceGenerator(name = "SERVICOSUBESPECIALIDADE_ID_SEQ", sequenceName = "realvida.servicosubespecialidade_id_seq", allocationSize = 1)
	@Column(name = "idservicosubespecialidade")
	private Long id;

	@Basic
	@Column(name = "boconsulta")
	private Boolean boConsulta;

	@Basic
	@Column(name = "bopadrao")
	private Boolean boPadrao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	// bi-directional many-to-one association to Tbsubespecialidade
	@ManyToOne
	@JoinColumn(name = "idsubespecialidade")
	private SubEspecialidade subEspecialidade;

	public ServicoSubEspecialidade() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoConsulta() {
		return boConsulta;
	}

	public void setBoConsulta(Boolean boConsulta) {
		this.boConsulta = boConsulta;
	}

	public Boolean getBoPadrao() {
		return boPadrao;
	}

	public void setBoPadrao(Boolean boPadrao) {
		this.boPadrao = boPadrao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public SubEspecialidade getSubEspecialidade() {
		return subEspecialidade;
	}

	public void setSubEspecialidade(SubEspecialidade subEspecialidade) {
		this.subEspecialidade = subEspecialidade;
	}
}