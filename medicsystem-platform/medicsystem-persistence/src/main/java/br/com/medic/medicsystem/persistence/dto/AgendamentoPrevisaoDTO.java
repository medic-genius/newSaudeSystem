package br.com.medic.medicsystem.persistence.dto;

public class AgendamentoPrevisaoDTO {
	
	private Long idEspecialidade;
	private Long idUnidade;
	private String nmEspecialidade;
	private Long idFuncionario;
	private String nmFuncionario;
	private Integer dia1;
	private Integer dia2;
	private Integer dia3;
	private Integer dia4;
	private Integer dia5;
	private Integer dia6;
	private Integer dia7;
	private Double notasms;
	private Integer performance;
	
	
	
	public Long getIdEspecialidade() {
		return idEspecialidade;
	}
	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getNmFuncionario() {
		return nmFuncionario;
	}
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}
	public Integer getDia1() {
		return dia1;
	}
	public void setDia1(Integer dia1) {
		this.dia1 = dia1;
	}
	public Integer getDia2() {
		return dia2;
	}
	public void setDia2(Integer dia2) {
		this.dia2 = dia2;
	}
	public Integer getDia3() {
		return dia3;
	}
	public void setDia3(Integer dia3) {
		this.dia3 = dia3;
	}
	public Integer getDia4() {
		return dia4;
	}
	public void setDia4(Integer dia4) {
		this.dia4 = dia4;
	}
	public Integer getDia5() {
		return dia5;
	}
	public void setDia5(Integer dia5) {
		this.dia5 = dia5;
	}
	public Integer getDia6() {
		return dia6;
	}
	public void setDia6(Integer dia6) {
		this.dia6 = dia6;
	}
	public Integer getDia7() {
		return dia7;
	}
	public void setDia7(Integer dia7) {
		this.dia7 = dia7;
	}
	public Double getNotasms() {
		return notasms;
	}
	public void setNotasms(Double notasms) {
		this.notasms = notasms;
	}
	public Integer getPerformance() {
		return performance;
	}
	public void setPerformance(Integer performance) {
		this.performance = performance;
	}
	public Long getIdUnidade() {
		return idUnidade;
	}
	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}
	
	
	

}
