package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbtemplatereceituario")
public class TemplateReceituario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEMPLATERECEITUARIO_ID_SEQ")
	@SequenceGenerator(name = "TEMPLATERECEITUARIO_ID_SEQ", sequenceName = "realvida.idtemplatereceituario_seq" , allocationSize = 1)
	@Column(name = "idtemplatereceituario")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "idespprofissional")
	private EspProfissional espProfissional;
	
	@Basic
	@Column(name = "nmtemplate")
	private String nmTemplate;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Date dtInclusaoLog;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Date dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name = "idtemplatereceituario", insertable=true, updatable=true)
	private List<MedicamentoTemplateReceituario> medicamentosTemplate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EspProfissional getEspProfissional() {
		return espProfissional;
	}

	public void setEspProfissional(EspProfissional espProfissional) {
		this.espProfissional = espProfissional;
	}

	public String getNmTemplate() {
		return nmTemplate;
	}

	public void setNmTemplate(String nmTemplate) {
		this.nmTemplate = nmTemplate;
	}

	public Date getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Date dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Date dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public List<MedicamentoTemplateReceituario> getMedicamentosTemplate() {
		return medicamentosTemplate;
	}

	public void setMedicamentosTemplate(List<MedicamentoTemplateReceituario> medicamentosTemplate) {
		this.medicamentosTemplate = medicamentosTemplate;
	}
}
