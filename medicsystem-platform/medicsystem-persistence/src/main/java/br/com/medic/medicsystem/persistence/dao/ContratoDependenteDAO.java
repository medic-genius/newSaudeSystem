package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;

@Named("contratodependente-dao")
@ApplicationScoped
public class ContratoDependenteDAO extends RelationalDataAccessObject<ContratoDependente> {
	
	public ContratoDependente getContratoDependente(Long idContratoDependente) {
		return searchByKey(ContratoDependente.class, idContratoDependente);
	}
	
	public ContratoDependente getContratoDependenteById (Long idDependente, Long idContrato){
		
		String queryString = "SELECT cd FROM ContratoDependente cd"
				+ " WHERE cd.inSituacao = 0 "
				+ " and cd.dependente.id = " + idDependente
				+ " and cd.contrato.id = " + idContrato ;
				
		try {
			
			TypedQuery<ContratoDependente> query = entityManager.createQuery(queryString, ContratoDependente.class);
			query.setMaxResults(1);
			ContratoDependente result = query.getSingleResult();
			return result;
			
		}catch(Exception e){
			
			return null;
		}
		
		
		
		
	}

}
