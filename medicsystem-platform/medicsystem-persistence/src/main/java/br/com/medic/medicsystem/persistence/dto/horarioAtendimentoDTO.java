package br.com.medic.medicsystem.persistence.dto;

public class horarioAtendimentoDTO {
	private String id;
	
	private String data;
	
	private Boolean boParticular;
	
	private Long idAtendimentoProfissional;
	
	private Boolean boAceitaEncaixe;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Boolean getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(Boolean boParticular) {
		this.boParticular = boParticular;
	}

	public Long getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Long idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public Boolean getBoAceitaEncaixe() {
		return boAceitaEncaixe;
	}

	public void setBoAceitaEncaixe(Boolean boAceitaEncaixe) {
		this.boAceitaEncaixe = boAceitaEncaixe;
	}
}
