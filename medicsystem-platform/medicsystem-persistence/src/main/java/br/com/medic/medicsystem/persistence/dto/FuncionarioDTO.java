package br.com.medic.medicsystem.persistence.dto;

public class FuncionarioDTO {
	private Long idFuncionario;
	private String nmFuncionario;
	private String nrCelular;
	private String imeiCelular;
	
	
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getNmFuncionario() {
		return nmFuncionario;
	}
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}
	
	public String getNrCelular() {
		return nrCelular;
	}
	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}
	public String getImeiCelular() {
		return imeiCelular;
	}
	public void setImeiCelular(String imeiCelular) {
		this.imeiCelular = imeiCelular;
	}
	
	
	
	
	

}
