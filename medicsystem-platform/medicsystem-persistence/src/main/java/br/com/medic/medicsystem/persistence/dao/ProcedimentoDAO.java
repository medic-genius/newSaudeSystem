package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Procedimento;

@Named("procedimento-dao")
@ApplicationScoped
public class ProcedimentoDAO extends RelationalDataAccessObject<Procedimento> {

}
