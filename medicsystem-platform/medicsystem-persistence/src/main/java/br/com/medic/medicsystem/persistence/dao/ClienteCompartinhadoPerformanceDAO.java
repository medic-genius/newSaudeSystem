package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObjectAcademia;
import br.com.medic.medicsystem.persistence.model.Cliente;


@Named("clientecompartinhadoperformance-dao")
@ApplicationScoped
public class ClienteCompartinhadoPerformanceDAO extends RelationalDataAccessObjectAcademia<Cliente>{
	
	public Cliente updateCliente(Cliente cliente){
		return entityManager.merge(cliente);
	}
}
