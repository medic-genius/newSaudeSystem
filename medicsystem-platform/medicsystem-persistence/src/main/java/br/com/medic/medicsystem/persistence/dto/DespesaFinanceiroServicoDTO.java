package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class DespesaFinanceiroServicoDTO implements Serializable{

	private static final long serialVersionUID = 577785809368509831L;

	private String nmObservacao;
	private String nmDescricao;
	private String nmFornecedor;
	private String nmCentroCusto;
	private String nmEmpresaSolicitante;
	private String nmEmpresaPagante;
	private String nmContaBancaria;
	private String nmBanco;
	private String nrDocumento;
	private Double nrValor;
	private Double nrValorPago;
	private Date  dtPagamento;
	private Integer nrQuantidadeParcelas;
	private Integer nrOrdemParcela;
	
	private String nrValorFormatado;
	private String nrValorPagoFormatado;
	private String dtPagamentoFormatado;

	public DespesaFinanceiroServicoDTO() {
	}
	
	public DespesaFinanceiroServicoDTO(String nmObservacao, String nmDescricao ,String nmFornecedor, String nmCentroCusto,
			String nmEmpresaSolicitante, String nmEmpresaPagante, String nmContaBancaria ,String nrDocumento, Double nrValor, 
			Double nrValorPago, String nmBanco, Date dtPagamento, Integer nrQuantidadeParcelas, Integer nrOrdemParcela) {
		this.nmObservacao = nmObservacao;
		this.nmDescricao = nmDescricao;
		this.nmFornecedor = nmFornecedor;
		this.nmCentroCusto = nmCentroCusto;
		this.nmEmpresaSolicitante = nmEmpresaSolicitante;
		this.nmEmpresaPagante = nmEmpresaPagante;
		this.nmContaBancaria = nmContaBancaria;
		this.nmBanco = nmBanco;
		this.nrDocumento = nrDocumento;
		this.nrValor = nrValor;
		this.nrValorPago = nrValorPago;
		this.nrValorFormatado = Utils.converterDoubelToCurrencyReal(nrValor);
		this.nrValorPagoFormatado = Utils.converterDoubelToCurrencyReal(nrValorPago);
		this.dtPagamento = dtPagamento;
		this.nrQuantidadeParcelas = nrQuantidadeParcelas;
		this.nrOrdemParcela = nrOrdemParcela;
	}

	public Integer getNrQuantidadeParcelas() {
		return nrQuantidadeParcelas;
	}

	public void setNrQuantidadeParcelas(Integer nrQuantidadeParcelas) {
		this.nrQuantidadeParcelas = nrQuantidadeParcelas;
	}

	public Integer getNrOrdemParcela() {
		return nrOrdemParcela;
	}

	public void setNrOrdemParcela(Integer nrOrdemParcela) {
		this.nrOrdemParcela = nrOrdemParcela;
	}

	public String getNmObservacao() {
		return nmObservacao;
	}
	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}
	public String getNmFornecedor() {
		return nmFornecedor;
	}
	public void setNmFornecedor(String nmFornecedor) {
		this.nmFornecedor = nmFornecedor;
	}
	public String getNmCentroCusto() {
		return nmCentroCusto;
	}
	public void setNmCentroCusto(String nmCentroCusto) {
		this.nmCentroCusto = nmCentroCusto;
	}
	public String getNmEmpresaSolicitante() {
		return nmEmpresaSolicitante;
	}
	public void setNmEmpresaSolicitante(String nmEmpresaSolicitante) {
		this.nmEmpresaSolicitante = nmEmpresaSolicitante;
	}
	public String getNmEmpresaPagante() {
		return nmEmpresaPagante;
	}
	public void setNmEmpresaPagante(String nmEmpresaPagante) {
		this.nmEmpresaPagante = nmEmpresaPagante;
	}
	public String getNrDocumento() {
		return nrDocumento;
	}
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	public Double getNrValor() {
		return nrValor;
	}
	public void setNrValor(Double nrValor) {
		this.nrValor = nrValor;
	}
	public String getNmDescricao() {
		return nmDescricao;
	}
	public void setNmDescricao(String nmDescricao) {
		this.nmDescricao = nmDescricao;
	}
	public String getNmContaBancaria() {
		return nmContaBancaria;
	}
	public void setNmContaBancaria(String nmContaBancaria) {
		this.nmContaBancaria = nmContaBancaria;
	}
	public Double getNrValorPago() {
		return nrValorPago;
	}
	public void setNrValorPago(Double nrValorPago) {
		this.nrValorPago = nrValorPago;
	}
	public String getNmBanco() {
		return nmBanco;
	}
	public void setNmBanco(String nmBanco) {
		this.nmBanco = nmBanco;
	}
	public String getNrValorFormatado() {
		return nrValorFormatado;
	}
	public void setNrValorFormatado(String nrValorFormatado) {
		this.nrValorFormatado = nrValorFormatado;
	}
	public String getNrValorPagoFormatado() {
		return nrValorPagoFormatado;
	}
	public void setNrValorPagoFormatado(String nrValorPagoFormatado) {
		this.nrValorPagoFormatado = nrValorPagoFormatado;
	}
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public String getDtPagamentoFormatado() {
		if( getDtPagamento() != null ) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format( getDtPagamento() );
		}
		return dtPagamentoFormatado;
	}
	public void setDtPagamentoFormatado(String dtPagamentoFormatado) {
		this.dtPagamentoFormatado = dtPagamentoFormatado;
	}
}
