package br.com.medic.medicsystem.persistence.dto;

import java.sql.Time;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.EnvioSMS;

public class AgendamentoViewDTO {
	
	private Integer inStatus;
	private String nrDespesa;
	private String nrAgendamento;
	private String hrAgendamento;
	private String dtAgendamentoFormatado;
	private String nmPaciente;
	private String nmCliente;
	private String nmEspecialidade;
	private String nmProfissional;
	private String nmServico;
	private String nmConvenio;
	private String nmRetorno;
	private Integer inTipo;
	private String dtInclusaoFormatada;
	private String dtModificacaoFormatada;
	private String operadorCadastro;
	private String operadorAlteracao;
	private Time horaPresenca;
	private Time hrFicha;
	
	private Long idAgendamento;
	private Long idCliente;
	private String nrCelular;
	private String nrTelefone;
	private List<EnvioSMS> envioSms;
		
	private String nmUnidade;
	private String inTipoCliente;
	
	private String dtFaturamentoLaudoFormatado;
	private Boolean servicoLaudavel;
		
//	private Integer contInStatusBloqueado;
//	private Integer contInStatusLiberado;
//	private Integer contInStatusAtendido;
//	private Integer contInStatusFaltoso;
//	private Integer contInStatusPresente;
	
	public Integer getInStatus() {
		return inStatus;
	}
	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}
	public String getNrDespesa() {
		return nrDespesa;
	}
	public void setNrDespesa(String nrDespesa) {
		this.nrDespesa = nrDespesa;
	}
	public String getNrAgendamento() {
		return nrAgendamento;
	}
	public void setNrAgendamento(String nrAgendamento) {
		this.nrAgendamento = nrAgendamento;
	}

	public String getHrAgendamento() {
		return hrAgendamento;
	}
	public void setHrAgendamento(String hrAgendamento) {
		this.hrAgendamento = hrAgendamento;
	}
	public String getDtAgendamentoFormatado() {
		return dtAgendamentoFormatado;
	}
	public void setDtAgendamentoFormatado(String dtAgendamentoFormatado) {
		this.dtAgendamentoFormatado = dtAgendamentoFormatado;
	}
	public String getNmPaciente() {
		return nmPaciente;
	}
	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	public String getNmCliente() {
		return nmCliente;
	}
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	public String getNmProfissional() {
		return nmProfissional;
	}
	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}
	public String getNmServico() {
		return nmServico;
	}
	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}
	public String getNmConvenio() {
		return nmConvenio;
	}
	public void setNmConvenio(String nmConvenio) {
		this.nmConvenio = nmConvenio;
	}
	public String getNmRetorno() {
		return nmRetorno;
	}
	public void setNmRetorno(String nmRetorno) {
		this.nmRetorno = nmRetorno;
	}
	public Integer getInTipo() {
		return inTipo;
	}
	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}
	public String getDtInclusaoFormatada() {
		return dtInclusaoFormatada;
	}
	public void setDtInclusaoFormatada(String dtInclusaoFormatada) {
		this.dtInclusaoFormatada = dtInclusaoFormatada;
	}
	public String getDtModificacaoFormatada() {
		return dtModificacaoFormatada;
	}
	public void setDtModificacaoFormatada(String dtModificacaoFormatada) {
		this.dtModificacaoFormatada = dtModificacaoFormatada;
	}
	public String getOperadorCadastro() {
		return operadorCadastro;
	}
	public void setOperadorCadastro(String operadorCadastro) {
		this.operadorCadastro = operadorCadastro;
	}
	public String getOperadorAlteracao() {
		return operadorAlteracao;
	}
	public void setOperadorAlteracao(String operadorAlteracao) {
		this.operadorAlteracao = operadorAlteracao;
	}
	public Time getHoraPresenca() {
		return horaPresenca;
	}
	public void setHoraPresenca(Time horaPresenca) {
		this.horaPresenca = horaPresenca;
	}
	public Time getHrFicha() {
		return hrFicha;
	}
	public void setHrFicha(Time hrFicha) {
		this.hrFicha = hrFicha;
	}
	
	public Long getIdAgendamento() {
		return idAgendamento;
	}
	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getNrCelular() {
		if(nrCelular != null) {
			nrCelular = nrCelular.replaceAll("\\s", "");
			nrCelular = nrCelular.trim();
			nrCelular = nrCelular.replaceAll("[^0-9]", "");
		}
		return nrCelular;
	}
	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}
	public String getNrTelefone() {
		if(nrTelefone != null) {
			nrTelefone = nrTelefone.replaceAll("\\s", "");
			nrTelefone = nrTelefone.trim();
			nrTelefone = nrTelefone.replaceAll("[^0-9]", "");
		}
		return nrTelefone;
	}
	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}
	public List<EnvioSMS> getEnvioSms() {
		return envioSms;
	}
	public void setEnvioSms(List<EnvioSMS> envioSms) {
		this.envioSms = envioSms;
	}
	
	public String getNmUnidade() {
		return nmUnidade;
	}
	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}
	public String getInTipoCliente() {
		return inTipoCliente;
	}
	public void setInTipoCliente(String inTipoCliente) {
		this.inTipoCliente = inTipoCliente;
	}
	public String getDtFaturamentoLaudoFormatado() {
		return dtFaturamentoLaudoFormatado;
	}
	public void setDtFaturamentoLaudoFormatado(
			String dtFaturamentoLaudoFormatado) {
		this.dtFaturamentoLaudoFormatado = dtFaturamentoLaudoFormatado;
	}
	public Boolean getServicoLaudavel() {
		return servicoLaudavel;
	}
	public void setServicoLaudavel(Boolean servicoLaudavel) {
		this.servicoLaudavel = servicoLaudavel;
	}
	
	
//	public Integer getContInStatusBloqueado() {
//		return contInStatusBloqueado;
//	}
//	public void setContInStatusBloqueado(Integer contInStatusBloqueado) {
//		this.contInStatusBloqueado = contInStatusBloqueado;
//	}
//	public Integer getContInStatusLiberado() {
//		return contInStatusLiberado;
//	}
//	public void setContInStatusLiberado(Integer contInStatusLiberado) {
//		this.contInStatusLiberado = contInStatusLiberado;
//	}
//	public Integer getContInStatusAtendido() {
//		return contInStatusAtendido;
//	}
//	public void setContInStatusAtendido(Integer contInStatusAtendido) {
//		this.contInStatusAtendido = contInStatusAtendido;
//	}
//	public Integer getContInStatusFaltoso() {
//		return contInStatusFaltoso;
//	}
//	public void setContInStatusFaltoso(Integer contInStatusFaltoso) {
//		this.contInStatusFaltoso = contInStatusFaltoso;
//	}
//	public Integer getContInStatusPresente() {
//		return contInStatusPresente;
//	}
//	public void setContInStatusPresente(Integer contInStatusPresente) {
//		this.contInStatusPresente = contInStatusPresente;
//	}
}
