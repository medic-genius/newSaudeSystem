package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(catalog = "financeiro", name = "tbcontacontabil")
public class ContaContabil implements Serializable{
	
	private static final long serialVersionUID = -3756363637911670598L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTA_CONTABIL_ID_SEQ")
	@SequenceGenerator(name = "CONTA_CONTABIL_ID_SEQ", sequenceName = "financeiro.conta_contabil_id_seq", allocationSize = 1)
	@Column(name = "idcontacontabil")
	private Long id;
	
	@Column(name = "nmdescricao")
	private String nmContaContabil;

	@Transient
	private String nrOrdem;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "contacontabil_idcontacontabil")
	private ContaContabil contaContabil;
	
	@OneToMany(mappedBy = "contaContabil", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<ContaContabil> contasContabil;

	@JsonIgnore
	@Column(name = "dtexclusao")
	private Date dtExclusao;
	
	@JsonProperty
	@Transient
	private BigDecimal valorContaContabil;
	
	@JsonProperty
	@Transient
	private String mesAno;
	
	@JsonProperty
	@Transient 
	private Integer mes;
	
	@JsonProperty
	@Transient
	private Integer ano;

	@JsonProperty
	@Transient
	private String anoForm;

	@JsonProperty
	@Transient
	private String anoBase;

	@JsonProperty
	@Transient
	private BigDecimal valorContaContabilBase;

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public ContaContabil() {
		valorContaContabil = new BigDecimal(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmContaContabil() {
		return nmContaContabil;
	}

	public void setNmContaContabil(String nmContaContabil) {
		this.nmContaContabil = nmContaContabil;
	}

	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	public List<ContaContabil> getContasContabil() {
		return contasContabil;
	}

	public void setContasContabil(List<ContaContabil> contasContabil) {
		for (ContaContabil contaContabil : contasContabil) {
			contaContabil.setContaContabil(this);
		}
		this.contasContabil = contasContabil;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getNrOrdem() {
		return nrOrdem;
	}

	public void setNrOrdem(String nrOrdem) {
		this.nrOrdem = nrOrdem;
	}
	
	public BigDecimal getValorContaContabil() {
		return valorContaContabil;
	}

	public void setValorContaContabil(BigDecimal valorContaContabil) {
		this.valorContaContabil = valorContaContabil;
	}

	public void setAnoAH(String anoForm) {
		this.anoForm = anoForm;
		
	}
	
	public String getAnoAH() {
		return anoForm;
	}

	public void setAnoBase(String anoBase) {
		this.anoBase = anoBase;
		
	}
	public String getAnoBase() {
		return anoBase;
	}

	public void setValorContaContabilBase(BigDecimal valorContaContabilBase) {
		this.valorContaContabilBase = valorContaContabilBase;
	}
	
	public BigDecimal getValorContaContabilBase() {
		return valorContaContabilBase;
	}

}
