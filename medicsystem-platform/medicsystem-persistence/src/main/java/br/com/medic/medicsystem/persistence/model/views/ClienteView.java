package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbcliente_view")
public class ClienteView {

	@Id
	@Column(name = "idcliente")
	private Long id;

	@Basic
	@Column(name = "nrcodcliente", nullable = false, unique = true)
	private String nrCodCliente;

	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "nrcpf")
	private String nrCPF;

	@Basic
	@Column(name = "nrrg")
	private String nrRG;

	@Basic
	@Column(name = "insituacaobloqueio")
	private Integer inSituacaoBloqueio;

	@Basic
	@Column(name = "bobloqueado")
	private Boolean boBloqueado;
	
	@Basic
	@Column(name = "nrcelular")
	private String nrCelular;
	
	@Basic
	@Column(name = "dtnascimento")
	private Date dtNascimento;
	
	@Basic
	@Column(name = "nrcep")
	private String nrCep;
	
	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;
	
	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;
	
	
	public ClienteView() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public String getNrRG() {
		return nrRG;
	}

	public void setNrRG(String nrRG) {
		this.nrRG = nrRG;
	}

	public Integer getInSituacaoBloqueio() {
		return inSituacaoBloqueio;
	}

	public void setInSituacaoBloqueio(Integer inSituacaoBloqueio) {
		this.inSituacaoBloqueio = inSituacaoBloqueio;
	}

	public Boolean getBoBloqueado() {
		return boBloqueado;
	}

	public void setBoBloqueado(Boolean boBloqueado) {
		this.boBloqueado = boBloqueado;
	}
	
	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public Integer getStatusGeral() {
		
		if (getNmCliente() == null || getNrRG() == null || getNrCelular() == null || getDtNascimento() == null 
				|| getNrCep() == null || getNmLogradouro() == null || getNrNumero() == null) { // Cadastro Pendente
			return 4;
		}
		
		//if (getInSituacaoBloqueio() == 2 && !getBoBloqueado() && getDtExclusao() == null) { // liberado
		if (getInSituacaoBloqueio() == 2 && getDtExclusao() == null) { // liberado
			return 0;
		}

		//if (getInSituacaoBloqueio() == 0 && !getBoBloqueado() && getDtExclusao() == null) { // semibloqueado
		if (getInSituacaoBloqueio() == 0 && getDtExclusao() == null) { // semibloqueado
			return 1;
		}

		//if (( getInSituacaoBloqueio() == 1 || getBoBloqueado() ) && getDtExclusao() == null) { // bloqueado
		if (( getInSituacaoBloqueio() == 1 ) && getDtExclusao() == null) { // bloqueado
			return 2;
		}

		if (getDtExclusao() != null) { // inadimplente
			return 3;
		}
		
		

		return null;
	}

	
	public String getStatusGeralFormatado() {
		if (getStatusGeral() != null) {
			if (getStatusGeral() == 0) {
				return "LIBERADO";
			}

			if (getStatusGeral() == 1) {
				return "SEMI-BLOQUEADO";
			}

			if (getStatusGeral() == 2) {
				return "BLOQUEADO";
			}

			if (getStatusGeral() == 3) {
				return "INADIMPLENTE";
			}
			
			if (getStatusGeral() == 4) {
				return "CADASTRO PENDENTE";
			}
		}
		return "";
	}

	public String getNrCPFFormatado() {
		if (nrCPF != null && nrCPF.length() == 11) {
			Pattern pattern = Pattern
			        .compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCPF);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3-$4");
		}

		return null;
	}

}
