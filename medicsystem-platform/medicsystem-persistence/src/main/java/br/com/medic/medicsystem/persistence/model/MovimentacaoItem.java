package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbmovimentacaoitem database table.
 * 
 */
@Entity
@Table(catalog = "realvida", name = "tbmovimentacaoitem")
public class MovimentacaoItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOVIMENTACAOITEM_ID_SEQ")
	@SequenceGenerator(name = "MOVIMENTACAOITEM_ID_SEQ", sequenceName = "realvida.movimentacaoitem_id_seq",allocationSize = 1)
	@Column(name = "idmovimentacaoitem")
	private Long idmovimentacaoitem;

	private Boolean boexcluido;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private Long idmensalidade;

	private Long idparcela;

	//bi-directional many-to-one association to Tbmovimentacaocaixa
	@ManyToOne
	@JoinColumn(name="idmovimentacaocaixa")
	private MovimentacaoCaixa tbmovimentacaocaixa;

	public MovimentacaoItem() {
	}

	public Long getIdmovimentacaoitem() {
		return this.idmovimentacaoitem;
	}

	public void setIdmovimentacaoitem(Long idmovimentacaoitem) {
		this.idmovimentacaoitem = idmovimentacaoitem;
	}

	public Boolean getBoexcluido() {
		return this.boexcluido;
	}

	public void setBoexcluido(Boolean boexcluido) {
		this.boexcluido = boexcluido;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Long getIdmensalidade() {
		return this.idmensalidade;
	}

	public void setIdmensalidade(Long idmensalidade) {
		this.idmensalidade = idmensalidade;
	}

	public Long getIdparcela() {
		return this.idparcela;
	}

	public void setIdparcela(Long idparcela) {
		this.idparcela = idparcela;
	}

	public MovimentacaoCaixa getTbmovimentacaocaixa() {
		return this.tbmovimentacaocaixa;
	}

	public void setMovimentacaoCaixa(MovimentacaoCaixa tbmovimentacaocaixa) {
		this.tbmovimentacaocaixa = tbmovimentacaocaixa;
	}

}