package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoContratoEnum;

@Entity
@Table(catalog = "realvida", name = "cobranca_financeiro_view")
public class CobrancaFinanceiroView implements Serializable{

	private static final long serialVersionUID = 8599604984755124354L;

	@Id
	@Column(name = "idcontrato")
	private Long idContrato;
		
	@Column(name = "idcliente")
	private Long idCliente;
	
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Column(name = "informapagamento")
	private Integer inFormaDePagamento;

	@Transient
	private String nmFormaDePagamento;
	
	@Column(name = "nmformapagamentotitulo")
	private String nmFormaPagamentoTitulo;
	
	@Column(name = "intipo")
	private String inTipo;
	
	@Column(name = "nmplano")
	private String nmPlano;

	@Column(name = "dtcontrato")
	private Date dtContrato;
	
	@Column(name = "dtvencimento")
	private Date dataVencimento;

	@Column(name = "bonaofazusoplano")
	private Boolean boNaoFazUsoDoPlano;
	
	@Column(name = "insituacao")
	private Integer inSituacao;
	
	@Column(name = "idtitulo")
	private Long idTitulo;
	
	@Column(name = "vltitulo")
	private Double vlTitulo;
	
	@Column(name = "vljuros")
	private Double valorJuros;
	
	@Column(name = "vlmultaatraso")
	private Double valorMultaAtraso;
	
	@Column(name = "nrtitulo")
	private String nrTitulo;
	
	@Transient
	private Long quantidadeTituloAberto;
	
	@Transient
	private Double totalDevido;
	
	@Transient
	private Double totalDevidoComJuros;
	
	@Transient
	private Integer quantidadeDependentes;

	public CobrancaFinanceiroView() {

	}
	
	public CobrancaFinanceiroView(Long idContrato, Long idCliente, String nrContrato, Integer inFormaDePagamento,
			String nmPlano, Date dtContrato, Boolean boNaoFazUsoDoPlano, Integer inSituacao,
			Long quantidadeTituloAberto, Double totalDevido, Double totalJuros, Double totalMultas) {
		super();
		this.idContrato = idContrato;
		this.idCliente = idCliente;
		this.nrContrato = nrContrato;
		this.inFormaDePagamento = inFormaDePagamento;
		this.nmPlano = nmPlano;
		this.dtContrato = dtContrato;
		this.boNaoFazUsoDoPlano = boNaoFazUsoDoPlano;
		this.inSituacao = inSituacao;
		this.quantidadeTituloAberto = quantidadeTituloAberto;
		this.totalDevido = totalDevido;
		totalJuros = totalJuros == null ? 0 : totalJuros;
		totalMultas = totalMultas == null ? 0 : totalMultas;
		this.totalDevidoComJuros = totalDevido + totalJuros + totalMultas;
	}

	public CobrancaFinanceiroView(String nrContrato, String nmFormaPagamentoTitulo, String inTipo, Date dataVencimento,
			Double vlTitulo, Double valorJuros, Double valorMultaAtraso, String nrTitulo) {
		this.nrContrato = nrContrato;
		this.nmFormaPagamentoTitulo = nmFormaPagamentoTitulo;
		this.inTipo = inTipo;
		this.dataVencimento = dataVencimento;
		this.vlTitulo = vlTitulo;
		this.valorJuros = valorJuros;
		this.valorMultaAtraso = valorMultaAtraso;
		this.nrTitulo = nrTitulo;
	}
	
	public CobrancaFinanceiroView(String nrContrato, String nmFormaPagamentoTitulo, String inTipo, Date dataVencimento,
			Double vlTitulo, Double valorJuros, Double valorMultaAtraso, String nrTitulo, Long idTitulo) {
		this(nrContrato, nmFormaPagamentoTitulo, inTipo, dataVencimento, vlTitulo, valorJuros, valorMultaAtraso, nrTitulo);
		this.idTitulo = idTitulo;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public Integer getInFormaDePagamento() {
		return inFormaDePagamento;
	}

	public void setInFormaDePagamento(Integer inFormaDePagamento) {
		this.inFormaDePagamento = inFormaDePagamento;
	}

	public String getNmFormaPagamentoTitulo() {
		return nmFormaPagamentoTitulo;
	}

	public void setNmFormaPagamentoTitulo(String nmFormaPagamentoTitulo) {
		this.nmFormaPagamentoTitulo = nmFormaPagamentoTitulo;
	}

	public String getInTipo() {
		return inTipo;
	}

	public void setInTipo(String inTipo) {
		this.inTipo = inTipo;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Date getDtContrato() {
		return dtContrato;
	}

	public void setDtContrato(Date dtContrato) {
		this.dtContrato = dtContrato;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Boolean getBoNaoFazUsoDoPlano() {
		return boNaoFazUsoDoPlano;
	}

	public void setBoNaoFazUsoDoPlano(Boolean boNaoFazUsoDoPlano) {
		this.boNaoFazUsoDoPlano = boNaoFazUsoDoPlano;
	}

	public Integer getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Integer inSituacao) {
		this.inSituacao = inSituacao;
	}

	public Long getIdTitulo() {
		return idTitulo;
	}

	public void setIdTitulo(Long idTitulo) {
		this.idTitulo = idTitulo;
	}

	public Double getVlTitulo() {
		return vlTitulo;
	}

	public void setVlTitulo(Double vlTitulo) {
		this.vlTitulo = vlTitulo;
	}

	public Double getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) {
		this.valorJuros = valorJuros;
	}

	public Double getValorMultaAtraso() {
		return valorMultaAtraso;
	}

	public void setValorMultaAtraso(Double valorMultaAtraso) {
		this.valorMultaAtraso = valorMultaAtraso;
	}

	public Long getQuantidadeTituloAberto() {
		return quantidadeTituloAberto;
	}

	public void setQuantidadeTituloAberto(Long quantidadeTituloAberto) {
		this.quantidadeTituloAberto = quantidadeTituloAberto;
	}

	public Double getTotalDevido() {
		return totalDevido;
	}

	public void setTotalDevido(Double totalDevido) {
		this.totalDevido = totalDevido;
	}

	public Integer getQuantidadeDependentes() {
		return quantidadeDependentes;
	}

	public void setQuantidadeDependentes(Integer quantidadeDependentes) {
		this.quantidadeDependentes = quantidadeDependentes;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public String getNmFormaDePagamento() {
		if(inFormaDePagamento != null)
			return FormaPagamentoContratoEnum.getTipoContatoById(inFormaDePagamento).getDescription();
		
		return nmFormaDePagamento;
	}

	public void setNmFormaDePagamento(String nmFormaDePagamento) {
		this.nmFormaDePagamento = nmFormaDePagamento;
	}

	public Double getTotalDevidoComJuros() {
		return totalDevidoComJuros;
	}

	public void setTotalDevidoComJuros(Double totalDevidoComJuros) {
		this.totalDevidoComJuros = totalDevidoComJuros;
	}
	
}
