package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Parcela;

public class DespesaViewDTO {

	private Despesa despesa;

	private List<DespesaServico> despesaServicos;

	private List<Parcela> parcelas;

	public DespesaViewDTO(Despesa despesa,
	        List<DespesaServico> despesaServicos, List<Parcela> parcelas) {
		super();
		this.despesa = despesa;
		this.despesaServicos = despesaServicos;
		this.parcelas = parcelas;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public List<DespesaServico> getDespesaServicos() {
		return despesaServicos;
	}

	public List<Parcela> getParcelas() {
		return parcelas;
	}

}
