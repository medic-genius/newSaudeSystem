package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


@Entity
@Table(catalog = "realvida", name = "tbconferencia")
public class Conferencia implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONFERENCIA_ID_SEQ")
	@SequenceGenerator(name = "CONFERENCIA_ID_SEQ", sequenceName = "realvida.conferencia_id_seq", allocationSize = 1)
	@Column(name = "idconferencia")
	private Long id;
	
	@Basic
	@Column(name = "acrescimo")
	private Float acrescimo;
	
	@Basic
	@Column(name = "desconto")
	private Double desconto;
	
	@Basic
	@Column(name = "dtfechamento")
	private Date dtFechamento;
	
	@Basic
	@Column(name = "mesano")
	private String mesAno;
	
	@Basic
	@Column(name = "obsdadosfinanceiros")
	private String obsDadosFinanceiros;
	
	@Basic
	@Column(name = "observacao")
	private String observacao;
	
	@Basic
	@Column(name = "vltotalcusto")
	private Double vlTotalCusto;
	
	@Basic
	@Column(name = "vltotalvenda")
	private Double vlTotalVenda;

	//bi-directional many-to-one association to Tbcredenciada
	@ManyToOne
	@JoinColumn(name="idcredenciada")
	private Credenciada credenciada;

	//bi-directional many-to-one association to Tbempresacliente
	@ManyToOne
	@JoinColumn(name="idempresacliente")
	private EmpresaCliente empresaCliente;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idusufaturamento")
	private Funcionario funcionario1;

	//bi-directional many-to-one association to Tbfuncionario
	@ManyToOne
	@JoinColumn(name="idusufechamento")
	private Funcionario funcionario2;

	//bi-directional many-to-one association to Tbdespesaservico
	@JsonIgnore
	@OneToMany(mappedBy="conferencia")
	private List<DespesaServico> despesaServicos;

	public Conferencia() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(Float acrescimo) {
		this.acrescimo = acrescimo;
	}

	public Date getDtFechamento() {
		return dtFechamento;
	}

	public void setDtFechamento(Date dtFechamento) {
		this.dtFechamento = dtFechamento;
	}

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public String getObsDadosFinanceiros() {
		return obsDadosFinanceiros;
	}

	public void setObsDadosFinanceiros(String obsDadosFinanceiros) {
		this.obsDadosFinanceiros = obsDadosFinanceiros;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Credenciada getCredenciada() {
		return credenciada;
	}

	public void setCredenciada(Credenciada credenciada) {
		this.credenciada = credenciada;
	}

	public EmpresaCliente getEmpresaCliente() {
		return empresaCliente;
	}

	public void setEmpresaCliente(EmpresaCliente empresaCliente) {
		this.empresaCliente = empresaCliente;
	}

	public Funcionario getFuncionario1() {
		return funcionario1;
	}

	public void setFuncionario1(Funcionario funcionario1) {
		this.funcionario1 = funcionario1;
	}

	public Funcionario getFuncionario2() {
		return funcionario2;
	}

	public void setFuncionario2(Funcionario funcionario2) {
		this.funcionario2 = funcionario2;
	}

	public List<DespesaServico> getDespesaServicos() {
		return despesaServicos;
	}

	public void setDespesaServicos(List<DespesaServico> fespesaServicos) {
		this.despesaServicos = fespesaServicos;
	}

	public DespesaServico addDespesaServico(DespesaServico despesaServico) {
		getDespesaServicos().add(despesaServico);
		despesaServico.setConferencia(this);

		return despesaServico;
	}

	public DespesaServico removeDespesaServico(DespesaServico despesaServico) {
		getDespesaServicos().remove(despesaServico);
		despesaServico.setConferencia(null);

		return despesaServico;
	}

	public Double getVlTotalCusto() {
		return vlTotalCusto;
	}

	public void setVlTotalCusto(Double vlTotalCusto) {
		this.vlTotalCusto = vlTotalCusto;
	}

	public Double getVlTotalVenda() {
		return vlTotalVenda;
	}

	public void setVlTotalVenda(Double vlTotalVenda) {
		this.vlTotalVenda = vlTotalVenda;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
	

}