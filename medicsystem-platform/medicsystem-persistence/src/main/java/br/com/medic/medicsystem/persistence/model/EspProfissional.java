package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbespprofissional")
public class EspProfissional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESPPROFISSIONAL_ID_SEQ")
	@SequenceGenerator(name = "ESPPROFISSIONAL_ID_SEQ", sequenceName = "realvida.espprofissional_id_seq", allocationSize = 1)
	@Column(name = "idespprofissional")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@ManyToOne
	@JoinColumn(name = "idespecialidade")
	private Especialidade especialidade;

	@ManyToOne
	@JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
	private Funcionario funcionario;

//	@ManyToOne
//	@JoinColumn(name = "idprofissional", referencedColumnName = "idfuncionario")
//	private Profissional profissional;

	public EspProfissional() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

//	public Profissional getProfissional() {
//		return profissional;
//	}
//
//	public void setProfissional(Profissional profissional) {
//		this.profissional = profissional;
//	}

}