package br.com.medic.medicsystem.persistence.model.enums;

public enum TipoMovimentacao {
	ENTRADA, SAIDA
}
