package br.com.medic.medicsystem.persistence.dto;

public class EspecialidadeDTO {
	
	private Long idEspecialidade;
	private String nmEspecialidade;
	
	
	
	
	
	public Long getIdEspecialidade() {
		return idEspecialidade;
	}
	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}
	public String getNmEspecialidade() {
		return nmEspecialidade;
	}
	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}
	
	
	
	

}
