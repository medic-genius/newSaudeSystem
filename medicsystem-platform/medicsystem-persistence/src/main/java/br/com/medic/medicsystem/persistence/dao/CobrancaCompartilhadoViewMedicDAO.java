package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.views.CobrancaCompartilhadoView;

@ApplicationScoped
@Named("cobrancacompartilhadoviewmedic-dao")
public class CobrancaCompartilhadoViewMedicDAO extends RelationalDataAccessObject<CobrancaCompartilhadoView> {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Long getQuantidadeClientesInadimplentes(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Boolean boAdimplente){	

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = cb.createQuery();
		
		Root<CobrancaCompartilhadoView> cobrancaRoot = criteriaQuery.from(CobrancaCompartilhadoView.class);
		criteriaQuery.select(cobrancaRoot.get("id"));
		criteriaQuery.distinct(true);

		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteriaQuery.where(predicate);
		
		CriteriaQuery<Long> criteriaQueryCount = criteriaQuery.select(cb.countDistinct(cobrancaRoot));
		TypedQuery<Long> typedQueryCount = entityManager.createQuery(criteriaQueryCount);
		Long totalLinhas = typedQueryCount.getSingleResult();
		
		return totalLinhas;
	}
	
	public List<CobrancaCompartilhadoView> getClientesInadimplentesFromCobrancaView(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, Date dataInicio, Date dataFim, Integer startPosition, Integer maxResults, Boolean boAdimplente){	
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CobrancaCompartilhadoView> criteria = cb.createQuery(CobrancaCompartilhadoView.class);
		
		Root<CobrancaCompartilhadoView> cobrancaRoot = criteria.from(CobrancaCompartilhadoView.class);
		criteria.multiselect(cobrancaRoot.get("id"), 
							cobrancaRoot.get("nmCliente"),
							cobrancaRoot.get("nrCpf"),
							cobrancaRoot.get("nrTelefone"),
							cobrancaRoot.get("nrCelular"),
							cobrancaRoot.get("nmUF"),
							cobrancaRoot.get("nmCidade"),
							cobrancaRoot.get("nmBairro"),
							cobrancaRoot.get("nmLogradouro"),
							cobrancaRoot.get("nrNumero"),							
							cobrancaRoot.get("nrCEP"),							
							cb.sum(cobrancaRoot.get("inInadimplente")),
							cb.sum(cobrancaRoot.get("valorDivida")),
							cb.sum(cobrancaRoot.get("valorJuros")),
							cb.sum(cobrancaRoot.get("valorMultaAtraso")),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "M"), 1).otherwise(0)),
							cb.sum(cb.<Integer>selectCase().when(cb.equal(cobrancaRoot.get("inTipo"), "D"), 1).otherwise(0))
							
				);
		
		Predicate predicate = getWhereClause(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, cb, cobrancaRoot, boAdimplente);
		
		criteria.where(predicate);
		criteria.groupBy(cobrancaRoot.get("id"), cobrancaRoot.get("nmCliente"), cobrancaRoot.get("nrCpf"), 
				cobrancaRoot.get("nrTelefone"), cobrancaRoot.get("nrCelular"), cobrancaRoot.get("nmUF"),
				cobrancaRoot.get("nmCidade"), cobrancaRoot.get("nmBairro"), cobrancaRoot.get("nmLogradouro"), 
				cobrancaRoot.get("nrNumero"), cobrancaRoot.get("nrCEP") );

		criteria.orderBy(cb.asc(cobrancaRoot.get("nmCliente")));
		
		TypedQuery<CobrancaCompartilhadoView> q = entityManager.createQuery(criteria);
		startPosition = startPosition == null ? 0 : startPosition;
		maxResults = maxResults == null ? 10 : maxResults;
		q.setFirstResult(startPosition);
		q.setMaxResults(maxResults);
		List<CobrancaCompartilhadoView> result = q.getResultList();
		
		return result;
	}
	
	private Predicate getWhereClause(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, 
			Date dataInicio, Date dataFim, CriteriaBuilder cb, Root<CobrancaCompartilhadoView> cobrancaRoot, Boolean boAdimplente){
		Predicate predicate = cb.conjunction();
		
		if(idEmpresa != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("idEmpresaGrupo"), idEmpresa));
		}
		
		if(idFormaPagamento != null){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inFormaDePagamento"), idFormaPagamento));
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			predicate = cb.and(predicate, cb.between(cobrancaRoot.get("dataVencimento"), dataInicio, dataFim));
		}
		
		if (nomeCliente != null) {
			String where_cliente;

			if(nomeCliente.startsWith("%") || nomeCliente.endsWith("%")) {
				where_cliente = nomeCliente.toUpperCase();

			} else {
				where_cliente = nomeCliente.toUpperCase() + "%";
			}

			predicate = cb.and(predicate, cb.like(cobrancaRoot.get("nmCliente"), where_cliente));
		}
				
		if(boAdimplente == null || !boAdimplente){
			predicate = cb.and(predicate, cb.equal(cobrancaRoot.get("inInadimplente"), 1));
		}
		
		return predicate;
	}

	public Long getQuantidadeClientesInadimplentesByInadimplenciaRecente(Long idEmpresa, Long idFormaPagamento, Integer dias){
		//A abordagem de dividir a quantidade de dias por 28 e multiplicar seu piso por 30 é utilizada para evitar
		//o problema que acontece quando as datas possuem meses com 28, 29, 31 dias (não múltiplos de 30), pois a query
		//foi preparar para receber apenas os múltiplos de 30 (30 dias - 1 mês, 60 dias - meses....)
		Integer qtdeCobrancasEmAberto = (int) Math.floor(dias/28);
		Integer diasTotal = qtdeCobrancasEmAberto * 30;
		String query = "";
		
		if( idFormaPagamento != null && (idFormaPagamento.equals(1L) || idFormaPagamento.equals(2L)) )
			qtdeCobrancasEmAberto = qtdeCobrancasEmAberto - 1;

		query =	"select * from compartilhado.cobraca_totalinadimplentes_recente ( "+ diasTotal +", "+qtdeCobrancasEmAberto+", "+idEmpresa+","+idFormaPagamento+")";
				
		Query q = findByNativeQuery(query);
		
		Integer r = (Integer) q.getSingleResult();

		return r.longValue();
	}

	@SuppressWarnings("unchecked")
	public List <CobrancaCompartilhadoView> getClientesInadimplentesByInadimplenciaRecente(Long idEmpresa, Long idFormaPagamento, Integer startPosition, Integer maxResults, Integer dias){
		Integer qtdeCobrancasEmAberto = (int) Math.floor(dias/28);
		Integer diasTotal = qtdeCobrancasEmAberto * 30;		
		String query = "";
		
		if( idFormaPagamento != null && (idFormaPagamento.equals(1L) || idFormaPagamento.equals(2L)) )	
			qtdeCobrancasEmAberto = qtdeCobrancasEmAberto - 1;	
		
	
		if(idEmpresa != null && idFormaPagamento != null) {
			
			query = "select id,nmCliente, nrCpf,nrTelefone, nrCelular, nrRg, nrCodCliente, nmEmail, nmLogradouro, nrCEP, nrNumero, idClienteBoleto_sl, nmBairro, nmCidade, nmUF, linkCobrancaBoleto_sl, qdteDevido, somaTotalDividas,somaTotalJuros,somaTotalMultas,qtdeMensalidade,qtdeDespesa   "
					+ " from compartilhado.cobraca_clientesinadimplentes_recente ( " + diasTotal + ", "+  qtdeCobrancasEmAberto  + "," + idEmpresa + "," + idFormaPagamento + "," + maxResults + ", " + startPosition + " ) " 
					+ " as cir( id bigint, nmCliente varchar, nrCpf varchar, nrTelefone varchar,nrCelular varchar, nrRg varchar, nrCodCliente varchar, nmEmail varchar, nmLogradouro varchar, nrCEP varchar, nrNumero varchar, idClienteBoleto_sl bigint , nmBairro varchar, nmCidade varchar, nmUF varchar, linkCobrancaBoleto_sl varchar, qdteDevido bigint, somaTotalDividas real, somaTotalJuros numeric,somaTotalMultas real, qtdeMensalidade bigint, qtdeDespesa bigint )";
			


			List<CobrancaCompartilhadoView> result = findByNativeQuery(query)
			        .unwrap(SQLQuery.class)
			        .addScalar("id", LongType.INSTANCE)
			        .addScalar("nmCliente", StringType.INSTANCE)
			        .addScalar("nrCpf", StringType.INSTANCE)
			        .addScalar("nrTelefone", StringType.INSTANCE)
			        .addScalar("nrCelular", StringType.INSTANCE)
			        .addScalar("nrRg", StringType.INSTANCE)
			        .addScalar("nrCodCliente", StringType.INSTANCE)
			        .addScalar("nmEmail", StringType.INSTANCE)
			        .addScalar("nmLogradouro", StringType.INSTANCE)
			        .addScalar("nrCEP", StringType.INSTANCE)
			        .addScalar("nrNumero", StringType.INSTANCE)
			        .addScalar("idClienteBoleto_sl", LongType.INSTANCE)
			        .addScalar("nmBairro", StringType.INSTANCE)
			        .addScalar("nmCidade", StringType.INSTANCE)
			        .addScalar("nmUF", StringType.INSTANCE)
			        .addScalar("linkCobrancaBoleto_sl", StringType.INSTANCE)
			        .addScalar("qdteDevido", LongType.INSTANCE)
			        .addScalar("somaTotalDividas", DoubleType.INSTANCE)
			        .addScalar("somaTotalJuros", DoubleType.INSTANCE)
			        .addScalar("somaTotalMultas", DoubleType.INSTANCE)
			        .addScalar("qtdeMensalidade", LongType.INSTANCE)
			        .addScalar("qtdeDespesa", LongType.INSTANCE)
			        .setResultTransformer(
			                Transformers.aliasToBean(CobrancaCompartilhadoView.class)).list();
			
			return result;
		
		} else
			return null;
	}
}
