package br.com.medic.dashboard.persistence.model;

import java.text.NumberFormat;
import java.util.Locale;

public class FaturamentoPlano implements GraphWrapper {

	private Long idplano;
	private String nomeplano;
	private Double valorplano;
	private Integer contratos;
	private Double totalreal;
	private Integer vidas;

	private String valorplanoFormatado;
	private String totalrealFormatado;
	private String custoVidaFormatado;

	public FaturamentoPlano() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getIdplano() {
	    return idplano;
    }
	
	public void setIdplano(Long idplano) {
	    this.idplano = idplano;
    }

	public String getNomeplano() {
		return nomeplano;
	}

	public void setNomeplano(String nomeplano) {
		this.nomeplano = nomeplano;
	}

	public Double getValorplano() {
		return valorplano;
	}

	public void setValorplano(Double valorplano) {
		this.valorplano = valorplano;
	}

	public Integer getContratos() {
		return contratos;
	}

	public void setContratos(Integer contratos) {
		this.contratos = contratos;
	}

	public Double getTotalreal() {
		return totalreal;
	}

	public void setTotalreal(Double totalreal) {
		this.totalreal = totalreal;
	}
	
	public String getValorplanoFormatado() {

		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getValorplano() != null)
			valorplanoFormatado = currencyFormatter.format(getValorplano());
		return valorplanoFormatado;

	}

	public String getTotalrealFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getTotalreal() != null)
			totalrealFormatado = currencyFormatter.format(getTotalreal());
		return totalrealFormatado;
	}
	
	public String getCustoVidaFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		Double custovidacalc = getTotalreal() / vidas;
		if (custovidacalc  != null)
			custoVidaFormatado = currencyFormatter.format(custovidacalc);
		return custoVidaFormatado;
    }

	public Integer getVidas() {
		return vidas;
	}

	public void setVidas(Integer vidas) {
		this.vidas = vidas;
	}

	@Override
    public String getGraphLabel() {
	    return null;
    }

	@Override
    public Double getGraphValue() {
	    // TODO Auto-generated method stub
	    return null;
    }

}
