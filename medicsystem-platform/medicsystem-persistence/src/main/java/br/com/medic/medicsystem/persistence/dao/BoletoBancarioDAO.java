package br.com.medic.medicsystem.persistence.dao;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.views.MensalidadeBoletoImprimirView;

@Named("boletobancario-dao")
@ApplicationScoped
public class BoletoBancarioDAO extends RelationalDataAccessObject<BoletoBancario>{
	
	public List<MensalidadeBoletoImprimirView> getMensalidadeBoletoImprimir(Long idContrato) {
		
		String queryStr = "SELECT mb FROM MensalidadeBoletoImprimirView mb where mb.idContrato = " + idContrato;
		TypedQuery<MensalidadeBoletoImprimirView> query = entityManager.createQuery(queryStr,
				MensalidadeBoletoImprimirView.class);
		List<MensalidadeBoletoImprimirView> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Collection<Contrato> getContratos(Long idEmpresaGrupo, String dtInicio, String dtFinal) {
				
		String whereData = null;
		
		if(dtInicio != null 
				&& dtFinal != null){
			
			whereData = " AND MEN.DTVENCIMENTO BETWEEN '"
					+ dtInicio
					+ "' AND '" + dtFinal + "'";
		} else {
			
			whereData = " AND MEN.DTVENCIMENTO >= CURRENT_DATE "; 
		}
			
		String queryStr = "SELECT CON.* FROM REALVIDA.TBMENSALIDADE MEN"
				+ " INNER JOIN REALVIDA.TBCONTRATO CON ON CON.IDCONTRATO = MEN.IDCONTRATO"
				+ " INNER JOIN REALVIDA.TBPLANO PLA ON PLA.IDPLANO = CON.IDPLANO"
				+ " INNER JOIN REALVIDA.TBCONTRATOCLIENTE CONT ON CONT.IDCONTRATO =  CON.IDCONTRATO"
				+ " INNER JOIN REALVIDA.TBCLIENTE CLI ON CLI.IDCLIENTE = CONT.IDCLIENTE"
				+ " LEFT JOIN REALVIDA.TBBOLETOBANCARIO BOL ON BOL.IDCHAVEBOLETO = MEN.IDMENSALIDADE"
				+ " WHERE CON.IDEMPRESAGRUPO = " + idEmpresaGrupo
				+ " AND CON.INFORMAPAGAMENTO = 0"
				+ " AND CLI.DTEXCLUSAO IS NULL" + " AND CON.INSITUACAO = 0"
				+ " AND CON.INRENOVADO = 1"
				+ whereData
				+ " AND MEN.DTEXCLUSAO IS NULL"
				+ " AND BOL.IDBOLETOBANCARIO IS NULL"
				+ " GROUP BY CON.IDCONTRATO" + " ORDER BY CON.IDCONTRATO";

		Collection<Contrato> result = findByNativeQuery(queryStr,
		        Contrato.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	
	public List<BoletoBancario> getBoletoBancario( Mensalidade mensalidade ) {
		
		try {
			
			TypedQuery<BoletoBancario> query = entityManager.createQuery(
					"FROM BoletoBancario boleto"			
					+ " WHERE boleto.nrBarra <> 'NULL'"
					+ " AND boleto.idMensalidade = " + mensalidade.getId() 
					+ " AND (boleto.inExcluido is null OR boleto.inExcluido = false)" 
					+ " ORDER BY boleto.id", BoletoBancario.class);
			
			List<BoletoBancario> result = query.getResultList();	
			
			if (result.isEmpty()) {
				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			}
	
			return result;		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			return null;
		}
	}
	
	
	public List<BoletoBancario> getBoletoBancario( Parcela parcela ) {
		
		try {
			
			TypedQuery<BoletoBancario> query = entityManager.createQuery(
					"FROM BoletoBancario boleto"			
					+ " WHERE boleto.nrBarra <> 'NULL'"
					+ " AND boleto.idParcela = " + parcela.getId() 
					+ " AND (boleto.inExcluido is null OR boleto.inExcluido = false)" 
					+ " ORDER BY boleto.id", BoletoBancario.class);
			
			List<BoletoBancario> result = query.getResultList();
			
			return result;	
			
		} catch (Exception e) {
			// TODO: handle exception,
			e.getMessage();
			return null;
		}
		
	}
	
	public BoletoBancario getBoletoByNossoNumero( String nossoNumero ) {
		
		try {
			
			TypedQuery<BoletoBancario> query = entityManager.createQuery(
					"FROM BoletoBancario boleto"			
					+ " WHERE boleto.nrBarra <> 'NULL'"
					+ " AND boleto.nrNossoNumero = '" + nossoNumero +"'"
					+ " AND (boleto.inExcluido is null OR boleto.inExcluido = false)" 
					+ " ORDER BY boleto.id", BoletoBancario.class);
			
			BoletoBancario result = query.getSingleResult();
			
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
			
	}
	
	public Mensalidade mensalidadesCliente(String nmCliente, String dataVencimento){		
		
		String query ="select m.* from realvida.tbmensalidade m"
						+ " inner join realvida.tbcontrato c on c.idcontrato = m.idcontrato"
						+ " inner join realvida.tbcontratocliente cc on cc.idcontrato = c.idcontrato"
						+ " inner join realvida.tbcliente cli on cli.idcliente = cc.idcliente"
						+ " where lower(trim(cli.nmcliente)) = lower(trim('"+nmCliente+"'))"
						+ " and m.instatus = 0"
						+ " and m.dtvencimento = '"+dataVencimento+"'"
						+ " and m.informapagamento = 5"
						+ " and m.dtexclusao is null"
						+ " and m.dtpagamento is null";
		try {
		List<Mensalidade> mensalidades =(List<Mensalidade>) findByNativeQuery(query,Mensalidade.class);
		
		
			if(mensalidades != null && mensalidades.size() > 0)
				return mensalidades.get(0);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}

}
