package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.MedicamentoReceituario;

@Named("receituariomedicamento-dao")
@ApplicationScoped
public class ReceituarioMedicamentoDAO extends RelationalDataAccessObject<MedicamentoReceituario> {
	
	public MedicamentoReceituario merge(MedicamentoReceituario recMedic) {
		return this.entityManager.merge(recMedic);
	}
}