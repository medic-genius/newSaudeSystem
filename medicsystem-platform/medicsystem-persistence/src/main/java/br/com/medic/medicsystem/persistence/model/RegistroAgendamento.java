package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(catalog = "estatistica", name = "tbregistroagendamento")
public class RegistroAgendamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGISTROAGENDAMENTO_ID_SEQ")
	@SequenceGenerator(name = "REGISTROAGENDAMENTO_ID_SEQ", catalog = "estatistica", sequenceName = "registroagendamento_id_seq", allocationSize = 1)
	@Column(name = "idregistroagendamento")
	private Long id;
	
	@Basic
	@Column(name="idagendamento")
	private Long idAgendamento;
	
	@Basic
	@Column(name="hratendimentoinicio")
	private Time hrAtendimentoInicio;
	
	@Basic
	@Column(name="hratendimentofim")
	private Time hrAtendimentoFim;
	
	@Basic
	@Column(name="tipoAtendimento")
	private Integer tipoAtendimento;
		
	@Basic
	@Column(name="vlhora")
	private Double vlHora;
	
	@Basic
	@Column(name="vlcomissao")
	private Double vlComissao;
	
	@Basic
	@Column(name="inturno")
	private Integer inTurno;
	
	@Basic
	@Column(name="qtdturno")
	private Integer qtdTurno;
	
	@Basic
	@Column(name="dtfaturamentolaudo")
	private Date dtFaturamentoLaudo;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	
	public Integer getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(Integer tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}

	public Time getHrAtendimentoInicio() {
		return hrAtendimentoInicio;
	}

	public void setHrAtendimentoInicio(Time hrAtendimentoInicio) {
		this.hrAtendimentoInicio = hrAtendimentoInicio;
	}

	public Time getHrAtendimentoFim() {
		return hrAtendimentoFim;
	}

	public void setHrAtendimentoFim(Time hrAtendimentoFim) {
		this.hrAtendimentoFim = hrAtendimentoFim;
	}
	
	public Double getVlHora() {
		return vlHora;
	}

	public void setVlHora(Double vlHora) {
		this.vlHora = vlHora;
	}	
	
	public Double getVlComissao() {
		return vlComissao;
	}

	public void setVlComissao(Double vlComissao) {
		this.vlComissao = vlComissao;
	}
	
	public Integer getInTurno() {
		return inTurno;
	}

	public void setInTurno(Integer inTurno) {
		this.inTurno = inTurno;
	}

	public Integer getQtdTurno() {
		return qtdTurno;
	}

	public void setQtdTurno(Integer qtdTurno) {
		this.qtdTurno = qtdTurno;
	}
	
	public Date getDtFaturamentoLaudo() {
		return dtFaturamentoLaudo;
	}

	public void setDtFaturamentoLaudo(Date dtFaturamentoLaudo) {
		this.dtFaturamentoLaudo = dtFaturamentoLaudo;
	}

}
