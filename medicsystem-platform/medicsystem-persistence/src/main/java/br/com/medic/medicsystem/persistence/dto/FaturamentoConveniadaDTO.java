package br.com.medic.medicsystem.persistence.dto;

import java.util.List;

import br.com.medic.medicsystem.persistence.model.views.ClienteDependenteConvenioView;
import br.com.medic.medicsystem.persistence.model.views.DespesaConvenioView;

public class FaturamentoConveniadaDTO {
	
	private Long idEmpresaCliente;
	
	private String nmEmpresaCliente;
	
	private Integer totalServico;
	
	private Integer totalVidas;
	
	private Double vlTotalServico;
	
	private Double vlTotal;
	
	private Boolean boBloqueado;
	
	/**
	 *  1 - Despesa, 2 - Mensalidade
	 */
	private Integer inTipo;
	
	private List<DespesaConvenioView> listDespesaServico;
	
	private List<ClienteDependenteConvenioView> listClienteDependente;
	

	public Integer getTotalServico() {
		return totalServico;
	}

	public void setTotalServico(Integer totalServico) {
		this.totalServico = totalServico;
	}
	
	public List<DespesaConvenioView> getListDespesaServico() {
		return listDespesaServico;
	}

	public void setListDespesaServico(List<DespesaConvenioView> listDespesaServico) {
		this.listDespesaServico = listDespesaServico;
	}

	public Long getIdEmpresaCliente() {
		return idEmpresaCliente;
	}

	public void setIdEmpresaCliente(Long idEmpresaCliente) {
		this.idEmpresaCliente = idEmpresaCliente;
	}

	public String getNmEmpresaCliente() {
		return nmEmpresaCliente;
	}

	public void setNmEmpresaCliente(String nmEmpresaCliente) {
		this.nmEmpresaCliente = nmEmpresaCliente;
	}

	public Double getVlTotalServico() {
		return vlTotalServico;
	}

	public void setVlTotalServico(Double vlTotalServico) {
		this.vlTotalServico = vlTotalServico;
	}

	public Double getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Double vlTotal) {
		this.vlTotal = vlTotal;
	}

	public List<ClienteDependenteConvenioView> getListClienteDependente() {
		return listClienteDependente;
	}

	public void setListClienteDependente(List<ClienteDependenteConvenioView> listClienteDependente) {
		this.listClienteDependente = listClienteDependente;
	}

	public Integer getTotalVidas() {
		return totalVidas;
	}

	public void setTotalVidas(Integer totalVidas) {
		this.totalVidas = totalVidas;
	}

	public Boolean getBoBloqueado() {
		return boBloqueado;
	}

	public void setBoBloqueado(Boolean boBloqueado) {
		this.boBloqueado = boBloqueado;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}


}
