package br.com.medic.medicsystem.persistence.appmobile.dto;

import java.io.Serializable;
import java.util.Date;

public class AgendamentoLembreteDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idAgendamento;
	
	private Date dtAgendamento;
	
	private String nmFuncionario;
	
	private String nmEspecialidade;
	
	private String nmUnidade;
	
	private Long idPaciente;
	
	private Integer tipoPaciente;
	
	private String nmPaciente;
	
	private String emailPaciente;
	
	private String celularPaciente;
	
	private String notificationRegId;

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}

	public Date getDtAgendamento() {
		return dtAgendamento;
	}

	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Integer getTipoPaciente() {
		return tipoPaciente;
	}

	public void setTipoPaciente(Integer tipoPaciente) {
		this.tipoPaciente = tipoPaciente;
	}
	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getEmailPaciente() {
		return emailPaciente;
	}

	public void setEmailPaciente(String emailPaciente) {
		this.emailPaciente = emailPaciente;
	}

	public String getCelularPaciente() {
		return celularPaciente;
	}

	public void setCelularPaciente(String celularPaciente) {
		this.celularPaciente = celularPaciente;
	}

	public String getNotificationRegId() {
		return notificationRegId;
	}

	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
}
