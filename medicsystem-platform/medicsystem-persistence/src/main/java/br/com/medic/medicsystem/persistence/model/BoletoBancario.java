package br.com.medic.medicsystem.persistence.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(catalog = "realvida", name = "tbboletobancario")
public class BoletoBancario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BOLETOBANCARIO_ID_SEQ")
	@SequenceGenerator(name = "BOLETOBANCARIO_ID_SEQ", sequenceName = "realvida.boletobancario_id_seq", allocationSize = 1)
	@Column(name = "idboletobancario")
	private Long id;

	@Basic
	@Column(name = "nrboleto")
	private Long nrBoleto;

	@Basic
	@Column(name = "intipoboleto")
	private Integer inTipoBoleto;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Basic
	@Column(name = "dtinclusao")
	private Date dtInclusao;

	@Basic
	@Column(name = "idchaveboleto")
	private Long idChaveBoleto;

	@Basic
	@Column(name = "nrcedente")
	private String nrCedente;
	
	@Basic
	@Column(name = "nrcarteira")
	private String nrCarteira;

	@Basic
	@Column(name = "cdbarra")
	private String cdBarra;

	@Basic
	@Column(name = "nrbarra")
	private String nrBarra;
	
	@Basic
	@Column(name = "nrnossonumero")
	private String nrNossoNumero;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "inexcluido")
	private Boolean inExcluido;
	
	//-- Informacoes boleto pjbank
	@Basic
	@Column(name = "idmensalidade")
	private Long idMensalidade;
		
	@Basic
	@Column(name = "idparcela")
	private Long idParcela;
	
	@Basic
	@Column(name = "linkboleto")
	private String linkBoleto;
	
	@Transient
	private Mensalidade mensalidade;
	
	@Transient
	private Cliente cliente;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNrBoleto() {
		return nrBoleto;
	}

	public void setNrBoleto(Long nrBoleto) {
		this.nrBoleto = nrBoleto;
	}

	public Integer getInTipoBoleto() {
		return inTipoBoleto;
	}

	public void setInTipoBoleto(Integer inTipoBoleto) {
		this.inTipoBoleto = inTipoBoleto;
	}

	public Date getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(Date dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public Long getIdChaveBoleto() {
		return idChaveBoleto;
	}

	public void setIdChaveBoleto(Long idChaveBoleto) {
		this.idChaveBoleto = idChaveBoleto;
	}

	public String getNrCedente() {
		return nrCedente;
	}

	public void setNrCedente(String nrCedente) {
		this.nrCedente = nrCedente;
	}

	public String getNrCarteira() {
		return nrCarteira;
	}

	public void setNrCarteira(String nrCarteira) {
		this.nrCarteira = nrCarteira;
	}

	public String getCdBarra() {
		return cdBarra;
	}

	public void setCdBarra(String cdBarra) {
		this.cdBarra = cdBarra;
	}

	public String getNrBarra() {
		return nrBarra;
	}

	public void setNrBarra(String nrBarra) {
		this.nrBarra = nrBarra;
	}

	public String getNrNossoNumero() {
		return nrNossoNumero;
	}

	public void setNrNossoNumero(String nrNossoNumero) {
		this.nrNossoNumero = nrNossoNumero;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Boolean getInExcluido() {
		return inExcluido;
	}

	public void setInExcluido(Boolean inExcluido) {
		this.inExcluido = inExcluido;
	}

	public Mensalidade getMensalidade() {
		return mensalidade;
	}

	public void setMensalidade(Mensalidade mensalidade) {
		this.mensalidade = mensalidade;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Long getIdMensalidade() {
		return idMensalidade;
	}

	public void setIdMensalidade(Long idMensalidade) {
		this.idMensalidade = idMensalidade;
	}

	public Long getIdParcela() {
		return idParcela;
	}

	public void setIdParcela(Long idParcela) {
		this.idParcela = idParcela;
	}

	public String getLinkBoleto() {
		return linkBoleto;
	}

	public void setLinkBoleto(String linkBoleto) {
		this.linkBoleto = linkBoleto;
	}

}
