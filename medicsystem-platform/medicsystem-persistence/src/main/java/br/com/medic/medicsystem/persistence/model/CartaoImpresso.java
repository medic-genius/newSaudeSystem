package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(catalog = "realvida", name = "tbcartaoimpresso")
public class CartaoImpresso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARTAOIMPRESSO_ID_SEQ")
	@SequenceGenerator(name = "CARTAOIMPRESSO_ID_SEQ", sequenceName = "realvida.cartaoimpresso_id_seq")
	@Column(name = "idcartaoimpresso")
	private Long id;
	
	@Basic
	@Column(name = "boimpresso")
	private Boolean boImpresso;

	@Basic
	@Column(name = "boliberadoimpressao")
	private Boolean boLiberadoImpressao;

	@Basic
	@Column(name = "dtadesao")
	private Date dtAdesao;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "dtimpressao")
	private Date dtImpressao;

	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "nmgrupo")
	private String nmGrupo;

	@Basic
	@Column(name = "nrmatricula")
	private String nrMatricula;

	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name="idcontratocliente")
	private ContratoCliente contratoCliente;

	@ManyToOne
	@JoinColumn(name="iddependente")
	private Dependente dependente;

	@ManyToOne
	@JoinColumn(name="idempresacliente")
	private EmpresaCliente empresaCliente;

	@ManyToOne
	@JoinColumn(name="idfuncionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="idplano")
	private Plano plano;

	public CartaoImpresso() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBoImpresso() {
		return boImpresso;
	}

	public void setBoImpresso(Boolean boImpresso) {
		this.boImpresso = boImpresso;
	}

	public Boolean getBoLiberadoImpressao() {
		return boLiberadoImpressao;
	}

	public void setBoLiberadoImpressao(Boolean boLiberadoImpressao) {
		this.boLiberadoImpressao = boLiberadoImpressao;
	}

	public Date getDtAdesao() {
		return dtAdesao;
	}

	public void setDtAdesao(Date dtAdesao) {
		this.dtAdesao = dtAdesao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtImpressao() {
		return dtImpressao;
	}

	public void setDtImpressao(Date dtImpressao) {
		this.dtImpressao = dtImpressao;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public String getNmGrupo() {
		return nmGrupo;
	}

	public void setNmGrupo(String nmGrupo) {
		this.nmGrupo = nmGrupo;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ContratoCliente getContratoCliente() {
		return contratoCliente;
	}

	public void setContratoCliente(ContratoCliente contratoCliente) {
		this.contratoCliente = contratoCliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public EmpresaCliente getEmpresaCliente() {
		return empresaCliente;
	}

	public void setEmpresaCliente(EmpresaCliente empresaCliente) {
		this.empresaCliente = empresaCliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

}