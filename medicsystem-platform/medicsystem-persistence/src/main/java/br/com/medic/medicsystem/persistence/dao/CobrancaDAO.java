package br.com.medic.medicsystem.persistence.dao;

import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Cobranca;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.enums.SituacaoSacCobranca;

@ApplicationScoped
@Named("cobranca-dao")
public class CobrancaDAO extends RelationalDataAccessObject<Cobranca>{

	public Cobranca getCobrancaStatusAbertoByCliente(Long idCliente){
		String querySql = "SELECT c FROM Sac s, Cobranca c where c.sac.id = s.id and s.cliente.id = " + idCliente + " and c.inStatus = " + SituacaoSacCobranca.ABERTO.getId();
		Cobranca result = null;
		
		TypedQuery<Cobranca> query = entityManager.createQuery(querySql, Cobranca.class);
		try{
			result = query.getSingleResult();
		}catch(NoResultException ex){
//			ex.printStackTrace();
		}
		return result;
	}
	
	public Long getQuantidadeCobrancaByFuncionario(Long idFuncionario, Date dtInicio, Date dtFim){
		TypedQuery<Long> query;
		Long result = null;
		
		String querySql = "SELECT count(c) FROM Sac s, Cobranca c where c.sac.id = s.id and s.funcionario1.id = " + idFuncionario;
	
		if(dtInicio == null || dtFim == null){
			query = entityManager.createQuery(querySql, Long.class);
		} else{
			querySql += " AND s.data BETWEEN :startDate AND :endDate";
			query = entityManager.createQuery(querySql, Long.class);
			query.setParameter("startDate", dtInicio);
			query.setParameter("endDate", dtFim);
		}
		
		try{
			result = query.getSingleResult();
		}catch(NoResultException ex){
			result = 0L;
		}
		return result;
	}
	
	public EmpresaGrupoCompartilhado getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(Long idEmpresaFinanceiro){
		String queryStr = "SELECT e FROM EmpresaGrupoCompartilhado e WHERE e.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro;
		TypedQuery<EmpresaGrupoCompartilhado> query = entityManager.createQuery(queryStr, EmpresaGrupoCompartilhado.class);
		EmpresaGrupoCompartilhado result = query.getSingleResult();
		return result;
	}
	
}
