package br.com.medic.medicsystem.persistence.model.dental;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
//
//@NamedQuery(name = "mqMensalidadeAVencer", query = "SELECT m, cli "
//        + "FROM Mensalidade m, ContratoCliente concli LEFT JOIN concli.contrato as c LEFT JOIN concli.cliente as cli "
//        + "WHERE m.contrato = c " + "AND m.informaPagamento = 5 "
//        + "AND m.dataExclusao is null "
//        + "AND c.empresaGrupo.id = 9 "
//        + "AND (m.boletoNegociada is null or m.boletoNegociada = false) "
//        + "AND m.dataVencimento = :dtvenc " + "AND m.dataPagamento is null "
//        + "AND c.situacao = 0 "
//        + "order by cli")

//@NamedQuery(name = "mqMensalidadeAVencer", query = "SELECT DISTINCT ON (cli.nmcliente,cli.nrcelular)  "  
//					+"m,cli "
//					+"FROM realvida.tbmensalidade m "
//					+"LEFT JOIN realvida.tbcontrato c on m.idcontrato = c.idcontrato " 
//					+"LEFT JOIN realvida.tbcontratocliente concli on c.idcontrato = concli.idcontrato " 
//					+"LEFT JOIN realvida.tbcliente cli on cli.idcliente = concli.idcliente  "
//					+"WHERE m.informapagamento in(1,5,30,4,9) "
//					+"AND m.dtexclusao is null "
//					+"AND c.idempresagrupo = 9 "
//					+"AND (m.bonegociada is null or m.bonegociada = false) "
//					+"AND m.dtvencimento = :dtvenc  "
//					+"AND m.dtpagamento is null "
//					+"AND c.insituacao = 0 "
//					+"AND (cli.nrtelefone is  not null or cli.nrcelular is not null) "
//					+"AND m.instatus = 0 "
//					+"ORDER BY cli.nmcliente") 
//

@Entity
@Table(catalog = "realvida", name = "tbmensalidade")
public class MensalidadeDental {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENSALIDADE_ID_SEQ_DENTAL")
	@SequenceGenerator(name = "MENSALIDADE_ID_SEQ_DENTAL", sequenceName = "realvida.mensalidade_id_seq")
	@Column(name = "idmensalidade")
	private Long id;

	@Basic
	@Column(name = "vlmensalidade")
	private Double valorMensalidade;

	@Basic
	@Column(name = "dtvencimento")
	private Date dataVencimento;

	@Basic
	@Column(name = "dtpagamento")
	private Date dataPagamento;

	@Basic
	@Column(name = "informapagamento")
	private Integer informaPagamento;

	@Basic
	@Column(name = "dtexclusao")
	private Date dataExclusao;

	@Basic
	@Column(name = "bonegociada")
	private Boolean boletoNegociada;
	
	@Basic
	@Column(name = "dtatualizacao")
	private Date dtAtualizacao;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtcarta1")
	private Date dtCarta1;
	
	@Basic
	@Column(name = "dtcarta2")
	private Date dtCarta2;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtnegociacao")
	private Date dtNegociacao;
	
	@Basic
	@Column(name = "dtprevisaopagto")
	private Date dtPrevisaoPagto;
	
	@Basic
	@Column(name = "formapagamentoefetuada")
	private String formaPagamentoEfetuada;
	
	@Basic
	@Column(name = "iddespesapai")
	private Integer idDespesaPai;	
	
	@Basic
	@Column(name = "idmensalidadeaux")
	private Long idMensalidadeAux;
	
	@Basic
	@Column(name = "idmensalidadeodonto")
	private Long idMensalidadeoOdonto;
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;
	
	@Basic
	@Column(name = "nrmensalidade")
	private String nrMensalidade;
	
	@Basic
	@Column(name = "observacao")
	private String observacao;
	
	@Basic
	@Column(name = "vljuros")
	private Double vlJuros;
	
	@Basic
	@Column(name = "vlmulta")
	private Double vlMulta;
	
	@Basic
	@Column(name = "vlpago")
	private Double vlPago;

	@Basic
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name = "linkcobrancaboleto_sl")
	private String linkCobrancaBoleto_sl;
	
	@Basic
	@Column(name = "idcobrancaboleto_sl")
	private Long idCobrancaBoleto_sl;

	 public MensalidadeDental() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValorMensalidade() {
		return valorMensalidade;
	}

	public void setValorMensalidade(Double valorMensalidade) {
		this.valorMensalidade = valorMensalidade;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Integer getInformaPagamento() {
		return informaPagamento;
	}

	public void setInformaPagamento(Integer informaPagamento) {
		this.informaPagamento = informaPagamento;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Boolean getBoletoNegociada() {
		return boletoNegociada;
	}

	public void setBoletoNegociada(Boolean boletoNegociada) {
		this.boletoNegociada = boletoNegociada;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtCarta1() {
		return dtCarta1;
	}

	public void setDtCarta1(Date dtCarta1) {
		this.dtCarta1 = dtCarta1;
	}

	public Date getDtCarta2() {
		return dtCarta2;
	}

	public void setDtCarta2(Date dtCarta2) {
		this.dtCarta2 = dtCarta2;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Date getDtNegociacao() {
		return dtNegociacao;
	}

	public void setDtNegociacao(Date dtNegociacao) {
		this.dtNegociacao = dtNegociacao;
	}

	public Date getDtPrevisaoPagto() {
		return dtPrevisaoPagto;
	}

	public void setDtPrevisaoPagto(Date dtPrevisaoPagto) {
		this.dtPrevisaoPagto = dtPrevisaoPagto;
	}

	public String getFormaPagamentoEfetuada() {
		return formaPagamentoEfetuada;
	}

	public void setFormaPagamentoEfetuada(String formaPagamentoEfetuada) {
		this.formaPagamentoEfetuada = formaPagamentoEfetuada;
	}

	public Integer getIdDespesaPai() {
		return idDespesaPai;
	}

	public void setIdDespesaPai(Integer idDespesaPai) {
		this.idDespesaPai = idDespesaPai;
	}

	public Long getIdMensalidadeAux() {
		return idMensalidadeAux;
	}

	public void setIdMensalidadeAux(Long idMensalidadeAux) {
		this.idMensalidadeAux = idMensalidadeAux;
	}

	public Long getIdMensalidadeoOdonto() {
		return idMensalidadeoOdonto;
	}

	public void setIdMensalidadeoOdonto(Long idMensalidadeoOdonto) {
		this.idMensalidadeoOdonto = idMensalidadeoOdonto;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public String getNrMensalidade() {
		return nrMensalidade;
	}

	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Double getVlJuros() {
		return vlJuros;
	}

	public void setVlJuros(Double vlJuros) {
		this.vlJuros = vlJuros;
	}

	public Double getVlMulta() {
		return vlMulta;
	}

	public void setVlMulta(Double vlMulta) {
		this.vlMulta = vlMulta;
	}

	public Double getVlPago() {
		return vlPago;
	}

	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getValorMensalidadeFormatado() {

		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		return currencyFormatter.format(getValorMensalidade());

	}
	
	public String getDataVencimentoFormatado() {

		Locale locale = new Locale("pt", "BR");
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);

		return dateFormatter.format(getDataVencimento());

	}

	public String getLinkCobrancaBoleto_sl() {
		return linkCobrancaBoleto_sl;
	}

	public void setLinkCobrancaBoleto_sl(String linkCobrancaBoleto_sl) {
		this.linkCobrancaBoleto_sl = linkCobrancaBoleto_sl;
	}

	public Long getIdCobrancaBoleto_sl() {
		return idCobrancaBoleto_sl;
	}

	public void setIdCobrancaBoleto_sl(Long idCobrancaBoleto_sl) {
		this.idCobrancaBoleto_sl = idCobrancaBoleto_sl;
	}
	
	

}
