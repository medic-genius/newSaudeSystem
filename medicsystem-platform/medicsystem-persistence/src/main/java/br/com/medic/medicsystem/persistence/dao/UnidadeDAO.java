package br.com.medic.medicsystem.persistence.dao;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Departamento;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Unidade;

@Named("unidade-dao")
@ApplicationScoped
public class UnidadeDAO extends RelationalDataAccessObject<Unidade> {

	public List<Unidade> getUnidades() {
		TypedQuery<Unidade> query = entityManager.createQuery(
		        "SELECT u FROM Unidade u where u.dtInativacao is null ORDER BY u.id DESC", Unidade.class);

		List<Unidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Collection<Especialidade> getEspecialidades(Long idUnidade) {

		String queryStr = "SELECT DISTINCT esp.*"
		        + " FROM realvida.tbAtendimentoProfissional_View ap"
		        + " INNER JOIN realvida.tbespecialidade esp ON esp.idespecialidade = ap.idespprofissional"
		        + " WHERE ap.idunidade = " + idUnidade
		        + " ORDER BY esp.nmespecialidade";

		Collection<Especialidade> result = findByNativeQuery(queryStr,
		        Especialidade.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public List<Unidade> getUnidadesbyEmpresaGrupo( Long IdEmpresaGrupo) {
		TypedQuery<Unidade> query = entityManager.createQuery(
		        "SELECT u FROM Unidade u where u.dtInativacao is null "
		        + " and u.empresaGrupo.id = " + IdEmpresaGrupo
		        + " ORDER BY u.id DESC", Unidade.class);

		List<Unidade> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Unidade getUnidadeById(Long idUnidade) {
		try {
			return entityManager.find(Unidade.class, idUnidade);
		} catch(Exception e) {
			return null;
		}
	}
	
	public Departamento getDepartamentoByUnidade(Long idUnidade) {
		TypedQuery<Departamento> query = entityManager.createQuery(
		        "SELECT d FROM Departamento d "
		        + " where d.unidade.id = " + idUnidade		        
		        + " ORDER BY d.id DESC", Departamento.class);
		
		query.setMaxResults(1);
		Departamento result = query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
}
