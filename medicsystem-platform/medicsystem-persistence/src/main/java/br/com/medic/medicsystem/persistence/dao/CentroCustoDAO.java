package br.com.medic.medicsystem.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.CentroCustoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.CentroCusto;


@Named("centrocusto-dao")
@ApplicationScoped
public class CentroCustoDAO extends RelationalDataAccessObject<CentroCusto>{

	public List<CentroCusto> getAllCentrosCusto(Long idCentroCusto){
		String queryStr = "SELECT c FROM CentroCusto c ";
		if(idCentroCusto != null && idCentroCusto != 0) {
			queryStr += " WHERE c.dtExclusao is null ";
		} else {
			queryStr += " WHERE c.centroCusto is null "
					+ " AND c.dtExclusao is null ";
		}
		
		if(idCentroCusto != null && idCentroCusto !=0) {
			queryStr += " AND c.id = :idCentroCusto";
		}
		queryStr += " ORDER BY c.nmCentroCusto ASC";
		
		if(idCentroCusto != null && idCentroCusto !=0 ) {
			TypedQuery<CentroCusto> query = entityManager.createQuery(queryStr, CentroCusto.class)
					.setParameter("idCentroCusto", idCentroCusto);
			ArrayList<CentroCusto> list = (ArrayList<CentroCusto>) query.getResultList();
			return list;
		} else {
			TypedQuery<CentroCusto> query = entityManager.createQuery(queryStr, CentroCusto.class);
			ArrayList<CentroCusto> list = (ArrayList<CentroCusto>) query.getResultList();
			return list;
		}

	}

	@SuppressWarnings("unchecked") //tipoStatus  0-ABERTO 1-PAGO 3- NAO PAGO
	public List<CentroCustoDTO> getRelatorioTodosCentroCusto(int mesSplit, int anoSplit, Long idCentroCusto, String idEmpresa, Integer tipoStatus) {
		String queryStr = "";
		if(idEmpresa != null && !idEmpresa.equals("0") && tipoStatus == 1) {
			queryStr += "SELECT d.idEmpresaFinanceiroStart, cc.nmCentroCusto , cc.idCentroCusto, coalesce(SUM(d.vlpago),0) as totalDespesa ";
		} else if(idEmpresa != null && !idEmpresa.equals("0") && (tipoStatus == 0 || tipoStatus == 2 )){
			queryStr += "SELECT d.idEmpresaFinanceiroStart, cc.nmCentroCusto, cc.idCentroCusto, coalesce(SUM(d.vldespesa), 0) as totaldespesa";
		} else if( tipoStatus == 1) {
			queryStr += "SELECT cc.nmCentroCusto , cc.idCentroCusto, coalesce(SUM(d.vlpago),0) as totalDespesa ";
		}else if(tipoStatus == 0  ||tipoStatus == 2) {
			queryStr += "SELECT cc.nmCentroCusto, cc.idCentroCusto, coalesce(SUM(d.vldespesa), 0) as totalDespesa";
		}
		
		queryStr +=" from financeiro.tbcentrocusto cc"
				+ " left join financeiro.tbdespesa d on d.idcentrocusto = cc.idcentrocusto"
				+ " and d.dtexclusao is null";
		if(tipoStatus == 0) {
			queryStr += " and d.instatus in (0,2)";
		}else if(tipoStatus == 1) {
			queryStr += " and d.instatus = 1";
		}else if( tipoStatus == 2) {
			queryStr += " and d.instatus in (0,1,2)";
		}
				
		if(idEmpresa != null && !idEmpresa.equals("0")) {
			queryStr += " and d.idempresafinanceirostart in (" + idEmpresa +")"; 
		}
		
		if(tipoStatus == 1) {
			 queryStr += " and extract('Month' from d.dtpagamento) = " + mesSplit
						+ " and extract('Year' from d.dtpagamento) = "+ anoSplit
						+ " where cc.dtexclusao is null";
		} else {
			queryStr += " and extract('Month' from d.dtvencimento) = " + mesSplit
					+ " and extract('Year' from d.dtvencimento) = " + anoSplit
					+ " where cc.dtexclusao is null";
		}
				
			if(idCentroCusto != null && idCentroCusto != 0) {
				queryStr += " and cc.idcentrocusto = " + idCentroCusto; 
			}
			if(idEmpresa != null && !idEmpresa.equals("0")) {
				queryStr += "  group by d.idempresafinanceirostart, cc.idcentrocusto";
			}else {
				queryStr += " group by cc.idcentrocusto";	
			}
			
				
			if(idEmpresa != null && !idEmpresa.equals("0")) {
				List<CentroCustoDTO> result = findByNativeQuery(queryStr)
						.unwrap(SQLQuery.class)
						.addScalar("idEmpresaFinanceiroStart", LongType.INSTANCE)
						.addScalar("nmCentroCusto", StringType.INSTANCE)
						.addScalar("idCentroCusto", LongType.INSTANCE)
						.addScalar("totalDespesa", DoubleType.INSTANCE)
						.setResultTransformer(Transformers.aliasToBean(CentroCustoDTO.class)).list();
				
				return result;
			} else {
				List<CentroCustoDTO> result = findByNativeQuery(queryStr)
						.unwrap(SQLQuery.class)
						.addScalar("nmCentroCusto", StringType.INSTANCE)
						.addScalar("idCentroCusto", LongType.INSTANCE)
						.addScalar("totalDespesa", DoubleType.INSTANCE)
						.setResultTransformer(Transformers.aliasToBean(CentroCustoDTO.class)).list();
				
				return result;
			}
			
	} 
	public List<CentroCusto> getAllCentroCusto() {
		String queryStr = "SELECT c FROM CentroCusto c WHERE c.centroCusto is null AND c.dtExclusao is null ORDER BY c.nmCentroCusto ASC";
		TypedQuery<CentroCusto> query = entityManager.createQuery(queryStr, CentroCusto.class);
		List<CentroCusto> lista = query.getResultList();
		return lista;
	}

	public List<CentroCusto> getBuscaCentroCusto(String  nmCentroCusto, Integer startPosition, Integer maxResults) {
				CriteriaBuilder cb = entityManager.getCriteriaBuilder();
				CriteriaQuery<CentroCusto> criteria = cb.createQuery(CentroCusto.class);

				Root<CentroCusto> centroCustoRoot = criteria.from(CentroCusto.class);
				criteria.select(centroCustoRoot);

				Predicate p = cb.conjunction();
				
				if (nmCentroCusto != null) {
					String where_cc ;

					if(nmCentroCusto.startsWith("%") || nmCentroCusto.endsWith("%")) {
						where_cc = nmCentroCusto.toUpperCase();

					} else {
						where_cc = "%" + nmCentroCusto.toLowerCase() + "%";
						
					}

					p = cb.and(p, cb.like(centroCustoRoot.get("nmCentroCusto"), where_cc));
					
				}

				p = cb.and(p, cb.isNull(centroCustoRoot.get("dtExclusao")));
				criteria.where(p);
				criteria.orderBy(cb.asc(centroCustoRoot.get("nmCentroCusto")));

				TypedQuery<CentroCusto> q = entityManager.createQuery(criteria);
				startPosition = startPosition == null ? 0 : startPosition;
				if(maxResults != null) {
					q.setMaxResults(maxResults);
				}
				q.setFirstResult(startPosition);
				
				List<CentroCusto> result = q.getResultList();

				if (result.isEmpty()) {
					throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
				}

				return result;
	}

	
}
