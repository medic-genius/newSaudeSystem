package br.com.medic.medicsystem.persistence.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;

public class PacienteDTO<T> {
	
	private Cliente cliente;
	
	private Dependente dependente;
	
	private ContratoCliente contratoCliente;
	
	private ContratoDependente contratoDependente;
	
	private String cdPaciente;

	private String nmPaciente;
	
	private String nmTitular;
	
	private String nmTipo;
	
	private String inStatusContrato;
	
	private Boolean bloqueado;
	
	private String cpf;
	
	private String urlFotoPaciente;
	
	private String nrContrato;
	
	private String nmPlano;
	
	private String obsPlano;
	
	private String boNaoFazUsoPlano;
	
//	private Boolean boFazUsoPlano;
	
	public PacienteDTO() {
		
	}

	public PacienteDTO(T parent) {

		if (parent instanceof Dependente) {
			nmPaciente = ((Dependente) parent).getNmDependente();
		} else if (parent instanceof Cliente) {
			nmPaciente = ((Cliente) parent).getNmCliente();
		}
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Dependente getDependente() {
		return dependente;
	}

	public void setDependente(Dependente dependente) {
		this.dependente = dependente;
	}

	public ContratoCliente getContratoCliente() {
		return contratoCliente;
	}

	public void setContratoCliente(ContratoCliente contratoCliente) {
		this.contratoCliente = contratoCliente;
	}

	public ContratoDependente getContratoDependente() {
		return contratoDependente;
	}

	public void setContratoDependente(ContratoDependente contratoDependente) {
		this.contratoDependente = contratoDependente;
	}

	public String getCdPaciente() {
		return cdPaciente;
	}

	public void setCdPaciente(String cdPaciente) {
		this.cdPaciente = cdPaciente;
	}

	public String getNmTitular() {
		return nmTitular;
	}

	public void setNmTitular(String nmTitular) {
		this.nmTitular = nmTitular;
	}

	public String getNmTipo() {
		return nmTipo;
	}

	public void setNmTipo(String nmTipo) {
		this.nmTipo = nmTipo;
	}

	public String getInStatusContrato() {
		return inStatusContrato;
	}

	public void setInStatusContrato(String inStatusContrato) {
		this.inStatusContrato = inStatusContrato;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getUrlFotoPaciente() {
		return urlFotoPaciente;
	}

	public void setUrlFotoPaciente(String urlFotoPaciente) {
		this.urlFotoPaciente = urlFotoPaciente;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public String getObsPlano() {
		return obsPlano;
	}

	public void setObsPlano(String obsPlano) {
		this.obsPlano = obsPlano;
	}

	public String getBoNaoFazUsoPlano() {
		return boNaoFazUsoPlano;
	}

	public void setBoNaoFazUsoPlano(String boNaoFazUsoPlano) {
		this.boNaoFazUsoPlano = boNaoFazUsoPlano;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

//	public Boolean getBoFazUsoPlano() {
//		return boFazUsoPlano;
//	}
//
//	public void setBoFazUsoPlano(Boolean boFazUsoPlano) {
//		this.boFazUsoPlano = boFazUsoPlano;
//	}

	
	
}
