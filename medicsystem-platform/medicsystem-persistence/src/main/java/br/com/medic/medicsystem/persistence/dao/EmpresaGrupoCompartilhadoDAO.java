package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;

@ApplicationScoped
@Named("empresagrupocompartilhado-dao")
public class EmpresaGrupoCompartilhadoDAO extends RelationalDataAccessObject<EmpresaGrupoCompartilhado>{

	public EmpresaGrupoCompartilhado getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(Long idEmpresaFinanceiro){
		String queryStr = "SELECT e FROM EmpresaGrupoCompartilhado e WHERE e.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro;
		TypedQuery<EmpresaGrupoCompartilhado> query = entityManager.createQuery(queryStr, EmpresaGrupoCompartilhado.class);
		EmpresaGrupoCompartilhado result = query.getSingleResult();
		return result;
	}
	
	public List<EmpresaGrupoCompartilhado> getAllEmpresasGrupoCompartilhado(){
		String queryStr = "SELECT e FROM EmpresaGrupoCompartilhado e";
		TypedQuery<EmpresaGrupoCompartilhado> query = entityManager.createQuery(queryStr, EmpresaGrupoCompartilhado.class);
		List<EmpresaGrupoCompartilhado> result = query.getResultList();
		return result;
	}
	
}
