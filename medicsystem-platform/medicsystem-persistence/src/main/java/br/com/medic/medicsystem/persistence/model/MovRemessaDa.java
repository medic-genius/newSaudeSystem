package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbmovremessada")
public class MovRemessaDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOVREMESSADA_ID_SEQ")
	@SequenceGenerator(name = "MOVREMESSADA_ID_SEQ", sequenceName = "realvida.movremessada_id_seq")
	@Column(name = "idmovremessada")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;
	
	@Basic
	@Column(name = "intipo")
	private Integer inTipo;
	
	@Basic
	@Column(name = "nrdvnossonumero")
	private String nrDvNossoNumero;
	
	@Basic
	@Column(name = "nrnossonumero")
	private String nrNossoNumero;
	
	@Basic
	@Column(name = "nrnossonumerocompleto")
	private String nrNossoNumeroCompleto;
	
	@Basic
	@Column(name = "nrtitulo")
	private String nrTitulo;
	
	@Basic
	@Column(name = "vltitulo")
	private float vlTitulo;

	//bi-directional many-to-one association to Tbremessada
	@ManyToOne
	@JoinColumn(name="idremessada")
	private RemessaDa remessaDa;

	//bi-directional many-to-one association to Tbtituloda
	@ManyToOne
	@JoinColumn(name="idtituloda")
	private TituloDa tituloDa;

	public MovRemessaDa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Integer getInTipo() {
		return inTipo;
	}

	public void setInTipo(Integer inTipo) {
		this.inTipo = inTipo;
	}

	public String getNrDvNossoNumero() {
		return nrDvNossoNumero;
	}

	public void setNrDvNossoNumero(String nrDvNossoNumero) {
		this.nrDvNossoNumero = nrDvNossoNumero;
	}

	public String getNrNossoNumero() {
		return nrNossoNumero;
	}

	public void setNrNossoNumero(String nrNossoNumero) {
		this.nrNossoNumero = nrNossoNumero;
	}

	public String getNrNossoNumeroCompleto() {
		return nrNossoNumeroCompleto;
	}

	public void setNrNossoNumeroCompleto(String nrNossoNumeroCompleto) {
		this.nrNossoNumeroCompleto = nrNossoNumeroCompleto;
	}

	public String getNrTitulo() {
		return nrTitulo;
	}

	public void setNrTitulo(String nrTitulo) {
		this.nrTitulo = nrTitulo;
	}

	public float getVlTitulo() {
		return vlTitulo;
	}

	public void setVlTitulo(float vlTitulo) {
		this.vlTitulo = vlTitulo;
	}

	public RemessaDa getRemessaDa() {
		return remessaDa;
	}

	public void setRemessaDa(RemessaDa remessaDa) {
		this.remessaDa = remessaDa;
	}

	public TituloDa getTituloDa() {
		return tituloDa;
	}

	public void setTituloDa(TituloDa tituloDa) {
		this.tituloDa = tituloDa;
	}

}