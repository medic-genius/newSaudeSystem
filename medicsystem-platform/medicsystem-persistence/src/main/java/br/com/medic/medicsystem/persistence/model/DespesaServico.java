package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.medic.medicsystem.persistence.dto.ServicoCredenciadaDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoProfissionalDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(catalog = "realvida", name = "tbdespesaservico")
public class DespesaServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESPESASERVICO_ID_SEQ")
	@SequenceGenerator(name = "DESPESASERVICO_ID_SEQ", sequenceName = "realvida.despesaservico_id_seq", allocationSize = 1)
	@Column(name = "iddespesaservico")
	private Long id;

	@Basic
	@Column(name = "acrescimo")
	private Double acrescimo;

	@Basic
	@Column(name = "borealizado")
	private Boolean boRealizado;

	@Basic
	@Column(name = "desconto")
	private Double desconto;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "iddespesaservicoaux")
	private Long idDespesaServicoAux;

	@Basic
	@Column(name = "iddespesaservicoodonto")
	private Long idDespesaServicoOdonto;

	@ManyToOne
	@JoinColumn(name = "idencaminhamento")
	private Encaminhamento encaminhamento;

	@Basic
	@Column(name = "incoberto")
	private Boolean inCoberto;

	@Basic
	@Column(name = "instatus")
	private Integer inStatus;

	@Basic
	@Column(name = "obsdescontoacrescimo")
	private String obsDescontoAcrescimo;

	@Basic
	@Column(name = "qtservico")
	private Integer qtServico;

	@Basic
	@Column(name = "vlcusto")
	private Double vlCusto;

	@Basic
	@Column(name = "vlservico")
	private Double vlServico;

	@ManyToOne
	@JoinColumn(name = "idconferencia")
	private Conferencia conferencia;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "iddespesa")
	private Despesa despesa;

	@ManyToOne
	@JoinColumn(name = "idprocedimento")
	private Procedimento procedimento;

	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	@Transient
	private Boolean boEncaminhado;

	@Transient
	@JsonProperty
	private Integer rdTipoEncaminhamento;

	@Transient
	@JsonProperty
	private Integer rdTipoEncaminhamentoInterno;

	@Transient
	@JsonProperty
	private ServicoProfissionalDTO profissionalDTO;

	@Transient
	@JsonProperty
	private ServicoCredenciadaDTO credenciadaInternaDTO;

	@Transient
	@JsonProperty
	private ServicoCredenciadaDTO credenciadaExternaDTO;

	@Transient
	@JsonProperty
	private Double vlCoberto;

	@Transient
	@JsonProperty
	private Double vlAberto;

	@Transient
	@JsonProperty
	private String vlServicoFormatado;
	
	@Transient
	@JsonProperty
	private String inCobertoFormatado;

	public DespesaServico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAcrescimo() {
		return acrescimo;
	}

	public void setAcrescimo(Double acrescimo) {
		this.acrescimo = acrescimo;
	}

	public Boolean getBoRealizado() {
		return boRealizado;
	}

	public void setBoRealizado(Boolean boRealizado) {
		this.boRealizado = boRealizado;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdDespesaServicoAux() {
		return idDespesaServicoAux;
	}

	public void setIdDespesaServicoAux(Long idDespesaServicoAux) {
		this.idDespesaServicoAux = idDespesaServicoAux;
	}

	public Long getIdDespesaServicoOdonto() {
		return idDespesaServicoOdonto;
	}

	public void setIdDespesaServicoOdonto(Long idDespesaServicoOdonto) {
		this.idDespesaServicoOdonto = idDespesaServicoOdonto;
	}

	public Encaminhamento getEncaminhamento() {
		return encaminhamento;
	}

	public void setEncaminhamento(Encaminhamento encaminhamento) {
		this.encaminhamento = encaminhamento;
	}

	public Boolean getInCoberto() {
		return inCoberto;
	}

	public void setInCoberto(Boolean inCoberto) {
		this.inCoberto = inCoberto;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public String getObsDescontoAcrescimo() {
		return obsDescontoAcrescimo;
	}

	public void setObsDescontoAcrescimo(String obsDescontoAcrescimo) {
		this.obsDescontoAcrescimo = obsDescontoAcrescimo;
	}

	public Integer getQtServico() {
		return qtServico;
	}

	public void setQtServico(Integer qtServico) {
		this.qtServico = qtServico;
	}

	public Double getVlCusto() {
		return vlCusto;
	}

	public void setVlCusto(Double vlCusto) {
		this.vlCusto = vlCusto;
	}

	public Double getVlServico() {
		return vlServico;
	}

	public void setVlServico(Double vlServico) {
		this.vlServico = vlServico;
	}

	public Conferencia getConferencia() {
		return conferencia;
	}

	public void setConferencia(Conferencia conferencia) {
		this.conferencia = conferencia;
	}

	public Despesa getDespesa() {
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Procedimento getProcedimento() {
		return procedimento;
	}

	public void setProcedimento(Procedimento procedimento) {
		this.procedimento = procedimento;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	@JsonProperty
	public String getVlServicoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
		        .getCurrencyInstance(locale);

		if (getVlServico() != null)
			return currencyFormatter.format(getVlServico());
		return null;
	}

	public Boolean getBoEncaminhado() {
		return boEncaminhado;
	}

	public void setBoEncaminhado(Boolean boEncaminhado) {
		this.boEncaminhado = boEncaminhado;
	}

	public void setRdTipoEncaminhamento(Integer rdTipoEncaminhamento) {
		this.rdTipoEncaminhamento = rdTipoEncaminhamento;
	}

	public void setRdTipoEncaminhamentoInterno(
	        Integer rdTipoEncaminhamentoInterno) {
		this.rdTipoEncaminhamentoInterno = rdTipoEncaminhamentoInterno;
	}

	public void setCredenciadaExternaDTO(
	        ServicoCredenciadaDTO credenciadaExternaDTO) {
		this.credenciadaExternaDTO = credenciadaExternaDTO;
	}

	public void setCredenciadaInternaDTO(
	        ServicoCredenciadaDTO credenciadaInternaDTO) {
		this.credenciadaInternaDTO = credenciadaInternaDTO;
	}

	public void setProfissionalDTO(ServicoProfissionalDTO profissionalDTO) {
		this.profissionalDTO = profissionalDTO;
	}

	public void setVlCoberto(Double vlCoberto) {
		this.vlCoberto = vlCoberto;
	}

	public void setVlAberto(Double vlAberto) {
		this.vlAberto = vlAberto;
	}

	public Integer getRdTipoEncaminhamento() {
		return rdTipoEncaminhamento;
	}

	public Integer getRdTipoEncaminhamentoInterno() {
		return rdTipoEncaminhamentoInterno;
	}

	public ServicoProfissionalDTO getProfissionalDTO() {
		return profissionalDTO;
	}

	public ServicoCredenciadaDTO getCredenciadaInternaDTO() {
		return credenciadaInternaDTO;
	}

	public ServicoCredenciadaDTO getCredenciadaExternaDTO() {
		return credenciadaExternaDTO;
	}

	public Double getVlAberto() {
		return vlAberto;
	}

	public Double getVlCoberto() {
		return vlCoberto;
	}

	public String getInCobertoFormatado() {
		if(inCoberto!= null && inCoberto)
			inCobertoFormatado = "SIM";
		else
			inCobertoFormatado = "NÃO";
		
		return inCobertoFormatado;
	}

	public void setInCobertoFormatado(String inCobertoFormatado) {
		this.inCobertoFormatado = inCobertoFormatado;
	}

}