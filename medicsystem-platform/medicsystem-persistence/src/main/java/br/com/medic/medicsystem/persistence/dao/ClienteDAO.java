package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.appmobile.dto.ClienteInfoDTO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.ClienteDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteFpDTO;
import br.com.medic.medicsystem.persistence.dto.CobrancaDTO;
import br.com.medic.medicsystem.persistence.dto.NotificacaoClienteDTO;
import br.com.medic.medicsystem.persistence.dto.SituacaoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Anamnese;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Informativo;
import br.com.medic.medicsystem.persistence.model.ObservacaoCliente;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AniversariantesView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedico;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedicoAnamneseView;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;
import br.com.medic.medicsystem.persistence.model.views.DespesaView;
import br.com.medic.medicsystem.persistence.model.views.ViewAgendamentoSMS;

@Named("cliente-dao")
@ApplicationScoped
public class ClienteDAO extends RelationalDataAccessObject<Cliente> {

	public List<Cliente> getNovosClientesDoDia(Date dataInclusao) {

		Query q = findByNamedQuery("mNovosClientes", Cliente.class);
		q.setParameter("dtincl", dataInclusao);

		@SuppressWarnings("unchecked")
		ArrayList<Cliente> result = (ArrayList<Cliente>) q.getResultList();

		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<NotificacaoClienteDTO> getClientesNovoContratoBoleto(Long hoursInterval) {
		
		String queryStr ="Select cli.idcliente, cli.nmcliente, cli.nrcelular, cli.nrtelefone, c.idcontrato "
				+ " from realvida.tbcliente cli "
				+ " inner join realvida.tbcontratocliente cc on cli.idcliente = cc.idcliente "
				+ " inner join realvida.tbcontrato c on cc.idcontrato = c.idcontrato"
				+ " where c.dtcontrato = current_date"
				+ " and c.informapagamento = 0 "
				+ " and c.insituacao = 0 "
				+ " and c.idempresacliente is null "
				+ " and c.idplano is not null ";
		
		if(hoursInterval != null && hoursInterval > 0) {
			String intervalParam = hoursInterval + " hours";
			queryStr += " AND c.dtinclusaolog BETWEEN (CURRENT_TIMESTAMP - INTERVAL '" + intervalParam + "') AND CURRENT_TIMESTAMP ";
		}
		try {
			List<NotificacaoClienteDTO> result = findByNativeQuery(
					queryStr)
					.unwrap(SQLQuery.class)
					.addScalar("idCliente", LongType.INSTANCE)
					.addScalar("nmCliente", StringType.INSTANCE)
					.addScalar("nrCelular", StringType.INSTANCE)
					.addScalar("nrTelefone", StringType.INSTANCE)
					.addScalar("idContrato", LongType.INSTANCE)
					.setResultTransformer(
							Transformers
									.aliasToBean(NotificacaoClienteDTO.class))
					.list();
			
//			if (result == null || result.isEmpty()) {
//				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
//			}
//			else	
				return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}
	
	public List<NotificacaoClienteDTO> getClientesContratoRenovado(Long hoursInterval) {
		
		String queryStr ="SELECT cli.idcliente, cli.nmcliente, cli.nrcelular, cli.nrtelefone, c.idcontrato "
				+ " FROM realvida.tbcliente cli "
				+ " INNER join realvida.tbcontratocliente cc on cli.idcliente = cc.idcliente "
				+ " INNER join realvida.tbcontrato c on cc.idcontrato = c.idcontrato "
				+ " WHERE c.dtrenovacao = CURRENT_DATE "
				+ " AND c.informapagamento = 0 "
				+ " AND c.insituacao = 0 "
				+ " AND c.idempresacliente is null "
				+ " AND c.idplano is not null ";
		
		if(hoursInterval != null && hoursInterval > 0) {
			String intervalParam = hoursInterval + " hours";
			queryStr += " AND c.dtrenovacao BETWEEN (CURRENT_TIMESTAMP - INTERVAL '" + intervalParam + "') AND CURRENT_TIMESTAMP ";
		}
		try {
			@SuppressWarnings("unchecked")
			List<NotificacaoClienteDTO> result = findByNativeQuery(queryStr)
					.unwrap(SQLQuery.class)
					.addScalar("idCliente", LongType.INSTANCE)
					.addScalar("nmCliente", StringType.INSTANCE)
					.addScalar("nrCelular", StringType.INSTANCE)
					.addScalar("nrTelefone", StringType.INSTANCE)
					.addScalar("idContrato", LongType.INSTANCE)
					.setResultTransformer(
							Transformers
									.aliasToBean(NotificacaoClienteDTO.class))
					.list();
			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}

	public List<Cliente> getAllClientes() {

		TypedQuery<Cliente> query = entityManager.createQuery(
		        "SELECT c FROM Cliente c", Cliente.class);
		List<Cliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}

	public List<Informativo> getInformativo() {
		String queryStr = "SELECT inf FROM Informativo inf";

				TypedQuery<Informativo> query = entityManager.createQuery(queryStr,
		        Informativo.class);
		List<Informativo> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<ClienteView> getClientes(String nrCodCliente, String nmCliente,
	        String nrCPF, String nrRG, Integer startPosition, Integer maxResults) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ClienteView> criteria = cb.createQuery(ClienteView.class);

		Root<ClienteView> clienteRoot = criteria.from(ClienteView.class);
		criteria.select(clienteRoot);

		Predicate p = cb.conjunction();
		if (nrCodCliente != null) {
			p = cb.and(p,
			        cb.equal(clienteRoot.get("nrCodCliente"), nrCodCliente));
		}
		if (nrCPF != null) {
			p = cb.and(p, cb.equal(clienteRoot.get("nrCPF"), nrCPF));
		}
		if (nrRG != null) {
			p = cb.and(p, cb.equal(clienteRoot.get("nrRG"), nrRG));
		}
		if (nmCliente != null) {
			String where_cliente;

			if(nmCliente.startsWith("%") || nmCliente.endsWith("%")) {
				where_cliente = nmCliente.toUpperCase();

			} else {
				where_cliente = "%" + nmCliente.toUpperCase() + "%";
			}

			p = cb.and(p, cb.like(clienteRoot.get("nmCliente"), where_cliente));
		}

		p = cb.and(p, cb.isNull(clienteRoot.get("dtExclusao")));

		criteria.where(p);

		TypedQuery<ClienteView> q = entityManager.createQuery(criteria);
		startPosition = startPosition == null ? 0 : startPosition;
		maxResults = maxResults == null ? 10 : maxResults;
		q.setFirstResult(startPosition);
		q.setMaxResults(maxResults);
		List<ClienteView> result = q.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<AgendamentoView> getAgendamentos(Long idCliente, boolean todos) {
		String queryStr = "SELECT av FROM AgendamentoView av "
		        + "WHERE av.dtExclusao is null " + "AND av.cliente.id = "
		        + idCliente;
		//queryStr += " AND (av.inStatus <> 2 OR av.boSolicitouRetorno = true) ";

		if (!todos) {
			queryStr += " AND av.dtAgendamento >= current_date ";
		}

		queryStr += " ORDER BY av.dtAgendamento DESC, av.hrAgendamento ASC";
		TypedQuery<AgendamentoView> query = entityManager.createQuery(queryStr,
		        AgendamentoView.class);
		List<AgendamentoView> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Collection<ViewAgendamentoSMS> getAgendamentosDiasRestantesPorCliente(Integer diasRestantes) {


		String queryStr = "select vsms.* from realvida.viewagendamentossms vsms"
				+ " WHERE dtagendamento = (CURRENT_DATE + " + diasRestantes + " + 1) ";
		try {

			Collection<ViewAgendamentoSMS> result = findByNativeQuery(queryStr,
					ViewAgendamentoSMS.class);

			if (result.isEmpty()) {
				return null;
			}

			return result;

		} catch (Exception e) {
			return null;
		}



	}

	public List<Dependente> getDependentes(Long idCliente) {
		TypedQuery<Dependente> query = entityManager.createQuery(
		        "SELECT d FROM Dependente d where d.cliente.id = " + idCliente + " and d.dtExclusao is null",
		        Dependente.class);
		List<Dependente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Collection<Contrato> getContratos(Long idCliente) {

		String queryStr = "SELECT cont.* FROM realvida.tbcontrato cont"
		        + " INNER JOIN realvida.tbcontratocliente contclit ON contclit.idcontrato = cont.idcontrato"
		        + " INNER JOIN realvida.tbcliente cli ON contclit.idcliente = cli.idcliente"
		        + " WHERE cont.insituacao = 0 AND cli.idcliente = " + idCliente;

		Collection<Contrato> result = findByNativeQuery(queryStr,
		        Contrato.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<ObservacaoCliente> getObservacaoCliente(Long idCliente) {
		TypedQuery<ObservacaoCliente> query = entityManager.createQuery(
		        "SELECT o FROM ObservacaoCliente o where o.cliente.id = "
		                + idCliente, ObservacaoCliente.class);
		List<ObservacaoCliente> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Collection<ContratoDependente> getDependentesContratos(Long idCliente,
	        Long idDependente) {
		String queryStr = "SELECT contdep.* FROM realvida.tbcontrato cont"
		        + " INNER JOIN realvida.tbcontratodependente contdep ON contdep.idcontrato = cont.idcontrato"
		        + " INNER JOIN realvida.tbcontratocliente contclit ON contclit.idcontrato = cont.idcontrato"
		        + " INNER JOIN realvida.tbcliente cli ON contclit.idcliente = cli.idcliente"
		        + " WHERE cont.insituacao = 0 AND contdep.insituacao = 0 AND cli.idcliente = "
		        + idCliente + " AND contdep.iddependente = " + idDependente
		        + "  ORDER BY cont.idplano ASC";

		Collection<ContratoDependente> result = findByNativeQuery(queryStr,
				ContratoDependente.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Integer getUltimoCodigoGerado() {

		String queryStr = "SELECT CAST(nrcodcliente AS INTEGER) "
		        + " FROM realvida.tbcliente " + " ORDER BY nrcodcliente DESC";

		Query query = entityManager.createNativeQuery(queryStr);

		query.setMaxResults(1);
		Integer result = (Integer) query.getSingleResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}

	public Cliente getClientePorCPF(String nrCPF) {
		TypedQuery<Cliente> query = entityManager.createQuery(
		        "SELECT o FROM Cliente o where o.nrCPF = :nrCPF ",
		        Cliente.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		query.setParameter("nrCPF", nrCPF);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	public ClienteFpDTO getClienteFpPorCPFMatricula(String nrCPF, String nrMatricula) {

		String query = "select matricula, nome, orgao, cpf, uf, nascimento, endereco, cep, fone, banco, agencia, digitoagencia, conta, digitoconta, cargo "
				+ " from vendas.new_prospect np where np.matricula is not null";
		String where = "";

		if(nrCPF != null)
			where += " and np.cpf = '" + nrCPF + "'";

		if(nrMatricula != null)
			where += " and np.matricula = '" + nrMatricula + "'";

		ClienteFpDTO result = (ClienteFpDTO) findByNativeQuery(query + where)
		        .unwrap(SQLQuery.class)
		        .addScalar("matricula", StringType.INSTANCE)
		        .addScalar("nome", StringType.INSTANCE)
		        .addScalar("orgao", StringType.INSTANCE)
		        .addScalar("cpf", StringType.INSTANCE)
		        .addScalar("uf", StringType.INSTANCE)
		        .addScalar("nascimento", StringType.INSTANCE)
		        .addScalar("endereco", StringType.INSTANCE)
		        .addScalar("cep", StringType.INSTANCE)
		        .addScalar("fone", StringType.INSTANCE)
		        .addScalar("banco", StringType.INSTANCE)
		        .addScalar("agencia", StringType.INSTANCE)
		        .addScalar("digitoagencia", StringType.INSTANCE)
		        .addScalar("conta", StringType.INSTANCE)
		        .addScalar("digitoconta", StringType.INSTANCE)
		        .addScalar("cargo", StringType.INSTANCE)
		        .uniqueResult();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}

	@SuppressWarnings("unchecked")
	public List<SituacaoDTO> listSituacaoCliente(Long idCliente) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dtAtual = df.format(new Date());

		String query = "SELECT nrcontrato, nmplano, nmformacombinada, count(nmsituacao) as qtdaberto, nmsituacao, dtvencimento  "
		        + " FROM realvida.tbSituacaoCliente_View "
		        + " WHERE idcliente = "
		        + idCliente
		        + " AND nmsituacao <> 'Quitada' "
		        + " AND dtpagamento is null "
		        + " AND dtvencimento < '"
		        + dtAtual
		        + "'"
		        + " AND insituacao = 'ATIVO'"
		        + " AND (bonegociada is null or bonegociada is false ) "
		        + " GROUP BY nrcontrato, nmplano, nmsituacao, nmformacombinada, dtvencimento "
		        + " ORDER BY dtvencimento DESC";

		List<SituacaoDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("nrcontrato", StringType.INSTANCE)
		        .addScalar("nmplano", StringType.INSTANCE)
		        .addScalar("nmformacombinada", StringType.INSTANCE)
		        .addScalar("qtdaberto", IntegerType.INSTANCE)
		        .addScalar("nmsituacao", StringType.INSTANCE)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(SituacaoDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<CobrancaDTO> getSituacaoFinanceiraQuitada(Long idCliente, String nrContrato) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String query ="SELECT * FROM realvida.tbsituacaocliente_view scv  "
		 +" WHERE scv.idCliente = :idCliente"
		 +" AND scv.nrContratoPesq = :nrContrato"
		 +" AND scv.dtPagamento IS NOT NULL  "
		 +" AND scv.dtVencimento between '2001-01-01' AND '" +df.format(new Date())+"'"
		 +" ORDER BY scv.dtPagamento DESC LIMIT 10";


		List<CobrancaDTO> result = findByNativeQuery(query)
		        .setParameter("idCliente", idCliente)
		        .setParameter("nrContrato", nrContrato.replaceAll("[^0-9]", ""))
		        .unwrap(SQLQuery.class)
		        .addScalar("nmTipo", StringType.INSTANCE)
		        .addScalar("nrDocumento", StringType.INSTANCE)
		        .addScalar("nmFormaCombinada", StringType.INSTANCE)
		        .addScalar("nmFormaQuitacao", StringType.INSTANCE)
		        .addScalar("dtVencimento", DateType.INSTANCE)
		        .addScalar("vlDocumento", DoubleType.INSTANCE)
		        .addScalar("vlPago", DoubleType.INSTANCE)
		        .addScalar("dtPagamento", DateType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(CobrancaDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<ClienteDTO> getClienteDTO(Integer inTipo,String dtInicio,String dtFim, List<Long> idUnidade) {
		// 0 = TODOS; 1 = ASSOCIADOS; 2 = NAO ASSOCIADOS
		String tipo = "";
		if (inTipo == 0){
			tipo = "";
		}
		else if (inTipo == 1){
			tipo = "false";
		}
		else if (inTipo == 2){
			tipo = "true";
		}

		String listUnidades = formatarIds(idUnidade);

		String query =" select cli.latitude, cli.longitude"
		+ " from realvida.tbagendamento ag"
		+ " inner join realvida.tbcliente cli on cli.idcliente = ag.idcliente and ag.dtexclusao is null"
		+ " where ag.dtagendamento between '" + dtInicio + "' and '" + dtFim + "'"
		+ (tipo.length() < 1 ? "" : " and cli.bonaoassociado is "+tipo)
		+ (listUnidades != null && listUnidades.length() > 0 ? " and ag.idunidade in ("+listUnidades+") ": "")
		+ " and (cli.latitude < 2.100410 and cli.latitude > -9.734057 and cli.latitude is not null)";


		List<ClienteDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("latitude", FloatType.INSTANCE)
		        .addScalar("longitude", FloatType.INSTANCE)

		        .setResultTransformer(
		                Transformers.aliasToBean(ClienteDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}

	private String formatarIds (List<Long> listIds){
		String ids = "";

		if (listIds != null && listIds.size() > 0 ){
			if (listIds.size() == 1 ){
				return listIds.get(0).toString();
			}
		for (Long id : listIds) {
			ids += id + ",";
		}
		return ids.substring(0, ids.length() - 1);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ClienteDTO> getClienteDTOLatLng() {

		String query ="select cli.nmlogradouro, cli.nrnumero, cid.nmcidade, bai.nmbairro, cli.idcliente, cli.latitude, cli.longitude "
		+ "from realvida.tbcliente cli "
		+ "inner join realvida.tbcidade cid on cli.idcidade = cid.idcidade "
		+ "inner join realvida.tbbairro bai on cli.idbairro = bai.idbairro "
		+ "where cli.nmlogradouro is not null "
		+ "and cli.nrnumero is not null "
		+ "and cid.nmcidade is not null "
		+ "and bai.nmbairro is not null "
		+ "and cli.latitude is null "
		+ "and cli.longitude is null "
		+ "group by cli.nmlogradouro, cli.nrnumero, cid.nmcidade, bai.nmbairro, cli.idcliente "
		+ "order by cid.nmcidade, bai.nmbairro "
		+ "limit 100";

		List<ClienteDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmLogradouro", StringType.INSTANCE)
		        .addScalar("nmBairro", StringType.INSTANCE)
		        .addScalar("nmCidade", StringType.INSTANCE)
		        .addScalar("nrNumero", StringType.INSTANCE)
		        .addScalar("latitude", FloatType.INSTANCE)
		        .addScalar("longitude", FloatType.INSTANCE)
		        .addScalar("idCliente", LongType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ClienteDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}

	public List<DespesaView> getClienteDespesas(Long idCliente) {
		TypedQuery<DespesaView> query = entityManager.createQuery(
		        "SELECT mdw FROM DespesaView mdw where mdw.idCliente = "
		                + idCliente + " order by mdw.id DESC",
		        DespesaView.class);
		List<DespesaView> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Integer desbloquearCliente(Long idCliente) {

		String query = "select realvida.desbloquearclientes(" + idCliente + ")";

		return (Integer) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .uniqueResult();
	}

	public Integer bloquearCliente(Long idCliente) {
		String query = "select realvida.bloquearclientes(" + idCliente + ")";

		return (Integer) findByNativeQuery(query).unwrap(SQLQuery.class)
		        .uniqueResult();
	}

	public Collection<AniversariantesView> getAniversariantes() {

//		String queryStr = "SELECT DISTINCT ON (cliente,celular) * FROM realvida.view_aniversariantes ANI"
//						  + " WHERE  ANI.celular IS NOT NULL"
//						  + " AND ANI.DIA = (SELECT date_part('DAY', current_timestamp) + 1)"
//						  + " AND ANI.MES = (SELECT date_part('MONTH', current_timestamp))"
//						  + "ORDER BY ANI.cliente";

		String queryStr = "SELECT DISTINCT ON (ANI.cliente,ANI.celular) c.idempresagrupo,ANI.* FROM realvida.view_aniversariantes ANI join realvida.tbcontratocliente cc"
				+ " on ANI.idcliente = cc.idcliente join realvida.tbcontrato c on cc.idcontrato = c.idcontrato"
				+ "  WHERE  ANI.celular IS NOT NULL"
				+ "  AND ANI.DIA = (SELECT date_part('DAY', current_timestamp) + 1)"
				+ "  AND ANI.MES = (SELECT date_part('MONTH', current_timestamp))"
				+ "  AND c.insituacao = 0"
				+ "  and c.idempresagrupo = 9"
				+ "  ORDER BY ANI.cliente";

		Collection<AniversariantesView> result = findByNativeQuery(queryStr,
				AniversariantesView.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}


	public List<AtendimentoMedico> getAtendimentoMedicoHistoricoView(Long idPaciente, Long idEspecialidade, Character tipoPaciente){

		String queryStr = "SELECT hist FROM AtendimentoMedico hist WHERE hist.idPaciente = "+idPaciente
				+ " AND idEspecialidade = "+idEspecialidade +" AND hist.inTipoPaciente = '"+tipoPaciente+"'";


		TypedQuery<AtendimentoMedico> query = entityManager.createQuery(queryStr,
				AtendimentoMedico.class);

		query.setMaxResults(10);

		List<AtendimentoMedico> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Anamnese getAnamnese(Long idPaciente, String inTipoPaciente){

		String queryStr = "SELECT ana FROM Anamnese ana";

		if(inTipoPaciente.equals("D")){
			queryStr += " WHERE ana.dependente.id = "+idPaciente;
		}
		else
			queryStr += " WHERE ana.dependente.id = null AND ana.cliente.id = "+idPaciente;


		TypedQuery<Anamnese> query = entityManager.createQuery(queryStr,
				Anamnese.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}


	public Anamnese getAnamnesePorId(Long id){

		String queryStr = "SELECT ana FROM Anamnese ana WHERE ana.id = "+id;

		TypedQuery<Anamnese> query = entityManager.createQuery(queryStr,
				Anamnese.class);

		query.setFirstResult(0);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}


	public List<AtendimentoMedicoAnamneseView> getAtendimentoMedicoAnamneseView(Long idPaciente, String inTipoPaciente){

		String queryStr = "SELECT anapac FROM AtendimentoMedicoAnamneseView anapac ";

		if(inTipoPaciente.equals("D")){
			queryStr += " WHERE anapac.idDependente = "+idPaciente;
		}
		else
			queryStr += " WHERE anapac.idDependente = null AND anapac.idCliente = "+idPaciente;


		TypedQuery<AtendimentoMedicoAnamneseView> query = entityManager.createQuery(queryStr,
				AtendimentoMedicoAnamneseView.class);


		List<AtendimentoMedicoAnamneseView> result = query.getResultList();

		if (result == null) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Cliente updateCliente(Cliente cliente){
		return entityManager.merge(cliente);
	}

	public BigInteger getCodigoImpressaoBoletoSL() {

		String query = "select nextval('realvida.impressaoboleto_id_seq')";

		return (BigInteger) findByNativeQuery(query)
				.unwrap(SQLQuery.class)
		        .uniqueResult();
	}

	public ClienteView clienteViewById(Long idCliente) {
		return entityManager.find(ClienteView.class, idCliente);
	}

	//----------------------------------------------------------
	// APLICATIVO
	public ClienteView getClienteView(Long idCliente) {
		CriteriaBuilder cb = getCriteriaBuilder();
		CriteriaQuery<ClienteView> criteria = cb.createQuery(ClienteView.class);

		Root<ClienteView> clienteRoot = criteria.from(ClienteView.class);
		criteria.select(clienteRoot);
		Predicate p = cb.equal(clienteRoot.get("id"), idCliente);
		criteria.where(p);
		try {
			TypedQuery<ClienteView> query = entityManager.createQuery(criteria);
			return query.getSingleResult();
		} catch(Exception e) {

		}
		return null;
	}
	
	public ClienteInfoDTO getClienteInfo(Long idCliente, Integer tipoUsuario) {
		try {
			String queryStr = null;
			if(tipoUsuario.intValue() == TipoUsuario.CLIENTE.valorTipo) {
				queryStr = "SELECT c.idcliente, c.nmcliente, c.nrcelular, c.nmemail, "
						+ "1 AS tipocliente, "
						+ "(CASE "
						+ "WHEN (c.nmcliente IS NULL OR c.nrrg IS NULL OR c.nrcelular IS NULL "
						+ "OR c.dtnascimento IS NULL OR c.nrcep IS NULL or c.nmlogradouro IS NULL "
						+ "OR c.nrnumero IS NULL) THEN 4 "
						+ "WHEN realvida.clientebloqueado(c.idcliente) = 2 AND c.dtexclusao IS NULL THEN 0 "
						+ "WHEN realvida.clientebloqueado(c.idcliente) = 0 AND c.dtexclusao IS NULL THEN 1 "
						+ "WHEN realvida.clientebloqueado(c.idcliente) = 1 AND c.dtexclusao IS NULL THEN 2 "
						+ "END ) AS situacaogeral "
						+ "FROM realvida.tbcliente c WHERE c.idcliente = :idCliente";
			} else {
				queryStr = "SELECT d.iddependente AS idcliente, d.nmdependente AS nmcliente, "
						+ "d.nrcelular, d.nmemail, 2 AS tipocliente, "
						+ "(CASE "
						+ "WHEN (d.nmdependente IS NULL OR d.nrcelular IS NULL OR d.dtnascimento IS NULL "
						+ "OR d.nrcep IS NULL or d.nmlogradouro IS NULL OR d.nrnumero IS NULL) THEN 4 "
						+ "WHEN realvida.clientebloqueado(d.idcliente) = 2 AND d.dtexclusao IS NULL THEN 0 "
						+ "WHEN realvida.clientebloqueado(d.idcliente) = 0 AND d.dtexclusao IS NULL THEN 1 "
						+ "WHEN realvida.clientebloqueado(d.idcliente) = 1 AND d.dtexclusao IS NULL THEN 2 "
						+ "END ) AS situacaogeral "
						+ "FROM realvida.tbdependente d WHERE d.iddependente = :idCliente";
			}
			Query query = findByNativeQuery(queryStr).setParameter("idCliente", idCliente);
			query.unwrap(SQLQuery.class)
			.addScalar("idCliente", LongType.INSTANCE)
			.addScalar("nmCliente", StringType.INSTANCE)
			.addScalar("nrCelular", StringType.INSTANCE)
			.addScalar("nmEmail", StringType.INSTANCE)
			.addScalar("tipoCliente", IntegerType.INSTANCE)
			.addScalar("situacaoGeral", IntegerType.INSTANCE)
			.setResultTransformer(
					Transformers.aliasToBean(ClienteInfoDTO.class));

			ClienteInfoDTO result = (ClienteInfoDTO) query.getSingleResult();
			return result;
		} catch (NoResultException e) {
		}
		return null;
	}

	public Cliente updateClienteInfo(Long idCliente, ClienteInfoDTO info) {
		String queryStr = "SELECT c FROM Cliente c WHERE c.id = :idCliente";
		TypedQuery<Cliente> query = entityManager.createQuery(queryStr, Cliente.class);
		query.setParameter("idCliente", idCliente);
		query.setMaxResults(1);
		try {
			Cliente cli = query.getSingleResult();
			if(info.getNmCliente() != null && !info.getNmCliente().equals(cli.getNmCliente()) ) {
				cli.setNmCliente(info.getNmCliente());
			}
			if(info.getNmEmail() != null && !info.getNmEmail().equals(cli.getNmEmail()) ) {
				cli.setNmEmail(info.getNmEmail());
			}
			if(info.getNrCelular() != null && !info.getNrCelular().equals(cli.getNrCelular())) {
				cli.setNrCelular(info.getNrCelular());
			}
			this.update(cli);
			return cli;
		} catch (NoResultException e) {
		}
		return null;
	}

	public Cliente getClienteByContrato(Long idContrato) {
		String queryStr = "SELECT cc.cliente FROM ContratoCliente cc "
				+ " WHERE cc.contrato.id = :idContrato";
		TypedQuery<Cliente> query = entityManager.createQuery(queryStr, Cliente.class);
		try {
			query.setMaxResults(1);
			query.setParameter("idContrato", idContrato);
			Cliente result = (Cliente) query.getSingleResult();
			return result;
		} catch (Exception e) {
			return null;
		}
	}
}
