package br.com.medic.medicsystem.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ConferenciaEmpresaClienteDTO {
	
	String mesAno;
	
	String period;
	
	Date dtFechamento;
	
	String nmFuncionario;
	
	String observacao;
	
	String obsDadosFinanceiros;

	Date dtPagamento;
	
	BigDecimal vlPago;
	
	BigDecimal vlMensalidade;
	
	String linkBoleto;
	
	public Date getDtPagamento() {
		return dtPagamento;
	}

	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	public BigDecimal getVlPago() {
		return vlPago;
	}

	public void setVlPago(BigDecimal vlPago) {
		this.vlPago = vlPago;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public BigDecimal getVlMensalidade() {
		return vlMensalidade;
	}

	public void setVlMensalidade(BigDecimal vlMensalidade) {
		this.vlMensalidade = vlMensalidade;
	}

	public String getLinkBoleto() {
		return linkBoleto;
	}

	public void setLinkBoleto(String linkBoleto) {
		this.linkBoleto = linkBoleto;
	}

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public Date getDtFechamento() {
		return dtFechamento;
	}

	public void setDtFechamento(Date dtFechamento) {
		this.dtFechamento = dtFechamento;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObsDadosFinanceiros() {
		return obsDadosFinanceiros;
	}

	public void setObsDadosFinanceiros(String obsDadosFinanceiros) {
		this.obsDadosFinanceiros = obsDadosFinanceiros;
	}

}
