package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "Caixa_Pagamento_View")
public class FormaPagamentoCaixa {
	
	@Id
	@Column(name = "idCaixa")
	private Long idCaixa;
	
	@Basic
	@Column(name = "informapagamento")
	private Integer inFormaPagamento;
	
	@Basic
	@Column(name = "vlmovdetalhamento")
	private Float vlMovdetalhamento;

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	}

	public Integer getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Integer inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Float getVlMovdetalhamento() {
		return vlMovdetalhamento;
	}

	public void setVlMovdetalhamento(Float vlMovdetalhamento) {
		this.vlMovdetalhamento = vlMovdetalhamento;
	}
	
	

}
