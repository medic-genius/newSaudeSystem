package br.com.medic.medicsystem.persistence.dto;

import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SummaryGuiaServicoByServicoDTO {

	private Integer qdtServico;
	
	private String nmServico;
	
	private Long idServico;

	private Double vlServicoAssociado;
	
	private Double custoServico;

	private String sqlFieldsSelect =
			"count(servico.idservico) as qdt_servico, " 
			+ "servico.nmservico, " 
			+ "cp.vlservico, "
			+ "servico.idservico, "			
			+ "cp.vlservicoassociado";

	private String sqlFrom = "realvida.tbguiaservico guia_servico";

	private String sqlJoins =
		"inner join realvida.tbservico as servico on servico.idservico = guia_servico.idservico "
		+ "inner join realvida.tbguia as guia on guia.idguia = guia_servico.idguia "
		+ "inner join realvida.tbcontrato as contrato on contrato.idcontrato = guia.idcontrato "
		+ "inner join realvida.tbplano as plano on plano.idplano = contrato.idplano "
		+ "inner join realvida.tbcoberturaplano as cp on plano.idplano = cp.idplano and cp.idservico = servico.idservico "
		+ "inner join realvida.tbempresacliente as empresacliente on empresacliente.idempresacliente = contrato.idempresacliente";

	private String sqlWhere = null;
	
	
	

	public Integer getQdtServico() {
		return qdtServico;
	}

	public void setQdtServico(Integer qdtServico) {
		this.qdtServico = qdtServico;
	}

	public Double getCustoServico() {
		return custoServico;
	}

	public void setCustoServico(Double custoServicos) {
		this.custoServico = custoServicos;
	}
	
	public Double getVlServicoAssociado() {
		return vlServicoAssociado;
	}

	public void setVlServicoAssociado(Double vlServicoAssociado) {
		this.vlServicoAssociado = vlServicoAssociado;
	}

	private String sqlOrderBy = "guia_servico.idguiaservico desc";

	@JsonIgnore
	public String getSqlSelect() {
		String sqlQuery =
				"select "
				+ this.sqlFieldsSelect
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		sqlQuery += " group by servico.idservico, cp.vlservico, cp.vlservicoassociado";
		if(null != this.sqlOrderBy) {
			sqlQuery += " order by " + this.sqlOrderBy;
		}
		return sqlQuery;
	}
	
	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	@JsonIgnore
	public String getSqlCount() {
		String sqlQuery =
				"select "
				+ " count(*)"
				+ " from"
				+ " " + this.sqlFrom
				+ " " + this.sqlJoins
			;
		if(null != this.sqlWhere) {
			sqlQuery += " where " + this.sqlWhere;
		}
		return sqlQuery;
	}

	public void addSqlWhereConditionAnd(String condition) {
		if(null == this.sqlWhere) {
			this.sqlWhere = condition;
		} else {
			this.sqlWhere += " and " + condition;
		}
	}

	@JsonIgnore
	public String getSqlOrderBy() {
		return this.sqlOrderBy;
	}
	
	public void setSqlOrderBy(String sqlOrderBy) {
		this.sqlOrderBy = sqlOrderBy;
	}
	
	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

}
