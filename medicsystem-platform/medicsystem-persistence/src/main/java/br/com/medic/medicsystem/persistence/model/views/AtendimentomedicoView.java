package br.com.medic.medicsystem.persistence.model.views;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "realvida", name = "atendimentomedico_agendamentos")
public class AtendimentomedicoView {
	
	@Id
	@Column(name = "idagendamento")
	private Long idAgendamento;
	
	
	@Basic
	@Column(name = "instatus")
	private Integer inStatus;
	
	@Basic
	@Column(name = "intipopaciente")
	private Character inTipoPaciente;
	
	@Basic
	@Column(name = "idpaciente")
	private Long idPaciente;
	
	@Basic
	@Column(name = "paciente")
	private String nmPaciente;
	
	@JsonIgnore
	@Basic
	@Column(name = "dtnascimento")
	private Date dtNascimento;
	
	@Basic
	@Column(name = "idespecialidade")
	private Long idEspecialidade;
	
	
	@Basic
	@Column(name = "especialidade")
	private String nmEspecialidade;
	
	
	@Basic
	@Column(name = "idmedico")
	private Long idmedico;
	
	@Basic
	@Column(name = "medico")
	private String nmProfissional;
	
	@Basic
	@Column(name = "idservico")
	private Long idServico;
	
	@Basic
	@Column(name = "servico")
	private String nmServico;
	
	@Basic
	@Column(name = "horapresenca")
	private Time horaPresenca;
	
	@Basic
	@Column(name = "bostatustriagem")
	private Boolean boStatusTriagem;
	

	@Basic
	@Column(name = "idunidade")
	private Long idUnidade;
	
	@Basic
	@Column(name = "idconsultorio")
	private Long idConsultorio;
	
	@Basic
	@Column(name = "nmconsultorio")
	private String nmConsultorio;
	
	@Basic
	@Column(name = "localizacao")
	private String localizacao;
	
	@Basic
	@Column(name = "botriagem")
	private Boolean boTriagem;
	
	@Basic
	@Column(name = "boprioridade")
	private Boolean boPrioridade;
	
	@Basic
	@Column(name = "hrficha")
	private Time hrFicha;
	
	@Basic
	@Column(name = "nrficha")
	private String nrFicha;
	
	@Basic
	@Column(name = "hrinicio")
	private Time hrInicio;
	
	@Basic
	@Column(name="intipoprioridade")
	private Integer inTipoPrioridade;
	
	@Basic
	@Column(name="instatusprioridade")
	private Integer inStatusPrioridade;
	
	@Basic
	@Column(name="obsurgencia")
	private String obsUrgencia;
	
	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Character getInTipoPaciente() {
		return inTipoPaciente;
	}

	public void setInTipoPaciente(Character inTipoPaciente) {
		this.inTipoPaciente = inTipoPaciente;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}
	
	@JsonIgnore
	public Date getDtNascimento() {
		return dtNascimento;
	}

	@JsonIgnore
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Long getIdEspecialidade() {
		return idEspecialidade;
	}

	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}

	public String getNmEspecialidade() {
		return nmEspecialidade;
	}

	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}



	public Long getIdmedico() {
		return idmedico;
	}

	public void setIdmedico(Long idmedico) {
		this.idmedico = idmedico;
	}

	public String getNmProfissional() {
		return nmProfissional;
	}

	public void setNmProfissional(String nmProfissional) {
		this.nmProfissional = nmProfissional;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNmServico() {
		return nmServico;
	}

	public void setNmServico(String nmServico) {
		this.nmServico = nmServico;
	}

	public Long getIdAgendamento() {
		return idAgendamento;
	}

	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
	
	@Transient
	public Integer getIdade() {
		GregorianCalendar hj=new GregorianCalendar();
		GregorianCalendar nascimento=new GregorianCalendar();
		
		if(getDtNascimento() != null){
				nascimento.setTime(getDtNascimento());
						
				int anohj=hj.get(Calendar.YEAR);
				int anoNascimento=nascimento.get(Calendar.YEAR);
				
				
				Integer idade = anohj-anoNascimento;
				
				 
				 if ( hj.get(Calendar.MONTH) < nascimento.get(Calendar.MONTH)) {
					 idade --;  
				    }
				
				return idade;
		
		}
		
		return 0;
	}

	public Time getHoraPresenca() {
		return horaPresenca;
	}

	public void setHoraPresenca(Time horaPresenca) {
		this.horaPresenca = horaPresenca;
	}

	public Boolean getBoStatusTriagem() {
		return boStatusTriagem;
	}

	public void setBoStatusTriagem(Boolean boStatusTriagem) {
		this.boStatusTriagem = boStatusTriagem;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Long getIdConsultorio() {
		return idConsultorio;
	}

	public void setIdConsultorio(Long idConsultorio) {
		this.idConsultorio = idConsultorio;
	}

	public String getNmConsultorio() {
		return nmConsultorio;
	}

	public void setNmConsultorio(String nmConsultorio) {
		this.nmConsultorio = nmConsultorio;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Boolean getBoTriagem() {
		return boTriagem;
	}

	public void setBoTriagem(Boolean boTriagem) {
		this.boTriagem = boTriagem;
	}

	public Boolean getBoPrioridade() {
		return boPrioridade;
	}

	public void setBoPrioridade(Boolean boPrioridade) {
		this.boPrioridade = boPrioridade;
	}

	public Time getHrFicha() {
		return hrFicha;
	}

	public void setHrFicha(Time hrFicha) {
		this.hrFicha = hrFicha;
	}
	
	public String getNrFicha() {
		return nrFicha;
	}
	
	public void setNrFicha(String nrFicha) {
		this.nrFicha = nrFicha;
	}

	public Time getHrInicio() {
		return hrInicio;
	}

	public void setHrInicio(Time hrInicio) {
		this.hrInicio = hrInicio;
	}

	public Integer getInTipoPrioridade() {
		return inTipoPrioridade;
	}

	public void setInTipoPrioridade(Integer inTipoPrioridade) {
		this.inTipoPrioridade = inTipoPrioridade;
	}

	public Integer getInStatusPrioridade() {
		return inStatusPrioridade;
	}

	public void setInStatusPrioridade(Integer inStatusPrioridade) {
		this.inStatusPrioridade = inStatusPrioridade;
	}

	public String getObsUrgencia() {
		return obsUrgencia;
	}

	public void setObsUrgencia(String obsUrgencia) {
		this.obsUrgencia = obsUrgencia;
	}

	
	
	
}
