package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.ReceituarioOftalmologico;


@Named("receituarioOftalmologico-dao")
@ApplicationScoped
public class ReceituarioOftalmologicoDAO extends RelationalDataAccessObject<ReceituarioOftalmologico>{
	
	public ReceituarioOftalmologico getReceituarioOftalPorAtendimento (Long idAtendimento){
		
		String query = "SELECT rec FROM ReceituarioOftalmologico rec WHERE rec.atendimento.id = "+ idAtendimento;
		
		TypedQuery<ReceituarioOftalmologico> queryStr = entityManager.createQuery(query,
				ReceituarioOftalmologico.class);
		
		queryStr.setFirstResult(0);
		queryStr.setMaxResults(1);
		
		try {
			return queryStr.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	
		
	}
	
	
	public List<ReceituarioOftalmologico> getReceituarioOftalmologicoPorIdPaciente(Long idPaciente, String inTipoPaciente){
		
		
	   String query = "SELECT rec FROM ReceituarioOftalmologico rec ";
	   
	    if(inTipoPaciente.equals("C"))
	    		query += " WHERE rec.cliente.id = "+idPaciente+" AND rec.dependente IS NULL";
	    
	    else if(inTipoPaciente.equals("D"))
    			query += " WHERE rec.dependente.id = "+idPaciente;
	    
	    
	    query += " ORDER BY dtreceituariooftalmologico DESC";
	    
		
		TypedQuery<ReceituarioOftalmologico> queryStr = entityManager.createQuery(query,
				ReceituarioOftalmologico.class);
		
		queryStr.setMaxResults(10);
		
		try {
			return queryStr.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		
		
	}

	
	
}
