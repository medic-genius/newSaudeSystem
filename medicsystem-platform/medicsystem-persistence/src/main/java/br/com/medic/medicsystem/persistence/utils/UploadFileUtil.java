package br.com.medic.medicsystem.persistence.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

public class UploadFileUtil {

	/**
	 * @author Joelton
	 * @since 02-05-2016
	 * @param header
	 * @return file name
	 */
	public static String getFileName( MultivaluedMap<String, String> header ) {	
	
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		for (String filename : contentDisposition) {
			if( filename.trim().startsWith("filename") ) {
				
				String[] name = filename.split("=");	
				String finalFilename = name[1].trim().replaceAll("\"", "");
				
				return finalFilename;
			}
		}
		
		return "unknown";
	}
	
	/**
	 * @author Joelton
	 * @since 02-05-2016 
	 * @param header
	 * @return file type
	 */
	public static String getFileType( MultivaluedMap<String, String> header ) {	
		
		String[] contentType = header.getFirst("Content-Type").split(";");
		for (String type : contentType) {	
			String[] separateType = type.split("/");
			type = separateType[1].trim();
				
			return type;
		}
		
		return "unknown";
	}

	/**
	 * @author Joelton
	 * @since 02-05-2016
	 * @param content - file to write
	 * @param fileName - name of the file
	 * 
	 * @throws IOException
	 */
	public static boolean writeFile(byte[] content, String fileName, String diretorio )  {
		
		boolean isSuccess = true;
		boolean isPath = true;
		
		File fileDirectory = new File( diretorio );
		
		if( !fileDirectory.exists() ) 
		     isPath = fileDirectory.mkdirs();
		
		if ( isPath ) {
			
			try {
				
				File file = new File( fileName );
				
				if( !file.exists() ) {
					file.createNewFile();
				} else {
					isSuccess = false; // arquivo sera atualizado. Nao precisa criar outra linha na base de dados
				}
				
				FileOutputStream fop = new FileOutputStream(file);
				
				fop.write(content);
				fop.flush();
				fop.close();
				
				
			} catch (Exception e) {
				
				isSuccess = false;
				e.printStackTrace();
				
			}
			
		}
			
		return isSuccess;
	}

	public static String retirarExtensao(String finalFilename) {
		String[] fullFileName = finalFilename.split("\\.");
		return fullFileName[0].trim();
	}
	
}
