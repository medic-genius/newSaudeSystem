package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.Timestamp;

@Entity
@Table(catalog = "realvida", name = "tbempresacliente")
public class EmpresaCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPRESACLIENTE_ID_SEQ")
	@SequenceGenerator(name = "EMPRESACLIENTE_ID_SEQ", sequenceName = "realvida.empresacliente_id_seq", allocationSize = 1)
	@Column(name = "idempresacliente")
	private Long id;

	@Basic
	@Column(name = "diafechamento")
	private Integer diaFechamento;

	@Basic
	@Column(name = "diavencimento")
	private Integer diaVencimento;

	@Basic
	@Column(name = "dtalteracao")
	private Date dtAlteracao;

	@Basic
	@Column(name = "dtcadastro")
	private Date dtCadastro;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtexclusao")
	private Date dtExclusao;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadorCadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name = "inacrescimovencimento")
	private Integer inAcrescimoVencimento;
	
	@Basic
	@Column(name = "nmcomplemento")
	private String nmComplemento;

	@Basic
	@Column(name = "nmemail")
	private String nmEmail;

	@Basic
	@Column(name = "nmfantasia")
	private String nmFantasia;

	@Basic
	@Column(name = "nmlogradouro")
	private String nmLogradouro;

	@Basic
	@Column(name = "nmpontoreferencia")
	private String nmPontoReferencia;

	@Basic
	@Column(name = "nmrazaosocial")
	private String nmRazaoSocial;

	@Basic
	@Column(name = "nmsite")
	private String nmSite;

	@Basic
	@Column(name = "nrcep")
	private String nrCep;

	@Basic
	@Column(name = "nrcnpj")
	private String nrCnpj;

	@Basic
	@Column(name = "nrfax")
	private String nrFax;

	@Basic
	@Column(name = "nrinscestadual")
	private String nrInscEstadual;

	@Basic
	@Column(name = "nrinscmunicipal")
	private String nrInscMunicipal;

	@Basic
	@Column(name = "nrinscsuframa")
	private String nrInscsuframa;

	@Basic
	@Column(name = "nrnumero")
	private String nrNumero;

	@Basic
	@Column(name = "nrtelefone1")
	private String nrTelefone1;

	@Basic
	@Column(name = "nrtelefone2")
	private String nrTelefone2;

	@Basic
	@Column(name = "nrtelefone3")
	private String nrtelefone3;

	@ManyToOne
	@JoinColumn(name = "idbairro")
	private Bairro bairro;

	@ManyToOne
	@JoinColumn(name = "idcidade")
	private Cidade cidade;

	@Basic
	@Column(name = "intipopagamento")
	private Integer inTipoPagamento; // 1 = DESPESA  2 = MENSALIDADE
	
	@Basic
	@Column(name = "intipocobranca") // 0 = FIXO   1 = POR VIDA
	private Integer inTipoCobranca;
	
	@Basic
	@Column(name = "nmlogin")
	private String nmLogin;
	
	@Basic
	@Column(name = "validadeguia")
	private Integer validadeGuia;
	

	public EmpresaCliente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDiaFechamento() {
		return diaFechamento;
	}

	public void setDiaFechamento(Integer diaFechamento) {
		this.diaFechamento = diaFechamento;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	public Date getDtAlteracao() {
		return dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Date getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}

	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}

	public Integer getInAcrescmoVencimento() {
		return inAcrescimoVencimento;
	}

	public void setInAcrescmoVencimento(Integer inAcrescmoVencimento) {
		this.inAcrescimoVencimento = inAcrescmoVencimento;
	}

	public Integer getInTipoPagamento() {
		return inTipoPagamento;
	}

	public void setInTipoPagamento(Integer inTipoPagamento) {
		this.inTipoPagamento = inTipoPagamento;
	}

	public String getNmComplemento() {
		return nmComplemento;
	}

	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}

	public String getNmLogradouro() {
		return nmLogradouro;
	}

	public void setNmLogradouro(String nmLogradouro) {
		this.nmLogradouro = nmLogradouro;
	}

	public String getNmPontoReferencia() {
		return nmPontoReferencia;
	}

	public void setNmPontoReferencia(String nmPontoReferencia) {
		this.nmPontoReferencia = nmPontoReferencia;
	}

	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}

	public String getNmSite() {
		return nmSite;
	}

	public void setNmSite(String nmSite) {
		this.nmSite = nmSite;
	}

	public String getNrCep() {
		return nrCep;
	}

	public void setNrCep(String nrCep) {
		this.nrCep = nrCep;
	}

	public String getNrCnpj() {
		return nrCnpj;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public String getNrFax() {
		return nrFax;
	}

	public void setNrFax(String nrFax) {
		this.nrFax = nrFax;
	}

	public String getNrInscEstadual() {
		return nrInscEstadual;
	}

	public void setNrInscEstadual(String nrInscEstadual) {
		this.nrInscEstadual = nrInscEstadual;
	}

	public String getNrInscMunicipal() {
		return nrInscMunicipal;
	}

	public void setNrInscMunicipal(String nrInscMunicipal) {
		this.nrInscMunicipal = nrInscMunicipal;
	}

	public String getNrInscsuframa() {
		return nrInscsuframa;
	}

	public void setNrInscsuframa(String nrInscsuframa) {
		this.nrInscsuframa = nrInscsuframa;
	}

	public String getNrNumero() {
		return nrNumero;
	}

	public void setNrNumero(String nrNumero) {
		this.nrNumero = nrNumero;
	}

	public String getNrTelefone1() {
		return nrTelefone1;
	}

	public void setNrTelefone1(String nrTelefone1) {
		this.nrTelefone1 = nrTelefone1;
	}

	public String getNrTelefone2() {
		return nrTelefone2;
	}

	public void setNrTelefone2(String nrTelefone2) {
		this.nrTelefone2 = nrTelefone2;
	}

	public String getNrtelefone3() {
		return nrtelefone3;
	}

	public void setNrtelefone3(String nrtelefone3) {
		this.nrtelefone3 = nrtelefone3;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	@JsonProperty
	public String getNrCnpjFormatado() {
		if (nrCnpj != null && nrCnpj.length() == 14) {
			Pattern pattern = Pattern
			        .compile("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})");
			Matcher matcher = pattern.matcher(nrCnpj);
			if (matcher.matches())
				return matcher.replaceAll("$1.$2.$3/$4-$5");
		}
		return nrCnpj;
	}
	
	@JsonProperty
	public void setNrCnpjFormatado(String nrCnpj) {
		
	}

	public Integer getInTipoCobranca() {
		return inTipoCobranca;
	}

	public void setInTipoCobranca(Integer inTipoCobranca) {
		this.inTipoCobranca = inTipoCobranca;
	}

	public String getNmLogin() {
		return nmLogin;
	}

	public void setNmLogin(String nmLogin) {
		this.nmLogin = nmLogin;
	}
	
	public Integer getValidadeGuia() {
		return validadeGuia;
	}

	public void setValidadeGuia(Integer validadeGuia) {
		this.validadeGuia = validadeGuia;
	}
	
	
}