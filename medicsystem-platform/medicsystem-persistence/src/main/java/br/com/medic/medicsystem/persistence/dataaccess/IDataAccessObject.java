package br.com.medic.medicsystem.persistence.dataaccess;

import java.util.Collection;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.medic.medicsystem.persistence.exception.ObjectAlreadyExistsException;

/**
 * Interface to represent the DAO contract.
 * 
 */
public interface IDataAccessObject<T> {


    static final String NOT_RESULT_MESSAGE = "Nenhum resultado encontrado para consulta: ";

    CriteriaBuilder getCriteriaBuilder();
    
    /**
     * Save an entity
     * 
     * @param bean
     * @throws ObjectAlreadyExistsException
     */
    T persist(T bean);
    
    TypedQuery<T> createQuery(String query, Class<T> clazz);

    /**
     * Update an entity
     * 
     * @param bean
     */
    T update(T bean);

    /**
     * Search an entity by its key
     * 
     * @param entityId
     * @throws ObjectNotFoundException
     * @return The found object
     */

    <E> E searchByKey(Class<E> clazz, Object object)
            throws ObjectNotFoundException;

    Collection<T> findByTypedQueryWithParams(String query, Class<T> clazz,
            Object... parameters);

    /**
     * Remove an entity
     * 
     * @param bean
     */
    void remove(T bean);

    /**
     * Find an entity using an specific query
     * 
     * @param hql
     * @throws ObjectNotFoundException
     * @return
     */
    @SuppressWarnings("rawtypes")
    Collection findByQuery(String query);

    /**
     * Find an entity using an specific query and matching the specified
     * parameters
     * 
     * @param query
     * @param ordened
     *            parameters
     * @return Collection
     */
    Collection<?> findByQuery(String query, Object... parameters);

    Query findByNativeQuery(String query);

    /**
     * 
     * Find an entity using an specific query
     * 
     * @param query
     * @throws ObjectNotFoundException
     * @return
     */
    <E> Collection<E> findByNativeQuery(String query, Class<E> clazz);

    /**
     * 
     * Find an entity using an specific Named Native Query and return Query
     * Object
     * 
     * @param query
     * @throws ObjectNotFoundException
     * @return
     */
    Query findByNamedNativeQuery(String nnqName);

    <E> Query findByNamedQuery(String nnqName, Class<E> clazz);

    /**
     * Load an object by query
     * 
     * @param query
     * @throws ObjectNotFoundException
     * @return
     */
    Object loadByQuery(String query);

    /**
     * Find an entity using an specific query defining a range for the result
     * 
     * @param hql
     * @throws ObjectNotFoundException
     * @return The result list
     */
    @SuppressWarnings("rawtypes")
    Collection findByQuery(String query, Map<String, ? extends Object> map,
            Integer firstResult, Integer maxResults);

    /**
     * Load an object by query
     * 
     * @param query
     * @param parameters
     * @return Object
     */
    Object loadByQuery(String query, Object... parameters);

    <R> R getObject(Class<T> clazz, Class<R> returnType, String field,
            String order);

    Collection<T> findByTypedQuery(String query, Class<T> clazz);

    TypedQuery<?> createCriteriaQuery(CriteriaQuery<?> criteriaQuery);

}
