package br.com.medic.medicsystem.persistence.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ObjectNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 6766301853140626005L;

	public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException(String message, RuntimeException rte) {
		super(message, rte);
	}
}
