package br.com.medic.medicsystem.persistence.model.views;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(catalog = "realvida", name = "atendimentomedico_despesaorcamentoexame")
public class AtendimentomedicoDespesaOrcamentoExameView {
	
	@Id
	@Column(name = "idorcamentoexame")
	private Long idOrcamentoExame;
	
	@Basic
	@Column(name = "intipopaciente")
	private String inTipoPaciente;
	
	@Basic
	@Column(name = "idpaciente")
	private Long idPaciente;
	
	@Basic
	@Column(name = "idespecialidade")
	private Long idEspecialidade;
	
	@Basic
	@Column(name = "nmespecialidade")
	private String nmEspecialidade;
	
	@Basic
	@Column(name = "nmfuncionario")
	private String nmFuncionario;
	
	@Basic
	@Column(name = "dtatendimento")
	private Date dtAtendimento;
	

	public Long getIdOrcamentoExame() {
		return idOrcamentoExame;
	}


	public void setIdOrcamentoExame(Long idOrcamentoExame) {
		this.idOrcamentoExame = idOrcamentoExame;
	}





	public String getInTipoPaciente() {
		return inTipoPaciente;
	}


	public void setInTipoPaciente(String inTipoPaciente) {
		this.inTipoPaciente = inTipoPaciente;
	}


	public Long getIdPaciente() {
		return idPaciente;
	}


	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}


	public Long getIdEspecialidade() {
		return idEspecialidade;
	}


	public void setIdEspecialidade(Long idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}


	public String getNmEspecialidade() {
		return nmEspecialidade;
	}


	public void setNmEspecialidade(String nmEspecialidade) {
		this.nmEspecialidade = nmEspecialidade;
	}


	public Date getDtAtendimento() {
		return dtAtendimento;
	}


	public void setDtAtendimento(Date dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}


	public String getNmFuncionario() {
		return nmFuncionario;
	}


	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	
		
	

}
