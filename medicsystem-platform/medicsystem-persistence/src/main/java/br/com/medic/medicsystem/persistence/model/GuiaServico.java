package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "realvida", name = "tbguiaservico")
public class GuiaServico implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUIASERVICO_ID_SEQ")
	@SequenceGenerator(name = "GUIASERVICO_ID_SEQ", sequenceName = "realvida.guiaservico_id_seq", allocationSize = 1)
	@Column(name = "idguiaservico")
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idguia")
	private Guia guia;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idservico")
	private Servico servico;

	/**
	 * 0 - Criado - Em aberto, 1 - Agendado, 2 - Consumido, 3 - Quitado, 4 -
	 * Inativo, 5 - Vencido
	 */
	@Basic
	@Column(name = "status")
	private Integer status;

	@Basic
	@Column(name = "dtutilizacao")
	private Timestamp dtUtilizacao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
	@Basic
	@Column(name = "dtexclusao")
	private Timestamp dtExclusao;
	
	@Transient
	private String statusFormatado;

	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao(Timestamp dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Guia getGuia() {
		return guia;
	}

	public void setGuia(Guia guia) {
		this.guia = guia;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	@SuppressWarnings("deprecation")
	public Integer getStatus() {
		if (0 == this.status) {
			Date todayZero = new Date();
			todayZero.setHours(0);
			todayZero.setMinutes(0);
			todayZero.setSeconds(0);
			if (todayZero.getYear() > this.guia.getDtValidade().getYear()
					|| (todayZero.getYear() == this.guia.getDtValidade().getYear()
							&& todayZero.getMonth() > this.guia.getDtValidade().getMonth())
					|| (todayZero.getYear() == this.guia.getDtValidade().getYear()
							&& todayZero.getMonth() == this.guia.getDtValidade().getMonth()
							&& todayZero.getDate() > this.guia.getDtValidade().getDate())) {
				return 5;
			}
		}
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getDtUtilizacao() {
		return dtUtilizacao;
	}

	public void setDtUtilizacao(Timestamp dtUtilizacao) {
		this.dtUtilizacao = dtUtilizacao;
	}

	public String getStatusFormatado() {
		
		if(status != null){
			switch (status) {
			case 0:
				return StatusGuiaServicoEnum.CRIADO.getDescription();
			case 1:
				return StatusGuiaServicoEnum.AGENDADO.getDescription();
			case 2:
				return StatusGuiaServicoEnum.CONSUMIDO.getDescription();
			case 4:
				return StatusGuiaServicoEnum.INATIVO.getDescription();
			case 5:
				return StatusGuiaServicoEnum.VENCIDO.getDescription();
			default:
				return null;
			}			
		}
		
		return null;		
	}

	public void setStatusFormatado(String statusFormatado) {
		this.statusFormatado = statusFormatado;
	}

}
