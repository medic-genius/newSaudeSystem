package br.com.medic.medicsystem.persistence.security.dto;

import java.util.List;

public class QueryObject {

    private List<String> names;

    private Object[] values;

    public QueryObject() {
        super();
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }
}
