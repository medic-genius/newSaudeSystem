package br.com.medic.medicsystem.persistence.dto;

public class InformativoDTO {

	private String nmInformativo;
	private String nrCelular;
	private String nrTelefone;
	private Long idCliente;
	private Long idAgendamento;
	
	public String getNmInformativo() {
		if(nmInformativo != null){
			nmInformativo = nmInformativo.replaceAll("\n", " ");
			return nmInformativo;
		}
		return nmInformativo;
	}
	public void setNmInformativo(String nmInformativo) {
		this.nmInformativo = nmInformativo;
	}
	public String getNrCelular() {
		return nrCelular;
	}
	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}
	public String getNrTelefone() {
		return nrTelefone;
	}
	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdAgendamento() {
		return idAgendamento;
	}
	public void setIdAgendamento(Long idAgendamento) {
		this.idAgendamento = idAgendamento;
	}
}
