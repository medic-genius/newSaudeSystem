package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Entidade que representa os funcionarios
 * de uma folha de pagamento.
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.4
 * 
 *
 */
@Entity
@Table(catalog = "realvida", name = "tbimportafolhafunc")
public class ImportaFolhaFunc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORTAFOLHAFUNC_ID_SEQ")
	@SequenceGenerator(name = "IMPORTAFOLHAFUNC_ID_SEQ", catalog = "realvida", sequenceName = "IMPORTAFOLHAFUNC_ID_SEQ", allocationSize = 1)
	@Column(name = "idfunc")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date admissao;

	private double basefgts;

	private double baseirrf;

	private String cargo;

	private String codigo;

	private double contrinss;

	private double fgts;

	private double insssegurado;

	private double liquido;

	private String nome;

	private Integer nrodep;

	private double salario;

	// 0-pendente, 1-aprovado, 2-rejeitado
	private Integer status;

	private String comentario;

	// bi-directional many-to-one association to ImportaFolha
	@ManyToOne
	@JoinColumn(name = "idfolha")
	private ImportaFolha tbimportafolha;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)   
	@JoinColumn(name="idfunc")
	private List<ImportaFolhaFuncFluxo> fluxos;

	private Double vlmesanterior;
	
	private Double vlmesanteriorBase;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idoperadoralteracao")
	private Long idOperadorAlteracao;

	@Basic
	@Column(name = "idoperadorcadastro")
	private Long idOperadOrcadastro;

	@Basic
	@Column(name = "idoperadorexclusao")
	private Long idOperadorExclusao;
	
	@ManyToOne
	@JoinColumn(name = "idempresafinanceiro")
	private EmpresaFinanceiro empresafinanceiro;
	
	private Double vlAcrescimoSalario;
	
	//valor acrescido a folha do funcionario
	private Double vlAcrescimoSalarioMesAnterior;

	public ImportaFolhaFunc() {
		fluxos = new ArrayList<>();
	}

	public Date getAdmissao() {
		return this.admissao;
	}

	public void setAdmissao(Date admissao) {
		this.admissao = admissao;
	}

	public double getBasefgts() {
		return this.basefgts;
	}

	public void setBasefgts(double basefgts) {
		this.basefgts = basefgts;
	}

	public double getBaseirrf() {
		return this.baseirrf;
	}

	public void setBaseirrf(double baseirrf) {
		this.baseirrf = baseirrf;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getContrinss() {
		return this.contrinss;
	}

	public void setContrinss(double contrinss) {
		this.contrinss = contrinss;
	}

	public double getFgts() {
		return this.fgts;
	}

	public void setFgts(double fgts) {
		this.fgts = fgts;
	}

	public double getInsssegurado() {
		return this.insssegurado;
	}

	public void setInsssegurado(double insssegurado) {
		this.insssegurado = insssegurado;
	}

	public double getLiquido() {
		return this.liquido;
	}

	public void setLiquido(double liquido) {
		this.liquido = liquido;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNrodep() {
		return this.nrodep;
	}

	public void setNrodep(Integer nrodep) {
		this.nrodep = nrodep;
	}

	public double getSalario() {
		return this.salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public ImportaFolha getTbimportafolha() {
		return this.tbimportafolha;
	}

	public void setTbimportafolha(ImportaFolha tbimportafolha) {
		this.tbimportafolha = tbimportafolha;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ImportaFolhaFuncFluxo> getFluxos() {
		return fluxos;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public void setFluxos(List<ImportaFolhaFuncFluxo> fluxos) {
		this.fluxos = fluxos;
	}

	public Double getVlmesanterior() {
		return vlmesanterior;
	}

	public void setVlmesanterior(Double vlmesanterior) {
		this.vlmesanterior = vlmesanterior;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}

	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}

	public Long getIdOperadOrcadastro() {
		return idOperadOrcadastro;
	}

	public void setIdOperadOrcadastro(Long idOperadOrcadastro) {
		this.idOperadOrcadastro = idOperadOrcadastro;
	}

	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}

	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}
	
	public Double getVlmesanteriorBase() {
		return vlmesanteriorBase;
	}
	
	public void setVlmesanteriorBase(Double vlmesanteriorBase) {
		this.vlmesanteriorBase = vlmesanteriorBase;
	}
	
	public EmpresaFinanceiro getEmpresafinanceiro() {
		return empresafinanceiro;
	}
	
	public void setEmpresafinanceiro(EmpresaFinanceiro empresafinanceiro) {
		this.empresafinanceiro = empresafinanceiro;
	}
	
	public Double getVlAcrescimoSalario() {
		return vlAcrescimoSalario;
	}
	
	public void setVlAcrescimoSalario(Double vlAcrescimoSalario) {
		this.vlAcrescimoSalario = vlAcrescimoSalario;
	}
	
	public Double getVlAcrescimoSalarioMesAnterior() {
		return vlAcrescimoSalarioMesAnterior;
	}
	
	public void setVlAcrescimoSalarioMesAnterior(Double vlAcrescimoSalarioMesAnterior) {
		this.vlAcrescimoSalarioMesAnterior = vlAcrescimoSalarioMesAnterior;
	}

}