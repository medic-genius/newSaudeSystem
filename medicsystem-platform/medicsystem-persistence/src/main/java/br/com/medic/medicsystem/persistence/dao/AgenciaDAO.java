package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agencia;

@Named("agencia-dao")
@ApplicationScoped
public class AgenciaDAO extends RelationalDataAccessObject<Agencia> {

	public List<Agencia> getAgencias(Long idBanco) {

		TypedQuery<Agencia> query = entityManager.createQuery(
		        "SELECT c FROM Agencia c where c.banco.id = " + idBanco
		                + " order by c.nmAgencia asc", Agencia.class);
		List<Agencia> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Agencia getAgenciabyCod( String nrAgencia, Long idBanco) {
		
		Integer codigoAgencia = Integer.parseInt(nrAgencia);
		
		String queryStr = "SELECT a FROM Agencia a WHERE a.nrAgencia like '%" + codigoAgencia + "%'"
				+ " AND a.banco.id = " + idBanco
				+ " order by a.nmAgencia asc"; 
		
		TypedQuery<Agencia> query = entityManager.createQuery(queryStr , Agencia.class);
						
		try {
			Agencia result = query.getSingleResult();
			
			if(result != null)
				return result;
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
				
		return null;
	}

	public List<Agencia> getAllAgencias() {
		TypedQuery<Agencia> query = entityManager.createQuery(
		        "SELECT ag FROM Agencia ag order by ag.nrAgencia, ag.nmAgencia asc", Agencia.class);
		List<Agencia> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<Agencia> getAgenciaByBanco(Long idBanco) {

		TypedQuery<Agencia> query = entityManager.createQuery("SELECT ag FROM Agencia ag WHERE ag.banco.id = "+ idBanco
				+ " ORDER BY ag.nrAgencia, ag.nmAgencia asc", Agencia.class);
		
		List<Agencia> result = query.getResultList();
		if(result.isEmpty()){
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}
	
	public Agencia getAgenciaByID(Long idAgencia){
		return searchByKey(Agencia.class, idAgencia);
	}
}
