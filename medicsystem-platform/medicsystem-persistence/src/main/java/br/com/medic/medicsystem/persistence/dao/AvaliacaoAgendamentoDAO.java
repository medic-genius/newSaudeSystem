package br.com.medic.medicsystem.persistence.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.AvaliacaoAgendamento;

@Named("avaliacao-agendamento-dao")
@ApplicationScoped
public class AvaliacaoAgendamentoDAO extends RelationalDataAccessObject<AvaliacaoAgendamento> {
	
}
