package br.com.medic.medicsystem.persistence.dao;


import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;


/**
 * 
 * @author Joelton
 * 
 * @since 28/03/2016
 *
 */

@Named("empresafinanceira-dao")
@ApplicationScoped
public class EmpresaFinanceiroDAO extends RelationalDataAccessObject<EmpresaFinanceiro>  {

	public List<EmpresaFinanceiro> getEmpresasFinanceiroAtivas() {
		
		String queryInitial = "SELECT ef FROM EmpresaFinanceiro ef";
		TypedQuery<EmpresaFinanceiro> query = entityManager.createQuery(queryInitial , EmpresaFinanceiro.class);

		List<EmpresaFinanceiro> result = query.getResultList();
		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		
		return result;
	}

	public List<EmpresaFinanceiro> getEmpresasFinanceiro(boolean isEmpresaOficial) {
		
		String queryStr = "SELECT ef FROM EmpresaFinanceiro ef ";
		
		if( isEmpresaOficial  )
			queryStr += "where ef.isEmpresaOficial is true "; 
		else
			queryStr += "where ef.isEmpresaOficial is false OR ef.isEmpresaOficial is null";
				
		queryStr +=  " ORDER BY ef.inTipoGrupo, ef.idEmpresaFinanceiro";
				
		
		TypedQuery<EmpresaFinanceiro> query = entityManager.createQuery(
		        queryStr , EmpresaFinanceiro.class);

		List<EmpresaFinanceiro> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public EmpresaFinanceiro getEmpresaById(Long idEmpresa){
		return searchByKey(EmpresaFinanceiro.class, idEmpresa);
	}

}
