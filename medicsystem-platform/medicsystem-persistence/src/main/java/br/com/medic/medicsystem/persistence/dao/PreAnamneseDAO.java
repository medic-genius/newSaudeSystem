package br.com.medic.medicsystem.persistence.dao;


import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.PreAnamnese;

@Named("preanamnese-dao")
@ApplicationScoped
public class PreAnamneseDAO extends RelationalDataAccessObject<PreAnamnese>{
	
	public List<PreAnamnese> getPreAnamnese(){
		
		TypedQuery<PreAnamnese> query = entityManager.createQuery(
		        "SELECT pa FROM PreAnamnese pa", PreAnamnese.class);
		List<PreAnamnese> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
		
	}

}
