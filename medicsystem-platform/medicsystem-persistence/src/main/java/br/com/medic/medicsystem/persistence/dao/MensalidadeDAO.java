package br.com.medic.medicsystem.persistence.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.MensalidadeDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.BoletoVencido;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Parcela;

@Named("mensalidade-dao")
@ApplicationScoped
public class MensalidadeDAO extends RelationalDataAccessObject<Mensalidade> {

	@SuppressWarnings("unchecked")
	public ArrayList<Object[]> obterBoletosAVencer(Date dataVencimento) {

		Query q = findByNamedNativeQuery("mqMensalidadeAVencer");
		q.setParameter("dtvenc", dataVencimento);

		ArrayList<Object[]> result = (ArrayList<Object[]>) q.getResultList();

		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<BoletoVencido> getBoletosRecorrenteVencidos(Integer inFormaPagamento) {
		

		String query = " select * from realvida.viewmensalidadevencidasms_boleto_recorrente "
					+ " where informapagamento = :inFormaPagamento";

		return findByNativeQuery(query)
				.setParameter("inFormaPagamento", inFormaPagamento)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .addScalar("vlmensalidade", DoubleType.INSTANCE)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrtelefone", StringType.INSTANCE)
		        .addScalar("nrcelular", StringType.INSTANCE)
		        .addScalar("idCliente", LongType.INSTANCE)
		        .addScalar("idContrato", LongType.INSTANCE)
		        .addScalar("idMensalidade", LongType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BoletoVencido.class))
		        .list();

	}
	
	
	@SuppressWarnings("unchecked")
	public List<BoletoVencido> getBoletosVencidosCCredito() {

		String query = "select  distinct on (cli.nmcliente,cli.nrcelular) "
				+ "m.dtvencimento, m.vlmensalidade, cli.idcliente, cli.nmcliente, cli.nrtelefone, cli.nrcelular "
				+ "from realvida.tbmensalidade m "
				+ "left join realvida.tbcontrato c on m.idcontrato = c.idcontrato "
				+ "left join realvida.tbcontratocliente concli on c.idcontrato = concli.idcontrato "
				+ "left join realvida.tbcliente cli on cli.idcliente = concli.idcliente "
				+ "where m.informapagamento = 4 "
				+ "and m.dtexclusao is null "
				+ "and c.idempresagrupo = 9 "
				+ "and (m.bonegociada is null or m.bonegociada = false) "
				+ "and m.dtvencimento BETWEEN date_trunc('day', current_date) - INTERVAL '16 day' AND date_trunc('day', current_date) - INTERVAL '10 day' "
				+ "and m.dtpagamento is null "
				+ "and c.insituacao = 0 "
				+" and (cli.nrtelefone is  not null or cli.nrcelular is not null) "
				+ "and m.instatus = 0";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .addScalar("vlmensalidade", DoubleType.INSTANCE)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrtelefone", StringType.INSTANCE)
		        .addScalar("nrcelular", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BoletoVencido.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<BoletoVencido> getBoletosVencidosCDebito() {

		String query = "select distinct on (cli.nmcliente,cli.nrcelular) "
				+ "m.dtvencimento, m.vlmensalidade, cli.idcliente, cli.nmcliente, cli.nrtelefone, cli.nrcelular "
				+ "from realvida.tbmensalidade m "
				+ "left join realvida.tbcontrato c on m.idcontrato = c.idcontrato "
				+ "left join realvida.tbcontratocliente concli on c.idcontrato = concli.idcontrato "
				+ "left join realvida.tbcliente cli on cli.idcliente = concli.idcliente "
				+ "where m.informapagamento = 9 "
				+ "and m.dtexclusao is null "
				+ "and c.idempresagrupo = 9 "
				+ "and (m.bonegociada is null or m.bonegociada = false) "
				+ "and m.dtvencimento BETWEEN date_trunc('day', current_date) - INTERVAL '16 day' AND date_trunc('day', current_date) - INTERVAL '10 day' "
				+ "and m.dtpagamento is null "
				+ "and c.insituacao = 0 "
				+" and (cli.nrtelefone is  not null or cli.nrcelular is not null) "
				+ "and m.instatus = 0";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .addScalar("vlmensalidade", DoubleType.INSTANCE)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrtelefone", StringType.INSTANCE)
		        .addScalar("nrcelular", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BoletoVencido.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<BoletoVencido> getBoletosVencidosCRecorrente() {

		String query = "select distinct on (cli.nmcliente,cli.nrcelular) "
				+ "m.dtvencimento, m.vlmensalidade, cli.idcliente, cli.nmcliente, cli.nrtelefone, cli.nrcelular "
				+ "from realvida.tbmensalidade m "
				+ "left join realvida.tbcontrato c on m.idcontrato = c.idcontrato "
				+ "left join realvida.tbcontratocliente concli on c.idcontrato = concli.idcontrato "
				+ "left join realvida.tbcliente cli on cli.idcliente = concli.idcliente "
				+ "where m.informapagamento = 30 "
				+ "and m.dtexclusao is null "
				+ "and c.idempresagrupo = 9 "
				+ "and (m.bonegociada is null or m.bonegociada = false) "
				+ "and m.dtvencimento BETWEEN date_trunc('day', current_date) - INTERVAL '10 day' AND date_trunc('day', current_date) - INTERVAL '4 day' "
				+ "and m.dtpagamento is null "
				+ "and c.insituacao = 0 "
				+" and (cli.nrtelefone is  not null or cli.nrcelular is not null) "
				+ "and m.instatus = 0";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .addScalar("vlmensalidade", DoubleType.INSTANCE)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrtelefone", StringType.INSTANCE)
		        .addScalar("nrcelular", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BoletoVencido.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<BoletoVencido> getMensalidadesDaCCheckVencidas(String dtInicio, Integer inFormaPagamento) {


		String query = " select distinct on (cli.nmcliente,cli.nrcelular) "
				+" m.dtvencimento, m.vlmensalidade, cli.idcliente, cli.nmcliente, cli.nrtelefone, cli.nrcelular, m.informapagamento "
				+"  from realvida.tbmensalidade m  "
				+"  join realvida.tbcontrato c on m.idcontrato = c.idcontrato " 
				+"  join realvida.tbcontratocliente concli on c.idcontrato = concli.idcontrato  "
				+"  join realvida.tbcliente cli on cli.idcliente = concli.idcliente " 
				+"  where m.dtexclusao is null " 
				+"  and c.idempresagrupo = 9  "
				+"  and (m.bonegociada is null or m.bonegociada = false)  "
				+"  and m.dtvencimento between '"+dtInicio+"' and (date_trunc('month',cast('"+dtInicio+"' as date)) + interval '1 month')- interval '1 day' "
				+"  and m.dtpagamento is null  "
				+"  and c.insituacao = 0  "
				+"  and((cli.nrtelefone IS NOT NULL and "
					+" length(cast(cli.nrtelefone as text)) > 7) or "
					+" (cli.nrcelular IS NOT NULL and "
					+" length(cast(cli.nrcelular as text)) > 7)) "
				 +" and m.instatus = 0 "
				+" and m.informapagamento = :inFormaPagamento";

		return findByNativeQuery(query)
				.setParameter("inFormaPagamento", inFormaPagamento)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtvencimento", DateType.INSTANCE)
		        .addScalar("vlmensalidade", DoubleType.INSTANCE)
		        .addScalar("nmcliente", StringType.INSTANCE)
		        .addScalar("nrtelefone", StringType.INSTANCE)
		        .addScalar("nrcelular", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(BoletoVencido.class))
		        .list();

}



	@SuppressWarnings("unchecked")
	public List<Mensalidade> listMensalidadeByClienteByContrato(Long idCliente,
			Long idContrato, Long[] idMensalidades) {
		
		String where = " WHERE" + " CLI.IDCLIENTE = " + idCliente;
		
		if(idMensalidades !=null 
				&& idMensalidades.length > 0){
			
			if(idMensalidades.length > 1){
				where += " AND MEN.IDMENSALIDADE IN ("; 
				
				for (int i = 0; i < idMensalidades.length; i++) {
					
					if(i == 0){
						
						where +=  idMensalidades[i] + ",";
					} else {
						
						if(i == (idMensalidades.length - 1)){
							
							where +=  idMensalidades[i] + ")";
						} else {
							
							where +=  idMensalidades[i] + ",";
						}
					}
				}
			} else {
				
				where += " AND MEN.IDMENSALIDADE = " + idMensalidades[0];
			}
		}
		
		where += " AND CON.IDCONTRATO =" + idContrato 
				+ " AND CON.INFORMAPAGAMENTO = 0"
				+ " AND CLI.DTEXCLUSAO IS NULL " + "AND CON.INSITUACAO = 0"
				+ " AND CON.INRENOVADO = 1"
				+ " AND MEN.DTVENCIMENTO >= CURRENT_DATE"
				+ " AND MEN.DTEXCLUSAO IS NULL"
				+ " AND BOL.IDBOLETOBANCARIO IS NULL"
				+ " ORDER BY MEN.NRMENSALIDADE";
		
		String query = "SELECT  MEN.*"
				+ " FROM REALVIDA.TBMENSALIDADE MEN"
				+ "	INNER JOIN REALVIDA.TBCONTRATO CON ON CON.IDCONTRATO = MEN.IDCONTRATO"
				+ "	INNER JOIN REALVIDA.TBPLANO PLA ON PLA.IDPLANO = CON.IDPLANO"
				+ "	INNER JOIN REALVIDA.TBCONTRATOCLIENTE CONT ON CONT.IDCONTRATO =  CON.IDCONTRATO"
				+ "	INNER JOIN REALVIDA.TBCLIENTE CLI ON CLI.IDCLIENTE = CONT.IDCLIENTE"
				+ "	LEFT JOIN REALVIDA.TBBOLETOBANCARIO BOL ON BOL.IDCHAVEBOLETO = MEN.IDMENSALIDADE"
				+ where;

		return findByNativeQuery(query).unwrap(SQLQuery.class).addEntity(Mensalidade.class).list();
		
	}
	
	public Collection<Mensalidade> getMensalidadeByCliente(Long idCliente, Integer dias) {
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentesrecente_getmensalidades("+dias+"," +idCliente+")";
		try {
			
			Collection<Mensalidade> result = findByNativeQuery(queryStr,
					Mensalidade.class);

			if (result.isEmpty()) {
				return null;
			}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<MensalidadeDTO> getMensalidadeByClienteInadimplente(Long idCliente, Integer dias) {
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentesrecente_getmensalidades("+dias+"," +idCliente+")";		
			
		List<MensalidadeDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("idMensalidade", LongType.INSTANCE)
				.addScalar("nrMensalidade", StringType.INSTANCE)
				.addScalar("dtVencimento", DateType.INSTANCE)
				.addScalar("vlMensalidade", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(MensalidadeDTO.class))
				.list();
		
		
		return result;
		
	}
	
	
	public Collection<Mensalidade> getMensalidadeByClientePorPeriodo(Long idCliente, String dtInicio, String dtFim) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");	
		String dtinicio = "";
		String dtfim = "";
		
		try {
			Date date1 = formatter.parse(dtInicio);
			Date date2 = formatter.parse(dtFim);
            
			dtinicio = formatter2.format(date1);
			dtfim = formatter2.format(date2);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentes_getmensalidades('"+dtinicio+"', '"+dtfim+"',"+idCliente+")";		
			
		try {
			
			Collection<Mensalidade> result = findByNativeQuery(queryStr,
					Mensalidade.class);

			if (result.isEmpty())
				return null;			
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<MensalidadeDTO> getMensalidadeByClienteInadimplentePorPeriodo(Long idCliente, String dtInicio, String dtFim) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");		
		String dtinicio = "";
		String dtfim = "";
		
		try {
			Date date1 = formatter.parse(dtInicio);
			Date date2 = formatter.parse(dtFim);
            
			dtinicio = formatter2.format(date1);
			dtfim = formatter2.format(date2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String queryStr = "select * from compartilhado.cobraca_clientesinadimplentes_getmensalidades('"+dtinicio+"', '"+dtfim+"',"+idCliente+")";
			
		List<MensalidadeDTO> result = findByNativeQuery(queryStr)
				.unwrap(SQLQuery.class)
				.addScalar("idMensalidade", LongType.INSTANCE)
				.addScalar("nrMensalidade", StringType.INSTANCE)
				.addScalar("dtVencimento", DateType.INSTANCE)
				.addScalar("vlMensalidade", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(MensalidadeDTO.class))
				.list();
		
		
		return result;
		
	}
	
	
		
	public Mensalidade updateMensalidade(Mensalidade mensalidade){
			
		return entityManager.merge(mensalidade);
	}
			
	public Mensalidade getMensalidadeByIdCobrancaSL(Long idCobrancaSL) {
		
		Mensalidade result;
		
		String queryStr = "From Mensalidade Where dataExclusao is null and idCobrancaBoleto_sl = " + idCobrancaSL;		
			
		TypedQuery<Mensalidade> query = entityManager.createQuery(queryStr, Mensalidade.class);
		query.setMaxResults(1);
		
		try {

			result = query.getSingleResult();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return null;
		}
						
	}
	
	public Collection<Parcela> getCobrancaByMensalidade(Long idMensalidade) {
		
		String querystr = "select parc.* from realvida.tbnegociacao neg "
				+"join realvida.tbdespesa desp on neg.iddespesa = desp.iddespesa "
				+"join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa "
				+"where neg.idmensalidade = "+ idMensalidade;
		try {
			
			Collection<Parcela> result = findByNativeQuery(querystr, Parcela.class);

			if (result.isEmpty()) {
				return null;
			}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public List<Mensalidade> getMensalidadeByContrato(Long idContrato){
		
		String queryStr = "From Mensalidade Where dataExclusao is null and contrato.id = " + idContrato;
		
		TypedQuery<Mensalidade> query = entityManager.createQuery(queryStr, Mensalidade.class);
		
		try {

			List<Mensalidade> result = query.getResultList();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return null;
		}
		
	}
	
	public List<Mensalidade> getMensalidadeByContratoNrMensalidade(Long idContrato, String nrMensalidade){
		
		String queryStr = "From Mensalidade "
				+ " where dataExclusao is null "
				+ " and contrato.id = " + idContrato
				+ " and nrMensalidade = '" + nrMensalidade +"'";
		
		TypedQuery<Mensalidade> query = entityManager.createQuery(queryStr, Mensalidade.class);
		
		try {

			List<Mensalidade> result = query.getResultList();

			if (result == null) {

				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {

				return result;
			}

		} catch (NoResultException nre) {

			return null;
		}
		
	}
	
	public List<Mensalidade> getMensalidadesBoletoForContratoRenovado(Long idContrato){
		String queryStr = "SELECT m FROM Mensalidade m WHERE m.inStatus = 0 AND m.informaPagamento = 5 AND m.dataExclusao IS NULL "
				+ "AND m.contrato.id = :idContrato "
				+ "AND m.contrato.dtRenovacao IS NOT NULL AND m.dataVencimento >= m.contrato.dtRenovacao";
		
		TypedQuery<Mensalidade> query = entityManager.createQuery(queryStr, Mensalidade.class)
				.setParameter("idContrato", idContrato);
		try {
			List<Mensalidade> result = query.getResultList();
			if (result == null) {
				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			} else {
				return result;
			}

		} catch (NoResultException nre) {
			return null;
		}
	}
	
}
