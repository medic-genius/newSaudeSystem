package br.com.medic.medicsystem.persistence.model.enums;

public enum FrenquenciaDespesa {
	
	DIARIAMENTE(0, "Diariamente"), SEMANALMENTE(1, "Semanalmente"), MENSALMENTE(2, "Mensalmente"),
	BIMESTRALMENTE(3, "Bimestralmente"), SEMESTRALMENTE(4, "Semestralmente"), ANUALMENTE(5, "Anualmente");
	
	private Integer id;
	private String description;	
	
	private FrenquenciaDespesa(Integer id, String description){
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
