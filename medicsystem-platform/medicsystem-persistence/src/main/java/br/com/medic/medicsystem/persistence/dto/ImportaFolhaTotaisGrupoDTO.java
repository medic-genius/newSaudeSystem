package br.com.medic.medicsystem.persistence.dto;

/**
 * @author Joelton Matos
 * @since 28/05/2016
 * @version 1.0
 */
public class ImportaFolhaTotaisGrupoDTO {

	private Double salarioBaseTotal;
	private Double somaAcrescimoTotal;
	private Double somaDescontoTotal;
	private Double somaValorPagarTotal;
	private Double somaSalarioFolhaTotal;
	
	public Double getSalarioBaseTotal() {
		return salarioBaseTotal;
	}
	
	public void setSalarioBaseTotal(Double salarioBaseTotal) {
		this.salarioBaseTotal = salarioBaseTotal;
	}
	
	public Double getSomaAcrescimoTotal() {
		return somaAcrescimoTotal;
	}
	
	public void setSomaAcrescimoTotal(Double somaAcrescimoTotal) {
		this.somaAcrescimoTotal = somaAcrescimoTotal;
	}
	
	public Double getSomaDescontoTotal() {
		return somaDescontoTotal;
	}
	
	public void setSomaDescontoTotal(Double somaDescontoTotal) {
		this.somaDescontoTotal = somaDescontoTotal;
	}
	
	public Double getSomaValorPagarTotal() {
		return somaValorPagarTotal;
	}
	
	public void setSomaValorPagarTotal(Double somaValorPagarTotal) {
		this.somaValorPagarTotal = somaValorPagarTotal;
	}
	
	public Double getSomaSalarioFolhaTotal() {
		return somaSalarioFolhaTotal;
	}
	
	public void setSomaSalarioFolhaTotal(Double somaSalarioFolhaTotal) {
		this.somaSalarioFolhaTotal = somaSalarioFolhaTotal;
	}
	
}
