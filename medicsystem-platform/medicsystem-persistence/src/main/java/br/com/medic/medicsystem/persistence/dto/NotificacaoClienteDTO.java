package br.com.medic.medicsystem.persistence.dto;

public class NotificacaoClienteDTO {

	private Long idCliente;
	
	private String nmCliente;
	
	private String nrCelular;
	
	private String nrTelefone;
	
	private Long idContrato;
	
	private String linkCarne;

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCelular() {
		return nrCelular;
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getLinkCarne() {
		return linkCarne;
	}

	public void setLinkCarne(String linkCarne) {
		this.linkCarne = linkCarne;
	}
	
	
}
