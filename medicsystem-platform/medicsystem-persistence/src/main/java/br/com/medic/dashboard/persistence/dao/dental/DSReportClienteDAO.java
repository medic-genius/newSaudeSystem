package br.com.medic.dashboard.persistence.dao.dental;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import br.com.medic.dashboard.persistence.dataaccess.DSRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.MovimentacaoCliente;

@Named("ds-report-cliente-dao")
@ApplicationScoped
public class DSReportClienteDAO extends 
		DSRelationalDataAccessObject<MovimentacaoCliente> {
	
	@SuppressWarnings("unchecked")
	public List<MovimentacaoCliente> getReportClienteAtivos() {

		String query = 
				
		 "SELECT 'CLIENTES' as Clientes, count(*) as quantidade "
		 + "FROM ( SELECT cli.idcliente "
		 + "FROM realvida.tbcliente cli "
		 + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
		 + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
		 + "AND cont.insituacao = 0 "
		 + "AND cli.dtexclusao is null "
		 + "AND cont.idempresagrupo = 8 "
		 + "GROUP BY cli.idcliente) as NumeroDeClientes "

		 + "UNION ALL "

		 + "SELECT 'CLIENTES ASSOCIADOS' as Clientes, count(*) as quantidade "
		 + "FROM ( SELECT cli.idcliente "
		 + "FROM realvida.tbcliente cli "
		 + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
		 + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
		 + "AND cont.insituacao = 0 "
		 + "AND cli.dtexclusao is null "
		 + "AND cont.idempresagrupo = 8 "
		 + "AND cont.idplano is not null "
		 + "GROUP BY cli.idcliente) as NumeroDeClientes "

		 + "UNION ALL "

		 + "SELECT 'CLIENTES NÃO ASSOCIADOS' as Clientes, count(*) as quantidade "
		 + "FROM ( SELECT cli.idcliente "
		 + "FROM realvida.tbcliente cli "
		 + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
		 + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
		 + "AND cont.insituacao = 0 "
		 + "AND cli.dtexclusao is null "
		 + "AND cont.idempresagrupo = 8 "
		 + "AND cont.idplano is null "
		 + "GROUP BY cli.idcliente) as NumeroDeClientes ";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("clientes", StringType.INSTANCE)
		        .addScalar("quantidade", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(MovimentacaoCliente.class))
		        .list();

	}	
	
	@SuppressWarnings("unchecked")
	public List<MovimentacaoCliente> getReportClientesFaturamento(
	        String inicio, String fim) {

		String query = 								
				"SELECT 'SERVIÇOS PAGOS ASSOCIADOS' as clientes, count(*) as quantidade, sum(t1.totalpago) as faturamento "
				+ "FROM (SELECT cli.idcliente, sum(parc.vlpago) as totalpago "
				+ "FROM realvida.tbcliente cli "
				+ "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				+ "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				+ "inner join realvida.tbdespesa desp on desp.idcontratocliente = contcli.idcontratocliente and (desp.boexcluida = false or desp.boexcluida is null) and desp.insituacao = 1 "
				+ "inner join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa and (parc.boexcluida = false or parc.boexcluida is null) and parc.dtpagamento is not null and parc.informapagamento not in (10,14,15,16) "
				//+ "inner join realvida.tbdespesaservico despserv on despserv.iddespesa = desp.iddespesa "
				//+ "inner join realvida.tbservico serv on serv.idservico = despserv.idservico "
				+ "AND cont.insituacao in (0,1) " 
				+ "AND cli.dtexclusao is null "
				+ "AND cont.idempresagrupo = 8 "
				+ "AND cont.idplano is not null "				
				+ "AND parc.dtpagamento BETWEEN '" + inicio +"' and '" + fim + "' "
				+ "group by cli.idcliente) as t1 "
				
				+ "UNION ALL "
				
				+ "SELECT 'SERVIÇOS PAGOS NAO ASSOCIADOS' as clientes, count(*) as quantidade, sum(t1.totalpago) as faturamento "
				+ "FROM (SELECT cli.idcliente, sum(parc.vlpago) as totalpago "
				+ "FROM realvida.tbcliente cli "
				+ "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				+ "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				+ "inner join realvida.tbdespesa desp on desp.idcontratocliente = contcli.idcontratocliente and (desp.boexcluida = false or desp.boexcluida is null) and desp.insituacao = 1 "
				+ "inner join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa and (parc.boexcluida = false or parc.boexcluida is null) and parc.dtpagamento is not null and parc.informapagamento not in (10,14,15,16) "
				//+ "inner join realvida.tbdespesaservico despserv on despserv.iddespesa = desp.iddespesa "
				//+ "inner join realvida.tbservico serv on serv.idservico = despserv.idservico "
				+ "AND cont.insituacao in (0,1) " 
				+ "AND cli.dtexclusao is null "
				+ "AND cont.idempresagrupo in (8,3) "
				+ "AND cont.idplano is null "				
				+ "AND parc.dtpagamento BETWEEN '" + inicio +"' and '" + fim + "' "
				+ "group by cli.idcliente) as t1 ";
				 
		        
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("clientes", StringType.INSTANCE)
		        .addScalar("quantidade", IntegerType.INSTANCE)
		        .addScalar("faturamento", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(MovimentacaoCliente.class))
		        .list();

	}

	
	@SuppressWarnings("unchecked")
	public List<MovimentacaoCliente> getReportClientesNovosFaturamento(
	        String inicio, String fim) {

		String query = 
				
				 "SELECT 'CLIENTES NOVOS ASSOCIADOS' as clientes, count(*) as quantidade, "
				 + "(SELECT sum(t1.totalpago) as faturamento "
				 + "FROM (select cli.idcliente, sum(parc.vlpago) as totalpago "
				 + "from realvida.tbcliente cli "
				 + "inner join realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				 + "inner join realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				 + "inner join realvida.tbdespesa desp on desp.idcontratocliente = contcli.idcontratocliente and (desp.boexcluida = false or desp.boexcluida is null) and desp.insituacao = 1 "
				 + "inner join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa and (parc.boexcluida = false or parc.boexcluida is null) and parc.dtpagamento is not null and parc.informapagamento not in (10,14,15,16) "
				 //+ "inner join realvida.tbdespesaservico despserv on despserv.iddespesa = desp.iddespesa "
				 //+ "inner join realvida.tbservico serv on serv.idservico = despserv.idservico "
				 + "AND cont.insituacao = 0 "
				 + "AND cli.dtexclusao is null "
				 + "AND cont.idempresagrupo = 8 "
				 + "AND cont.idplano is not null "
				 + "AND cli.dtinclusao BETWEEN '" + inicio +"' and '" + fim +"' "
				 + "AND parc.dtpagamento BETWEEN '" + inicio +"' and '" + fim +"' "
				 + "group by cli.idcliente) as t1) as faturamento "
				 + "FROM ( SELECT cli.idcliente FROM realvida.tbcliente cli " 
				 + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				 + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				 + "AND cont.insituacao = 0 "
				 + "AND cli.dtexclusao is null "
				 + "AND cont.idempresagrupo = 8 "
				 + "AND cont.idplano is not null "
				 + "AND cli.dtinclusao BETWEEN '" + inicio +"' and '" + fim +"' ) as NumeroDeClientes "
				
				 + "UNION ALL " 
				
				 + "SELECT 'CLIENTES NOVOS NAO ASSOCIADOS' as clientes, count(*) as quantidade, "
				 + "(SELECT sum(t1.totalpago) as faturamento "
				 + "FROM (select cli.idcliente, sum(parc.vlpago) as totalpago "
				 + "from realvida.tbcliente cli "
				 + "inner join realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				 + "inner join realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				 + "inner join realvida.tbdespesa desp on desp.idcontratocliente = contcli.idcontratocliente and (desp.boexcluida = false or desp.boexcluida is null) and desp.insituacao = 1 "
				 + "inner join realvida.tbparcela parc on parc.iddespesa = desp.iddespesa and (parc.boexcluida = false or parc.boexcluida is null) and parc.dtpagamento is not null and parc.informapagamento not in (10,14,15,16) "
				 //+ "inner join realvida.tbdespesaservico despserv on despserv.iddespesa = desp.iddespesa "
				 //+ "inner join realvida.tbservico serv on serv.idservico = despserv.idservico "
				 + "AND cont.insituacao = 0 "
				 + "AND cli.dtexclusao is null "
				 + "AND cont.idempresagrupo in (8) "
				 + "AND cont.idplano is null "
				 + "AND cli.dtinclusao BETWEEN '" + inicio +"' and '" + fim +"' "
				 + "AND parc.dtpagamento BETWEEN '" + inicio +"' and '" + fim +"' "
				 + "group by cli.idcliente) as t1) as faturamento "
				 + "FROM ( SELECT cli.idcliente FROM realvida.tbcliente cli "
				 + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcliente = cli.idcliente "
				 + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = contcli.idcontrato "
				 + "AND cont.insituacao = 0 "
				 + "AND cli.dtexclusao is null "
				 + "AND cont.idempresagrupo = 8 "
				 + "AND cont.idplano is null "
				 + "AND cli.dtinclusao BETWEEN '" + inicio +"' and '" + fim +"' ) as NumeroDeClientes";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("clientes", StringType.INSTANCE)
		        .addScalar("quantidade", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(MovimentacaoCliente.class))
		        .list();

	}
	
	
}
