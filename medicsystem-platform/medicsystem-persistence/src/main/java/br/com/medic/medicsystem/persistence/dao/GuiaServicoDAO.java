package br.com.medic.medicsystem.persistence.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.dto.GuiaServicoDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryGuiaServicoByServicoDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryServicoByStatusDTO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.Plano;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Named("guiaServico-dao")
@ApplicationScoped
@SuppressWarnings("unchecked")
public class GuiaServicoDAO extends RelationalDataAccessObject<GuiaServico> {

	public GenericPaginateDTO getGuiaServicosDTO(GenericPaginateDTO paginator, Long idEmpresaCliente, Long id,
			Long idGuia, String startDate, String endDate, List<Integer> lStatus, Long idServico) {

		try {
			GuiaServicoDTO guiaServicoDTO = new GuiaServicoDTO();
			guiaServicoDTO.setPaginator(paginator);
			
			guiaServicoDTO.addSqlWhereConditionAnd("guia_servico.dtexclusao is null");
			
			if(null != idEmpresaCliente)
				guiaServicoDTO.addSqlWhereConditionAnd("empresacliente.idempresacliente = :idEmpresaCliente");
			
			Date startDateObj = null;
			Date endDateObj = null;
			
			if (null != startDate && null != endDate) {
				
				String whereOr = "";				
				if(lStatus != null && lStatus.size() > 0){					
					for(Integer st : lStatus){
						if(st.equals(StatusGuiaServicoEnum.CRIADO.getId()))
							whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")" :
								" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")";
						
						if(st.equals(StatusGuiaServicoEnum.CONSUMIDO.getId()))
							whereOr += whereOr.length() == 0 ? "(" + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")" :
								" or " + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")";
												
						if(st.equals(StatusGuiaServicoEnum.INATIVO.getId()))
							whereOr += whereOr.length() == 0 ? "(" + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")" :
								" or " + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")";
						
						if(st.equals(StatusGuiaServicoEnum.VENCIDO.getId()))
							whereOr += whereOr.length() == 0 ? "(" + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")" :
								" or " + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")";
						
						if(st.equals(StatusGuiaServicoEnum.AGENDADO.getId()))
							whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")" :
										" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")";

					}
					
					startDateObj = DateUtil.parseDate(startDate, "yyyy-MM-dd");
					endDateObj = DateUtil.parseDate(endDate, "yyyy-MM-dd");
					
				}
				whereOr += whereOr.length() > 0 ? " )": "";
				if(whereOr.length() > 0)
					guiaServicoDTO.addSqlWhereConditionAnd(whereOr);
			}
			
			if (null != idServico) {
				guiaServicoDTO.addSqlWhereConditionAnd("servico.idservico = :idServico");
			}
			
//					((lStatus.get(0) == StatusGuiaServicoEnum.CRIADO.getId() && lStatus.get(1) == StatusGuiaServicoEnum.CONSUMIDO.getId())
//					|| (lStatus.get(0) == StatusGuiaServicoEnum.CONSUMIDO.getId() && lStatus.get(1) == StatusGuiaServicoEnum.CRIADO.getId()))) {
//				guiaServicoDTO.addSqlWhereConditionAnd("(guia_servico.dtutilizacao between :startDate and :endDate) or"
//						+ " (guia.dtinicio between :startDate and :endDate)");
				
						
			
//			if (0 < lStatus.size()) {
//				this.markExpired();
//				String clauseStatus = null;
//				clauseStatus = "(guia_servico.status in (";
//				for (int i = 0; i < lStatus.size(); i++) {
//					String separator = (0 == i) ? "" : ",";
//					clauseStatus += separator + lStatus.get(i);
//				}
//				clauseStatus += "))";
//				guiaServicoDTO.addSqlWhereConditionAnd(clauseStatus);
//			}
			
						

			Query queryCount = getEntityManager().createNativeQuery(guiaServicoDTO.getSqlCount());
			if(null != idEmpresaCliente) {
				queryCount.setParameter("idEmpresaCliente", idEmpresaCliente);
			}
			
			Query query = getEntityManager().createNativeQuery(guiaServicoDTO.getSqlSelect());
			if(null != idEmpresaCliente) {
				query.setParameter("idEmpresaCliente", idEmpresaCliente);
			}			

			if (null != startDate && startDateObj != null) {				
				query.setParameter("startDate", startDateObj, TemporalType.DATE);
				queryCount.setParameter("startDate", startDateObj, TemporalType.DATE);
			}
			if (null != endDate && endDateObj != null) {				
				query.setParameter("endDate", endDateObj, TemporalType.DATE);
				queryCount.setParameter("endDate", endDateObj, TemporalType.DATE);
			}
			
			if (null != idServico) {
				query.setParameter("idServico", idServico);
				queryCount.setParameter("idServico", idServico);
			}

			List<BigInteger> resultCount = queryCount.getResultList();
			BigInteger totalIntems = resultCount.get(0);
			paginator.setTotal(totalIntems.longValue());

			List<Object[]> results = query.getResultList();
			List<GuiaServicoDTO> lGuiaServicoDTO = new ArrayList<GuiaServicoDTO>();

			results.stream().forEach((record) -> {
				int i = lGuiaServicoDTO.size();
				lGuiaServicoDTO.add(new GuiaServicoDTO());
				lGuiaServicoDTO.get(i).setId(((BigInteger) record[0]).longValue());
				lGuiaServicoDTO.get(i).setGuia(new Guia());
				lGuiaServicoDTO.get(i).getGuia().setId(((BigInteger) record[1]).longValue());
				lGuiaServicoDTO.get(i).getGuia().setDtValidade((Date) record[7]);
				lGuiaServicoDTO.get(i).getGuia().setDtExclusao((Timestamp) record[8]);
				lGuiaServicoDTO.get(i).getGuia().setDtInicio((Date) record[9]);
				lGuiaServicoDTO.get(i).getGuia().setCliente(new Cliente());
				lGuiaServicoDTO.get(i).getGuia().getCliente().setId(((BigInteger) record[10]).longValue());
				lGuiaServicoDTO.get(i).getGuia().getCliente().setNmCliente((String) record[12]);
				if (null != record[11]) {
					lGuiaServicoDTO.get(i).getGuia().setDependente(new Dependente());
					lGuiaServicoDTO.get(i).getGuia().getDependente().setId(((BigInteger) record[11]).longValue());
					lGuiaServicoDTO.get(i).getGuia().getDependente().setNmDependente((String) record[13]);
				}
				lGuiaServicoDTO.get(i).getGuia().setContrato(new Contrato());
				lGuiaServicoDTO.get(i).getGuia().getContrato().setId(((BigInteger) record[14]).longValue());
				lGuiaServicoDTO.get(i).getGuia().getContrato().setNrContrato((String) record[15]);
				lGuiaServicoDTO.get(i).getGuia().getContrato().setPlano(new Plano());
				lGuiaServicoDTO.get(i).getGuia().getContrato().getPlano().setId(((BigInteger) record[16]).longValue());
				lGuiaServicoDTO.get(i).getGuia().getContrato().getPlano().setNmPlano((String) record[21]);
				lGuiaServicoDTO.get(i).getGuia().getContrato().setEmpresaCliente(new EmpresaCliente());
				lGuiaServicoDTO.get(i).getGuia().getContrato().getEmpresaCliente()
						.setId(((BigInteger) record[17]).longValue());
				lGuiaServicoDTO.get(i).getGuia().getContrato().getEmpresaCliente()
						.setNmRazaoSocial((String) record[22]);
				lGuiaServicoDTO.get(i).getGuia().getContrato().setBoBloqueado((Boolean) record[18]);
				lGuiaServicoDTO.get(i).getGuia().getContrato().setBoInativadoConveniada((Boolean) record[20]);

				lGuiaServicoDTO.get(i).setServico(new Servico());
				lGuiaServicoDTO.get(i).getServico().setId(((BigInteger) record[2]).longValue());
				lGuiaServicoDTO.get(i).getServico().setNmServico((String) record[6]);
				lGuiaServicoDTO.get(i).setStatus((Integer) record[3]);
				lGuiaServicoDTO.get(i).setValorServico((Float) record[23]);
				lGuiaServicoDTO.get(i).setVlServicoAssociado((double) ((Float) record[24]).floatValue());
			});
			paginator.setData(lGuiaServicoDTO);
		} catch (Exception e) {
			System.out.println("Error!");
			System.out.println(e);
			return null;
		}
		return paginator;
	}
	
	public void markExpired() {
		 entityManager
		 .createNativeQuery("update realvida.tbguiaservico as guia_servico set status = 5 from realvida.tbguia as guia where (date_trunc('day', guia.dtvalidade) < date_trunc('day', now())) and (guia_servico.status in(0, 1)) and (guia.idguia = guia_servico.idguia)")
	     .executeUpdate();
	}
	
	public List<SummaryServicoByStatusDTO> getSummaryServicoByStatus(Long idEmpresaCliente, Long id,
			Long idGuia, String startDate, String endDate, List<Integer> lStatus)
	{
		SummaryServicoByStatusDTO summaryServicoByStatusDTO = new SummaryServicoByStatusDTO();
		
		summaryServicoByStatusDTO.addSqlWhereConditionAnd("guia_servico.dtexclusao is null");
		
		if(null != idEmpresaCliente)
			summaryServicoByStatusDTO.addSqlWhereConditionAnd("empresacliente.idempresacliente = :idEmpresaCliente");
		
		Date startDateObj = null;
		Date endDateObj = null;
		
		if (null != startDate && null != endDate) {
			String whereOr = "";				
			if(lStatus != null && lStatus.size() > 0){					
				for(Integer st : lStatus){
					if(st.equals(StatusGuiaServicoEnum.CRIADO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")" :
							" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.CONSUMIDO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")" :
							" or " + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")";
											
					if(st.equals(StatusGuiaServicoEnum.INATIVO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")" :
							" or " + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.VENCIDO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")" :
							" or " + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.AGENDADO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")" :
									" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")";

				}
				startDateObj = DateUtil.parseDate(startDate, "yyyy-MM-dd");
				endDateObj = DateUtil.parseDate(endDate, "yyyy-MM-dd");
			}
			
			whereOr += whereOr.length() > 0 ? " )": "";
			if(whereOr.length() > 0)
				summaryServicoByStatusDTO.addSqlWhereConditionAnd(whereOr);
		}
		
//		if (0 < lStatus.size()) {
//			this.markExpired();
//			String clauseStatus = null;
//			clauseStatus = "(guia_servico.status in (";
//			for (int i = 0; i < lStatus.size(); i++) {
//				String separator = (0 == i) ? "" : ",";
//				clauseStatus += separator + lStatus.get(i);
//			}
//			clauseStatus += "))";
//			summaryServicoByStatusDTO.addSqlWhereConditionAnd(clauseStatus);
//		}

		Query query = getEntityManager().createNativeQuery(summaryServicoByStatusDTO.getSqlSelect());
		
		if(null != idEmpresaCliente)		
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
				
		if (null != startDate && startDateObj != null)			
			query.setParameter("startDate", startDateObj, TemporalType.DATE);
		
		if (null != endDate && endDateObj != null)			
			query.setParameter("endDate", endDateObj, TemporalType.DATE);
		
		try {
			List<Object[]> results = query.getResultList();
			List<SummaryServicoByStatusDTO> lSummaryServicoByStatusDTO = new ArrayList<SummaryServicoByStatusDTO>();
			results.stream().forEach((record) -> {
				int i = lSummaryServicoByStatusDTO.size();
				lSummaryServicoByStatusDTO.add(new SummaryServicoByStatusDTO());
				lSummaryServicoByStatusDTO.get(i).setQdtServico((int) ((BigInteger) record[0]).longValue());
				lSummaryServicoByStatusDTO.get(i).setStatus(((Integer) record[1]).intValue());
				lSummaryServicoByStatusDTO.get(i).setCustoServicos((double) ((Float) record[2]).floatValue());
			});
			return lSummaryServicoByStatusDTO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	public List<SummaryGuiaServicoByServicoDTO> getSummaryGuiaServicoByServico(Long idEmpresaCliente, Long id,
			Long idGuia, String startDate, String endDate, List<Integer> lStatus)
	{
		SummaryGuiaServicoByServicoDTO summaryServicoByStatusDTO = new SummaryGuiaServicoByServicoDTO();
		summaryServicoByStatusDTO.setSqlOrderBy("1 desc");
		
		summaryServicoByStatusDTO.addSqlWhereConditionAnd("guia_servico.dtexclusao is null");
		
		if(null != idEmpresaCliente)		
			summaryServicoByStatusDTO.addSqlWhereConditionAnd("empresacliente.idempresacliente = :idEmpresaCliente");
		
		Date startDateObj = null;
		Date endDateObj = null;
		
		if (null != startDate && null != endDate) {
			String whereOr = "";				
			if(lStatus != null && lStatus.size() > 0){
				for(Integer st : lStatus){
					if(st.equals(StatusGuiaServicoEnum.CRIADO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")" :
							" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CRIADO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.CONSUMIDO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")" :
							" or " + "(cast(guia_servico.dtutilizacao as date) between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.CONSUMIDO.getId() + ")";
											
					if(st.equals(StatusGuiaServicoEnum.INATIVO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")" :
							" or " + "(guia.dtexclusao between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.INATIVO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.VENCIDO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")" :
							" or " + "(guia.dtvalidade between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.VENCIDO.getId() + ")";
					
					if(st.equals(StatusGuiaServicoEnum.AGENDADO.getId()))
						whereOr += whereOr.length() == 0 ? "(" + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")" :
									" or " + "(guia.dtinicio between :startDate and :endDate and guia_servico.status = " + StatusGuiaServicoEnum.AGENDADO.getId() + ")";
				}
				
				startDateObj = DateUtil.parseDate(startDate, "yyyy-MM-dd");
				endDateObj = DateUtil.parseDate(endDate, "yyyy-MM-dd");
			}
			
			whereOr += whereOr.length() > 0 ? " )": "";
			if(whereOr.length() > 0)
				summaryServicoByStatusDTO.addSqlWhereConditionAnd(whereOr);
		}
		
//		if (0 < lStatus.size()) {
//			this.markExpired();
//			String clauseStatus = null;
//			clauseStatus = "(guia_servico.status in (";
//			for (int i = 0; i < lStatus.size(); i++) {
//				String separator = (0 == i) ? "" : ",";
//				clauseStatus += separator + lStatus.get(i);
//			}
//			clauseStatus += "))";
//			summaryServicoByStatusDTO.addSqlWhereConditionAnd(clauseStatus);
//		}

		Query query = getEntityManager().createNativeQuery(summaryServicoByStatusDTO.getSqlSelect());
		
		if(null != idEmpresaCliente)		
			query.setParameter("idEmpresaCliente", idEmpresaCliente);
		
		if (null != startDate && startDateObj != null)			
			query.setParameter("startDate", startDateObj, TemporalType.DATE);
				
		if (null != endDate && endDateObj != null)			
			query.setParameter("endDate", endDateObj, TemporalType.DATE);
						
		try {
			List<Object[]> results = query.getResultList();
			List<SummaryGuiaServicoByServicoDTO> lSummarGuiayServicoByServicoDTO = new ArrayList<SummaryGuiaServicoByServicoDTO>();
			results.stream().forEach((record) -> {
				int i = lSummarGuiayServicoByServicoDTO.size();
				lSummarGuiayServicoByServicoDTO.add(new SummaryGuiaServicoByServicoDTO());
				lSummarGuiayServicoByServicoDTO.get(i).setQdtServico((int) ((BigInteger) record[0]).longValue());
				lSummarGuiayServicoByServicoDTO.get(i).setNmServico(((String) record[1]));
				lSummarGuiayServicoByServicoDTO.get(i).setCustoServico((double) ((Float) record[2]).floatValue());
				lSummarGuiayServicoByServicoDTO.get(i).setIdServico(((BigInteger) record[3]).longValue());				
				lSummarGuiayServicoByServicoDTO.get(i).setVlServicoAssociado((double) ((Float) record[4]).floatValue());
				
			});
			return lSummarGuiayServicoByServicoDTO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<GuiaServico> getGuiaServicoByGuia(Long id) {
		Query query = entityManager.createQuery("SELECT guiaServico FROM GuiaServico guiaServico " 
				+ "WHERE guiaServico.guia.id = " + id);
		try {
			List<GuiaServico> list = query.getResultList();
			return list;
		} catch (Exception e) {
			return null;
		}
	}
}
