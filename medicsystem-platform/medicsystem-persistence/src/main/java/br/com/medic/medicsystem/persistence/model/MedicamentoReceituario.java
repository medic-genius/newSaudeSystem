package br.com.medic.medicsystem.persistence.model;

import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name="tbmedicamentosreceituario")
public class MedicamentoReceituario {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEDICAMENTOSRECEITUARIO_ID_SEQ")
	@SequenceGenerator(name = "MEDICAMENTOSRECEITUARIO_ID_SEQ", sequenceName = "realvida.tbmedicamentosreceituario_id_seq", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Basic
	@Column(name="idreceituario")
	private Long idReceituario;
	
	@ManyToOne
	@JoinColumn(name="idmedicamento", referencedColumnName="codigo")
	private Medicamento medicamento;
	
	@Basic
	@Column(name="nmmedicamento")
	private String nmMedicamento;
	
	@Basic
	@Column(name="nmposologia")
	private String nmPosologia;
	
	@Basic
	@Column(name="nmorientacoes")
	private String nmOrientacoes;
	
	@Basic
	@Column(name="vlmedicamento")
	private BigDecimal vlMedicamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdReceituario() {
		return idReceituario;
	}

	public void setIdReceituario(Long idReceituario) {
		this.idReceituario = idReceituario;
	}

	public Medicamento getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public String getNmMedicamento() {
		return nmMedicamento;
	}

	public void setNmMedicamento(String nmMedicamento) {
		this.nmMedicamento = nmMedicamento;
	}

	public String getNmPosologia() {
		return nmPosologia;
	}

	public void setNmPosologia(String nmPosologia) {
		this.nmPosologia = nmPosologia;
	}

	public String getNmOrientacoes() {
		return nmOrientacoes;
	}

	public void setNmOrientacoes(String nmOrientacoes) {
		this.nmOrientacoes = nmOrientacoes;
	}

	public BigDecimal getVlMedicamento() {
		return vlMedicamento;
	}

	public void setVlMedicamento(BigDecimal vlMedicamento) {
		this.vlMedicamento = vlMedicamento;
	}
}