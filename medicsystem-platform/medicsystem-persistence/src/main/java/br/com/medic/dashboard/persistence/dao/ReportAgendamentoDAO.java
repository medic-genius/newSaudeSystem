package br.com.medic.dashboard.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import br.com.medic.dashboard.persistence.dataaccess.MLRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.AgendamentoUnidade;

@Named("ml-report-agendamento-dao")
@ApplicationScoped
public class ReportAgendamentoDAO extends MLRelationalDataAccessObject<AgendamentoUnidade> {

		
	@SuppressWarnings("unchecked")
	public List<AgendamentoUnidade> getReportAgendamentoUnidade(String inicio, String fim) {

		String query = 				
		"SELECT case t1.idunidade "
		+ "WHEN 1 THEN cast('JOAQUIM NABUCO' as text) "
		+ "WHEN 2 THEN cast('TARUMÃ' as text) "
		+ "WHEN 7 THEN cast('GRANDE CIRCULAR' as text) "
		+ "ELSE CAST('DESCONHECIDA' AS text) " 
		+ "END AS unidade, t1.tipo,"
		+ "count(*) as totalagendamento, sum(t1.bloqueado) as totalbloqueado, sum(t1.liberado) as totalliberado, "
		+ "sum(t1.atendido) as totalatendido, sum(t1.faltoso) as totalfaltoso, sum(t1.presente) as totalpresente "
		+ "from( (select CAST('CRIADOS' AS TEXT) as tipo, ag.idagendamento, ag.idunidade, "
		+ "	CASE instatus "
		+ "		WHEN 0 THEN 1 "
		+ "	END AS bloqueado, "
		+ "	CASE instatus "
		+ "		WHEN 1 THEN 1 "
		+ "	END AS liberado, "
		+ "	CASE instatus "
		+ "		WHEN 2 THEN 1 "
		+ "	END AS atendido, "
		+ "	CASE instatus "
		+ "		WHEN 3 THEN 1 "
		+ "	END AS faltoso, "
		+ "	CASE instatus "
		+ "		WHEN 4 THEN 1 "
		+ "	END AS presente "
		+ "FROM realvida.tbagendamento ag "		
		+ "WHERE ag.dtexclusao is null "
		+ "and cast(ag.dtinclusaolog as date) between '" + inicio + "' and '" + fim + "' ) "  
		
		+ "UNION ALL "
		
		+ "(select CAST('AGENDADOS' AS TEXT) as tipo, ag.idagendamento, ag.idunidade, "
		+ "CASE instatus "
	 	+ "	WHEN 0 THEN 1 "
	 	+ "END AS bloqueado, " 
	 	+ "CASE instatus "
	 	+ "	WHEN 1 THEN 1 "
	 	+ "END AS liberado, "
	 	+ "CASE instatus "
	 	+ "	WHEN 2 THEN 1 "
	 	+ "END AS atendido, "
	 	+ "CASE instatus "
	 	+ "	WHEN 3 THEN 1 "
	 	+ "END AS faltoso, "
	 	+ "CASE instatus "
	 	+ "	WHEN 4 THEN 1 "
	 	+ "END AS presente "
		+ "FROM realvida.tbagendamento ag "
		+ "WHERE ag.dtexclusao is null "
		+ "and dtagendamento between '" + inicio + "' and '" + fim + "' ) "	
		+ ") as t1 " 
		+ "GROUP BY t1.idunidade, t1.tipo "
		+ "ORDER BY t1.idunidade, t1.tipo";
				
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("unidade", StringType.INSTANCE)
		        .addScalar("tipo", StringType.INSTANCE)
		        .addScalar("totalagendamento", IntegerType.INSTANCE)
		        .addScalar("totalbloqueado", IntegerType.INSTANCE)
		        .addScalar("totalliberado", IntegerType.INSTANCE)
		        .addScalar("totalatendido", IntegerType.INSTANCE)
		        .addScalar("totalfaltoso", IntegerType.INSTANCE)
		        .addScalar("totalpresente", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(AgendamentoUnidade.class))
		        .list();

	}
}
