package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbitemsolicitado database table.
 * 
 */
@Entity
public class ItemSolicitado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBITEMSOLICITADO_IDITEMSOLICITADO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBITEMSOLICITADO_IDITEMSOLICITADO_GENERATOR")
	private Long iditemsolicitado;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private Integer qtrecebida;

	private Integer qtsolicitada;

	private float vlcustototal;

	private float vlcustounit;

	//bi-directional many-to-one association to Tbpedidocompra
	@ManyToOne
	@JoinColumn(name="idpedidocompra")
	private PedidoCompra pedidoCompra;

	public ItemSolicitado() {
	}

	public Long getIditemsolicitado() {
		return this.iditemsolicitado;
	}

	public void setIditemsolicitado(Long iditemsolicitado) {
		this.iditemsolicitado = iditemsolicitado;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public Integer getQtrecebida() {
		return this.qtrecebida;
	}

	public void setQtrecebida(Integer qtrecebida) {
		this.qtrecebida = qtrecebida;
	}

	public Integer getQtsolicitada() {
		return this.qtsolicitada;
	}

	public void setQtsolicitada(Integer qtsolicitada) {
		this.qtsolicitada = qtsolicitada;
	}

	public float getVlcustototal() {
		return this.vlcustototal;
	}

	public void setVlcustototal(float vlcustototal) {
		this.vlcustototal = vlcustototal;
	}

	public float getVlcustounit() {
		return this.vlcustounit;
	}

	public void setVlcustounit(float vlcustounit) {
		this.vlcustounit = vlcustounit;
	}

	public PedidoCompra getPedidoCompra() {
		return this.pedidoCompra;
	}

	public void setPedidoCompra(PedidoCompra pedidoCompra) {
		this.pedidoCompra = pedidoCompra;
	}

}