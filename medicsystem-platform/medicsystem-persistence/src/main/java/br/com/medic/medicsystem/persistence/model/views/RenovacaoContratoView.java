package br.com.medic.medicsystem.persistence.model.views;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog="realvida", name="rel_renovacaocontrato_view")
public class RenovacaoContratoView {
	
	@Id
	@Column(name="idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name="nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name="dtrenovacao")
	private Timestamp dtRenovacao;
	
	@Basic
	@Column(name="informapagamento")
	private Long inFormaPagamento;
	
	@Basic
	@Column(name="vlcontrato")
	private Double vlContrato;
	
	@Basic
	@Column(name="insituacao")
	private Long inSituacao;
	
	@Basic
	@Column(name="nmcliente")
	private String nmCliente;
	
	@Basic
	@Column(name="nrcodcliente")
	private String nrCodCliente;
	
	@Basic
	@Column(name="idcliente")
	private Long idCliente;
	
	@Basic
	@Column(name="idempresagrupo")
	private Long idEmpresaGrupo;
	
	@Basic
	@Column(name="nmfantasia")
	private String nmFantasia;
	
	@Basic
	@Column(name="nmplano")
	private String nmPlano;
	
	@Basic
	@Column(name="nrqtdemensalidade")
	private Integer nrqtdemensalidade;
	
	@Basic
	@Column(name="idfuncionario")
	private Long idFuncionario;
	
	@Basic
	@Column(name="nmfuncionario")
	private String nmFuncionario;
	
	@Basic
	@Column(name = "boacaoboleto")
	private Boolean boAcaoBoleto;
	
	@Basic
	@Column(name = "vlcontratoantigo")
	private Double vlContratoAntigo;

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}
	

	public Timestamp getDtRenovacao() {
		return dtRenovacao;
	}

	public void setDtRenovacao(Timestamp dtRenovacao) {
		this.dtRenovacao = dtRenovacao;
	}

	public Long getInFormaPagamento() {
		return inFormaPagamento;
	}

	public void setInFormaPagamento(Long inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}

	public Double getVlContrato() {
		return vlContrato;
	}

	public void setVlContrato(Double vlContrato) {
		this.vlContrato = vlContrato;
	}

	public Long getInSituacao() {
		return inSituacao;
	}

	public void setInSituacao(Long inSituacao) {
		this.inSituacao = inSituacao;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNrCodCliente() {
		return nrCodCliente;
	}

	public void setNrCodCliente(String nrCodCliente) {
		this.nrCodCliente = nrCodCliente;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Integer getNrqtdemensalidade() {
		return nrqtdemensalidade;
	}

	public void setNrqtdemensalidade(Integer nrqtdemensalidade) {
		this.nrqtdemensalidade = nrqtdemensalidade;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getNmFuncionario() {
		return nmFuncionario;
	}

	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}

	public Boolean getBoAcaoBoleto() {
		return boAcaoBoleto;
	}

	public void setBoAcaoBoleto(Boolean boAcaoBoleto) {
		this.boAcaoBoleto = boAcaoBoleto;
	}

	public Double getVlContratoAntigo() {
		return vlContratoAntigo;
	}

	public void setVlContratoAntigo(Double vlContratoAntigo) {
		this.vlContratoAntigo = vlContratoAntigo;
	}
}
