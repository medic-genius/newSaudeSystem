package br.com.medic.medicsystem.persistence.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.model.enums.AcaoMovimentacao;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@ApplicationScoped
@Named("movimentacaoconta-dao")
public class MovimentacaoContaDAO extends RelationalDataAccessObject<MovimentacaoConta> {

	public List<MovimentacaoConta> getMovimentacoes(Date dataInicial, Date dataFinal, Long idContaBancaria, Long idEmpresa) {
		List<MovimentacaoConta> movimentacoes = null;

		TypedQuery<MovimentacaoConta> query;
		
		String queryStr = "SELECT m FROM MovimentacaoConta m WHERE m.dtExclusao is  null";
			
		String orderBy = " Order by m.inTipo, m.acaoMovimentacao desc, m.id";
		if (idContaBancaria != null) {
			queryStr += " AND m.contaBancaria.id = " + idContaBancaria;
		}

		if (idEmpresa != null) {
			 queryStr += " AND m.contaBancaria.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresa;
		}

		if (dataInicial != null && dataFinal != null) {
			queryStr += " AND m.dtMovimentacao BETWEEN :startDate AND :endDate";
			query = entityManager.createQuery(queryStr + orderBy , MovimentacaoConta.class);
			query.setParameter("startDate", dataInicial);
			query.setParameter("endDate", dataFinal);
		} else {
			query = entityManager.createQuery(queryStr + orderBy, MovimentacaoConta.class);
		}
		movimentacoes = query.getResultList();

		return movimentacoes;
	}
	
	public List<MovimentacaoConta> getMovimentacaoEntradaPorCaixa( Long idEmpresaFinanceiro, Long idCaixa) {
		
		List<MovimentacaoConta> movimentacoes = null;
		TypedQuery<MovimentacaoConta> query;
		
		String queryStr = " SELECT mc FROM MovimentacaoConta mc WHERE   "
				+ " mc.acaoMovimentacao = " + AcaoMovimentacao.RECEBIMENTO_CAIXA.getId()
				+ " and mc.contaBancaria.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro 
				+ " and mc.idCaixaConferido = " + idCaixa
				+ " and mc.dtExclusao is null";
		
		query = entityManager.createQuery( queryStr, MovimentacaoConta.class);
		
		movimentacoes = query.getResultList();
		
		return movimentacoes;
	}

	public void excluirMovimentacaoDeCaixa(List<MovimentacaoConta> movimentacoes, Funcionario funcionarioLogado) {	
		for (MovimentacaoConta movimentacaoConta : movimentacoes) {
			movimentacaoConta.setDtExclusao( new Date() );
			movimentacaoConta.setFuncionarioExclusao(funcionarioLogado);
			entityManager.merge( movimentacaoConta );
		}
	}

	/** Ajusta movimentacoes de conta bancaria apos pagamento de uma despesa financeiro a partir
	 * da data de pagamento da despesa ate a data atual  */
	public Integer ajustarMovimentacaoContaPosPagamentoDespesa(Long id, Date dtPagamento) {
		
		DateFormat dataformat = new SimpleDateFormat( "yyyy-MM-dd" );
		String dataFormatada = dataformat.format( dtPagamento );
		
		String query = "select financeiro.temp_movimentacaocaixasaldoanterior('"+ dataFormatada +"',"+ id+")"; 
		Query q = findByNativeQuery( query );
		Integer result = (Integer) q.getSingleResult();
		
		return result;
		
	}

	public List< MovimentacaoConta > getMovimentacaoContaByIdDespesa(Long id, Integer acaoMovimentacao ) {
		
		String sqlStr = "SELECT mov FROM MovimentacaoConta mov, MovimentacaoItemConta movitem WHERE "
				+ " mov.id = movitem.movimentacaoConta.id "
				+"  AND movitem.despesaFinanceiro.id = " + id;
		
		if( acaoMovimentacao != null )
			sqlStr += " AND mov.acaoMovimentacao = " + acaoMovimentacao;
		
		sqlStr += " ORDER BY mov.id DESC";
		
		TypedQuery<MovimentacaoConta> query = entityManager.createQuery( sqlStr, MovimentacaoConta.class );
		
		if( !query.getResultList().isEmpty() )
			return query.getResultList();
		
		
		return null;
	}
	
public MovimentacaoConta getUltimoSaldoAnteriorDePeriodoDeContaBancaria(Date dtReferencia, Long idContaBancaria) {
		
		Date dtReferenciaLastMonth = DateUtil.addAmountInFieldCalendar(dtReferencia, Calendar.MONTH , -1);
		
		String dtStart = DateUtil.getDateAsString( DateUtil.getFirstDateFromMonth(dtReferencia), "yyyy-MM-dd" );  
		String dtEnd   = DateUtil.getDateAsString( DateUtil.getLastDateFromMonth( dtReferencia  ), "yyyy-MM-dd" );
		
		String dtStartLastMonth = DateUtil.getDateAsString( DateUtil.getFirstDateFromMonth(dtReferenciaLastMonth), "yyyy-MM-dd" );  
		String dtEndLastMonth   = DateUtil.getDateAsString( DateUtil.getLastDateFromMonth( dtReferenciaLastMonth  ), "yyyy-MM-dd" );
		
		
		
		String sqlStr = "SELECT mov FROM MovimentacaoConta mov WHERE "
				+ " mov.contaBancaria.id =  " + idContaBancaria
				+ " AND  mov.acaoMovimentacao = " + AcaoMovimentacao.SALDO_ANTERIOR.getId()  
				+ " AND  mov.dtMovimentacao between '"+ dtStart  + "' AND '"+ dtEnd  + "'"
				+ " AND ("
					+ " SELECT COUNT(submov.id) FROM MovimentacaoConta submov WHERE "
					+ " submov.dtMovimentacao between '"+ dtStartLastMonth  + "' AND '"+ dtEndLastMonth  + "'"
					+ " AND submov.contaBancaria.id = " + idContaBancaria
					+ " AND submov.dtExclusao is null"
				+ " ) > 0"
				+ " ORDER BY mov.dtMovimentacao";
		
		TypedQuery<MovimentacaoConta> query = entityManager.createQuery( sqlStr, MovimentacaoConta.class );
		
		if( !query.getResultList().isEmpty() )
			return query.getResultList().get(0);
		
		return null;
		
	}
}
