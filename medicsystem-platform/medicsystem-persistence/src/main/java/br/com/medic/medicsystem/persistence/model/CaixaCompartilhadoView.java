package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(catalog = "compartilhado", name = "conferencia_caixa_compartilhado_view")
public class CaixaCompartilhadoView implements Serializable {
	
	private static final long serialVersionUID = -4985648321002535891L;

	@Id
	@Column(name = "idcaixa")
	private Long idCaixa; 
	
	@Column(name = "nmcaixa")
	private String nmCaixa;
	
	@Column(name = "instatus")
	private Integer inStatus; 

	@Column(name = "dtcaixa")
	private Date dtCaixa;
	
	@Column(name = "nmobservacaoconferencia")
	private String nmObservacaoConferencia;
	
	@Column(name = "dtconferencia")
	private Date dtConferencia;
	
	@Column(name = "idoperador")
	private Long idOperador;
	
	@Column(name = "nmoperador")
	private String nmOperador;
	
	@Column(name = "idconferente")
	private Long idConferente;
	
	@Column(name = "nmconferente")
	private String nmConferente;
	
	@Column(name = "idunidade")
	private Long idUnidade;
	
	@Column(name = "nmunidade")
	private String nmUnidade; 

	@Column(name = "idempresagrupo")
	private Long idEmpresaGrupo; 
	
	@Column(name = "nmfantasia")
	private String nmFantasia;
	
	@Transient
	private String dtCaixaFormatada;

	public Long getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Long idCaixa) {
		this.idCaixa = idCaixa;
	}

	public String getNmCaixa() {
		return nmCaixa;
	}

	public void setNmCaixa(String nmCaixa) {
		this.nmCaixa = nmCaixa;
	}

	public Integer getInStatus() {
		return inStatus;
	}

	public void setInStatus(Integer inStatus) {
		this.inStatus = inStatus;
	}

	public Date getDtCaixa() {
		return dtCaixa;
	}

	public void setDtCaixa(Date dtCaixa) {
		this.dtCaixa = dtCaixa;
	}

	public String getNmObservacaoConferencia() {
		return nmObservacaoConferencia;
	}

	public void setNmObservacaoConferencia(String nmObservacaoConferencia) {
		this.nmObservacaoConferencia = nmObservacaoConferencia;
	}

	public Date getDtConferencia() {
		return dtConferencia;
	}

	public void setDtConferencia(Date dtConferencia) {
		this.dtConferencia = dtConferencia;
	}

	public Long getIdOperador() {
		return idOperador;
	}

	public void setIdOperador(Long idOperador) {
		this.idOperador = idOperador;
	}

	public String getNmOperador() {
		return nmOperador;
	}

	public void setNmOperador(String nmOperador) {
		this.nmOperador = nmOperador;
	}

	public Long getIdConferente() {
		return idConferente;
	}

	public void setIdConferente(Long idConferente) {
		this.idConferente = idConferente;
	}

	public String getNmConferente() {
		return nmConferente;
	}

	public void setNmConferente(String nmConferente) {
		this.nmConferente = nmConferente;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

	public Long getIdEmpresaGrupo() {
		return idEmpresaGrupo;
	}

	public void setIdEmpresaGrupo(Long idEmpresaGrupo) {
		this.idEmpresaGrupo = idEmpresaGrupo;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}
	public String getDtCaixaFormatada() {
		if ( getDtCaixa()  != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(getDtCaixa());
		}
		return null;
	}
	
	public void setDtCaixaFormatado(String dtCaixaFormatado) {
		this.dtCaixaFormatada = dtCaixaFormatado;
	}
	
}
