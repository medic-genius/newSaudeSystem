package br.com.medic.dashboard.persistence.model;

import java.text.NumberFormat;
import java.util.Locale;

public class MovimentacaoCliente implements GraphWrapper {

	private String clientes;
	
	private String nmUnidade;

	private Integer quantidade;
	
	private Double faturamento;
	
	private String faturamentoFormatado;

	public MovimentacaoCliente() {
		// TODO Auto-generated constructor stub
	}

	public String getClientes() {
		return clientes;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setClientes(String clientes) {
		this.clientes = clientes;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	@Override
    public String getGraphLabel() {
	    // TODO Auto-generated method stub
	    return null;
    }

	@Override
    public Double getGraphValue() {
	    // TODO Auto-generated method stub
	    return null;
    }

	public Double getFaturamento() {
		if(faturamento == null)
			faturamento = 0.0;
		
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public String getFaturamentoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

		if (getFaturamento() != null)
			faturamentoFormatado = currencyFormatter.format(getFaturamento());
		return faturamentoFormatado;
	}

	public void setFaturamentoFormatado(String faturamentoFormatado) {
		this.faturamentoFormatado = faturamentoFormatado;
	}

	public String getNmUnidade() {
		return nmUnidade;
	}

	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}

}
