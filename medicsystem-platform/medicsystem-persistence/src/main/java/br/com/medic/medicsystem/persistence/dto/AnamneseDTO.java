package br.com.medic.medicsystem.persistence.dto;

import javax.persistence.Basic;
import javax.persistence.Column;

public class AnamneseDTO {
	
	private Long idAnamnese;
	
	private String nmDoencaAnterior;
	
	private String nmDoencaFamilia;

	private String nmTipoAlergia;
	
	private Long idPaciente;
	
	private String inTipoPaciente;

	public String getNmDoencaAnterior() {
		return nmDoencaAnterior;
	}

	public void setNmDoencaAnterior(String nmDoencaAnterior) {
		this.nmDoencaAnterior = nmDoencaAnterior;
	}

	public String getNmDoencaFamilia() {
		return nmDoencaFamilia;
	}

	public void setNmDoencaFamilia(String nmDoencaFamilia) {
		this.nmDoencaFamilia = nmDoencaFamilia;
	}

	public String getNmTipoAlergia() {
		return nmTipoAlergia;
	}

	public void setNmTipoAlergia(String nmTipoAlergia) {
		this.nmTipoAlergia = nmTipoAlergia;
	}

	public Long getIdAnamnese() {
		return idAnamnese;
	}

	public void setIdAnamnese(Long idAnamnese) {
		this.idAnamnese = idAnamnese;
	}	

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getInTipoPaciente() {
		return inTipoPaciente;
	}

	public void setInTipoPaciente(String inTipoPaciente) {
		this.inTipoPaciente = inTipoPaciente;
	}


	
	

}
