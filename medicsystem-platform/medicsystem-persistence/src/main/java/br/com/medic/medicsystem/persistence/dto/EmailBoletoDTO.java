package br.com.medic.medicsystem.persistence.dto;

public class EmailBoletoDTO {
	
	private String nmCliente;
	private String dataPagamento;
	private String nossoNumero;
	private String datavencimento;
	private String vlPago;
	private String nrMensalidade;
	
	
	
	public EmailBoletoDTO(String nmCliente, String dataPagamento,
			String nossoNumero, String datavencimento, String vlPago,
			String nrMensalidade) {
		this.nmCliente = nmCliente;
		this.dataPagamento = dataPagamento;
		this.nossoNumero = nossoNumero;
		this.datavencimento = datavencimento;
		this.vlPago = vlPago;
		this.nrMensalidade = nrMensalidade;
	}
	
	public EmailBoletoDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getNmCliente() {
		return nmCliente;
	}
	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}
	public String getDataPagamento() {
		return dataPagamento;
	}
	public void setDataPagamento(String dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public String getNossoNumero() {
		return nossoNumero;
	}
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}
	public String getDatavencimento() {
		return datavencimento;
	}
	public void setDatavencimento(String datavencimento) {
		this.datavencimento = datavencimento;
	}
	public String getVlPago() {
		return vlPago;
	}
	public void setVlPago(String vlPago) {
		this.vlPago = vlPago;
	}
	public String getNrMensalidade() {
		return nrMensalidade;
	}
	public void setNrMensalidade(String nrMensalidade) {
		this.nrMensalidade = nrMensalidade;
	}
	
	
	

}
