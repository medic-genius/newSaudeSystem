package br.com.medic.medicsystem.persistence.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.medic.medicsystem.persistence.utils.Utils;

public class PacienteCadastroDTO {
	private Long idPaciente;
	
	private Integer tipo;
	
	private Date dtNascimento;
	
	private Integer inSexo;
	
	private String nrCelular;
	
	private String nmEmail;

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Integer getInSexo() {
		return inSexo;
	}

	public void setInSexo(Integer inSexo) {
		this.inSexo = inSexo;
	}

	public String getNrCelular() {
		return Utils.removeSpecialChars(nrCelular);
	}

	public void setNrCelular(String nrCelular) {
		this.nrCelular = nrCelular;
	}

	public String getNmEmail() {
		return nmEmail;
	}

	public void setNmEmail(String nmEmail) {
		this.nmEmail = nmEmail;
	}
	
	public String getDtNascimentoFormatado(){
		if (this.dtNascimento != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(this.dtNascimento);
		}
		return null;
	}
	
}
