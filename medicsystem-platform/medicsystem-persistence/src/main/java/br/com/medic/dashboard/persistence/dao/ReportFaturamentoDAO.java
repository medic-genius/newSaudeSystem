package br.com.medic.dashboard.persistence.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import br.com.medic.dashboard.persistence.dataaccess.MLRelationalDataAccessObject;
import br.com.medic.dashboard.persistence.model.FaturamentoDateQuery;
import br.com.medic.dashboard.persistence.model.FaturamentoMesAMes;
import br.com.medic.dashboard.persistence.model.FaturamentoMovimentacaoContratos;
import br.com.medic.dashboard.persistence.model.FaturamentoTipoPagamento;

@Named("ml-report-faturamento-dao")
@ApplicationScoped
public class ReportFaturamentoDAO extends
        MLRelationalDataAccessObject<FaturamentoTipoPagamento> {
	
	public FaturamentoTipoPagamento getTotalFaturamento(List<FaturamentoTipoPagamento> list) {
		FaturamentoTipoPagamento ftp = new FaturamentoTipoPagamento();
		
		Double totalFaturamento = 0D;
		Integer totalNumeroContratos = 0;
		
		for (FaturamentoTipoPagamento ftpItem : list) {
	        totalFaturamento += ftpItem.getFaturamento();
	        totalNumeroContratos += ftpItem.getNumerocontratos();
        }
				
		ftp.setFormapagamento("<b>TOTAL</b>");
		ftp.setFaturamento(totalFaturamento);
		ftp.setNumerocontratos(totalNumeroContratos);
		return ftp;
	}
	
	@SuppressWarnings("unchecked")
	public List<FaturamentoTipoPagamento> getReportFaturamentoPorTipoPagamento(
	        String ano, String mes) {

		String query = 
				"SELECT "
				+ "CASE "
				+ "WHEN mens.informapagamento = 1 THEN 'DEBITO' " 
				+ "WHEN mens.informapagamento = 5 THEN 'BOLETO' "
				+ "WHEN mens.informapagamento = 0 THEN 'CONTRA CHEQUE' " 
				+ "WHEN mens.informapagamento = 30 THEN 'RECORRENTE' "
				+ "ELSE 'OUTROS' END AS FormaPagamento, "
				+ "COUNT(cont.idcontrato) as NumeroContratos, SUM(mens.vlmensalidade) AS Faturamento " 
				+ "FROM realvida.tbmensalidade mens "
				+ "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = mens.idcontrato " 
				+ "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcontrato = cont.idcontrato "
				+ "INNER JOIN realvida.tbcliente cli ON cli.idcliente = contcli.idcliente "
				+ "AND ( mens.instatus = 0 OR mens.instatus = 1 ) "
				+ "AND mens.dtexclusao is null "
				+ "AND cont.idempresagrupo = 9 "
				+ "AND ( mens.bonegociada is null OR mens.bonegociada = false ) "
				+ "AND (( cont.insituacao = 1 and ( mens.dtvencimento - cont.dtinativacao) <= 0 ) or cont.insituacao = 0) "
				+ "AND ( Extract('Month' From mens.dtvencimento ) = '" + mes + "' ) "
				+ "AND ( Extract('Year' From mens.dtvencimento ) = '"+ ano + "' ) "
				+ "GROUP BY FormaPagamento " 
				+ "ORDER BY Faturamento DESC ";
				
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("formapagamento", StringType.INSTANCE)
		        .addScalar("numerocontratos", IntegerType.INSTANCE)
		        .addScalar("faturamento", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(FaturamentoTipoPagamento.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<FaturamentoTipoPagamento> getReportFaturamentoPagoPorTipoPagamento(
	        String ano, String mes) {

		String query = "SELECT "
		        + "CASE "
		        + "WHEN mens.informapagamento = 1 THEN 'DEBITO' "
		        + "WHEN mens.informapagamento = 5 THEN 'BOLETO' "
		        + "WHEN mens.informapagamento = 0 THEN 'CONTRA CHEQUE' "
		        + "WHEN mens.informapagamento = 30 THEN 'RECORRENTE' "
		        + "ELSE 'OUTROS' END AS FormaPagamento, "
		        + "COUNT(cont.idcontrato) as NumeroContratos, SUM(mens.vlpago) AS Faturamento "
		        + "FROM realvida.tbmensalidade mens "
		        + "INNER JOIN realvida.tbcontrato cont ON cont.idcontrato = mens.idcontrato "
		        + "INNER JOIN realvida.tbcontratocliente contcli ON contcli.idcontrato = cont.idcontrato "
		        + "INNER JOIN realvida.tbcliente cli ON cli.idcliente = contcli.idcliente "
		        + "AND mens.instatus = 1 "
		        + "AND mens.dtexclusao is null " 
		        + "AND cont.idempresagrupo = 9 "
		        + "AND ( mens.bonegociada is null OR mens.bonegociada = false ) "
		        + "AND ( Extract('Month' From mens.dtvencimento ) = '" + mes + "' ) " 
		        + "AND ( Extract('Year' From mens.dtvencimento ) = '" + ano + "' ) "
		        + "GROUP BY FormaPagamento "
		        + "ORDER BY Faturamento DESC";

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("formapagamento", StringType.INSTANCE)
		        .addScalar("numerocontratos", IntegerType.INSTANCE)
		        .addScalar("faturamento", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(FaturamentoTipoPagamento.class))
		        .list();

	}


	@SuppressWarnings("unchecked")
	public List<FaturamentoMovimentacaoContratos> getReportFaturamentoMovimentacaoContratos(
	        String inicio, String fim) {

		String query = 
				"SELECT 'NOVOS CONTRATOS' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento " 
				+ "FROM realvida.tbcontrato cont "
				+ "inner join realvida.tbcontratocliente cc on cont.idcontrato = cc.idcontrato " 
				+ "WHERE cont.idempresagrupo = 9 "
				+ "AND cont.idempresacliente is null "	
				+ "AND cont.idplano is not null "
				+ "AND cont.dtcontrato BETWEEN '" + inicio + "' and '" + fim + "' "
				+ "and cc.idcliente not in ( "
				+ "SELECT cc.idcliente "
				+ "FROM realvida.tbcontrato cont "
				+ "inner join realvida.tbcontratocliente cc on cont.idcontrato = cc.idcontrato " 
				+ "WHERE cont.insituacao = 1 " 
				+ "AND cont.idempresagrupo = 9 "
				+ "AND cont.idempresacliente is null "	
				+ "AND cont.intipoinativacao = 1 " //inativo por cliente
				+ "AND cont.idplano is not null "
				+ "AND cont.dtinativacao BETWEEN '" + inicio + "' and '" + fim + "' "
				+ "AND (cont.motivo ilike '%MIGRACAO%' or cont.motivo ilike '%MIGRAÇÃO%' or cont.motivo ilike '%MIGROU%') "
				+ "AND cont.motivo <> 'ERRO DE CADASTRO' "		
				+ "GROUP BY cc.idcliente "
				+ "order by cc.idcliente) "
		        
		        + "UNION ALL "
		        
				+ "SELECT 'CONTRATOS INATIVADOS' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento "
				+ "FROM realvida.tbcontrato cont "
				+ "WHERE cont.insituacao = 1 " 
				+ "AND cont.idempresagrupo = 9 "
				+ "AND cont.idempresacliente is null "				
				+ "AND cont.idplano is not null "
				+ "AND cont.dtinativacao BETWEEN '" + inicio + "' and '" + fim + "' " 
				+ "AND cont.motivo <> 'ERRO DE CADASTRO'";
		
		/*
		"SELECT"
        + "'NOVOS CONTRATOS' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento "
        + "FROM realvida.tbcontrato cont "
        + "WHERE cont.insituacao = 0 "
        + "AND cont.idempresagrupo = 9 "
        + "AND cont.idplano is not null "
        + "AND cont.dtcontrato BETWEEN '" + inicio + "' and '" + fim + "' "
		*/

		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("situacaocontrato", StringType.INSTANCE)
		        .addScalar("numerocontratos", IntegerType.INSTANCE)
		        .addScalar("faturamento", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(FaturamentoMovimentacaoContratos.class))
		        .list();

	}
	
	@SuppressWarnings("unchecked")
	public List<FaturamentoMovimentacaoContratos> getReportFaturamentoMovimentacaoContratosDetalhado(
	        String inicio, String fim) {
		
		String query =
				"SELECT 'NOVOS CONTRATOS (MIGRAÇÃO)' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento " 
				+ "FROM realvida.tbcontrato cont "
				+ "inner join realvida.tbcontratocliente cc on cont.idcontrato = cc.idcontrato " 
				+ "WHERE cont.insituacao = 0 "
				+ "AND cont.idempresagrupo = 9 "
				+ "AND cont.idempresacliente is null "	
				+ "AND cont.idplano is not null "
				+ "AND cont.dtcontrato BETWEEN '" + inicio + "' and '" + fim + "' "
				+ "and cc.idcliente in ( "
				+ "SELECT cc.idcliente "
				+ "FROM realvida.tbcontrato cont "
				+ "inner join realvida.tbcontratocliente cc on cont.idcontrato = cc.idcontrato " 
				+ "WHERE cont.insituacao = 1 " 
				+ "AND cont.idempresagrupo = 9 "
				+ "AND cont.idempresacliente is null "	
				+ "AND cont.intipoinativacao = 1 " //inativo por cliente
				+ "AND cont.idplano is not null "
				+ "AND cont.dtinativacao BETWEEN '" + inicio + "' and '" + fim + "' "
				+ "AND (cont.motivo ilike '%MIGRACAO%' or cont.motivo ilike '%MIGRAÇÃO%' or cont.motivo ilike '%MIGROU%') "
				+ "AND cont.motivo <> 'ERRO DE CADASTRO' "		
				+ "GROUP BY cc.idcliente "
				+ "order by cc.idcliente) "
				
				+ "UNION ALL "
				
		        + "SELECT 'CONTRATOS INATIVADOS (EMPRESA)' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento "
		        + "FROM realvida.tbcontrato cont "
		        + "WHERE cont.insituacao = 1 " 
		        + "AND cont.idempresagrupo = 9 "
		        + "AND cont.idempresacliente is null "
		        + "AND cont.intipoinativacao = 0" //inativo por empresa
		        + "AND cont.idplano is not null "
		        + "AND cont.dtinativacao BETWEEN '" + inicio + "' and '" + fim + "' " 
		        + "AND cont.motivo <> 'ERRO DE CADASTRO' "
		        
		        + "UNION ALL "
		        
		        + "SELECT 'CONTRATOS INATIVADOS (CLIENTE)' as situacaocontrato, COUNT(cont.idcontrato) as NumeroContratos, SUM(cont.vltotal) AS Faturamento "
		        + "FROM realvida.tbcontrato cont "
		        + "WHERE cont.insituacao = 1 " 
		        + "AND cont.idempresagrupo = 9 "
		        + "AND cont.idempresacliente is null "
		        + "AND cont.intipoinativacao = 1" //inativo por cliente
		        + "AND cont.idplano is not null "
		        + "AND cont.dtinativacao BETWEEN '" + inicio + "' and '" + fim + "' " 
		        + "AND cont.motivo <> 'ERRO DE CADASTRO' ";
				
		return findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("situacaocontrato", StringType.INSTANCE)
		        .addScalar("numerocontratos", IntegerType.INSTANCE)
		        .addScalar("faturamento", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers
		                        .aliasToBean(FaturamentoMovimentacaoContratos.class))
		        .list();
	}

	private List<FaturamentoDateQuery> generateDateRange(Date start, Date end) {
		List<FaturamentoDateQuery> datequeries = new ArrayList<FaturamentoDateQuery>();
		Locale locale = new Locale("pt", "BR");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Calendar startDate = Calendar.getInstance();
		startDate.setTime(start);

		Calendar endDate = Calendar.getInstance();		
		endDate.setTime(start);

		//int startMonth = startDate.get(Calendar.MONTH);
		//int endMonth = endDate.get(Calendar.MONTH);
		
		String month = "";
		String monthFirstDay = "";
		String monthLastDay = "";
		
		for (int i = 0; i <= 12; i++) {
			
			if(i < 1)
			{
				startDate.add(Calendar.MONTH, i);
				month = startDate.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
				month = month.concat( "/"+ String.valueOf(startDate.get(Calendar.YEAR)).substring(2) ) ;
	
				startDate.set(Calendar.DAY_OF_MONTH, startDate.getActualMinimum(Calendar.DAY_OF_MONTH));
				monthFirstDay = formatter.format(startDate.getTime());
				
				endDate.add(Calendar.MONTH, i);
				endDate.set(Calendar.DAY_OF_MONTH, startDate.getActualMaximum(Calendar.DAY_OF_MONTH));
				monthLastDay = formatter.format(endDate.getTime());
			}else{
				startDate.add(Calendar.MONTH, 1);
				month = startDate.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
				month = month.concat( "/"+String.valueOf(startDate.get(Calendar.YEAR)).substring(2) ) ;
	
				startDate.set(Calendar.DAY_OF_MONTH, startDate.getActualMinimum(Calendar.DAY_OF_MONTH));
				monthFirstDay = formatter.format(startDate.getTime());
				
				endDate.add(Calendar.MONTH, 1);
				endDate.set(Calendar.DAY_OF_MONTH, startDate.getActualMaximum(Calendar.DAY_OF_MONTH));
				monthLastDay = formatter.format(endDate.getTime());
			}

			datequeries.add(new FaturamentoDateQuery(month, monthFirstDay, monthLastDay));
		}

		return datequeries;
	}

	@SuppressWarnings("unchecked")
	public List<FaturamentoMesAMes> getReportFaturamentoMesAMes(Date start,
	        Date end) {

		List<FaturamentoDateQuery> datequeries = generateDateRange(start, end);

		StringBuffer query = new StringBuffer();

		for (FaturamentoDateQuery fdq : datequeries) {
			query.append(" UNION ALL ");
			query.append("SELECT '" + fdq.getMes() + "' AS mes, ")
			        .append("(SELECT COALESCE(SUM(cont.vltotal),0) AS Faturamento "
			                + "FROM realvida.tbcontrato cont  "
			                + "WHERE cont.insituacao = 0 "
			                + "AND cont.idempresagrupo = 9 "
			                + "AND cont.idempresacliente is null "
			                + "AND cont.idplano is not null ")
			        .append("AND cont.dtcontrato BETWEEN '" + fdq.getDataInicioMes() + "' and '" + fdq.getDataFimMes() + "') AS contratosNovos, ")
			        .append("(SELECT COALESCE(SUM(cont.vltotal),0) AS Faturamento "
			                + "FROM realvida.tbcontrato cont  "
			                + "WHERE cont.insituacao = 1 "
			                + "AND cont.idempresagrupo = 9 "
			                + "AND cont.idempresacliente is null "
			                + "AND cont.idplano is not null ")
			        .append("AND cont.dtinativacao BETWEEN '" + fdq.getDataInicioMes() + "' and '" + fdq.getDataFimMes() + "'"
			                + " AND cont.motivo <> 'ERRO DE CADASTRO') AS contratosInativados");
		}

		if (datequeries.size() > 0) {
			query.delete(0, 11);
		}

		return findByNativeQuery(query.toString())
		        .unwrap(SQLQuery.class)
		        .addScalar("mes", StringType.INSTANCE)
		        .addScalar("contratosnovos", DoubleType.INSTANCE)
		        .addScalar("contratosinativados", DoubleType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(FaturamentoMesAMes.class))
		        .list();
	}
}
