package br.com.medic.medicsystem.persistence.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa as infomacoes obtidas
 * dos arquivos da folha de pagamento.
 * 
 * @author Phillip
 * 
 * @since 01/2016
 * 
 * @version 1.3
 *
 */

public class FolhaImportResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mesReferencia;
	
	private String nmUnidade;
	
	private String nmEmpresaFinanceiro;

	private Long nroImportados;

	private String erros;

	private List<FuncionarioFolha> output;
	
	private Boolean isFolhaAvulsa;

	public FolhaImportResult() {
		output = new ArrayList<>();
	}

	public String getErros() {
		return erros;
	}

	public String getMesReferencia() {
		return mesReferencia;
	}

	public Long getNroImportados() {
		return nroImportados;
	}

	public void setErros(String erros) {
		this.erros = erros;
	}

	public void setMesReferencia(String mesReferencia) {
		this.mesReferencia = mesReferencia;
	}

	public void setNroImportados(Long nroImportados) {
		this.nroImportados = nroImportados;
	}

	public List<FuncionarioFolha> getOutput() {
		return output;
	}

	public void setOutput(List<FuncionarioFolha> output) {
		this.output = output;
	}
	
	public String getNmUnidade() {
		return nmUnidade;
	}
	
	public void setNmUnidade(String nmUnidade) {
		this.nmUnidade = nmUnidade;
	}
	
	public String getNmEmpresaFinanceiro() {
		return nmEmpresaFinanceiro;
	}
	
	public void setNmEmpresaFinanceiro(String nmEmpresaFinanceiro) {
		this.nmEmpresaFinanceiro = nmEmpresaFinanceiro;
	}
	
	public Boolean getIsFolhaAvulsa() {
		return isFolhaAvulsa;
	}
	
	public void setIsFolhaAvulsa(Boolean isFolhaAvulsa) {
		this.isFolhaAvulsa = isFolhaAvulsa;
	}
}
