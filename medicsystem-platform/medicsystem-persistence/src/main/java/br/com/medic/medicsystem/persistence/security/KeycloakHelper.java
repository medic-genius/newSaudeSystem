package br.com.medic.medicsystem.persistence.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jboss.logging.Logger;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;

import br.com.medic.medicsystem.persistence.security.dto.KeycloakTokenDTO;
import br.com.medic.medicsystem.persistence.security.dto.KeycloakUserRolesDTO;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@ApplicationScoped
public class KeycloakHelper {

	@Inject
	private Logger logger;

	private KeycloakDeployment deployment;

	private String url;

	private ObjectMapper mapper = new ObjectMapper();

	public void initialize(String keycloakJSON) {
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(keycloakJSON);
		this.deployment = KeycloakDeploymentBuilder.build(is);

		this.url = deployment.getAuthServerBaseUrl() + "/realms/"
		        + deployment.getRealm() + "/protocol/openid-connect/token";

		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	public KeycloakTokenDTO login(String username, String password) {
		CloseableHttpClient httpclient = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("username", username));
		nvps.add(new BasicNameValuePair("password", password));
		nvps.add(new BasicNameValuePair("client_id", deployment
		        .getResourceName()));
		nvps.add(new BasicNameValuePair("grant_type", "password"));
		nvps.add(new BasicNameValuePair("client_secret", deployment
		        .getResourceCredentials().get("secret")));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			CloseableHttpResponse response = httpclient.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 401) {
				return null;
			}

			if (response.getStatusLine().getStatusCode() == 200) {

				KeycloakTokenDTO token = mapper.readValue(response.getEntity()
				        .getContent(), KeycloakTokenDTO.class);

				return token;
			}
		} catch (Throwable e) {
			logger.error("Falha ao logar com gerente login____"+username, e);
			return null;
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				logger.error("Falha ao fechar conexao com servidor keycloak", e);
			}
		}
		return null;
	}

	public String getUserID(String username, String token) {
		CloseableHttpClient httpclient = HttpClients.createDefault();

		HttpGet httpGet = new HttpGet(deployment.getAuthServerBaseUrl()
		        + "/admin/realms/" + deployment.getRealm() + "/users?username="
		        + username);
		httpGet.addHeader("Authorization", "Bearer " + token);

		try {
			CloseableHttpResponse responseUserData = httpclient
			        .execute(httpGet);
			if (responseUserData.getStatusLine().getStatusCode() == 200) {

				@SuppressWarnings("unchecked")
				ArrayList<LinkedHashMap<String, String>> usersFound = mapper
				        .readValue(responseUserData.getEntity().getContent(),
				                ArrayList.class);

				return usersFound.get(0).get("id");
			}
		} catch (Throwable e) {
			logger.error("Falha ao logar com gerente getUserID___"+username, e);
			return null;
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				logger.error("Falha ao fechar conexao com servidor keycloak", e);
			}
		}

		return null;
	}

	public boolean hasRoles(String userID, String token, String... roles) {
		CloseableHttpClient httpclient = HttpClients.createDefault();

		HttpGet httpGet2 = new HttpGet(deployment.getAuthServerBaseUrl()
		        + "/admin/realms/" + deployment.getRealm() + "/users/" + userID
		        + "/role-mappings");
		httpGet2.addHeader("Authorization", "Bearer " + token);

		try {
			CloseableHttpResponse responseUserRoles = httpclient
			        .execute(httpGet2);

			KeycloakUserRolesDTO usersRoles = mapper.readValue(
			        responseUserRoles.getEntity().getContent(),
			        KeycloakUserRolesDTO.class);

			@SuppressWarnings("unchecked")
			ArrayList<LinkedHashMap<String, String>> mappings = (ArrayList<LinkedHashMap<String, String>>) usersRoles
			        .getClientMappings().get("medicsystem-api").get("mappings");

			for (String role : roles) {
				for (LinkedHashMap<String, String> keycloakMappingDTO : mappings) {
					if (keycloakMappingDTO.get("name").equals(role)) {
						return true;
					}
				}
			}

		} catch (Throwable e) {
			logger.error("Falha ao logar com gerente hasRoles___"+userID, e);
			return false;
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				logger.error("Falha ao fechar conexao com servidor keycloak", e);
			}
		}

		return false;
	}

}
