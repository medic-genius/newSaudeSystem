package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "conveniada_cliente_dependente_view")
public class ClienteDependenteConvenioView {
	
	@Id
	@Column(name = "id")
	Long id;
	
	@Basic
	@Column(name = "idcliente")
	Long idCliente;

	@Basic
	@Column(name = "nmcliente")
	String nmCliente;
	
	@Basic
	@Column(name = "nmdependente")
	String nmDependente;
	
	@Basic
	@Column(name = "idempresacliente")
	Long idEmpresaCliente;
	
	@Basic
	@Column(name = "nrcontrato")
	String nrContrato;
	
	@Basic
	@Column(name = "vlcontrato")
	Float vlContrato;
	
	@Basic
	@Column(name = "nmplano")
	String nmPlano;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpresaCliente() {
		return idEmpresaCliente;
	}

	public void setIdEmpresaCliente(Long idEmpresaCliente) {
		this.idEmpresaCliente = idEmpresaCliente;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Float getVlContrato() {
		return vlContrato;
	}

	public void setVlContrato(Float vlContrato) {
		this.vlContrato = vlContrato;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmDependente() {
		return nmDependente;
	}

	public void setNmDependente(String nmDependente) {
		this.nmDependente = nmDependente;
	}
	
	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
}
