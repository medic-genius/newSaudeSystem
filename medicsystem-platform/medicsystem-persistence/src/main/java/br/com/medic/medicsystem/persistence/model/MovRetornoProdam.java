package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbmovretornoprodam database table.
 * 
 */
@Entity
public class MovRetornoProdam implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBMOVRETORNOPRODAM_IDMOVRETORNOPRODAM_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBMOVRETORNOPRODAM_IDMOVRETORNOPRODAM_GENERATOR")
	private Long idmovretornoprodam;

	private Long cddesconto;

	private String cdgrupo;

	private String cdgrupoauxiliar;

	private Long cdgrupofolha;

	private Long cdorgao;

	private Timestamp dtatualizacaolog;

	private Timestamp dtinclusaolog;

	private String nmlotacao;

	private String nmorgao;

	private String nmtitular;

	private String nrmatricula;

	private String ocorrencia1;

	private Long qtparcelavencer;

	private double vldesconto;

	private double vlmensalidadeatual;

	//bi-directional many-to-one association to Tbcontratocliente
	@ManyToOne
	@JoinColumn(name="idcontratocliente")
	private ContratoCliente contratoCliente;

	//bi-directional many-to-one association to Tborgao
	@ManyToOne
	@JoinColumn(name="idorgao")
	private Orgao tborgao;

	//bi-directional many-to-one association to Tbretornoprodam
	@ManyToOne
	@JoinColumn(name="idretornoprodam")
	private RetornoProdam tbretornoprodam;

	public MovRetornoProdam() {
	}

	public Long getIdmovretornoprodam() {
		return this.idmovretornoprodam;
	}

	public void setIdmovretornoprodam(Long idmovretornoprodam) {
		this.idmovretornoprodam = idmovretornoprodam;
	}

	public Long getCddesconto() {
		return this.cddesconto;
	}

	public void setCddesconto(Long cddesconto) {
		this.cddesconto = cddesconto;
	}

	public String getCdgrupo() {
		return this.cdgrupo;
	}

	public void setCdgrupo(String cdgrupo) {
		this.cdgrupo = cdgrupo;
	}

	public String getCdgrupoauxiliar() {
		return this.cdgrupoauxiliar;
	}

	public void setCdgrupoauxiliar(String cdgrupoauxiliar) {
		this.cdgrupoauxiliar = cdgrupoauxiliar;
	}

	public Long getCdgrupofolha() {
		return this.cdgrupofolha;
	}

	public void setCdgrupofolha(Long cdgrupofolha) {
		this.cdgrupofolha = cdgrupofolha;
	}

	public Long getCdorgao() {
		return this.cdorgao;
	}

	public void setCdorgao(Long cdorgao) {
		this.cdorgao = cdorgao;
	}

	public Timestamp getDtatualizacaolog() {
		return this.dtatualizacaolog;
	}

	public void setDtatualizacaolog(Timestamp dtatualizacaolog) {
		this.dtatualizacaolog = dtatualizacaolog;
	}

	public Timestamp getDtinclusaolog() {
		return this.dtinclusaolog;
	}

	public void setDtinclusaolog(Timestamp dtinclusaolog) {
		this.dtinclusaolog = dtinclusaolog;
	}

	public String getNmlotacao() {
		return this.nmlotacao;
	}

	public void setNmlotacao(String nmlotacao) {
		this.nmlotacao = nmlotacao;
	}

	public String getNmorgao() {
		return this.nmorgao;
	}

	public void setNmorgao(String nmorgao) {
		this.nmorgao = nmorgao;
	}

	public String getNmtitular() {
		return this.nmtitular;
	}

	public void setNmtitular(String nmtitular) {
		this.nmtitular = nmtitular;
	}

	public String getNrmatricula() {
		return this.nrmatricula;
	}

	public void setNrmatricula(String nrmatricula) {
		this.nrmatricula = nrmatricula;
	}

	public String getOcorrencia1() {
		return this.ocorrencia1;
	}

	public void setOcorrencia1(String ocorrencia1) {
		this.ocorrencia1 = ocorrencia1;
	}

	public Long getQtparcelavencer() {
		return this.qtparcelavencer;
	}

	public void setQtparcelavencer(Long qtparcelavencer) {
		this.qtparcelavencer = qtparcelavencer;
	}

	public double getVldesconto() {
		return this.vldesconto;
	}

	public void setVldesconto(double vldesconto) {
		this.vldesconto = vldesconto;
	}

	public double getVlmensalidadeatual() {
		return this.vlmensalidadeatual;
	}

	public void setVlmensalidadeatual(double vlmensalidadeatual) {
		this.vlmensalidadeatual = vlmensalidadeatual;
	}

	public ContratoCliente getTbcontratocliente() {
		return this.contratoCliente;
	}

	public void setContratoCliente(ContratoCliente tbcontratocliente) {
		this.contratoCliente = tbcontratocliente;
	}

	public Orgao getTborgao() {
		return this.tborgao;
	}

	public void setOrgao(Orgao tborgao) {
		this.tborgao = tborgao;
	}

	public RetornoProdam getTbretornoprodam() {
		return this.tbretornoprodam;
	}

	public void setRetornoProdam(RetornoProdam tbretornoprodam) {
		this.tbretornoprodam = tbretornoprodam;
	}

}