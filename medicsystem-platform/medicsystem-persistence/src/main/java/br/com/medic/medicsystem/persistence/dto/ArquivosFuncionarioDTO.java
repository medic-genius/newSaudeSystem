package br.com.medic.medicsystem.persistence.dto;

import java.util.Collection;

import br.com.medic.medicsystem.persistence.model.Funcionario;

/**
 * @author Joelton
 * @since 19/04/2016
 * @version 1.0
 *
 * last modification 09/05/2016
 * by Joelton Matos
 */

public class ArquivosFuncionarioDTO {
	
	private Funcionario funcionario;
	private Collection<?> arquivos;
	
	public Funcionario getFuncionario() {
		return funcionario;
	}
	
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	public Collection<?> getArquivos() {
		return arquivos;
	}
	
	public void setArquivos(Collection<?> arquivos) {
		this.arquivos = arquivos;
	}

}
