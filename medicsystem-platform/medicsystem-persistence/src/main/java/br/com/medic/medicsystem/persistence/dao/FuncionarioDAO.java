package br.com.medic.medicsystem.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.AtividadeAgendamentoDTO;
import br.com.medic.medicsystem.persistence.dto.EspecialidadeDTO;
import br.com.medic.medicsystem.persistence.dto.IntervaloBloqueadoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Bloqueio;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.views.ServicoExame;

/**
 * @author Phillip
 * @since 01/2016
 * @version 1.2
 * 
 * last modification: 09/05/2016
 * by : Joelton Matos
 *
 */

@Named("funcionario-dao")
@ApplicationScoped
public class FuncionarioDAO extends RelationalDataAccessObject<Funcionario> {
	
	@Inject
	private Logger logger;

	public Collection<Profissional> getProfissionaisPorEspecialidade(
	        Long idEspecialidade, Long idSubEspecialidade, Long idUnidade) {

		String query = "SELECT DISTINCT func.*, prof.intipoprofissional, prof.nrcrmcro"
		        + " FROM realvida.tbAtendimentoProfissional_View ap"
		        + " INNER JOIN realvida.tbprofissional prof ON prof.idfuncionario = ap.idprofissional"
		        + " INNER JOIN realvida.tbfuncionario func ON func.idfuncionario = prof.idfuncionario"
		        + " WHERE ap.inTipoAtendimento = inTipoAtendimento ";
		       
		
		if (idEspecialidade != null) {
			query +=   " AND ap.idEspProfissional = " + idEspecialidade;
		}
		
		if (idUnidade != null) {
			query += " AND ap.idUnidade = " + idUnidade;
		}

		if (idSubEspecialidade != null) {
			query += " AND ap.idSubEspProfissional = " + idSubEspecialidade;
		}

		query += " ORDER BY func.nmfuncionario";

		Collection<Profissional> result = findByNativeQuery(query,
		        Profissional.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	
	public Collection<Profissional> getProfissionaisPorEspecialidadeTriagem(
			List<Long> idEspecialidade, Long idSubEspecialidade, Long idUnidade) {
		
		
		String listIds = "";

		if (idEspecialidade != null && idEspecialidade.size() == 1) {

			listIds = idEspecialidade.get(0).toString();
		} else {

			for (Long id : idEspecialidade) {

				listIds += id + ",";

			}

			listIds = listIds.substring(0, listIds.length() - 1);

		}

		String query = "SELECT DISTINCT func.*, prof.intipoprofissional, prof.nrcrmcro"
		        + " FROM realvida.tbAtendimentoProfissional_View ap"
		        + " INNER JOIN realvida.tbprofissional prof ON prof.idfuncionario = ap.idprofissional"
		        + " INNER JOIN realvida.tbfuncionario func ON func.idfuncionario = prof.idfuncionario"
		        + " WHERE ap.inTipoAtendimento = inTipoAtendimento ";
		       
		
		if (idEspecialidade != null) {
			query +=   " AND ap.idEspProfissional IN  (" + listIds+")";
		}
		
		if (idUnidade != null) {
			query += " AND ap.idUnidade = " + idUnidade;
		}

		if (idSubEspecialidade != null) {
			query += " AND ap.idSubEspProfissional = " + idSubEspecialidade;
		}

		query += " ORDER BY func.nmfuncionario";

		Collection<Profissional> result = findByNativeQuery(query,
		        Profissional.class);

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	
	public List<Funcionario> getMedicosPorNome(String nmMedico) {
		logger.debug("query = ");
		String query_str = "SELECT f FROM Funcionario f WHERE f.inGrupoFuncionario = 1 ";

		if (nmMedico != null) {
			String where_medico;
			String where_cargo;

			if(nmMedico.startsWith("%") || nmMedico.endsWith("%")) {
				where_medico = nmMedico.toUpperCase();

			} else {
				where_medico = "%" + nmMedico.toUpperCase() + "%";
			}

			if(nmMedico.startsWith("%") || nmMedico.endsWith("%")) {
				where_cargo = nmMedico.toUpperCase();

			} else {
				where_cargo = "%" + nmMedico.toUpperCase() + "%";
			}

			query_str += " AND f.nmFuncionario LIKE '" + where_medico + "'";
			query_str += " OR f.nmCargo LIKE '" + where_cargo + "'";
		}

		query_str += " ORDER BY f.nmFuncionario";
		
		logger.debug("query = " + query_str);
		
		
		TypedQuery<Funcionario> query = entityManager.createQuery(query_str, Funcionario.class);
		List<Funcionario> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public List<IntervaloBloqueadoDTO> getDiasBloqueadosDoMes(
	        Long idProfissional, String startDate, String endDate, Long idUnidade) {
		
		
		String queryStr = " SELECT dtinicio, dtfim FROM realvida.tbbloqueio "
		+" WHERE idfuncionario = "+  idProfissional
		+" AND dtexclusaobloqueio is null"  
		+" AND bonaoativo is false "
		+" AND idunidade = "+idUnidade
		+" AND ((dtinicio between '"+startDate+"' and '"+endDate+"' OR dtfim between '"+startDate+"' and '"+endDate+"') "
		+" or ('"+startDate+"' between dtinicio and dtfim and ('"+endDate+"' between dtinicio and dtfim ) ))"
		+" AND dtfim >= current_date AND inTurno = -1 ORDER BY dtinicio ";


		@SuppressWarnings("unchecked")
		List<IntervaloBloqueadoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("dtinicio", DateType.INSTANCE)
		        .addScalar("dtfim", DateType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(IntervaloBloqueadoDTO.class))
		        .list();

		return result;

	}
	
	public List<Bloqueio> getBloqueioDoMes(
	        Long idProfissional, String date, Long idUnidade) {
		
		String queryStr =	"SELECT b "
		+" FROM Bloqueio b "
		+" WHERE b.profissional.id =  "+ idProfissional
		+" AND b.dtExclusaoBloqueio is null  "
		+" AND b.boNaoAtivo is false "
		+" AND b.unidade.id = "+idUnidade
		+" AND  '"+date+"' between b.dtInicio and b.dtFim "
		+" AND '"+date+"' >= current_date "
		+" AND b.inTurno != -1"
		+" ORDER BY b.dtInicio  ";

		TypedQuery<Bloqueio> query = entityManager.createQuery(queryStr, Bloqueio.class);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<Long> getBloqueioByprofissional(
	        Long idProfissional, String date) {
		
		String queryStr = " SELECT idunidade FROM realvida.tbbloqueio b"
		+" WHERE idfuncionario = "+  idProfissional
		+" AND dtexclusaobloqueio is null"  
		+" AND bonaoativo is false "
		+" AND  '"+date+"' between b.dtInicio and b.dtFim "
		+" AND '"+date+"' >= current_date "
		+" AND b.inTurno = -1"
		+" ORDER BY b.dtInicio  ";
		
		try {
			Query q = findByNativeQuery(queryStr);
			List<Long> result = (List<Long>) q.getResultList();
			return result;
		} catch (Exception e) {
			return null;
		}

	}

	public List<Funcionario> getVendedoresPorEmpresaGrupo(Long idEmpresa) {
		TypedQuery<Funcionario> query = entityManager.createQuery(
		        "SELECT c FROM Funcionario c where c.unidade.empresaGrupo.id = "
		                + idEmpresa + " order by c.nmFuncionario asc",
		        Funcionario.class);
		List<Funcionario> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

	public Funcionario getFuncionarioPorLogin(String login) {
		TypedQuery<Funcionario> query = entityManager
		        .createQuery("SELECT c FROM Funcionario c where lower(c.nmLogin) = lower('"
		                + login + "')", Funcionario.class);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	public Funcionario getFuncionario(Long id) {
		return searchByKey(Funcionario.class, id);
	}
	
	/**
	 * @author Joelton
	 * @since 05-05-2016
	 * @return funcionarios ativos
	 */
	@SuppressWarnings("unchecked")
	public List<Funcionario> getListaFuncionario(Integer inGrupoFuncionario) {

		String sql = "SELECT func "
        		+ " FROM Funcionario func "
        		+ " where func.dtExclusao is null "
        		+ (inGrupoFuncionario != null ? " AND func.inGrupoFuncionario = "+inGrupoFuncionario : "")
        		+ " order by func.nmFuncionario ";
		 
		Query query = entityManager
		        .createQuery(sql);

		List<Funcionario> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<Funcionario> getListaFuncionarioInativo() {

		Query query = entityManager
		        .createQuery("SELECT func FROM Funcionario func where func.dtExclusao is not null order by func.nmFuncionario ");

		List<Funcionario> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;

	}
	
	@SuppressWarnings("unchecked")
	public List<EspecialidadeDTO> getEspecialidadesProfissional(Long idFuncionario) {
		
		String query = "SELECT esp.idespecialidade, esp.nmespecialidade FROM realvida.tbespprofissional esppro JOIN realvida.tbespecialidade esp ON "+
		        		 	" esppro.idespecialidade = esp.idespecialidade "+
		        		 	" WHERE esppro.idfuncionario = " + idFuncionario + " AND dtexclusao IS NULL";
		
		List<EspecialidadeDTO> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(EspecialidadeDTO.class)).list();
		
		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		
		return result;
		
	}
	@SuppressWarnings("unchecked")
	public List<ServicoExame> getAtendimentomedicoservicosexameView(Long idEspecialidade ) {
		String query = "SELECT atenview.* FROM realvida.tbservicoexame atenview WHERE atenview.idespecialidade IN (100, 101, 33, 70, "+ idEspecialidade+")";
				
		List<ServicoExame> result = findByNativeQuery(query)
		        .unwrap(SQLQuery.class)
		        .addScalar("idEspecialidade", LongType.INSTANCE)
		        .addScalar("nmEspecialidade", StringType.INSTANCE)
		        .addScalar("idServico", LongType.INSTANCE)
		        .addScalar("nmServico", StringType.INSTANCE)
		        .addScalar("ranking", LongType.INSTANCE)
		        .addScalar("tipo", StringType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(ServicoExame.class)).list();
			
			if (result.isEmpty()) {
				throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
			}
			
			return result;


	}
	
	@SuppressWarnings("unchecked")
	public List<AtividadeAgendamentoDTO> getAtividadeAgendamento(Date dataInicio, Date dataFim ) {
//		String query = "select t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil, count(idagendamento) as qtdagendamento from( "
//							+" select f.nmfuncionario, f.nmcargo, f.nmlogin, p.nmperfil, ag.idagendamento, ag.dtagendamento "
//							+" from realvida.tbfuncionario f  "
//							+" left join realvida.tbperfil p on f.nmlogin = p.nmlogin "
//							+" left join realvida.tbagendamento ag on f.idfuncionario = ag.idoperadorcadastro and ag.dtinclusaolog between :dataInicio  and :dataFim "
//							+" where f.dtexclusao is null  "
//							+" order by f.nmfuncionario "
//						+" )t1 "
//						+" group by t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil "
//						+ " having count(idagendamento) > 0 "
//						+" order by t1.qtdagendamento desc "; 
		
//		String query = " select nmfuncionario, nmcargo, nmlogin, nmperfil, qtdagendamento, idfuncionario from( "
//				+" select t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil, count(idagendamento) as qtdagendamento, t1.idfuncionario "
//				+" from(  select f.nmfuncionario, f.nmcargo, f.nmlogin, p.nmperfil, ag.idagendamento, ag.dtagendamento, f.idfuncionario  "
//				+" from realvida.tbfuncionario f   left join realvida.tbperfil p on f.nmlogin = p.nmlogin  "
//				+" left join realvida.tbagendamento ag on f.idfuncionario = ag.idoperadorcadastro "
//				+" and cast(ag.dtinclusaolog as date) between :dataInicio  and :dataFim  where f.dtexclusao is null   order by f.nmfuncionario  )t1  "
//				+" group by t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil, t1.idfuncionario  having count(idagendamento) > 0) as sumario  order by qtdagendamento desc ";
		
		String query = "select t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil, count(idagendamento) as qtdagendamento, t1.idfuncionario, "
		+" (select count(*) from realvida.tbagendamento agen" 
		+" where cast(agen.dtinclusaolog as date) between :dataInicio  and :dataFim "
		  +" and agen.dtexclusao is not null" 
		  +" and agen.idoperadorcadastro  = t1.idfuncionario"
		  +" and agen.idoperadorexclusao = t1.idfuncionario) as qtdexclusao"	       
	 +" from( " 
		 +" select distinct f.nmfuncionario, f.nmcargo, f.nmlogin, p.nmperfil, ag.idagendamento, ag.dtagendamento, f.idfuncionario"		 		 	 
		 +" from realvida.tbfuncionario f"   
		 +" left join realvida.tbperfil p on f.nmlogin = p.nmlogin"  
		 +" left join realvida.tbagendamento ag on f.idfuncionario = ag.idoperadorcadastro" 		   
		 +" where f.dtexclusao is null"
		 +" and cast(ag.dtinclusaolog as date) between :dataInicio  and :dataFim"
	 +" )t1"  
	 +" group by t1.nmfuncionario, t1.nmcargo, t1.nmlogin, t1.nmperfil, t1.idfuncionario " 
	 +" having count(idagendamento) > 0"
	 +" order by count(idagendamento) desc";
				
		List<AtividadeAgendamentoDTO> result = findByNativeQuery(query).setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE)
		        .unwrap(SQLQuery.class)
		        .addScalar("nmFuncionario", StringType.INSTANCE)
		        .addScalar("nmCargo", StringType.INSTANCE)
		        .addScalar("nmLogin", StringType.INSTANCE)
		        .addScalar("nmPerfil", StringType.INSTANCE)
		        .addScalar("qtdAgendamento", IntegerType.INSTANCE)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("qtdExclusao", IntegerType.INSTANCE)
		        .setResultTransformer(
		                Transformers.aliasToBean(AtividadeAgendamentoDTO.class)).list();
			
			if (result.isEmpty()) {
				return null;
			}
			
			return result;


	}
	
	@SuppressWarnings("unchecked")
	public List<AtividadeAgendamentoDTO> getMedicosAtividadeAtendimento(Date dataInicio, Date dataFim ) {
		
		String query = " select t1.idfuncionario, t1.nmfuncionario, t1.idprofissionalref from ( "
				+ " select f.idfuncionario as idfuncionario, f.nmfuncionario as nmfuncionario, f.idfuncionarioreferencia as idprofissionalref, f.dtexclusao, sum(apr.qtdatendido) as totalatendido "
				+ " from realvida.tbfuncionario f "
				+ " inner join realvida.tbprofissional p on f.idfuncionario = p.idfuncionario "
				+ " inner join realvida.tbespprofissional esppro on p.idfuncionario = esppro.idfuncionario and esppro.idespecialidade <> 83 "
				+ " left join estatistica.tbatendimentoprofissional_result apr on apr.idprofissional = f.idfuncionario "
				+ " and apr.dtinclusao between :dataInicio and :dataFim and apr.idespecialidade <> 83 "
				+ " group by f.idfuncionario "
				+ " order by f.nmfuncionario ) t1 "
				+ " where (t1.dtexclusao is null or (t1.dtexclusao is not null and totalatendido > 0)) "; 
				
		List<AtividadeAgendamentoDTO> result = findByNativeQuery(query).setParameter("dataInicio", dataInicio, TemporalType.DATE).setParameter("dataFim", dataFim, TemporalType.DATE)
		        .unwrap(SQLQuery.class)
		         .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)		        
		        .addScalar("idProfissionalRef", LongType.INSTANCE)		      
		        .setResultTransformer(
		                Transformers.aliasToBean(AtividadeAgendamentoDTO.class)).list();
			
			if (result.isEmpty()) {
				return null;
			}
			
			return result;


	}
	
	public Profissional getProfissionalAgendamento(Long idProfissional) {
		try {
			Profissional result = entityManager.find(Profissional.class, idProfissional);
			return result;
		} catch(Exception e) {
			return null;
		}
	}
	
}
