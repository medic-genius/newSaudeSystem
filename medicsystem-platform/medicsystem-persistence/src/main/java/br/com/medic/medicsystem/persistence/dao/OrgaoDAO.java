package br.com.medic.medicsystem.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.TypedQuery;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Orgao;

@Named("orgao-dao")
@ApplicationScoped
public class OrgaoDAO extends RelationalDataAccessObject<Orgao> {

	public List<Orgao> getAllOrgaos() {

		TypedQuery<Orgao> query = entityManager.createQuery(
		        "SELECT c FROM Orgao c order by c.nmOrgao asc", Orgao.class);
		List<Orgao> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}
	
	public Orgao getOrgaobyCod( String codOrgao) {
		
		Integer codigoOrgao = Integer.parseInt(codOrgao);
		
		String queryStr = "SELECT c FROM Orgao c WHERE c.cdOrgao like '%" + codigoOrgao + "%'"
				+ " order by c.nmOrgao asc"; 
		
		TypedQuery<Orgao> query = entityManager.createQuery(queryStr , Orgao.class);
						
		try {
			Orgao result = query.getSingleResult();
			
			if(result != null)
				return result;
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
				
		return null;
	}
	

	public List<Orgao> getOrgaosByEsfera(Integer inEsfera) {

		TypedQuery<Orgao> query = entityManager.createQuery(
		        "SELECT c FROM Orgao c where c.dtExclusao is null and c.inEsfera = " + inEsfera 
		        + "order by c.nmOrgao asc", Orgao.class);
		List<Orgao> result = query.getResultList();

		if (result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}

		return result;
	}

}
