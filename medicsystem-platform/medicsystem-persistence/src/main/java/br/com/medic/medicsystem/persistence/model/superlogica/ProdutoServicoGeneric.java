package br.com.medic.medicsystem.persistence.model.superlogica;

import java.math.BigDecimal;

public class ProdutoServicoGeneric {
	
	private Integer id_produto_prd;
	private Integer nm_quantidade_comp;
	private Double vl_unitario_prd;
	
	public Integer getId_produto_prd() {
		return id_produto_prd;
	}
	public void setId_produto_prd(Integer id_produto_prd) {
		this.id_produto_prd = id_produto_prd;
	}
	public Integer getNm_quantidade_comp() {
		return nm_quantidade_comp;
	}
	public void setNm_quantidade_comp(Integer nm_quantidade_comp) {
		this.nm_quantidade_comp = nm_quantidade_comp;
	}
	public Double getVl_unitario_prd() {
		BigDecimal bigDecimal = new BigDecimal(vl_unitario_prd).setScale(2,BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}
	public void setVl_unitario_prd(Double vl_unitario_prd) {
		this.vl_unitario_prd = vl_unitario_prd;
	}

	
	

}
