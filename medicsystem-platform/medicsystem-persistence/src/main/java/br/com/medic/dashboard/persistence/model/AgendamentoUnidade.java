package br.com.medic.dashboard.persistence.model;

public class AgendamentoUnidade implements GraphWrapper {

	private String unidade;
	
	private String tipo;

	private Integer totalagendamento;
	
	private Integer totalbloqueado;
	
	private Integer totalliberado;

	private Integer totalatendido;
	
	private Integer totalfaltoso;
	
	private Integer totalpresente;
	

	public AgendamentoUnidade(){
		// TODO Auto-generated constructor stub
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Integer getTotalagendamento() {
		if (totalagendamento == null)
			totalagendamento = 0;
		
		return totalagendamento;
	}

	public void setTotalagendamento(Integer totalagendamento) {
		this.totalagendamento = totalagendamento;
	}

	public Integer getTotalbloqueado() {
		if (totalbloqueado == null)
			totalbloqueado = 0;
		
		return totalbloqueado;
	}

	public void setTotalbloqueado(Integer totalbloqueado) {
		this.totalbloqueado = totalbloqueado;
	}

	public Integer getTotalliberado() {
		if (totalliberado == null)
			totalliberado = 0;
		
		return totalliberado;
	}

	public void setTotalliberado(Integer totalliberado) {
		this.totalliberado = totalliberado;
	}

	public Integer getTotalatendido() {
		if (totalatendido == null)
			totalatendido = 0;
		
		return totalatendido;
	}

	public void setTotalatendido(Integer totalatendido) {
		this.totalatendido = totalatendido;
	}

	public Integer getTotalfaltoso() {
		if (totalfaltoso == null)
			totalfaltoso = 0;
		
		return totalfaltoso;
	}

	public void setTotalfaltoso(Integer totalfaltoso) {
		this.totalfaltoso = totalfaltoso;
	}

	public Integer getTotalpresente() {
		if (totalfaltoso == null)
			totalfaltoso = 0;
		
		return totalpresente;
	}

	public void setTotalpresente(Integer totalpresente) {
		this.totalpresente = totalpresente;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public String getGraphLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getGraphValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
