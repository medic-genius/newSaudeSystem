package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbcidade")
public class Cidade implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CIDADE_ID_SEQ")
	@SequenceGenerator(name = "CIDADE_ID_SEQ", sequenceName = "realvida.cidade_id_seq", allocationSize = 1)
	@Column(name = "idcidade")
	private Long id;

	@Basic
	@Column(name = "nmcidade", nullable = false)
	private String nmCidade;

	@Basic
	@Column(name = "nmuf", nullable = false)
	private String nmUF;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;

	@Basic
	@Column(name = "idcidadeaux")
	private Long idCidadeAux;

	@Basic
	@Column(name = "idcidadeodonto")
	private Long idCidadeOdonto;

	public Cidade() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Long getIdCidadeAux() {
		return idCidadeAux;
	}

	public void setIdCidadeAux(Long idCidadeAux) {
		this.idCidadeAux = idCidadeAux;
	}

	public Long getIdCidadeOdonto() {
		return idCidadeOdonto;
	}

	public void setIdCidadeOdonto(Long idCidadeOdonto) {
		this.idCidadeOdonto = idCidadeOdonto;
	}

	public String getNmCidade() {
		return nmCidade;
	}

	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}

	public String getNmUF() {
		return nmUF;
	}

	public void setNmUF(String nmUF) {
		this.nmUF = nmUF;
	}
}
