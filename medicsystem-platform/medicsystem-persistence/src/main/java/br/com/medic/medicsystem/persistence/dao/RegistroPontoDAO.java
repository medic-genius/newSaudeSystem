package br.com.medic.medicsystem.persistence.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.medic.medicsystem.persistence.dataaccess.impl.RelationalDataAccessObject;
import br.com.medic.medicsystem.persistence.dto.RegistroPontoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.RegistroPonto;

@Named("registroponto-dao")
@ApplicationScoped
public class RegistroPontoDAO extends RelationalDataAccessObject<RegistroPonto> {
	
	public RegistroPonto getRegistroPonto(Long id) {
		return searchByKey(RegistroPonto.class, id);
	}
	
	public RegistroPonto getUltimoRegistroPontoFuncionarioDia(Long idFuncionario, Long idUnidade){
		
		SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String dtRegFormat = dataFormatada.format(new Date());
		
		String queryStr = " FROM RegistroPonto rp "
				+ " WHERE rp.id = (SELECT MAX(id) FROM RegistroPonto WHERE funcionario.id = " + idFuncionario 
				+ " and dtRegistro = '" + dtRegFormat + "' and unidade.id = " + idUnidade + " )";

		try {
			
			TypedQuery<RegistroPonto> result = entityManager.createQuery(queryStr, RegistroPonto.class);
			RegistroPonto listresult = result.getSingleResult();
			
			if(listresult != null && listresult.getId() != null)
				return listresult;
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return null;
	}
	
	public List<RegistroPonto> getAllRegistroFuncionarioDia(Long idFuncionario, Date dtRegistro, Long idUnidade){
		
		SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String dtRegFormat = dataFormatada.format(dtRegistro);
		
		String queryStr = 
				" FROM RegistroPonto "
				+ " WHERE funcionario.id = " + idFuncionario 
				+ " and dtRegistro = '" + dtRegFormat + "'"
				+ " and unidade.id = " + idUnidade
				+ " ORDER BY id";
								
		TypedQuery<RegistroPonto> query = entityManager.createQuery(queryStr, RegistroPonto.class);
		
		try {
			return query.getResultList();
		} catch (NoResultException e) { 
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RegistroPontoDTO> getRegistroFuncionarioPeriodo(Long idUnidade, Date dtInicio, Date dtFim, Long idFuncionario) {
		SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String dtInicioFormatada = dataFormatada.format(dtInicio);
		String dtFimFormatada = dataFormatada.format(dtFim);
		
		String queryStr = "	select f.idfuncionario, f.nmfuncionario, rp.dtregistro "
//		+ "(SELECT DISTINCT "
//		+ "( SELECT array_to_string "
//		+ "(	ARRAY "
//		+ "( SELECT to_char(hrregistro, 'HH24:MI:SS') "
//		+ "FROM realvida.tbregistroponto "
//		+ "WHERE idpontounidade = rp.idpontounidade "
//		+ "and idfuncionario = f.idfuncionario "
//		+ "and dtregistro = rp.dtregistro "
//		+ "order by hrregistro "
//		+ "), cast(' | ' as text) "
//		+ "               ) AS array_to_string "
//		+ "       ) AS hrregistro) "

		+ " from realvida.tbfuncionario f "
		+ " inner join realvida.tbregistroponto rp on f.idfuncionario = rp.idfuncionario "
		+ " inner join realvida.tbunidade u on u.idunidade = rp.idpontounidade "
		+ " where rp.dtregistro between '" + dtInicioFormatada + "' and '"+ dtFimFormatada + "' "
		+ " and rp.idpontounidade = " + idUnidade 
		+ " and f.idfuncionario = " + idFuncionario
		+ " group by u.idunidade, f.idfuncionario, rp.dtregistro, rp.idpontounidade "
		+ " order by rp.dtregistro desc, f.nmfuncionario ";
		
		List<RegistroPontoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)
		        .addScalar("dtRegistro", DateType.INSTANCE)		        
		        .setResultTransformer(
		                Transformers.aliasToBean(RegistroPontoDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<RegistroPontoDTO> getFuncionarioPeriodo(Long idUnidade, Date dtInicio, Date dtFim, Integer grupoFuncionario) {
		SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String dtInicioFormatada = dataFormatada.format(dtInicio);
		String dtFimFormatada = dataFormatada.format(dtFim);
		
		String queryStr = 
		" select t1.idfuncionario, t1.nmfuncionario from ( "
		+ " select f.idfuncionario, f.nmfuncionario, f.ingrupofuncionario, rp.dtregistro "		
		+ " from realvida.tbfuncionario f "
		+ " inner join realvida.tbregistroponto rp on f.idfuncionario = rp.idfuncionario "
		+ " inner join realvida.tbunidade u on u.idunidade = rp.idpontounidade "
		+ " where rp.dtregistro between '" + dtInicioFormatada + "' and '" + dtFimFormatada + "'"
		+ " and rp.idpontounidade = " + idUnidade		
		+ " @tipoFuncionario "		
		+ " group by u.idunidade, f.idfuncionario, rp.dtregistro, rp.idpontounidade "
		+ " order by rp.dtregistro desc, f.nmfuncionario"
		+ " ) t1"
		+ " group by t1.idfuncionario, t1.nmfuncionario, t1.ingrupofuncionario"
		+ " order by t1.nmfuncionario";
		
		if(grupoFuncionario != null)
			queryStr = queryStr.replace("@tipoFuncionario", (" and f.ingrupofuncionario = " + grupoFuncionario));
		else
			queryStr = queryStr.replace("@tipoFuncionario", "");
		

		List<RegistroPontoDTO> result = findByNativeQuery(queryStr)
		        .unwrap(SQLQuery.class)
		        .addScalar("idFuncionario", LongType.INSTANCE)
		        .addScalar("nmFuncionario", StringType.INSTANCE)		        
		        .setResultTransformer(
		                Transformers.aliasToBean(RegistroPontoDTO.class)).list();

		if (result == null || result.isEmpty()) {
			throw new ObjectNotFoundException(NOT_RESULT_MESSAGE);
		}
		return result;
	}

}
