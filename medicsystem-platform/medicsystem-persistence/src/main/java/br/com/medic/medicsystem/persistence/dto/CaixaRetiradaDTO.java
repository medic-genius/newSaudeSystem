package br.com.medic.medicsystem.persistence.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaixaRetiradaDTO {
	
	private Date dtMovimentacaoCaixa; 
	private String nmFuncionario; 
	private Float vlMovimentacaoCaixa;
	private Boolean boExcluido; 
	private Long idMovimentacaoCaixa;
	private Long idCaixaConferido;

	public Date getDtMovimentacaoCaixa() {
		return dtMovimentacaoCaixa;
	}
	public void setDtMovimentacaoCaixa(Date dtMovimentacaoCaixa) {
		this.dtMovimentacaoCaixa = dtMovimentacaoCaixa;
	}
	public String getNmFuncionario() {
		return nmFuncionario;
	}
	public void setNmFuncionario(String nmFuncionario) {
		this.nmFuncionario = nmFuncionario;
	}
	public Float getVlMovimentacaoCaixa() {
		return vlMovimentacaoCaixa;
	}
	public void setVlMovimentacaoCaixa(Float vlMovimentacaoCaixa) {
		this.vlMovimentacaoCaixa = vlMovimentacaoCaixa;
	}
	public Boolean getBoExcluido() {
		return boExcluido;
	}
	public void setBoExcluido(Boolean boExcluido) {
		this.boExcluido = boExcluido;
	}
	public Long getIdMovimentacaoCaixa() {
		return idMovimentacaoCaixa;
	}
	public void setIdMovimentacaoCaixa(Long idMovimentacaoCaixa) {
		this.idMovimentacaoCaixa = idMovimentacaoCaixa;
	}
	
	public String getDtMovimentacaoCaixaFormatada() {
		if(dtMovimentacaoCaixa != null) {
			DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			return f.format(dtMovimentacaoCaixa);
		}
		return null;
	}
	public Long getIdCaixaConferido() {
		return idCaixaConferido;
	}
	public void setIdCaixaConferido(Long idCaixaConferido) {
		this.idCaixaConferido = idCaixaConferido;
	}
	
	
}
