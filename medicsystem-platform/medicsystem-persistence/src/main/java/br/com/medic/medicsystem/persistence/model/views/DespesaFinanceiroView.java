package br.com.medic.medicsystem.persistence.model.views;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.medic.medicsystem.persistence.model.CentroCusto;
import br.com.medic.medicsystem.persistence.model.ContaBancaria;
import br.com.medic.medicsystem.persistence.model.ContaContabil;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;
import br.com.medic.medicsystem.persistence.model.Fornecedor;

@Entity
@Table(catalog="financeiro", name="vwdespesa")
public class DespesaFinanceiroView {
	
	@Id
	@Column(name="iddespesa")
	private Long id;
	
	@Basic
	@Column(name="intipodespesa")
	private Short inTipo;
	
	@Basic
	@Column(name="nmdespesa")
	private String nmDespesa;
	
	@Basic
	@Column(name="dtvencimento")
	private Date dtVencimento;
	
	@Basic
	@Column(name="vldespesa")
	private Double vlDespesa;
	
	@Basic
	@Column(name="vlmulta")
	private Double vlMulta;
	
	@Basic
	@Column(name="vljuros")
	private Double vlJuros;
	
	@Basic
	@Column(name="vldesconto")
	private Double vlDesconto;
	
	@Basic
	@Column(name="instatus")
	private Short inStatus;
	
	@Basic
	@Column(name="nmobservacao")
	private String nmObservacao;
	
	@Basic
	@Column(name="dtcompetencia")
	private Date dtCompetencia;
	
	@Basic
	@Column(name="dtexclusao")
	private Date dtExclusao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idempresafinanceirostart")
	private EmpresaFinanceiro empresaFinanceiroSolicitante;

	@ManyToOne
	@JoinColumn(name = "idempresafinanceiroend")
	private EmpresaFinanceiro empresaFinanceiroPagante;
	
	@ManyToOne
	@JoinColumn(name = "idcontabancaria")
	private ContaBancaria contaBancaria;
	
	@ManyToOne
	@JoinColumn(name = "idfornecedor")
	private Fornecedor fornecedor;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idcontacontabil")
	private ContaContabil contaContabil;
	
	@ManyToOne
	@JoinColumn(name = "idcentrocusto")
	private CentroCusto centroCusto;

	@Basic
	@Column(name="idoperadorcadastro")
	private Long idOperadorCadastro;
	
	@Basic
	@Column(name="idoperadoralteracao")
	private Long idOperadorAlteracao;
	
	@Basic
	@Column(name="idoperadorexclusao")
	private Long idOperadorExclusao;

	@Basic
	@Column(name="dtpagamento")
	private Date dtPagamento;
	
	@Basic
	@Column(name="vlpago")
	private Double vlPago;
	
	@Basic
	@Column(name="nrordemparcela")
	private Long nrOrdemParcela;
	
	@Basic
	@Column(name="nrquantidadeparcelas")
	private Long nrQuantidadeParcelas;

	@Basic
	@Column(name="nrdocumento")
	private String nrDocumento;
	
	@Basic
	@Column(name="nrservico")
	private String nrServico;
	
	@Basic
	@Column(name="informapagamento")
	private Long inFormaPagamento;
	
	@Basic
	@Column(name="idtransacaobancaria")
	private Long idTransacaoBancaria;
	
	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;
	
	@JsonIgnore
	@Transient
	private String vlDespesaFormatado;
	
	@JsonIgnore
	@Transient
	private String vlPagoFormatado;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Short getInTipo() {
		return inTipo;
	}
	public void setInTipo(Short inTipo) {
		this.inTipo = inTipo;
	}
	public String getNmDespesa() {
		return nmDespesa.toUpperCase();
	}
	public void setNmDespesa(String nmDespesa) {
		this.nmDespesa = nmDespesa;
	}
	
	public Date getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public Double getVlDespesa() {
		return vlDespesa;
	}
	public void setVlDespesa(Double vlDespesa) {
		this.vlDespesa = vlDespesa;
	}
	public Double getVlMulta() {
		return vlMulta;
	}
	public void setVlMulta(Double vlMulta) {
		this.vlMulta = vlMulta;
	}
	public Double getVlJuros() {
		return vlJuros;
	}
	public void setVlJuros(Double vlJuros) {
		this.vlJuros = vlJuros;
	}
	public Double getVlDesconto() {
		return vlDesconto;
	}
	public void setVlDesconto(Double vlDesconto) {
		this.vlDesconto = vlDesconto;
	}
	public Short getInStatus() {
		return inStatus;
	}
	public void setInStatus(Short inStatus) {
		this.inStatus = inStatus;
	}
	public String getNmObservacao() {
		return nmObservacao;
	}
	public void setNmObservacao(String nmObservacao) {
		this.nmObservacao = nmObservacao;
	}
	public Date getDtCompetencia() {
		return dtCompetencia;
	}
	public void setDtCompetencia(Date dtCompetencia) {
		this.dtCompetencia = dtCompetencia;
	}
	public Date getDtExclusao() {
		return dtExclusao;
	}
	public void setDtExclusao(Date dtExclusao) {
		this.dtExclusao = dtExclusao;
	}

	public EmpresaFinanceiro getEmpresaFinanceiroSolicitante() {
		return empresaFinanceiroSolicitante;
	}
	
	public void setEmpresaFinanceiroSolicitante(EmpresaFinanceiro empresaFinanceiroSolicitante) {
		this.empresaFinanceiroSolicitante = empresaFinanceiroSolicitante;
	}

	public EmpresaFinanceiro getEmpresaFinanceiroPagante() {
		return empresaFinanceiroPagante;
	}
	
	public void setEmpresaFinanceiroPagante(EmpresaFinanceiro empresaFinanceiroPagante) {
		this.empresaFinanceiroPagante = empresaFinanceiroPagante;
	}

	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}

	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public Long getIdOperadorCadastro() {
		return idOperadorCadastro;
	}
	public void setIdOperadorCadastro(Long idOperadorCadastro) {
		this.idOperadorCadastro = idOperadorCadastro;
	}
	public Long getIdOperadorAlteracao() {
		return idOperadorAlteracao;
	}
	public void setIdOperadorAlteracao(Long idOperadorAlteracao) {
		this.idOperadorAlteracao = idOperadorAlteracao;
	}
	public Long getIdOperadorExclusao() {
		return idOperadorExclusao;
	}
	public void setIdOperadorExclusao(Long idOperadorExclusao) {
		this.idOperadorExclusao = idOperadorExclusao;
	}
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public Double getVlPago() {
		return vlPago;
	}
	public void setVlPago(Double vlPago) {
		this.vlPago = vlPago;
	}
	public Long getNrOrdemParcela() {
		return nrOrdemParcela;
	}
	public void setNrOrdemParcela(Long nrOrdemParcela) {
		this.nrOrdemParcela = nrOrdemParcela;
	}
	public Long getNrQuantidadeParcelas() {
		return nrQuantidadeParcelas;
	}
	public void setNrQuantidadeParcelas(Long nrQuantidadeParcelas) {
		this.nrQuantidadeParcelas = nrQuantidadeParcelas;
	}
	public String getNrDocumento() {
		return nrDocumento;
	}
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	public String getNrServico() {
		return nrServico;
	}
	public void setNrServico(String nrServico) {
		this.nrServico = nrServico;
	}
	public Long getInFormaPagamento() {
		return inFormaPagamento;
	}
	public void setInFormaPagamento(Long inFormaPagamento) {
		this.inFormaPagamento = inFormaPagamento;
	}
	public Long getIdTransacaoBancaria() {
		return idTransacaoBancaria;
	}
	public void setIdTransacaoBancaria(Long idTransacaoBancaria) {
		this.idTransacaoBancaria = idTransacaoBancaria;
	}
	@JsonProperty
	public String getVlDespesaFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);

		if (getVlDespesa() != null)
			return currencyFormatter.format(getVlDespesa());
		return null;
	}
	
	@JsonProperty
	public String getVlPagoFormatado() {
		Locale locale = new Locale("pt", "BR");
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);

		if (getVlPago() != null)
			return currencyFormatter.format(getVlPago());
		return null;
	}
	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}
	
}
