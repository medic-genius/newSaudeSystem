package br.com.medic.medicsystem.persistence.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Time;
import java.sql.Timestamp;


@Entity
@Table(catalog = "realvida", name = "tbatestadomedico")
public class AtestadoMedico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATESTADOMEDICO_ID_SEQ")
	@SequenceGenerator(name = "ATESTADOMEDICO_ID_SEQ", sequenceName = "realvida.atestadomedico_id_seq")
	@Column(name = "idatestadomedico")
	private Long id;

	@Basic
	@Column(name = "dtatualizacaolog")
	private Timestamp dtAtualizacaoLog;

	@Basic
	@Column(name = "dtinclusaolog")
	private Timestamp dtInclusaoLog;
	
	@Basic
	@Column(name = "hrfim")
	private Time hrFim;
	
	@Basic
	@Column(name = "hrinicio")
	private Time hrInicio;

	@Basic
	@Column(name = "intipoatestado")
	private Integer inTipoAtestado;
	
	@Basic
	@Column(name = "nrquantdias")
	private Integer nrQuantDias;

	//bi-directional many-to-one association to Tbatendimento
	@ManyToOne
	@JoinColumn(name="idatendimento")
	private Atendimento atendimento;

	public AtestadoMedico() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDtAtualizacaoLog() {
		return dtAtualizacaoLog;
	}

	public void setDtAtualizacaoLog(Timestamp dtAtualizacaoLog) {
		this.dtAtualizacaoLog = dtAtualizacaoLog;
	}

	public Timestamp getDtInclusaoLog() {
		return dtInclusaoLog;
	}

	public void setDtInclusaoLog(Timestamp dtInclusaoLog) {
		this.dtInclusaoLog = dtInclusaoLog;
	}

	public Time getHrFim() {
		return hrFim;
	}

	public void setHrFim(Time hrFim) {
		this.hrFim = hrFim;
	}

	public Time getHrInicio() {
		return hrInicio;
	}

	public void setHrInicio(Time hrInicio) {
		this.hrInicio = hrInicio;
	}

	public Integer getInTipoAtestado() {
		return inTipoAtestado;
	}

	public void setInTipoAtestado(Integer inTipoAtestado) {
		this.inTipoAtestado = inTipoAtestado;
	}

	public Integer getNrQuantDias() {
		return nrQuantDias;
	}

	public void setNrQuantDias(Integer nrQuantDias) {
		this.nrQuantDias = nrQuantDias;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

}