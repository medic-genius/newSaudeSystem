package br.com.medic.medicsystem.persistence.model.views;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "realvida", name = "tbempresacliente_tbcontratocliente_view")
public class EmpresaClienteContratoClienteView {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Basic
	@Column(name = "idcontratocliente")
	private Long idContratoCliente;
	
	@Basic
	@Column(name = "idempresacliente")
	private Long idEmpresaCliente;
	
	@Basic
	@Column(name = "idcontrato")
	private Long idContrato;
	
	@Basic
	@Column(name = "nrcontrato")
	private String nrContrato;
	
	@Basic
	@Column(name = "nmcliente")
	private String nmCliente;

	@Basic
	@Column(name = "nmplano")
	private String nmPlano;
	
	@Basic
	@Column(name = "boempresacliente")
	private Boolean boEmpresaCliente;
	
	@Basic
	@Column(name = "bobloqueado")
	private Boolean boBloqueado;
	
	@Basic
	@Column(name = "nrcpf")
	private String nrCPF;
	
	@Basic
	@Column(name = "idcliente")
	private Long idCliente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdContratoCliente() {
		return idContratoCliente;
	}

	public void setIdContratoCliente(Long idContratoCliente) {
		this.idContratoCliente = idContratoCliente;
	}

	public Long getIdEmpresaCliente() {
		return idEmpresaCliente;
	}

	public void setIdEmpresaCliente(Long idEmpresaCliente) {
		this.idEmpresaCliente = idEmpresaCliente;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNrContrato() {
		return nrContrato;
	}

	public void setNrContrato(String nrContrato) {
		this.nrContrato = nrContrato;
	}

	public String getNmCliente() {
		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {
		this.nmCliente = nmCliente;
	}

	public String getNmPlano() {
		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {
		this.nmPlano = nmPlano;
	}

	public Boolean getBoEmpresaCliente() {
		return boEmpresaCliente;
	}

	public void setBoEmpresaCliente(Boolean boEmpresaCliente) {
		this.boEmpresaCliente = boEmpresaCliente;
	}

	public Boolean getBoBloqueado() {
		return boBloqueado;
	}

	public void setBoBloqueado(Boolean boBloqueado) {
		this.boBloqueado = boBloqueado;
	}

	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
	
	
	
}
