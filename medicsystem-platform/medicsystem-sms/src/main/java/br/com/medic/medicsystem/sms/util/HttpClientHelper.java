package br.com.medic.medicsystem.sms.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import br.com.medic.medicsystem.sms.bean.Part;
import br.com.medic.medicsystem.sms.bean.ProxyConfiguration;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.bean.SimpleMessage;
import br.com.medic.medicsystem.sms.enumerator.ProtocolEnum;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

/**
 * Classe responsavel por possuir ferramentas http.
 * 
 * @author Phillip Furtado
 */
public class HttpClientHelper {

	private String host;
	private String uri;
	private int port;
	private ProtocolEnum protocol;
	private ProxyConfiguration proxy;
	private boolean useSSL;
	private CloseableHttpClient httpClient;

	HttpHost target;

	/**
	 * Construtor default
	 */
	public HttpClientHelper() {
		this.setUseSSL(false);
		this.httpClient = HttpClients.createDefault();
	}

	/**
	 * Construtor alternativo que permite fornecer uma instancia da classe
	 * HttpClient.
	 * 
	 * @param httpClient
	 */
	public HttpClientHelper(CloseableHttpClient httpClient) {
		this.setUseSSL(false);
		this.httpClient = httpClient;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param protocol
	 *            the protocol to set
	 */
	public void setProtocol(ProtocolEnum protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return the protocol
	 */
	public ProtocolEnum getProtocol() {
		return protocol;
	}

	/**
	 * @param proxy
	 *            the proxy to set
	 */
	public void setProxy(ProxyConfiguration proxy) {
		this.proxy = proxy;
	}

	/**
	 * @return the proxy
	 */
	public ProxyConfiguration getProxy() {
		return proxy;
	}

	/**
	 * Seta a porta e o protocolo referente ao uso (ou nao) de ssl.
	 * 
	 * @param useSSL
	 *            the useSSL to set
	 */
	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;

		if (this.useSSL) {
			this.port = 443;
			this.setProtocol(ProtocolEnum.SECURE_PROTOCOL);
		} else {
			this.port = 80;
			this.setProtocol(ProtocolEnum.PROTOCOL);
		}
	}

	/**
	 * @return the useSSL
	 */
	public boolean isUseSSL() {
		return useSSL;
	}

	/**
	 * Cria, executa e retorna o resultado de uma requicao multipart.
	 * 
	 * @param parts
	 * @return
	 * @throws ClientHumanException
	 */
	public List<Response> multipartRequest(Part... parts)
			throws ClientHumanException {
		HttpPost post = new HttpPost();

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		for (Part part : parts) {
			builder.addPart(part.getLabel(), part.getBody());
		}

		HttpEntity reqEntity = builder.build();
		post.setEntity(reqEntity);

		return this.simpleRequest(post);
	}

	/**
	 * Cria, executa e retorna o resultado de uma requicao simples.
	 * 
	 * @param post
	 * @return
	 * @throws ClientHumanException
	 */
	public List<Response> simpleRequest(HttpPost post)
			throws ClientHumanException {
		InputStream stream = request(post);
		List<Response> response = getResponse(stream);
		return response;
	}

	/**
	 * Busca por todas as mensagens recebidas(MO) da conta no gateway .
	 * 
	 * @param post
	 * @return
	 * @throws ClientHumanException
	 */
	public List<SimpleMessage> requestAndGetMessages(HttpPost post)
			throws ClientHumanException {
		InputStream stream = request(post);
		List<SimpleMessage> messages = ResponseParser.getMessages(stream);
		return messages;
	}

	/**
	 * Faz a requisição para o gateway de sms e retorna uma stream com o
	 * conteudo retornado.
	 * 
	 * @param post
	 * @return stream com os dados retornados pela consulta ao gateway
	 */
	private InputStream request(HttpPost post) throws ClientHumanException {

		try {
			target = new HttpHost(host, port, protocol.getValue());
			if(this.proxy != null)
				post.setConfig(configureProxy());
			post.setURI(new URI(this.uri));

			CloseableHttpResponse response = httpClient.execute(target, post);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return response.getEntity().getContent();
			} else {
				throw new ClientHumanException(response.getStatusLine()
						.getReasonPhrase());
			}
		} catch (IOException e) {
			throw new ClientHumanException(e);
		} catch (URISyntaxException e) {
			throw new ClientHumanException(e);
		}
	}

	/**
	 * Configura uma conexao com o host
	 * 
	 * @param client
	 */
	private RequestConfig configureProxy() {

		RequestConfig config = null;

		if (this.proxy != null && !StringUtils.isEmpty(this.proxy.getHost())) {
			HttpHost proxy = new HttpHost(this.proxy.getHost(),
					this.proxy.getPort());

			config = RequestConfig.custom().setProxy(proxy).build();

			if (!StringUtils.isEmpty(this.proxy.getUsername())
					&& !StringUtils.isEmpty(this.proxy.getPassword())) {

				CredentialsProvider credsProvider = new BasicCredentialsProvider();
				credsProvider.setCredentials(
						new AuthScope(this.proxy.getHost(), this.proxy
								.getPort()),
						new UsernamePasswordCredentials(this.proxy
								.getUsername(), this.proxy.getPassword()));
				httpClient = HttpClients.custom()
						.setDefaultCredentialsProvider(credsProvider).build();

			}

		}

		return config;
	}

	/**
	 * Busca os dados de resposta e formata em uma lista.
	 * 
	 * @param responseBody
	 * @return
	 * @throws ClientHumanException
	 */
	private List<Response> getResponse(InputStream responseBody)
			throws ClientHumanException {
		return ResponseParser.getResponses(responseBody);
	}

}
