package br.com.medic.medicsystem.sms.exception;

/**
 * Excecao personalizada da API.
 * 
 * @author Phillip Furtado
 */
public class ClientHumanException extends Exception {
	private static final long serialVersionUID = 4631279046552827396L;

	public ClientHumanException() {
	}

	public ClientHumanException(String message) {
		super(message);
	}

	public ClientHumanException(Exception ex) {
		super(ex);
	}

}