package br.com.medic.medicsystem.sms.util;

public class SMSFormatterUtils {
	private static final String BR_CODE = "55";

	private static final String MAO_CODE = "92";

	private static final String NINE_DIGIT = "9";
	
	public static String getFormattedMessage(String message, Object... args) {
		return String.format(message, args);
	}
	
	public static String getFirstName(String name) {
		if(name != null) {
			name = name.trim();
			if (!name.isEmpty()) {
				if(name.indexOf(" ") != -1) {
					return name.split(" ")[0];
				}
			}
		}
		return name;
	}
	
	public static String phoneNumberCleanup(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");
			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {
				if (numberStr.length() == 8) {
					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					return BR_CODE + MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					return BR_CODE + numberStr;
				}
			}
		}
		return null;
	}
	
	public static String phoneNumberCleanupBarateiro(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");
			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {
				if (numberStr.length() == 8) {
					return  MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					return  MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					return MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					return  numberStr;
				}
			}
		}
		return null;
	}
	
	public static String getNumbersOnly(String str) {
		if(str != null) {
			str = str.replaceAll("[^0-9]", "");
			str.trim();
		}
		return str;
	}
}
