package br.com.medic.medicsystem.sms.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;

import br.com.medic.medicsystem.sms.enumerator.LayoutTypeEnum;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe responsavel por armazenar uma lista de sms para envio para o gateway
 * de sms.
 * 
 * @author Phillip Furtado
 */
public class ListResource extends MultipleMessage {

	/**
	 * Lista de sms a ser enviada.
	 */
	private String content;

	/**
	 * Construtor da classe informando a lista de sms e o tipo de layout da
	 * mesma.
	 * 
	 * @param list
	 * @param type
	 */
	@JsonCreator
	public ListResource(@JsonProperty("content") String content, @JsonProperty("type") Character type) {
		super(LayoutTypeEnum.get(type));
		this.content = content;
	}

	@Override
	public String getContent() throws ClientHumanException {
		return this.content;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
