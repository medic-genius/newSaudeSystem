package br.com.medic.medicsystem.sms.service.base;

import java.util.List;

import br.com.medic.medicsystem.sms.bean.Message;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

/**
 * Interface base para servicos que envolve envio de mensagens sms.
 * 
 * @author Phillip Furtado
 */
public interface IBaseService {
	/**
	 * Envia uma requisicao ao servidor para envio de mensagem sms.
	 * 
	 * @param message
	 * @return
	 * @throws ClientHumanException
	 */
	public List<Response> send(Message message) throws ClientHumanException;
}
