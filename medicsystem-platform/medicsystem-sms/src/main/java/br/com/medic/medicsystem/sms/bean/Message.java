package br.com.medic.medicsystem.sms.bean;

import br.com.medic.medicsystem.sms.enumerator.CallbackTypeEnum;

/**
 * Classe abstrata utilizada como molde para construcao de todo o tipo de classe
 * para envio de sms.
 * 
 * @author Phillip Furtado
 */
public abstract class Message {
	/**
	 * O callback que a mensagem (ou a lista de mensagens) ira ter.
	 */
	private CallbackTypeEnum callback;

	/**
	 * Construtor da classe setando o callback default (Inativo).
	 */
	public Message() {
		callback = CallbackTypeEnum.INACTIVE;
	}

	/**
	 * Retorna o tipo de callback
	 * 
	 * @return
	 */
	public CallbackTypeEnum getCallback() {
		return callback;
	}

	/**
	 * Seta o tipo de callback
	 * 
	 * @param callback
	 */
	public void setCallback(CallbackTypeEnum callback) {
		this.callback = callback;
	}
}
