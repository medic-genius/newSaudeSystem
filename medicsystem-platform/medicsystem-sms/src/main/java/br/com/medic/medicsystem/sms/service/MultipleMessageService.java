package br.com.medic.medicsystem.sms.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

import br.com.medic.medicsystem.sms.bean.FileResource;
import br.com.medic.medicsystem.sms.bean.ListResource;
import br.com.medic.medicsystem.sms.bean.Message;
import br.com.medic.medicsystem.sms.bean.MultipleMessage;
import br.com.medic.medicsystem.sms.bean.Part;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.enumerator.LayoutTypeEnum;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.base.BaseService;
import br.com.medic.medicsystem.sms.service.base.IMultipleBaseService;
import br.com.medic.medicsystem.sms.util.HttpClientHelper;

/**
 * Classe responsavel pelos servicos destinados ao envio de multiplas mensagens
 * de sms para o gateway.
 * 
 * @author Phillip Furtado
 */
@ApplicationScoped
public class MultipleMessageService extends BaseService implements
		IMultipleBaseService {

	private static final String SEND_MULTIPLE = "sendMultiple";
	private static final String CHECK_MULTIPLE = "checkMultiple";
	public static final String PARAM_FILE = "file";
	public static final String PARAM_LIST = "list";
	public static final String PARAM_TYPE = "type";
	public static final String PARAM_ID_LIST = "idList";

	/**
	 * Construtor da classe que permite fornecer a conta, senha e a instancia da
	 * conexao para autenticacao no gateway.
	 * 
	 * @param account
	 * @param password
	 */
	public MultipleMessageService() {
		super();
		this.getHttp().setHost(GATEWAY_HOST);
		this.getHttp().setUri(GATEWAY_URI);
		this.getHttp().setPort(GATEWAY_PORT);
	}

	/**
	 * Construtor da classe que permite fornecer a conta, a senha e o helper de
	 * conexao para autenticacao no gateway.
	 * 
	 * @param account
	 * @param password
	 * @param httpHelper
	 */
	public MultipleMessageService(HttpClientHelper httpHelper) {
		super(httpHelper);
	}

	@Override
	public List<Response> send(Message message) throws ClientHumanException {
		String value = System.getenv("SEND_SMS");
		
		if (value != null && value.equals("true")) {
			
			MultipleMessage multipleMessage = (MultipleMessage) message;
			validateAccountAndPassword();
	
			validateSend(multipleMessage);
	
			if (multipleMessage instanceof ListResource) {
				HttpPost post = new HttpPost();
	
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	
				nvps.add(new BasicNameValuePair(PARAM_DISPATCH, SEND_MULTIPLE));
				nvps.add(new BasicNameValuePair(PARAM_ACCOUNT, this.getAccount()));
				nvps.add(new BasicNameValuePair(PARAM_CODE, this.getPassword()));
				nvps.add(new BasicNameValuePair(PARAM_CALLBACK_OPTION, String
						.valueOf(message.getCallback().getId())));
				nvps.add(new BasicNameValuePair(PARAM_TYPE, multipleMessage
						.getType().getType().toString()));
				nvps.add(new BasicNameValuePair(PARAM_LIST, multipleMessage
						.getContent()));
	
				try {
					post.setEntity(new UrlEncodedFormEntity(nvps));
				} catch (UnsupportedEncodingException e) {
					throw new ClientHumanException(e);
				}
	
				return this.sendRequest(post);
			} else if (multipleMessage instanceof FileResource) {
				FileResource fileResource = (FileResource) multipleMessage;
	
				Part filePart;
				filePart = new Part(PARAM_FILE,
						new FileBody(fileResource.getFile()));
	
				Part[] parts = {
						new Part(PARAM_DISPATCH, new StringBody(SEND_MULTIPLE,
								ContentType.TEXT_PLAIN)),
						new Part(PARAM_ACCOUNT, new StringBody(this.getAccount(),
								ContentType.TEXT_PLAIN)),
						new Part(PARAM_CODE, new StringBody(this.getPassword(),
								ContentType.TEXT_PLAIN)),
						new Part(PARAM_CALLBACK_OPTION, new StringBody(
								multipleMessage.getCallback().toString(),
								ContentType.TEXT_PLAIN)),
						new Part(PARAM_TYPE, new StringBody(multipleMessage
								.getType().getType().toString(),
								ContentType.TEXT_PLAIN)), filePart };
	
				return this.sendRequest(parts);
	
			} else {
				throw new ClientHumanException("Message is invalid.");
			}
		}
		return null;
	}

	@Override
	public List<Response> query(String[] ids) throws ClientHumanException {
		if ((ids == null) || (ids.length == 0)) {
			throw new ClientHumanException("Id is empty.");
		} else if (ids.length == 1) {
			SimpleMessageService sMessageService = new SimpleMessageService();
			return sMessageService.query(ids[0]);
		}
		validateAccountAndPassword();

		HttpPost post = new HttpPost();

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		nvps.add(new BasicNameValuePair(PARAM_ACCOUNT, this.getAccount()));
		nvps.add(new BasicNameValuePair(PARAM_CODE, this.getPassword()));
		nvps.add(new BasicNameValuePair(PARAM_DISPATCH, CHECK_MULTIPLE));

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ids.length; i++) {
			if (i == (ids.length - 1)) {
				sb.append(ids[i]);
			} else {
				sb.append(ids[i]).append(";");
			}
		}
		nvps.add(new BasicNameValuePair(PARAM_ID_LIST, sb.toString()));

		try {
			post.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			throw new ClientHumanException(e);
		}

		return this.sendRequest(post);
	}

	/**
	 * Valida os dados para envio de mensagem multipla
	 * 
	 * @param message
	 * @return
	 * @throws ClientHumanException
	 */
	private boolean validateSend(MultipleMessage message)
			throws ClientHumanException {
		String content = message.getContent();
		if (StringUtils.isEmpty(content)) {
			throw new ClientHumanException(
					"Was not informed to send a list of messages.");
		}

		String[] line = null;
		if (content.contains("\r\n")) {
			line = content.split("\r\n");
		} else {
			line = content.split("\n");
		}

		for (int i = 0; i < line.length; i++) {
			validateMessage(message.getType(), line[i]);
		}
		return true;
	}

	/**
	 * Valida parametros gerais da mensagem (campos obrigatorios, tamanho do
	 * texto, etc).
	 * 
	 * @throws ClientHumanException
	 */
	private void validateMessage(LayoutTypeEnum type, String linha)
			throws ClientHumanException {
		String[] fields = linha.split(";");

		if (LayoutTypeEnum.TYPE_A.equals(type)) {
			if (fields.length != 2) {
				throw new ClientHumanException("File format invalid.");
			}

			this.validateMessage(fields[0], fields[1]);
		} else if (LayoutTypeEnum.TYPE_B.equals(type)) {
			if (fields.length != 3) {
				throw new ClientHumanException("File format invalid.");
			}

			this.validateMessage(fields[0], fields[1], fields[2]);
		} else if (LayoutTypeEnum.TYPE_C.equals(type)) {
			if (fields.length != 3) {
				throw new ClientHumanException("File format invalid.");
			}

			this.validateMessage(fields[0], fields[1], fields[2], null);
		} else if (LayoutTypeEnum.TYPE_D.equals(type)) {
			if (fields.length != 4) {
				throw new ClientHumanException("File format invalid.");
			}

			this.validateMessage(fields[0], fields[1], fields[2], fields[3]);
		} else if (LayoutTypeEnum.TYPE_E.equals(type)) {
			if (fields.length != 5) {
				throw new ClientHumanException("File format invalid.");
			}

			this.validateMessage(fields[0], fields[1], fields[2], fields[3],
					fields[4]);
		} else {
			throw new ClientHumanException("Type of file format invalid.");
		}
	}
}
