package br.com.medic.medicsystem.sms.enumerator;

public enum TypeService {
	LONGCODE("LONGCODE"),
	SHORTCODE_1W("SHORTCODE 1WAY"),
	SHORTCODE_2W("SHORTCODE 2WAY"),
	VOZ("VOZ");
	
	private String name;
	
	private TypeService(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}
}
