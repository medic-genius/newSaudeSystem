package br.com.medic.medicsystem.sms.service.base;

import java.util.List;

import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

/**
 * Interface base para envio de multiplos sms.
 * 
 * @author Phillip Furtado
 */
public interface IMultipleBaseService extends IBaseService {
	/**
	 * Envia uma requisicao ao servidor para consulta de status de sms.
	 * 
	 * @param ids
	 * @return
	 * @throws ClientHumanException
	 */
	public List<Response> query(String[] ids) throws ClientHumanException;
}
