package br.com.medic.medicsystem.sms.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

@ApplicationScoped
public class URLShortenerService {
	
	private final String HOST = "www.googleapis.com";
	
	// server exclusive key
	private final String API_KEY = "AIzaSyBEfyfipk2JDsrQMwstu9p7ei8BwAxP1w8";
	
	// development key
//	private final String API_KEY = "AIzaSyCHM8kSDIT0mqHLJs2S9I2NZxqyfUUdu8I";
	
	private String SHORTENER_URL = "/urlshortener/v1/url";
	
	public String shortenURL(String longURL) {
		if(longURL != null && !longURL.trim().isEmpty()) {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			
			Map<String, Object> reqBody = new HashMap<String, Object>();
			reqBody.put("longUrl", longURL);
			
			HttpPost post = new HttpPost();
			post.addHeader("Content-Type", "application/json");
			String entityStr = null;
			ObjectMapper mapper = new ObjectMapper();
			try {
				post.setURI(new URI( this.SHORTENER_URL + "?key=" + API_KEY ));
				entityStr = mapper.writeValueAsString(reqBody);
				post.setEntity(new StringEntity(entityStr));
				
				HttpHost target = new HttpHost(HOST, 443, "https");
				CloseableHttpResponse response = httpClient.execute(target, post);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					InputStream responseStrem = response.getEntity().getContent();
					
					BufferedReader buffer = new BufferedReader(new InputStreamReader(
							responseStrem));
					
					StringBuffer responseTxt = new StringBuffer();
					String line = null;
					while ((line = buffer.readLine()) != null) {
						responseTxt.append(line);
					}
					@SuppressWarnings("unchecked")
					Map<String, Object> responseMap = mapper.readValue(responseTxt.toString(), Map.class);
					
					if(responseMap != null && responseMap.containsKey("id") ) {
						return (String) responseMap.get("id");
					}				
				}
			} catch(Exception e) {
			}
		}
		
		return null;
	}

}