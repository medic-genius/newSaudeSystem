package br.com.medic.medicsystem.sms.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import br.com.medic.medicsystem.sms.bean.Message;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.bean.SimpleMessage;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.base.BaseService;
import br.com.medic.medicsystem.sms.service.base.ISimpleBaseService;

/**
 * Classe responsavel pelos servicos destinados ao envio de mensagens sms
 * individuais para o gateway.
 * 
 * @author Phillip Furtado
 */
@ApplicationScoped
public class SimpleMessageService extends BaseService implements
		ISimpleBaseService {

	private static final String SEND = "send";
	private static final String CHECK = "check";
	

	/**
	 * Construtor da classe que permite fornecer a conta e a senha para
	 * autenticacao no gateway.
	 * 
	 * @param account
	 * @param password
	 */
	public SimpleMessageService() {
		super();
		this.getHttp().setHost(GATEWAY_HOST);
		this.getHttp().setUri(GATEWAY_URI);
		this.getHttp().setPort(GATEWAY_PORT);
	}

	@Override
	public List<Response> send(Message message) throws ClientHumanException {
		
		String value = System.getenv("SEND_SMS");
		
		if (value != null && value.equals("true")) {
			SimpleMessage simpleMessage = (SimpleMessage) message;
	
			validateAccountAndPassword();
	
			String format = null;
			if (simpleMessage.getSchedule() != null) {
				format = this.getDateFormat().format(simpleMessage.getSchedule());
			}
	
			validateMessage(simpleMessage.getTo(), simpleMessage.getMessage(),
					simpleMessage.getId(), simpleMessage.getFrom(), format);
	
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	
			HttpPost post = new HttpPost();
			nvps.add(new BasicNameValuePair(PARAM_DISPATCH, SEND));
			nvps.add(new BasicNameValuePair(PARAM_ACCOUNT, this.getAccount()));
			nvps.add(new BasicNameValuePair(PARAM_CODE, this.getPassword()));
			nvps.add(new BasicNameValuePair(PARAM_MSG, simpleMessage.getMessage()));
			nvps.add(new BasicNameValuePair(PARAM_TO, simpleMessage.getTo()));
	
			if (simpleMessage.getFrom() != null) {
				nvps.add(new BasicNameValuePair(PARAM_FROM, simpleMessage.getFrom()));
			}
			if (simpleMessage.getId() != null) {
				nvps.add(new BasicNameValuePair(PARAM_ID, simpleMessage.getId()));
			}
			if (format != null) {
				nvps.add(new BasicNameValuePair(PARAM_SCHEDULE, format));
			}
			nvps.add(new BasicNameValuePair(PARAM_CALLBACK_OPTION, String
					.valueOf(simpleMessage.getCallback().getId())));
	
			try {
				post.setEntity(new UrlEncodedFormEntity(nvps));
			} catch (UnsupportedEncodingException e) {
				throw new ClientHumanException(e);
			}
	
			return this.sendRequest(post);
		}
		return null;
	}

	@Override
	public List<Response> query(String id) throws ClientHumanException {
		validateAccountAndPassword();

		if (id == null || id.trim().isEmpty()) {
			throw new ClientHumanException("Id is empty.");
		}

		HttpPost post = new HttpPost();

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		nvps.add(new BasicNameValuePair(PARAM_ACCOUNT, this.getAccount()));
		nvps.add(new BasicNameValuePair(PARAM_CODE, this.getPassword()));

		nvps.add(new BasicNameValuePair(PARAM_DISPATCH, CHECK));
		nvps.add(new BasicNameValuePair(PARAM_ID, id));

		try {
			post.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			throw new ClientHumanException(e);
		}
		return this.sendRequest(post);
	}
}
