package br.com.medic.medicsystem.sms.enumerator;

import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Enumerador para os tipos de layout existentes
 * 
 * @author Phillip Furtado
 */
public enum LayoutTypeEnum {
	/**
	 * to;msg
	 */
	TYPE_A('A'),
	/**
	 * to;msg;from
	 */
	TYPE_B('B'),
	/**
	 * to;msg;id
	 */
	TYPE_C('C'),
	/**
	 * to;msg;id;from
	 */
	TYPE_D('D'),
	/**
	 * to;msg;id;from;schedule
	 */
	TYPE_E('E');

	private final Character type;

	@JsonValue
	public Character getType() {
		return type;
	}
	
	LayoutTypeEnum(Character type) {
		this.type = type;
	}
	
	public static LayoutTypeEnum get(Character type) {
		for (LayoutTypeEnum camp : LayoutTypeEnum.values()) {
			if (camp.type.equals(type)) {
				return camp;
			}
		}
		throw new IllegalArgumentException();
	}
/*
	public static Character get(int id) {
		for (LayoutTypeEnum camp : LayoutTypeEnum.values()) {
			if (camp.id.equals(id)) {
				return camp.type;
			}
		}
		throw new IllegalArgumentException();
	}*/

//	public Integer getId() {
//		return id;
//	}
}
