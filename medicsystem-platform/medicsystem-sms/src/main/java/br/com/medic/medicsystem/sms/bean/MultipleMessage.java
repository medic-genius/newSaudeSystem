package br.com.medic.medicsystem.sms.bean;

import br.com.medic.medicsystem.sms.enumerator.LayoutTypeEnum;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

/**
 * Classe abstrata que e utilizada como modelos para os tipos de mensagens
 * multiplas possiveis.
 * 
 * @author Phillip Furtado
 */
public abstract class MultipleMessage extends Message {

	/**
	 * Tipo de layout da lista ou arquivo a ser enviado
	 */
	protected LayoutTypeEnum type;

	/**
	 * Contrutor da classe informando o tipo de layout do arquivo ou lista
	 * 
	 * @param type
	 */
	public MultipleMessage(LayoutTypeEnum type) {
		super();
		this.type = type;
	}

	/**
	 * Seta o tipo de layout do arquivo ou lista
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(LayoutTypeEnum type) {
		this.type = type;
	}

	/**
	 * Retorna o tipo de layout do arquivo ou lista
	 * 
	 * @return the type
	 */
	public LayoutTypeEnum getType() {
		return type;
	}

	/**
	 * Retorna o conteudo a ser enviado
	 * 
	 * @return
	 * @throws ClientHumanException
	 */
	public abstract String getContent() throws ClientHumanException;
}
