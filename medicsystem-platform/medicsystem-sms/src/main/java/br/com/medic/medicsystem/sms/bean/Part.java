package br.com.medic.medicsystem.sms.bean;

import org.apache.http.entity.mime.content.AbstractContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

/**
 * Classe responsavel por representar um pedaco de conteudo a ser
 * enviado ao servidor via Multipart request
 * 
 * @author Phillip Furtado
 *
 */
public class Part {
	
	/**
	 * Nome do atributo do pedaco
	 */
	private String label;
	
	/**
	 * Conteudo do pedaco
	 */
	private AbstractContentBody body;

	public Part(String label, FileBody fileBody) {
		this.label = label;
		this.body = fileBody;
	}
	
	public Part(String label, StringBody stringBody) {
		this.label = label;
		this.body = stringBody;
	}
	
	public AbstractContentBody getBody() {
		return body;
	}
	
	public String getLabel() {
		return label;
	}
}
