package br.com.medic.medicsystem.sms.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import br.com.medic.medicsystem.sms.bean.SimpleMessage;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.base.BaseService;
import br.com.medic.medicsystem.sms.util.HttpClientHelper;

/**
 * Classe responsavel pelos servicos destinados a consulta de mensagens de sms
 * no gateway.
 * 
 * @author Phillip Furtado
 *
 */
public class QueryMessageService extends BaseService {

	private static final String DISPATCH_RECEIVED = "listReceived";

	/**
	 * Construtor da classe que permite fornecer a conta e a senha para
	 * autenticacao no gateway.
	 * 
	 * @param account
	 * @param password
	 */
	public QueryMessageService() {
		super();
		this.getHttp().setHost(GATEWAY_HOST);
		this.getHttp().setUri(GATEWAY_URI);
		this.getHttp().setPort(GATEWAY_PORT);
	}

	/**
	 * Construtor da classe que permite fornecer a conta, a senha e o helper de
	 * conexao para autenticacao no gateway.
	 * 
	 * @param account
	 * @param password
	 * @param httpHelper
	 */
	public QueryMessageService(HttpClientHelper httpHelper) {
		super(httpHelper);
	}

	/**
	 * Busca por mensagens recebidas(MO) no gateway.
	 * 
	 * @return Lista de mensagens recebidas(MO). Caso nenhuma mensagem seja
	 *         encontrada sera retornada uma lista vazia.
	 * @throws ClientHumanException
	 */
	public List<SimpleMessage> listReceivedSMS() throws ClientHumanException {
		validateAccountAndPassword();
		HttpPost post = new HttpPost();

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		nvps.add(new BasicNameValuePair(PARAM_ACCOUNT, this.getAccount()));
		nvps.add(new BasicNameValuePair(PARAM_CODE, this.getPassword()));
		nvps.add(new BasicNameValuePair(PARAM_DISPATCH, DISPATCH_RECEIVED));

		try {
			post.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			throw new ClientHumanException(e);
		}

		List<SimpleMessage> messages = this.getHttp().requestAndGetMessages(
				post);
		return messages;
	}

}
