package br.com.medic.medicsystem.sms.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import br.com.medic.medicsystem.sms.enumerator.TypeService;
import br.com.medic.medicsystem.sms.service.base.BaseBarateiro;



@ApplicationScoped
public class MenssageBarateiroService extends BaseBarateiro {
	//antigo serviço (LocaSMS)
//	HttpPost httpPost = new HttpPost("http://54.173.24.177/painel/api.ashx");
	//serviço atual simplesSMS
	HttpPost httpPost = new HttpPost("http://sistema.simplessms.com/api.ashx");
	CloseableHttpClient httpclient = HttpClients.createDefault();

	public String send(String mensagem, String numero, String data, String hora) {
		return this.send(mensagem, numero, data, hora, TypeService.LONGCODE, null);
	}

	public String send(String mensagem, String numero, String data, String hora, TypeService typeService) {
		return this.send(mensagem, numero, data, hora, typeService, null);
	}
	
	public String send(String mensagem, String numero, String callbackUrl) {
		return this.send(mensagem, numero, null, null, TypeService.SHORTCODE_2W, callbackUrl);
	}

	public String send(String mensagem, String numero, String data, String hora, 
			TypeService typeService, String callbackUrl) {

		String value = System.getenv("SEND_SMS");

		if (value != null && value.equals("true")) {

			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair(ACTION, ACTIONTYPE));
			nvps.add(new BasicNameValuePair(LGN, this.getLOGIN()));
			nvps.add(new BasicNameValuePair(PWD, this.getSENHA()));
			nvps.add(new BasicNameValuePair(MSG, mensagem));
			nvps.add(new BasicNameValuePair(NUMBERS, numero));
			nvps.add(new BasicNameValuePair(TYPE_SERVICE, typeService.toString()));
			if(data != null) {
				nvps.add(new BasicNameValuePair(JOBDATE, data));
			}
			if(hora != null) {
				nvps.add(new BasicNameValuePair(JOBTIME, hora));
			}
			if(callbackUrl != null) {
				nvps.add(new BasicNameValuePair(CALLBACK, callbackUrl));
			}
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nvps));
				System.out.println(httpPost.getMethod());
				CloseableHttpResponse response = httpclient.execute(httpPost);
				//				if (response.getStatusLine().getStatusCode() == 401) {
				//					System.out.println("401");
				//				}
				//
				//				if (response.getStatusLine().getStatusCode() == 200) {
				//					System.out.println("200");
				//				}

				System.out.println(response.getStatusLine().getStatusCode());

				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					StringBuffer content = new StringBuffer();
					String line;
					while ((line = br.readLine()) != null) {
						content.append(line);
					}
					response.close();
					return content.toString();
					//					httpclient.close();
				} catch (Exception e) {
					System.err.println("ao fechar a conexão "+ e);
				}
			} catch (Throwable e) {

				System.err.println("Falha ao logar com gerente"+ e);

			} finally {
				try {
					//					System.out.println("passou no finally");
				} catch (Exception e) {
					System.err.println("Falha ao fechar conexao com servidor keycloak" + e);
				}
			}	
		}
		return null;
	}
}
