package br.com.medic.medicsystem.main.mapper;

import javax.ejb.EJBAccessException;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.exception.ObjectAlreadyExistsException;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;

/**
 * @author Phillip Furtado
 *
 */
@Provider
public class BusinessExceptionMapper implements ExceptionMapper<EJBException> {

	@Inject
	private Logger logger;

	@Override
	public Response toResponse(final EJBException exception) {

		if (exception instanceof EJBAccessException) {
			return Response.status(Status.FORBIDDEN).build();
		}

		final Exception originalException = exception.getCausedByException();

		logger.error(originalException.getMessage(), originalException);

		if (originalException instanceof ObjectNotFoundException) {
			return Response.status(Status.NO_CONTENT).build();
		}

		if (originalException instanceof ObjectAlreadyExistsException) {
			return Response
			        .status(Status.BAD_REQUEST)
			        .type(MediaType.APPLICATION_JSON)
			        .entity(new ExceptionMessage(ErrorType.DANGER,
			                "The object already exists", exception.getMessage()))
			        .build();
		}

		if (originalException instanceof IllegalArgumentException) {

			IllegalArgumentException iException = (IllegalArgumentException) originalException;
			String message = "";
			if (iException.getMessage() != null) {
				message = iException.getMessage();
			}
			return Response
			        .status(Status.BAD_REQUEST)
			        .type(MediaType.APPLICATION_JSON)
			        .entity(new ExceptionMessage(ErrorType.DANGER,
			                "Bad Request.", ErrorMessage.INVALID_PARAMS_STR
			                        .getMessage() + message)).build();
		}

		return Response
		        .status(Status.INTERNAL_SERVER_ERROR)
		        .type(MediaType.APPLICATION_JSON)
		        .entity(new ExceptionMessage(ErrorType.DANGER,
		                "Internal Server Error",
		                ErrorMessage.INTERNAL_ERROR_STR.getMessage())).build();

	}

}
