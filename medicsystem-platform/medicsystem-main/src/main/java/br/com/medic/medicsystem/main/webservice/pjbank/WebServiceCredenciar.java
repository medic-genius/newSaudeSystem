package br.com.medic.medicsystem.main.webservice.pjbank;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;
import br.com.medic.medicsystem.persistence.model.pjbank.EmpresaCredenciar;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WebServiceCredenciar extends BaseHttpService {

	private final String request = "https://pjbank.com.br/api/v2/credenciar";
	
	public WebServiceCredenciar() {
		super();
		setUseSSL(true); //HTTPS
	}
	
	public String credenciarEmpresa( EmpresaCredenciar empresaCredenciar ) {
		
		String url = request;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
				
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		try {
			jsonStr = mapper.writeValueAsString(empresaCredenciar);
			post.setEntity(new StringEntity(jsonStr));
			
			String response = sendRequest(url, post);
//			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
}
