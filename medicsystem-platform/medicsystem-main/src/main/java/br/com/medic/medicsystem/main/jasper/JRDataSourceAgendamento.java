package br.com.medic.medicsystem.main.jasper;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;

public class JRDataSourceAgendamento implements JRDataSource{
	
	private DateFormat date = new SimpleDateFormat( "dd/MM/yyyy" );
	private DecimalFormat formatador = new DecimalFormat();
	
	private Iterator< AgendamentoView > itr;
	private Object valorAtual;
	private boolean irParaProximo = true;
	
	public JRDataSourceAgendamento(List<AgendamentoView> agendamentosVw) {

		formatador.setMinimumFractionDigits(2);
		formatador.setMaximumFractionDigits(2);

		this.itr = agendamentosVw.iterator();
	}
	
	public boolean next() throws JRException {

		valorAtual = itr.hasNext() ? itr.next() : null;
		irParaProximo = ( valorAtual != null );

		return irParaProximo;
	}

	@Override
	public Object getFieldValue(JRField campo) throws JRException {
		
		Object valor = null;

		AgendamentoView agendamentoVw = (AgendamentoView) valorAtual;
		
		if ("nrAgendamento".equals(campo.getName())) {
			valor = agendamentoVw.getNrAgendamento();
			
		} else if ("nrDespesa".equals(campo.getName())) {
			valor = agendamentoVw.getNrDespesa();
				
		} else if ("nmPaciente".equals(campo.getName())) {
			valor = agendamentoVw.getPaciente();
		
		} else if ("inTipoCliente".equals(campo.getName())) {
			if(agendamentoVw.getCliente().getBoNaoAssociado())
				valor = "NÃO ASSOCIADO";
			else
				valor = "ASSOCIADO";
				
		} else if ("nmCliente".equals(campo.getName())) {
			valor = agendamentoVw.getCliente().getNmCliente();
		
		} else if ("nmEspecialidade".equals(campo.getName())) {
			valor = agendamentoVw.getEspecialidade() != null ? agendamentoVw.getEspecialidade().getNmEspecialidade() : null;
		
		} else if ("nmProfissional".equals(campo.getName())) {
			valor = agendamentoVw.getProfissional() != null ? agendamentoVw.getProfissional().getNmFuncionario() : null;
			
		} else if ("nmServico".equals(campo.getName())) {
			valor = agendamentoVw.getServico() != null ? agendamentoVw.getServico().getNmServico() : null;
		
		} else if ("dtAgendamento".equals(campo.getName())) {
			valor = agendamentoVw.getDtAgendamentoFormatado();
			
		} else if ("hrAgendamento".equals(campo.getName())) {
			valor = agendamentoVw.getHrAgendamento();
			
		} else if ("nmUnidade".equals(campo.getName())) {
			valor = agendamentoVw.getUnidade() != null ? agendamentoVw.getUnidade().getNmUnidade() : null;
			
		} else if ("inStatus".equals(campo.getName())) {
			if(agendamentoVw.getInStatus().equals(1))
				valor = "LIBERADO";
			else if((agendamentoVw.getNrDespesa() != null ? agendamentoVw.getFormaPagamentoDespesa().equals(10) : false) && agendamentoVw.getContrato().getBoBloqueado() && agendamentoVw.getInStatus().equals(1))
				valor = "BLOQUEADO";
			else if(agendamentoVw.getInStatus().equals(0))
				valor = "BLOQUEADO";			
			else if(agendamentoVw.getInStatus().equals(2))
				valor = "ATENDIDO";
			else if(agendamentoVw.getInStatus().equals(3))
				valor = "FALTOSO";
			else if(agendamentoVw.getInStatus().equals(4))
				valor = "PRESENTE";
		}
		
		return valor;
	}

}
