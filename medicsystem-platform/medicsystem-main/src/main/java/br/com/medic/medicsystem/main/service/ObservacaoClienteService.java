package br.com.medic.medicsystem.main.service;

import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.exception.ObservacaoException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ObservacaoClienteDAO;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.ObservacaoCliente;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class ObservacaoClienteService {

	@Inject
	private Logger logger;

	@Inject
	@Named("observacao-cliente-dao")
	private ObservacaoClienteDAO observacaoClienteDAO;

	@Inject
	private SecurityService securityService;

	private Properties props = PropertiesLoader.getInstance().load(
	        "message.properties");

	public ObservacaoCliente saveObservacaoCliente(
	        ObservacaoCliente observacaoCliente) {

		logger.debug("salvando cliente...");

		Funcionario func = securityService.getFuncionarioLogado();

		if (func == null) {
			throw new ObservacaoException(
			        props.getProperty("funcionario.login.notfound"),
			        ErrorType.DANGER);
		}

		observacaoCliente.setFuncionario(func);

		return observacaoClienteDAO.persist(observacaoCliente);

	}

	public void removeObservacao(Long id) {
		ObservacaoCliente oc = getObservacao(id);
		observacaoClienteDAO.remove(oc);

	}

	public ObservacaoCliente getObservacao(Long id) {
		return observacaoClienteDAO.searchByKey(ObservacaoCliente.class, id);
	}

	public ObservacaoCliente updateObservacaoCliente(ObservacaoCliente obs) {
		logger.debug("atualizando cliente...");

		return observacaoClienteDAO.update(obs);
	}

}
