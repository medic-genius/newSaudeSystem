package br.com.medic.dashboard.main.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.dashboard.persistence.dao.MonitoramentoAtendimentoDAO;
import br.com.medic.dashboard.persistence.dao.MonitoramentoProfissionalDAO;
import br.com.medic.dashboard.persistence.dto.MonitoramentoDTO;
import br.com.medic.dashboard.persistence.model.MonitoramentoAtendimento;
import br.com.medic.dashboard.persistence.model.MonitoramentoProfissional;

@ApplicationScoped
public class MonitoramentoService {
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("ml-monitoramento-profissional-dao")
	private MonitoramentoProfissionalDAO monitoramentoProfissionalDAO;
	
	@Inject
	@Named("ml-monitoramento-atendimento-dao")
	private MonitoramentoAtendimentoDAO monitoramentoAtendimentoDAO;
	
	
	public MonitoramentoDTO getMonitorProfissional(Integer tolerancia){
		
		String nomeRelatorio = "Monitoramento de profissionais para atendimento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		
		MonitoramentoDTO mDTO = new MonitoramentoDTO();
		List<MonitoramentoProfissional> listMpAlert = new ArrayList<MonitoramentoProfissional>();
		List<MonitoramentoProfissional> listMp = new ArrayList<MonitoramentoProfissional>();
		List<MonitoramentoProfissional> listMpSemBloqueio = new ArrayList<MonitoramentoProfissional>();
		Integer vlPcMonitor = 100;
				
		List<MonitoramentoProfissional> listMonitoramentoProfissional = monitoramentoProfissionalDAO.getMonitorProfissional(tolerancia); 
				
		for(MonitoramentoProfissional mp : listMonitoramentoProfissional)
			if(mp.getStatus().equals(0))
				listMpAlert.add(mp);				
			else{ 
				listMp.add(mp);
				if(mp.getBloqueio() == null || mp.getBloqueio().length() <= 0)
					listMpSemBloqueio.add(mp);
			}
		
		if(listMonitoramentoProfissional.size() > 0)
			vlPcMonitor = ( ( listMp.size() * 100 ) / listMonitoramentoProfissional.size() );
		
		mDTO.setValorMonitor(vlPcMonitor);
		mDTO.setNrTotal(listMonitoramentoProfissional.size() );
		mDTO.setNrTotalOk(listMpSemBloqueio.size());
		mDTO.setMonitoramento(listMp);
		mDTO.setMonitoramentoAlert(listMpAlert);
						
		return mDTO;
		
	}
	
	public MonitoramentoDTO getMonitorAtendimento(Integer tolerancia){
	
		String nomeRelatorio = "Monitoramento no atendimento de pacientes";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		
		MonitoramentoDTO mDTO = new MonitoramentoDTO();
		List<MonitoramentoAtendimento> listMaAlert = new ArrayList<MonitoramentoAtendimento>();
		List<MonitoramentoAtendimento> listMa = new ArrayList<MonitoramentoAtendimento>();
		Integer vlPcMonitor = 100;
		
		List<MonitoramentoAtendimento> listMonitoramentoAtendimento = monitoramentoAtendimentoDAO.getMonitorAtendimento(tolerancia); 
		
		for(MonitoramentoAtendimento ma : listMonitoramentoAtendimento)						
			if(ma.getStatus().equals(0)) //Problemas
				listMaAlert.add(ma);				
			else
				listMa.add(ma);
		
		if(listMonitoramentoAtendimento.size() > 0)
			vlPcMonitor = ( ( listMa.size() * 100 ) / listMonitoramentoAtendimento.size() );
		
		mDTO.setValorMonitor(vlPcMonitor);
		mDTO.setNrTotal(listMonitoramentoAtendimento.size());
		mDTO.setNrTotalOk(listMa.size());
		mDTO.setMonitoramento(listMa);
		mDTO.setMonitoramentoAlert(listMaAlert);
		
		return mDTO;
		
	}

}
