package br.com.medic.medicsystem.main.service;

import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.samples.AbstractReportGenerator;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.jasper.JRDataSourcePlanoDiamante;
import br.com.medic.medicsystem.main.jasper.JRDataSourcePlanoDiamanteSemCarencia;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.main.vindi.data.subscription.Subscription;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.service.SubscriptionService;
import br.com.medic.medicsystem.persistence.dao.AgenciaDAO;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.BancoDAO;
import br.com.medic.medicsystem.persistence.dao.CarenciaDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.CoberturaPlanoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDependenteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.OrgaoDAO;
import br.com.medic.medicsystem.persistence.dao.PlanoDAO;
import br.com.medic.medicsystem.persistence.dao.TituloDaDAO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.ContratoDocumentoDTO;
import br.com.medic.medicsystem.persistence.dto.ContratoProducaoVendaDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Agencia;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Banco;
import br.com.medic.medicsystem.persistence.model.Carencia;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Orgao;
import br.com.medic.medicsystem.persistence.model.Plano;
import br.com.medic.medicsystem.persistence.model.TituloDa;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoContratoEnum;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.views.ViewRenovacaoContrato;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Stateless
public class ContratoService {

	@Inject
	private Logger logger;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
	@Inject
	@Named("contratocliente-dao")
	private ContratoClienteDAO contratoclienteDAO;
	
	@Inject
	@Named("contratodependente-dao")
	private ContratoDependenteDAO contratodependenteDAO;
	
	@Inject
	@Named("empresagrupo-dao")
	private EmpresaGrupoDAO empresaGrupoDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
		
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;
	
	@Inject
	@Named("tituloda-dao")
	private TituloDaDAO titulodaDAO;
	
	@Inject
	@Named("plano-dao")
	private PlanoDAO planoDAO;
	
	@Inject
	@Named("coberturaplano-dao")
	private CoberturaPlanoDAO coberturaplanoDAO;
	
	@Inject
	@Named("carencia-dao")
	private CarenciaDAO carenciaDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("orgao-dao")
	private OrgaoDAO orgaoDAO;
	
	@Inject
	@Named("banco-dao")
	private BancoDAO bancoDAO; 
	
	@Inject
	@Named("agencia-dao")
	private AgenciaDAO agenciaDAO;
	
//	@Inject
//	private ClienteServiceSP clienteServiceSP;
			
	@Inject
	private SubscriptionService subscriptionService;
	
	@Inject
	private BoletoServicePJB boletoServicePJB;
	
	@Inject
	private SecurityService securityService;
	
	@Inject
	private VendasService vendasService;
	
	
	public Contrato saveContrato(Contrato contrato, Long idCliente, List<Long> idDependentes, Long idAssinaturaRecorrente){		
		
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
							
		contrato.setNovo(true);		
		contrato.setSituacao( 0 ); //Ativo
		contrato.setBoBloqueado( true );
		contrato.setDtContrato( new Date() );
		contrato.setIdOperadorCadastro( securityService.getFuncionarioLogado().getId() );
				
		contrato.setNrTalao( geraNrTalao() );
		contrato.setNrContrato( geraCodigoContrato() );

		contrato.setVlDesconto( 0.0 );
		contrato.setVlMultaAtraso( 0.0 );
		contrato.setVlTaxaJuros( 0.0 );

//		contrato.setBoNaoFazUsoPlano(boNaoFazUsoPlano); --> Front end
		contrato.setInRenovado( 0 ); // Contrato nao renovado

		contrato.setPlano( planoDAO.searchByKey(Plano.class, contrato.getPlano().getId()) );
		contrato.setDtTerminoContrato( definirDataTerminoContrato(contrato.getDtVencimentoInicial(), contrato.getPlano().getNrQtdeMensalidade()) );
		
		contrato.setEmpresaGrupo( empresaGrupoDAO.searchByKey(EmpresaGrupo.class, contrato.getEmpresaGrupo().getId()) ); 		
		contrato.setFuncionario( securityService.getFuncionarioLogado() ); //Vendedor do plano
		contrato.setUnidadeVenda( securityService.getFuncionarioLogado().getUnidade().getId().intValue() );
		
		contrato.setBanco( contrato.getBanco() != null && contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId()) ? 
				bancoDAO.searchByKey(Banco.class, contrato.getBanco().getId()) : null ); //--> Front end
		
		contrato.setAgencia( contrato.getAgencia() != null && contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId()) ? 
				agenciaDAO.searchByKey(Agencia.class, contrato.getAgencia().getId()) : null ); //--> Front end
		
		contrato.setRazao(null);		
		contrato.setNrConta(contrato.getNrConta() != null && contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId()) ? 
				contrato.getNrConta() : null); //--> Front end	
		
		contrato.setNrDigitoVerificador(contrato.getNrDigitoVerificador() != null && contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId()) ? 
				contrato.getNrDigitoVerificador() : null); //--> Front end	
		
		contrato.setBoCadastradoBancoBrasil( false );
		contrato.setDtTerminoVinculo( null );	
		
		ContratoCliente contratocliente = new ContratoCliente();
		contratocliente.setInTipoCliente(contrato.getInTipoCliente()); //(0-Funcionario Clinica, 1-Funcionario Publico, 2-Associado, 3-Empresarial, 4-Não Associado) --> Front end
		contratocliente.setBoAtivo( true );		
		contratocliente.setCliente( cliente );
		contratocliente.setContrato( contrato );
		contratocliente.setPlano( contrato.getPlano() );
		contratocliente.setInVinculo( null );
		contratocliente.setDtInclusaoLog(new Timestamp(System.currentTimeMillis()));		
						
		//Se cliente funcionario publico
		contratocliente.setNrMatricula(contrato.getNrMatricula() != null ? contrato.getNrMatricula() : null); //--> Front end		
		contratocliente.setOrgao( contrato.getIdOrgao() != null ? orgaoDAO.searchByKey(Orgao.class, contrato.getIdOrgao()) : null ); //--> Front end
		
		contrato.addContratoCliente(contratocliente);
		
		for(Long iddependente : idDependentes){
			
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, iddependente);
			ContratoDependente contratodependente = new ContratoDependente();
			contratodependente.setBlAutorizaDebAut( false );
			contratodependente.setContrato(contrato);
			contratodependente.setInSituacao(0); //Ativo		
			contratodependente.setDependente(dependente);
			
			contrato.addContratoDependente(contratodependente);
		}
		
		
		//Mensalidades
		List<Mensalidade> mensalidades = criarMensalidades( contrato );
		
		//CLIENTE passa a ser ASSOCIADO
		cliente.setBoNaoAssociado( false );
		clienteDAO.update(cliente);
		
		contratoDAO.persist(contrato);
		
		//Forma Pagamento Contrato
		tratarFormaPagamento(contrato, cliente, mensalidades, idAssinaturaRecorrente);
		
		//Add carencia contrato
		adicionarCarenciaContrato( contrato );
		
		inativarContratoNaoAssociado(cliente.getId());
		
		updateAgendamentosNovosCliente(cliente.getId(), contrato);
						
		return contrato;
		
	}
	
	public Integer atualizarContratoRecorrentebyIdsubscription(Long idAssinaturaRecorrente){
		
		return contratoDAO.atualizarContratoRecorrentebyIdsubscription(idAssinaturaRecorrente);
	
	}
	
	public byte[] getImpressaoBoletoContrato(Long idContrato){
		
//		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
//		Contrato contrato = contratoDAO.searchByKey(Contrato.class, idContrato);
//		return cobrancaserviceSP.imprimirCarne(cliente, contrato);
		
		List<Mensalidade> mensalidades = mensalidadeDAO.getMensalidadeByContrato(idContrato); 
		
		return boletoServicePJB.imprimirBoleto(mensalidades);

	}

	public Contrato getContrato(Long idContrato) {

		logger.debug("Obtendo contrato com id " + idContrato);

		return contratoDAO.getContrato(idContrato);
	}

	public ContratoCliente getContratoCliente(Long idContrato, Long idCliente) {
		
		logger.debug("Obtendo contrato cliente com id " + idContrato);
		
		return contratoDAO.getContratoCliente(idContrato, idCliente);
	}

	public List<ContratoDependente> getContratosDependentes(Long idcontrato, Long idcliente) {
		
		return contratoDAO.getContratosDependentes(idcontrato, idcliente);
	}

	public List<FormaPagamentoEnum> getFormasPagamentoLiberado(Long idContrato) {

		List<FormaPagamentoEnum> formaPagamentoList = new ArrayList<FormaPagamentoEnum>();
		Properties props = PropertiesLoader.getInstance().load(
		        "message.properties");

		ContratoCliente cc = contratoDAO.getContratoCliente(idContrato);

		if (cc.getInTipoCliente() == 1 || cc.getInTipoCliente() == 2
		        || cc.getInTipoCliente() == 4 || cc.getInTipoCliente() == 88) {

			formaPagamentoList.add(FormaPagamentoEnum.DINHEIRO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_A_VISTA);
			formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_CREDITO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_DEBITO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_PRE_DATADO_LOWER);

			if (cc.getContrato().getBoBloqueado() == null
			        || !cc.getContrato().getBoBloqueado()) {

				if (cc.getContrato().getBanco() != null
				        && cc.getContrato().getAgencia() != null
				        && cc.getContrato().getNrDigitoVerificador() != null
				        && cc.getContrato().getNrConta() != null) {

					formaPagamentoList
					        .add(FormaPagamentoEnum.DEBITO_AUTOMATICO_LOWER);
				}
			}
		}

		if (cc.getInTipoCliente() == 3) { // Empresarial

			formaPagamentoList.add(FormaPagamentoEnum.DINHEIRO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_A_VISTA);
			formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_CREDITO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_DEBITO_LOWER);
			formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_PRE_DATADO_LOWER);

			if (cc.getContrato().getBoBloqueado() == null
			        || !cc.getContrato().getBoBloqueado()) {
				if (cc.getContrato().getEmpresaCliente() != null) {

					Contrato contratoConveniada = new Contrato();
					contratoConveniada = contratoDAO
					        .getContratoPorEmpresaCliente(cc.getContrato()
					                .getEmpresaCliente().getId());

					if (contratoConveniada != null) {
						if (contratoConveniada.getNrParcelaConvenio() != null
						        && contratoConveniada.getNrParcelaConvenio() > 0)
							formaPagamentoList
							        .add(FormaPagamentoEnum.CONVENIO_LOWER);
					} else {

						throw new AgendamentoException(
						        props.getProperty("convenio.inativo"),
						        ErrorType.ERROR);
					}
				}
			}

			if (cc.getContrato().getBoBloqueado() == null
			        || !cc.getContrato().getBoBloqueado()) {
				if (cc.getContrato().getBanco() != null
				        && cc.getContrato().getAgencia() != null
				        && cc.getContrato().getNrDigitoVerificador() != null
				        && cc.getContrato().getNrConta() != null) {

					formaPagamentoList
					        .add(FormaPagamentoEnum.DEBITO_AUTOMATICO_LOWER);
				}
			}
		}

		return formaPagamentoList;
	}
	
	public byte[] getImpressaoContrato(Long idCliente, Long idContrato, List<Long>idDependentes, Integer tipoPlano){
		
		ByteArrayOutputStream output = null;
		byte[] pdf = null;	
		
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
		Contrato contrato = contratoDAO.searchByKey(Contrato.class, idContrato);
		
		Calendar cnow = Calendar.getInstance();
		
		Calendar c = Calendar.getInstance();
		c.setTime(contrato.getDtVencimentoInicial());
		
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");		
		Date dt = null;
		try {
			dt = df.parse(DateUtil.getDateAsString(cnow.getTime()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DateFormat df2 = new SimpleDateFormat ("MMMMM", new Locale ("pt", "BR"));
//		System.out.println (df2.format (dt));
		
		ContratoDocumentoDTO contratoDocumentoDto = new ContratoDocumentoDTO();
		
		contratoDocumentoDto.setDia( String.valueOf(cnow.get(Calendar.DATE)) );
		contratoDocumentoDto.setMes( df2.format (dt) );
		contratoDocumentoDto.setAno( String.valueOf(cnow.get(Calendar.YEAR)) );
		
		contratoDocumentoDto.setNrMatricula(contrato.getContratosCliente().get(0).getNrMatricula());	
		//contratoDocumentoDto.setTipoContrato(tipoContrato);
		
		contratoDocumentoDto.setNmTitular(cliente.getNmCliente());
		contratoDocumentoDto.setCpfTitular(cliente.getNrCPF());
		contratoDocumentoDto.setRgTitular(cliente.getNrRG());
		contratoDocumentoDto.setSexo(cliente.getInSexo().toString());
		contratoDocumentoDto.setEstadoCivil(cliente.getInEstadoCivil().toString());
		contratoDocumentoDto.setOcupacaoTitular(cliente.getNmOcupacao());
		
		
		contratoDocumentoDto.setTelefoneCelular(cliente.getNrCelular());
		contratoDocumentoDto.setTelefoneComercial(null);
		contratoDocumentoDto.setRamalTelComercial(null);
		contratoDocumentoDto.setTelefoneResidencial(cliente.getNrTelefone());		
		
		contratoDocumentoDto.setLogradouroTitular(cliente.getNmLogradouro());
		contratoDocumentoDto.setNrLogradouro(cliente.getNrNumero());
		contratoDocumentoDto.setNmCidade(cliente.getCidade().getNmCidade());
		contratoDocumentoDto.setBairro(cliente.getBairro().getNmBairro());
		contratoDocumentoDto.setCep(cliente.getNrCEP());	
		contratoDocumentoDto.setUf(cliente.getCidade().getNmUF());
		contratoDocumentoDto.setComplementoLogradouro(cliente.getNmComplemento());
		contratoDocumentoDto.setAptoLogradouro(null);		
		contratoDocumentoDto.setBlocoLogradouro(null);	
		
		int cont = 0;
		for(Long iddependente : idDependentes){
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, iddependente);
			
			if(cont == 0){
				contratoDocumentoDto.setNmDependente1(dependente.getNmDependente());
				contratoDocumentoDto.setDtNascimento1( dependente.getDtNascimento() != null ? df.format(dependente.getDtNascimento()) : null );
				contratoDocumentoDto.setParentescoDept1(dependente.getParentescoFormatado());
			}
			
			if(cont == 1){
				contratoDocumentoDto.setNmDependente2(dependente.getNmDependente());
				contratoDocumentoDto.setDtNascimento2( dependente.getDtNascimento() != null ? df.format(dependente.getDtNascimento()) : null );
				contratoDocumentoDto.setParentescoDept2(dependente.getParentescoFormatado());
			}
			
			if(cont == 2){
				contratoDocumentoDto.setNmDependente3(dependente.getNmDependente());
				contratoDocumentoDto.setDtNascimento3( dependente.getDtNascimento() != null ? df.format(dependente.getDtNascimento()) : null );
				contratoDocumentoDto.setParentescoDept3(dependente.getParentescoFormatado());
			}
			
			if(cont == 3){
				contratoDocumentoDto.setNmDependente4(dependente.getNmDependente());
				contratoDocumentoDto.setDtNascimento4( dependente.getDtNascimento() != null ? df.format(dependente.getDtNascimento()) : null );
				contratoDocumentoDto.setParentescoDept4(dependente.getParentescoFormatado());
			}
			
			cont++;			
		}

		
		contratoDocumentoDto.setDiaPagamento( String.valueOf(c.get(Calendar.DATE)) );
		contratoDocumentoDto.setFormaPagamento( getFormaPagamentoFormatado(contrato.getInformaPagamento()) );
		contratoDocumentoDto.setNmVendedor(contrato.getFuncionario().getNmFuncionario());
		contratoDocumentoDto.setGrupoConveniado(null);
		contratoDocumentoDto.setTotalPlano(contrato.getVlTotalFormatado());
		contratoDocumentoDto.setTitularnaofazuso(contrato.getBoNaoFazUsoPlano());
				
		contratoDocumentoDto.setNmBanco( contrato.getBanco() != null && contrato.getInformaPagamento() == 1 ? contrato.getBanco().getNmBanco() : null);
		contratoDocumentoDto.setNrAgencia(contrato.getAgencia() != null && contrato.getInformaPagamento() == 1 ? contrato.getAgencia().getNrAgencia() : null);
		contratoDocumentoDto.setNrConta(contrato.getNrConta() != null && contrato.getInformaPagamento() == 1 ? contrato.getNrConta() : null);		
		
		try {
			
			output = new ByteArrayOutputStream();
			
		    // Generate the report
			if(tipoPlano.equals(0))//Plano Diamante		    
				new JRDataSourcePlanoDiamante(contratoDocumentoDto).generateReport( AbstractReportGenerator.OutputType.PDF , output);
			else			
				new JRDataSourcePlanoDiamanteSemCarencia(contratoDocumentoDto).generateReport( AbstractReportGenerator.OutputType.PDF , output);
			
			pdf = output.toByteArray();			   
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Contrato",
			        ErrorType.ERROR, e);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Contrato",
			        ErrorType.ERROR, e);
		}
		
		try {
			if (output != null) {
				output.close();
			}
		} catch (Exception e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Contrato",
			        ErrorType.ERROR, e);
		} 
		
		return pdf;
	}
	
	public List<ContratoProducaoVendaDTO> getContratoProdVenda(String dtInicio, String dtFim){
		
		return contratoDAO.getContratoProdVenda(securityService.getFuncionarioLogado().getId(), dtInicio, dtFim);
	}
	
	public List<ContratoProducaoVendaDTO> getContratoProdVendaFuncionarios(String dtInicio, String dtFim, Integer idUnidade){
		
		return contratoDAO.getContratoProdVendaFuncionarios(dtInicio, dtFim, idUnidade);
	}
	
	/**
	 * Atualiza todos os agendamentos atuais e futuros de contrato nao associado para uso do contrato de associado.
	 * @param idCliente
	 * @param contrato
	 */
	public void updateAgendamentosNovosCliente(Long idCliente, Contrato contrato){
		
		List<Agendamento> agendamentos = agendamentoDAO.getAgendamentosNovosCliente(idCliente);
		
		if(agendamentos != null && agendamentos.size() > 0)
			for(Agendamento agendamento : agendamentos){
				if(agendamento.getDependente() != null && contrato.getContratosDependente() != null && contrato.getContratosDependente().size() > 0){
					
					for(ContratoDependente contratodependente : contrato.getContratosDependente()){
						if(agendamento.getDependente().getId().equals(contratodependente.getDependente().getId()))
							agendamento.setContrato(contrato);
					}
				}else
					agendamento.setContrato(contrato);
				
				agendamentoDAO.update(agendamento);
			}
		
	}	
	
	private void adicionarCarenciaContrato(Contrato contrato){
		
		List<CoberturaPlano> cobertura = new ArrayList< CoberturaPlano >();
		
		cobertura = coberturaplanoDAO.getCoberturaPlanoPorPlano(contrato.getPlano().getId()); 
		for (CoberturaPlano cp : cobertura) {
				
			Carencia carencia = new Carencia();
			carencia.setContrato( contrato );
			carencia.setServico( cp.getServico() );
			carencia.setPlano( cp.getPlano() );
			carencia.setCarencia( cp.getCarencia() );
			carencia.setTotalConsulta( 0 );
			carenciaDAO.persist(carencia);
			
		}

	}
	
	/**
	 * Gera acoes de acordo com a forma de pagamento do contrato
	 * @param contrato
	 * @param cliente
	 * @param mensalidades
	 */
	private void tratarFormaPagamento(Contrato contrato, Cliente cliente, List<Mensalidade> mensalidades, Long idAssinaturaRecorrente){
		
		if(contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getId())){
					
			for ( Mensalidade iMensalidade : mensalidades ) {
											
				//Cadastrar boleto Pjbank
				try {
					mensalidadeDAO.persist(iMensalidade);
					boletoServicePJB.registrarBoleto(iMensalidade, cliente);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			//Cadastrar o cliente no sistema SUPER LOGICA
//			if(cliente.getIdClienteBoleto_sl() == null){
//				cliente = clienteServiceSP.cadastrarCliente(cliente);
//			}
			//Cadastrar cobranca no sistema SUPER LOGICA					
//			if(iMensalidade.getIdCobrancaBoleto_sl() == null)
//				iMensalidade = cobrancaserviceSP.cadastrarCobrancaAvulsaCliente(iMensalidade, cliente);
//			
//			mensalidadeDAO.persist(iMensalidade);
			
		}
		
		
		if(contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId())){
			
			for ( Mensalidade iMensalidade : mensalidades ) {					
				
				mensalidadeDAO.persist(iMensalidade);
				
				TituloDa titulo = new TituloDa();
				titulo.setAgencia( iMensalidade.getContrato().getAgencia() );
				titulo.setBanco( iMensalidade.getContrato().getBanco() );
				titulo.setCliente( cliente );
				titulo.setDtEmissao( iMensalidade.getContrato().getDtContrato() );
				titulo.setDtVencimento( iMensalidade.getDataVencimento() );
				titulo.setInMotivoRetorno( null );
				titulo.setVlTitulo( iMensalidade.getValorMensalidade() );
				titulo.setInStatus( 0 ); //Aberto
				titulo.setInTipo( 1 );
				titulo.setMensalidade( iMensalidade );
				titulo.setNmStatusBb( iMensalidade.getContrato().getBoCadastradoBancoBrasil() != null && iMensalidade.getContrato().getBoCadastradoBancoBrasil() ? "C" : "N" );
				titulo.setNrContaCorrente( iMensalidade.getContrato().getNrConta() );
				titulo.setNrDigitoVerificador( iMensalidade.getContrato().getNrDigitoVerificador() );
				titulo.setParcela( null );
				titulo.setRazao( iMensalidade.getContrato().getRazao() );
				titulo.setNrTitulo( titulodaDAO.getNumeracaoTitulo() );
				titulo.setFuncionario( securityService.getFuncionarioLogado() );

				titulodaDAO.persist( titulo );
				titulo = null;
				
			}
		}
		
		if(contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getId())){
			for ( Mensalidade iMensalidade : mensalidades ) {
				mensalidadeDAO.persist(iMensalidade);
			}
		}
		
		if(contrato.getInformaPagamento().equals(FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getId())){
			
			String numeroCiclo = "";
			Integer cont = 1;			
			Subscription subscription = new Subscription(); 
					
			try {
				subscription = subscriptionService.getSubscription( Integer.valueOf(idAssinaturaRecorrente.toString()) );
				
				if( subscription != null ) {
					if(cliente.getIdRecorrencia() == null){
						cliente.setIdRecorrencia(subscription.getCustomer().getId().longValue());
						clienteDAO.update(cliente);
					}
					
					for (Mensalidade iMensalidade : mensalidades) {				
						
						iMensalidade.setIdAssinaturaRecorrente(subscription.getId().longValue());		
						numeroCiclo = cont.toString().length() == 1 ? "0" + cont.toString() : cont.toString();					
						iMensalidade.setNrCicloRecorrente( numeroCiclo );					
						
						mensalidadeDAO.persist( iMensalidade );
						cont++;
					}					
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (VindiException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}
	}
	
	/**
	 * Inativacao do contrato de nao associado, para clientes associados.
	 * @param idCliente
	 */
	public void inativarContratoNaoAssociado(Long idCliente){
		
		List<Contrato> contratosAtivos = (List<Contrato>) clienteDAO.getContratos(idCliente);
		
		for(Contrato contrato : contratosAtivos){
			
			if(contrato.getPlano() == null){
				
				contrato.setSituacao( 1 );
				contrato.setDtInativacao( new Date() );
				contrato.setIdOperadorInativacao( 4738L );
				contrato.setDtTerminoContrato( new Date() );
				contrato.setInTipoInativacao( 0 ); // inativacao por empresa 
				contrato.setMotivo("INATIVACAO AUTOMATICA COMO CONSEQUENCIA DO CADASTRO DE PLANO ASSOCIADO");
				contratoDAO.update(contrato);

				ContratoCliente concli = contratoDAO.getContratoCliente(contrato.getId());
				concli.setBoAtivo( false );
				contratoclienteDAO.update(concli);
								
				List<ContratoDependente> condeps = contratoDAO.getContratosDependentes(contrato.getId());
				for(ContratoDependente contdep : condeps){
					if( contdep.getDtInativacao() == null || contdep.getInSituacao().equals(0) ){
						contdep.setDtInativacao( new Date() );
						contdep.setInSituacao( 1 );
						contratodependenteDAO.update(contdep);
					}
				}		   
				
			}
		}
		
		
	}
	
		
	private String geraNrTalao(){

		String ultimoNumeroGerado = contratoDAO.getUltimoNumeroTalaoGerado();

		return String.format( "%06d", Integer.parseInt(ultimoNumeroGerado) + 1 );
	}
	
	public String geraCodigoContrato(){

		String ultimoNumeroGerado = contratoDAO.getUltimoCodigoGerado();

		Calendar calendar = Calendar.getInstance();
		int anoAtual = calendar.get( Calendar.YEAR );

		if ( ultimoNumeroGerado == null )
			return String.format( "%06d/%d", 1, anoAtual );

		int sequencial = Integer.parseInt( ultimoNumeroGerado.split( "/" )[ 0 ] );
		int ano = Integer.parseInt( ultimoNumeroGerado.split( "/" )[ 1 ] );

		return String.format( "%06d/%d", ano == anoAtual ? sequencial + 1 : 1, anoAtual );
	}
	
	private Date definirDataTerminoContrato( Date dtVencimentoInicial, Integer qtdMensalidades ) {
		
		Calendar dataTermino = Calendar.getInstance();
		
		dataTermino.setTime( dtVencimentoInicial );		
        dataTermino.add( Calendar.MONTH, qtdMensalidades - 1 );
		
		return dataTermino.getTime();
	}
	
	private List< Mensalidade > criarMensalidades( Contrato contrato ) {

		List< Mensalidade > mensalidades = new LinkedList< Mensalidade >();
		
		// Plano médico gera 12 mensalidades e Plano Odontolégico gera 24 mensalidades

		Integer qtMensalidade =  contrato.getPlano().getNrQtdeMensalidade();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime( contrato.getDtVencimentoInicial() );
		int diaInical = calendar.get( Calendar.DAY_OF_MONTH );
		
		int numeroMensalidade = 1;
		while ( numeroMensalidade <= qtMensalidade ) {
			
			Mensalidade mensalidade = new Mensalidade();
			mensalidade.setContrato( contrato );
			mensalidade.setDataVencimento( calendar.getTime() );
			mensalidade.setInStatus( 0 ); // Aberto
			mensalidade.setNrMensalidade( contrato.getNrContrato() + String.format( "/%02d", numeroMensalidade ) );
			mensalidade.setValorMensalidade( contrato.getVlTotal() );
			mensalidade.setInformaPagamento( getFormaPagamento( contrato.getInformaPagamento() ));

			mensalidades.add( mensalidade );
			calendar.add( Calendar.MONTH, 1 );

			calendar.set( Calendar.DAY_OF_MONTH, diaInical > calendar.getActualMaximum( Calendar.DAY_OF_MONTH )
				? calendar.getActualMaximum( Calendar.DAY_OF_MONTH ) : diaInical );

			numeroMensalidade++;
		}

		return mensalidades;
	}
	
	private int getFormaPagamento(int informapagamentoContrato){
		
		if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getId() )
			return 5;    
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId() )
			return  1 ;  
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getId() )
			return 0 ;
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getId())
			return 30;
		
		return -1;		
	}
	
	private String getFormaPagamentoFormatado(int informapagamentoContrato){
		
		if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getId() )
			return FormaPagamentoContratoEnum.FORMA_PAGAMENTO_BOLETO.getDescription();    
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getId() )
			return  FormaPagamentoContratoEnum.FORMA_PAGAMENTO_DEBITO_AUTOMATICO.getDescription() ;  
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getId() )
			return FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CONTRA_CHEQUE.getDescription() ;
		else if ( informapagamentoContrato == FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getId())
			return FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE.getDescription();
		
		return null;		
	}
	
	public Contrato getContratoEmpresaClienteById(Long idEmpresaCliente) {
		return contratoDAO.getContratoEmpresaClienteById(idEmpresaCliente);
	}

	public List<FormaPagamentoContratoEnum> getFormaPagamentoRenovacao() {
		return FormaPagamentoContratoEnum.getFormaPagamento();
	}

	public GenericPaginateDTO getRenovacoes(String dtInicio, String dtFim,
			Long inFormaPagamento, Long idPlano, Long idEmpresaGrupo,
			Long idFuncionario, int offset, int limit) {
		if(limit > 50){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String dataFim = dtFim + " 23:59:59";
		String dataInicio = dtInicio + " 00:00:00";
		Date dateInicio;
		Date dateFim;
		Timestamp timestampFim;
		Timestamp timestampInicio;
		
		try{
			dateInicio = sdf.parse(dataInicio);
			dateFim = sdf.parse(dataFim);

			timestampFim = new Timestamp(dateFim.getTime());
			timestampInicio  = new Timestamp(dateInicio.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		Long total = contratoDAO.getTotalRenovacoesContrato(timestampInicio,
				timestampFim, inFormaPagamento, idPlano, idEmpresaGrupo,
				idFuncionario);
		List<?> listRenovacoes = contratoDAO.getRenovacoesContrato(offset,
				limit, timestampInicio, timestampFim, inFormaPagamento,
				idPlano, idEmpresaGrupo, idFuncionario);
		
		GenericPaginateDTO renovacoes = new GenericPaginateDTO();
		renovacoes.setOffset(offset);
		renovacoes.setLimit(limit);
		renovacoes.setTotal(total); //tenho que pegar o total ainda
		renovacoes.setData(listRenovacoes);
		renovacoes.prepare();
		return renovacoes;
	}
	
	public List<FuncionarioDTO> getFuncionariosRenovacao(String dtInicio, String dtFim, Long inFormaPagamento, Long idPlano, Long idEmpresaGrupo) {
		Date dateInicio = DateUtil.parseDate(dtInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		Date dateFim = DateUtil.parseDate(dtFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
		return contratoDAO.getFuncionariosRenovacao(dateInicio, dateFim, inFormaPagamento, idPlano, idEmpresaGrupo);
	}
	
	public List<ViewRenovacaoContrato> renovarContratosRecorrentes(String dtFimContratoStr) {
		Date dtFimContrato = DateUtil.parseDate(dtFimContratoStr,
				DateUtil.DatePattern.DDMMAA.getPattern());
		if (dtFimContrato == null) {
			dtFimContrato = new Date();
		}

		List<ViewRenovacaoContrato> listRenovacao = contratoDAO
				.getContratosParaRenovacao(
						dtFimContrato,
						FormaPagamentoContratoEnum.FORMA_PAGAMENTO_CARTAO_RECORRENTE);
		if(listRenovacao != null && !listRenovacao.isEmpty()) {
			for(ViewRenovacaoContrato renovacao : listRenovacao) {
				Contrato contratoAtual = contratoDAO.getContrato(renovacao
						.getIdContrato());
				
				if (contratoAtual == null || contratoAtual.getPlano() == null
						|| contratoAtual.getPlano().getIdRecorrenciaPlano() == null) {
					logger.debug("Contrato ou plano não podem ser renovados");
					continue;
				}
				Cliente cliente = clienteDAO.getClienteByContrato(contratoAtual
						.getId());
				
				Integer vindiCustomer = Integer.parseInt(cliente.getIdRecorrencia().toString());
				AssinaturaRecorrenteResultDTO assinatura = null;
				int counter = 0;
				do {
					assinatura = vendasService.renovarAssinaturaRecorrenteContrato(
							vindiCustomer, contratoAtual.getPlano()
									.getIdRecorrenciaPlano(), renovacao);
					++counter;
				} while (!assinatura.getStatus().equals("success") && counter < 3);
				
				if(assinatura.getStatus().equals("success")) {
					logger.debug("Assinatura recorrente renovada");
					contratoDAO.renovaContratoProcedure(
							renovacao.getIdContrato(),
							assinatura.getSubscriptionId());
				} else {
					logger.debug("Erro ao renovar assinatura recorrente");
				}
			}
		}
		return listRenovacao;
	}
	
}
