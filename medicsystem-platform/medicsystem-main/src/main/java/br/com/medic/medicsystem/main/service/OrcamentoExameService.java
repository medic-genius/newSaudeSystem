package br.com.medic.medicsystem.main.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioEncaminhamentoContinuo;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioOrcamentoServico;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.OrcamentoExameDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoOrcamentoExameDAO;
import br.com.medic.medicsystem.persistence.dto.OrcamentoExameDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.OrcamentoExame;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.ServicoOrcamentoExame;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoDespesaOrcamentoExameView;
import br.com.medic.medicsystem.persistence.model.views.ServicoExame;

@Stateless
public class OrcamentoExameService {
	
	
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("atendimento-dao")
	private AtendimentoDAO atendimentoDAO;
	
	@Inject
	@Named("servicoorcamentoexame-dao")
	private ServicoOrcamentoExameDAO servicoOrcamentoExameDAO;
	
	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;
	
	@Inject
	@Named("orcamentoexame-dao")
	private OrcamentoExameDAO orcamentoExameDAO;
	
	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	

	
	
		
	public boolean updateOrcamento(List<OrcamentoExameDTO> orcamentoExameDTO) {
		
		List<ServicoOrcamentoExame> listServOrcExameOriginal = new ArrayList<ServicoOrcamentoExame>();	
		
		boolean atualizado = false;
		
		OrcamentoExame orcamentoExameOriginal = orcamentoExameDAO.searchByKey(OrcamentoExame.class,orcamentoExameDTO.get(0).getIdOrcamentoExame()) ;
		
			listServOrcExameOriginal = servicoOrcamentoExameDAO.getServicoExamePorOrcamento(orcamentoExameOriginal.getId());
			
			if(listServOrcExameOriginal != null && listServOrcExameOriginal.size() > 0){
				servicoOrcamentoExameDAO.deleteServicoOrcamentoExame(listServOrcExameOriginal);
				
			}
				
			for (OrcamentoExameDTO orc : orcamentoExameDTO) {
				if(orc.getIdServico() != null){
					ServicoOrcamentoExame servicoOrcamentoExame = new ServicoOrcamentoExame();
					Servico servico = servicoDAO.searchByKey(Servico.class, orc.getIdServico());
					
					servicoOrcamentoExame.setServico(servico);
					servicoOrcamentoExame.setQuantidade(orc.getQuantidade());
					servicoOrcamentoExame.setOrcamentoExame(orcamentoExameOriginal);
					
					try {
						servicoOrcamentoExameDAO.persist(servicoOrcamentoExame);
						atualizado = true;
					} catch (Exception e) {
						throw new ObjectNotFoundException(
								"Erro ao inserir servicoOrçamento");
					}
					
				}
			}
			
			return atualizado ;
		
	}
	
	
	public boolean saveOrcamento( Long idAgendamento,List<OrcamentoExameDTO> orcamentoExameDTO) {
		
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());
		boolean orcamentoSalvo = false;
		
		if(atendimentoOriginal != null){
			OrcamentoExame orcamentoExameOriginal = new OrcamentoExame();
			ServicoOrcamentoExame servicoOrcamentoExame = new ServicoOrcamentoExame();
			orcamentoExameOriginal.setAtendimento(atendimentoOriginal);
			
			try {
				OrcamentoExame orcamentoExameatualizado = orcamentoExameDAO.persist(orcamentoExameOriginal);
				
				for (OrcamentoExameDTO orcamentoExameDTO2 : orcamentoExameDTO) {
					servicoOrcamentoExame = new ServicoOrcamentoExame();
					Servico servico = servicoDAO.searchByKey(Servico.class, orcamentoExameDTO2.getIdServico());
					
					servicoOrcamentoExame.setServico(servico);
					servicoOrcamentoExame.setQuantidade(orcamentoExameDTO2.getQuantidade());
					servicoOrcamentoExame.setOrcamentoExame(orcamentoExameatualizado);
					
					try {
						servicoOrcamentoExameDAO.persist(servicoOrcamentoExame);
						orcamentoSalvo = true;
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				
			} catch (Exception e) {
				System.out.println(e);
				throw new ObjectNotFoundException(
						"Erro ao inserir Orçamento do exame");
			}
			
			
		
		}else{
			Atendimento novoAtendimento = new Atendimento();
			novoAtendimento.setNmQueixaPaciente("");
			novoAtendimento.setDtAtendimento(Calendar.getInstance().getTime());
			novoAtendimento.setBoFinalizado(new Boolean(false));
			ServicoOrcamentoExame servicoOrcamentoExame = new ServicoOrcamentoExame();
			
			novoAtendimento.setAgendamento(agendamentoOriginal);
			
			try {
				atendimentoDAO.persist(novoAtendimento);
						try {
							
							OrcamentoExame orcamentoExame = new OrcamentoExame();
							
							
							
							orcamentoExame.setAtendimento(novoAtendimento);
							OrcamentoExame orcamentoExameatualizado = orcamentoExameDAO.persist(orcamentoExame);
							
							for (OrcamentoExameDTO orcamentoExameDTO2 : orcamentoExameDTO) {
								servicoOrcamentoExame = new ServicoOrcamentoExame();
								Servico servico = servicoDAO.searchByKey(Servico.class, orcamentoExameDTO2.getIdServico());
								
								servicoOrcamentoExame.setServico(servico);
								servicoOrcamentoExame.setQuantidade(orcamentoExameDTO2.getQuantidade());
								servicoOrcamentoExame.setOrcamentoExame(orcamentoExameatualizado);
								
								try {
									servicoOrcamentoExameDAO.persist(servicoOrcamentoExame);
									orcamentoSalvo = true;
								} catch (Exception e) {
									System.out.println(e);
									throw new ObjectNotFoundException(
											"Erro ao inserir Orçamento Servico do exame");
								}
							}
							
						} catch (Exception e) {
							System.out.println(e);
							throw new ObjectNotFoundException(
									"Erro ao registrar orçamento ");
						}
				
				
			} catch (Exception e) {
				System.out.println(e);
				throw new ObjectNotFoundException(
						"Erro ao registrar o novo atendimento");
			}
			
			
		}
		
		return orcamentoSalvo;
		
	}
	
	private OrcamentoExameDTO setaEspecialidade(List<ServicoExame> listRank, OrcamentoExameDTO dto){
		
		if(listRank != null && listRank.size() > 0){
			for (ServicoExame servicoExame : listRank) {
				if(servicoExame.getIdServico().equals(dto.getIdServico())){
					dto.setIdEspecialidade(servicoExame.getIdEspecialidade());
					
					return dto;
				
				}
			}
		}
		
		dto.setIdEspecialidade(-1L);
		
		return dto;
		
	}
	
	public List<OrcamentoExameDTO> getOrcamento(Long idAgendamento, Long idEspecialidade) {
		
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());
		List<ServicoOrcamentoExame> listSEO = null;
		List<OrcamentoExameDTO> listOEDTO = null;
		
		if(atendimentoOriginal != null){
			
			OrcamentoExame orcamentoExameOriginal = orcamentoExameDAO.getOrcamentoPorAtendimento(atendimentoOriginal.getId());
			
			if(orcamentoExameOriginal != null){
				
				listSEO = servicoOrcamentoExameDAO.getServicoExamePorOrcamento(orcamentoExameOriginal.getId());
				
				if(listSEO != null){
					listOEDTO = new ArrayList<OrcamentoExameDTO>();
					List<ServicoExame> listRank = funcionarioDAO.getAtendimentomedicoservicosexameView(idEspecialidade);
					
					
					
					for (ServicoOrcamentoExame seo : listSEO) {
						OrcamentoExameDTO orcamentoExameDTOAux = new OrcamentoExameDTO();
						orcamentoExameDTOAux.setIdAtendimento(seo.getOrcamentoExame().getAtendimento().getId());
						orcamentoExameDTOAux.setIdOrcamentoExame(seo.getOrcamentoExame().getId());
						orcamentoExameDTOAux.setIdServico(seo.getServico().getId());
						orcamentoExameDTOAux.setIdServicoOrcamentoExame(seo.getId());
						orcamentoExameDTOAux.setNmServico(seo.getServico().getNmServico());
						orcamentoExameDTOAux.setQuantidade(seo.getQuantidade());
						orcamentoExameDTOAux = setaEspecialidade(listRank,orcamentoExameDTOAux);
							
						
						listOEDTO.add(orcamentoExameDTOAux);
					}
				
					return listOEDTO;
					
				}
			}
		}
		
		return null;
		
	}
	
	
	public List<AtendimentomedicoDespesaOrcamentoExameView> getOrcamentoPorIdPaciente(Long idPaciente){
		
		return orcamentoExameDAO.getOrcamentoPorIdPaciente(idPaciente);
		
	}
	
	
	public List<Servico> getServicoPorOrcamento(Long idOrcamentoExame){
		
		 List<ServicoOrcamentoExame> listServOrc = servicoOrcamentoExameDAO.getServicoExamePorOrcamento(idOrcamentoExame);
		
		 if (listServOrc != null){
			 List<Servico> listServicos = new ArrayList<Servico>();
			 for (ServicoOrcamentoExame servicoOrcamentoExame : listServOrc) {
				 Servico servicoAux = new Servico();
				 servicoAux = servicoOrcamentoExame.getServico();
				 servicoAux.setQuantidade(servicoOrcamentoExame.getQuantidade() != null ? servicoOrcamentoExame.getQuantidade() : new Integer(1));
				 listServicos.add(servicoAux);
			}
			 
			 return listServicos;
		 }
		 
		 return null;
	}
	
	
	public byte[] getServicoOrcamentoPDF(Long idOrcamentoExame,Long idAgendamento) {

		List<ServicoOrcamentoExame> listServOrc = servicoOrcamentoExameDAO.getServicoExamePorOrcamento(idOrcamentoExame);
		AtendimentomedicoDespesaOrcamentoExameView amdoDTO = orcamentoExameDAO.getOrcamentoPorIdOrcamento(idOrcamentoExame);
		Agendamento agendamento = agendamentoDAO.getAgendamento(idAgendamento);
		String nmPaciente = null;
		
		
		if(agendamento.getDependente() != null){
			nmPaciente = agendamento.getDependente().getNmDependente();
		}else{
			nmPaciente = agendamento.getCliente().getNmCliente();
		}
		
		String crm  = agendamento.getProfissional().getNrCrmCro();
		
		
		 
		 List<Servico> listServicos = null;
		
		 if (listServOrc != null && amdoDTO != null){
			 listServicos = new ArrayList<Servico>();
			 for (ServicoOrcamentoExame servicoOrcamentoExame : listServOrc) {
				 Servico servicoAux = new Servico();
				 servicoAux = servicoOrcamentoExame.getServico();
				 servicoAux.setQuantidade(servicoOrcamentoExame.getQuantidade() != null ? servicoOrcamentoExame.getQuantidade() : new Integer(1));
				 listServicos.add(servicoAux);
			}
			 		
		 }else{
			  return null;
		 }
		 
		 
		
		RelatorioOrcamentoServico relatorioOrcamentoServico = new RelatorioOrcamentoServico(amdoDTO.getNmFuncionario(),nmPaciente,crm,listServicos);
		 
		ByteArrayOutputStream output = null;
		byte[] pdf = null;

		try {
			
			InputStream inputStream = getClass().getResourceAsStream(
			        "/jasper/OrcamentoExames1.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
			        .compileReport(design);

			HashMap<String, Object> params = new HashMap<String, Object>();
			
			InputStream caminhoImagemlogo = getClass().getResourceAsStream("/jasper/imagensJasper/LogoMedicSaude.jpg");
//			InputStream caminhoImagemlinha = getClass().getResourceAsStream("/jasper/imagensJasper/linha1.jpg");
//			InputStream caminhoImagemrodape = getClass().getResourceAsStream("/jasper/imagensJasper/rodape1.jpg");
			params.put("logomarca", caminhoImagemlogo);
//			params.put("linha", caminhoImagemlinha);
//			params.put("rodape", caminhoImagemrodape);
			

			JasperPrint jasperPrint = JasperFillManager.fillReport(
			        jasperReport, params, relatorioOrcamentoServico);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);

			pdf = output.toByteArray();

		} catch (JRException e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Orcamento",
			        ErrorType.ERROR, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e) {
				throw new AgendamentoException(
				        "Nao foi possivel gerar o relatorio em PDF - Orcamento",
				        ErrorType.ERROR, e);
			}

		}
		return pdf;

	}
	

}
