package br.com.medic.medicsystem.main.exception;

import javax.ejb.ApplicationException;

import br.com.medic.medicsystem.main.mapper.ErrorType;

@ApplicationException(rollback = false)
public class DespesaInformativeException extends GeneralException {

	public DespesaInformativeException(String message, ErrorType errorType) {
		super(message, errorType);
	}
	
	public DespesaInformativeException(String message, ErrorType errorType, Throwable e) {
		super(message, errorType, e);
	}

	private static final long serialVersionUID = 2752602977746710855L;

}
