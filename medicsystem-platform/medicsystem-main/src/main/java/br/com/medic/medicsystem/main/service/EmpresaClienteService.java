package br.com.medic.medicsystem.main.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.util.ValidaCPF;
import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.CoberturaPlanoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDependenteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaClienteDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.dao.EnderecoCobrancaDAO;
import br.com.medic.medicsystem.persistence.dao.GuiaDAO;
import br.com.medic.medicsystem.persistence.dao.PlanoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dto.ClienteConvDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Cidade;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.EnderecoCobranca;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.Plano;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.views.EmpresaClienteContratoClienteView;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class EmpresaClienteService {

	@Inject
	@Named("plano-dao")
	private PlanoDAO planoDAO;
	
	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("empresaCliente-dao")
	private EmpresaClienteDAO empresaClienteDAO;

	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;
	
	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;
	
	@Inject
	@Named("endereco-cobranca-dao")
	private EnderecoCobrancaDAO enderecoCobrancaDAO;
	
	@Inject
	@Named("empresagrupo-dao")
	private EmpresaGrupoDAO empresaGrupoDAO;
	
	@Inject
	@Named("contratodependente-dao")
	private ContratoDependenteDAO contratoDependenteDAO;

	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;

	@Inject
	@Named("guia-dao")
	private GuiaDAO guiaDAO;

	@Inject
	@Named("coberturaplano-dao")
	private CoberturaPlanoDAO coberturaPlanoDAO;

	@Inject
	@Named("contratocliente-dao")
	private ContratoClienteDAO contratoClienteDAO;

	@Inject
	private SecurityService securityService;
	
	@Inject
	private ContratoService contratoService;

	@Inject
	private DependenteService dependenteService;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
	
	public EmpresaCliente saveEmpresaCliente(EmpresaCliente empresaCliente, Integer tipoCobranca, Double vlFixo) {
		Cidade cidade = cidadeDAO.getCidade(empresaCliente.getCidade().getId());
		empresaCliente.setCidade(cidade);
		
		Bairro bairro = bairroDAO.getBairro(empresaCliente.getBairro().getId());
		empresaCliente.setBairro(bairro);
		
		empresaClienteDAO.persist(empresaCliente);
		criaClienteConveniada(empresaCliente, tipoCobranca,vlFixo);
		return empresaCliente;

	}
	
	
	private void criaClienteConveniada(EmpresaCliente empresaCliente, Integer tipoCobranca, Double vlFixo) {
		try {
			Cliente clienteConveniada = new Cliente();
			clienteConveniada.setNrCPF("00000000000");
			clienteConveniada.setNrRG("00000000");
			clienteConveniada.setNrCEP("00000000");
			clienteConveniada.setNmCliente(empresaCliente.getNmRazaoSocial());
			clienteConveniada.setNmComplemento("");
			clienteConveniada.setNmEmail("");
			clienteConveniada.setNmLogradouro("");
			clienteConveniada.setNmOcupacao("");
			clienteConveniada.setNmPontoReferencia("");
			clienteConveniada.setNrNumero("");
			clienteConveniada.setCidade(empresaCliente.getCidade());
			clienteConveniada.setBairro(empresaCliente.getBairro());
			clienteConveniada.setNrCodCliente(geraCodigoCliente());
			clienteConveniada.setBoNaoAssociado(true);
			clienteDAO.persist(clienteConveniada);

			EmpresaGrupo empresagrupo = new EmpresaGrupo();
			empresagrupo = securityService.getFuncionarioLogado().getUnidade().getEmpresaGrupo();
			
			Contrato contratoConveniada = new Contrato();
			contratoConveniada.setBoCadastradoBancoBrasil(false);
			contratoConveniada.setBoNaoFazUsoPlano(false);
			contratoConveniada.setDtContrato(new Date());
			contratoConveniada.setInformaPagamento(0);

			if (empresaCliente.getInTipoPagamento() == 1) {//despesa
				contratoConveniada.setVlContrato(0.0);
				contratoConveniada.setVlTotal(0.0);
			} else {
				if (tipoCobranca != null) {
					if (tipoCobranca == 0 && vlFixo != null) {
						contratoConveniada.setVlContrato(vlFixo);
						contratoConveniada.setVlTotal(vlFixo);
					} else {
						contratoConveniada.setVlContrato(vlFixo);
						contratoConveniada.setVlTotal(vlFixo);
					}
				}
			}
			
			contratoConveniada.setVlDesconto(0.0);
			contratoConveniada.setSituacao(0);
			contratoConveniada.setBoBloqueado(false);
			contratoConveniada.setEmpresaCliente(empresaCliente);
			contratoConveniada.setEmpresaGrupo(empresagrupo);
			contratoConveniada.setBoEmpresaCliente(true);
			contratoConveniada.setNrParcelaConvenio(1);

			Plano plano = new Plano();
			plano = planoDAO.searchByKey(Plano.class, 13L);

			contratoConveniada.setPlano(plano);

			// Associa o contrato criado ao cliente
			ContratoCliente contratoClienteConveniada = new ContratoCliente();
			contratoClienteConveniada.setCliente(clienteConveniada);
			contratoClienteConveniada.setContrato(contratoConveniada);
			contratoClienteConveniada.setBoAtivo(true);
			contratoClienteConveniada.setPlano(plano);
			contratoClienteConveniada.setInTipoCliente(3); // Cliente Empresarial
			contratoConveniada.getContratosCliente().add(contratoClienteConveniada);
			contratoConveniada.setNrContrato(geraCodigoContrato());
			Calendar calendar = new GregorianCalendar();
			int day = empresaCliente.getDiaVencimento();
			int month = calendar.get(Calendar.MONTH) + 1;
			int year = calendar.get(Calendar.YEAR);
			calendar.set(year, month, day); // data de vencimento do contrato
			Date dtVencimento = calendar.getTime();
			contratoConveniada.setDtVencimentoInicial(dtVencimento);
			contratoDAO.persist(contratoConveniada);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EmpresaCliente updateEmpresaCliente(EmpresaCliente empresaCliente, Long idContrato, Double vlTotal) {
		EmpresaCliente empCliente = empresaClienteDAO.searchByKey(EmpresaCliente.class, empresaCliente.getId());
		Contrato contrato = contratoDAO.getContrato(idContrato);
		contrato.setVlTotal(vlTotal);
		if(empCliente.getNmLogin() != null){
			empresaCliente.setNmLogin(empCliente.getNmLogin());
		}
		
		contrato = contratoDAO.update(contrato);
		empresaClienteDAO.update(empresaCliente);

		return empresaCliente;

	}

	public EmpresaCliente updateEmpresaCliente(EmpresaCliente empresaCliente) {

		Cidade cidade = cidadeDAO.getCidade(empresaCliente.getCidade().getId());
		empresaCliente.setCidade(cidade);

		Bairro bairro = bairroDAO.getBairro(empresaCliente.getBairro().getId());
		empresaCliente.setBairro(bairro);

		empresaCliente = empresaClienteDAO.update(empresaCliente);

		return empresaCliente;
	}

	public List<EmpresaCliente> getListaEmpresasAtivas() {

		return empresaClienteDAO.getListaEmpresasAtivas();
	}

	public List<EmpresaCliente> getListaEmpresasInativas() {

		return empresaClienteDAO.getListaEmpresasInativas();
	}

	public void inativarEmpresaCliente(Long id) {

		EmpresaCliente empresa = empresaClienteDAO.searchByKey(EmpresaCliente.class, id);
		empresa.setDtExclusao(new Date());
		empresa.setIdOperadorExclusao(securityService.getFuncionarioLogado().getId());
	}

	public void ativarEmpresaCliente(Long id) {

		EmpresaCliente empresa = empresaClienteDAO.searchByKey(EmpresaCliente.class, id);
		empresa.setDtExclusao(null);
		empresa.setIdOperadorExclusao(null);
		empresa.setIdOperadorAlteracao(securityService.getFuncionarioLogado().getId());
		empresa.setDtAlteracao(new Date());
	}

	private String geraCodigoContrato() {

		String ultimoNumeroGerado = contratoDAO.getUltimoCodigoGerado();

		Calendar calendar = Calendar.getInstance();
		int anoAtual = calendar.get(Calendar.YEAR);

		if (ultimoNumeroGerado == null)
			return String.format("%06d/%d", 1, anoAtual);

		int sequencial = Integer.parseInt(ultimoNumeroGerado.split("/")[0]);
		int ano = Integer.parseInt(ultimoNumeroGerado.split("/")[1]);

		return String.format("%06d/%d", ano == anoAtual ? sequencial + 1 : 1, anoAtual);
	}

	private String geraCodigoCliente() throws SQLException, NamingException {
		Integer codigo = clienteDAO.getUltimoCodigoGerado();
		return String.format("%06d", codigo == 0 ? 1 : codigo + 1);
	}

	public Cliente saveCliente(Cliente cliente) {
		cliente.setNrCPF(cleanupNumberOnlyVariables(cliente.getNrCPF()));
		cliente.setNrRG(cleanupNumberOnlyVariables(cliente.getNrRG()));
		cliente.setNrCEP(cleanupNumberOnlyVariables(cliente.getNrCEP()));

		if (!ValidaCPF.isCPF(cliente.getNrCPF())) {
			throw new IllegalArgumentException("[CPF Invalido].");
		}

		Cliente encontrato = clienteDAO.getClientePorCPF(cliente.getNrCPF());

		if (encontrato != null) {
			throw new IllegalArgumentException("[CPF Invalido/Cliente existente].");
		}

		cliente.setCidade(cliente.getCidade() != null && cliente.getCidade().getId() != null
				? cidadeDAO.getCidade(cliente.getCidade().getId())
				: null);
		cliente.setBairro(cliente.getBairro() != null && cliente.getBairro().getId() != null
				? bairroDAO.getBairro(cliente.getBairro().getId())
				: null);
		cliente.setDtInclusao(new Date());

		EnderecoCobranca enderecoCobranca = new EnderecoCobranca();
		// TODO get info from logged user or from default place
		enderecoCobranca.setNmLogradouro("AV. JOAQUIM NABUCO");
		enderecoCobranca.setNrNumero("2213");
		enderecoCobranca.setNmComplemento("");
		enderecoCobranca.setNrCep("69020031");
		enderecoCobranca.setNmPontoReferencia("");
		enderecoCobranca.setBoEnderecoIgual(false);
		enderecoCobranca.setCidade(cidadeDAO.getCidade(29L)); // Manaus
		enderecoCobranca.setBairro(bairroDAO.getBairro(878L)); // Centro

		enderecoCobranca = enderecoCobrancaDAO.persist(enderecoCobranca);
		cliente.setEnderecoCobranca(enderecoCobranca);

		cliente.setNrCodCliente(gerarCodigoCliente());

		cliente.setBoNaoAssociado(new Boolean(false));

		cliente = clienteDAO.persist(cliente);

		return cliente;
	}

	private String cleanupNumberOnlyVariables(String var) {
		if (var != null) {
			var = var.replaceAll("\\s", "");
			var = var.trim();
			var = var.replaceAll("[^0-9]", "");

			return var;
		}

		return null;
	}

	private String gerarCodigoCliente() {

		Integer codigo = clienteDAO.getUltimoCodigoGerado();

		return String.format("%06d", codigo == 0 ? 1 : codigo + 1);
	}

	public List<ClienteConvDTO> getDadosClienteByLoginConveniada(Integer inSituacao) {
		String user = securityService.getUserLogin();
		return empresaClienteDAO.getClientesConveniada(user, inSituacao);
	}

	public List<EmpresaClienteContratoClienteView> getDadosCliente(Long idConveniada) {

		return empresaClienteDAO.getDadosCliente(idConveniada);
	}

	public ContratoCliente addClienteOnConveniada(Cliente cliente) {
		if (cliente.getId() != null) {
			cliente = clienteDAO.searchByKey(Cliente.class, cliente.getId());
		} else {
			cliente = saveCliente(cliente);
		}
		if(cliente != null)
		{
			return createContratoCliente(cliente);
		}
		return null;
	}

	public ContratoCliente createContratoCliente(Cliente cliente) {
		cliente.setBoNaoAssociado(new Boolean(false));
		cliente.setDtExclusao(null);
		cliente = clienteDAO.updateCliente(cliente);

		Contrato contrato = new Contrato();
		EmpresaCliente empresaCliente = getEmpresaCliente();
		Contrato contratoEmpresa = getContratoEmpresaCliente(empresaCliente.getId());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, empresaCliente.getDiaVencimento());

		contrato.setDtVencimentoInicial(cal.getTime());
		contrato.setPlano(empresaClienteDAO.getPlanoEmpresaCliente(empresaCliente.getId()));
		contrato.setInformaPagamento(contratoEmpresa.getInformaPagamento());
		contrato.setBoBloqueado(contratoEmpresa.getBoBloqueado());

		if (empresaCliente.getInTipoCobranca() != null && empresaCliente.getInTipoCobranca().equals(1)) {// cobrança por
																											// vida
			contrato.setVlContrato(contratoEmpresa.getVlContrato());
			contrato.setVlDesconto(contratoEmpresa.getVlDesconto());
			contrato.setVlTotal(contratoEmpresa.getVlTotal());

		} else {
			contrato.setVlContrato(new Double(0));
			contrato.setVlDesconto(new Double(0));
			contrato.setVlTotal(new Double(0));
		}

		contrato.setEmpresaGrupo(empresaGrupoDAO.searchByKey(EmpresaGrupo.class, 9L)); // MEDIC LAB SAUDE // MEDIC LAB
																						// SERVICOS E ATIVIDADE MEDICA
																						// LTDA

		contrato.setSituacao(0); // Ativo
		contrato.setDtContrato(new Date());
		contrato.setBoNaoFazUsoPlano(false);

		contrato.setNrConta("");
		contrato.setNrDigitoVerificador("");
		contrato.setBoCadastradoBancoBrasil(false);
		contrato.setBanco(null);
		contrato.setAgencia(null);
		contrato.setRazao(null);

		contrato.setFuncionario(null);
		contrato.setEmpresaCliente(empresaCliente);
		contrato.setNrContrato(contratoService.geraCodigoContrato());
		contrato.setIdOperadorCadastro(4738L); // id do funcionario 'conveniada'
		contratoDAO.persist(contrato);
		ContratoCliente contratoCliente = new ContratoCliente();
		contratoCliente.setPlano(contrato.getPlano());
		contratoCliente.setBoAtivo(true);
		contratoCliente.setInTipoCliente(3);// Empresarial
		contratoCliente.setInVinculo(null);
		contratoCliente.setNrMatricula(null);
		contratoCliente.setOrgao(null);
		contratoCliente.setCredito(0);
		contratoCliente.setCliente(cliente);
		contratoCliente.setContrato(contrato);
		contratoClienteDAO.persist(contratoCliente);
		if (contrato.getId() != null && contratoCliente.getId() != null) {
			contratoService.inativarContratoNaoAssociado(cliente.getId());
			contratoService.updateAgendamentosNovosCliente(cliente.getId(), contrato);
		}
		return contratoCliente;
	}

	public ContratoDependente addContratodependente(Long idCliente, Dependente dependente) {

		EmpresaCliente empresaCliente = getEmpresaCliente();
		Contrato contratoTitular = empresaClienteDAO.getContratoEmpresaClienteByCliente(idCliente,
				empresaCliente.getId());
		Contrato contratoEmpresa = getContratoEmpresaCliente(empresaCliente.getId());

		// Fazer a query para validar se o cliente pertence a conveniada
		if (dependente.getId() != null) {
			ContratoDependente contratoAtivo = empresaClienteDAO.getContratoDepByClientesEmpresaConveniada(idCliente,
					empresaCliente.getId(), dependente.getId(), true);
			if (contratoAtivo != null && contratoAtivo.getId() != null) {
				contratoAtivo.setBoInativadoConveniada(false);
				contratoDependenteDAO.update(contratoAtivo);
				return contratoAtivo;
			}

		}

		if (dependente.getId() == null) {
			dependente = dependenteService.saveOnlyDependenteInCliente(dependente, idCliente);
		} else {
			dependente = dependenteService.getDependente(dependente.getId());
		}
		dependente.setBoFazUsoPlano(true);
		dependenteDAO.persist(dependente);
		ContratoDependente contratoDependente = new ContratoDependente();
		contratoDependente.setBlAutorizaDebAut(false);
		contratoDependente.setDependente(dependente);
		contratoDependente.setCreditoConveniada(0);
		contratoDependente.setInSituacao(0);

		contratoDependente.setContrato(contratoTitular);
		contratoDependente.setBoFazUsoPlano(true);
		contratoDependente.setBoInativadoConveniada(false);

		if (empresaCliente.getInTipoCobranca() != null && empresaCliente.getInTipoCobranca().equals(1)) {// cobrança por
																											// vida
			contratoTitular.setVlContrato(contratoEmpresa.getVlContrato() + contratoTitular.getVlContrato());
			contratoTitular.setVlDesconto(contratoEmpresa.getVlDesconto() + contratoTitular.getVlDesconto());
			contratoTitular.setVlTotal(contratoEmpresa.getVlTotal() + contratoTitular.getVlTotal());

		}

		contratoTitular.addContratoDependente(contratoDependente);

		contratoDAO.persist(contratoTitular);

		return contratoDependente;

	}

	public EmpresaCliente getEmpresaCliente() {
		String user = securityService.getUserLogin();
		EmpresaCliente empre = empresaClienteDAO.getEmpresaCliente(user);
		return empre;
	}

	public Contrato getContratoEmpresaCliente(Long idEmpresaCliente) {
		Contrato contratoEmpresa = empresaClienteDAO.getContratoEmpresaCliente(idEmpresaCliente);
		return contratoEmpresa;
	}

	/**
	 * inactive and resolve a private contract Contrato
	 * 
	 * @param idCliente
	 * @return Object
	 * @throws Exception
	 */
	public Contrato inactiveAndResolveContrato(Long idCliente) throws Exception{
		String user = securityService.getUserLogin();
		EmpresaCliente empre = empresaClienteDAO.getEmpresaCliente(user);
		Contrato contrato = empresaClienteDAO.getContratoEmpresaClienteByCliente(idCliente, empre.getId());
		if(null == contrato) {
			throw new Exception("Contrato não encontrado");
		}
		if(!clienteIsActive(contrato))
		{
			throw new Exception("Contrato já inativado");
		}
		contrato.setInTipoInativacao(0);
		contrato.setMotivo("INATIVAÇÃO POR EMPRESA CONTRATANTE");
		contrato.setSituacao(1); //0-ativo, 1-inativo
		contrato.setDtInativacao(new Date());
		contrato.setIdOperadorInativacao(null);
		contrato.setDtTerminoContrato(new Date());
		contratoDAO.update(contrato);
		inactiveDependentesByContrato(contrato);
		ContratoCliente contratoCliente = resolveContratosNaoAssociado(idCliente);
		return contratoCliente.getContrato();
	}
	
	private ContratoCliente resolveContratosNaoAssociado(Long idCliente) {
		ContratoCliente contratoCliente = contratoClienteDAO.getPrivateContratoByCliente(idCliente);
		if(null == contratoCliente)
		{
			contratoCliente = new ContratoCliente();
			contratoCliente.setContrato(new Contrato());
			Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
			contratoCliente.setCliente(cliente);
			cliente.setBoNaoAssociado(true);
			contratoCliente.getContrato().setNrContrato(contratoService.geraCodigoContrato());
		} else
		{
			System.out.println("updating old contract private");
		}
		contratoCliente.getContrato().setInTipoInativacao(null);
		contratoCliente.getContrato().setMotivo(null);
		contratoCliente.getContrato().setDtInativacao(null);
		contratoCliente.getContrato().setIdOperadorInativacao(null);
		contratoCliente.getContrato().setDtTerminoContrato(null);
		contratoCliente.getContrato().setBoCadastradoBancoBrasil(false);
		contratoCliente.getContrato().setBoNaoFazUsoPlano(false);
		contratoCliente.getContrato().setDtContrato(new Date());
		contratoCliente.getContrato().setInformaPagamento(0);
		contratoCliente.getContrato().setVlContrato(new Double(0));
		contratoCliente.getContrato().setVlDesconto(new Double(0));
		contratoCliente.getContrato().setVlTotal(new Double(0));
		contratoCliente.getContrato().setSituacao(0);
		contratoCliente.getContrato().setBoBloqueado(false);
		contratoCliente.getContrato().setEmpresaGrupo(this.getEmpresaGrupoDefault());
		contratoCliente.getContrato().setIdOperadorInativacao(null);
		contratoCliente.setBoAtivo(true);
		contratoCliente.setPlano(null);
		contratoCliente.setInTipoCliente(4); // Cliente Não Associado
		contratoDAO.persist(contratoCliente.getContrato());
		contratoCliente.setContrato(contratoCliente.getContrato());
		contratoClienteDAO.persist(contratoCliente);
		return contratoCliente;
	}

	private EmpresaGrupo getEmpresaGrupoDefault() {
		return empresaGrupoDAO.getEmpresaGrupoDefault();
	}
	
	private boolean clienteIsActive(Contrato contrato) {
		if (0 == contrato.getSituacao()) {
			return true;
		} else {
			return false;
		}
	}

	private void inactiveDependentesByContrato(Contrato contrato) {
		for (ContratoDependente condep : contrato.getContratosDependente()) {
			if (condep.getDtInativacao() == null || condep.getInSituacao() == 0) {
				condep.setDtInativacao(new Date());
				condep.setInSituacao(1);
			}
		}
	}

	public Contrato reativarContratoEmpresaCliente(Long idContratoCliente) throws Exception {
		String user = securityService.getUserLogin();
		EmpresaCliente empre = empresaClienteDAO.getEmpresaCliente(user);
		ContratoCliente contratoCliente = contratoClienteDAO.searchByKey(ContratoCliente.class, idContratoCliente);
		if (null == contratoCliente) {
			throw new Exception("Contrato (ContratoCliente) não encontrado");
		}
		if (clienteIsActive(contratoCliente.getContrato())) {
			throw new Exception("Contrato está ativado");
		}
		if (empre.getId() != contratoCliente.getContrato().getEmpresaCliente().getId()) {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).build());
		}
		contratoCliente.getContrato().setInTipoInativacao(null);
		contratoCliente.getContrato().setMotivo(null);
		contratoCliente.getContrato().setSituacao(0); //0-ativo, 1-inativo
		contratoCliente.getContrato().setDtInativacao(null);
		contratoCliente.getContrato().setIdOperadorInativacao(null);
		contratoCliente.getContrato().setDtTerminoContrato(null);
		contratoCliente.getContrato().setDtAlteracao(new Date());
		contratoDAO.update(contratoCliente.getContrato());
		contratoCliente = inactivePrivateContrato(contratoCliente.getCliente());
		return contratoCliente.getContrato();
	}

	private ContratoCliente inactivePrivateContrato(Cliente cliente) throws Exception {
		ContratoCliente contratoCliente = contratoClienteDAO.getPrivateContratoByCliente(cliente.getId());
		if (null == contratoCliente) {
			throw new Exception("Contrato (ContratoCliente) particular não encontrato!");
		}
		contratoCliente.getContrato().setInTipoInativacao(null);
		contratoCliente.getContrato().setMotivo("Reativando contrato de convêniada");
		contratoCliente.getContrato().setDtInativacao(new Date());
		contratoCliente.getContrato().setIdOperadorInativacao(null);
		contratoCliente.getContrato().setDtTerminoContrato(null);
		contratoCliente.getContrato().setBoNaoFazUsoPlano(false);
		contratoCliente.getContrato().setSituacao(1);
		contratoCliente.getContrato().setIdOperadorInativacao(null);
		contratoCliente.setBoAtivo(false);
		contratoDAO.persist(contratoCliente.getContrato());
		contratoClienteDAO.persist(contratoCliente);
		return contratoCliente;
	}

	public ContratoDependente updateInativarContratoEmpresaDependente(Long idCliente, Long idDependente) {
		String user = securityService.getUserLogin();
		EmpresaCliente empre = empresaClienteDAO.getEmpresaCliente(user);

		ContratoDependente contratoAtivo = empresaClienteDAO.getContratoDepByClientesEmpresaConveniada(idCliente,
				empre.getId(), idDependente, false);
		if (contratoAtivo != null && contratoAtivo.getId() != null) {
			contratoAtivo.setBoInativadoConveniada(true);
			return contratoDependenteDAO.update(contratoAtivo);
		}
		return null;
	}

	public List<Dependente> getDependentesByClientesEmpresaConveniada(Long idCliente) {

		EmpresaCliente empresaCliente = getEmpresaCliente();
		return empresaClienteDAO.getDependentesByClientesEmpresaConveniada(idCliente, empresaCliente.getId());

	}

	public List<Servico> getServicos(String name) {
		EmpresaCliente empresaCliente = this.getEmpresaCliente();
		Plano plano = this.getPlano(empresaCliente.getId());
		return servicoDAO.getServicosPorPlano(plano.getId(), name);
	}

	public Plano getPlano(long idEmpresaCliente) {
		return empresaClienteDAO.getPlanoEmpresaCliente(idEmpresaCliente);
	}

	public GenericPaginateDTO getGuias(int offset, int limit, Long id, String nmCliente, String nameBeneficiary,
			Integer status) {
		EmpresaCliente empresaCliente = this.getEmpresaCliente();
		GenericPaginateDTO paginator = new GenericPaginateDTO();
		paginator.setOffset(offset);
		paginator.setLimit(limit);
		paginator = guiaDAO.getListByConveniada(paginator, empresaCliente.getId(), id, nmCliente, nameBeneficiary,
				status);
		paginator.prepare();
		return paginator;
	}

	public Guia getGuia(Long id) {
		Guia guia = guiaDAO.getGuia(id);

		if (guia != null) {
			if (this.getEmpresaCliente().getId() != guia.getContrato().getEmpresaCliente().getId()) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		return guia;
	}

	public List<CoberturaPlano> getCoberturaPlano(Long idEmpresaCliente) {
		List<CoberturaPlano> listCoberturaPlano = new ArrayList<CoberturaPlano>();
		if (null == idEmpresaCliente) {
			idEmpresaCliente = this.getEmpresaCliente().getId();
		}
		Plano plano = empresaClienteDAO.getPlanoEmpresaCliente(idEmpresaCliente);
		if (plano != null) {
			listCoberturaPlano = coberturaPlanoDAO.getCoberturaPlanoPorPlano(plano.getId());
			return listCoberturaPlano;
		} else {
			return null;
		}
	}

	public List<Dependente> getDependentesByContrato(Long idContrato) {
		return dependenteDAO.getDependentesByContrato(idContrato);
	}
	
	public ContratoDependente addContratoDependenteByContrato(Long idContrato, Dependente dependente) {
		EmpresaCliente empresaCliente = getEmpresaCliente();
		Contrato contratoTitular = contratoDAO.searchByKey(Contrato.class, idContrato);
		Cliente cliente = clienteDAO.getClienteByContrato(idContrato);
		
		if(empresaCliente.getId() != contratoTitular.getEmpresaCliente().getId())
		{
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
		dependente = this.resolveDependente(dependente, cliente.getId());
		dependenteDAO.persist(dependente);
		ContratoDependente contratoDependente = this.resolveContratoDependente(dependente, contratoTitular);
		contratoDependente.setBlAutorizaDebAut(false);
		contratoDependente.setDependente(dependente);
		contratoDependente.setCreditoConveniada(0);
		contratoDependente.setInSituacao(0);
		contratoDependente.setContrato(contratoTitular);
		contratoDependente.setBoFazUsoPlano(true);
		contratoDependente.setBoInativadoConveniada(false);
		contratoDependente.setDtInativacao(null);
		if (empresaCliente.getInTipoCobranca() != null && empresaCliente.getInTipoCobranca().equals(1)) {// cobrança por vida
			Contrato contratoEmpresa = getContratoEmpresaCliente(empresaCliente.getId());
			contratoTitular.setVlContrato(contratoEmpresa.getVlContrato() + contratoTitular.getVlContrato());
			contratoTitular.setVlDesconto(contratoEmpresa.getVlDesconto() + contratoTitular.getVlDesconto());
			contratoTitular.setVlTotal(contratoEmpresa.getVlTotal() + contratoTitular.getVlTotal());
		}
		contratoTitular.addContratoDependente(contratoDependente);
		contratoDAO.persist(contratoTitular);
		return contratoDependente;
	}

	private ContratoDependente resolveContratoDependente(Dependente dependente, Contrato contrato) {
		ContratoDependente  contratoDependente = contratoClienteDAO.getByContratoAndDependente(contrato.getId(), dependente.getId());
		if(contratoDependente == null) {
			contratoDependente = new ContratoDependente();
		}
		return contratoDependente;
	}

	private Dependente resolveDependente(Dependente dependente, Long idCliente) {
		if (dependente.getId() == null) {
			dependente = dependenteService.saveOnlyDependenteInCliente(dependente, idCliente);
		} else {
			dependente = dependenteService.getDependente(dependente.getId());
		}
		return dependente;
	}

	public List<Dependente> getDependentesByContratoNoExclude(Long idContrato) {
		return empresaClienteDAO.getDependentesByContratoAndNo(idContrato);
	}
	
	public ContratoDependente inactiveContratoDependente(Long idContratoDependente) {
		EmpresaCliente empresaCliente = getEmpresaCliente();
		ContratoDependente contratoDependente = contratoDependenteDAO.searchByKey(ContratoDependente.class, idContratoDependente);
		if (empresaCliente.getInTipoCobranca() != null && empresaCliente.getInTipoCobranca().equals(1)) {// cobrança por vida
			Contrato contratoEmpresa = getContratoEmpresaCliente(empresaCliente.getId());
			contratoDependente.getContrato().setVlContrato(contratoDependente.getContrato().getVlContrato() - contratoEmpresa.getVlContrato());
			contratoDependente.getContrato().setVlDesconto(contratoDependente.getContrato().getVlDesconto() - contratoEmpresa.getVlDesconto());
			contratoDependente.getContrato().setVlTotal(contratoDependente.getContrato().getVlTotal() - contratoEmpresa.getVlTotal());
		}
		contratoDependente.setBoFazUsoPlano(false);
		contratoDependente.setInSituacao(1);
		contratoDependente.setDtInativacao(new Date());
		contratoDependenteDAO.update(contratoDependente);
		return contratoDependente;
	}

	/*add 24/04/2018*/
	public List<EmpresaCliente> getEmpresaClienteByCnpj(String cnpj) {
		List<EmpresaCliente> empresaCliente = empresaClienteDAO.getEmpresaByCNPJ(cnpj);
		
		return empresaCliente;
	}
	
	/*end*/
	
}
