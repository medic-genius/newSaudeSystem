package br.com.medic.medicsystem.main.appmobile.service;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implementa a comunicação com o Firebase Cloud Messaging 
 * para envio de push notifications.
 * @author Patrick Lima
 *
 */
public class PushNotificationService {
	private String HOST = "fcm.googleapis.com";
	private String URL = "/fcm/send";
	private String API_KEY = "AAAAUtbYmJ4:APA91bEvwL35lM7P0Xke8Nk-EgsYXOFD9_jbZUrs-46VVr9UHOTP3PFrsuIW-jkAHegt5d3Sd28wkU8_GKPZxs_88wZNPu8jndC8ufbH4a6lc2cr86c_A-Ac8rnlfrHEtCelF6GjTtDz";
	
	private CloseableHttpClient httpClient;
	
	public PushNotificationService() {
		this.httpClient =  HttpClients.createDefault();
	}
	
	public Boolean sendNotification(List<String> registrationIds, String title, 
			String message, String badge, Map<String, Object> data, String tag, String imagePath) {
		if(data == null) {
			data = new HashMap<String, Object>();
		}
		data.put("content-available", true);
		data.put("title", title);
		data.put("message", message);
		int notificationId = (new Random()).nextInt() & ((-1) >>> 1);
		data.put("notId", notificationId);
		data.put("sound", "default");
		data.put("badge", badge != null ? badge : "1");
		data.put("tag", tag != null ? tag : "");
		if(imagePath != null) {
			data.put("image", imagePath);
		}
		
		Map<String, Object> fields = new HashMap<String, Object>();
		if(registrationIds.size() == 1) {
			fields.put("to", registrationIds.get(0));
		} else {
			fields.put("registration_ids", registrationIds);
		}
		fields.put("data", data);
		
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Authorization", "key="+API_KEY);
		String entityStr = null;
		try {
			post.setURI(new URI(this.URL));
			entityStr = (new ObjectMapper()).writeValueAsString(fields);
			post.setEntity(new StringEntity(entityStr));
		} catch(Exception e) {
			
		}
		
		HttpHost target = new HttpHost(HOST, 443, "https");
		try {
			CloseableHttpResponse response = this.httpClient.execute(target, post);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK || 
					response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
				return true;				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
