package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.CentroCustoService;
import br.com.medic.medicsystem.persistence.model.CentroCusto;


@Path("/centrocusto")
public class CentroCustoController extends BaseController {

	@Inject
	private CentroCustoService centroCustoService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCentroCusto(@Valid CentroCusto centroCusto){
		CentroCusto salvo = centroCustoService.saveCentroCusto(centroCusto);
		return Response.created(URI.create("/centrocusto/" + salvo.getId())).build();
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCentroCusto(@Valid CentroCusto centroCusto, @PathParam("id") Long id){
		CentroCusto salvo = centroCustoService.saveCentroCusto(centroCusto);
		return Response.created(URI.create("/centrocusto/" + salvo.getId())).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	public CentroCusto getCentroCustoById(@PathParam("id") Long id){
		CentroCusto centroCusto = centroCustoService.getCentroCustoById(id);
		return centroCusto;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CentroCusto> getAllCentrosCusto(){
		List<CentroCusto> list = centroCustoService.getAllCentrosCusto();
		return list;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List<CentroCusto> getAllCentrosCusto(
			@QueryParam ("dtInicio") String dtInicio,
			@QueryParam ("dtFim") String dtFim,
			@QueryParam("idEmpresa") List<String> idEmpresa,
			@QueryParam("idCentroCusto") Long idCentroCusto,
			@QueryParam("tipoStatus") Integer tipoStatus){
		
	
		DateTimeFormatter dtForm = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter mesForm = DateTimeFormatter.ofPattern("MM/yyyy");
		YearMonth mesFinal = YearMonth.parse(dtFim, dtForm);
		List<CentroCusto> listResult = new ArrayList<CentroCusto>();
		for(YearMonth mes = YearMonth.parse(dtInicio, dtForm);
				! mes.isAfter(mesFinal);
				mes = mes.plusMonths(1)) {
			String[] splitMesAno =  mes.format(mesForm).split("/");
			int mesSplit = Integer.valueOf(splitMesAno[0]);
			int anoSplit = Integer.valueOf(splitMesAno[1]);
			
			listResult.addAll(centroCustoService.getAllCentrosCusto(mesSplit, anoSplit, idEmpresa, idCentroCusto, tipoStatus));
		}
		
		return listResult;
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteCentroCusto(@PathParam("id") Long id){
		centroCustoService.deleteCentroCusto(id);
		return Response.ok().build();
	}
	
	
	@GET
	@Path("/allcentrocusto")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CentroCusto> getAllCentroCusto(){
		List<CentroCusto> list = centroCustoService.getAllCentroCusto();
		return list;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/busca")
	public List<CentroCusto> getBuscaCentroCusto(){
		 return centroCustoService.getBuscaCentroCusto(
				 getQueryParam("nmCentroCusto", String.class),
				 getQueryParam("start", Integer.class),
				 getQueryParam("size", Integer.class)
				 );
	}
	
}
