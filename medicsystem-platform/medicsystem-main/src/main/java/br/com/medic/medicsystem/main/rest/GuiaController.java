package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.GuiaService;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Guia;

@Path("/guia")
@RequestScoped
public class GuiaController {
	
	@Inject
	private GuiaService guiaService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createGuia(@Valid Guia guia) {
		Guia guiaPersisted = guiaService.saveGuia(guia);
		return Response.status(Response.Status.CREATED.getStatusCode()).entity(guiaPersisted).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GenericPaginateDTO list(
			@QueryParam("offset") @DefaultValue("1") int offset, 
			@QueryParam("limit") @DefaultValue("10") int limit,
			@QueryParam("id") Long id,
			@QueryParam("nm_cliente") String nmCliente) {
		return guiaService.getGuias(offset, limit, id, nmCliente);
	}
	
	@GET
	@Path("/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Guia getGuia(@PathParam("id") Long id) {
		return guiaService.getGuia(id);
	}
	
	@GET
	@Path("/cliente")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listByCliente(
			@QueryParam("offset") @DefaultValue("1") int offset,
			@QueryParam("limit") @DefaultValue("20") int limit, 
			@QueryParam("id_cliente") Long idCliente) {
		
		GenericPaginateDTO paginator;
		if (null != idCliente) {
			paginator = guiaService.getGuiasByCliente(offset, limit, idCliente);
		} else {
			paginator = guiaService.getGuias(offset, limit, null, null);
		}

		if (paginator.getTotal() > 0) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(paginator).build();
		} else {
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	@DELETE
	@Path("/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteGuia(@PathParam("id") Long id) {
		try {
			Guia guia = guiaService.deleteGuia(id);
			if(guia != null)
			{
				return Response.status(200).type(MediaType.APPLICATION_JSON).entity(guia).build();
			} else {
				return Response.status(404).type(MediaType.APPLICATION_JSON).build();
			}
		} catch (WebApplicationException wae) {
			System.out.println("It does not happen!");
			return null;
		} catch (Exception e) {
			String http403 = "javax.ejb.EJBException: javax.ws.rs.WebApplicationException: HTTP 403 Forbidden";
			if(http403.equals(e.toString()))
			{
				return Response.status(403).type(MediaType.APPLICATION_JSON).build();
			}
			return null;
		}
	}
	
	@PUT
	@Path("/reactive/{id:[0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response reactive(@PathParam("id") Long id) {
		try {
			Guia guia = guiaService.reactive(id);
			if(guia != null)
			{
				return Response.status(200).type(MediaType.APPLICATION_JSON).entity(guia).build();
			} else {
				return Response.status(404).type(MediaType.APPLICATION_JSON).build();
			}
		} catch (WebApplicationException wae) {
			System.out.println("It does not happen!");
			return null;
		} catch (Exception e) {
			String http403 = "javax.ejb.EJBException: javax.ws.rs.WebApplicationException: HTTP 403 Forbidden";
			if(http403.equals(e.toString()))
			{
				return Response.status(403).type(MediaType.APPLICATION_JSON).build();
			}
			return null;
		}
	}	
}
