package br.com.medic.medicsystem.main.vindi.data.parameters;

public class Discount {
	private String discount_type;
	private Number percentage;
	private Number amount;
	private Integer quantity;
	private Integer cycles;
	
	public String getDiscount_type() {
		return discount_type;
	}
	public void setDiscount_type(String discount_type) {
		this.discount_type = discount_type;
	}
	public Number getPercentage() {
		return percentage;
	}
	public void setPercentage(Number percentage) {
		this.percentage = percentage;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getCycles() {
		return cycles;
	}
	public void setCycles(Integer cycles) {
		this.cycles = cycles;
	}
}
