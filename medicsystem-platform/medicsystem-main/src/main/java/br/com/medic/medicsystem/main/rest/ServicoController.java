package br.com.medic.medicsystem.main.rest;

import java.net.URI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.service.ServicoService;
import br.com.medic.medicsystem.persistence.dto.DataCoberturaDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoCredenciadaDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.TipoConsultaDTO;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Servico;

@Path("/servicos")
@RequestScoped
public class ServicoController extends BaseController {

	@Inject
	private ServicoService servicoService;

	@Context
	protected UriInfo info;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Servico> getAllServicos() {

		return servicoService.getServicos(
		        getQueryParam("nmServico", String.class),
		        getQueryParam("start", Integer.class),
		        getQueryParam("size", Integer.class));
	}

	@GET
	@Path("/{ids}/credenciadas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServicoCredenciadaDTO> getCredenciadas(
	        @PathParam("ids") String idsServico,
	        @QueryParam("idCliente") Long idCliente,
	        @QueryParam("idDependente") Long idDependente,
	        @QueryParam("idContrato") Long idContrato,
	        @QueryParam("tipoCredenciada") Integer tipoCredenciada) {

		if (idsServico != null && idCliente != null && idContrato != null
		        && tipoCredenciada != null) {
			return servicoService.getCredenciadas(idsServico, idCliente,
			        idDependente, idContrato, tipoCredenciada);
		}

		throw new IllegalArgumentException();

	}

	@GET
	@Path("/{ids}/profissionais")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServicoProfissionalDTO> getProfissionais(
	        @PathParam("ids") String idsServico,
	        @QueryParam("idCliente") Long idCliente,
	        @QueryParam("idDependente") Long idDependente,
	        @QueryParam("idContrato") Long idContrato) {

		if (idsServico != null && idCliente != null && idContrato != null) {
			return servicoService.getProfissionais(idsServico, idCliente,
			        idDependente, idContrato);
		}

		throw new IllegalArgumentException();

	}

	@PUT
	@Path("/{id:[0-9][0-9]*}/despesaServicoDisponivel")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaServico getDespesaServicoDisponivel(
	        @PathParam("id") Long idServico,
	        @QueryParam("idCliente") Long idCliente,
	        @QueryParam("idDependente") Long idDependente,
	        @QueryParam("idContrato") Long idContrato,
	        DespesaServico despesaServicoAtual) {

		if (idServico != null && idCliente != null && idContrato != null
		        && despesaServicoAtual != null) {
			return servicoService.getDespesaServicoDisponivel(idServico,
			        idCliente, idDependente, idContrato, despesaServicoAtual);
		}

		throw new IllegalArgumentException();

	}
	
	@GET
	@Path("/tipoServico/{inTipo:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Servico> getServicosByTipo(@PathParam("inTipo") Integer inTipo) {

		if (inTipo != null) {
			return servicoService.getServicosByTipo(inTipo);
		}

		throw new IllegalArgumentException();

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/valores")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServicoDTO> getValores(
	        @PathParam("id") String idServico) {

		if (idServico != null) {
			return servicoService.getValoresServico(idServico);
		}

		throw new IllegalArgumentException();

	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/tipoconsulta")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TipoConsultaDTO> getTipoConsulta(
	        @PathParam("id") Long idServico,
	        @QueryParam("idFuncionario") Long idFuncionario,
	        @QueryParam("idEspecialidade") Long idEspecialidade) {

		if (idServico != null) {
			return servicoService.getTipoConsulta(idServico, idFuncionario, idEspecialidade );
		}

		throw new IllegalArgumentException();

	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/ultimoagendamentocobertovalido")
	@Produces(MediaType.APPLICATION_JSON)
	public DataCoberturaDTO getTipoConsulta(
	        @PathParam("id") Long idServico,
	        @QueryParam("idPaciente") Long idPaciente,
	        @QueryParam("tipoPaciente") String tipoPaciente,
	        @QueryParam("idContrato") Long idContrato) {

		if (idServico != null) {
			return servicoService.getUltimoServicoCobertoValido(idServico, idPaciente, tipoPaciente, idContrato);
		}else{
			return null;
		}


	}
	
	//------------------------------------------------------------------
	// APLICATIVO
	
	@GET
	@Path("/{id:[0-9][0-9]*}/agendamentocoberto/ultimo")
	public AppResponse getUltimoAgendamentoCoberto(
			@PathParam("id") Long idServico,
	        @QueryParam("idContrato") Long idContrato) {
		if(idServico != null && idContrato != null) {
			return servicoService.getUltimoServicoCobertoValido(idServico, idContrato);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		return new FailResponse(-10, r);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateServico(Servico servico) {

		if (servico != null) {
			Servico servicoAtualizado = servicoService.updateServico(servico);

			if (servicoAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createServico(Servico Servico) {
		
		Servico salvo = servicoService.saveServico(Servico);
		
		return Response.created(URI.create("/servicos/"+ salvo.getId())).build();
	
	}
	
	@PUT
	@Path("/inativar/{id:[0-9][0-9]*}")
	public Response inativarServico(@PathParam("id") Long id){
		
		servicoService.inativarServico(id);
		return Response.ok().build();
		
	}
	
}