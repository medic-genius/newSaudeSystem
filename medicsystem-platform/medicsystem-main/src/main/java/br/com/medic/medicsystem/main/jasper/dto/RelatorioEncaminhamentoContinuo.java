package br.com.medic.medicsystem.main.jasper.dto;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.persistence.model.DespesaServico;

/**
 * Esta classe representa... Limitacoes: Exemplo: (opcional) Autor: root
 * Referencias:
 */
public class RelatorioEncaminhamentoContinuo implements JRDataSource {

	/**
	 * Comentarios de implementacao
	 */
	private DecimalFormat formatador = new DecimalFormat();
	private NumberFormat nf = new DecimalFormat("0");
	private Iterator<DespesaServico> itrEncaminhamento;
	private DespesaServico valorAtual;
	private boolean irParaProximoEncaminhamento = true;
	private String funcionarioLogado;

	public RelatorioEncaminhamentoContinuo(List<DespesaServico> lista,
	        String funcionarioLogado) {

		this.formatador.setMinimumFractionDigits(2);
		this.itrEncaminhamento = lista.iterator();
		this.funcionarioLogado = funcionarioLogado;
	}

	public boolean next() throws JRException {

		this.valorAtual = this.itrEncaminhamento.hasNext() ? this.itrEncaminhamento
		        .next() : null;
		this.irParaProximoEncaminhamento = (this.valorAtual != null);

		return this.irParaProximoEncaminhamento;
	}

	public Object getFieldValue(JRField campo) throws JRException {

		Object valor = null;

		DespesaServico encaminhamento = (DespesaServico) valorAtual;

		if ("nrMatricula".equals(campo.getName())) {
			valor = encaminhamento.getDespesa().getContratoCliente()
			        .getNrMatricula() != null ? encaminhamento.getDespesa()
			        .getContratoCliente().getNrMatricula() : encaminhamento
			        .getDespesa().getContratoCliente().getCliente()
			        .getNrCodCliente();

		} else if ("nmTitular".equals(campo.getName())) {
			valor = encaminhamento.getDespesa().getContratoCliente()
			        .getCliente().getNmCliente().toUpperCase();

		} else if ("nmPaciente".equals(campo.getName())) {
			valor = encaminhamento.getDespesa().getContratoDependente() != null ? encaminhamento
			        .getDespesa().getContratoDependente().getDependente()
			        .getNmDependente().toUpperCase()
			        : encaminhamento.getDespesa().getContratoCliente()
			                .getCliente().getNmCliente().toUpperCase();

		} else if ("nrFone".equals(campo.getName())) {
			valor = encaminhamento.getDespesa().getContratoCliente()
			        .getCliente().getNrTelefone();

		} else if ("nrEncaminhamento".equals(campo.getName())) {
			valor = encaminhamento.getEncaminhamento().getNrEncaminhamento();

		} else if ("nmServico".equals(campo.getName())) {
			valor = encaminhamento.getServico().getNmServico();

		} else if ("nmLogado".equals(campo.getName())) {
			valor = funcionarioLogado;

		} else if ("nmExecutor".equals(campo.getName())) {
			valor = encaminhamento.getEncaminhamento().getProfissional() != null ? encaminhamento
			        .getEncaminhamento().getProfissional().getNmFuncionario()
			        : encaminhamento.getEncaminhamento().getCredenciada()
			                .getNmFantasia();

		} else if ("nmOrgao".equals(campo.getName())) {
			valor = encaminhamento.getDespesa().getContratoCliente().getOrgao() != null ? encaminhamento
			        .getDespesa().getContratoCliente().getOrgao().getNmOrgao()
			        : "";

		} else if ("nmObservacao".equals(campo.getName())) {
			valor = "";

		} else if ("qtServico".equals(campo.getName())) {
			nf.setMinimumIntegerDigits(2);
			valor = nf.format(encaminhamento.getQtServico());
		}
		return valor;
	}

	public Date getData() {
		return new Date();
	}

}
