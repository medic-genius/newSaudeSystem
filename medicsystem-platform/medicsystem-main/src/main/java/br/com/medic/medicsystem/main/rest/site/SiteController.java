package br.com.medic.medicsystem.main.rest.site;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.service.AgendamentoService;
import br.com.medic.medicsystem.main.service.ClienteService;
import br.com.medic.medicsystem.main.service.EspecialidadeService;
import br.com.medic.medicsystem.main.service.site.HorarioService;
import br.com.medic.medicsystem.main.util.SimpleKVGenericDTO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoWebDTO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaSiteDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteSearchDTO;
import br.com.medic.medicsystem.persistence.dto.PagamentoAgendamentoDTO;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.EmailMessage;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoProfissionalView;


@Path("/site")
@RequestScoped
public class SiteController extends BaseController {
	
	
	@Inject
	private HorarioService horarioService;
	
	@Inject
	private AgendamentoService agendamentoService;
	
	@Inject
	private EspecialidadeService especialidadeService;
	
	@Inject
	private ClienteService clienteService;
	
	@GET
	@Path("/especialidades")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Especialidade> getEspecialidadesAtivas() {

		return especialidadeService.getEspecialidadesAtivas();

	}
	
	@GET
	@Path("/horarios/data/atendimento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentoProfissionalView> getDiasSemAtendimentoProfissionalTeste(
	        @QueryParam("idEspecialidade") Long idEspecialidade) {
		if (idEspecialidade != null) {

			return horarioService.getDiasAtendimentoProfissional(idEspecialidade);
		} else {

			throw new IllegalArgumentException();
		}

	}
	
	@GET
	@Path("/horarios/horario/atendimento")
	@Produces(MediaType.APPLICATION_JSON)
	public HashMap<String, List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>>> getDiasSemAtendimentoProfissionalTeste(
	        @QueryParam("idProfissional") Long idProfissional,
	        @QueryParam("idServico") Long idServico,
	        @QueryParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("idUnidade") Long idUnidade,
	        @QueryParam("data") String data,
	        @QueryParam("boParticular") Boolean boParticular) {
		if (idEspecialidade != null && idProfissional != null && idServico != null && idUnidade != null && data!= null) {

			return horarioService.getHorariosAtendimentoProfissional(idProfissional, idServico, idEspecialidade, idUnidade, data, boParticular);
		} else {

			throw new IllegalArgumentException();
		}

	}
	
	@POST
	@Path("/agendamentos/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaSiteDTO createAgendamento(AgendamentoWebDTO agendamento,
			@QueryParam("source") String client) {
		Despesa despesa = agendamentoService.createAgendamento(agendamento, client);
		agendamentoService.setConsultorioAgendamento(despesa.getAgendamento());
		
		DespesaSiteDTO despesaSite = null;
		
		if(despesa != null ){
			
			String nomePlano = despesa.getAgendamento().getContrato().getPlano() != null ? 
					despesa.getAgendamento().getContrato().getPlano().getNmPlano() : "Particular";
			
			
			 despesaSite = new DespesaSiteDTO(despesa.getAgendamento().getId(),
					despesa.getAgendamento().getIsConvenio(), despesa.getVlDespesaTotal(), despesa.getAgendamento().getInStatus(),
					nomePlano, despesa.getAgendamento().getContrato().getNrContrato());
			 
			 try {
				 agendamentoService.sendEmailConsultaMarcada(despesa, agendamento.getHorarioPrevisto());
			} catch (Exception e) {
				System.out.println("Erro ao enviar email");
			}
			
			
		}
		return despesaSite;
	}
	
	@POST
	@Path("/agendamentos/pagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public AssinaturaRecorrenteResultDTO createPagamentoAvulso(
			@QueryParam("source") String source,
			PagamentoAgendamentoDTO data) {
		if(data != null && data.getFormaPagamento() != null && 
				data.getIdAgendamento() != null ) {
			return agendamentoService.pagarAgendamento(data, source);
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus("fail");
		result.setMessage("Dados não fornecidos ou ausentes");
		return result;
	}
	
	@GET
	@Path("/search/{cpf:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public PacienteSearchDTO getUserByCPF(@PathParam("cpf") String nrCpf) {
		if(nrCpf != null && nrCpf.length() == 11) {
			return clienteService.searchUserByCPF(nrCpf);
		}
		return null;
	}

	@GET
	@Path("/job/agendamentos_nao_pagos/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Agendamento> getAgendamentosExcluir() {
		return agendamentoService.deleteAgendamentosFromSite();
	}
	

	@POST
	@Path("/send_email")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendEmail(EmailMessage emailMessage) {
		Boolean emailSent = agendamentoService.sendEmail(emailMessage);
		if (emailSent) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).build();
		} else {
			return Response.status(400).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	
}
