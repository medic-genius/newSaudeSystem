package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import br.com.medic.medicsystem.main.service.EstatisticaService;
import br.com.medic.medicsystem.persistence.dto.AgendamentoPrevisaoDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResuPagamentoDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResultDTO;
import br.com.medic.medicsystem.persistence.dto.UnidadeDTO;



@Path("/estatistica")
@RequestScoped
public class EstatisticaController extends BaseController {
	
	@Inject
	private EstatisticaService estatisticaService;
	
	@GET
	@Path("/{idProfissional:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public AtendimentoProfissionalResultDTO getRelarioAtendimentoMedico
	(@PathParam ("idProfissional") Long idProfissional,
	 @QueryParam("dataInicio") String dataInicio, 
	 @QueryParam("dataFim")	String dataFim ){
		
		if (idProfissional == null || dataInicio == null || dataFim == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return estatisticaService.getRelarioAtendimentoMedico(idProfissional, dataInicio, dataFim);
		
	}
	
	@GET
	@Path("/relatoriopagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public AtendimentoProfissionalResuPagamentoDTO getRelarioAtendimentoMedicoPagamento
	(
	 @QueryParam("dataInicio") String dataInicio, 
	 @QueryParam("dataFim")	String dataFim ){
		
		if (dataInicio == null || dataFim == null) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return estatisticaService.getRelarioAtendimentoMedicoPagamento(dataInicio, dataFim);
		
	}
			
	@GET
	@Path("/unidade/{idUnidade:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public AtendimentoProfissionalResultDTO getRelarioAtendimentoProfissionalUnidade
	(@PathParam ("idUnidade") Long idUnidade,
	 @QueryParam("dataInicio") String dataInicio, 
	 @QueryParam("dataFim")	String dataFim ){
		
		if (idUnidade == null || dataInicio == null || dataFim == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return estatisticaService.getRelarioAtendimentoProfissionalUnidade(idUnidade, dataInicio, dataFim);
		
	}
	
	@GET
	@Path("/previsao/agendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoPrevisaoDTO> getRelarioAtendimentoProfissionalUnidade(
			@QueryParam("idUnidade")Long idUnidade) {
		return estatisticaService.getPrevisaoAgendamento(idUnidade);
		
	}
	
	@GET
	@Path("/email/agendamento")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response enviarPorEmail(@QueryParam("email") List<String> email){
		if(email != null && email.size() > 0){
			
			estatisticaService.enviarAgendamentobyEmail(email);
			
			ResponseBuilder response = Response.ok();			
		    return response.build();
		}
		else{
			
			throw new IllegalArgumentException();
		}		
	}
	
	@GET
	@Path("/email/aprazamento")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response enviarAprazamentoPorEmail(@QueryParam("email") List<String> email){
				
		if(email != null && email.size() > 0){
			
			estatisticaService.enviarAprazamentobyEmail(email);
			
			ResponseBuilder response = Response.ok();			
		    return response.build();
		}
		else{
			
			throw new IllegalArgumentException();
		}		
	}

}
