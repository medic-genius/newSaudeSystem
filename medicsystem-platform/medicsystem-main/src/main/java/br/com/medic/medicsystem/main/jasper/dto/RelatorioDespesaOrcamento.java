package br.com.medic.medicsystem.main.jasper.dto;

import java.text.DecimalFormat;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.Despesa;

public class RelatorioDespesaOrcamento {

	private String nrMatricula;

	private String nmCliente;

	private String nmOrgao;

	private String nmPlano;

	private String nmUsuario;

	private String vlDespesa;

	private List<RelatorioDespesaServico> servicosDespesa;

	private List<RelatorioDespesaParcela> parcelasDespesa;

	private List<RelatorioDespesaEncaminhamento> encaminhamentosDespesa;

	public RelatorioDespesaOrcamento(Despesa despesa,
	        List<RelatorioDespesaServico> servicosDespesa) {

		DecimalFormat formatador = new DecimalFormat();
		formatador.setMinimumFractionDigits(2);

		this.nrMatricula = despesa.getContratoCliente().getCliente()
		        .getNrCodCliente();
		this.nmCliente = despesa.getContratoCliente().getCliente()
		        .getNmCliente();
		this.nmOrgao = (despesa.getContratoCliente() != null
		        && despesa.getContratoCliente().getContrato() != null
		        && despesa.getContratoCliente().getContrato().getEmpresaGrupo() != null ? despesa
		        .getContratoCliente().getContrato().getEmpresaGrupo()
		        .getNmFantasia()
		        : despesa.getUnidade().getEmpresaGrupo().getNmFantasia());
		this.nmPlano = (despesa.getContratoCliente().getPlano() != null ? despesa
		        .getContratoCliente().getPlano().getNmPlano()
		        : "NAO ASSOCIADO");
		this.nmUsuario = ((despesa.getContratoDependente() != null && despesa
		        .getContratoDependente().getDependente() != null) ? despesa
		        .getContratoDependente().getDependente().getNmDependente()
		        : despesa.getContratoCliente().getCliente().getNmCliente());

		this.vlDespesa = formatador.format(despesa.getVlDespesaAberto());

		this.servicosDespesa = servicosDespesa;

	}

	public String getNmCliente() {

		return nmCliente;
	}

	public void setNmCliente(String nmCliente) {

		this.nmCliente = nmCliente;
	}

	public String getNmOrgao() {

		return nmOrgao;
	}

	public void setNmOrgao(String nmOrgao) {

		this.nmOrgao = nmOrgao;
	}

	public String getNmPlano() {

		return nmPlano;
	}

	public void setNmPlano(String nmPlano) {

		this.nmPlano = nmPlano;
	}

	public String getNmUsuario() {

		return nmUsuario;
	}

	public void setNmUsuario(String nmUsuario) {

		this.nmUsuario = nmUsuario;
	}

	public String getNrMatricula() {

		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {

		this.nrMatricula = nrMatricula;
	}

	public String getVlDespesa() {

		return vlDespesa;
	}

	public void setVlDespesa(String vlDespesa) {

		this.vlDespesa = vlDespesa;
	}

	public List<RelatorioDespesaEncaminhamento> getEncaminhamentosDespesa() {

		return encaminhamentosDespesa;
	}

	public void setEncaminhamentosDespesa(
	        List<RelatorioDespesaEncaminhamento> encaminhamentosDespesa) {

		this.encaminhamentosDespesa = encaminhamentosDespesa;
	}

	public List<RelatorioDespesaParcela> getParcelasDespesa() {

		return parcelasDespesa;
	}

	public void setParcelasDespesa(List<RelatorioDespesaParcela> parcelasDespesa) {

		this.parcelasDespesa = parcelasDespesa;
	}

	public List<RelatorioDespesaServico> getServicosDespesa() {

		return servicosDespesa;
	}

	public void setServicosDespesa(List<RelatorioDespesaServico> servicosDespesa) {

		this.servicosDespesa = servicosDespesa;
	}

}
