package br.com.medic.medicsystem.main.rest.superlogica;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.service.CobrancaService;
import br.com.medic.medicsystem.main.service.superlogica.ClienteServiceSP;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.superlogica.ClienteSP;


@Path("/superlogica/cliente")
@RequestScoped
public class ClienteController extends BaseController{

	@Inject
	ClienteServiceSP clienteService;
	
	@Inject
	CobrancaService cobrancaService;
	
	@POST
	@Path("/cadastrarcliente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> cadastrarCliente(List<ClienteSP> clientes){
		return clienteService.cadastrarCliente(clientes);
	}
	
	@POST
	@Path("/compartilhado/cadastrarcliente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> cadastrarClienteCompartilado(List<ClienteSP> clientes){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(  getQueryParam("nrIdEmpresaFinanceiro", Long.class));
		
		return clienteService.cadastrarClienteCompartilhado(clientes, empresa);
	}

}
