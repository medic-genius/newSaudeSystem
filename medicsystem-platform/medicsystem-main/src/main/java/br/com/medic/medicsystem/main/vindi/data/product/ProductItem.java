package br.com.medic.medicsystem.main.vindi.data.product;

import java.util.List;

import br.com.medic.medicsystem.main.vindi.data.summary.Discount;

public class ProductItem {
	private Integer id;
	private String status;
	private Integer cycles;
	private Integer quantity;
	private String created_at;
	private String updated_at;
	private br.com.medic.medicsystem.main.vindi.data.summary.Product product;
	private PricingSchema pricing_schema;
	private List<Discount> discounts;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCycles() {
		return cycles;
	}
	public void setCycles(Integer cycles) {
		this.cycles = cycles;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public br.com.medic.medicsystem.main.vindi.data.summary.Product getProduct() {
		return product;
	}
	public void setProduct(br.com.medic.medicsystem.main.vindi.data.summary.Product product) {
		this.product = product;
	}
	public PricingSchema getPricing_schema() {
		return pricing_schema;
	}
	public void setPricing_schema(PricingSchema pricing_schema) {
		this.pricing_schema = pricing_schema;
	}
	public List<Discount> getDiscounts() {
		return discounts;
	}
	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}
}
