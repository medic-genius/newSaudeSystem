package br.com.medic.medicsystem.main.util;

public class SimpleGenericDTO<T> {

	private T data;
	
	public SimpleGenericDTO(T data) {
		this.data = data;
    }

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
