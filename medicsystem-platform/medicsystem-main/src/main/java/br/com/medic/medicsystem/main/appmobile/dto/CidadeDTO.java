package br.com.medic.medicsystem.main.appmobile.dto;

public class CidadeDTO {
	private Long idCidade;
	
	private String nmCidade;
	
	private String nmUF;
	
	public CidadeDTO() {
		
	}
	
	public CidadeDTO(Long idCidade, String nmCidade, String nmUF) {
		this.idCidade = idCidade;
		this.nmCidade = nmCidade;
		this.nmUF = nmUF;
	}

	public Long getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Long idCidade) {
		this.idCidade = idCidade;
	}

	public String getNmCidade() {
		return nmCidade;
	}

	public void setNmCidade(String nmCidade) {
		this.nmCidade = nmCidade;
	}

	public String getNmUF() {
		return nmUF;
	}

	public void setNmUF(String nmUF) {
		this.nmUF = nmUF;
	}
}
