package br.com.medic.medicsystem.main.vindi.data.subscription;

import br.com.medic.medicsystem.main.vindi.data.summary.PaymentMethod;
import br.com.medic.medicsystem.main.vindi.data.summary.Transaction;

public class BillCharge {
	private Integer id;
	private Number amount;
	private String status;
	private String due_at;
	private String paid_at;
	private Integer installments;
	private Integer attempt_count;
	private Integer next_attempt;
	private String print_url;
	private String created_at;
	private String updated_at;
	private Transaction last_transaction;
	private PaymentMethod payment_method;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDue_at() {
		return due_at;
	}
	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}
	public String getPaid_at() {
		return paid_at;
	}
	public void setPaid_at(String paid_at) {
		this.paid_at = paid_at;
	}
	public Integer getInstallments() {
		return installments;
	}
	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
	public Integer getAttempt_count() {
		return attempt_count;
	}
	public void setAttempt_count(Integer attempt_count) {
		this.attempt_count = attempt_count;
	}
	public Integer getNext_attempt() {
		return next_attempt;
	}
	public void setNext_attempt(Integer next_attempt) {
		this.next_attempt = next_attempt;
	}
	public String getPrint_url() {
		return print_url;
	}
	public void setPrint_url(String print_url) {
		this.print_url = print_url;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public Transaction getLast_transaction() {
		return last_transaction;
	}
	public void setLast_transaction(Transaction last_transaction) {
		this.last_transaction = last_transaction;
	}
	public PaymentMethod getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(PaymentMethod payment_method) {
		this.payment_method = payment_method;
	}
}
