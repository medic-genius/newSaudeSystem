package br.com.medic.medicsystem.main;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jboss.resteasy.plugins.interceptors.CorsFilter;

@ApplicationPath("/")
public class RESTActivator extends Application {

	Set<Object> singletons;
	
	@Override
	public Set<Object> getSingletons() {
		if (singletons == null) {
			CorsFilter corsFilter = new CorsFilter();
			corsFilter.getAllowedOrigins().add("*");

			singletons = new HashSet<>();
			singletons.add(corsFilter);
		}
		return singletons;
	}
}
