package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.service.RegistroPontoService;
import br.com.medic.medicsystem.persistence.dto.RegistroPontoDTO;
import br.com.medic.medicsystem.persistence.model.RegistroPonto;

@Path("/registroponto")
@RequestScoped
public class RegistroPontoController {

	@Inject
	private RegistroPontoService registroPontoService;
	
	@GET
	@Path("/unidadeperiodo")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RegistroPontoDTO> getFuncionarioPeriodo(
			@QueryParam("idUnidade") Long idUnidade,
			@QueryParam("dtInicio") Date dtInicio,
			@QueryParam("dtFim") Date dtFim,
			@QueryParam("grupoFuncionario") Integer grupoFuncionario){
		
		if(idUnidade != null && dtInicio != null && dtFim != null){
			
			return registroPontoService.getFuncionarioPeriodo(idUnidade, dtInicio, dtFim, grupoFuncionario);
		}		
		else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/funcionarioperiodo")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RegistroPontoDTO> getRegistroFuncionarioPeriodo(
			@QueryParam("idUnidade") Long idUnidade,
			@QueryParam("dtInicio") Date dtInicio,
			@QueryParam("dtFim") Date dtFim,
			@QueryParam("idFuncionario") Long idFuncionario){
		
		if(idUnidade != null && dtInicio != null && dtFim != null){
			
			return registroPontoService.getRegistroFuncionarioPeriodo(idUnidade, dtInicio, dtFim, idFuncionario);
		}		
		else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/updateregistros")
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public Response updateRegistroPonto(List<RegistroPonto> listRegistroPonto){
		
		Boolean isUpdate = null;		
				
		isUpdate = registroPontoService.updateRegistroPonto(listRegistroPonto);
		
		if (isUpdate != null) {
			return Response.status(Status.NO_CONTENT).build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}		
	}

	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	public Status saveRegistroPonto(List<String> listHorarioPonto,
			@QueryParam ("idUnidade") Long idUnidade,
			@QueryParam ("idFuncionario") Long idFuncionario,
			@QueryParam("dtRegistro") String dtRegistro) {
		
		try {
			registroPontoService.saveRegistroPonto(listHorarioPonto,idUnidade, idFuncionario, dtRegistro);
			return Response.Status.CREATED;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.Status.INTERNAL_SERVER_ERROR;
		}
		
	}
	
	@GET
	@Path("/registro")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RegistroPonto> getRegistroPonto(
			@QueryParam("idFuncionario") Long idFuncionario,
			@QueryParam("dtRegistro") String dtRegistro,
			@QueryParam("idUnidade") Long idUnidade) throws ParseException{
		
		if(idUnidade != null && dtRegistro != null && idFuncionario != null){
			
			return registroPontoService.getRegistroPonto(idFuncionario, dtRegistro, idUnidade);
		}		
		else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
}
