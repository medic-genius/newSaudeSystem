package br.com.medic.medicsystem.main.vindi.service;

import java.util.List;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.payment.PaymentProfile;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.response.ResponseParser;
import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;

public class PaymentProfileService extends BaseHttpService{
	private final String BASE_URL = "/api/v1/payment_profiles";
	private ResponseParser<PaymentProfile> parser = new ResponseParser<PaymentProfile>(PaymentProfile.class);
	
	public PaymentProfileService() {
		super();
		setUseSSL(true);
	}
	
	public List<PaymentProfile> getPaymentProfiles() {
		String url = BASE_URL;
		HttpGet get = new HttpGet();
		get.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String response = sendRequest(url, get);
			return parser.parseResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public PaymentProfile createPaymentProfile(
			br.com.medic.medicsystem.main.vindi.data.parameters.create.PaymentProfile payment) throws VindiException {
		String url = BASE_URL;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String jsonStr = (new ObjectMapper()).writeValueAsString(payment);
			post.setEntity(new StringEntity(jsonStr));
			String response = sendRequest(url, post);
			return parser.parseSingleResponse(response);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public PaymentProfile deletePaymentProfile(Integer paymentId) throws VindiException {
		String url = BASE_URL + "/" + paymentId;
		HttpDelete delete = new HttpDelete();
		delete.addHeader("Content-Type", "application/json");
		delete.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {			
			String response = sendRequest(url, delete);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
