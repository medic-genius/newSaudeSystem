package br.com.medic.medicsystem.main.jasper.dto;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import br.com.medic.medicsystem.persistence.model.Parcela;

public class RelatorioDespesaParcela {

	private String inParcela;

	private String nmFormaPagamento;

	private String vlParcela;

	private String dtVencimento;

	private String nmQuitada;

	public RelatorioDespesaParcela(Parcela parcela, Integer inParcela) {
		DecimalFormat formatador = new DecimalFormat();
		formatador.setMinimumFractionDigits(2);

		DateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy");

		this.inParcela = String.valueOf(inParcela);
		this.nmFormaPagamento = (parcela.getDespesa().getInSituacao() == 1
		        && parcela.getFormaPagamentoEfetuada() != null ? parcela
		        .getFormaPagamentoEfetuada() : parcela.getFormaPagamento());
		this.vlParcela = formatador.format(parcela.getVlParcela());
		this.dtVencimento = dataFormat.format(parcela.getDtVencimento());
		this.nmQuitada = (parcela.getDtPagamento() != null ? "Sim" : "Não");
	}

	public String getDtVencimento() {

		return dtVencimento;
	}

	public void setDtVencimento(String dtVencimento) {

		this.dtVencimento = dtVencimento;
	}

	public String getNmFormaPagamento() {

		return nmFormaPagamento;
	}

	public void setNmFormaPagamento(String nmFormaPagamento) {

		this.nmFormaPagamento = nmFormaPagamento;
	}

	public String getNmQuitada() {

		return nmQuitada;
	}

	public void setNmQuitada(String nmQuitada) {

		this.nmQuitada = nmQuitada;
	}

	public String getVlParcela() {

		return vlParcela;
	}

	public void setVlParcela(String vlParcela) {

		this.vlParcela = vlParcela;
	}

	public String getInParcela() {

		return inParcela;
	}

	public void setInParcela(String inParcela) {

		this.inParcela = inParcela;
	}

}
