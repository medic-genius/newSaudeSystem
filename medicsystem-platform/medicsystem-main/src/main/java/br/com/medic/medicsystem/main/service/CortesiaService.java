package br.com.medic.medicsystem.main.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.CortesiaDAO;
import br.com.medic.medicsystem.persistence.model.Cortesia;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.security.SecurityService;


@Stateless
public class CortesiaService {
	
	@Inject
	@Named("cortesia-dao")
	private CortesiaDAO cortesiaDAO;
	
	@Inject
	private SecurityService securityService;
	
	
	public List<Cortesia> getAllCortesia(Integer inStatusCortesia, Boolean boUtilizada, Date dtInicio, Date dtFim){
		
		return cortesiaDAO.getAllCortesia(inStatusCortesia, boUtilizada, dtInicio, dtFim);
	}
	
	public Cortesia updateLiberarCortesia(Cortesia cortesia){
		
		Funcionario funcionarioLogado = securityService.getFuncionarioLogado();
		cortesia.setFuncionarioLiberou(funcionarioLogado);
		if(cortesia.getBoUtilizada() == null || cortesia.getBoUtilizada().booleanValue() == false){
			cortesia.setBoUtilizada(new Boolean(true));
		}
		return cortesiaDAO.update(cortesia);
	}
	
	public Cortesia updateStatusCortesia(Long idCortesia, Integer inStatusCortesia){
		
			Funcionario funcionarioLogado = securityService.getFuncionarioLogado();
		
			Cortesia cortesia = cortesiaDAO.searchByKey(Cortesia.class, idCortesia);
			cortesia.setInStatusCortesia(inStatusCortesia);
			cortesia.setFuncionarioAproRepro(funcionarioLogado);
			
			return cortesiaDAO.update(cortesia);
		}
	
	
	

}
