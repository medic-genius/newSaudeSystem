package br.com.medic.dashboard.main.model;

public class GraphData {

	private String label;

	private Double value;

	public GraphData() {
		// TODO Auto-generated constructor stub
	}

	public GraphData(String label, Double value) {
		super();
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public Double getValue() {
		return value;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
