package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.main.service.LaudoAtendimentoService;
import br.com.medic.medicsystem.persistence.model.LaudoAtendimento;
import br.com.medic.medicsystem.persistence.model.views.LaudoAtendimentoView;

@Path("/laudoatendimento")
@RequestScoped
public class LaudoAtendimentoController {
	
	@Inject
	private LaudoAtendimentoService laudoAtendimentoService;
	
	@POST
	@Path("/{idAgendamento:[0-9][0-9]*}/save")
	@Produces("application/pdf")
	public Response saveLaudo(@PathParam("idAgendamento") Long idAgendamento, LaudoAtendimento laudoAtendimento){
		
		if(idAgendamento != null && laudoAtendimento != null){
			
			byte[] pdf = laudoAtendimentoService.saveLaudoAtendimento(idAgendamento, laudoAtendimento);
		
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/pdf")
	public Response updateLaudo(LaudoAtendimento laudoAtendimento){
		
		if(laudoAtendimento != null){
			
			byte[] pdf = laudoAtendimentoService.updateLaudoAtendimento(laudoAtendimento);
		
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}		
		
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public LaudoAtendimento getLaudoAtendimento(@PathParam("id") Long idLaudoAtendimento){
		
		if(idLaudoAtendimento != null){
			
			return laudoAtendimentoService.getLaudoAtendimento(idLaudoAtendimento);
		}		
		else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{idAgendamento:[0-9][0-9]*}/agendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public LaudoAtendimento getLaudoAtendimentoPorAgendamento(@PathParam("idAgendamento") Long idAgendamento){
		
		if(idAgendamento != null){
			
			return laudoAtendimentoService.getLaudoAtendimentoPorAgendamento(idAgendamento);
		}		
		else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/paciente")
	@Produces(MediaType.APPLICATION_JSON)
	public List<LaudoAtendimentoView> getLaudoAtendimentoPorCliente(
			@QueryParam("listIdPaciente") List<Long> listIdPaciente,
			@QueryParam("tipoPaciente") String tipoPaciente){
		
		if(listIdPaciente != null && listIdPaciente.size() > 0){
			
			return laudoAtendimentoService.getLaudoAtendimentoPorCliente(listIdPaciente, tipoPaciente);
		}		
		else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	

}
