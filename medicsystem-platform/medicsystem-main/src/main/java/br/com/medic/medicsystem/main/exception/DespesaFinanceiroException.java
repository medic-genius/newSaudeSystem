package br.com.medic.medicsystem.main.exception;

import javax.ejb.ApplicationException;

import br.com.medic.medicsystem.main.mapper.ErrorType;

@ApplicationException(rollback = true)
public class DespesaFinanceiroException extends GeneralException {
	
	private static final long serialVersionUID = 1L;

	public DespesaFinanceiroException(String message, ErrorType errorType) {
		super(message, errorType);
	}
	
	public DespesaFinanceiroException(String message, ErrorType errorType, Throwable e) {
		super(message, errorType, e);
	}
}
