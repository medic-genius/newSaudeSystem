package br.com.medic.medicsystem.main.service.superlogica;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.service.DespesaService;
import br.com.medic.medicsystem.main.service.SMSService;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;
import br.com.medic.medicsystem.main.util.PdfUtil;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.main.webservice.superlogica.WebServiceCobranca;
import br.com.medic.medicsystem.persistence.dao.ClienteCompartinhadoDentalDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteCompartinhadoPerformanceDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeCompartilhadoDentalDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeCompartilhadoPeformanceDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.NegociacaoDAO;
import br.com.medic.medicsystem.persistence.dao.ParcelaDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dto.BoletoDTO;
import br.com.medic.medicsystem.persistence.dto.InformativoDTO;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Negociacao;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.dental.MensalidadeDental;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.performance.MensalidadePerformance;
import br.com.medic.medicsystem.persistence.model.superlogica.Cobranca;
import br.com.medic.medicsystem.persistence.model.superlogica.ProdutoServicoGeneric;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.URLShortenerService;
import br.com.medic.medicsystem.sms.util.SMSFormatterUtils;


@Stateless
public class CobrancaServiceSP<E> extends BaseController{
	
	@Inject
	WebServiceCobranca ws;
	
	@Inject
	private BoletoServicePJB bsPJB;
	
	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
//	@Inject
//	@Named("contratodental-dao")
//	private ContratoDentalDAO contratoDentalDAO;
//	
//	@Inject
//	@Named("contratoperformance-dao")
//	private ContratoPerformanceDAO contratoPerformanceDAO;
	
	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;
	
//	@Inject
//	@Named("servicodental-dao")
//	private ServicoDentalDAO servicoDentalDAO;
//	
//	@Inject
//	@Named("servicoperformance-dao")
//	private ServicoPerformanceDAO servicoPerformanceDAO;
	
	@Inject
	@Named("despesa-dao")
	private DespesaDAO despesaDAO;
	
//	@Inject
//	@Named("despesadental-dao")
//	private DespesaDentalDAO despesaDentalDAO;
//	
//	@Inject
//	@Named("despesaperformance-dao")
//	private DespesaPerformanceDAO despesaPerformanceDAO;
	
	@Inject
	@Named("parcela-dao")
	private ParcelaDAO parcelaDAO;
	
//	@Inject
//	@Named("parceladental-dao")
//	private ParcelaDentalDAO parcelaDentalDAO;
//	
//	@Inject
//	@Named("parcelaperformance-dao")
//	private ParcelaPerformanceDAO parcelaPerformanceDAO;
	
	@Inject
	@Named("negociacao-dao")
	private NegociacaoDAO negociacaoDAO;
	
//	@Inject
//	@Named("negociacaodental-dao")
//	private NegociacaoDentalDAO negociacaoDentalDAO;
//	
//	@Inject
//	@Named("negociacaoperformance-dao")
//	private NegociacaoPerformanceDAO negociacaoPerformanceDAO;
	
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeMedicDAO;
	
	@Inject
	@Named("mensalidadecompartilhadodental-dao")
	private MensalidadeCompartilhadoDentalDAO mensalidadeCompartilhadoDentalDAO;
	
	@Inject
	@Named("mensalidadecompartilhadopeformance-dao")
	private MensalidadeCompartilhadoPeformanceDAO mensalidadeCompartilhadoPeformanceDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteMedicDAO;
	
	@Inject
	@Named("clientecompartinhadodental-dao")
	private ClienteCompartinhadoDentalDAO clienteCompartinhadoDentalDAO;
	
	@Inject
	@Named("clientecompartinhadoperformance-dao")
	private ClienteCompartinhadoPerformanceDAO clienteCompartinhadoPerformanceDAO;
		
	@Inject
	private SecurityService securityService;

	@Inject
	private DespesaService despesaService;
	
	@Inject
	private SMSService serviceSMS;
	
	@Inject
	private URLShortenerService urlShortenerService;
	
	final String MENSAGEM_BOLETO_NEGOCIADO = "%s sua NEGOCIACAO no valor R$%s vence %s. Segue o BOLETO para pagamento no link %s DUVIDAS 4003-8040";
	
		
	public List<JSONObject> cadastrarCobrancaCliente(List<Cobranca> cobrancas){
		JSONObject json = null;
		List<JSONObject> jsons = new ArrayList<JSONObject>();		
		Calendar c = Calendar.getInstance();		
		c.add(Calendar.MONTH, + 1);
		Date dtVencimento = c.getTime();
		
		for (Cobranca cobranca : cobrancas) {
			json  = ws.cadastrarCobrancaCliente(cobranca, dtVencimento);
			if(json != null && json.getInt("status") == 200){
				gerarDespesa(cobranca.getIdContrato(),json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"));
				
			}
			jsons.add(json);
		}
		return jsons;
		
	}
	
	private <E> E searchByKey (EmpresaGrupoCompartilhado empresa, Class<E> clazz, Long id){
		Integer baseDadosSistema = empresa.getInSistemaExterno();
		
		switch ( baseDadosSistema ) {
		case 0:
			return clienteMedicDAO.searchByKey(clazz, id);
		case 1:
			return clienteCompartinhadoPerformanceDAO.searchByKey(clazz, id);
		case 2:
			return clienteCompartinhadoDentalDAO.searchByKey(clazz, id);
		default:
			return null;
		}
	}
	

	@SuppressWarnings("unchecked")
	private Collection<E> getMensalidadesByCliente (EmpresaGrupoCompartilhado empresa, Long idCliente, String dtInicio, String dtFim){
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();			
		Integer dias = 0;
		
		try {
			
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		Date dataInicio = DateUtil.parseDate(dtInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		Date dataFim = DateUtil.parseDate(dtFim, DateUtil.DatePattern.DDMMAA.getPattern());
	
			dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		switch ( baseDadosSistema ) {
		case 0:
			return (Collection<E>) mensalidadeMedicDAO.getMensalidadeByCliente(idCliente, dias);
		case 1:
			return (Collection<E>)mensalidadeCompartilhadoPeformanceDAO.getMensalidadeByCliente(idCliente, dias);
		case 2:
			return (Collection<E>)mensalidadeCompartilhadoDentalDAO.getMensalidadeByCliente(idCliente, dias);
		default:
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<E> getMensalidadesByClientePorPeriodo (EmpresaGrupoCompartilhado empresa, Long idCliente, String dtInicio, String dtFim){
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();		
						
		switch ( baseDadosSistema ) {
		case 0:
			return (Collection<E>) mensalidadeMedicDAO.getMensalidadeByClientePorPeriodo(idCliente, dtInicio, dtFim);
		default:
			return null;
		}
	}
	
	/*
	private void updateMensalidades (EmpresaGrupoCompartilhado empresa, List<?> mensalidades){
		Integer baseDadosSistema = empresa.getInSistemaExterno();
		for (Object mensalidadeObject : mensalidades) {
			
			switch ( baseDadosSistema ) {
			case 0:
				Mensalidade mensalidadeMedic = (Mensalidade) mensalidadeObject;
				 mensalidadeMedicDAO.updateMensalidade(mensalidadeMedic);
				 break;
			case 1:
				 MensalidadePerformance mensalidadePerformance = (MensalidadePerformance) mensalidadeObject;
				 mensalidadeCompartilhadoPeformanceDAO.updateMensalidade(mensalidadePerformance);
				 break;
			case 2:
				MensalidadeDental mensalidadeDental = (MensalidadeDental) mensalidadeObject;
				 mensalidadeCompartilhadoDentalDAO.updateMensalidade(mensalidadeDental);
				 break;
			default:
				 
			}			
		}	
	}	
	*/
	
	private Cobranca formatCobranca(EmpresaGrupoCompartilhado empresa, Long idCliente, Collection<?> mensalidades, Double vltotalBoleto, Integer tipoValor){
		
		//tipoValor = 1 -> Valor Fixo
		//tipoValor = 2 -> Valor em % de desconto
		
		Cobranca cobranca = new Cobranca();
		ProdutoServicoGeneric psg = new ProdutoServicoGeneric();
		psg.setId_produto_prd(new Integer(4));
		psg.setNm_quantidade_comp(new Integer(1));
		psg.setVl_unitario_prd(0D);
		SimpleDateFormat sdf= new SimpleDateFormat("MM/yyyy");
		String st_observacaoexterna_contrato = "";
		String st_observacaoexterna_recb = "";
		
		if(tipoValor != null){			
			if(tipoValor.equals(1)){ //Valor Fixo
				if(vltotalBoleto != null && !vltotalBoleto.equals(0))
					psg.setVl_unitario_prd(vltotalBoleto);
			}		
			else if(tipoValor.equals(2)){ //Valor em % de desconto
				if(vltotalBoleto != null && !vltotalBoleto.equals(0)){
					
					for (Object mensalidadeObject : mensalidades) {
						if(empresa.getInSistemaExterno() == 0){
							Mensalidade mensalidade = (Mensalidade) mensalidadeObject;
							psg.setVl_unitario_prd(psg.getVl_unitario_prd() + ((vltotalBoleto * mensalidade.getValorMensalidade()) / 100));
							st_observacaoexterna_contrato = "Cobrança referente a " + mensalidade.getContrato().getNrContrato() + " - ";				
							st_observacaoexterna_recb = st_observacaoexterna_recb + " " + sdf.format(mensalidade.getDataVencimento());
						}else if(empresa.getInSistemaExterno() == 1){
							MensalidadePerformance mensalidade = (MensalidadePerformance) mensalidadeObject;
							psg.setVl_unitario_prd(psg.getVl_unitario_prd() + ((vltotalBoleto * mensalidade.getValorMensalidade()) / 100));
							st_observacaoexterna_recb = st_observacaoexterna_recb +
									sdf.format(mensalidade.getDataVencimento())+" - ";
						}else if(empresa.getInSistemaExterno() == 2){
							MensalidadeDental mensalidade = (MensalidadeDental) mensalidadeObject;
							psg.setVl_unitario_prd(psg.getVl_unitario_prd() + ((vltotalBoleto * mensalidade.getValorMensalidade()) / 100));
							st_observacaoexterna_recb = st_observacaoexterna_recb +
									sdf.format(mensalidade.getDataVencimento())+" - ";
							
						}					
					}
				}
			}else if(tipoValor.equals(3)){ //Valor com juros
									
				for (Object mensalidadeObject : mensalidades) {
					if(empresa.getInSistemaExterno() == 0){
						Mensalidade mensalidade = (Mensalidade) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() +  mensalidade.getValorMensalidade());
						st_observacaoexterna_contrato = "Cobrança referente a " + mensalidade.getContrato().getNrContrato() + " - ";				
						st_observacaoexterna_recb = st_observacaoexterna_recb + " " + sdf.format(mensalidade.getDataVencimento());
					}else if(empresa.getInSistemaExterno() == 1){
						MensalidadePerformance mensalidade = (MensalidadePerformance) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() + mensalidade.getValorMensalidade());
						st_observacaoexterna_recb = st_observacaoexterna_recb +
								sdf.format(mensalidade.getDataVencimento())+" - ";
					}else if(empresa.getInSistemaExterno() == 2){
						MensalidadeDental mensalidade = (MensalidadeDental) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() + mensalidade.getValorMensalidade());
						st_observacaoexterna_recb = st_observacaoexterna_recb +
								sdf.format(mensalidade.getDataVencimento())+" - ";
						
					}					
				}				
			}
			
		}else{
			
			if(vltotalBoleto != null && !vltotalBoleto.equals(0))
				psg.setVl_unitario_prd(vltotalBoleto);
			else{
				for (Object mensalidadeObject : mensalidades) {
					if(empresa.getInSistemaExterno() == 0){
						Mensalidade mensalidade = (Mensalidade) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() + mensalidade.getValorMensalidade() );
						st_observacaoexterna_contrato = "Cobrança referente a " + mensalidade.getContrato().getNrContrato() + " - ";				
						st_observacaoexterna_recb = st_observacaoexterna_recb + " " + sdf.format(mensalidade.getDataVencimento());
					}else if(empresa.getInSistemaExterno() == 1){
						MensalidadePerformance mensalidade = (MensalidadePerformance) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() + mensalidade.getValorMensalidade());
						st_observacaoexterna_recb = st_observacaoexterna_recb +
								sdf.format(mensalidade.getDataVencimento())+" - ";
					}else if(empresa.getInSistemaExterno() == 2){
						MensalidadeDental mensalidade = (MensalidadeDental) mensalidadeObject;
						psg.setVl_unitario_prd(psg.getVl_unitario_prd() + mensalidade.getValorMensalidade());
						st_observacaoexterna_recb = st_observacaoexterna_recb +
								sdf.format(mensalidade.getDataVencimento())+" - ";
						
					}					
				}
			}
			
		}
		
		st_observacaoexterna_recb = st_observacaoexterna_contrato + st_observacaoexterna_recb; 
		st_observacaoexterna_recb = st_observacaoexterna_recb.substring(0, st_observacaoexterna_recb.length() > 200 ? 200 : st_observacaoexterna_recb.length());
		Cliente cliente = searchByKey(empresa , Cliente.class, idCliente);
		cobranca.setProdutoServicoGeneric(psg);
		cobranca.setId_sacado_sac(Integer.parseInt(cliente.getIdClienteBoleto_sl().toString()));
		cobranca.setSt_observacaoexterna_recb(st_observacaoexterna_recb);
		cobranca.setSt_NomeCliente(cliente.getNmCliente());
		cobranca.setEndereco(cliente.getNmLogradouro()+", "+cliente.getNrNumero() +" - "+cliente.getBairro().getNmBairro());
		cobranca.setEnderecoComplemento(cliente.getNrCEP() + " - "+cliente.getCidade().getNmCidade()+"-"+cliente.getCidade().getNmUF());
		
		return cobranca;
	}
	
		
	@SuppressWarnings("unchecked")
	public List<JSONObject> cadastrarCobrancaClienteAvulsa(List<Long> idClientes, EmpresaGrupoCompartilhado empresa, String dtInicio, String dtFim, Double vltotalBoleto, Integer tipoValor){
		
		Properties props = PropertiesLoader.getInstance().load("message.properties");	
		
		JSONObject json = null;
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		Calendar c = Calendar.getInstance();		
		c.add(Calendar.DATE, + 14);
		Date dtVencimento = c.getTime();
		
		for (Long idCliente : idClientes) {
		  if(searchByKey(empresa , Cliente.class, idCliente).getIdClienteBoleto_sl() != null){
			  
				if (empresa.getInSistemaExterno() == 0) {
					
					Collection<Mensalidade> mensalidades = (Collection<Mensalidade>) getMensalidadesByCliente(empresa, idCliente, dtInicio, dtFim);					
					
					if(mensalidades != null && mensalidades.size() > 0){
						List<Mensalidade> updateMensalidades = new ArrayList<Mensalidade>();
						Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, null, null);
						json = ws.cadastrarCobrancaCliente(cobranca, dtVencimento);
						if (json != null && json.getInt("status") == 200) {
							for (Mensalidade mensalidade : mensalidades) {
//								mensalidade.setLinkCobrancaBoleto_sl(json.getJSONObject("data").getString("link_2via"));
//								mensalidade.setIdCobrancaBoleto_sl(json.getJSONObject("data").getLong("id_recebimento_recb"));
								updateMensalidades.add(mensalidade);
							}
							gerarDespesa(updateMensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, null);
		
							//updateMensalidades(empresa, updateMensalidades);
						}
						
					}

					jsons.add(json);
				} 
				
				else if (empresa.getInSistemaExterno() == 1) {
					Collection<MensalidadePerformance> mensalidades = (Collection<MensalidadePerformance>) getMensalidadesByCliente(empresa, idCliente, dtInicio, dtFim);
					if(mensalidades != null && mensalidades.size() > 0){
						List<MensalidadePerformance> updateMensalidades = new ArrayList<MensalidadePerformance>();
						Cobranca cobranca = formatCobranca(empresa, idCliente,mensalidades, null, null);
						json = ws.cadastrarCobrancaCliente(cobranca, dtVencimento);
						if (json != null && json.getInt("status") == 200) {
							for (MensalidadePerformance mensalidade : mensalidades) {
//								mensalidade.setLinkCobrancaBoleto_sl(json.getJSONObject("data").getString("link_2via"));
//								mensalidade.setIdCobrancaBoleto_sl(json.getJSONObject("data").getLong("id_recebimento_recb"));
								updateMensalidades.add(mensalidade);
							}
							gerarDespesa(updateMensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, null);

							//updateMensalidades(empresa, updateMensalidades);
						}
					}
					
					jsons.add(json);
				} 
				
				else if (empresa.getInSistemaExterno() == 2) {
					Collection<MensalidadeDental> mensalidades = (Collection<MensalidadeDental>) getMensalidadesByCliente(empresa, idCliente, dtInicio, dtFim);
					if(mensalidades != null && mensalidades.size() > 0){
					List<MensalidadeDental> updateMensalidades = new ArrayList<MensalidadeDental>();
					Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, null, null);
					json = ws.cadastrarCobrancaCliente(cobranca, dtVencimento);
					if (json != null && json.getInt("status") == 200) {
						for (MensalidadeDental mensalidade : mensalidades) {
//							mensalidade.setLinkCobrancaBoleto_sl(json.getJSONObject("data").getString("link_2via"));
//							mensalidade.setIdCobrancaBoleto_sl(json.getJSONObject("data").getLong("id_recebimento_recb"));
							updateMensalidades.add(mensalidade);
						}
						gerarDespesa(updateMensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, null);

						//updateMensalidades(empresa, updateMensalidades);
					}
				}
					jsons.add(json);
				}
			
			
			}else{
				throw new AgendamentoException(props.getProperty("clientesp.naocadastro"), ErrorType.DANGER);
			}
		}
		return jsons;	
		
	}
	
	@SuppressWarnings("unchecked")
	public List<JSONObject> cadastrarCobrancaClienteAvulsaPeriodo(List<Long> idClientes, EmpresaGrupoCompartilhado empresa, String dtInicio, String dtFim, Integer tipoValor, Double vlCobranca){
		
		Properties props = PropertiesLoader.getInstance().load("message.properties");	
		
		JSONObject json = null;
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		Calendar c = Calendar.getInstance();		
		c.add(Calendar.DATE, + 14);
		Date dtVencimento = c.getTime();
		
		for (Long idCliente : idClientes) {
		  if(searchByKey(empresa , Cliente.class, idCliente).getIdClienteBoleto_sl() != null){
			  
				if (empresa.getInSistemaExterno() == 0) {
										
					Collection<Mensalidade> mensalidades = (Collection<Mensalidade>) getMensalidadesByClientePorPeriodo(empresa, idCliente, dtInicio, dtFim);
					
					if(mensalidades != null && mensalidades.size() > 0){
						List<Mensalidade> updateMensalidades = new ArrayList<Mensalidade>();
						Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, vlCobranca, tipoValor);
						json = ws.cadastrarCobrancaCliente(cobranca, dtVencimento);
						if (json != null && json.getInt("status") == 200) {
							for (Mensalidade mensalidade : mensalidades) {
//								mensalidade.setLinkCobrancaBoleto_sl(json.getJSONObject("data").getString("link_2via"));
//								mensalidade.setIdCobrancaBoleto_sl(json.getJSONObject("data").getLong("id_recebimento_recb"));
								updateMensalidades.add(mensalidade);
							}
							gerarDespesa(updateMensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, null);
		
							//updateMensalidades(empresa, updateMensalidades);
						}
						
					}

					jsons.add(json);
				}		
			
			}else{
				throw new AgendamentoException(props.getProperty("clientesp.naocadastro"), ErrorType.DANGER);
			}
		}
		return jsons;	
		
	}
	
		
	/**
	 * 
	 * @param idCliente ID do cliente para qual a cobrança será vinculada
	 * @param idMensalidades ID das mensalidades que serão incluídas na negociação
	 * @param dtVencimento data de vencimento do boleto de negociação
	 * @param vlBoleto valor do boleto de negociação a ser pago pelo cliente
	 * @param empresa empresa da qual a negociação é oriunda
	 * @param nrCelular número do celular para o qual será enviado o boleto
	 * @param updateCel flag informando se o número indicado por nrCeluar deve ser salvo como número do cliente
	 * @return
	 */
	public List<JSONObject> createCobrancaAvulsaCliente(Long idCliente,
			List<Long> idMensalidades, String dtVencimento, Double vlBoleto,
			EmpresaGrupoCompartilhado empresa, String nrCelular,
			Boolean updateCel) {
		// deixa somente numeros do telefone informado
		nrCelular = SMSFormatterUtils.getNumbersOnly(nrCelular);

		JSONObject json = null;
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		Date dtVencBoleto = DateUtil.parseDate(dtVencimento,
				DateUtil.DatePattern.DDMMAA.getPattern());
		
		Cliente cliente = searchByKey(empresa , Cliente.class, idCliente);
		  
		if (empresa.getInSistemaExterno() == 0) { //saudesystem
			
			List<Mensalidade> mensalidades = new ArrayList<Mensalidade>();
			
			for(Long idmensalidade: idMensalidades){
				Mensalidade mensalidade = mensalidadeMedicDAO.searchByKey(Mensalidade.class, idmensalidade);
				mensalidades.add(mensalidade);
			}				
			
			if(mensalidades != null && mensalidades.size() > 0) {
									
				try {
					Parcela p = gerarDespesa(mensalidades, vlBoleto, null, null, empresa, dtVencBoleto);
					BoletoBancario bb = bsPJB.registrarBoleto(p, cliente);
					json = new JSONObject();
					json.put("link", bb.getLinkBoleto());
					
					if(nrCelular != null && updateCel) {
						cliente.setNrCelular(nrCelular);
						clienteMedicDAO.update(cliente);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//					Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, vlBoleto, null);
//					json = ws.cadastrarCobrancaCliente(cobranca, dtVencBoleto);
//					if (json != null && json.getInt("status") == 200) {						
//						gerarDespesa(mensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, dtVencBoleto);
//					}					
			}

//			jsons.add(json);
		} else if (empresa.getInSistemaExterno() == 1) { //elevesystem
			
			List<MensalidadePerformance> mensalidades = new ArrayList<MensalidadePerformance>();
			
			for(Long idmensalidade: idMensalidades){
				MensalidadePerformance mensalidade = mensalidadeCompartilhadoPeformanceDAO.searchByKey(MensalidadePerformance.class, idmensalidade);
				mensalidades.add(mensalidade);
			}
			
			if(mensalidades != null && mensalidades.size() > 0){					
				Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, vlBoleto, null);
				json = ws.cadastrarCobrancaCliente(cobranca, dtVencBoleto);
				if (json != null && json.getInt("status") == 200) {						
					gerarDespesa(mensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, dtVencBoleto);
				}
				
				if(nrCelular != null && updateCel) {
					cliente.setNrCelular(nrCelular);
					clienteCompartinhadoPerformanceDAO.update(cliente);
				}
			}
			
//			jsons.add(json);
		} else if (empresa.getInSistemaExterno() == 2) { //dentalsystem
			
			List<MensalidadeDental> mensalidades = new ArrayList<MensalidadeDental>();
			
			for(Long idmensalidade: idMensalidades){
				MensalidadeDental mensalidade = mensalidadeCompartilhadoDentalDAO.searchByKey(MensalidadeDental.class, idmensalidade);
				mensalidades.add(mensalidade);
			}
			
			if(mensalidades != null && mensalidades.size() > 0) {				
				Cobranca cobranca = formatCobranca(empresa, idCliente, mensalidades, vlBoleto, null);
				json = ws.cadastrarCobrancaCliente(cobranca, dtVencBoleto);
				if (json != null && json.getInt("status") == 200) {					
					gerarDespesa(mensalidades, json.getJSONObject("data").getDouble("vl_total_recb"), json.getJSONObject("data").getString("link_2via"), json.getJSONObject("data").getLong("id_recebimento_recb"), empresa, dtVencBoleto);
				}
				
				if(nrCelular != null && updateCel) {
					cliente.setNrCelular(nrCelular);
					clienteCompartinhadoDentalDAO.update(cliente);
				}
			}
//			jsons.add(json);
		}
		
		
//		}else{
//			throw new AgendamentoException(props.getProperty("clientesp.naocadastro"), ErrorType.DANGER);
//		}
			
		if(cliente != null && json != null && json.has("link") && !json.isNull("link")) {
			String shortLink = urlShortenerService.shortenURL(json.getString("link"));
			if (shortLink != null) {
				InformativoDTO sms = new InformativoDTO();
				List<InformativoDTO> result = new ArrayList<InformativoDTO>();

				String infMessage = SMSFormatterUtils.getFormattedMessage(
						MENSAGEM_BOLETO_NEGOCIADO,
						SMSFormatterUtils.getFirstName(cliente.getNmCliente()),
						vlBoleto, dtVencimento, shortLink.replace("https://", ""));
				
				sms.setIdCliente(cliente.getId());
				sms.setNmInformativo(infMessage);
				sms.setNrCelular(nrCelular);
//				sms.setNrTelefone(cliente.getNrTelefone());
				result.add(sms);
				String nmEmpresa = "DR. CARD";
				try {
					serviceSMS.enviarInformativoSMS(result, 1, nmEmpresa);
				} catch (ClientHumanException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jsons.add(json);
			}
		}
		
		return jsons;	
	}
	
	
	public byte[] getBoletosPDF(List<BoletoDTO> links ){
		
//		final String PDF_FOLDER_PATH = "C:\\Users\\medicsaude_dev\\Documents\\testexls\\";
		//final String PDF_FOLDER_PATH = "C:\\Users\\MARCOS\\Documents\\boletos\\";
		final String PDF_FOLDER_PATH = "/opt/sistema/arquivos/boletos/";
		
		final String BASE_PDF_NAME = "ListaPDFs.pdf";
		
		return PdfUtil.getPdfMergedArrayFromLinkList(links, PDF_FOLDER_PATH+BASE_PDF_NAME, securityService.getFuncionarioLogado().getId());
		
	}
	
	private void gerarDespesa(Long idContrato, Double valorBoleto, String linkBoleto, Long idCobrancaSuperLogica){
	
	try {
		
	
		Calendar c = Calendar.getInstance();		
		c.add(Calendar.MONTH, + 1);
		Date dtVencimento = c.getTime();
		
		Contrato contrato = contratoDAO.getContrato(idContrato);
		contrato.setBoNegociadoAuto(new Boolean(true));
		contrato.setDtNegociadoAuto(new Date());
		contratoDAO.update(contrato);
		
		Despesa despesa = new Despesa();
		despesa.setBoExcluida(false);
		despesa.setAgendamento( null );
		despesa.setVlDespesaAberto( new Double( 0 ) );
		despesa.setVlDespesaCoberta(new Double( 0 ) );
		despesa.setVlDespesaTotal( new Double( 0 ) );
		despesa.setDtDespesa( new Date());
		despesa.setInSituacao( 0 );// em aberto
		despesa.setIdOperadorCadastro( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getId() : 0L);		
		despesa.setNmObservacao( "Mensalidades em aberto negociadas " );			
		despesa.setUnidade( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getUnidade() : null );
		despesa.setNrDespesa( despesaService.gerarCodigoDespesa() );
		
		ContratoCliente contratoCliente = contratoDAO.getContratoCliente(idContrato);
		despesa.setContratoCliente( contratoCliente );			
		
		despesa.setVlDespesaAberto( new Double (valorBoleto));
		despesa.setVlDespesaTotal( new Double (valorBoleto));
		despesa.setBoParcelaDa( false );
		despesa.setInFormaPagamento( 5 ); //Boleto
		
		DespesaServico despesaServico =  new DespesaServico();
		Servico servico = servicoDAO.searchByKey( Servico.class, 23356L );
		despesaServico.setServico(servico);
		despesaServico.setDespesa( despesa );
		despesaServico.setQtServico( 1 );
		despesaServico.setVlServico( new Double(valorBoleto));
		despesaServico.setInCoberto( false );
		despesa.getDespesaServicos().add( despesaServico );
		despesaDAO.persist(despesa);
		
		Parcela parcelaDespesa = new Parcela();
		parcelaDespesa.setDespesa( despesa );
		parcelaDespesa.setDtInclusao( new Date() );
		parcelaDespesa.setDtVencimento( dtVencimento ); //data atual + 1 mês
		parcelaDespesa.setInFormaPagamento( 5 ); // Boleto
		parcelaDespesa.setNrParcela( despesa.getNrDespesa().concat( "/01" ) );
		parcelaDespesa.setIdCobrancaBoleto_sl(idCobrancaSuperLogica);
		parcelaDespesa.setLinkCobrancaBoleto_sl(linkBoleto);
		
		parcelaDespesa.setVlParcela( new Double (valorBoleto) ); // A formatacao sera feita no preenchimento do boleto bancario
		parcelaDAO.persist(parcelaDespesa);
		
		Negociacao negociacao = new Negociacao();
		negociacao.setDespesa( despesa );
		negociacao.setParcela( parcelaDespesa );
		negociacao.setBoNegociadaAuto(true);
		
		negociacaoDAO.persist(negociacao);
		
		} catch (Exception e) {
		// TODO: handle exception
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private Parcela gerarDespesa(List<?> mensalidadesInadimplentes, Double valorBoleto, String linkBoleto, Long idCobrancaSuperLogica, EmpresaGrupoCompartilhado empresa, Date dtVencBoleto){
		
		try {
			Date dtVencimento;
			
			if(dtVencBoleto == null){
				Calendar c = Calendar.getInstance();		
				c.add(Calendar.DATE, + 14);
				dtVencimento = c.getTime();
			}
			else
				dtVencimento = dtVencBoleto;
			
			
			Parcela parcelaDespesa = new Parcela();
			
			DespesaServico despesaServico =  new DespesaServico();
			despesaServico.setQtServico( 1 );
			despesaServico.setVlServico( new Double(valorBoleto));
			despesaServico.setInCoberto( false );
			
			parcelaDespesa.setDtInclusao( new Date() );
			parcelaDespesa.setDtVencimento( dtVencimento ); //data atual + 14 dias
			parcelaDespesa.setInFormaPagamento( 5 ); // Boleto
			parcelaDespesa.setIdCobrancaBoleto_sl(idCobrancaSuperLogica);
			parcelaDespesa.setLinkCobrancaBoleto_sl(linkBoleto);
			
			parcelaDespesa.setVlParcela( new Double (valorBoleto) ); // A formatacao sera feita no preenchimento do boleto bancario
			
			if(empresa.getInSistemaExterno() == 0){
				
				Despesa despesa = new Despesa();
				despesa.setAgendamento( null );
				despesa.setVlDespesaAberto( new Double( 0 ) );
				despesa.setVlDespesaCoberta(new Double( 0 ) );
				despesa.setVlDespesaTotal( new Double( 0 ) );
				despesa.setDtDespesa( new Date());
				despesa.setInSituacao( 0 );// em aberto
				despesa.setIdOperadorCadastro( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getId() : 0L);		
				despesa.setNmObservacao( "Despesa de mensalidades negociadas ");			
				despesa.setUnidade( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getUnidade() : null );
				despesa.setVlDespesaAberto( new Double (valorBoleto));
				despesa.setVlDespesaTotal( new Double (valorBoleto));
				despesa.setBoParcelaDa( false );
				despesa.setInFormaPagamento( 5 ); //Boleto
				despesa.setBoExcluida(new Boolean(false));
				
				
				List<Mensalidade> mensalidades = (List<Mensalidade>) mensalidadesInadimplentes;
				ContratoCliente contratoCliente = contratoDAO.getContratoCliente(mensalidades.get(0).getContrato().getId());
				despesa.setContratoCliente( contratoCliente );
				despesa.setNrDespesa( despesaService.gerarCodigoDespesa() );
				Servico servico = servicoDAO.searchByKey( Servico.class, 23356L );
				despesaServico.setServico(servico);
				despesaServico.setDespesa( despesa );
				despesa.getDespesaServicos().add( despesaServico );
				despesaDAO.persist(despesa);
				parcelaDespesa.setDespesa( despesa );
				parcelaDespesa.setNrParcela( despesa.getNrDespesa().concat( "/01" ) );
				parcelaDAO.persist(parcelaDespesa);
				
				for (Mensalidade mensalidade : mensalidades) {
					Negociacao negociacao = new Negociacao();
					negociacao.setDespesa( despesa );
					negociacao.setMensalidade(mensalidade);
					negociacaoDAO.persist(negociacao);
				}
				
			}
			
			return parcelaDespesa;
			
//			else if(empresa.getInSistemaExterno() == 1){
//				
//				
//				DespesaPerformance despesa = new DespesaPerformance();
//				despesa.setAgendamento( null );
//				despesa.setVlDespesaAberto( new Double( 0 ) );
//				despesa.setVlDespesaCoberta(new Double( 0 ) );
//				despesa.setVlDespesaTotal( new Double( 0 ) );
//				despesa.setDtDespesa( new Date());
//				despesa.setInSituacao( 0 );// em aberto
//				despesa.setIdOperadorCadastro( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getId() : 0L);		
//				despesa.setNmObservacao( "Despesa de mensalidades negociadas ");			
//				despesa.setUnidade( securityService.getFuncionarioLogado() != null ? securityService.getFuncionarioLogado().getUnidade() : null );
//				despesa.setVlDespesaAberto( new Double (valorBoleto));
//				despesa.setVlDespesaTotal( new Double (valorBoleto));
//				despesa.setBoParcelaDa( false );
//				despesa.setInFormaPagamento( 5 ); //Boleto
//				
//				List<MensalidadePerformance> mensalidades = (List<MensalidadePerformance>) mensalidadesInadimplentes;
//				ContratoClientePerformance contratoCliente = new ContratoClientePerformance();
//				contratoCliente = contratoPerformanceDAO.getContratoCliente(mensalidades.get(0).getIdContrato());
//				despesa.setIdContratoCliente(contratoCliente.getId());
//				despesa.setNrDespesa( despesaService.gerarCodigoDespesa(empresa) );
//				Servico servico = servicoPerformanceDAO.searchByKey( Servico.class, 48L );
//				despesaServico.setServico(servico);
//				despesaServico.setDespesa( despesa );
//				despesa.getDespesaServicos().add( despesaServico );
//				despesaPerformanceDAO.persist(despesa);
//				parcelaDespesa.setDespesa( despesa );
//				parcelaDespesa.setNrParcela( despesa.getNrDespesa().concat( "/01" ) );
//				parcelaDAO.persist(parcelaDespesa);
//				
//				for (MensalidadePerformance mensalidade : mensalidades) {
//					NegociacaoPerformance negociacao = new NegociacaoPerformance();
//					negociacao.setDespesa( despesa );
//					negociacao.setMensalidade(mensalidade);
//					negociacaoPerformanceDAO.persist(negociacao);
//				}
//				
//			}else if(empresa.getInSistemaExterno() == 2){
//				List<MensalidadeDental> mensalidades = (List<MensalidadeDental>) mensalidadesInadimplentes;
//				contratoCliente = contratoDentalDAO.getContratoCliente(mensalidades.get(0).getIdContrato());
//				despesa.setContratoCliente( contratoCliente );
//				despesa.setNrDespesa( despesaService.gerarCodigoDespesa(empresa) );
//				Servico servico = servicoDentalDAO.searchByKey( Servico.class, 23356L );
//				despesaServico.setServico(servico);
//				despesaServico.setDespesa( despesa );
//				despesa.getDespesaServicos().add( despesaServico );
//				despesaDentalDAO.persist(despesa);
//				parcelaDespesa.setDespesa( despesa );
//				parcelaDespesa.setNrParcela( despesa.getNrDespesa().concat( "/01" ) );
//				parcelaDAO.persist(parcelaDespesa);
//				
//				for (MensalidadeDental mensalidade : mensalidades) {
//					NegociacaoDental negociacao = new NegociacaoDental();
//					negociacao.setDespesa( despesa );
//					negociacao.setMensalidade(mensalidade);
//					negociacaoDentalDAO.persist(negociacao);
//				}
//			}
			
			} catch (Exception e) {
			// TODO: handle exception
				return null;
			}
			
		}
	
	public Boolean liquidarCobrancaSl( JSONObject json){
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade;
		Parcela parcela;
		Date dtPagamento;
		Boolean despesaPaga = true;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;
						
		try {
					
			mensalidade = mensalidadeMedicDAO.getMensalidadeByIdCobrancaSL( json.getLong("data[id_recebimento_recb]") );
			parcela = parcelaDAO.getParcelaByIdCobrancaSL(json.getLong("data[id_recebimento_recb]"));
			
			dtPagamento = sdf.parse(json.getString("data[dt_liquidacao_recb]"));
			vlJuros = json.has("data[compo_recebimento][1][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][1][st_valor_comp]") : 0.0;
			vlMulta = json.has("data[compo_recebimento][2][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][2][st_valor_comp]") : 0.0;
			
			
			if(parcela != null && parcela.getDtPagamento() == null){
				
				List<Negociacao> listnegociacoes = new ArrayList<Negociacao>();
				
				parcela.setDtPagamento(dtPagamento);
				parcela.setVlPago(json.getDouble("data[vl_total_recb]"));
				parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
				parcela.setInFormaPagamento(5);
				parcela.setVlJuros( vlJuros );
				parcela.setVlMulta( vlMulta );				
								
				for(Parcela pc: parcela.getDespesa().getParcelas())					
					if(pc.getDtPagamento() == null && (pc.getBoExcluida() == null || pc.getBoExcluida() == false))
						despesaPaga = false;			
								
				if(despesaPaga)
					parcela.getDespesa().setInSituacao(1);
				
				listnegociacoes = negociacaoDAO.getNegociacaoByDespesa(parcela.getDespesa().getId());
				if(listnegociacoes != null && listnegociacoes.size() > 0)
					for(Negociacao neg : listnegociacoes){
						neg.getMensalidade().setDataPagamento(dtPagamento);
						neg.getMensalidade().setVlPago(json.getDouble("data[vl_total_recb]"));
						neg.getMensalidade().setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
						neg.getMensalidade().setInStatus(1);
						neg.getMensalidade().setVlJuros(vlJuros);
						neg.getMensalidade().setVlMulta(vlMulta);
						neg.getMensalidade().setBoletoNegociada(true);
						neg.getMensalidade().setDtNegociacao(parcela.getDespesa().getDtDespesa());
						
						mensalidadeMedicDAO.update(neg.getMensalidade());
					}				
				
				
				parcelaDAO.update(parcela);
				
				return true;
				
			}else if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
								
				mensalidade.setDataPagamento(dtPagamento);
				mensalidade.setVlPago(json.getDouble("data[vl_total_recb]"));
				mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
				mensalidade.setInStatus(1);
				mensalidade.setVlJuros(vlJuros);
				mensalidade.setVlMulta(vlMulta);
				
				mensalidadeMedicDAO.update(mensalidade);
				
				return true;
			}			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return false;
	}
	
	private Cobranca formatCobranca(Cliente cliente, Mensalidade mensalidade){
		
		Cobranca cobranca = new Cobranca();
		ProdutoServicoGeneric psg = new ProdutoServicoGeneric();
		
		psg.setId_produto_prd(new Integer(999999982));
		psg.setNm_quantidade_comp(new Integer(1));
		psg.setVl_unitario_prd(new Double(mensalidade.getValorMensalidade()));		
				
		cobranca.setProdutoServicoGeneric(psg);
		cobranca.setSt_observacaoexterna_recb("Contrato: " + mensalidade.getContrato().getNrContrato());
		
		cobranca.setId_sacado_sac(Integer.parseInt(cliente.getIdClienteBoleto_sl().toString()));		
		cobranca.setSt_NomeCliente(cliente.getNmCliente());
		cobranca.setEndereco(cliente.getNmLogradouro()+", "+cliente.getNrNumero() +" - "+cliente.getBairro().getNmBairro());
		cobranca.setEnderecoComplemento(cliente.getNrCEP() + " - "+ cliente.getCidade().getNmCidade() + "-" + cliente.getCidade().getNmUF());
				
		return cobranca;
	}
	
	public Mensalidade cadastrarCobrancaAvulsaCliente(Mensalidade mensalidade, Cliente cliente){
		
		JSONObject json = null;
				
		Cobranca cobranca = formatCobranca(cliente, mensalidade);
		json = ws.cadastrarCobrancaCliente(cobranca, mensalidade.getDataVencimento());
		
		if (json != null && json.getInt("status") == 200) {
			mensalidade.setIdCobrancaBoleto_sl( json.getJSONObject("data").getLong("id_recebimento_recb") );
			mensalidade.setLinkCobrancaBoleto_sl( json.getJSONObject("data").getString("link_2via") );
		}
						
		return mensalidade;
	}
	
	
	public byte[] imprimirCarne(Cliente cliente, Contrato contrato) throws JSONException{
		
		
		JSONObject json = null;
		Long idImpressao;
		
		BigInteger idimpressaoBoletoSl = clienteMedicDAO.getCodigoImpressaoBoletoSL();
		
		json = ws.imprimirCarne(cliente.getIdClienteBoleto_sl(), contrato, idimpressaoBoletoSl);		
		
		if(json != null && json.getInt("status") == 200){
			idImpressao = new JSONObject(json.getJSONArray("data").get(0).toString()).getLong("id_impressao");
			byte[] bytes = ws.downloadImpressao(idImpressao, null);				
		
			return bytes;
		}else{
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //"dormir" 1000 milisegundos = 1 segundo  
		  
			byte[] bytes = ws.downloadImpressao(null, idimpressaoBoletoSl);
			
			return bytes;
		}
	}	


}
