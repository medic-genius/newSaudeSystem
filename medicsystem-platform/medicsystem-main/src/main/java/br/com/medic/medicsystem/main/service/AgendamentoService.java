package br.com.medic.medicsystem.main.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.ErrorResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.appmobile.response.SuccessResponse;
import br.com.medic.medicsystem.main.appmobile.service.PushNotificationService;
import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;
import br.com.medic.medicsystem.main.util.EmailUtil;
import br.com.medic.medicsystem.main.util.FormatUtil;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.appmobile.dao.UsuarioLoginDAO;
import br.com.medic.medicsystem.persistence.appmobile.dto.AgendamentoLembreteDTO;
import br.com.medic.medicsystem.persistence.appmobile.dto.PagedListDTO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.appmobile.views.HistoricoAgendamentosCliente;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoProfissionalDAO;
import br.com.medic.medicsystem.persistence.dao.AvaliacaoAgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.CarenciaDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDependenteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaGuiaServicoDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaServicoDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.dao.EncaminhamentoDAO;
import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.LogGerenteDAO;
import br.com.medic.medicsystem.persistence.dao.OrdemAgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ParcelaDAO;
import br.com.medic.medicsystem.persistence.dao.PlanoDAO;
import br.com.medic.medicsystem.persistence.dao.RegistroAgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoEspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoProfissionalDAO;
import br.com.medic.medicsystem.persistence.dao.TituloDaDAO;
import br.com.medic.medicsystem.persistence.dao.UnidadeDAO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoViewDTO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoWebDTO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.ContrachequeInfoDTO;
//import br.com.medic.medicsystem.persistence.dto.AgendamentoViewDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaSiteDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteCadastroDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteDTO;
import br.com.medic.medicsystem.persistence.dto.PagamentoAgendamentoDTO;
import br.com.medic.medicsystem.persistence.dto.horarioAtendimentoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.AvaliacaoAgendamento;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Carencia;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaGuiaServico;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.EmailMessage;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.Encaminhamento;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.LogGerente;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.OrdemAgendamento;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.RegistroAgendamento;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.ServicoEspecialidade;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;
import br.com.medic.medicsystem.persistence.model.ServicoSubEspecialidade;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusAgendamento;
import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusPagamentoDespesa;
import br.com.medic.medicsystem.persistence.model.enums.TipoAtendimento;
import br.com.medic.medicsystem.persistence.model.enums.TipoPessoa;
import br.com.medic.medicsystem.persistence.model.enums.TipoPrioridade;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoView;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.security.keycloak.KeycloakUtil;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.Utils;

@Stateless
public class AgendamentoService {

	@PersistenceContext(unitName = "saudesystem")
	protected EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Inject
	private Logger logger;

	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;

	@Inject
	@Named("ordemagendamento-dao")
	private OrdemAgendamentoDAO ordemAgendamentoDAO;

	@Inject
	@Named("despesa-dao")
	private DespesaDAO despesaDAO;

	@Inject
	@Named("especialidade-dao")
	private EspecialidadeDAO especialidadeDAO;

	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;

	@Inject
	@Named("despesaservico-dao")
	private DespesaServicoDAO despesaServicoDAO;

	@Inject
	@Named("encaminhamento-dao")
	private EncaminhamentoDAO encaminhamentoDAO;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;

	@Inject
	@Named("carencia-dao")
	private CarenciaDAO carenciaDAO;

	@Inject
	@Named("parcela-dao")
	private ParcelaDAO parcelaDAO;

	@Inject
	@Named("tituloda-dao")
	private TituloDaDAO tituloDaDAO;

	@Inject
	@Named("plano-dao")
	private PlanoDAO planoDAO;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;

	@Inject
	@Named("empresagrupo-dao")
	private EmpresaGrupoDAO empresaGrupoDAO;

	@Inject
	@Named("contratodependente-dao")
	private ContratoDependenteDAO contratoDependenteDAO;

	@Inject
	@Named("atendimentoprofissional-dao")
	private AtendimentoProfissionalDAO atendimentoProfissionalDAO;

	@Inject
	@Named("registroagendamento-dao")
	private RegistroAgendamentoDAO registroAgendamentoDAO;

	@Inject
	@Named("servicoprofissional-dao")
	private ServicoProfissionalDAO servicoProfissionalDAO;

	@Inject
	@Named("servicoespecialidade-dao")
	private ServicoEspecialidadeDAO servicoEspecialidadeDAO;

	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;

	@Inject
	@Named("avaliacao-agendamento-dao")
	private AvaliacaoAgendamentoDAO avaliacaoAgendamentoDAO;

	@Inject
	@Named("log-gerente-dao")
	private LogGerenteDAO logGerenteDAO;

	@Inject
	@Named("despesaguiaservico-dao")
	private DespesaGuiaServicoDAO despesaGuiaServicoDAO;

	@Inject
	private ClienteService clienteServico;

	@Inject
	private DespesaService despesaService;

	@Inject
	private SecurityService securityService;

	@Inject
	private KeycloakUtil keycloakUtil;

	@Inject
	private VendasService vendasService;

	@Inject
	private BoletoServicePJB boletoServicePJB;

	@Inject
	private GuiaService guiaService;

	// para aplicativo
	@Inject
	@Named("usuario-login-dao")
	private UsuarioLoginDAO usuarioLoginDAO;
	
	private final String EMPRESA_CONSUTLA = "MAIS CONSULTA";

	public Agendamento getAgendamento(Long id) {
		logger.debug("Obtendo um agendamento...");

		Agendamento ag = agendamentoDAO.searchByKey(Agendamento.class, id);
		if (ag != null) {
			PacienteDTO<?> paciente = null;

			if (ag.getDependente() != null) {
				paciente = new PacienteDTO<Dependente>(ag.getDependente());
				paciente.setDependente(ag.getDependente());
			} else {
				paciente = new PacienteDTO<Cliente>(ag.getCliente());
			}
			paciente.setCliente(ag.getCliente());
			ag.setPaciente(paciente);
		}

		if (ag.getIdOperadorCadastro() != null) {
			Funcionario funcionario = null;
			try {
				funcionario = funcionarioDAO.searchByKey(Funcionario.class, ag.getIdOperadorCadastro());
			} catch (Exception e) {

			}

			if (funcionario != null)
				ag.setNmOperadorCadastro(funcionario.getNmFuncionario());

			funcionario = null;
			if (ag.getIdOperadorAlteracao() != null) {
				funcionario = funcionarioDAO.searchByKey(Funcionario.class, ag.getIdOperadorAlteracao());
				ag.setNmOperadorAlteracao(funcionario.getNmFuncionario());
			}

		}

		return ag;
	}

	private Calendar truncarCalendar(Calendar calendar) {

		// calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar;

	}

	private long mapDatas(Date valor, Date intervaloMinimo, Date intervaloMaximo, Date saidaMinima, Date saidaMaxima) {

		return (((valor.getTime() - intervaloMinimo.getTime()) * (saidaMaxima.getTime() - saidaMinima.getTime()))
				/ (intervaloMaximo.getTime() - intervaloMinimo.getTime())) + saidaMinima.getTime();

	}

	private Date configDate(Long time) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		calendar.set(Calendar.DAY_OF_MONTH, 23);
		calendar.set(Calendar.MONTH, 1);
		calendar.set(Calendar.YEAR, 2017);

		return calendar.getTime();
	}

	public Agendamento setHorarioPrevisto(Agendamento agendamento) {

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");

		calendar.setTime(agendamento.getDtAgendamento());
		AtendimentoProfissional atendProfissional = new AtendimentoProfissional();

		atendProfissional = getAtendimentoProfissionalByAgendamento(agendamento);

		if (atendProfissional != null) {
			if (atendProfissional.getHrOrdemInicio() != null && atendProfissional.getHrOrdemFim() != null) {

				agendamento.setHrInicio(sdf.format(atendProfissional.getHrOrdemInicio()));
				agendamento.setHrFim(sdf.format(atendProfissional.getHrOrdemFim()));

				Date dataAgendamento = configDate(agendamento.getHrAgendamento().getTime());
				Date dataInitProf = configDate(atendProfissional.getHrInicio().getTime());
				Date dataFimProf = configDate(atendProfissional.getHrFim().getTime());
				Date dataInitProfMin = configDate(atendProfissional.getHrOrdemInicio().getTime());
				Date dataFimProfMax = configDate(atendProfissional.getHrOrdemFim().getTime());

				calendar.setTimeInMillis(
						mapDatas(dataAgendamento, dataInitProf, dataFimProf, dataInitProfMin, dataFimProfMax));

				calendar = truncarCalendar(calendar);
				agendamento.setHrInicio(sdf.format(calendar.getTime()));
				calendar.add(Calendar.HOUR_OF_DAY, 1);

				if (calendar.getTime().after(dataFimProfMax)) {
					agendamento.setHrFim(sdf.format(dataFimProfMax.getTime()));
				} else {
					agendamento.setHrFim(sdf.format(calendar.getTime()));
				}

			} else if (atendProfissional.getBoOrdemProducao() != null && atendProfissional.getBoOrdemProducao()) {
				agendamento.setHrInicio(atendProfissional.getHrInicio().toString());
				calendar.setTimeInMillis(atendProfissional.getHrInicio().getTime());
				calendar.add(Calendar.HOUR_OF_DAY, 1);
				calendar.set(Calendar.SECOND, 0);
				agendamento.setHrFim(sdf.format(calendar.getTime()));

			} else {

				calendar.setTime(agendamento.getHrAgendamento());
				calendar = truncarCalendar(calendar);
				calendar.add(Calendar.HOUR_OF_DAY, -1);
				calendar.set(Calendar.SECOND, 0);

				if (calendar.get(Calendar.HOUR_OF_DAY) >= 7) {
					agendamento.setHrInicio(sdf.format(calendar.getTime()));
				} else {
					calendar.set(Calendar.HOUR_OF_DAY, 7);
					calendar.set(Calendar.SECOND, 0);
					agendamento.setHrInicio(sdf.format(calendar.getTime()));
				}
				calendar.setTime(agendamento.getHrAgendamento());
				calendar = truncarCalendar(calendar);
				agendamento.setHrFim(sdf.format(calendar.getTime()));
			}

		} else {
			agendamento.setHrInicio("00:00");
			agendamento.setHrFim("00:00");
		}

		return agendamento;

	}

	public Agendamento setConsultorioAgendamento(Agendamento ag) {

		Agendamento agendamento = new Agendamento();
		agendamento = agendamentoDAO.getAgendamento(ag.getId());

		setHorarioPrevisto(agendamento);
		AtendimentoProfissional atendimentoProfissional = getAtendimentoProfissionalByAgendamento(agendamento);
		if (atendimentoProfissional != null && atendimentoProfissional.getConsultorio() != null
				&& atendimentoProfissional.getConsultorio().getId() != null) {
			agendamento.setConsultorio(atendimentoProfissional.getConsultorio());
		}

		return agendamento;
	}

	public Agendamento saveAgendamento(Agendamento agendamento) {
		// checando limite diario de agendamento
		Contrato tmpContrato = contratoDAO.getContratoPorCliente(agendamento.getCliente().getId());
		if (agendamento.getInTipo().equals(0) && agendamento.getLoginGerenteAutorizacao() == null && tmpContrato != null
				&& tmpContrato.getPlano() != null) {
			Integer quantidade = null;
			if (agendamento.getDependente() != null) {
				quantidade = this.countPacienteAgendamentosConsulta(agendamento.getDependente().getId(),
						TipoPessoa.DEPENDENTE.getId(), agendamento.getDtAgendamento(),
						agendamento.getEspecialidade().getId(), agendamento.getServico().getId());
			} else {
				quantidade = this.countPacienteAgendamentosConsulta(agendamento.getCliente().getId(),
						TipoPessoa.CLIENTE.getId(), agendamento.getDtAgendamento(),
						agendamento.getEspecialidade().getId(), agendamento.getServico().getId());
			}
			if (quantidade != null && quantidade >= 2) {
				throw new WebApplicationException("Paciente selecionado atingiu o limite de agendamentos diários", 500);
			}
		}

		Properties props = PropertiesLoader.getInstance().load("message.properties");
		logger.info("Salvando Agendamento...");

		if (!agendamento.getInTipo().equals(1) && agendamentoDAO.getHorariosConsultasMarcadasNoDia(agendamento)) {
			throw new AgendamentoException(props.getProperty("agendamento.duplicado"), ErrorType.DANGER);
		}
		if (agendamento.getServico() != null) {
			agendamento.setServico(servicoDAO.getServico(agendamento.getServico().getId()));
		}

		agendamento.setCliente(clienteServico.getCliente(agendamento.getCliente().getId()));

		// setting this weird DTO
		PacienteDTO<?> paciente = null;

		if (agendamento.getDependente() != null) {
			agendamento.setDependente(clienteServico.getDependente(agendamento.getDependente().getId()));
			paciente = new PacienteDTO<Dependente>(agendamento.getDependente());

			if (agendamento.getContrato() == null || agendamento.getContrato().getId() == null) {
				// Adicionar dependente no contrato do cliente nao fazendo uso
				Contrato contrato = contratoDAO.getContratoPorCliente(agendamento.getCliente().getId());
				if (contrato != null) {
					ContratoDependente condep = new ContratoDependente();
					condep.setCreditoConveniada(0);
					condep.setBlAutorizaDebAut(false);
					condep.setContrato(contrato);
					condep.setInSituacao(0);
					condep.setDependente(agendamento.getDependente());
					condep.setBoFazUsoPlano(false);
					contrato.addContratoDependente(condep);
					contratoDAO.persist(contrato);
				}
				ContratoDependente contratoDependente = contratoDAO.getContratoDependente(contrato.getId(),
						agendamento.getDependente().getId());
				paciente.setContratoDependente(contratoDependente);
			} else {
				ContratoDependente contratoDependente = contratoDAO
						.getContratoDependente(agendamento.getContrato().getId(), agendamento.getDependente().getId());
				paciente.setContratoDependente(contratoDependente);
			}
		} else
			paciente = new PacienteDTO<Cliente>(agendamento.getCliente());

		paciente.setCliente(agendamento.getCliente());
		agendamento.setPaciente(paciente);
		agendamento.setContrato(
				agendamento.getContrato().getId() != null ? contratoDAO.getContrato(agendamento.getContrato().getId())
						: paciente.getContratoDependente().getContrato());
		agendamento.setNrAgendamento(geraCodigoAgendamento(agendamento));

		String motivoEncaixe = agendamento.getMotivoEncaixe();

		if (agendamento.getIdAgendamentoAux() != null && agendamento.getBoRetorno()) {
			Agendamento agPai = agendamentoDAO.searchByKey(Agendamento.class, agendamento.getIdAgendamentoAux());
			agPai.setBoRetornoGerado(true);
			agendamentoDAO.update(agPai);
			salvarAgendamentoRetorno(agendamento);
		} else {

			Despesa despesaCoberta = null;
			Despesa despesaConveniada = null;

			if (agendamento.getBoParticular() != null && !agendamento.getBoParticular().booleanValue()) {

				if (agendamento.getContrato().getEmpresaCliente() != null) {// Conveniada?
					despesaConveniada = gerarCoberturaConvenio(agendamento);
				} else {
					despesaCoberta = gerarCoberturaPlano(agendamento);
				}
			} else {

				paciente = carregarPaciente(agendamento);
				agendamento.setContrato(paciente.getContratoCliente().getContrato());
			}

			if (despesaCoberta == null && despesaConveniada == null) { // Nenhuma despesa coberta ou convenio gerada
				agendamento = agendamentoDAO.persist(agendamento);
			} else {
				Despesa despesa;
				if (despesaCoberta != null) {
					despesa = despesaCoberta;
				} else {
					despesa = despesaConveniada;
				}
				agendamento = saveAgendamentoCobertoeConvenio(agendamento, despesa);
			}
		}

		if (agendamento.getInTipo() == 1) { // Encaixe
			LogGerente log = new LogGerente();
			log.setAcao(1);
			Funcionario func = securityService.getFuncionarioLogado();
			if (func != null) {
				log.setIdFuncionario(func.getId());
			}
			log.setIdCliente(paciente.getCliente().getId());
			log.setIdContrato(agendamento.getContrato().getId());
			log.setIdDocumento(agendamento.getId());
			log.setIdPlano(
					agendamento.getContrato().getPlano() != null ? agendamento.getContrato().getPlano().getId() : null);
			log.setDtAcao(new Date());
			log.setObservacao(motivoEncaixe);
			logGerenteDAO.saveLogGerente(log);
		}

		if (agendamento.getLoginGerenteAutorizacao() != null && agendamento.getId() != null) {
			LogGerente log = new LogGerente();
			log.setAcao(8);
			Funcionario func = funcionarioDAO.getFuncionarioPorLogin(agendamento.getLoginGerenteAutorizacao());
			if (func != null) {
				log.setIdFuncionario(func.getId());
			}
			log.setIdCliente(agendamento.getCliente().getId());
			log.setIdContrato(agendamento.getContrato().getId());
			log.setIdDocumento(agendamento.getId());
			log.setIdPlano(
					agendamento.getContrato().getPlano() != null ? agendamento.getContrato().getPlano().getId() : null);
			log.setDtAcao(new Date());
			log.setObservacao(agendamento.getJustificativaLoginGerente());
			logGerenteDAO.saveLogGerente(log);
		}
		return agendamento;
	}

	private Despesa gerarCoberturaConvenio(Agendamento agendamento) {
		Map<String, Boolean> boos = verificarCobertura(agendamento.getServico(), agendamento);

		if (!boos.get("isGuia")) {
			return null;
		}

		DespesaDTO despesaDTO = validaGerarDespesa(agendamento);

		return despesaDTO.getDespesa();
	}

	private Despesa inicializarDespesaConvenio(Despesa despesa) {
		// É conveniada ou empresacliente?
		// Possui guia?
		// Serviço equivalente a guia?
		// GuiaServico guiaServicoValid = null;

		if (despesa.getId() != null) {
			despesa.setBoExcluida(false);
			despesa.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
			despesa.setInSituacao(StatusPagamentoDespesa.PAGO.getId());

			List<Parcela> listParcela = agendamentoDAO.getParcelasDespesa(despesa.getId());

			if (listParcela != null && listParcela.size() > 0) {
				for (Parcela p : listParcela) {
					p.setBoNegociada(false);
					p.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
					p.setFormaPagamentoEfetuada(FormaPagamentoEnum.CONVENIO.getDescription());
					p.setDtPagamento(new Date());
					p.setVlPago(despesa.getVlDespesaTotal());
					despesa.addParcela(p);
				}
			}

			// guiaServicoValid =
			// despesaGuiaServicoDAO.getGuiaServicobyDespesa(despesa.getId());
			//
			// if(guiaServicoValid != null)
			// return null;

			return despesa;

		} else {
			despesa.setBoExcluida(false);
			despesa.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
			despesa.setInSituacao(1);
			despesa.setNrDespesa(despesaService.gerarCodigoDespesa());

			Parcela p = new Parcela();
			p.setBoNegociada(false);
			p.setDespesa(despesa);
			p.setDtInclusao(new Date());
			p.setDtPagamento(new Date());
			p.setDtVencimento(new Date());
			p.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
			p.setFormaPagamentoEfetuada(FormaPagamentoEnum.CONVENIO.getDescription());
			p.setNrParcela(despesa.getNrDespesa() + "/01");
			p.setVlPago(despesa.getVlDespesaTotal());
			p.setVlParcela(despesa.getVlDespesaTotal());
			despesa.addParcela(p);

			return despesa;

		}

	}

	private String getNumberOnly(String var) {
		if (var != null) {
			var = var.replaceAll("\\s", "");
			var = var.trim();
			var = var.replaceAll("[^0-9]", "");
			return var;
		}
		return null;
	}

	/**
	 * Cria um agendamento a partir da web (site ou aplicativo)
	 * 
	 * @param agendamentoWeb
	 *            Dados básicos para criação de um agendamento
	 * @param accessedFrom
	 *            Plataforma de acesso, que podem ser: "mobile_app" ou "web"
	 * @return Objeto agendamento já persistido.
	 * 
	 * @throws AgendamentoException
	 * 
	 * @author Patrick Lima em 2017-10-03
	 */
	public Despesa createAgendamento(AgendamentoWebDTO agendamentoWeb, String accessedFrom) {
		// caso cliente não exista, cria um cliente
		if (agendamentoWeb.getCliente() != null) {
			Cliente cli = agendamentoWeb.getCliente();
			cli.setNmCliente(cli.getNmCliente().toUpperCase());
			if (cli.getNrCPF() != null) {
				cli.setNrCPF(getNumberOnly(cli.getNrCPF()));
			}
			if (cli.getNrCelular() != null) {
				cli.setNrCelular(this.getNumberOnly(cli.getNrCelular()));
			}
			if (cli.getNmCliente() == null || cli.getNrCPF() == null || cli.getNrCelular() == null
					|| cli.getNmEmail() == null) {
				throw new AgendamentoException("Preencha todos os dados do cliente", ErrorType.ERROR);
			}

			EmpresaGrupo defaultEmpresa = empresaGrupoDAO.searchByKey(EmpresaGrupo.class, 9L);
			Cliente savedCliente = clienteServico.saveCliente(cli, 0, defaultEmpresa);
			PacienteCadastroDTO cadastro = new PacienteCadastroDTO();
			cadastro.setIdPaciente(savedCliente.getId());
			cadastro.setTipo(1);

			ArrayList<PacienteCadastroDTO> list = new ArrayList<PacienteCadastroDTO>();
			list.add(cadastro);
			agendamentoWeb.setCadastrosPaciente(list);
		}
		logger.info("Salvando Agendamento...");
		Agendamento agendamentoObj = new Agendamento();
		agendamentoObj.setInTipo(0);
		agendamentoObj.setInTipoConsulta(0);
		agendamentoObj.setInTipoAgendamento(0);
		agendamentoObj.setInStatus(0);
		agendamentoObj.setInTipoAtendimento(1);
		agendamentoObj.setBoRetorno(false);
		agendamentoObj.setInTurno(0);
		// from front-end
		agendamentoObj.setDtAgendamento(DateUtil.parseDate(agendamentoWeb.getDtAgendamento(), "dd/MM/yyyy"));

		if (accessedFrom != null && accessedFrom.equals("mobile_app")) {
			// set id operador cadastro aplicativo
			agendamentoObj.setIdOperadorCadastro(4733L);
		} else {
			// set id operador cadastro site
			agendamentoObj.setIdOperadorCadastro(4734L);
		}

		Servico servico = servicoDAO.getServico(agendamentoWeb.getIdServico());
		Profissional profissional = funcionarioDAO.getProfissionalAgendamento(agendamentoWeb.getIdProfissional());
		Unidade unidade = unidadeDAO.getUnidadeById(agendamentoWeb.getIdUnidade());
		Especialidade especialidade = especialidadeDAO.getEspecialidadeById(agendamentoWeb.getIdEspecialidade());
		
		agendamentoObj.setServico(servico);
		agendamentoObj.setProfissional(profissional);
		agendamentoObj.setUnidade(unidade);
		agendamentoObj.setEspecialidade(especialidade);
		agendamentoObj.setNrAgendamento(geraCodigoAgendamento(agendamentoObj));

		// pegar hora automaticamente (e turno também)
		Calendar c = Calendar.getInstance();
		c.setTime(agendamentoObj.getDtAgendamento());
		for (horarioAtendimentoDTO hr : agendamentoWeb.getHorarios()) {
			String[] hrArr = hr.getData().split(":");
			c.set(Calendar.HOUR, Integer.parseInt(hrArr[0]));
			c.set(Calendar.MINUTE, Integer.parseInt(hrArr[1]));
			agendamentoObj.setHrAgendamento(c.getTime());
			agendamentoObj.setBoParticular(hr.getBoParticular());
			if (agendamentoDAO.getHorariosConsultasMarcadasNoDia(agendamentoObj)) {
				agendamentoObj.setHrAgendamento(null);
				agendamentoObj.setBoParticular(null);
			} else {
				break;
			}
		}
		if (agendamentoObj.getHrAgendamento() == null) {
			throw new AgendamentoException("Horário escolhido indisponível", ErrorType.ERROR);
		}
		// ----------------------------------------------------------------

		// armazena dados do cadastro ideal (associado, ou cliente) a ser utilizado caso
		// não haja cobertura para o agendamento
		Cliente defaultCliente = null;
		Dependente defaultDependente = null;
		ContratoCliente defaultContratoCliente = null;
		ContratoDependente defaultContratoDependente = null;
		Integer defaultTipoPaciente = null;
		Boolean boPacienteBloqueado = null;

		Iterator<PacienteCadastroDTO> it = agendamentoWeb.getCadastrosPaciente().iterator();
		while (it.hasNext()) {
			PacienteCadastroDTO pc = it.next();
			// setting this weird DTO
			PacienteDTO<?> paciente = null;

			if (pc.getTipo() == 2) { // dependente
				Dependente dependente = clienteServico.getDependente(pc.getIdPaciente());

				if (pc.getNrCelular() != null) {
					dependente.setNrCelular(pc.getNrCelular());
				}

				if (pc.getNmEmail() != null) {
					dependente.setNmEmail(pc.getNmEmail());
				}

				dependenteDAO.update(dependente);

				ClienteView cView = clienteServico.clienteViewById(dependente.getCliente().getId());
				if (cView.getStatusGeral() != null && (cView.getStatusGeral() == 3 || cView.getStatusGeral() == 2)) { // cliente
																														// bloqueado
					boPacienteBloqueado = new Boolean(true);
					// retornar erro
					continue;
				}

				boPacienteBloqueado = new Boolean(false);

				agendamentoObj.setCliente(dependente.getCliente());
				agendamentoObj.setDependente(dependente);

				paciente = new PacienteDTO<Dependente>(agendamentoObj.getDependente());

				Collection<ContratoDependente> contratosDependente = null;
				try {
					contratosDependente = clienteServico.getDependentesContratos(agendamentoObj.getCliente().getId(),
							agendamentoObj.getDependente().getId());
				} catch (Exception e) {
				}

				// dependente nao tem contrato
				if (contratosDependente == null || contratosDependente.isEmpty()) {
					Contrato contrato = contratoDAO.getContratoPorCliente(agendamentoObj.getCliente().getId());
					if (contrato == null) { // cliente nao tem contrato ativo
						ContratoCliente tmpCc = clienteServico.createContratoForCliente(agendamentoObj.getCliente(),
								null);
						contrato = tmpCc.getContrato();
					}

					// cria contrato para dependente
					ContratoDependente condep = new ContratoDependente();
					condep.setCreditoConveniada(0);
					condep.setBlAutorizaDebAut(false);
					condep.setContrato(contrato);
					condep.setInSituacao(0);
					condep.setDependente(agendamentoObj.getDependente());
					condep.setBoFazUsoPlano(false);
					contrato.addContratoDependente(condep);
					contratoDAO.persist(contrato);

					ContratoDependente contratoDependente = contratoDAO.getContratoDependente(contrato.getId(),
							agendamentoObj.getDependente().getId());
					contratosDependente = new LinkedList<ContratoDependente>();
					contratosDependente.add(contratoDependente);
				}

				if (defaultTipoPaciente == null) {
					defaultTipoPaciente = 2;
					defaultDependente = dependente;
					defaultCliente = dependente.getCliente();
					// pega primeiro contrato
					defaultContratoDependente = contratosDependente.iterator().next();
				} else {
					ContratoDependente tmpCd = contratosDependente.iterator().next();
					if (tmpCd.getContrato().getPlano() != null && ((defaultTipoPaciente.equals(1)
							&& defaultContratoCliente.getContrato().getPlano() == null)
							|| (defaultTipoPaciente.equals(2)
									&& defaultContratoDependente.getContrato().getPlano() == null))) {
						defaultTipoPaciente = 2;
						defaultDependente = dependente;
						defaultCliente = dependente.getCliente();
						// pega primeiro contrato
						defaultContratoDependente = tmpCd;
					}
				}

				for (ContratoDependente cd : contratosDependente) {
					paciente.setContratoDependente(cd);
					paciente.setCliente(agendamentoObj.getCliente());
					agendamentoObj.setPaciente(paciente);

					agendamentoObj.setContrato(cd.getContrato());

					if (agendamentoObj.getBoParticular() != null && agendamentoObj.getBoParticular().booleanValue()) { // particular
						agendamentoObj = agendamentoDAO.persist(agendamentoObj);
						entityManager.flush();
						DespesaDTO despesaDto = validaGerarDespesa(agendamentoObj, true);
						Despesa despesa = registrarDespesa(agendamentoObj.getId(), despesaDto);
						if (despesa == null) {
							throw new AgendamentoException("Nao foi possivel criar agendamento", ErrorType.ERROR);
						}
						return despesa;
					} else {
						Despesa despesaCoberta = gerarCoberturaPlano(agendamentoObj);
						if (despesaCoberta != null) {
							if (agendamentoObj.getContrato().getPlano() != null) {
								Integer qtd = this.countPacienteAgendamentosConsulta(dependente.getId(),
										TipoPessoa.DEPENDENTE.getId(), agendamentoObj.getDtAgendamento(),
										agendamentoObj.getEspecialidade().getId(), agendamentoObj.getServico().getId());
								if (qtd != null && qtd >= 2) {
									throw new AgendamentoException(
											"Paciente atingiu o limite de agendamentos para esse dia", ErrorType.ERROR);
								}
							}

							agendamentoObj = saveAgendamentoCobertoeConvenio(agendamentoObj, despesaCoberta);

							return despesaCoberta;
						}
					}
				}
			} else if (pc.getTipo() == 1) { // cliente
				Cliente cliente = clienteServico.getCliente(pc.getIdPaciente());

				if (pc.getNrCelular() != null) {
					cliente.setNrCelular(pc.getNrCelular());
				}

				if (pc.getNmEmail() != null) {
					cliente.setNmEmail(pc.getNmEmail());
				}

				clienteServico.updateCliente(cliente);

				ClienteView cView = clienteServico.clienteViewById(cliente.getId());
				if (cView.getStatusGeral() != null && (cView.getStatusGeral() == 3 || cView.getStatusGeral() == 2)) { // cliente
																														// bloqueado

					boPacienteBloqueado = new Boolean(true);
					// retornar erro
					continue;
				}

				boPacienteBloqueado = new Boolean(false);

				agendamentoObj.setCliente(cliente);

				paciente = new PacienteDTO<Cliente>(agendamentoObj.getCliente());

				List<ContratoCliente> contratosCliente = null;
				try {
					contratosCliente = contratoDAO.getContratosClienteByIdCliente(agendamentoObj.getCliente().getId());
				} catch (Exception e) {
				}

				if (contratosCliente == null || contratosCliente.isEmpty()) {
					ContratoCliente contr = clienteServico.createContratoForCliente(cliente, null);
					contratosCliente = new LinkedList<ContratoCliente>();
					contratosCliente.add(contr);
				}

				if (defaultTipoPaciente == null) {
					defaultTipoPaciente = 1;
					defaultCliente = cliente;
					defaultContratoCliente = contratosCliente.iterator().next();
					defaultDependente = null;
					defaultContratoDependente = null;
				} else {
					ContratoCliente tmpCc = contratosCliente.iterator().next();
					if ((tmpCc.getContrato().getPlano() != null && ((defaultTipoPaciente.equals(1)
							&& defaultContratoCliente.getContrato().getPlano() == null)
							|| defaultTipoPaciente.equals(2)))
							|| (tmpCc.getContrato().getPlano() == null && defaultTipoPaciente.equals(2)
									&& defaultContratoDependente.getContrato().getPlano() == null)) {
						defaultTipoPaciente = 1;
						defaultCliente = cliente;
						defaultContratoCliente = contratosCliente.iterator().next();
						defaultDependente = null;
						defaultContratoDependente = null;
					}
				}

				for (ContratoCliente cc : contratosCliente) {
					paciente.setContratoCliente(cc);
					paciente.setCliente(agendamentoObj.getCliente());
					agendamentoObj.setPaciente(paciente);

					agendamentoObj.setContrato(cc.getContrato());

					if (agendamentoObj.getBoParticular() != null && agendamentoObj.getBoParticular().booleanValue()) { // particular
						agendamentoObj = agendamentoDAO.persist(agendamentoObj);
						entityManager.flush();
						DespesaDTO despesaDto = validaGerarDespesa(agendamentoObj, true);
						Despesa despesa = registrarDespesa(agendamentoObj.getId(), despesaDto);
						if (despesa == null) {
							throw new AgendamentoException("Nao foi possivel criar agendamento", ErrorType.ERROR);
						}
						return despesa;
					} else {
						Despesa despesaCoberta = gerarCoberturaPlano(agendamentoObj);
						if (despesaCoberta != null) {
							if (agendamentoObj.getContrato().getPlano() != null) {
								Integer qtd = this.countPacienteAgendamentosConsulta(cliente.getId(),
										TipoPessoa.CLIENTE.getId(), agendamentoObj.getDtAgendamento(),
										agendamentoObj.getEspecialidade().getId(), agendamentoObj.getServico().getId());
								if (qtd != null && qtd >= 2) {
									throw new AgendamentoException(
											"Paciente atingiu o limite de agendamentos para esse dia", ErrorType.ERROR);
								}
							}

							agendamentoObj = saveAgendamentoCobertoeConvenio(agendamentoObj, despesaCoberta);

							return despesaCoberta;
						}
					}
				}
			}
		}

		if (agendamentoObj.getId() == null && defaultTipoPaciente != null) {
			PacienteDTO<?> paciente;

			Integer qtd = null;
			if (defaultTipoPaciente.equals(2)) { // tipo dependente
				agendamentoObj.setCliente(defaultDependente.getCliente());
				agendamentoObj.setDependente(defaultDependente);
				agendamentoObj.setContrato(defaultContratoDependente.getContrato());

				paciente = new PacienteDTO<Dependente>(agendamentoObj.getDependente());
				paciente.setContratoDependente(defaultContratoDependente);
				paciente.setCliente(agendamentoObj.getCliente());
				agendamentoObj.setPaciente(paciente);

				if (agendamentoObj.getContrato().getPlano() != null) {
					qtd = this.countPacienteAgendamentosConsulta(defaultDependente.getId(),
							TipoPessoa.DEPENDENTE.getId(), agendamentoObj.getDtAgendamento(),
							agendamentoObj.getEspecialidade().getId(), agendamentoObj.getServico().getId());
				}

			} else if (defaultTipoPaciente.equals(1)) { // tipo cliente
				agendamentoObj.setCliente(defaultCliente);
				agendamentoObj.setDependente(null);
				agendamentoObj.setContrato(defaultContratoCliente.getContrato());

				paciente = new PacienteDTO<Cliente>(defaultCliente);
				paciente.setContratoCliente(defaultContratoCliente);
				paciente.setCliente(defaultCliente);
				agendamentoObj.setPaciente(paciente);

				if (agendamentoObj.getContrato().getPlano() != null) {
					qtd = this.countPacienteAgendamentosConsulta(defaultCliente.getId(), TipoPessoa.CLIENTE.getId(),
							agendamentoObj.getDtAgendamento(), agendamentoObj.getEspecialidade().getId(),
							agendamentoObj.getServico().getId());
				}
			}
			if (qtd != null && qtd >= 2) {
				throw new AgendamentoException("Paciente atingiu o limite de agendamentos para esse dia",
						ErrorType.ERROR);
			}

			// verifica se é convênio;
			Map<String, Boolean> agProps = checkPropriedadesAgendamento(agendamentoObj);
			agendamentoObj.setIsConvenio(agProps.get("isConvenio"));

			paciente = carregarPaciente(agendamentoObj);
			agendamentoObj.setContrato(paciente.getContratoCliente().getContrato());
			agendamentoObj = agendamentoDAO.persist(agendamentoObj);
			entityManager.flush();
			DespesaDTO despesaDto = validaGerarDespesa(agendamentoObj, true);
			Despesa despesa = registrarDespesa(agendamentoObj.getId(), despesaDto);
			if (despesa == null) {
				throw new AgendamentoException("Nao foi possivel criar agendamento", ErrorType.ERROR);
			}
			return despesa;
		} else if (defaultTipoPaciente != null) {
			throw new AgendamentoException("Cliente possui pendências", ErrorType.DANGER);
		} else if (boPacienteBloqueado != null && boPacienteBloqueado.booleanValue()) {
			throw new AgendamentoException("Paciente bloqueado", ErrorType.DANGER);
		} else {
			throw new AgendamentoException("Erro ao criar agendamento", ErrorType.DANGER);
		}
	}

	public void sendEmailConsultaMarcada(Despesa despesa, String horaPrevista) {
		Agendamento agendamento = despesa.getAgendamento();
		String email = "";
		String assunto = "Confirmação de agendamento";

		if (agendamento.getDependente() != null) {
			if (agendamento.getDependente().getNmEmail() != null) {
				email = agendamento.getDependente().getNmEmail();
				if (!validar(email)) {
					email = agendamento.getCliente().getNmEmail();
				}
			} else {
				email = agendamento.getCliente().getNmEmail();
			}
		} else {
			email = agendamento.getCliente().getNmEmail();
		}

		String corpoEmail = getMensagemEmailCliente(despesa, horaPrevista);
		EmailUtil.sendEmail(email, agendamento.getPaciente().getNmPaciente(), assunto, corpoEmail);
	}

	public static boolean validar(String email) {
		boolean isEmailIdValid = false;
		if (email != null && email.length() > 0) {
			String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
			Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(email);
			if (matcher.matches()) {
				isEmailIdValid = true;
			}
		}
		return isEmailIdValid;
	}

	private String getMensagemEmailCliente(Despesa despesa, String horaPrevista) {
		Agendamento agendamento = despesa.getAgendamento();
		InputStream inputStream = getClass().getResourceAsStream("/html/drmaisconsulta.tex");
		String bodyMessage = "";
		try {
			bodyMessage = IOUtils.toString(inputStream, "utf-8");
			bodyMessage = bodyMessage.replaceAll("nomePaciente",
					despesa.getAgendamento().getPaciente().getNmPaciente());
			bodyMessage = bodyMessage.replaceAll("nomeServico", agendamento.getServico().getNmServico());
			bodyMessage = bodyMessage.replaceAll("dataAgendamento", agendamento.getDtAgendamentoFormatado());
			bodyMessage = bodyMessage.replaceAll("nomeProfissional", agendamento.getProfissional().getNmFuncionario());
			bodyMessage = bodyMessage.replaceAll("nomeUnidade", agendamento.getUnidade().getNmApelido());
			bodyMessage = bodyMessage.replaceAll("horaAgendamento",
					horaPrevista != null ? "entre " + horaPrevista : "às " + agendamento.getHrAgendamentoFormatado());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bodyMessage;
	}
	
	private String getMensagemEmailContrachequeInfo(Agendamento agendamento, ContrachequeInfoDTO info) {
		InputStream inputStream = getClass().getResourceAsStream("/html/email-contracheque-info.txt");
		String bodyMessage = "";
		try {
			bodyMessage = IOUtils.toString(inputStream, "utf-8");
			bodyMessage = bodyMessage.replaceAll("nomePaciente", agendamento.getCliente().getNmCliente());
			bodyMessage = bodyMessage.replaceAll("nrContrato", agendamento.getContrato().getNrContrato());
			bodyMessage = bodyMessage.replaceAll("nrMatricula", info.getNrMatricula());
			bodyMessage = bodyMessage.replaceAll("nmOrgao", info.getNmOrgao());
			bodyMessage = bodyMessage.replaceAll("nmSenha", info.getNmSenha());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bodyMessage;
	}

	public void sendEmailBoletoPagamentoAgendamento(Despesa despesa, BoletoBancario boleto) {
		Agendamento agendamento = despesa.getAgendamento();
		String email = "";
		String assunto = "Pagamento de agendamento";

		if (agendamento.getDependente() != null) {
			if (agendamento.getDependente().getNmEmail() != null) {
				email = agendamento.getDependente().getNmEmail();
				if (!validar(email)) {
					email = agendamento.getCliente().getNmEmail();
				}
			} else {
				email = agendamento.getCliente().getNmEmail();
			}
		} else {
			email = agendamento.getCliente().getNmEmail();
		}

		String corpoEmail = getMensagemBoletoPagamentoAgendamento(despesa);
		String fileName = "boleto_" + agendamento.getId() + ".pdf";
		EmailUtil.sendEmailBoletoPagamentoAgendamento(email, assunto, corpoEmail, boleto.getLinkBoleto(), fileName);
	}

	private String getMensagemBoletoPagamentoAgendamento(Despesa despesa) {
		Agendamento agendamento = despesa.getAgendamento();
		InputStream inputStream = getClass().getResourceAsStream("/html/email-boleto-agendamento.txt");
		String bodyMessage = "";
		try {
			bodyMessage = IOUtils.toString(inputStream, "utf-8");
			String name = agendamento.getDependente() != null ? agendamento.getDependente().getNmDependente()
					: agendamento.getCliente().getNmCliente();
			bodyMessage = bodyMessage.replaceAll("nomePaciente", name);
			bodyMessage = bodyMessage.replaceAll("nomeServico", agendamento.getServico().getNmServico());
			bodyMessage = bodyMessage.replaceAll("nomeUnidade", agendamento.getUnidade().getNmApelido());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bodyMessage;
	}

	private Despesa novaDespesaRetorno(Despesa despesaAnterior, Agendamento novoAgendamento) {
		Despesa despesaConsultaRetorno = new Despesa();

		despesaConsultaRetorno.setAgendamento(novoAgendamento);
		despesaConsultaRetorno.setContratoCliente(despesaAnterior.getContratoCliente());
		despesaConsultaRetorno.setContratoDependente(despesaAnterior.getContratoDependente());
		despesaConsultaRetorno.setInSituacao(1); // Quitada
		despesaConsultaRetorno.setInFormaPagamento(16); // Retorno

		despesaConsultaRetorno.setDtDespesa(new Date());
		despesaConsultaRetorno.setNmObservacao("Despesa de Retorno");

		despesaConsultaRetorno.setNrDespesa(despesaService.gerarCodigoDespesa());

		despesaConsultaRetorno.setVlDespesaAberto(0D);
		despesaConsultaRetorno.setVlDespesaCoberta(0D);
		despesaConsultaRetorno.setVlDespesaTotal(0D);

		return despesaConsultaRetorno;

	}

	private void salvarAgendamentoRetorno(Agendamento agendamento) {

		agendamento.setInStatus(1);
		agendamento.setBoDespesaGerada(true);
		// Salva o novo agendamento
		agendamentoDAO.persist(agendamento);

		// Recupera a despesa salva no agendamento anterior
		Despesa despesaAnterior = agendamentoDAO.getDespesaAgendamento(agendamento.getIdAgendamentoAux());

		// Cria uma nova despesa com os mesmos valores da despesa anterior
		Despesa novaDespesaRetorno = novaDespesaRetorno(despesaAnterior, agendamento);

		// Salva a despesa
		novaDespesaRetorno = despesaDAO.persist(novaDespesaRetorno);

		// Cria e salva a parcela unica
		Parcela parcelaDespesa = new Parcela();
		parcelaDespesa.setDespesa(novaDespesaRetorno);
		parcelaDespesa.setDtInclusao(new Date());
		parcelaDespesa.setDtVencimento(new Date());
		parcelaDespesa.setInFormaPagamento(novaDespesaRetorno.getInFormaPagamento());
		parcelaDespesa.setInFormaPagamento(novaDespesaRetorno.getInFormaPagamento());
		parcelaDespesa.setFormaPagamentoEfetuada("RETORNO");
		parcelaDespesa.setNrParcela(novaDespesaRetorno.getNrDespesa().concat("/01"));
		parcelaDespesa.setVlParcela(novaDespesaRetorno.getVlDespesaAberto());

		parcelaDespesa.setDtPagamento(new Date());
		parcelaDespesa.setVlPago(novaDespesaRetorno.getVlDespesaAberto());
		parcelaDespesa.setVlJuros(0D);
		parcelaDespesa.setVlMulta(0D);

		parcelaDAO.persist(parcelaDespesa);

		// Cria e salva o(s) servico(s) da despesa e o(s) encaminhamento(s)

		HashMap<Long, Encaminhamento> enc = new LinkedHashMap<Long, Encaminhamento>();
		HashMap<Long, DespesaServico> servDesp = new LinkedHashMap<Long, DespesaServico>();

		for (DespesaServico servicos : despesaAnterior.getDespesaServicos()) {

			DespesaServico novaDespesaServico = new DespesaServico();
			novaDespesaServico.setDespesa(novaDespesaRetorno);
			novaDespesaServico.setInCoberto(true);
			novaDespesaServico.setQtServico(servicos.getQtServico());
			novaDespesaServico.setServico(servicos.getServico());
			novaDespesaServico.setVlServico(servicos.getVlServico());

			novaDespesaServico = despesaServicoDAO.persist(novaDespesaServico);

			servDesp.put(servicos.getServico().getId(), novaDespesaServico);

			if (servicos.getEncaminhamento() != null) {
				if (!enc.containsKey(servicos.getEncaminhamento().getId())) {
					enc.put(servicos.getEncaminhamento().getId(), servicos.getEncaminhamento());
				}
			}
		}

		for (Map.Entry<Long, Encaminhamento> entry : enc.entrySet()) {

			Encaminhamento encaminhamentoAnterior = encaminhamentoDAO.searchByKey(Encaminhamento.class, entry.getKey());

			Encaminhamento novoEncaminhamento = new Encaminhamento();
			if (encaminhamentoAnterior.getCredenciada() != null) {
				novoEncaminhamento.setCredenciada(encaminhamentoAnterior.getCredenciada());
			} else if (encaminhamentoAnterior.getProfissional() != null) {
				novoEncaminhamento.setProfissional(agendamento.getProfissional());
			}
			novoEncaminhamento.setDtEncaminhamento(new Date());
			novoEncaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());

			novoEncaminhamento = encaminhamentoDAO.persist(novoEncaminhamento);

			List<DespesaServico> servicosAnteriores = despesaServicoDAO
					.getDespesaServicoPorEncaminhamento(entry.getKey());

			for (DespesaServico servico : servicosAnteriores) {
				if (servDesp.containsKey(servico.getServico().getId())) {
					DespesaServico ds = (DespesaServico) servDesp.get(servico.getServico().getId());
					ds.setEncaminhamento(novoEncaminhamento);
					despesaServicoDAO.update(ds);
				}
			}
		}
	}

	public Agendamento saveAgendamentoCobertoeConvenio(Agendamento agendamento, Despesa despesa) {
		agendamento.setBoDespesaGerada(true);
		// Salva agendamento
		if (agendamento.getId() == null) {
			if (despesa.getInFormaPagamento().equals(FormaPagamentoEnum.COBERTURA_DO_PLANO.getId())
					|| (despesa.getInFormaPagamento().equals(FormaPagamentoEnum.CONVENIO.getId())
							&& despesa.getInSituacao().equals(StatusPagamentoDespesa.PAGO.getId()))) {
				agendamento.setInStatus(1);

			} else {
				agendamento.setInStatus(0);
			}
			agendamento = agendamentoDAO.persist(agendamento);

		}

		// Salva despesa
		if (despesa.getId() == null) {
			Contrato contratoaux = new Contrato();

			Encaminhamento novoEncaminhamento = new Encaminhamento();
			novoEncaminhamento.setProfissional(agendamento.getProfissional());
			novoEncaminhamento.setDtEncaminhamento(new Date());
			novoEncaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());
			novoEncaminhamento.setNmObservacao("");
			novoEncaminhamento = encaminhamentoDAO.persist(novoEncaminhamento);

			if (despesa.getContratoCliente().getContrato().getEmpresaCliente() != null) {
				contratoaux = contratoDAO.getContratoAtivoEmpresaCliente(
						despesa.getContratoCliente().getContrato().getEmpresaCliente().getId());
			} else {
				contratoaux = despesa.getContratoCliente().getContrato();
			}

			List<DespesaServico> despesaServicos = despesa.getDespesaServicos();
			for (DespesaServico item : despesaServicos) {

				item.setEncaminhamento(novoEncaminhamento);

				List<Carencia> listaCarencia = carenciaDAO.getCarencia(contratoaux.getId(), item.getServico().getId());
				if (listaCarencia != null && listaCarencia.size() > 0) {
					for (Carencia c : listaCarencia) {
						c.setTotalConsulta(c.getTotalConsulta() + 1);
						carenciaDAO.update(c);
					}
				}
			}

			despesa.setAgendamento(agendamento);
			despesaDAO.persist(despesa);
		} else {

			Encaminhamento encaminhamento = despesa.getDespesaServicos().get(0).getEncaminhamento();

			if (encaminhamento != null && encaminhamento.getId() == null) {

				encaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());
				encaminhamentoDAO.persist(encaminhamento);

			}

			despesaDAO.update(despesa);
		}

		// despesa.getInFormaPagamento().equals(FormaPagamentoEnum.
		if (despesa.getInFormaPagamento().equals(FormaPagamentoEnum.CONVENIO.getId())
				&& despesa.getInSituacao().equals(StatusPagamentoDespesa.PAGO.getId())) {

			GuiaServico guiaServico = guiaService.getGuiaValidabyClienteDependenteServico(agendamento);

			guiaServico.setStatus(StatusGuiaServicoEnum.AGENDADO.getId());
			guiaService.updateGuiaServico(guiaServico);

			DespesaGuiaServico despesaGS = new DespesaGuiaServico();
			despesaGS.setDespesa(despesa);
			despesaGS.setGuiaServico(guiaServico);
			despesaGS.setDtInclusaoLog(new Timestamp(new Date().getTime()));
			despesaGuiaServicoDAO.persist(despesaGS);
		}

		return agendamento;
	}

	private String geraCodigoAgendamento(Agendamento agendamento) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String nrAgendamento = "";
		Date data = new Date();

		// Ano atual
		String nrAnoAtual = df.format(data);
		String novoCodAgendamento = agendamentoDAO.getNovoCodigoAgendamento();

		if (novoCodAgendamento.equals("0")) {
			nrAgendamento = "000001/" + nrAnoAtual;
		} else {

			// Retorna o ano da ultima atividade cadastrada.
			String[] ultimocodigo = novoCodAgendamento.split("/");
			String nrAnoMaximo = ultimocodigo[ultimocodigo.length - 1];

			// Retorna o sequencial da ultima atividade cadastrada
			String nrSequencial = ultimocodigo[0];

			// Verifica se o ano atual e o mesmo que o ano da ultima atividade
			// cadastrada
			if (nrAnoMaximo.equals(nrAnoAtual)) {
				// Acrescenta um(1) ao sequencial retornado
				Integer novoSeq = Integer.parseInt(nrSequencial) + 1;
				String nrNovoSeq = novoSeq.toString();

				// Preenche o sequencial para o formato de 4 digitos
				while (nrNovoSeq.trim().length() < 6) {
					nrNovoSeq = "0".concat(nrNovoSeq);
				}

				// Gera o numero da atividade no formato ano/sequencial.
				nrAgendamento = nrNovoSeq.concat("/").concat(nrAnoAtual);

			} else {
				// Gera o primeiro numero de atividade do ano atual no formato
				// ano/sequencial.
				nrAgendamento = "000001".concat("/").concat(nrAnoAtual);
			}
		}

		return nrAgendamento;
	}

	private Map<String, Boolean> checkPropriedadesAgendamento(Agendamento agendamento) {
		ServicoSubEspecialidade subesp = null;
		ServicoEspecialidade esp = null;

		if (agendamento.getSubEspecialidade() != null) {
			try {
				subesp = especialidadeDAO
						.getServicoSubEspecialidadePorSubEspecialidade(agendamento.getSubEspecialidade().getId());
			} catch (NoResultException e) {
				logger.error("Nao foi encontrado Servico Sub-Especialidade", e);
				subesp = null;
			}
		}

		if (agendamento.getEspecialidade() != null) {
			try {
				esp = especialidadeDAO.getServicoEspecialidadePorEspecialidade(agendamento.getEspecialidade().getId());
			} catch (NoResultException e) {
				logger.error("Nao foi encontrado Servico Especialidade", e);
				esp = null;
			}
		}

		DespesaServico despesaServico = new DespesaServico();

		despesaServico.setInCoberto(false);
		despesaServico.setQtServico(1);

		if (agendamento.getServico() != null) {

			despesaServico.setServico(agendamento.getServico());
			despesaServico.setVlServico(agendamento.getServico().getVlServico());
		} else if (subesp != null) {
			despesaServico.setServico(subesp.getServico());
			despesaServico.setVlServico(subesp.getServico().getVlServico());
		} else if (esp != null) {
			despesaServico.setServico(esp.getServico());
			despesaServico.setVlServico(esp.getServico().getVlServico());
		}

		Map<String, Boolean> boos = verificarCobertura(despesaServico.getServico(), agendamento);
		return boos;
	}

	private Despesa gerarCoberturaPlano(Agendamento agendamento) {

		ServicoSubEspecialidade subesp = null;
		ServicoEspecialidade esp = null;

		if (agendamento.getSubEspecialidade() != null) {
			try {
				subesp = especialidadeDAO
						.getServicoSubEspecialidadePorSubEspecialidade(agendamento.getSubEspecialidade().getId());
			} catch (NoResultException e) {
				logger.error("Nao foi encontrado Servico Sub-Especialidade", e);
				subesp = null;
			}
		}

		if (agendamento.getEspecialidade() != null) {
			try {
				esp = especialidadeDAO.getServicoEspecialidadePorEspecialidade(agendamento.getEspecialidade().getId());
			} catch (NoResultException e) {
				logger.error("Nao foi encontrado Servico Especialidade", e);
				esp = null;
			}
		}

		DespesaServico despesaServico = new DespesaServico();

		despesaServico.setInCoberto(false);
		despesaServico.setQtServico(1);

		if (agendamento.getServico() != null) {

			despesaServico.setServico(agendamento.getServico());
			despesaServico.setVlServico(agendamento.getServico().getVlServico());
		} else if (subesp != null) {
			despesaServico.setServico(subesp.getServico());
			despesaServico.setVlServico(subesp.getServico().getVlServico());
		} else if (esp != null) {
			despesaServico.setServico(esp.getServico());
			despesaServico.setVlServico(esp.getServico().getVlServico());
		}

		Map<String, Boolean> boos = verificarCobertura(despesaServico.getServico(), agendamento);

		if (!boos.get("isCobertura"))
			return null;

		Despesa despesa = new Despesa();

		despesa.setAgendamento(agendamento);

		if (agendamento.getCliente() != null) {
			ContratoCliente cc = (agendamento.getContrato() != null && agendamento.getContrato().getId() != null)
					? contratoDAO.getContratoCliente(agendamento.getContrato().getId())
					: null;
			despesa.setContratoCliente(cc);
			agendamento.getPaciente().setContratoCliente(cc);
		} else {
			ContratoDependente cd = (agendamento.getContrato() != null && agendamento.getContrato().getId() != null)
					? contratoDAO.getContratoDependente(agendamento.getContrato().getId())
					: null;
			despesa.setContratoDependente(cd);

			agendamento.getPaciente().setContratoDependente(cd);
		}

		if (agendamento.getPaciente().getContratoDependente() != null) {
			// ContratoDependente cd = contratoDAO
			// .getContratoDependente(agendamento.getPaciente().getContratoDependente().getId());
			despesa.setContratoDependente(agendamento.getPaciente().getContratoDependente());
			agendamento.getPaciente().setContratoDependente(agendamento.getPaciente().getContratoDependente());
		}

		// System.out.println("agendamento.getDependente().getId() = " +
		// agendamento.getDependente().getId());
		// agendamento.setDependente(clienteServico.getDependente(agendamento
		// .getDependente().getId()));
		// paciente = new PacienteDTO<Dependente>(agendamento.getDependente());

		despesa.setBoExcluida(false);
		despesa.setBoParcelaDa(false);
		despesa.setDtDespesa(new Date());
		despesa.setInFormaPagamento(10);
		despesa.setInSituacao(1);
		despesa.setNmObservacao("");
		despesa.setNrDespesa(despesaService.gerarCodigoDespesa());
		despesa.setVlDespesaAberto(0D);
		despesa.setUnidade(agendamento.getUnidade());
		despesaServico.setDespesa(despesa);
		despesaServico.setInCoberto(boos.get("isCobertura"));

		Map<String, Double> doubles = getValoresPorProfissional(agendamento.getServico().getId(),
				agendamento.getProfissional().getId(), agendamento.getPaciente(), boos.get("isLiberado"),
				boos.get("isServicoProfissional"), agendamento.getBoParticular());

		despesaServico.setVlServico(doubles.get("vlServico"));
		despesaServico.setVlCusto(agendamento.getServico().getVlCusto());
		despesaServico.setDespesa(despesa);
		despesa.addDespesaservico(despesaServico);

		despesa.setVlDespesaAberto(0D);
		despesa.setVlDespesaCoberta(doubles.get("vlServicoAssociadoAux"));
		despesa.setVlDespesaTotal(despesa.getVlDespesaAberto() + despesa.getVlDespesaCoberta());

		despesa.setInSituacao(1);

		Parcela parcela = new Parcela();
		parcela.setBoNegociada(false);
		parcela.setDespesa(despesa);
		parcela.setDtInclusao(despesa.getDtDespesa());
		parcela.setDtPagamento(despesa.getDtDespesa());
		parcela.setDtVencimento(despesa.getDtDespesa());
		parcela.setInFormaPagamento(despesa.getInFormaPagamento());
		parcela.setFormaPagamentoEfetuada("COBERTURA DO PLANO");
		parcela.setNrParcela(despesa.getNrDespesa() + "/01");
		parcela.setVlPago(despesa.getVlDespesaTotal());
		parcela.setVlParcela(despesa.getVlDespesaTotal());
		despesa.addParcela(parcela);

		return despesa;
	}

	public Agendamento updateAgendamento(Agendamento agendamento) {
		Properties props = PropertiesLoader.getInstance().load("message.properties");

		if (agendamento.getJustificativaRetorno() != null && agendamento.getJustificativaLoginGerente() != null) {
			LogGerente log = new LogGerente();
			log.setAcao(7);
			Funcionario func;
			try {
				func = securityService.getFuncionarioByLogin(agendamento.getJustificativaLoginGerente());
				log.setIdFuncionario(func.getId());
			} catch (NoResultException e) {
				throw new AgendamentoException(props.getProperty("funcionario.login.notfound"), ErrorType.DANGER);
			}

			log.setIdCliente(agendamento.getCliente().getId());
			log.setIdContrato(agendamento.getContrato().getId());
			log.setIdDocumento(agendamento.getId());
			log.setIdPlano(
					agendamento.getContrato().getPlano() != null ? agendamento.getContrato().getPlano().getId() : null);
			log.setDtAcao(new Date());
			log.setObservacao(agendamento.getJustificativaRetorno());

			logGerenteDAO.saveLogGerente(log);
		} else {
			if (agendamento.getBoDespesaGerada() != null && agendamento.getBoDespesaGerada()) {
				Despesa despesa = agendamentoDAO.getDespesaAgendamento(agendamento.getId());
				if (despesa != null) {
					GuiaServico gs = despesaGuiaServicoDAO.getGuiaServicobyDespesa(despesa.getId());
					if (gs != null && !(agendamento.getDtAgendamento().after(gs.getGuia().getDtInicio())
							&& agendamento.getDtAgendamento().before(gs.getGuia().getDtValidade()))) {
						throw new AgendamentoException(props.getProperty("agendamento.dtinvalida.guia"),
								ErrorType.WARNING);
					}
				}
			}
		}

		return agendamentoDAO.update(agendamento);
	}

	public Despesa getDespesaAgendamento(Long idAgendamento) {
		Despesa despesa = agendamentoDAO.getDespesaAgendamento(idAgendamento);
		// despesa.getDespesaServicos().size();
		return despesa;
	}

	public byte[] generateDespesaPDF(Long idAgendamento) {

		Despesa despesa = getDespesaAgendamento(idAgendamento);

		return despesaService.generateDespesaPDF(despesa.getId());

	}

	private Agendamento validaConsultorio(Agendamento agendamento) {

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String dtAgendamento = df.format(agendamento.getDtAgendamento());
		String dtAtual = df.format(new Date());
		Properties props = PropertiesLoader.getInstance().load("message.properties");

		if (dtAgendamento.compareTo(dtAtual) < 0) {

			throw new AgendamentoException(props.getProperty("data.ultrapassada"), ErrorType.DANGER);
		} else {

			/*
			 * Fluxo para verificar se o agendamento está no horario de atendimento. Status
			 * -1 indica que o agendamento está fora do horario.
			 */

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(agendamento.getDtAgendamento());
			Integer diaSemana = new Integer(calendar.get(Calendar.DAY_OF_WEEK) - 1);

			AtendimentoProfissional atend = agendamentoDAO.getConsultorioPorEspecialidadeProfissional(
					agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(),
					agendamento.getUnidade().getId(), agendamento.getHrAgendamentoFormatado(),
					agendamento.getServico().getId(), diaSemana);

			if (atend == null) {
				agendamento.setBoForaHorario(true);
				return agendamento;
			}

			/*
			 * if(agendamento.getInStatus() != 4){
			 * if(!verificaHorarioPresenca(agendamento)){
			 * agendamento.setBoForaPresenca(true); return agendamento; } }else{
			 */
			agendamento.setBoForaPresenca(false);
			// }

		}

		return agendamento;
	}

	private static Time convertStringToTime(String value) {
		try {
			String[] tempo = value.split(":");
			int hora = Integer.parseInt(tempo[0]);
			int minutos = Integer.parseInt(tempo[1]);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, hora);
			cal.set(Calendar.MINUTE, minutos);
			cal.set(Calendar.SECOND, 0);
			Time time = new Time(cal.getTimeInMillis());
			return time;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public AtendimentoProfissional getAtendimentoProfissionalByAgendamento(Long idAgendamento) {
		Agendamento agendamento = agendamentoDAO.getAgendamento(idAgendamento);
		return getAtendimentoProfissionalByAgendamento(agendamento);

	}

	public AtendimentoProfissional getAtendimentoProfissionalByAgendamento(Agendamento agendamento) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(agendamento.getDtAgendamento());
		Integer diaSemana = new Integer(calendar.get(Calendar.DAY_OF_WEEK) - 1);

		AtendimentoProfissional atendProfissional = agendamentoDAO.getConsultorioPorEspecialidadeProfissional(
				agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(),
				agendamento.getUnidade().getId(), agendamento.getHrAgendamentoFormatado(),
				agendamento.getServico().getId(), diaSemana);
		return atendProfissional;
	}

	@SuppressWarnings("unused")
	private boolean verificaHorarioPresenca(Agendamento agendamento) {

		setConsultorioAgendamento(agendamento);

		Time horaInicial = convertStringToTime(agendamento.getHrInicio());
		Time horaFinal = convertStringToTime(agendamento.getHrFim());

		Calendar moment = Calendar.getInstance();
		moment.set(Calendar.SECOND, 0);

		Calendar hrInicial = Calendar.getInstance();
		hrInicial.setTimeInMillis(horaInicial.getTime());

		Calendar hrFinal = Calendar.getInstance();
		hrFinal.setTimeInMillis(horaFinal.getTime());

		Long diffMinInicio = hrInicial.getTimeInMillis() - moment.getTimeInMillis();
		Long diffMinFim = hrFinal.getTimeInMillis() - moment.getTimeInMillis();

		if ((diffMinInicio / (60 * 1000)) < 60L && (diffMinFim / (60 * 1000)) > -60L) {
			return true;
		}
		return false;

	}

	public Agendamento updateStatusPresente(Long idAgendamento, boolean triado, Integer prioridade,
			boolean boLogadoGerente, Integer statusPrioridade, String obsUrgencia) {

		Agendamento agendamento = agendamentoDAO.searchByKey(Agendamento.class, idAgendamento);
		agendamento.setPaciente(carregarPaciente(agendamento));

		agendamento = validaConsultorio(agendamento);
		if (agendamento.getBoForaHorario()) {
			return agendamento;
		} else if (agendamento.getBoForaPresenca() && !boLogadoGerente) {

			return agendamento;
		}
		agendamento.setBoForaPresenca(false);

		// Processo para distincao do agendamento: agendamento normal ou retorno

		// Servico agendamento é consulta?
		ServicoEspecialidade se = servicoEspecialidadeDAO.getServicoEspecialidade(agendamento.getServico().getId(),
				agendamento.getEspecialidade().getId());
		Agendamento ag = null;
		Long idCliente = agendamento.getCliente().getId();
		Long idDependente = agendamento.getDependente() != null ? agendamento.getDependente().getId() : null;
		Long idFuncionario = agendamento.getProfissional().getId();
		Long idEspecialidade = agendamento.getEspecialidade().getId();

		if (se != null && se.getBoConsulta().equals(true)) {
			// Obter ultimo agendamento consulta dessa especialidade e profissional
			ag = agendamentoDAO.getUltimoAgendamentoConsultaByProfissionalEspecialidade(idCliente, idDependente,
					idFuncionario, idEspecialidade);
			if (ag != null) {

				long diffdata = 0;
				long dias = 0;
				Calendar cHrAtendIni = new GregorianCalendar();
				Calendar cHrAtendFim = new GregorianCalendar();

				cHrAtendIni.setTime(ag.getDtAgendamento());
				cHrAtendFim.setTime(agendamento.getDtAgendamento());

				diffdata = cHrAtendFim.getTimeInMillis() - cHrAtendIni.getTimeInMillis();
				dias = diffdata / 86400000L;
				System.out.println("Dias 1: " + dias);

				if (ag.getBoReturnUltimate() == null) {
					agendamentoDAO.updateRetornoAgendamento(idCliente, idDependente, idFuncionario, idEspecialidade);
					ag = agendamentoDAO.getUltimoAgendamentoConsultaByProfissionalEspecialidade(idCliente, idDependente,
							idFuncionario, idEspecialidade);
				}

				if (dias > 30)
					agendamento.setBoReturnUltimate(false);
				else {
					if (!ag.getBoReturnUltimate()) {
						agendamento.setBoReturnUltimate(true);
						agendamento.setIdAgendamentoPai(ag.getId());
					} else if (ag.getBoReturnUltimate())
						agendamento.setBoReturnUltimate(false);
				}
			} else
				agendamento.setBoReturnUltimate(false);

		}

		// Processo para distincao do agendamento consulta: normal ou retorno
		retornoAgendamentoConsulta(agendamento);

		validaStatusPresente(agendamento, triado, prioridade, statusPrioridade, obsUrgencia); //verificar os boparticular que ainda existem
		agendamento.setInTipoPrioridade(prioridade);
		agendamento.setInStatusPrioridade(statusPrioridade);
		agendamento.setObsUrgencia(obsUrgencia);
		agendamento = agendamentoDAO.update(agendamento);

		return agendamento;
	}

	// sobre-carga do metodo para o totem
	//public Agendamento updateStatusPresente(Agendamento agendamento, boolean triado, Boolean prioridade) {
	public Agendamento updateStatusPresente(Agendamento agendamento, boolean triado, Integer prioridade, Integer statusPrioridade, String obsUrgencia) {
		agendamento.setPaciente(carregarPaciente(agendamento));
		agendamento = validaConsultorio(agendamento);

		if (agendamento.getBoForaHorario()) {
			return agendamento;
		} else if (agendamento.getBoForaPresenca()) {

			return agendamento;
		}

		// Processo para distincao do agendamento consulta: normal ou retorno
		retornoAgendamentoConsulta(agendamento);
		
		//mudar depois
		validaStatusPresente(agendamento, triado, prioridade, statusPrioridade, obsUrgencia);

		agendamento = agendamentoDAO.update(agendamento);

		return agendamento;
	}

	/**
	 * Obtem agendamento, verifica se serviço é consulta e identifica o agendamento
	 * com retorno ou não.
	 * 
	 * @param agendamento
	 * @return
	 */
	private Agendamento retornoAgendamentoConsulta(Agendamento agendamento) {

		// Servico agendamento é consulta?
		ServicoEspecialidade se = servicoEspecialidadeDAO.getServicoEspecialidade(agendamento.getServico().getId(),
				agendamento.getEspecialidade().getId());
		Agendamento ag = null;
		Long idCliente = agendamento.getCliente().getId();
		Long idDependente = agendamento.getDependente() != null ? agendamento.getDependente().getId() : null;
		Long idFuncionario = agendamento.getProfissional().getId();
		Long idEspecialidade = agendamento.getEspecialidade().getId();

		if (se != null && se.getBoConsulta().equals(true)) {
			// Obter ultimo agendamento consulta dessa especialidade e profissional
			ag = agendamentoDAO.getUltimoAgendamentoConsultaByProfissionalEspecialidade(idCliente, idDependente,
					idFuncionario, idEspecialidade);
			if (ag != null) {

				long diffdata = 0;
				long dias = 0;
				Calendar cHrAtendIni = new GregorianCalendar();
				Calendar cHrAtendFim = new GregorianCalendar();

				cHrAtendIni.setTime(ag.getDtAgendamento());
				cHrAtendFim.setTime(agendamento.getDtAgendamento());

				diffdata = cHrAtendFim.getTimeInMillis() - cHrAtendIni.getTimeInMillis();
				dias = diffdata / 86400000L;
				System.out.println("Dias 1: " + dias);

				if (ag.getBoReturnUltimate() == null) {
					agendamentoDAO.updateRetornoAgendamento(idCliente, idDependente, idFuncionario, idEspecialidade);
					ag = agendamentoDAO.getUltimoAgendamentoConsultaByProfissionalEspecialidade(idCliente, idDependente,
							idFuncionario, idEspecialidade);
				}

				if (dias > 30)
					agendamento.setBoReturnUltimate(false);
				else {
					if (!ag.getBoReturnUltimate()) {
						agendamento.setBoReturnUltimate(true);
						agendamento.setIdAgendamentoPai(ag.getId());
					} else if (ag.getBoReturnUltimate())
						agendamento.setBoReturnUltimate(false);
				}
			} else
				agendamento.setBoReturnUltimate(false);

		}

		return agendamento;
	}

	public Agendamento updateStatusAusente(Long idAgendamento, Long idOperador) {

		Agendamento agendamento = agendamentoDAO.searchByKey(Agendamento.class, idAgendamento);
		Agendamento agendamentoAtualizado = null;
		agendamento.setPaciente(carregarPaciente(agendamento));
		agendamento.setInStatus(3);
		agendamento.setIdOperadorAlteracao(idOperador);

		try {
			agendamentoAtualizado = agendamento = agendamentoDAO.update(agendamento);
		} catch (Exception e) {
			throw new ObjectNotFoundException("Erro ao atualizar o status para ausente");
		}

		return agendamentoAtualizado;
	}

	public Agendamento updateStatusAtendido(Long idAgendamento, Long idOperador) {

		RegistroAgendamento regAgendamento = null;
		Agendamento agendamentoAtualizado = null;
		Despesa despesa = null;
		GuiaServico guiaServico = null;

		Agendamento agendamento = agendamentoDAO.searchByKey(Agendamento.class, idAgendamento);
		agendamento.setPaciente(carregarPaciente(agendamento));
		agendamento.setInStatus(2);
		agendamento.setIdOperadorAlteracao(idOperador);

		AtendimentoProfissional atendProfissional = atendimentoProfissionalDAO.getAtendimentoProfissional(
				agendamento.getDtAgendamento(), agendamento.getProfissional().getId(), agendamento.getUnidade().getId(),
				agendamento.getHrAgendamento(), agendamento.getEspecialidade().getId());

		BigInteger intTurno = atendimentoProfissionalDAO.getAtendimentoProfissional(agendamento.getDtAgendamento(),
				agendamento.getProfissional().getId(), agendamento.getUnidade().getId());

		ServicoProfissional servProfissional = servicoProfissionalDAO
				.getServicoProfissional(agendamento.getProfissional().getId(), agendamento.getServico().getId());

		// Registro de atendimento para faturamento medico
		if (atendProfissional != null) {
			regAgendamento = new RegistroAgendamento();
			regAgendamento.setIdAgendamento(agendamento.getId());
			regAgendamento.setHrAtendimentoInicio(atendProfissional.getHrInicioContratado());
			regAgendamento.setHrAtendimentoFim(atendProfissional.getHrFimContratado());
			regAgendamento.setVlHora(atendProfissional.getVlHora());
			regAgendamento.setVlComissao(
					agendamento.getBoReturnUltimate() != null && agendamento.getBoReturnUltimate() == true ? 0.0
							: (servProfissional.getVlComissao() != null ? servProfissional.getVlComissao() : 0.0));
			regAgendamento.setInTurno(atendProfissional.getInTurno());
			regAgendamento.setQtdTurno(Integer.parseInt(intTurno.toString()));

			if (atendProfissional.getBoParticular() != null && atendProfissional.getBoParticular())
				regAgendamento.setTipoAtendimento(TipoAtendimento.PARTICULAR.getId());
			else if (atendProfissional.getBoHorista() != null && atendProfissional.getBoHorista())
				regAgendamento.setTipoAtendimento(TipoAtendimento.HORISTA.getId());
			else if (atendProfissional.getBoProducao() != null && atendProfissional.getBoProducao())
				regAgendamento.setTipoAtendimento(TipoAtendimento.PRODUCAO.getId());
			else if (atendProfissional.getBoFixo() != null && atendProfissional.getBoFixo())
				regAgendamento.setTipoAtendimento(TipoAtendimento.FIXO.getId());
			else
				regAgendamento.setTipoAtendimento(TipoAtendimento.FIXO.getId());
			
			// define se agendamento foi finalizado por código de barras
			agendamento.setBoAtendimentoBarcode(atendProfissional.getBoBarcode());
		} else {
			regAgendamento = new RegistroAgendamento();
			regAgendamento.setIdAgendamento(agendamento.getId());
			regAgendamento.setHrAtendimentoInicio(null);
			regAgendamento.setHrAtendimentoFim(null);
			regAgendamento.setVlHora(null);
			regAgendamento.setVlComissao(servProfissional != null ? servProfissional.getVlComissao() : null);
			regAgendamento.setInTurno(null);
			regAgendamento.setQtdTurno(intTurno != null ? Integer.parseInt(intTurno.toString()) : null);
			regAgendamento.setTipoAtendimento(null);
		}

		// Registro de atendimento com guia
		despesa = agendamentoDAO.getDespesaAgendamento(idAgendamento);
		guiaServico = despesaGuiaServicoDAO.getGuiaServicobyDespesa(despesa.getId());
		if (guiaServico != null && guiaServico.getStatus().equals(StatusGuiaServicoEnum.AGENDADO.getId())) {
			guiaServico.setStatus(StatusGuiaServicoEnum.CONSUMIDO.getId());
			guiaServico.setDtUtilizacao(new Timestamp(new Date().getTime()));
			guiaService.updateGuiaServico(guiaServico);
		}

		try {
			registroAgendamentoDAO.persist(regAgendamento);
			agendamentoAtualizado = agendamento = agendamentoDAO.update(agendamento);
		} catch (Exception e) {
			throw new ObjectNotFoundException("Erro ao atualizar o status para atendido");
		}

		return agendamentoAtualizado;
	}

	private void registrarPresenca(Agendamento agendamento) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("America/Manaus"));
		calendar.set(Calendar.MILLISECOND, 0);
		agendamento.setHoraPresenca(new Time(calendar.getTime().getTime()));
		// System.out.println( agendamento.getHoraPresenca() );
	}

	private void inativaOrdem(Agendamento agendamento) {

		List<OrdemAgendamento> listOrdemAgendamento = new ArrayList<OrdemAgendamento>();

		listOrdemAgendamento = ordemAgendamentoDAO.getListOrdemAgendamentoPorAgendamento(agendamento.getId());

		if (listOrdemAgendamento != null && listOrdemAgendamento.size() > 0) {
			for (OrdemAgendamento oa : listOrdemAgendamento) {
				oa.setBoExcluido(new Boolean(true));
				ordemAgendamentoDAO.update(oa);
			}
		}

		entityManager.flush();

	}

	private OrdemAgendamento verificaDivergenciaPrioridade(Agendamento agendamento, Integer prioridade) {

		OrdemAgendamento ordemAgendamento = new OrdemAgendamento();

		ordemAgendamento = ordemAgendamentoDAO.getOrdemAgendamentoPorAgendamento(agendamento.getId());

		if (ordemAgendamento != null && ordemAgendamento.getInTipoPrioridade().intValue() == prioridade) {
			return ordemAgendamento;
		} else {
			return null;
		}

	}

	private void registraOrdem(Agendamento agendamento, Integer prioridade, Integer inStatusPrioridade, String obsUrgencia) {

		OrdemAgendamento ordemAgendamento = new OrdemAgendamento();

		ordemAgendamento = verificaDivergenciaPrioridade(agendamento, prioridade);

		if (ordemAgendamento == null) {

			inativaOrdem(agendamento);

			Double nrOrdem = new Double(0);
			String nrFicha = "";

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(agendamento.getDtAgendamento());
			Integer diaSemana = new Integer(calendar.get(Calendar.DAY_OF_WEEK) - 1);

			// BigDecimal bigDecimal = new
			// BigDecimal(ordemAgendamentoDAO.maxOrdemNormal(agendamento.getProfissional().getId(),
			// agendamento.getEspecialidade().getId()) + new
			// Float(1.1)).setScale(2).floatValue();

			AtendimentoProfissional atend = agendamentoDAO.getConsultorioPorEspecialidadeProfissional(
					agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(),
					agendamento.getUnidade().getId(), agendamento.getHrAgendamentoFormatado(),
					agendamento.getServico().getId(), diaSemana);
			if(prioridade != null){
				if(prioridade.equals(TipoPrioridade.PRIORIDADE_80ANOS.getId())){ //prioridade 80
					nrOrdem = new BigDecimal(ordemAgendamentoDAO.maxOrdemPrioridadedaPrioridade(agendamento.getProfissional().getId(),
							agendamento.getEspecialidade().getId(), atend.getId()) + new Float(1))
							.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					nrFicha = nrOrdem.intValue() + "PP";
				} else if (prioridade.equals(TipoPrioridade.PRIORIDADE.getId())) { //prioridade
					
					BigDecimal ordemPP = new BigDecimal(ordemAgendamentoDAO.maxOrdemPrioridadedaPrioridade(agendamento.getProfissional().getId(),
							agendamento.getEspecialidade().getId(), atend.getId()));
					
					BigDecimal ordemP = new BigDecimal(ordemAgendamentoDAO.maxOrdemPrioridade(agendamento.getProfissional().getId(),
							agendamento.getEspecialidade().getId(), atend.getId()));
					/*nrOrdem = new BigDecimal(ordemAgendamentoDAO.maxOrdemPrioridade(agendamento.getProfissional().getId(),
							agendamento.getEspecialidade().getId(), atend.getId()) + new Float(1.1))
									.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();*/
					if(ordemPP.intValue() == 0 && ordemP.intValue() == 0){
						nrOrdem = new BigDecimal(ordemP.intValue() + new Float(1.1)).setScale(2,  BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(ordemPP.intValue() > ordemP.intValue()){
						nrOrdem = new BigDecimal(ordemPP.intValue() + new Float(0.1)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else{
						nrOrdem = new BigDecimal(ordemP.intValue() + new Float(1.1)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}
					nrFicha = nrOrdem.intValue() + "P";
				} else if(prioridade.equals(TipoPrioridade.NORMAL.getId())){//normal
				
					BigDecimal ordemTodos = new BigDecimal(ordemAgendamentoDAO.maxOrdemTodos(agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(), atend.getId()));
					BigDecimal ordemNormal = new BigDecimal(ordemAgendamentoDAO.maxOrdemNormal(agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(), atend.getId()));
					if(ordemTodos.intValue() > ordemNormal.intValue()){
						nrOrdem = new BigDecimal(ordemTodos.intValue() + new Float(0.2)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else{
						nrOrdem = new BigDecimal(ordemNormal.intValue() + new Float(1.2)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}
					/*nrOrdem = new BigDecimal(
							ordemAgendamentoDAO
									.maxOrdemNormal(agendamento.getProfissional().getId(),
											agendamento.getEspecialidade().getId(), atend.getId())
									.intValue() + new Float(1.2)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();*/

					nrFicha = nrOrdem.intValue() + "";
				}
			}else{
				BigDecimal ordemNormal = new BigDecimal(ordemAgendamentoDAO.maxOrdemNormal(agendamento.getProfissional().getId(), agendamento.getEspecialidade().getId(), atend.getId()));
				nrOrdem = new BigDecimal(ordemNormal.intValue() + new Float(1.2)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
				nrFicha = nrOrdem.intValue() + "";
			}

			ordemAgendamento = new OrdemAgendamento();

			ordemAgendamento.setIdAgendamento(agendamento.getId());
			ordemAgendamento.setIdEspecialidade(agendamento.getEspecialidade().getId());
			ordemAgendamento.setIdFuncionario(agendamento.getProfissional().getId());
			//ordemAgendamento.setBoPrioridade(new Boolean(prioridade));
			ordemAgendamento.setInTipoPrioridade(prioridade);
			ordemAgendamento.setInStatusPrioridade(inStatusPrioridade);
			ordemAgendamento.setObsUrgencia(obsUrgencia);
			ordemAgendamento.setNrOrdem(nrOrdem);
			ordemAgendamento.setNrFicha(nrFicha);
			ordemAgendamento.setIdAtendimentoProfissional(atend.getId());

			ordemAgendamentoDAO.persist(ordemAgendamento);

		}

	}

	/**
	 * Verifica se agendamento é retorno e tem cobertura de plano.
	 * 
	 * @param agendamento
	 * @return
	 */
	private boolean validaContratoDoRetorno(Agendamento agendamento) {
		List<Despesa> listdespesaPai = new ArrayList<Despesa>();
		List<Parcela> listParcelaPai = new ArrayList<Parcela>();

		if (agendamento.getBoReturnUltimate() && agendamento.getIdAgendamentoPai() != null) {

			listdespesaPai = agendamentoDAO.getDespesasPorAgendamento(agendamento.getIdAgendamentoPai());

			if (listdespesaPai != null) {
				listParcelaPai = agendamentoDAO.getParcelasDespesa(listdespesaPai.get(0).getId());

				if (listParcelaPai != null && listParcelaPai.size() > 0
						&& listParcelaPai.get(0).getFormaPagamentoEfetuada() != null
						&& listParcelaPai.get(0).getFormaPagamentoEfetuada().equals("COBERTURA DO PLANO")) {

					return true;

				}
			}
		}

		return false;
	}

	private Agendamento validaStatusPresente(Agendamento agendamento, boolean triado, Integer prioridade, Integer statusPrioridade, String obsUrgencia) { /*boolean prioridade*/

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String dtAgendamento = df.format(agendamento.getDtAgendamento());
		String dtAtual = df.format(new Date());
		Properties props = PropertiesLoader.getInstance().load("message.properties");

		if (dtAgendamento.compareTo(dtAtual) < 0) {

			throw new AgendamentoException(props.getProperty("data.ultrapassada"), ErrorType.DANGER);
		} else {

			Map<String, Boolean> mapBooleanValues = verificarCoberturaPorAgendamento(agendamento);

			// Agendamento Liberado
			if (agendamento.getInStatus().equals(1)) {

				// Contrato Bloqueado
				if (mapBooleanValues.get("isBloqueado")) {

					// Já possui despesa?
					if (agendamento.getBoDespesaGerada() == true) {
						List<Despesa> listdespesa = new ArrayList<Despesa>();
						listdespesa = agendamentoDAO.getDespesasPorAgendamento(agendamento.getId());

						if (listdespesa != null) {

							if (listdespesa.get(0).getInFormaPagamento()
									.equals(FormaPagamentoEnum.COBERTURA_DO_PLANO.getId())
									|| validaContratoDoRetorno(agendamento)) {

								// Bloqueia Agendamento
								agendamento.setInStatus(0);
								agendamento.setBoStatusTriagem(new Boolean(false));
								inativaOrdem(agendamento);

							} else {

								if (listdespesa.get(0).getInFormaPagamento() != FormaPagamentoEnum.COBERTURA_DO_PLANO
										.getId() && listdespesa.get(0).getInSituacao().equals(1)) {

									if (dtAgendamento.compareTo(dtAtual) == 0) {
										agendamento.setInStatus(4); // 'Paciente presente'
										registrarPresenca(agendamento);
										agendamento.setBoStatusTriagem(new Boolean(false));
										registraOrdem(agendamento, prioridade, statusPrioridade, obsUrgencia);

									} else
										throw new AgendamentoException(props.getProperty("liberacao.agendamento"),
												ErrorType.DANGER);
								}
							}
						} else {

							if (((listdespesa != null && listdespesa.get(0).getInFormaPagamento()
									.equals(FormaPagamentoEnum.DEBITO_AUTOMATICO.getId()))
									|| (listdespesa != null && listdespesa.get(0).getInFormaPagamento() == 17))
									&& listdespesa.get(0).getInSituacao().equals(0)) {

								if (dtAgendamento.compareTo(dtAtual) == 0) {
									agendamento.setInStatus(4); // 'Paciente presente'
									registrarPresenca(agendamento);
									agendamento.setBoStatusTriagem(new Boolean(false));
									registraOrdem(agendamento, prioridade, statusPrioridade, obsUrgencia);

								} else
									throw new AgendamentoException(props.getProperty("liberacao.agendamento"),
											ErrorType.DANGER);

							} else
								throw new AgendamentoException(props.getProperty("despesa.quitada"), ErrorType.DANGER);
						}
					}
				} else {
					if (dtAgendamento.compareTo(dtAtual) == 0) {
						agendamento.setInStatus(4); // 'Paciente Presente'
						registrarPresenca(agendamento);
						agendamento.setBoStatusTriagem(new Boolean(false));
						registraOrdem(agendamento, prioridade, statusPrioridade, obsUrgencia);

					} else
						throw new AgendamentoException(props.getProperty("liberacao.agendamento"), ErrorType.DANGER);
				}

				// Agendamento PRESENTE
			} else if (agendamento.getInStatus().equals(4)) {

				// TODO - Implementar lógica de verificação de perfil
				agendamento.setInStatus(1); // Agendamento liberado
				agendamento.setBoStatusTriagem(new Boolean(false));
				inativaOrdem(agendamento);

				// Agendamento BLOQUEADO
			} else if (agendamento.getInStatus().equals(0)) {

				// Contrato Liberado
				if (!mapBooleanValues.get("isBloqueado")) {
					if (agendamento.getBoDespesaGerada() != null && agendamento.getBoDespesaGerada().equals(true)) {

						Despesa despesa = getDespesaAgendamento(agendamento.getId());

						if (despesa.getInSituacao() == StatusPagamentoDespesa.PAGO.getId()) {

							// Agendamento Liberado
							agendamento.setInStatus(1);
							agendamento.setBoStatusTriagem(new Boolean(false));
							inativaOrdem(agendamento);
						} else {
							if (mapBooleanValues.get("isConvenio") && mapBooleanValues.get("isGuia")) {
								if (dtAgendamento.compareTo(dtAtual) == 0) {
									agendamento.setInStatus(4); // 'Paciente presente'
									registrarPresenca(agendamento);
									agendamento.setBoStatusTriagem(new Boolean(false));
									registraOrdem(agendamento, prioridade, statusPrioridade, obsUrgencia);

									despesa = inicializarDespesaConvenio(despesa);
									return saveAgendamentoCobertoeConvenio(agendamento, despesa);

								} else
									throw new AgendamentoException(props.getProperty("liberacao.agendamento"),
											ErrorType.DANGER);

							} else
								throw new AgendamentoException(props.getProperty("agendamento.despesa"),
										ErrorType.DANGER);
						}
					} else
						throw new AgendamentoException(props.getProperty("agendamento.despesa"), ErrorType.DANGER);
				} else
					throw new AgendamentoException(props.getProperty("paciente.liberado"), ErrorType.DANGER);
			}
		}

		return agendamento;
	}

	private Map<String, Boolean> verificarCoberturaPorAgendamento(Agendamento agendamento) {

		// Verifica Cobertura
		Carencia carencia = null;
		CoberturaPlano coberturaPlano = null;
		List<Mensalidade> listaMensalidade = new ArrayList<Mensalidade>();
		Contrato contratoAux = new Contrato();

		Boolean isLiberado = true;
		Boolean isBloqueado = false;
		Boolean isCarencia = false;
		Boolean isDevendo = false;
		Boolean isCobertura = false;
		Boolean isConveniadaBloqueada = false;
		Boolean isTodasMensalidadesPagas = true;
		Boolean isConvenio = false;
		Boolean isGuia = false;

		Map<String, Boolean> booleanValuesMap = new HashMap<String, Boolean>();

		PacienteDTO<?> pacienteDTO = carregarPaciente(agendamento);

		// status do agendamento BLOQUEADO ou LIBERADO
		if (agendamento.getInStatus().equals(0) || agendamento.getInStatus().equals(1))

			// Contrato Bloqueado
			if (agendamento.getContrato().getBoBloqueado()) {
				isBloqueado = true;

				if (agendamento.getContrato().getEmpresaCliente() != null)
					isConveniadaBloqueada = true;
			}

		if (isBloqueado) {
			isDevendo = true;

			// Traz as mensalidades pagas
			listaMensalidade = agendamentoDAO.getMensalidadesPagas(agendamento.getContrato().getId());

			if (listaMensalidade == null || listaMensalidade.size() == 0) {

				isTodasMensalidadesPagas = false;
			}
		}

		// Associado faz uso do plano?
		if (isLiberado) {
			if (pacienteDTO.getContratoDependente() == null) {
				if (pacienteDTO.getContratoCliente().getContrato().getBoNaoFazUsoPlano() != null) {
					if (pacienteDTO.getContratoCliente().getContrato().getBoNaoFazUsoPlano()) {
						isLiberado = false;
					}
				}
			}
		}

		if (agendamento.getContrato().getPlano() != null && agendamento.getServico() != null) {

			try {
				coberturaPlano = agendamentoDAO.getCoberturaPlano(agendamento.getContrato().getPlano().getId(),
						agendamento.getServico().getId());
			} catch (Exception e) {
				coberturaPlano = null;
			}
		}

		// Tem Cobertura?
		if (coberturaPlano != null && isLiberado) {

			// tem cobertura de plano
			isCobertura = coberturaPlano.getBoCobertura();
		}

		// Verifica a carencia
		if (isCobertura) {
			if (coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0) {
				contratoAux = new Contrato();

				// Se o paciente tem contrato com conveniada
				if (agendamento.getContrato().getEmpresaCliente() != null) {

					contratoAux = agendamentoDAO
							.getContratoPorEmpresaCliente(agendamento.getContrato().getEmpresaCliente().getId());
				} else {
					contratoAux = pacienteDTO.getContratoCliente().getContrato();
				}
				// Existe algo na tabela carencia ?
				carencia = agendamentoDAO.getCarencia(contratoAux.getId(), agendamento.getServico().getId());

				if (carencia == null) {
					isCarencia = true;
				}

				if (!isCarencia)
					if (coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0)
						if (carencia.getCarencia() == null || carencia.getCarencia() > 0)
							isCarencia = true;
			}
		}

		// Planos Empresariais
		if (agendamento.getContrato().getEmpresaCliente() != null) {
			isConvenio = true;
			Despesa despesa = agendamentoDAO.getDespesaAgendamento(agendamento.getId());
			if (despesa.getContratoDependente() != null && despesa.getContratoDependente().getId() != null)

				if (despesa.getContratoDependente().getBoFazUsoPlano().equals(true)
						|| despesa.getContratoDependente().getBoFazUsoPlano() == null)
					isConvenio = true;
				else
					isConvenio = false;
		}

		if (isConvenio) {
			GuiaServico guiaServico = guiaService.getGuiaValidabyClienteDependenteServico(agendamento);
			if (guiaServico != null)
				isGuia = true;
		}

		booleanValuesMap.put("isBloqueado", isBloqueado);
		booleanValuesMap.put("isDevendo", isDevendo);
		booleanValuesMap.put("isCarencia", isCarencia);
		booleanValuesMap.put("isCobertura", isCobertura);
		booleanValuesMap.put("isLiberado", isLiberado);
		booleanValuesMap.put("isTodasMensalidadesPagas", isTodasMensalidadesPagas);
		booleanValuesMap.put("isConveniadaBloqueada", isConveniadaBloqueada);
		booleanValuesMap.put("isConvenio", isConvenio);
		booleanValuesMap.put("isGuia", isGuia);

		return booleanValuesMap;
	}

	public Map<String, Double> getValoresPorProfissional(Long idServico, Long idProfissional, PacienteDTO<?> paciente,
			Boolean isLiberado, Boolean isServicoProfissional, Boolean boParticular) {

		isLiberado = true;
		CoberturaPlano coberturaPlano = null;
		Boolean isCobertura = false;
		Boolean isConvenio = false;

		Map<String, Double> doubleValuesMap = new HashMap<String, Double>();

		Servico servico = servicoDAO.getServico(idServico);

		doubleValuesMap.put("vlServico", servico.getVlServicoAssociado());
		doubleValuesMap.put("vlServicoAux", servico.getVlServico());
		doubleValuesMap.put("vlServicoAssociadoAux", servico.getVlServicoAssociado());
		doubleValuesMap.put("vlCusto", servico.getVlCusto());

		ServicoProfissional servicoProfissional = null;
		if (idProfissional != null) {
			servicoProfissional = agendamentoDAO.getListaProfissionais(idServico, idProfissional);
		}

		if (servicoProfissional != null) {

			isServicoProfissional = true;
			doubleValuesMap.put("vlServico", servicoProfissional.getVlServicoAssociado());
			doubleValuesMap.put("vlServicoAux", servicoProfissional.getVlServico());
			doubleValuesMap.put("vlServicoAssociadoAux", servicoProfissional.getVlServicoAssociado());
		}

		// Associado
		if (isLiberado) {
			if (paciente.getCliente().getBoNaoAssociado() != null && paciente.getCliente().getBoNaoAssociado()) {
				doubleValuesMap.put("vlServico", servico.getVlServico());
				isLiberado = false;
				if (isServicoProfissional != null && isServicoProfissional) {

					doubleValuesMap.put("vlServico", servicoProfissional.getVlServico());
				}
			}
		}

		// Associado faz uso do plano
		// if (isLiberado) {
		if ((paciente.getContratoDependente() == null)
				&& (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano() != null
						&& (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano()))) {
			doubleValuesMap.put("vlServico", servico.getVlServico());
			isLiberado = false;
			if (isServicoProfissional != null && isServicoProfissional) {

				doubleValuesMap.put("vlServico", servicoProfissional.getVlServico());
			}
		}
		// }

		// Associado está Bloqueado
		if (isLiberado) {
			if (paciente.getContratoCliente().getContrato().getBoBloqueado() != null
					&& paciente.getContratoCliente().getContrato().getBoBloqueado()) {
				doubleValuesMap.put("vlServico", servico.getVlServico());
				isLiberado = false;
				if (isServicoProfissional != null && isServicoProfissional) {

					doubleValuesMap.put("vlServico", servicoProfissional.getVlServico());
				}
			}
		}

		// O serviço está na lista da cobertura plano ?
		if (isLiberado) {

			if (paciente.getContratoCliente().getPlano() != null) {

				try {
					if (boParticular != null && boParticular.booleanValue()) {
						coberturaPlano = null;
					} else {
						if (paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null
								&& (paciente.getContratoDependente().getBoFazUsoPlano() != null
										&& paciente.getContratoDependente().getBoFazUsoPlano().equals(false))) {
							coberturaPlano = null;
							isLiberado = false;
							isConvenio = false;
						} else {
							coberturaPlano = agendamentoDAO.getCoberturaPlano(
									paciente.getContratoCliente().getPlano().getId(), servico.getId());
						}
					}

				} catch (NoResultException e) {
					coberturaPlano = null;
				}
			}

			// Tem Cobertura ?
			if (boParticular != null && boParticular.booleanValue()) {
				isCobertura = false;
			} else {
				if (coberturaPlano != null) {
					isCobertura = coberturaPlano.getBoCobertura();
				}

			}

			// Plano tem total de consultas ?
			if (isCobertura) {
				if (paciente.getContratoCliente().getPlano().getTotalConsulta() != null
						&& paciente.getContratoCliente().getPlano().getTotalConsulta() > 0) {
					if (paciente.getContratoCliente().getCredito() == null || (paciente.getContratoCliente()
							.getCredito() > paciente.getContratoCliente().getPlano().getTotalConsulta())) {
						isCobertura = false;
					}
				}
			}

			// O servico tem carencia ?
			List<Carencia> listaCarencia = new ArrayList<Carencia>();
			if (isCobertura) {
				if ((coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0)
						|| (coberturaPlano.getTotalConsulta() != null && coberturaPlano.getTotalConsulta() > 0)) {
					Contrato contratoaux = new Contrato();

					if (paciente.getContratoCliente().getContrato().getEmpresaCliente() != null
							&& (paciente.getContratoCliente().getContrato().getEmpresaCliente().getId() != 64
									|| paciente.getContratoCliente().getContrato().getEmpresaCliente().getId() != 80))
						contratoaux = contratoDAO.getContratoAtivoEmpresaCliente(
								paciente.getContratoCliente().getContrato().getEmpresaCliente().getId());
					else
						contratoaux = paciente.getContratoCliente().getContrato();

					// Carencia
					listaCarencia = carenciaDAO.getCarencia(contratoaux.getId(), idServico);
					if (listaCarencia == null || listaCarencia.size() <= 0)
						isCobertura = false;

					if (isCobertura)
						if (coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0)
							if (listaCarencia.get(0).getCarencia() == null || listaCarencia.get(0).getCarencia() > 0)
								isCobertura = false;

				}
			}

			// O servico tem total Consulta?
			if (isCobertura)
				if (coberturaPlano.getTotalConsulta() != null && coberturaPlano.getTotalConsulta() > 0)
					if (listaCarencia.get(0).getTotalConsulta() == null
							|| (listaCarencia.get(0).getTotalConsulta() >= coberturaPlano.getTotalConsulta()))
						isCobertura = false;

		}

		// Planos Empresariais
		if (paciente.getContratoCliente().getContrato().getEmpresaCliente() != null) {
			if (!(paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null
					&& paciente.getContratoDependente().getBoFazUsoPlano().equals(false))) {
				isConvenio = true;
			}
		}

		if ((isCobertura || isConvenio) && coberturaPlano != null) {
			doubleValuesMap.put("vlServico", coberturaPlano.getVlServico());

			if (isLiberado) {
				doubleValuesMap.put("vlServico", coberturaPlano.getVlServicoAssociado());
			}

			doubleValuesMap.put("vlServicoAux", coberturaPlano.getVlServico());
			doubleValuesMap.put("vlServicoAssociadoAux", coberturaPlano.getVlServicoAssociado());

		}

		return doubleValuesMap;
	}

	public Map<String, Boolean> verificarCobertura(Servico servico, Agendamento agendamento) {
		CoberturaPlano coberturaPlano = null;

		Encaminhamento encaminhamento = new Encaminhamento();

		Boolean isLiberado = true;
		Boolean isServProfissional = false;
		Boolean isCobertura = false;
		Boolean isConvenio = false;
		Boolean isGuia = false;
		Boolean isCoberturaCredenciada = true;

		Map<String, Boolean> booleanValuesMap = new HashMap<String, Boolean>();

		ServicoProfissional servicoProfissional = null;
		if (agendamento.getProfissional() != null) {
			servicoProfissional = agendamentoDAO.getListaProfissionais(servico.getId(),
					agendamento.getProfissional().getId());
		}

		// Se existe o serviço cadastrado para o profissional, os valores sao
		// cobrados de acordo com a tabela de serviços do profissional.
		if (servicoProfissional != null) {
			isServProfissional = true;
		}

		PacienteDTO<?> paciente = carregarPaciente(agendamento);

		// Associado ?
		if (isLiberado)
			if (paciente.getCliente().getBoNaoAssociado() != null && paciente.getCliente().getBoNaoAssociado()) {
				isLiberado = false;
				isCoberturaCredenciada = false;
			}

		// Associado faz uso do plano ?
		if (isLiberado) {
			if (agendamento.getPaciente().getContratoDependente() == null) {
				System.out.println("1............ paciente.getContratoDependente() == null");
			}

			if (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano() != null) {
				System.out.println(
						"2............ paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano() != null");
			}

			if (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano()) {
				System.out.println("3............ paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano()");
			}

			if ((paciente.getContratoDependente() == null)
					&& (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano() != null
							&& (paciente.getContratoCliente().getContrato().getBoNaoFazUsoPlano()))) {
				isLiberado = false;
				isCoberturaCredenciada = false;
			}
		}

		// Associado está Bloqueado ?
		if (isLiberado)
			if (paciente.getContratoCliente().getContrato().getBoBloqueado() != null
					&& paciente.getContratoCliente().getContrato().getBoBloqueado()) {
				isLiberado = false;
				isCoberturaCredenciada = false;
			}

		// O serviço está na lista da cobertura plano ?
		if (isLiberado) {

			if (paciente.getContratoCliente().getPlano() != null)
				try {
					if (agendamento.getBoParticular() != null && agendamento.getBoParticular().booleanValue()) {
						coberturaPlano = null;
					} else {
						if (paciente.getDependente() != null && paciente.getDependente().getId() != null
								&& paciente.getContratoDependente() != null
								&& (paciente.getContratoDependente().getBoFazUsoPlano() != null
										&& paciente.getContratoDependente().getBoFazUsoPlano().equals(false))) {
							coberturaPlano = null;
							isLiberado = false;
							isConvenio = false;
							isCoberturaCredenciada = false;
						} else {
							coberturaPlano = agendamentoDAO.getCoberturaPlano(
									paciente.getContratoCliente().getPlano().getId(), servico.getId());
						}
					}

				} catch (NoResultException e) {
					coberturaPlano = null;
				}

			// Tem Cobertura ?
			if (agendamento.getBoParticular() != null && agendamento.getBoParticular().booleanValue()) {
				isCobertura = false;
			} else {
				if (coberturaPlano != null) {
					isCobertura = coberturaPlano.getBoCobertura();
				}

			}

			// Plano tem total de consultas ?
			if (isCobertura)
				if (paciente.getContratoCliente().getPlano().getTotalConsulta() != null
						&& paciente.getContratoCliente().getPlano().getTotalConsulta() > 0)
					if (paciente.getContratoCliente().getCredito() == null || (paciente.getContratoCliente()
							.getCredito() > paciente.getContratoCliente().getPlano().getTotalConsulta()))
						isCobertura = false;

			// O servico tem carencia ?
			Carencia carencia = null;
			if (isCobertura)
				if ((coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0)
						|| (coberturaPlano.getTotalConsulta() != null && coberturaPlano.getTotalConsulta() > 0)) {
					Contrato contratoaux = null;

					if (paciente.getContratoCliente().getContrato().getEmpresaCliente() != null
							&& (paciente.getContratoCliente().getContrato().getEmpresaCliente().getId() != 64
									&& paciente.getContratoCliente().getContrato().getEmpresaCliente().getId() != 109
									&& paciente.getContratoCliente().getContrato().getEmpresaCliente().getId() != 80)) {
						contratoaux = agendamentoDAO.getContratoPorEmpresaCliente(
								paciente.getContratoCliente().getContrato().getEmpresaCliente().getId());
					} else {
						contratoaux = paciente.getContratoCliente().getContrato();
					}

					// Carencia
					carencia = agendamentoDAO.getCarencia(contratoaux.getId(), servico.getId());
					if (carencia == null)
						isCobertura = false;

					if (isCobertura)
						if (coberturaPlano.getCarencia() != null && coberturaPlano.getCarencia() > 0)
							if (carencia.getCarencia() == null || carencia.getCarencia() > 0)
								isCobertura = false;
				}

			// O servico tem total Consulta?
			// if (isCobertura)
			// if (coberturaPlano.getTotalConsulta() != null &&
			// coberturaPlano.getTotalConsulta() > 0)
			// if (coberturaPlano.getTotalConsulta() == null
			// || (coberturaPlano.getTotalConsulta() >= coberturaPlano.getTotalConsulta()))
			// isCobertura = false;

			if (isCobertura) {
				if ((paciente.getContratoCliente().getPlano().getId().equals(99L)) // Plano Sa�de Leste
						|| (paciente.getContratoCliente().getPlano().getId().equals(100L)) // Plano Odonto Leste
						|| (paciente.getContratoCliente().getPlano().getId().equals(101L))) { // Plano Orto Leste

					/* Encaminhamento para Credenciada */

					// Get encaminhamento ou usando credenciada ou profissional
					if (encaminhamento != null) {

						if (encaminhamento.getCredenciada() != null) { // Encaminhamento para Credenciada
							if (!encaminhamento.getCredenciada().getId().equals(94L)) { // Credenciada Grande Circular
								isCobertura = false;
								isCoberturaCredenciada = false;
							}
						} else {
							isCobertura = false;
							isCoberturaCredenciada = false;
						}
					}

					// TODO - Verificar necessidade de Agendamento para Unidade
					// da Grande Circular
				}
			}

		}

		// Planos Empresariais
		if (paciente.getContratoCliente().getContrato().getEmpresaCliente() != null) {
			isConvenio = true;
			if (paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null)

				if (paciente.getContratoDependente().getBoFazUsoPlano().equals(true)
						|| paciente.getContratoDependente().getBoFazUsoPlano() == null)
					isConvenio = true;
				else
					isConvenio = false;
		}

		if (isConvenio) {
			GuiaServico guiaServico = guiaService.getGuiaValidabyClienteDependenteServico(agendamento);
			if (guiaServico != null) {
				isGuia = true;
				isCobertura = true;
			}
		}

		if (isLiberado && coberturaPlano != null && coberturaPlano.getValidadeContratual() != null
				&& coberturaPlano.getValidadeContratual().intValue() > 0) {

			Date dataDespesa = agendamentoDAO.getUltimaDespesaCoberta(agendamento.getServico().getId(),
					agendamento.getContrato().getId(), coberturaPlano.getValidadeContratual(),
					agendamento.getCliente().getId(),
					agendamento.getDependente() != null && agendamento.getDependente().getId() != null
							? agendamento.getDependente().getId()
							: null,
					coberturaPlano.getPlano().getId());

			if (dataDespesa != null) {
				isCobertura = false;
				isCoberturaCredenciada = false;
			}
		}

		booleanValuesMap.put("isServProfissional", isServProfissional);
		booleanValuesMap.put("isCobertura", isCobertura);
		booleanValuesMap.put("isConvenio", isConvenio);
		booleanValuesMap.put("isGuia", isGuia);
		booleanValuesMap.put("isLiberado", isLiberado);
		booleanValuesMap.put("isCoberturaCredenciada", isCoberturaCredenciada);
		return booleanValuesMap;
	}

	private PacienteDTO<?> carregarPaciente(Agendamento agendamento) {

		PacienteDTO<?> pacienteDTO = null;
		List<ContratoCliente> contratoClientes = null;
		List<ContratoDependente> contratoDependentes = null;
		Properties props = PropertiesLoader.getInstance().load("message.properties");

		Cliente cliente = agendamentoDAO.getCliente(agendamento.getCliente().getId());
		List<ContratoCliente> contratoClientesPorCliente = agendamentoDAO.getContratoClientePorCliente(cliente.getId());

		if (agendamento.getDependente() != null) {
			pacienteDTO = new PacienteDTO<Dependente>();
			// ContratoDependente contratoDependente =
			// contratoDependenteDAO.getContratoDependenteById(agendamento.getDependente().getId(),
			// agendamento.getContrato().getId());
			pacienteDTO.setCliente(agendamento.getDependente().getCliente());
			pacienteDTO.setCdPaciente(agendamento.getDependente().getNrCodCliente());
			pacienteDTO.setDependente(agendamento.getDependente());
			pacienteDTO.setNmPaciente(agendamento.getDependente().getNmDependente());
			pacienteDTO.setNmTitular(agendamento.getDependente().getCliente().getNmCliente());
			// pacienteDTO.setBoFazUsoPlano(contratoDependente.getBoFazUsoPlano());
			pacienteDTO.setNmTipo("Dependente");

			System.out.println("agendamento.getContrato() = " + agendamento.getContrato());
			// if (agendamento.getContrato() != null && agendamento.getContrato().getId() !=
			// null
			// && (pacienteDTO.getBoFazUsoPlano() == null ||
			// pacienteDTO.getBoFazUsoPlano().equals(true))
			// ) {

			// pacienteDTO.setBloqueado(agendamento.getContrato().getBoBloqueado() != null ?
			// agendamento.getContrato().getBoBloqueado() : false);
			// pacienteDTO.setInStatusContrato(pacienteDTO.getBloqueado() ? "Inativo" :
			// "Ativo");
			// pacienteDTO.setNrContrato(agendamento.getContrato().getNrContrato());
			// } else {

			// pacienteDTO.setBloqueado(false);
			// pacienteDTO.setInStatusContrato("Ativo");
			// }

			pacienteDTO.setCpf(FormatUtil.formatarCpf(
					agendamento.getDependente().getNrCpf() != null ? agendamento.getDependente().getNrCpf() : ""));

			if (agendamento.getContrato() != null && agendamento.getContrato().getId() != null) {
				// && (pacienteDTO.getBoFazUsoPlano() == null ||
				// pacienteDTO.getBoFazUsoPlano().equals(true))

				pacienteDTO.setBloqueado(
						agendamento.getContrato().getBoBloqueado() != null ? agendamento.getContrato().getBoBloqueado()
								: false);
				pacienteDTO.setInStatusContrato(pacienteDTO.getBloqueado() ? "Inativo" : "Ativo");
				pacienteDTO.setNrContrato(agendamento.getContrato().getNrContrato());

				contratoClientes = agendamentoDAO.getContratoCliente(agendamento.getContrato().getId());
				for (ContratoCliente cc : contratoClientes) {

					if ((cc.getContrato().getSituacao() == null || cc.getContrato().getSituacao().equals(0))
							&& cc.getCliente().getId().equals(agendamento.getDependente().getCliente().getId())) {

						pacienteDTO.setNmPlano(cc.getContrato().getPlano() != null
								? cc.getContrato().getPlano().getNmPlano().toString()
								: "PARTICULAR NÃO ASSOCIADO");
						pacienteDTO.setObsPlano(cc.getContrato().getPlano() != null
								&& cc.getContrato().getPlano().getObservacao() != null
										? cc.getContrato().getPlano().getObservacao().toString().toUpperCase()
										: "");
						pacienteDTO.setContratoCliente(cc);
						break;
					}
				}

				contratoDependentes = agendamentoDAO.getContratosDependentes(agendamento.getContrato().getId());
				for (ContratoDependente cd : contratoDependentes) {
					if ((cd.getInSituacao() == null || cd.getInSituacao().equals(0))
							&& cd.getDependente().getId().equals(agendamento.getDependente().getId())
							&& cd.getBoFazUsoPlano().equals(true)) {

						pacienteDTO.setContratoDependente(cd);
						break;
					} else if ((cd.getInSituacao() == null || cd.getInSituacao().equals(0))
							&& cd.getDependente().getId().equals(agendamento.getDependente().getId())
							&& cd.getBoFazUsoPlano().equals(false)) {
						pacienteDTO.setContratoDependente(cd);
					}
				}

			} else {

				pacienteDTO.setBloqueado(false);
				pacienteDTO.setInStatusContrato("Ativo");

				for (ContratoCliente contratoCliente : contratoClientesPorCliente) {
					if ((contratoCliente.getContrato().getSituacao() == null
							|| contratoCliente.getContrato().getSituacao().equals(0))
							&& contratoCliente.getCliente().getId()
									.equals(agendamento.getDependente().getCliente().getId())) {

						pacienteDTO.setNmPlano(contratoCliente.getContrato().getPlano() != null
								? contratoCliente.getContrato().getPlano().getNmPlano().toString()
								: "PARTICULAR NÃO ASSOCIADO");

						pacienteDTO.setObsPlano(contratoCliente.getContrato().getPlano() != null
								&& contratoCliente.getContrato().getPlano().getObservacao() != null
										? contratoCliente.getContrato().getPlano().getObservacao().toString()
												.toUpperCase()
										: "");

						pacienteDTO.setContratoCliente(contratoCliente);

						contratoDependentes = agendamentoDAO
								.getContratosDependentes(contratoCliente.getContrato().getId());

						for (ContratoDependente cd : contratoDependentes) {
							if ((cd.getInSituacao() == null || cd.getInSituacao().equals(0))
									&& cd.getDependente().getId().equals(agendamento.getDependente().getId())) {

								pacienteDTO.setContratoDependente(cd);
								break;
							}
						}

						// break;
					}
				}
			}
		} else {

			pacienteDTO = new PacienteDTO<Cliente>();

			pacienteDTO.setCliente(agendamento.getCliente());
			pacienteDTO.setCdPaciente(agendamento.getCliente().getNrCodCliente());
			pacienteDTO.setDependente(null);
			pacienteDTO.setNmPaciente(agendamento.getCliente().getNmCliente());
			pacienteDTO.setNmTitular("");
			pacienteDTO.setNmTipo("Titular");

			if (agendamento.getContrato() != null && agendamento.getContrato().getId() != null) {
				pacienteDTO.setNrContrato(agendamento.getContrato().getNrContrato());
				pacienteDTO.setBloqueado(
						agendamento.getContrato().getBoBloqueado() != null ? agendamento.getContrato().getBoBloqueado()
								: false);
				pacienteDTO.setInStatusContrato(pacienteDTO.getBloqueado() ? "Inativo" : "Ativo");
			} else {
				pacienteDTO.setNrContrato("");
				pacienteDTO.setBloqueado(false);
				pacienteDTO.setInStatusContrato(pacienteDTO.getBloqueado() ? "Inativo" : "Ativo");
			}

			pacienteDTO.setCpf(FormatUtil.formatarCpf(agendamento.getCliente().getNrCPF()));

			// TODO - Verificar necessidade para setar imagem do paciente

			if (agendamento.getContrato() != null && agendamento.getContrato().getId() != null) {

				contratoClientes = agendamentoDAO.getContratoCliente(agendamento.getContrato().getId());
				for (ContratoCliente item : contratoClientes) {
					if ((item.getContrato().getSituacao() == null || item.getContrato().getSituacao().equals(0))
							&& item.getCliente().getId().equals(agendamento.getCliente().getId())) {

						pacienteDTO.setNmPlano(item.getPlano() != null ? item.getPlano().getNmPlano().toString()
								: "PARTICULAR NÃO ASSOCIADO");

						pacienteDTO.setObsPlano(item.getPlano() != null && item.getPlano().getObservacao() != null
								? item.getPlano().getObservacao().toString().toUpperCase()
								: "");
						pacienteDTO.setContratoCliente(item);
						break;
					} else {

						throw new AgendamentoException(props.getProperty("contrato.inativo"), ErrorType.ERROR);
					}
				}
			} else {

				for (ContratoCliente item : contratoClientesPorCliente) {
					if ((item.getContrato().getSituacao() == null || item.getContrato().getSituacao().equals(0))
							&& item.getCliente().getId().equals(agendamento.getCliente().getId())) {

						pacienteDTO.setNmPlano(item.getPlano() != null ? item.getPlano().getNmPlano().toString()
								: "PARTICULAR NÃO ASSOCIADO");

						pacienteDTO.setObsPlano(item.getPlano() != null && item.getPlano().getObservacao() != null
								? item.getPlano().getObservacao().toString().toUpperCase()
								: "");
						pacienteDTO.setContratoCliente(item);
						break;
					}
				}
				cliente = null;
			}

			pacienteDTO.setContratoDependente(null);
		}

		return pacienteDTO;
	}

	public DespesaDTO getGerarDespesaAgendamento(Long idAgendamento) {

		// Properties props = PropertiesLoader.getInstance().load("message.properties");

		Agendamento agendamento = getAgendamento(idAgendamento);

		Despesa despesa = getDespesaAgendamento(idAgendamento);

		DespesaDTO despesaDTO = null;

		if (despesa != null)
			despesaDTO = validaGerarDespesaNovamente(agendamento, despesa);
		else
			despesaDTO = validaGerarDespesa(agendamento);

		if (despesaDTO != null && despesaDTO.getBoQuitada()) {
			agendamento.setInStatus(1);
			saveAgendamentoCobertoeConvenio(agendamento, despesaDTO.getDespesa());
		}

		return despesaDTO;
	}

	private DespesaDTO validaGerarDespesaNovamente(Agendamento agendamento, Despesa despesa) {
		// Properties props = PropertiesLoader.getInstance().load("message.properties");
		DespesaDTO despesaDTO = null;

		if (despesa != null) {

			despesa.setBoExcluida(true);
			despesaDAO.update(despesa);

			Parcela parcela = parcelaDAO.getParcelaPorDespesa(despesa.getId());

			if (parcela != null) {

				parcela.setBoExcluida(true);
				parcelaDAO.update(parcela);

				// if (parcela.getInformaPagamento() == 1) {
				//
				// TituloDa tituloDa = tituloDaDAO.getTituloPorParcela(parcela
				// .getId());
				//
				// if (tituloDa != null) {
				//
				// tituloDa.setInExcluido(true);
				// tituloDaDAO.update(tituloDa);
				// }
				// }
			}

			if (despesa.getAgendamento() != null) {
				agendamento.setBoDespesaGerada(false);
				agendamento.setInStatus(0);
				agendamentoDAO.update(agendamento);
			}

			despesaDTO = validaGerarDespesa(agendamento);

			// if(despesaDTO == null){
			//
			// throw new
			// AgendamentoException(props.getProperty("agendamento.despesacoberta"),
			// ErrorType.DANGER);
			// }
		}

		return despesaDTO;
	}

	// sobrecarga para geracao de despesa
	public DespesaDTO validaGerarDespesa(Agendamento agendamento) {
		return validaGerarDespesa(agendamento, null);
	}

	public DespesaDTO validaGerarDespesa(Agendamento agendamento, Boolean isFromWeb) {
		Properties props = PropertiesLoader.getInstance().load("message.properties");
		DespesaDTO despesaDTO = new DespesaDTO();

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String dtAgendamento = df.format(agendamento.getDtAgendamento());
		String dtAtual = df.format(new Date());

		if (dtAgendamento.compareTo(dtAtual) < 0) {

			throw new AgendamentoException(props.getProperty("data.ultrapassada"), ErrorType.DANGER);
		}

		PacienteDTO<?> paciente = carregarPaciente(agendamento);

		Despesa despesa = new Despesa();

		despesa.setAgendamento(agendamento);
		despesa.setContratoCliente(paciente.getContratoCliente());

		if (paciente.getContratoDependente() != null) {
			despesa.setContratoDependente(paciente.getContratoDependente());
		}

		despesa.setVlDespesaAberto(0D);
		despesa.setVlDespesaCoberta(0D);
		despesa.setVlDespesaTotal(0D);
		despesa.setDtDespesa(new Date());
		despesa.setNmObservacao("");
		despesa.setUnidade(agendamento.getUnidade());

		// TODO - Implements method "carregarDespesasNaoPagas", if needs.
		Map<String, Boolean> mapBooleanValues = verificarCobertura(agendamento.getServico(), agendamento);
		Map<String, Double> mapDoubles = getValoresPorProfissional(agendamento.getServico().getId(),
				agendamento.getProfissional().getId(), paciente, mapBooleanValues.get("isLiberado"),
				mapBooleanValues.get("isServicoProfissional"), agendamento.getBoParticular());

		if (!mapBooleanValues.get("isCobertura")
				|| (agendamento.getBoParticular() != null && agendamento.getBoParticular())) {
			DespesaServico despesaServico = verificarCoberturaPlano(despesa, agendamento, paciente);

			if (mapBooleanValues.get("isConvenio") && mapBooleanValues.get("isGuia")) {
				Despesa despesaGuia = inicializarDespesaConvenio(despesaServico.getDespesa());
				despesaDTO.setDespesa(despesaGuia);
				despesaDTO.setBoQuitada(new Boolean(true));

				return despesaDTO;
			} else {
				despesaDTO = inicializarDespesaPagamento(agendamento, despesaServico, paciente);
				despesaDTO.setBoQuitada(new Boolean(false));
			}

		} else if (mapBooleanValues.get("isCobertura") && mapBooleanValues.get("isGuia")) { // it ensures that use Guia Vasconcelos
			DespesaServico despesaServico = verificarCoberturaPlano(despesa, agendamento, paciente);

			if (mapBooleanValues.get("isConvenio") && mapBooleanValues.get("isGuia")) {
				Despesa despesaGuia = inicializarDespesaConvenio(despesaServico.getDespesa());
				despesaDTO.setDespesa(despesaGuia);
				despesaDTO.setBoQuitada(new Boolean(true));
				return despesaDTO;
			}
		} else { // Despesa Cobertura de Plano
			despesa.setBoExcluida(false);
			despesa.setInFormaPagamento(FormaPagamentoEnum.COBERTURA_DO_PLANO.getId());
			despesa.setInSituacao(1);
			despesa.setNrDespesa(despesaService.gerarCodigoDespesa());

			DespesaServico despesaservico = new DespesaServico();
			despesaservico.setDespesa(despesa);
			despesaservico.setServico(agendamento.getServico());

			despesaservico.setVlServico(mapDoubles.get("vlServico"));
			despesaservico.setInCoberto(mapBooleanValues.get("isCobertura"));
			despesaservico.setVlCusto(mapDoubles.get("vlCusto"));
			despesaservico.setQtServico(1);

			// Salvar o Encaminhamento da Despesa
			Encaminhamento novoEncaminhamento = new Encaminhamento();
			novoEncaminhamento.setProfissional(agendamento.getProfissional());
			novoEncaminhamento.setDtEncaminhamento(new Date());
			novoEncaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());
			novoEncaminhamento.setNmObservacao("");
			despesaservico.setEncaminhamento(novoEncaminhamento);

			despesa.addDespesaservico(despesaservico);
			despesa.setVlDespesaCoberta(mapDoubles.get("vlServicoAssociadoAux"));
			despesa.setVlDespesaTotal(despesa.getVlDespesaAberto() + despesa.getVlDespesaCoberta());

			Parcela parcela = new Parcela();
			parcela.setBoNegociada(false);
			parcela.setDespesa(despesa);
			parcela.setDtInclusao(despesa.getDtDespesa());
			parcela.setDtPagamento(despesa.getDtDespesa());
			parcela.setDtVencimento(despesa.getDtDespesa());
			parcela.setInFormaPagamento(despesa.getInFormaPagamento());
			parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.COBERTURA_DO_PLANO.getDescription());
			parcela.setNrParcela(despesa.getNrDespesa() + "/01");
			parcela.setVlPago(despesa.getVlDespesaTotal());
			parcela.setVlParcela(despesa.getVlDespesaTotal());
			despesa.addParcela(parcela);

			// saveAgendamentoCobertoeConvenio(agendamento, despesa);
			despesaDTO.setDespesa(despesa);
			despesaDTO.setBoQuitada(new Boolean(true));

			// throw new DespesaInformativeException("Despesa Coberta Gerada Automaticamente
			// com Sucesso!",
			// ErrorType.WARNING);

			// throw new
			// AgendamentoException(props.getProperty("agendamento.despesacoberta"),
			// ErrorType.DANGER);
			return despesaDTO;
		}
		
		// data de vencimento da despesa para web
		if (isFromWeb != null && isFromWeb.booleanValue() && dtAgendamento.compareTo(dtAtual) > 0) {
			Calendar c = Calendar.getInstance();
			if (agendamento.getIsConvenio() != null && agendamento.getIsConvenio().booleanValue()) {
				c.setTime(agendamento.getDtAgendamento());
			} else {
				// dia de geração da despesa (tempo atual)
				c.setTime(despesaDTO.getDespesa().getDtDespesa());
				c.add(Calendar.DATE, 1);
				if (dtAgendamento.compareTo(dtAtual) > 1) {
					if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						c.add(Calendar.DATE, 1);
					}
				}
			}
			despesaDTO.setDtVencimento(c.getTime());
		}
		return despesaDTO;
	}

	/**
	 * Completa as informações da DespesaServico, Ecaminhamento e ajustes(valores e
	 * insituacao) da Despesa.
	 * 
	 * @param despesa
	 * @param agendamento
	 * @param paciente
	 * @return
	 */
	private DespesaServico verificarCoberturaPlano(Despesa despesa, Agendamento agendamento, PacienteDTO<?> paciente) {
		Boolean usaCredito = false;

		Double vlNaoAssociado = null;
		Double vlAssociado = null;
		Double vlCusto = new Double(agendamento.getServico().getVlCusto());

		DespesaServico despesaServico = new DespesaServico();
		Encaminhamento encaminhamento = new Encaminhamento();

		Map<String, Double> mapDoubleValues = null;
		Map<String, Boolean> mapBooleanValues = verificarCobertura(agendamento.getServico(), agendamento);

		mapDoubleValues = getValoresPorProfissional(agendamento.getServico().getId(),
				agendamento.getProfissional().getId(), paciente, mapBooleanValues.get("isLiberado"),
				mapBooleanValues.get("isServicoProfissional"), agendamento.getBoParticular());

		if (agendamento.getServico() != null && agendamento.getServico().getNmServico().trim().length() > 0) {

			despesaServico.setQtServico(1);
			despesaServico.setServico(agendamento.getServico());
			despesaServico.setVlServico(new Double(agendamento.getServico().getVlServico()));
		}

		if (mapDoubleValues != null && mapDoubleValues.get("vlServicoAux") != null)
			vlNaoAssociado = mapDoubleValues.get("vlServicoAux");
		else
			vlNaoAssociado = agendamento.getServico().getVlServico();

		if (mapDoubleValues != null && mapDoubleValues.get("vlServicoAssociadoAux") != null)
			vlAssociado = mapDoubleValues.get("vlServicoAssociadoAux");
		else
			vlAssociado = agendamento.getServico().getVlServicoAssociado();

		// TODO - Needs verify if Conditional clause with Encaminhamento by
		// Credenciada is necessary

		encaminhamento.setProfissional(agendamento.getProfissional());
		encaminhamento.setDtEncaminhamento(new Date());
		encaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());
		encaminhamento.setNmObservacao("");

		despesaServico.setEncaminhamento(encaminhamento);

		if (mapBooleanValues.get("isCobertura")) {

			if (paciente.getContratoCliente().getPlano().getTotalConsulta() != null
					&& paciente.getContratoCliente().getPlano().getTotalConsulta() > 0) {
				usaCredito = true;
				for (int i = 0; i < despesaServico.getQtServico(); i++) { // debita os creditos
					if (paciente.getContratoCliente().getCredito() != null
							&& paciente.getContratoCliente().getCredito() > 0) { // verifica se paciente
																					// ainda tem credito
																					// para consulta
						paciente.getContratoCliente().setCredito(paciente.getContratoCliente().getCredito() - 1);
						despesa.setVlDespesaCoberta(
								(despesa.getVlDespesaCoberta() != null ? despesa.getVlDespesaCoberta() : 0)
										+ vlAssociado);
					} else
						despesa.setVlDespesaAberto(
								(despesa.getVlDespesaAberto() != null ? despesa.getVlDespesaAberto() : 0)
										+ vlAssociado);
				}
			}
		}

		if (mapBooleanValues.get("isLiberado"))
			despesaServico.setVlServico(vlAssociado);
		else
			despesaServico.setVlServico(vlNaoAssociado);

		despesaServico.setInCoberto(mapBooleanValues.get("isCobertura"));
		if (!usaCredito) {

			if (despesaServico.getInCoberto()) {

				despesa.setVlDespesaCoberta(despesa.getVlDespesaCoberta()
						+ (despesaServico.getQtServico() * despesaServico.getVlServico()));
			} else {

				despesa.setVlDespesaAberto(
						despesa.getVlDespesaAberto() + (despesaServico.getQtServico() * despesaServico.getVlServico()));
			}
		}

		if (despesa.getVlDespesaAberto() > 0)
			despesa.setInSituacao(0);
		else
			despesa.setInSituacao(1);

		despesa.setVlDespesaTotal(despesa.getVlDespesaAberto() + despesa.getVlDespesaCoberta());

		despesaServico.setVlCusto(vlCusto);
		despesaServico.setDespesa(despesa);

		despesa.addDespesaservico(despesaServico);

		return despesaServico;
	}

	public Despesa registrarDespesa(Long idAgendamento, DespesaDTO despesaDTO) {

		return registrarDespesaAgendamento(idAgendamento, despesaDTO);
	}

	private Despesa registrarDespesaAgendamento(Long idAgendamento, DespesaDTO despesaDTO) {

		Agendamento agendamento = getAgendamento(idAgendamento);
		Despesa despesa = despesaDTO.getDespesa();
		despesa.setAgendamento(agendamento);
		despesa.setBoExcluida(false);
		despesa.setBoParcelaDa(false);

		List<DespesaServico> ds = new ArrayList<DespesaServico>();
		ds.addAll(despesa.getDespesaServicos());
		despesa.getDespesaServicos().clear();

		Parcela parcelaDespesa = new Parcela();

		validaRegistrarDespesa(despesaDTO, agendamento);
		ds.get(0).setDespesa(despesa);
		ds.get(0).setBoRealizado(false);
		ds.get(0).setDtInclusaoLog(new Timestamp(new Date().getTime()));

		despesa.getDespesaServicos().add(ds.get(0));
		Encaminhamento encaminhamento = despesa.getDespesaServicos().get(0).getEncaminhamento();

		if (encaminhamento != null && encaminhamento.getId() == null) {

			encaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento());
			encaminhamentoDAO.persist(encaminhamento);
		}

		if (agendamento != null) {
			if (despesa.getInSituacao().equals(0)) {

				agendamento.setInStatus(0);
			} else {

				agendamento.setInStatus(1); // Liberado
			}
		}

		agendamento.setBoDespesaGerada(true);
		agendamentoDAO.update(agendamento);

		despesa.setNrDespesa(despesaService.gerarCodigoDespesa());
		
		Unidade unidade = null;
		Funcionario funcionario = securityService.getFuncionarioLogado();
		if (funcionario != null) {
			unidade = funcionario.getUnidade();
		} else {
			unidade = agendamento.getUnidade();
		}
		despesa.setUnidade(unidade);

		parcelaDespesa.setDtInclusao(new Date());
		parcelaDespesa.setDtVencimento(despesaDTO.getDtVencimento());
		parcelaDespesa.setInFormaPagamento(despesaDTO.getInFormaPagamento());
		parcelaDespesa.setNrParcela(despesa.getNrDespesa().concat("/01"));
		parcelaDespesa.setVlParcela(despesaDTO.getVlLanctoValorDespesa());
		parcelaDespesa.setBoAutorizadaGerente(despesaDTO.getBoAutorizadoGerente());

		despesa.addParcela(parcelaDespesa);

		despesa.setInFormaPagamento(despesaDTO.getInFormaPagamento());
		despesa.setNmObservacao(despesaDTO.getNmObservacao() == null ? "" : despesaDTO.getNmObservacao());
		despesa = despesaDAO.persist(despesa);// Atualiza despesa

		// if (despesaDTO.getInFormaPagamento().equals(1)) { // Debito
		// // automatico
		//
		// TituloDa tituloDa = new TituloDa();
		//
		// if (despesa.getContratoCliente().getContrato() == null) {
		//
		// despesa.getContratoCliente().setContrato(
		// agendamento.getContrato());
		// }
		//
		// if (despesa.getContratoCliente().getCliente() == null) {
		//
		// despesa.getContratoCliente().setCliente(
		// agendamento.getCliente());
		// }
		//
		// tituloDa.setAgencia(despesa.getContratoCliente().getContrato()
		// .getAgencia());
		// tituloDa.setBanco(despesa.getContratoCliente().getContrato()
		// .getBanco());
		// tituloDa.setCliente(despesa.getContratoCliente().getCliente());
		// tituloDa.setDtEmissao(new Date());
		// tituloDa.setDtVencimento(despesaDTO.getDtVencimento());
		// tituloDa.setInMotivoRetorno(null);
		// tituloDa.setVlTitulo(despesaDTO.getVlLanctoValorDespesa());
		// tituloDa.setInStatus(0);
		// tituloDa.setInTipo(2);
		// tituloDa.setMensalidade(null);
		// tituloDa.setNmStatusBb((despesa.getContratoCliente().getContrato()
		// .getBoCadastradoBancoBrasil() != null
		// && despesa.getContratoCliente().getContrato()
		// .getBoCadastradoBancoBrasil() ? "C" : "N"));
		// tituloDa.setNrContaCorrente(despesa.getContratoCliente()
		// .getContrato().getNrConta());
		// tituloDa.setNrDigitoVerificador(despesa.getContratoCliente()
		// .getContrato().getNrDigitoVerificador());
		// tituloDa.setParcela(parcelaDespesa);
		// tituloDa.setRazao(despesa.getContratoCliente().getContrato()
		// .getRazao());
		// tituloDa.setNrTitulo(agendamentoDAO.getNumeracaoTitulo());
		//
		// // TODO - set Funcionario logado in object Titulo
		//
		// tituloDaDAO.persist(tituloDa);
		// }

		return despesa;
	}

	public Boolean isAutorizacaoGerente(Long idAgendamento, DespesaDTO despesaDTO) {

		Agendamento agendamento = getAgendamento(idAgendamento);
		Boolean isPrecisaAutorizacao = false;

		if (agendamento.getCliente().getVlLimite() != null && agendamento.getCliente().getVlLimite() > 0
				&& (despesaDTO.getBoAutorizadoGerente() == null || !despesaDTO.getBoAutorizadoGerente())) {

			if (despesaDTO.getInFormaPagamento().equals(17) || despesaDTO.getInFormaPagamento().equals(15)
					|| despesaDTO.getInFormaPagamento().equals(1)) {

				if (((despesaDTO.getVlLanctoValorDespesa() + despesaDTO.getVlLanctoDebitoUsado()) > despesaDTO
						.getVlLanctoLimite()) && !(despesaDTO.getBoAutorizadoGerente() != null)) {

					isPrecisaAutorizacao = true;
				}
			}
		}

		return isPrecisaAutorizacao;
	}

	private void validaRegistrarDespesa(DespesaDTO despesaDTO, Agendamento agendamento) {

		Properties props = PropertiesLoader.getInstance().load("message.properties");

		if (despesaDTO != null && agendamento != null && "0".equals(despesaDTO.getInParcelado())) {

			if (despesaDTO.getDtVencimento() == null) {

				throw new AgendamentoException(props.getProperty("data.vencimento"), ErrorType.ERROR);
			}
			// else {

			// if (despesaDTO.getInFormaPagamento().equals(-1)) {
			//
			// throw new AgendamentoException(
			// props.getProperty("forma.pagamento.escolhida"),
			// ErrorType.ERROR);
			// } else {
			//
			// if (despesaDTO.getVlLanctoValorDespesa() == null
			// || despesaDTO.getVlLanctoValorDespesa() == 0) {
			//
			// throw new AgendamentoException(
			// props.getProperty("informar.valor"),
			// ErrorType.ERROR);
			// } else {
			//
			// // Contra-Cheque
			// if (despesaDTO.getInFormaPagamento().equals(0)
			// && (despesaDTO.getContratoClienteList() != null && despesaDTO
			// .getContratoClienteList().size() == 0)) {
			//
			// throw new AgendamentoException(
			// props.getProperty("matricula.nao.especificada"),
			// ErrorType.ERROR);
			// } else {
			//
			// if (despesaDTO.getInFormaPagamento().equals(1)) {
			// if ((despesaDTO.getContratoList() != null && despesaDTO
			// .getContratoList().size() == 0)) {
			//
			// throw new AgendamentoException(
			// props.getProperty("dados.bancarios.nao.especificados"),
			// ErrorType.ERROR);
			// } else {
			//
			// if (agendamento.getDependente() != null) {
			//
			// Boolean permiteDebito = false;
			// for (Dependente dependente : despesaDTO
			// .getDependenteList()) {
			//
			// if (dependente.getId().equals(
			// agendamento.getDependente()
			// .getId())) {
			// permiteDebito = true;
			// }
			// }
			//
			// if (!permiteDebito) {
			//
			// throw new AgendamentoException(
			// props.getProperty("dependente.nao.autorizado.debito"),
			// ErrorType.WARNING);
			// }
			// }
			// }
			//
			// } else {
			//
			// if (despesaDTO.getInFormaPagamento().equals(4)
			// && (despesaDTO
			// .getFormaPagamentoCredito() == null)) {
			//
			// throw new AgendamentoException(
			// props.getProperty("adm.cartao.credito.nao.informada"),
			// ErrorType.ERROR);
			// } else {
			//
			// if (despesaDTO.getInFormaPagamento()
			// .equals(9)
			// && (despesaDTO
			// .getFormaPagamentoDebito() == null)) {
			//
			// throw new AgendamentoException(
			// props.getProperty("adm.cartao.debito.nao.informada"),
			// ErrorType.ERROR);
			// }
			// }
			// }
			// }
			// }
			// }
			// }
		}
	}

	public DespesaDTO inicializarDespesaPagamento(Agendamento agendamento, DespesaServico despesaServico,
			PacienteDTO<?> paciente) {

		DespesaDTO despesaDTO = new DespesaDTO();
		Despesa despesa = despesaServico.getDespesa();

		despesaDTO.setNmLanctoTitular(agendamento.getCliente().getNmCliente());

		despesaDTO.setNmLanctoPlano(
				agendamento.getCliente().getBoNaoAssociado() != null && !agendamento.getCliente().getBoNaoAssociado()
						? paciente.getContratoCliente().getPlano().getNmPlano()
						: "Particular Não Associado");

		despesaDTO
				.setNmLanctoPaciente(agendamento.getDependente() != null ? agendamento.getDependente().getNmDependente()
						: agendamento.getCliente().getNmCliente());
		despesaDTO.setNmLanctoTipoDespesa("Despesa Geral");

		despesaDTO.setVlLanctoCobertura((despesa.getVlDespesaCoberta() != null ? despesa.getVlDespesaCoberta() : 0D));
		despesaDTO.setVlLanctoDespesaTotal((despesa.getVlDespesaTotal() != null ? despesa.getVlDespesaTotal() : 0D));

		despesaDTO.setVlLanctoCreditoPessoal((agendamento.getCliente().getVlCreditoPessoalTotal() != null
				? agendamento.getCliente().getVlCreditoPessoalTotal()
				: 0D));

		despesaDTO.setVlLanctoLimite(
				(agendamento.getCliente().getVlLimite() != null ? agendamento.getCliente().getVlLimite() : 0D));
		despesaDTO.setVlLanctoSalario(
				(agendamento.getCliente().getVlSalario() != null ? agendamento.getCliente().getVlSalario() : 0D));

		despesaDTO.setVlLanctoPercentual(agendamento.getCliente().getNrPercentual() != null
				? new Double(agendamento.getCliente().getNrPercentual())
				: 0D);

		Double vlDebito = 0D;
		Double vlBoleto = 0D;

		ContratoCliente contratoCliente = paciente.getContratoCliente();

		vlDebito += agendamentoDAO.getSomaValorDebitoBoleto(1, contratoCliente.getCliente().getId().toString()) != null
				? agendamentoDAO.getSomaValorDebitoBoleto(1, contratoCliente.getCliente().getId().toString())
				: 0D;

		vlDebito += agendamentoDAO.getSomaValorDebitoBoleto(15, contratoCliente.getCliente().getId().toString()) != null
				? agendamentoDAO.getSomaValorDebitoBoleto(15, contratoCliente.getCliente().getId().toString())
				: 0D;

		vlDebito += agendamentoDAO.getSomaValorDebitoBoleto(17, contratoCliente.getCliente().getId().toString()) != null
				? agendamentoDAO.getSomaValorDebitoBoleto(17, contratoCliente.getCliente().getId().toString())
				: 0D;

		despesaDTO.setVlLanctoDebitoUsado(vlDebito);
		despesaDTO.setVlLanctoBoletoUsado(vlBoleto);

		despesaDTO.setVlLanctoDisponivel((despesaDTO.getVlLanctoLimite()
				- (despesaDTO.getVlLanctoDebitoUsado() + despesaDTO.getVlLanctoBoletoUsado())) < 0 ? 0D
						: despesaDTO.getVlLanctoLimite()
								- (despesaDTO.getVlLanctoDebitoUsado() + despesaDTO.getVlLanctoBoletoUsado()));

		despesaDTO.setVlLanctoValorDespesa(despesa.getVlDespesaAberto());
		despesaDTO.setInParcelado("0"); // Pagamento unico
		despesaDTO.setInFormaPagamento(-1);
		despesaDTO.setInIndexTab(0);
		despesaDTO.setDtVencimento(new Date());
		despesaDTO.setInQtdParcela(1);

		despesaDTO.setDependenteList(agendamentoDAO.getDependentesPorCliente(agendamento.getCliente().getId()));
		despesaDTO.setContratoList(agendamentoDAO.getContratosPorCliente(agendamento.getCliente().getId()));
		try {
			despesaDTO.setContratoClienteList(agendamentoDAO.getMatriculasPorCliente(agendamento.getCliente().getId()));
		} catch (Exception e) {
			logger.error("Nao e funcionario publico", e);
		}

		despesa.addDespesaservico(despesaServico);
		despesaDTO.setDespesa(despesa);
		// despesaDTO.setDespesaServico(despesaServico);

		return despesaDTO;
	}

	@SuppressWarnings("rawtypes")
	public List<FormaPagamentoEnum> getFormaPagamentoEnum(Long idAgendamento) {

		List<FormaPagamentoEnum> formaPagamentoList = new ArrayList<FormaPagamentoEnum>();
		Properties props = PropertiesLoader.getInstance().load("message.properties");
		Agendamento agendamento = getAgendamento(idAgendamento);
		PacienteDTO paciente = carregarPaciente(agendamento);

		if (paciente.getContratoCliente() != null) {

			if (paciente.getContratoCliente().getInTipoCliente() == 1
					|| paciente.getContratoCliente().getInTipoCliente() == 2
					|| paciente.getContratoCliente().getInTipoCliente() == 4
					|| paciente.getContratoCliente().getInTipoCliente() == 88) {

				formaPagamentoList.add(FormaPagamentoEnum.DINHEIRO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_A_VISTA);
				formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_CREDITO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_DEBITO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_PRE_DATADO_LOWER);

				if (paciente.getContratoCliente().getContrato().getBoBloqueado() == null
						|| !paciente.getContratoCliente().getContrato().getBoBloqueado()) {

					if (paciente.getContratoCliente().getContrato().getBanco() != null
							&& paciente.getContratoCliente().getContrato().getAgencia() != null
							&& paciente.getContratoCliente().getContrato().getNrDigitoVerificador() != null
							&& paciente.getContratoCliente().getContrato().getNrConta() != null) {

						formaPagamentoList.add(FormaPagamentoEnum.DEBITO_AUTOMATICO_LOWER);
					}
				}
			}

			if (paciente.getContratoCliente().getInTipoCliente() == 3) { // Empresarial

				formaPagamentoList.add(FormaPagamentoEnum.DINHEIRO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_A_VISTA);
				formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_CREDITO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CARTAO_DE_DEBITO_LOWER);
				formaPagamentoList.add(FormaPagamentoEnum.CHEQUE_PRE_DATADO_LOWER);

				if (paciente.getContratoCliente().getContrato().getBoBloqueado() == null
						|| !paciente.getContratoCliente().getContrato().getBoBloqueado()) {
					if (paciente.getContratoCliente().getContrato().getEmpresaCliente() != null) {

						Contrato contratoConveniada = new Contrato();
						contratoConveniada = agendamentoDAO.getContratoPorEmpresaCliente(
								paciente.getContratoCliente().getContrato().getEmpresaCliente().getId());

						if (contratoConveniada != null) {
							if (contratoConveniada.getNrParcelaConvenio() != null
									&& contratoConveniada.getNrParcelaConvenio() > 0)
								formaPagamentoList.add(FormaPagamentoEnum.CONVENIO_LOWER);
						} else {

							throw new AgendamentoException(props.getProperty("convenio.inativo"), ErrorType.ERROR);
						}
					}
				}

				if (paciente.getContratoCliente().getContrato().getBoBloqueado() == null
						|| !paciente.getContratoCliente().getContrato().getBoBloqueado()) {
					if (paciente.getContratoCliente().getContrato().getBanco() != null
							&& paciente.getContratoCliente().getContrato().getAgencia() != null
							&& paciente.getContratoCliente().getContrato().getNrDigitoVerificador() != null
							&& paciente.getContratoCliente().getContrato().getNrConta() != null) {

						formaPagamentoList.add(FormaPagamentoEnum.DEBITO_AUTOMATICO_LOWER);
					}
				}
			}
		}

		return formaPagamentoList;
	}

	public void removeAgendamento(Long idAgendamento) {
		Agendamento agendamento = agendamentoDAO.getAgendamento(idAgendamento);

		Despesa despesa = agendamentoDAO.getDespesaAgendamento(idAgendamento);
		if (despesa != null) {
			despesaService.removeDespesa(despesa.getId(), "EXCLUSAO DE AGENDAMENTO COM EXCLUSAO DE DESPESA EM CADEIA.");
		}

		agendamento.setDtExclusao(new Date());
		agendamento.setIdOperadorExclusao(securityService.getFuncionarioLogado().getId());
		agendamentoDAO.update(agendamento);

	}

	public List<AgendamentoViewDTO> getAgendamentosMedico(Long idProfissional, String data, Long idUnidade) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
		String dataform = "";

		try {
			dataform = df.format(df.parse(data));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
		}

		List<AgendamentoView> agendamentoView = agendamentoDAO.getAgendamentosMedico(idProfissional, dataform,
				idUnidade);
		List<AgendamentoViewDTO> listaAgendamentoViewDTO = new ArrayList<AgendamentoViewDTO>();
		if (agendamentoView != null && agendamentoView.size() > 0) {
			for (AgendamentoView ls : agendamentoView) {
				AgendamentoViewDTO agendamentoDTO = new AgendamentoViewDTO();
				agendamentoDTO.setInStatus(ls.getInStatus());
				if (ls.getNrDespesa() != null) {
					agendamentoDTO.setNrDespesa(ls.getNrDespesa());
				}
				agendamentoDTO.setIdAgendamento(ls.getId());
				agendamentoDTO.setNrAgendamento(ls.getNrAgendamento());
				agendamentoDTO.setHoraPresenca(ls.getHoraPresenca() != null ? ls.getHoraPresenca() : null);
				agendamentoDTO.setHrFicha(ls.getHrFicha() != null ? ls.getHrFicha() : null);
				agendamentoDTO.setHrAgendamento(ls.getHrAgendamento());
				agendamentoDTO.setDtAgendamentoFormatado(ls.getDtAgendamentoFormatado());
				agendamentoDTO.setNmPaciente(ls.getPaciente());
				agendamentoDTO.setNmCliente(ls.getCliente().getNmCliente()); // titular
				agendamentoDTO.setNmEspecialidade(ls.getEspecialidade().getNmEspecialidade());
				agendamentoDTO.setNmProfissional(ls.getProfissional().getNmFuncionario());
				agendamentoDTO.setNmServico(ls.getServico().getNmServico());
				agendamentoDTO.setServicoLaudavel(ls.getBoLaudo());
				agendamentoDTO.setInTipo(ls.getInTipo());
				if (ls.getContrato().getEmpresaCliente() != null
						&& ls.getContrato().getEmpresaCliente().getDtExclusao() != null) {
					agendamentoDTO.setNmConvenio("SIM");
				} else {
					agendamentoDTO.setNmConvenio("NÃO");
				}
				agendamentoDTO.setNmRetorno(ls.getRetornoAgendamentoFormatado());
				agendamentoDTO.setDtFaturamentoLaudoFormatado(ls.getDtFaturamentoLaudoFormatado());
				agendamentoDTO
						.setDtInclusaoFormatada(ls.getDtInclusaoLog() != null ? dt.format(ls.getDtInclusaoLog()) : "");
				agendamentoDTO.setDtModificacaoFormatada(
						ls.getDtAtualizacaoLog() != null ? dt.format(ls.getDtAtualizacaoLog()) : "");
				agendamentoDTO.setOperadorCadastro(ls.getOperadorCadastro() != null ? ls.getOperadorCadastro() : "");
				agendamentoDTO.setOperadorAlteracao(ls.getOperadorAlteracao() != null ? ls.getOperadorAlteracao() : "");
				if (ls.getEnvioSms() != null) {
					agendamentoDTO.setEnvioSms(ls.getEnvioSms());
				}
				if (ls.getCliente().getNrTelefone() != null) {
					agendamentoDTO.setNrTelefone(ls.getCliente().getNrTelefone());
				}
				if (ls.getCliente().getNrCelular() != null) {
					agendamentoDTO.setNrCelular(ls.getCliente().getNrCelular());
				}
				if (ls.getDependente() != null) {
					agendamentoDTO.setIdCliente(ls.getDependente().getId());
				} else {
					agendamentoDTO.setIdCliente(ls.getCliente().getId());
				}
				listaAgendamentoViewDTO.add(agendamentoDTO);
			}
		}
		// if(listaAgendamentoViewDTO.size()>0)
		// return contadorStatusAgendamento(listaAgendamentoViewDTO);
		return listaAgendamentoViewDTO;
	}

	public List<AtendimentomedicoView> getAtendimentoMedico(Long idProfissional, Long idEspecialidade) {

		return agendamentoDAO.getAtendimentoMedico(idProfissional, idEspecialidade);

	}

	public List<AtendimentomedicoView> getAtendimentoEnfermagem(List<Long> idEspecialidade, Long idUnidade,
			List<Long> idProfissional) {

		return agendamentoDAO.getAtendimentoEnfermagem(idEspecialidade, idUnidade, idProfissional);

	}

	public Agendamento updateStatusTriagem(Long idAgendamento, Integer prioridade, Integer statusPrioridade, String obsUrgencia) {

		Agendamento agendamento = agendamentoDAO.searchByKey(Agendamento.class, idAgendamento);
		Agendamento agendamentoAtualizado = null;
		agendamento.setBoStatusTriagem(new Boolean(true));
		agendamento.setInTipoPrioridade(prioridade);
		agendamento.setInStatusPrioridade(statusPrioridade);
		agendamento.setObsUrgencia(obsUrgencia);
		
		try {
			agendamentoAtualizado = agendamentoDAO.update(agendamento);
			registraOrdem(agendamentoAtualizado, prioridade, statusPrioridade, obsUrgencia);
		} catch (Exception e) {
			throw new ObjectNotFoundException("Erro ao atualizar o status da triagem do agendamento");
		}

		return agendamentoAtualizado;
	}

	public String getDiasBloqueadosTeleamarketing(Long idPaciente, String tipoPaciente) {
		SimpleDateFormat angularFormatter = new SimpleDateFormat("dd/MM/yyyy");

		Date result = agendamentoDAO.getUltimoAgendamentoFaltoso(idPaciente, tipoPaciente);

		if (result != null)
			return angularFormatter.format(result);
		else
			return null;

	}

	/**
	 * Define todas as parcelas de uma despesa como pagas. em 2017-10-05
	 * 
	 * @author Patrick Lima
	 * 
	 * @param despesa
	 *            Objeto despesa a ser quitada
	 * @param formaPagamento
	 *            forma de pagamento usada no pagamento
	 * @return Objeto despesa atualizado
	 */
	private Despesa quitarDespesa(Despesa despesa, Integer formaPagamento) {
		if (despesa.getInSituacao() == 0 && formaPagamento != null) {
			for (Parcela p : despesa.getParcelas()) {
				p.setDtPagamento(new Date());
				p.setVlPago(despesa.getVlDespesaTotal());
				p.setInFormaPagamento(formaPagamento); // cartão de crédito
				p.setFormaPagamentoEfetuada(FormaPagamentoEnum.CARTAO_DE_CREDITO.getDescription());
			}
			despesa.setInFormaPagamento(formaPagamento);
			despesa.setInSituacao(1);
			return despesaDAO.update(despesa);
		}
		return null;
	}

	/**
	 * Gera boletos para as parcelas de uma despesa no PJBank em 2017-10-05
	 * 
	 * @author Patrick Lima
	 * 
	 * @param despesa
	 *            Despesa cujos boletos serão gerados
	 * @return bytes do pdf a ser impresso
	 */
	private byte[] gerarBoletoDespesa(Despesa despesa) {
		if (despesa.getInSituacao().equals(0)) {
			for (Parcela parcela : despesa.getParcelas()) {
				try {
					boletoServicePJB.registrarBoleto(parcela, despesa.getAgendamento().getCliente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return boletoServicePJB.imprimirBoleto(despesa.getParcelas());
		}
		return null;
	}

	/**
	 * Paga agendamento por cartão de crédito ou gera boleto vinculados a despesa do
	 * agendamento
	 * 
	 * @author Patrick Lima em 2017-10-27
	 * 
	 * @param data
	 *            Objeto de pagamento de agendamento
	 * @param source
	 *            forma de acesso (site ou app)
	 * @return
	 */
	public AssinaturaRecorrenteResultDTO pagarAgendamento(PagamentoAgendamentoDTO data, String source) {
		Agendamento agendamento = agendamentoDAO.getAgendamento(data.getIdAgendamento());

		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		if (agendamento != null) {
			Despesa despesa = agendamentoDAO.getDespesaAgendamento(agendamento.getId());
			if (despesa == null) { // provavelmente nunca será usado
				DespesaDTO despesaDTO = this.validaGerarDespesa(agendamento);
				despesa = this.registrarDespesa(agendamento.getId(), despesaDTO);
			}

			if (data.getFormaPagamento().equals(1)) { // boleto bancario
				byte[] boletoData = this.gerarBoletoDespesa(despesa);
				if (boletoData == null) {
					result.setStatus("fail");
					result.setMessage("Não foi possível gerar boleto no momento");
				} else {
					result.setStatus("success");
					Parcela parecela = despesa.getParcelas().get(0);
					List<BoletoBancario> boletos = boletoServicePJB.getBoletosBancarios(parecela);
					if (source != null && source.equals("mobile_app")) {
						if (boletos != null && !boletos.isEmpty()) {
							BoletoBancario boleto = boletos.get(boletos.size() - 1);
							result.setData(boleto.getNrBarra());

							// enviar boleto por e-mail
							this.sendEmailBoletoPagamentoAgendamento(despesa, boleto);
						}
					} else {
						result.setData(boletoData);
						if (boletos != null && !boletos.isEmpty()) {
							BoletoBancario boleto = boletos.get(boletos.size() - 1);
							// enviar boleto por e-mail
							this.sendEmailBoletoPagamentoAgendamento(despesa, boleto);
						}
					}
				}
			} else if (data.getFormaPagamento().equals(2)) {
				data.setCartaoNumero(Utils.removeSpecialChars(data.getCartaoNumero()));
				data.setValor(new BigDecimal(despesa.getVlDespesaTotal()));
				result = vendasService.pagamentoAgendamentoCartao(data, source);
				if (result.getStatus().equals("success")) {
					despesa = quitarDespesa(despesa, 30);
					agendamento.setInStatus(1);
					agendamentoDAO.update(agendamento);
				}
			}
			return result;
		}
		result.setStatus("fail");
		result.setMessage("Agendamento referido é inexistente");
		return result;
	}
	
	public AgendamentoView getAgendamentoView(Long idAgendamento) {
		return agendamentoDAO.getAgendamentoViewById(idAgendamento);
	}
	
	public AppResponse getHistoricoAgendamentos(Integer offsetFact, Integer tipoListagem) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if (user != null) {
			PagedListDTO<HistoricoAgendamentosCliente> result = agendamentoDAO
					.getHistoricoAgendamentos(user.getIdUsuario(), user.getTipoUsuario(), offsetFact, tipoListagem);
			if (result == null) {
				ErrorResponse r = new ErrorResponse(-1, "Houve um erro ao processar solicitação");
				return r;
			}
			return new SuccessResponse(200, result);
		}
		return new ErrorResponse(403, "Usuário não logado ou autorizado");
	}

	public AppResponse persistAvaliacaoAtendimento(AvaliacaoAgendamento avaliacao) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if (user != null) {
			try {
				Agendamento ag = agendamentoDAO.getAgendamento(avaliacao.getIdAgendamento());
				Long idClienteAgendamento = null;
				int tipoUsuario = user.getTipoUsuario().intValue();
				if (tipoUsuario == TipoUsuario.CLIENTE.valorTipo) {
					idClienteAgendamento = ag.getCliente().getId();
				} else if (tipoUsuario == TipoUsuario.DEPENDENTE.valorTipo) {
					idClienteAgendamento = ag.getDependente().getId();
				}
				if (idClienteAgendamento == null || !idClienteAgendamento.equals(user.getIdUsuario())) {
					return new ErrorResponse(403, "Agendamento não pertence a esse usuário");
				}
				if (avaliacao.getNota() < 0) {
					avaliacao.setNota(0);
				} else if (avaliacao.getNota() > 5) {
					avaliacao.setNota(5);
				}
				avaliacao.setNota(avaliacao.getNota());
				avaliacao.setDtAvaliacao(new Date());
				avaliacaoAgendamentoDAO.persist(avaliacao);
				return new SuccessResponse(200, null);
			} catch (Exception e) {
			}
			return new ErrorResponse(-1, "Houve um erro ao processar solicitação");
		}
		return new ErrorResponse(403, "Usuario não logado ou autorizado");
	}

	public AppResponse getUltimoAgendamentoFaltoso() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if (user == null) {
			return new ErrorResponse(403, "Usuario não logado ou autorizado");
		}
		String tipoPaciente;
		if (user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			tipoPaciente = "C";
		} else {
			tipoPaciente = "D";
		}
		String response = getDiasBloqueadosTeleamarketing(user.getIdUsuario(), tipoPaciente);
		return new SuccessResponse(200, response);
	}

	public AppResponse createAgendamentoFromApp(AgendamentoWebDTO agendamento) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if (user == null) {
			return new ErrorResponse(403, "Usuario não logado ou autorizado");
		}
		Despesa despesa = this.createAgendamento(agendamento, "mobile_app");
		if (despesa != null) {
			this.setConsultorioAgendamento(despesa.getAgendamento());

			DespesaSiteDTO despesaSite = null;
			String nomePlano = despesa.getAgendamento().getContrato().getPlano() != null
					? despesa.getAgendamento().getContrato().getPlano().getNmPlano()
					: "Particular";

			despesaSite = new DespesaSiteDTO(despesa.getAgendamento().getId(), despesa.getAgendamento().getIsConvenio(),
					despesa.getVlDespesaTotal(), despesa.getAgendamento().getInStatus(), nomePlano,
					despesa.getAgendamento().getContrato().getNrContrato());

			try {
				this.sendEmailConsultaMarcada(despesa, agendamento.getHorarioPrevisto());
			} catch (Exception e) {
				System.out.println("Erro ao enviar email");
			}
			return new SuccessResponse(200, despesaSite);
		}
		return new FailResponse();
	}

	public Boolean sendNotificacaoAvaliacao(Agendamento agendamento) {
		List<String> regIds = null;
		String clienteName;
		if (agendamento.getDependente() != null) {
			regIds = usuarioLoginDAO.getUsuarioRegistrationIds(agendamento.getDependente().getId(), 2);
			clienteName = agendamento.getDependente().getNmDependente().split(" ")[0];
		} else {
			regIds = usuarioLoginDAO.getUsuarioRegistrationIds(agendamento.getCliente().getId(), 1);
			clienteName = agendamento.getCliente().getNmCliente().split(" ")[0];
		}
		if (regIds == null || regIds.isEmpty()) {
			return null;
		}
		PushNotificationService pushService = new PushNotificationService();
		String title = "Agradecemos a presenca " + clienteName;
		String message = "Avalie agora seu atendimento tocando aqui.";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("idAgendamento", agendamento.getId());
		data.put("nomeCliente", clienteName);
		data.put("idProfissional", agendamento.getProfissional().getId());
		data.put("nmProfissional", agendamento.getProfissional().getNmFuncionario());
		data.put("nmEspecialidade", agendamento.getEspecialidade().getNmEspecialidade());
		data.put("showRateAgendamento", true);
		return pushService.sendNotification(regIds, title, message, "1", data, null, null);
	}

	private String getMensagemformatada(String message, Object... args) {
		return String.format(message, args);
	}

	@Schedule(dayOfMonth = "*", dayOfWeek = "*", hour = "4", minute = "10", persistent = false)
	public List<AgendamentoLembreteDTO> cronLembreteAgendamento() {
		String AGENDAMENTO_A_REALIZAR_SMS = "Olá %s, você possui uma consulta de %s em " + "%s, na unidade %s.";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		int[] daysAhead = { 0, 3 };

		List<AgendamentoLembreteDTO> agendamentos = null;
		for (int i = 0; i < daysAhead.length; i++) {
			agendamentos = agendamentoDAO.getAgendamentosParaLembrete(daysAhead[i]);
			if (agendamentos == null || agendamentos.isEmpty()) {
				System.out.println("VAZIO !!");
				continue;
			}

			for (AgendamentoLembreteDTO agendamento : agendamentos) {
				String title = EMPRESA_CONSUTLA + " Lembrete";
				String message = getMensagemformatada(AGENDAMENTO_A_REALIZAR_SMS,
						Utils.getPrimeiroNomeCliente(agendamento.getNmPaciente()), agendamento.getNmEspecialidade(),
						sdf.format(agendamento.getDtAgendamento()), agendamento.getNmUnidade());
				List<String> regIds = Arrays.asList(agendamento.getNotificationRegId());

				Map<String, Object> notificationData = new HashMap<String, Object>();
				notificationData.put("showViewDetailLembrete", true);
				notificationData.put("idAgendamento", agendamento.getIdAgendamento());
				PushNotificationService pushService = new PushNotificationService();
				pushService.sendNotification(regIds, title, message, "1", notificationData, null, null);
			}
		}
		return agendamentos;
	}

	@Schedule(dayOfMonth = "*", dayOfWeek = "*", hour = "1", minute = "10", persistent = false)
	public List<Agendamento> deleteAgendamentosFromSite() {
		List<Agendamento> agendamentos = agendamentoDAO.getAgendamentosFromSiteExcluir();
		if (agendamentos != null && !agendamentos.isEmpty()) {
			for (Agendamento agendamento : agendamentos) {
				Despesa despesa = agendamentoDAO.getDespesaAgendamento(agendamento.getId());
				if (despesa != null) {
					despesaService.removeDespesa(despesa.getId(),
							"EXCLUSAO DE AGENDAMENTO COM EXCLUSAO DE DESPESA EM CADEIA.");
				}
				agendamento.setDtExclusao(new Date());
				agendamento.setIdOperadorExclusao(2891L);
				agendamentoDAO.update(agendamento);
			}
		}
		return agendamentos;
	}

	public Integer countPacienteAgendamentosConsulta(Long idPaciente, Integer tipoPaciente, Date dia,
			Long idEspecialidade, Long idServico) {
		return agendamentoDAO.countPacienteAgendamentosConsulta(idPaciente, tipoPaciente, dia, idEspecialidade,
				idServico);
	}

	public Boolean sendEmail(EmailMessage emailMessage) {
		try {
	        InternetAddress[] lAddress = {
	        	new InternetAddress("mateus.mediclab@gmail.com"),
				new InternetAddress("odontomed.design@gmail.com"),
				new InternetAddress("ligacao.consultas@gmail.com")
			};
	        emailMessage.setReplyTo(emailMessage.getFrom());
	        EmailUtil.sendEmailGeneric(lAddress, emailMessage);
	        return true;
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public List<StatusAgendamento> getStatusAgendamento(){
		return StatusAgendamento.getStatus();
	}

	public GenericPaginateDTO getRelatorioAgendamentosMedico(String dataInicio, String dataFim, Integer statusAg,
			List<String> idOperadorCadastro, Long inGrupoFuncionario, Long idUnidade, Long idEspecialidade, Long idProfissional,int offset, int limit) {
		if(limit > 50){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		String ids = converterListaEmString(idOperadorCadastro);
		
		Long total = agendamentoDAO.getTotalRelatorioAgendamentosMedico(dataInicio, dataFim, statusAg, ids, inGrupoFuncionario, idUnidade, idEspecialidade, idProfissional);
		List<?> listAgendamentosMedico = agendamentoDAO.getRelatorioAgendamentosMedico(dataInicio, dataFim, statusAg, ids, inGrupoFuncionario, idUnidade, idEspecialidade, idProfissional, offset, limit);
		
		GenericPaginateDTO agendamentos = new GenericPaginateDTO();
		agendamentos.setOffset(offset);
		agendamentos.setLimit(limit);
		agendamentos.setTotal(total); 
		agendamentos.setData(listAgendamentosMedico);
		agendamentos.prepare();
		return agendamentos;
	}
	
	public String converterListaEmString(List<String> lista) {
		StringBuilder str = new StringBuilder();
		for (String elemento : lista) {
			str.append(elemento).append(",");
		}
		
		if(str.toString().isEmpty()) {
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
	
	}
	
	public Boolean registerContrachequeInfo(Long idAgendamento, ContrachequeInfoDTO info) {
		Agendamento ag = agendamentoDAO.getAgendamento(idAgendamento);
		if(ag != null) {
			String bodyMessage = getMensagemEmailContrachequeInfo(ag, info);
			try {
				InternetAddress[] listTo = {
						new InternetAddress("administracao01@medic-sistema.com.br"),
						new InternetAddress("gerencia-administrativo@medic-sistema.com.br"),
						new InternetAddress("adrianabandeira.adm@gmail.com"),
						new InternetAddress("fernando.maisconsulta@gmail.com")
				};
				InternetAddress[] listCCo = {
						new InternetAddress("sistemas@medic-sistema.com.br"),
						new InternetAddress("rodrigodiniz1@gmail.com")
				};
				String subj = ag.getCliente().getNmCliente() + " C.C.";
				boolean r = EmailUtil.sendEmailGeneric(listTo, null, listCCo, subj, bodyMessage);
				return r;
			}catch (AddressException e){
			}
		}
		return false;
	}
}
