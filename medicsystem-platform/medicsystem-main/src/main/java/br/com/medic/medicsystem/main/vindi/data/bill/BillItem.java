package br.com.medic.medicsystem.main.vindi.data.bill;

import br.com.medic.medicsystem.main.vindi.data.product.PricingSchema;
import br.com.medic.medicsystem.main.vindi.data.summary.Discount;
import br.com.medic.medicsystem.main.vindi.data.summary.Product;
import br.com.medic.medicsystem.main.vindi.data.summary.ProductItem;

public class BillItem {
	private Integer id;
	private Number amount;
	private Integer quantity;
	private Integer pricing_range_id;
	private String description;
	private PricingSchema pricing_schema;
	private Product product;
	private ProductItem product_item;
	private Discount discount;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPricing_range_id() {
		return pricing_range_id;
	}
	public void setPricing_range_id(Integer pricing_range_id) {
		this.pricing_range_id = pricing_range_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public PricingSchema getPricing_schema() {
		return pricing_schema;
	}
	public void setPricing_schema(PricingSchema pricing_schema) {
		this.pricing_schema = pricing_schema;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public ProductItem getProduct_item() {
		return product_item;
	}
	public void setProduct_item(ProductItem product_item) {
		this.product_item = product_item;
	}
	public Discount getDiscount() {
		return discount;
	}
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}
}
