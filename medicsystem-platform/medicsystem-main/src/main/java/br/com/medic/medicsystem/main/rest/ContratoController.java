package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.main.service.ContratoService;
import br.com.medic.medicsystem.persistence.dto.ContratoProducaoVendaDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoContratoEnum;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.views.ViewRenovacaoContrato;

@Path("/contratos")
@RequestScoped
public class ContratoController extends BaseController {

	@Inject
	private ContratoService contratoService;
			
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createContrato(@Valid Contrato contrato, 
			@QueryParam("idCliente") Long idCliente, 
			@QueryParam("idDependentes") List<Long> listDependentes,
			@QueryParam("idAssinaturaRecorrente") Long idAssinaturaRecorrente) {
		
		Contrato salvo = contratoService.saveContrato(contrato, idCliente, listDependentes, idAssinaturaRecorrente);
		
		if(idAssinaturaRecorrente != null)
			contratoService.atualizarContratoRecorrentebyIdsubscription(idAssinaturaRecorrente);
		
		return Response.created(URI.create("/contratos/" + salvo.getId())).build();
	}

	@GET
	@Path("/boletopdf")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/pdf")
	public Response getImpressaoBoletoContratoSLPdf(@QueryParam("idContrato") Long idContrato){
		
		if (idContrato != null) {
			byte[] pdf = contratoService.getImpressaoBoletoContrato(idContrato);
			
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/contratopdf")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/pdf")
	public Response getImpressaoContratoPdf(@QueryParam("idCliente") Long idCliente,
			@QueryParam("idContrato") Long idContrato,
			@QueryParam("idDependentes") List<Long> idDependentes,
			@QueryParam("tipoPlano") Integer tipoPlano){
		
		if (idCliente != null && idContrato != null) {
			byte[] pdf = contratoService.getImpressaoContrato(idCliente, idContrato, idDependentes, tipoPlano);
			
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
	@GET
	@Path("/contratopv")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContratoProducaoVendaDTO> getContratoProdVenda(
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim){
		
		return contratoService.getContratoProdVenda(dtInicio, dtFim);
	}
	
	@GET
	@Path("/contratospvfuncionarios")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContratoProducaoVendaDTO> getContratoProdVendaFuncionarios(
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim,
			@QueryParam("idUnidade") Integer idUnidade){
			
		return contratoService.getContratoProdVendaFuncionarios(dtInicio, dtFim, idUnidade);
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Contrato getContrato(@PathParam("id") Long id) {
		return contratoService.getContrato(id);

	}
		
	@GET
	@Path("/{idcontrato:[0-9][0-9]*}/clientes/{idcliente:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoCliente getContratoCliente(
	        @PathParam("idcontrato") Long idcontrato,
	        @PathParam("idcliente") Long idcliente) {
		return contratoService.getContratoCliente(idcontrato, idcliente);

	}

	@GET
	@Path("/empresaCliente/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Contrato getContratoEmpresaClienteById(@PathParam("id") Long idEmpresaCliente) {
		return contratoService.getContratoEmpresaClienteById(idEmpresaCliente);

	}
	
	@GET
	@Path("/{idcontrato:[0-9][0-9]*}/clientes/{idcliente:[0-9][0-9]*}/dependentes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContratoDependente> getContratosDependentes(
	        @PathParam("idcontrato") Long idcontrato,
	        @PathParam("idcliente") Long idcliente) {
		return contratoService.getContratosDependentes(idcontrato, idcliente);

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/formasPagamentoLiberados")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FormaPagamentoEnum> getSomaValorDebito(@PathParam("id") Long idContrato) {

		if (idContrato != null) {

			return contratoService.getFormasPagamentoLiberado(idContrato);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}
	
	/*relatorio de renovacao automatica*/
	@GET
	@Path("/renovacao/formapagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FormaPagamentoContratoEnum> getFormaPagamentoRenovacao(){
		return contratoService.getFormaPagamentoRenovacao();
	}
	
	@GET
	@Path("/relatoriorenovacao")
	@Produces(MediaType.APPLICATION_JSON)
	public GenericPaginateDTO getRenovacoes(
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim,
			@QueryParam("inFormaPagamento") Long inFormaPagamento,
			@QueryParam("idPlano") Long idPlano,
			@QueryParam("idEmpresaGrupo") Long idEmpresaGrupo,
			@QueryParam("idFuncionario") Long idFuncionario,
			@QueryParam("offset") @DefaultValue("1") int offset,
			@QueryParam("limit") @DefaultValue("50") int limit) {
		return contratoService.getRenovacoes(dtInicio, dtFim, inFormaPagamento,
				idPlano, idEmpresaGrupo, idFuncionario, offset, limit);
	}
	
	@GET
	@Path("/renovacao/funcionarios")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuncionarioDTO> getFuncionariosRenovacao(
			@QueryParam("dtInicio") String dtIncio,
			@QueryParam("dtFim") String dtFim,
			@QueryParam("inFormaPagamento") Long inFormaPagamento,
			@QueryParam("idPlano") Long idPlano,
			@QueryParam("idEmpresaGrupo") Long idEmpresaGrupo) {
		if(dtIncio != null && dtFim != null) {
			return contratoService.getFuncionariosRenovacao(dtIncio, dtFim, inFormaPagamento, idPlano, idEmpresaGrupo);
		}
		return null;
	}
	
	@GET
	@Path("/renovacoes/recorrentes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ViewRenovacaoContrato> renovarContratos(@QueryParam("fimContrato") String dtFimContrato) {
		return contratoService.renovarContratosRecorrentes(dtFimContrato);
	}
}
