package br.com.medic.medicsystem.main.vindi.data.summary;

public class Period {
	private Integer id;
	private String billing_at;
	private Integer cycle;
	private String start_at;
	private String end_at;
	private Integer duration;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBilling_at() {
		return billing_at;
	}
	public void setBilling_at(String billing_at) {
		this.billing_at = billing_at;
	}
	public Integer getCycle() {
		return cycle;
	}
	public void setCycle(Integer cycle) {
		this.cycle = cycle;
	}
	public String getStart_at() {
		return start_at;
	}
	public void setStart_at(String start_at) {
		this.start_at = start_at;
	}
	public String getEnd_at() {
		return end_at;
	}
	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
}
