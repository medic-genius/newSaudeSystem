package br.com.medic.medicsystem.main.util;

import java.util.ArrayList;

public abstract class SearchContainer<K, V> {

	protected ArrayList<V> coll;

	public SearchContainer(ArrayList<V> collection) {
		this.coll = collection;
	}

	public abstract boolean contains(K key);

}
