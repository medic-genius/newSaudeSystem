package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.GrupoOrgaoDAO;
import br.com.medic.medicsystem.persistence.model.GrupoOrgao;

@Stateless
public class GrupoOrgaoService {
	
	@Inject
	@Named("grupoOrgao-dao")
	private GrupoOrgaoDAO grupoOrgaoDAO;

	public List<GrupoOrgao> getAllGrupoOrgao() {
		
		return grupoOrgaoDAO.getAllGrupoOrgao();
		
	}

}
