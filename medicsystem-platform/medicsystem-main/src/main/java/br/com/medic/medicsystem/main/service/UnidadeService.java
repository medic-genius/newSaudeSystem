package br.com.medic.medicsystem.main.service;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.UnidadeDAO;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Unidade;

@Stateless
public class UnidadeService {

	@Inject
	private Logger logger;

	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;

	public List<Unidade> getUnidades() {
		logger.debug("Obtendo unidades...");

		return unidadeDAO.getUnidades();
	}

	public Collection<Especialidade> getEspecialidades(Long idUnidade) {
	    return unidadeDAO.getEspecialidades(idUnidade);
    }
	
	public Unidade getUnidade(Long id) {
		Unidade unidade = unidadeDAO.searchByKey(Unidade.class, id);
		return unidade;
	}

}
