package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.BancarioService;
import br.com.medic.medicsystem.persistence.model.Agencia;
import br.com.medic.medicsystem.persistence.model.Banco;

@Path("/bancos")
@RequestScoped
public class BancoController extends BaseController {

	@Inject
	private BancarioService bancarioService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createBanco(Banco banco) {
		
		Banco salvo = bancarioService.saveBanco(banco);
		
		return Response.created(URI.create("/bancos/"+ salvo.getId())).build();
		
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Banco> getAllBancos() {

		return bancarioService.getAllBancos();
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/agencias")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Agencia> getAgencias(@PathParam("id") Long idBanco) {

		List<Agencia> list = bancarioService
		        .getAgencias(idBanco);
		if (list.isEmpty() || list == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return list;
		}
	}

}