package br.com.medic.medicsystem.main.vindi.service;

import java.util.List;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.subscription.Subscription;
import br.com.medic.medicsystem.main.vindi.data.subscription.SubscriptionResponse;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.response.SubscriptionParser;
import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;

public class SubscriptionService extends BaseHttpService{
	private final String BASE_URL = "/api/v1/subscriptions";
	private SubscriptionParser parser = new SubscriptionParser();
	
	public SubscriptionService() {
		super();
		setUseSSL(true);
	}
	
	public List<Subscription> getSubscriptions() {
		String url = BASE_URL;
		HttpGet get = new HttpGet();
		get.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String response = sendRequest(url, get);
			return parser.parseResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public SubscriptionResponse createSubscription(
			br.com.medic.medicsystem.main.vindi.data.parameters.create.Subscription subscription) throws VindiException {
		String url = BASE_URL;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		try {
			jsonStr = mapper.writeValueAsString(subscription);
			post.setEntity(new StringEntity(jsonStr));
			
			String response = sendRequest(url, post); 
			return parser.parseCreationResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Subscription deleteSubscription(Integer subscriptionId) throws VindiException {
		String url = BASE_URL + "/" + subscriptionId;
		HttpDelete delete = new HttpDelete();
		delete.addHeader("Content-Type", "application/json");
		delete.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {			
			String response = sendRequest(url, delete);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Subscription getSubscription(Integer idSubscription) throws VindiException {
		String url = BASE_URL + "/" + idSubscription;
		HttpGet get = new HttpGet();
		get.addHeader("Content-Type", "application/json");
		get.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {			
			String response = sendRequest(url, get);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
