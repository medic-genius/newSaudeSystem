package br.com.medic.medicsystem.main.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.WebApplicationException;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import br.com.medic.medicsystem.main.jasper.JRDataSourceAgendamento;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaDAO;
import br.com.medic.medicsystem.persistence.dao.EstatisticaDAO;
import br.com.medic.medicsystem.persistence.dao.UnidadeDAO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoPrevisaoDTO;
import br.com.medic.medicsystem.persistence.dto.AprazamentoDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResuDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResuPagamentoDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalResultDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalUnidadeResultDTO;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissionalResult;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;


@Stateless
public class EstatisticaService {
	
	private static final String PATH_TEMP_FILES = "/root/systemtemp/";
//	private static final String PATH_TEMP_FILES = "D:/systemtemp/";
	
	@Inject
	@Named("estatistica-dao")
	private EstatisticaDAO estatisticaDAO;
	
	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("despesa-dao")
	private DespesaDAO despesaDAO;
	
				
	/**
	 * Estatistica de atendimento por profissional.
	 * @param idProfissional
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 */
	public AtendimentoProfissionalResultDTO getRelarioAtendimentoMedico(Long idProfissional, String dataInicio, String dataFim){
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date dateInicio = null;
		Date dateFim = null;
		
		
		try {
			dateInicio = format.parse(dataInicio);
			dateFim = format.parse(dataFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
		
		
		
		 List<AtendimentoProfissionalResult> result = estatisticaDAO.getRelarioAtendimentoMedico(idProfissional, dateInicio, dateFim);
		 AtendimentoProfissionalResultDTO resultDTO = new AtendimentoProfissionalResultDTO();
		 
		 
		 if(result != null && result.size() > 0){
			 Integer qtdAtendido = new Integer(0);
			 Integer qtdAtendimentos = new Integer(0);
			 Integer qtdAtendimentosContratados = new Integer(0);
			 Integer perfClinica = new Integer(0);
			 Integer perfProfissional = new Integer(0);
			 Integer indiceAgendNaoRealizados = new Integer(0);
			 Integer qtdAgendamentos = new Integer(0);
			 
			 
			 for (AtendimentoProfissionalResult apr : result) {
				 
				 qtdAtendido += apr.getQtdAtendido() != null ? apr.getQtdAtendido() : new Integer(0);
				 qtdAtendimentos += apr.getQtdAtendimento() != null ? apr.getQtdAtendimento() : new Integer(0);
				 qtdAtendimentosContratados += apr.getQtdAtendimentoContratado() != null ? apr.getQtdAtendimentoContratado() : new Integer(0);
				 qtdAgendamentos += apr.getQtdAgendamento() != null ? apr.getQtdAgendamento() : new Integer(0);
				 
			 }
			 
			 			 			 
			 if(qtdAtendimentos == null || qtdAtendimentos == 0)
				 perfClinica = new Integer(100);							 
			 else
				 perfClinica = new Integer (new Double((new Double(qtdAgendamentos) / new Double(qtdAtendimentos)) * 100).intValue());
			 
			 if(qtdAtendimentosContratados == null || qtdAtendimentosContratados == 0)
				 perfProfissional = new Integer(100);	
			 else {				 
				 perfProfissional = new Integer (new Double((new Double(qtdAtendido) / new Double(qtdAtendimentosContratados)) * 100).intValue());
			 }
			 			 
			 indiceAgendNaoRealizados = new Integer( 100 - new Double((new Double(qtdAtendido) / new Double(qtdAgendamentos)) * 100).intValue() );
			 			 
			 
			 resultDTO.setAtendimentoProfissionalResults(result);
			 resultDTO.setPerformanceClinica(perfClinica);
			 resultDTO.setPerformanceProfissional(perfProfissional);
			 resultDTO.setIndiceAgendNaoRealizados(indiceAgendNaoRealizados);
			 
			 
		 }
		 
		 return resultDTO;
	}
	
	/**
	 * Estatistica de atendimento por unidade.
	 * @param idUnidade
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 */
	public AtendimentoProfissionalResultDTO getRelarioAtendimentoProfissionalUnidade(Long idUnidade, String dataInicio, String dataFim){
		
		AtendimentoProfissionalResultDTO resultDTO = new AtendimentoProfissionalResultDTO();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date dateInicio = null;
		Date dateFim = null;
						
		try {
			dateInicio = format.parse(dataInicio);
			dateFim = format.parse(dataFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
				
		List<AtendimentoProfissionalUnidadeResultDTO> result = estatisticaDAO.getRelarioAtendimentoProfissionalUnidade(idUnidade, dateInicio, dateFim);
						 
		if(result != null && result.size() > 0){
			
			Integer qtdAtendido = new Integer(0);
			Integer qtdAtendimentos = new Integer(0);
			Integer qtdAtendimentosContratados = new Integer(0);
			Integer qtdAgendamentos = new Integer(0);
			Integer perfClinica = new Integer(0);
			Integer perfProfissional = new Integer(0);		
			Integer indiceAgendNaoRealizados = new Integer(0);
			 
			for (AtendimentoProfissionalUnidadeResultDTO apr : result) {
				 
				qtdAtendido += apr.getQtdAtendido() != null ? apr.getQtdAtendido() : new Integer(0);
				qtdAtendimentos += apr.getQtdAtendimento() != null ? apr.getQtdAtendimento() : new Integer(0);
				qtdAtendimentosContratados += apr.getQtdAtendimentoContratado() != null ? apr.getQtdAtendimentoContratado() : new Integer(0);
				qtdAgendamentos += apr.getQtdAgendamento() != null ? apr.getQtdAgendamento() : new Integer(0);
				
			}
			
			 			 			 
			if(qtdAtendimentos == null || qtdAtendimentos == 0)
				perfClinica = new Integer(100);							 
			else
				perfClinica = new Integer (new Double((new Double(qtdAgendamentos) / new Double(qtdAtendimentos)) * 100).intValue());
			 
			if(qtdAtendimentosContratados == null || qtdAtendimentosContratados == 0)
				perfProfissional = new Integer(100);	
			else {				 
				perfProfissional = new Integer (new Double((new Double(qtdAtendido) / new Double(qtdAtendimentosContratados)) * 100).intValue());
			}
			
			indiceAgendNaoRealizados = new Integer( 100 - new Double((new Double(qtdAtendido) / new Double(qtdAgendamentos)) * 100).intValue() );
			
			Unidade unidade = unidadeDAO.searchByKey(Unidade.class, idUnidade);
			
			resultDTO.setAtendimentoProfissionalResults(null);
			resultDTO.setAtendimentoProfissionalUnidadeResult(result);
			resultDTO.setPerformanceClinica(perfClinica);
			resultDTO.setPerformanceProfissional(perfProfissional);
			resultDTO.setIndiceAgendNaoRealizados(indiceAgendNaoRealizados);
			resultDTO.setNmUnidade(unidade.getNmUnidade());
			 
		}
		 
		return resultDTO;
	}
	
	public AtendimentoProfissionalResuPagamentoDTO getRelarioAtendimentoMedicoPagamento(String dataInicio, String dataFim){
		
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date dateInicio = null;
		Date dateFim = null;
		
		
		try {
			dateInicio = format.parse(dataInicio);
			dateFim = format.parse(dataFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
		
		AtendimentoProfissionalResuPagamentoDTO ListDTO = new AtendimentoProfissionalResuPagamentoDTO();
		List<AtendimentoProfissionalResuDTO> result = estatisticaDAO.getRelarioAtendimentoMedicoPagamento(dateInicio, dateFim);
		
		
		if(result != null && result.size() >0){
			Double totalValorComissionado = new Double(0);
			
			for (AtendimentoProfissionalResuDTO atpDTO : result) {
				totalValorComissionado += atpDTO.getTotalVlComissaoConsulta() != null ? new BigDecimal(atpDTO.getTotalVlComissaoConsulta()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue() : new Double(0);
				totalValorComissionado += atpDTO.getTotalVlComissaoProcedimento() != null ? new BigDecimal(atpDTO.getTotalVlComissaoProcedimento()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue() : new Double(0);
				totalValorComissionado += atpDTO.getSalario() != null ? new BigDecimal(atpDTO.getSalario()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue() : new Double(0);

			}
			ListDTO.setAtendimentoProfissionalResuDTO(result);
			ListDTO.setSomatotalVlComissaoSalario(totalValorComissionado);
			
		}
		
		return ListDTO;
		
	}
	
	public List<AgendamentoPrevisaoDTO> getPrevisaoAgendamento(Long idUnidade){
		return estatisticaDAO.getPrevisaoAgendamento(idUnidade);
	}
	
	public void enviarAprazamentobyEmail(List<String>emails){
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String table = "";
		
		List<AprazamentoDTO> listAprazamento = estatisticaDAO.getAprazamento(120);
		
		for(AprazamentoDTO aprazamento: listAprazamento){
			
			table +=										
			"<tr>"
			+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + aprazamento.getNmEspecialidade() + "</td>"
			+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + format.format(aprazamento.getDtDisponivel()) + "</td>"
			+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + aprazamento.getNmUnidade() + "</td>"
			+ "</tr>";
								
		}
		
		String assunto = "Bom dia, segue o aprazamento por especialidade.";
		
		String mensagem = 
		"<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td style='background-color: #ffffff;' align='center' valign='top' bgcolor='#444444'><br /> <br />"
		+ "<table border='0' width='650' cellspacing='0' cellpadding='0'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td style='background-color: #2d639a; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff; padding: 0px 15px 10px 15px;' align='left' valign='top' bgcolor='#f73f9b'>"
		+ "<div style='font-size: 48px;'><strong>T.I. Informa</strong></div>"
		+ "<div>Bom dia, segue informa&ccedil;&otilde;es de aprazamento por especialidade.</div>"
		+ "</td>"
		+ "</tr>"
		+ "<tr>"
		+ "<td style='background-color: #353535; padding: 15px;' align='left' valign='top' bgcolor='#1ba5db'>"
		+ "<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td align='left' valign='top' width='100%'>"
		+ "<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'>Especialidade</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'>Disponibilidade</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'>Unidade</td>"
		+ "</tr>"
		+ table 
		+ "</tbody>"
		+ "</table>"
		+ "</td>"
		+ "</tr>"
		+ "</tbody>"
		+ "</table>"
		+ "</td>"
		+ "</tr>"
		+ "</tbody>"
		+ "</table>"
		+ "<table style='margin-top: 10px;' border='0' width='600' cellspacing='0' cellpadding='0'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td align='center' valign='middle' bgcolor='#e1e1e1'>"
		+ "<table border='0' width='650' cellspacing='0' cellpadding='0' align='center'>"
		+ "<tbody>"
		+ "<tr>"
		+ "<td style='color: #595959; font-size: 11px; font-family: Arial, Helvetica, sans-serif;' align='center' valign='middle' width='55%'><strong>Para mais detalhes, acesse o nosso sistema.</strong> <br />Link de acesso: <a style='color: #595959; text-decoration: none;' href='li1076-46.members.linode.com'>li1076-46.members.linode.com</a> <br /> <br /> <strong>E-mail autom&aacute;tico, por gentileza n&atilde;o responda. Obrigado</strong></td>"
		+ "</tr>"
		+ "</tbody>"
		+ "</table>"
		+ "</td>"
		+ "</tr>"
		+ "</tbody>"
		+ "</table>"
		+ "</td>"
		+ "</tr>"
		+ "</tbody>"
		+ "</table>";
		
		for(String email : emails)
			sendEmail(email, assunto, mensagem, null);
		 
		
	}
	
	public void enviarAgendamentobyEmail(List<String>emails){
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String dtInicio = "";
		String dtFim = "";
		Date dateInicio = null;
		Date dateFim = null;
				
		Calendar c = Calendar.getInstance();
								
		try {
			dtInicio = format.format(c.getTime());
			c.add(Calendar.DAY_OF_MONTH, 1);
			dtFim = format.format(c.getTime());
			
			dateInicio = format.parse(dtInicio);			
			dateFim = format.parse(dtFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
		
		//Unidade: Joaquim Nabuco 
		List<AtendimentoProfissionalUnidadeResultDTO> resultJn = estatisticaDAO.getRelarioAtendimentoProfissionalUnidade(1L, dateInicio, dateFim);
						
		Integer perfClinicaJnDia = new Integer(100);		
		List <Integer> listPerfClinicaJnDia = new ArrayList<Integer>();
				
		if(resultJn != null && resultJn.size() > 0){			
			for(AtendimentoProfissionalUnidadeResultDTO rjn : resultJn){				
				if(rjn.getQtdAtendimento() != null && rjn.getQtdAtendimento() > 0){
					perfClinicaJnDia = new Integer (new Double((new Double(rjn.getQtdAgendamento()) / new Double(rjn.getQtdAtendimento())) * 100).intValue());
					listPerfClinicaJnDia.add(perfClinicaJnDia);
				}
				perfClinicaJnDia = new Integer(100);
			}			
		}
		
		//Unidade: Taruma		
		List<AtendimentoProfissionalUnidadeResultDTO> resultT = estatisticaDAO.getRelarioAtendimentoProfissionalUnidade(2L, dateInicio, dateFim);
				
		Integer perfClinicaTDia = new Integer(100);
		List <Integer> listPerfClinicaTDia = new ArrayList<Integer>();
		
		if(resultT != null && resultT.size() > 0){			
			for(AtendimentoProfissionalUnidadeResultDTO rt : resultT){	
				if(rt.getQtdAtendimento() != null && rt.getQtdAtendimento() > 0){
					perfClinicaTDia = new Integer (new Double((new Double(rt.getQtdAgendamento()) / new Double(rt.getQtdAtendimento())) * 100).intValue());
					listPerfClinicaTDia.add(perfClinicaTDia);
				}
				perfClinicaTDia = new Integer(100);
			}
		}		
		
		//Unidade: GC		
		List<AtendimentoProfissionalUnidadeResultDTO> resultGc = estatisticaDAO.getRelarioAtendimentoProfissionalUnidade(7L, dateInicio, dateFim);
				
		Integer perfClinicaGcDia = new Integer(100);				
		List <Integer> listPerfClinicaGcDia = new ArrayList<Integer>();
		
		if(resultGc != null && resultGc.size() > 0){			
			for(AtendimentoProfissionalUnidadeResultDTO rgc : resultGc){	
				if(rgc.getQtdAtendimento() != null && rgc.getQtdAtendimento() > 0){
					perfClinicaGcDia = new Integer (new Double((new Double(rgc.getQtdAgendamento()) / new Double(rgc.getQtdAtendimento())) * 100).intValue());
					listPerfClinicaGcDia.add(perfClinicaGcDia);
				}
				perfClinicaGcDia = new Integer(100);
			}
		}	
		
		String assunto = "Bom dia, segue informações da unidade na data de hoje e amanhã";
		String mensagem =				
				"<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr>"
				+"<td style='background-color: #ffffff;' align='center' valign='top' bgcolor='#444444'><br /> <br />"
				+"<table border='0' width='650' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr>"
				+"<td style='background-color: #2d639a; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff; padding: 0px 15px 10px 15px;' align='left' valign='top' bgcolor='#f73f9b'>"
				+"<div style='font-size: 48px;'><strong>T.I. Informa</strong></div>"				
				+"<div>Bom dia, segue informa&ccedil;&otilde;es por unidade em um per&iacute;odo de 2(dois) dias.</div>"
				+"</td>"
				+"</tr>"
				+"<tr>"
				+"<td style='background-color: #353535; padding: 15px;' align='left' valign='top' bgcolor='#1ba5db'>"
				+"<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td align='left' valign='top' width='100%'>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'><span style='text-decoration: underline;'>Unidade: Joaquim Nabuco</span></td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtInicio + "</strong>"
				+"<table border='0' width='70%'' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: " + (listPerfClinicaJnDia != null && listPerfClinicaJnDia.size() > 0 ? listPerfClinicaJnDia.get(0) : perfClinicaJnDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultJn != null && resultJn.size() > 0 && resultJn.get(0).getQtdAgendamento() != null ? "Agendamento(s): " +resultJn.get(0).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
						
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtFim + "</strong>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: " + (listPerfClinicaJnDia != null && listPerfClinicaJnDia.size() > 1 ? listPerfClinicaJnDia.get(1) : perfClinicaJnDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultJn != null && resultJn.size() > 1 && resultJn.get(1).getQtdAgendamento() != null ? "Agendamento(s): " +resultJn.get(1).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"<tr><td align='left' valign='top' width='100%'>&nbsp;"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'><span style='text-decoration: underline;'>Unidade: Tarum&atilde;</span></td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtInicio + "</strong>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: "+ (listPerfClinicaTDia != null && listPerfClinicaTDia.size() > 0 ? listPerfClinicaTDia.get(0) : perfClinicaTDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultT != null && resultT.size() > 0 && resultT.get(0).getQtdAgendamento() != null ? "Agendamento(s): " +resultT.get(0).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtFim + "</strong>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: "+ (listPerfClinicaTDia != null && listPerfClinicaTDia.size() > 1 ? listPerfClinicaTDia.get(1) : perfClinicaTDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultT != null && resultT.size() > 1 && resultT.get(1).getQtdAgendamento() != null ? "Agendamento(s): " +resultT.get(1).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"<tr><td align='left' valign='top' width='100%'>&nbsp;"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #ffffff;' align='left' valign='top'><span style='text-decoration: underline;'>Unidade: Grande Circular</span></td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtInicio + "</strong>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: "+ (listPerfClinicaGcDia != null && listPerfClinicaGcDia.size() > 0 ? listPerfClinicaGcDia.get(0) : perfClinicaGcDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultGc != null && resultGc.size() > 0 && resultGc.get(0).getQtdAgendamento() != null ? "Agendamento(s): " +resultGc.get(0).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />&nbsp;&nbsp; <strong>" + dtFim + "</strong>"
				+"<table border='0' width='70%' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; Performance cl&iacute;nica: "+ (listPerfClinicaGcDia != null && listPerfClinicaGcDia.size() > 1 ? listPerfClinicaGcDia.get(1) : perfClinicaGcDia) +"%</td></tr>"
				+"<tr><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'>&nbsp;&nbsp;&nbsp;&nbsp; " + (resultGc != null && resultGc.size() > 1 && resultGc.get(1).getQtdAgendamento() != null ? "Agendamento(s): " +resultGc.get(1).getQtdAgendamento() : "Agendamento(s): 0") + " </td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td></tr>"
				+"</tbody>"
				+"</table>"
				+"<table style='margin-top: 10px;' border='0' width='600' cellspacing='0' cellpadding='0'>"
				+"<tbody>"
				+"<tr><td align='center' valign='middle' bgcolor='#e1e1e1'>"
				+"<table border='0' width='650' cellspacing='0' cellpadding='0' align='center'>"
				+"<tbody>"
				+"<tr><td style='color: #595959; font-size: 11px; font-family: Arial, Helvetica, sans-serif;' align='center' valign='middle' width='55%'>"
				+"<strong>Para mais detalhes, acesse o nosso sistema.</strong>"
				+"<br />Link de acesso: <a style='color: #595959; text-decoration: none;' href='li1076-46.members.linode.com'>li1076-46.members.linode.com</a> <br /> <br />"
				+"<strong>E-mail autom&aacute;tico, por gentileza n&atilde;o responda. Obrigado</strong></td></tr>"
				+"</tbody>"
				+"</table>"
				+"</td>"
				+"</tr>"
				+"</tbody>"
				+"</table>"
				+"</td>"
				+"</tr>"
				+"</tbody>"
				+"</table>";
		
		try{
			DateFormat formatt = new SimpleDateFormat("yyyy-MM-dd");
			dtInicio = formatt.format(dateInicio);
			dtFim = formatt.format(dateFim);
			
			List<String> reportFilePathNames = new ArrayList<String>();
			List<File> agendamentoFiles = new ArrayList<File>();
			
			List<Unidade> unidades = unidadeDAO.getUnidadesbyEmpresaGrupo(9L); //Drconsulta
			
			for(Unidade unidade : unidades){
				
				List<AgendamentoView> listaAgendamentoView = agendamentoDAO.getAgendamentosMedicoPorPeriodo(dtInicio, dtFim, unidade.getId());
				
				if(listaAgendamentoView != null && listaAgendamentoView.size() > 0){
					String reportFilePathName = PATH_TEMP_FILES + "ag_" + unidade.getNmApelido() + "_" +  System.currentTimeMillis() + ".pdf";
					reportFilePathNames.add(reportFilePathName);			
					
					generateReport(listaAgendamentoView, reportFilePathName);
					
					File agendamentoFile = new File(reportFilePathName);
					agendamentoFiles.add(agendamentoFile);
				}
			}												
			
			for(String email : emails)
				sendEmail(email, assunto, mensagem, reportFilePathNames);
			
			for(File agendamentoFile : agendamentoFiles)
				agendamentoFile.delete();
			
		}catch (Exception ex){
			ex.printStackTrace();
			throw new WebApplicationException("Erro ao enviar por email");
		}
		
	}
	
	public static void sendEmail(String emailCliente, String assunto, String mensagem, List<String> pathNameFiles){

		try {
			String host = "smtp.gmail.com";
	        String Password = "odontomedLima321";
	        String from = "odontomed.ti@gmail.com";
	        String toAddress = emailCliente;
	        List<String> filenames = pathNameFiles;
	        String[] splitunidade;
	       	        
	        // Propriedades do sistema
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.socketFactory.port", "465");
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtp.port", "465");
	        props.put("mail.smtp.starttls.enable", "true");
	        
	        Session session = Session.getInstance(props, null);	
	        MimeMessage message = new MimeMessage(session);
	
	        message.setFrom(new InternetAddress(from));
	        message.setRecipients(Message.RecipientType.TO, toAddress);
	        message.setSubject(assunto);
	        
	        BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mensagem, "text/html; charset=utf-8");
	
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);	        
	        message.setContent(multipart, "text/html; charset=utf-8");
	        
	        if(filenames != null && filenames.size() > 0){
		        for(String filename : filenames){
		        	splitunidade = filename.split("_");
		        	messageBodyPart = new MimeBodyPart();
			        DataSource source = new FileDataSource(filename);
			        messageBodyPart.setDataHandler(new DataHandler(source));
			        messageBodyPart.setFileName(splitunidade[1] + "_" +  System.currentTimeMillis() + ".pdf");
			        multipart.addBodyPart(messageBodyPart);
		        }
	        }
	        
	        message.setContent(multipart);

            Transport tr = session.getTransport("smtps");
            tr.connect(host, from, Password);
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
        } catch (SendFailedException sfe) {
        	sfe.printStackTrace();
			throw new WebApplicationException("Erro ao enviar por email");
        } catch (MessagingException e) {
			e.printStackTrace();
			throw new WebApplicationException("Erro ao enviar por email");
		}
	}
	
	@SuppressWarnings({ "deprecation", "rawtypes" })
	private void generateReport(List<AgendamentoView> agendamentoVwJasper, String pathNameFileToSave) throws Exception{
		
		JRDataSourceAgendamento jrDataSourceAgendamento = new JRDataSourceAgendamento(agendamentoVwJasper);
		JasperPrint jasperPrintAgendamento = getJasperPrintReportAgendamento(jrDataSourceAgendamento);
				
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		jasperPrintList.add(jasperPrintAgendamento);
		
		JRExporter exporter = new JRPdfExporter();
        exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrintList);

        OutputStream output = new FileOutputStream(new File(pathNameFileToSave));

        exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, output);
 
        exporter.exportReport();
        output.close();
	}
	
	private JasperPrint getJasperPrintReportAgendamento(JRDataSourceAgendamento jrDataSourceAgendamento ) throws Exception{
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logomarca", getClass().getResourceAsStream("/jasper/imagensJasper/LOGO-DR+CONSULTA.png"));
				
		InputStream jrxmlAgendamentos = getClass().getResourceAsStream("/jasper/Agendamentos.jrxml");
		JasperReport reportAgendamentos = JasperCompileManager.compileReport(jrxmlAgendamentos);
		
		JasperPrint jasperPrintMedidas = JasperFillManager.fillReport(reportAgendamentos, params, jrDataSourceAgendamento);
		
		return jasperPrintMedidas;
	}
	

}
