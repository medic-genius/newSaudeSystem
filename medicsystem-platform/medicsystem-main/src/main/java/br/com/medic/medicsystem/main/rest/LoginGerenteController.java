package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.medic.medicsystem.main.service.LoginGerenteService;
import br.com.medic.medicsystem.persistence.security.dto.LoginDTO;

@Path("/logingerente")
@RequestScoped
public class LoginGerenteController extends BaseController {

	@Inject
	private LoginGerenteService loginGerenteService;

	@Context
	protected UriInfo info;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(@Valid LoginDTO loginDTO) {

		boolean autorizado = loginGerenteService.login(loginDTO);
		if (autorizado) {
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}

}