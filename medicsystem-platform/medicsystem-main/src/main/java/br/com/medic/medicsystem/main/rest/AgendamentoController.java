package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.service.AgendamentoService;
import br.com.medic.medicsystem.main.service.SMSService;
import br.com.medic.medicsystem.persistence.dto.AgendamentoViewDTO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoWebDTO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.ContrachequeInfoDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteDTO;
import br.com.medic.medicsystem.persistence.dto.PagamentoAgendamentoDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.AvaliacaoAgendamento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusAgendamento;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoView;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.DateUtil.DatePattern;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;

@Path("/agendamentos")
@RequestScoped
public class AgendamentoController extends BaseController {

	@Inject
	private AgendamentoService agendamentoService;
	
	@Inject
	private SMSService smsService;

	@Context
	protected UriInfo info;

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Agendamento getAgendamento(@PathParam("id") Long id) {
		final Agendamento ag = agendamentoService.getAgendamento(id);

		Agendamento agendamento = agendamentoService.setConsultorioAgendamento(ag);

		if (agendamento == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		agendamentoService.setConsultorioAgendamento(ag);

		if (agendamento != null) {
			PacienteDTO<?> paciente = null;

			if (agendamento.getDependente() != null) {
				paciente = new PacienteDTO<Dependente>(agendamento.getDependente());
				paciente.setDependente(agendamento.getDependente());
			} else {
				paciente = new PacienteDTO<Cliente>(agendamento.getCliente());
			}
			paciente.setCliente(agendamento.getCliente());
			agendamento.setPaciente(paciente);
		}

		return agendamento;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Agendamento createAgendamento(@Valid Agendamento agendamento) {
		if (agendamento.getPaciente() == null) {
			throw new IllegalArgumentException("Paciente");
		}
		Agendamento salvo = agendamentoService.saveAgendamento(agendamento);

		return agendamentoService.setConsultorioAgendamento(salvo);
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Agendamento updateAgendamento(@PathParam("id") Long id,
			@Valid Agendamento agendamento) {

		if (agendamento != null && agendamento.getId() != null) {
			Agendamento agendamentoAtualizado = agendamentoService
					.updateAgendamento(agendamento);

			if (agendamentoAtualizado != null) {
				return agendamentoService.setConsultorioAgendamento(agendamentoAtualizado);
			} else {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/despesa")
	@Produces(MediaType.APPLICATION_JSON)
	public Despesa getDespesa(@PathParam("id") Long idAgendamento) {

		if (idAgendamento != null) {

			return agendamentoService.getDespesaAgendamento(idAgendamento);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/despesa/editar")
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaDTO getEditarDespesaAgendamento(
			@PathParam("id") Long idAgendamento) {

		if (idAgendamento != null) {

			return agendamentoService.getGerarDespesaAgendamento(idAgendamento);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/despesa/combo/formapagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FormaPagamentoEnum> getCarregaComboFormaPagamento(
			@PathParam("id") Long idAgendamento) {

		if (idAgendamento != null) {

			return agendamentoService.getFormaPagamentoEnum(idAgendamento);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/despesa/pdf")
	@Produces("application/pdf")
	public Response getDespesaPDF(@PathParam("id") Long idAgendamento) {

		if (idAgendamento != null) {

			byte[] pdf = agendamentoService.generateDespesaPDF(idAgendamento);

			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();

		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@PUT
	@Path("/{id:[0-9][0-9]*}/presente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusPresente(@PathParam("id") Long idAgendamento, 
			@QueryParam("triado") boolean triado,
			@QueryParam("prioridade") Integer prioridade,
			@QueryParam("boLogadoGerente") boolean boLogadoGerente,
			@QueryParam("statusPrioridade") Integer statusPrioridade,
			@QueryParam("obsUrgencia") String obsUrgencia,
			Agendamento agendamento) {

		if (idAgendamento != null) {

			Agendamento ag = agendamentoService
					.updateStatusPresente(idAgendamento, !triado,prioridade, boLogadoGerente, statusPrioridade, obsUrgencia); 
			return Response.ok(ag).build();
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@POST
	@Path("/{id:[0-9][0-9]*}/despesa/editar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response generateDespesaAgendamento(
			@PathParam("id") Long idAgendamento, @Valid DespesaDTO despesaDTO) {

		if (idAgendamento != null && despesaDTO != null) {

			Boolean isAutorizacaoGerente = agendamentoService
					.isAutorizacaoGerente(idAgendamento, despesaDTO);
			if (isAutorizacaoGerente) {

				despesaDTO.setBoAutorizadoGerente(isAutorizacaoGerente);
				return Response.accepted(despesaDTO).build();
			} else {

				Despesa salvo = agendamentoService.registrarDespesa(
						idAgendamento, despesaDTO);
				return Response.created(
						URI.create("/agendamento/"
								+ salvo.getAgendamento().getId())).build();
			}

		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response removeAgendamento(@PathParam("id") Long id) {
		agendamentoService.removeAgendamento(id);
		return Response.ok().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/agendamentos/medico")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoViewDTO> getAgendamentosMedico(
			@PathParam("id") Long idProfissional,
			@QueryParam("data") String data,
			@QueryParam("idUnidade") Long idUnidade
			){
		return agendamentoService.getAgendamentosMedico(idProfissional,data,idUnidade);
	}


	@GET
	@Path("/{id:[0-9][0-9]*}/atendimento/medico")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentomedicoView> getMedicoAgendamentos(
			@PathParam("id") Long idProfissional,
			@QueryParam("idEspecialidade") Long idEspecialidade
			){
		return agendamentoService.getAtendimentoMedico(idProfissional,idEspecialidade);
	}


	@PUT
	@Path("/{id:[0-9][0-9]*}/ausente/{idOperador:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusAusente(@PathParam("id") Long idAgendamento, @PathParam("idOperador") Long idOperador) {

		if (idAgendamento != null && idOperador != null) {

			Agendamento ag = agendamentoService
					.updateStatusAusente(idAgendamento,idOperador);
			if(ag != null)
				return Response.ok().build();
			else
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}/atendido/{idOperador:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusAtendido(@PathParam("id") Long idAgendamento,
			@PathParam("idOperador") Long idOperador) {

		if (idAgendamento != null && idOperador != null) {
			Agendamento ag = agendamentoService
					.updateStatusAtendido(idAgendamento, idOperador);
			if(ag != null) {
				try {
					Boolean pushSent = agendamentoService.sendNotificacaoAvaliacao(ag);
					if(pushSent == null || !pushSent.booleanValue()) {
						smsService.enviarSmsAvaliacaoConsulta(ag);
					}
				} catch (ClientHumanException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return Response.ok().build();
			} else {
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/atendimento/{idUnidade:[0-9][0-9]*}/enfermagem")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentomedicoView> getAtendimentoEnfermagem(
			@PathParam("idUnidade") Long idUnidade,
			@QueryParam("idEspecialidade") List<Long> idEspecialidade,
			@QueryParam("idProfissional") List<Long> idProfissional

			){

		if(idEspecialidade != null && idEspecialidade.size() > 0 && idUnidade != null )
			return agendamentoService.getAtendimentoEnfermagem(idEspecialidade, idUnidade, idProfissional);
		else
			return null;
	}


	@PUT
	@Path("/{id:[0-9][0-9]*}/update/statustriagem")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusTriagem(@PathParam("id") Long idAgendamento, 
			@QueryParam("prioridade") Integer prioridade,
			@QueryParam("statusPrioridade") Integer statusPrioridade,
			@QueryParam("obsUrgencia") String obsUrgencia) {

		if (idAgendamento != null) {

			Agendamento ag = agendamentoService
					.updateStatusTriagem(idAgendamento, prioridade, statusPrioridade, obsUrgencia);
			if(ag != null)
				return Response.ok().build();
			else
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}


	@GET
	@Path("/{id:[0-9][0-9]*}/ultimofaltoso/{intipopaciente}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getServicoExame( @PathParam("id") Long idPaciente, 
			@PathParam("intipopaciente") String tipoPaciente ) {
		List<String> list = new ArrayList<String>();
		list.add(agendamentoService.getDiasBloqueadosTeleamarketing(idPaciente, tipoPaciente));

		return list;
	}
	
	@GET
	@Path("/pacientes/{idPaciente:[0-9]+}/tipos/{tipoPaciente:[0-9]}/quantidade")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Integer> countPacienteAgendamentosConsulta(@PathParam("idPaciente") Long idPaciente, 
			@PathParam("tipoPaciente") Integer tipoPaciente,
			@QueryParam("data") String data,
			@QueryParam("idEspecialidade") Long idEspecialidade,
			@QueryParam("idServico") Long idServico) {
		if(idEspecialidade == null || idServico == null) {
			throw new ServerErrorException("Especialidade e/ou servico não fornecidos", 500);
		}
		Date dia = new Date();
		if(data != null) {
			dia = DateUtil.parseDate(data, DatePattern.DDMMAA.getPattern());
		}
		Integer qtd = agendamentoService.countPacienteAgendamentosConsulta(idPaciente, tipoPaciente, 
				dia, idEspecialidade, idServico);
		Map<String, Integer> result = new HashMap<String, Integer>();
		result.put("quantidade", qtd);
		return result;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/view")
	@Produces(MediaType.APPLICATION_JSON)
	public AgendamentoView getAgendamentoView(@PathParam("id") Long idAgendamento) {
		if(idAgendamento != null) {
			return agendamentoService.getAgendamentoView(idAgendamento);
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	// Aplicativo
	
	@GET
	@Path("/historico_cliente")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getHistoricoAgendamentosClientes(
			@QueryParam("type") Integer tipoListagem,
			@QueryParam("offsetfact") Integer offsetFact) {
		
		
		if(tipoListagem != null) {
			if(offsetFact == null) {
				offsetFact = 0;
			}
			return agendamentoService.getHistoricoAgendamentos(offsetFact, tipoListagem);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@POST
	@Path("/avaliacao_atendimento")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse avaliarAtendimento(AvaliacaoAgendamento avaliacao) {
		if(avaliacao != null) {
			return agendamentoService.persistAvaliacaoAtendimento(avaliacao);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@GET
	@Path("/cliente/ultimo_faltoso")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getUltimoAgendamentoFaltoso() {
		return agendamentoService.getUltimoAgendamentoFaltoso();
	}
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse createAgendamentoFromApp(AgendamentoWebDTO agendamentoWeb) {
		if (agendamentoWeb == null) {
			throw new IllegalArgumentException("Paciente");
		}
		return agendamentoService.createAgendamentoFromApp(agendamentoWeb);
	}
	
	@POST
	@Path("/pagamento_avulso")
	@Produces(MediaType.APPLICATION_JSON)
	public AssinaturaRecorrenteResultDTO createPagamentoAvulso(
			@QueryParam("source") String source,
			PagamentoAgendamentoDTO data) {
		if(data != null && data.getFormaPagamento() != null && 
				data.getIdAgendamento() != null ) {
			return agendamentoService.pagarAgendamento(data, source);
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus("fail");
		result.setMessage("Dados não fornecidos ou ausentes");
		return result;
	}
	
	/*relatorio agendamento medico */
	@GET
	@Path("/statusagendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StatusAgendamento> getStatusAgendamento(){
		return agendamentoService.getStatusAgendamento();
	}
	
	/*@GET
	@Path("/relatorioagendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoView> getAgendamentosMedico( @QueryParam("dataInicio") String dataInicio, @QueryParam("dataFim") String dataFim, @QueryParam("statusAg") Integer statusAg,
			@QueryParam("idOperador") List<String> idOperadorCadastro, @QueryParam("inGrupoFuncionario") Long inGrupoFuncionario, @QueryParam("idUnidade") Long idUnidade, 
			@QueryParam("idEspecialidade") Long idEspecialidade, @QueryParam("idProfissional") Long idProfissional){
		return agendamentoService.getRelatorioAgendamentosMedico(dataInicio, dataFim, statusAg, idOperadorCadastro, inGrupoFuncionario, idUnidade, idEspecialidade, idProfissional);
	}*/
	
	@GET
	@Path("/relatorioagendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public GenericPaginateDTO getAgendamentosMedico( @QueryParam("dataInicio") String dataInicio, @QueryParam("dataFim") String dataFim, @QueryParam("statusAg") Integer statusAg,
			@QueryParam("idOperador") List<String> idOperadorCadastro, @QueryParam("inGrupoFuncionario") Long inGrupoFuncionario, @QueryParam("idUnidade") Long idUnidade, 
			@QueryParam("idEspecialidade") Long idEspecialidade, @QueryParam("idProfissional") Long idProfissional, @QueryParam("offset") @DefaultValue("1") Integer offset,
			@QueryParam("limit") @DefaultValue("50") Integer limit){
		return agendamentoService.getRelatorioAgendamentosMedico(dataInicio, dataFim, statusAg, idOperadorCadastro, inGrupoFuncionario, idUnidade, idEspecialidade, idProfissional, offset, limit);
	}
	
	@PUT
	@Path("/{id:[0-9]+}/contracheque/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerContrachequeInfo(
			@PathParam("id") Long idAgendamento, ContrachequeInfoDTO info) {
		if (info != null && info.getNmOrgao() != null
				&& info.getNrMatricula() != null && info.getNmSenha() != null) {
			Boolean result = agendamentoService.registerContrachequeInfo(
					idAgendamento, info);
			if (result) {
				return Response.ok().build();
			}
		}
		return Response.serverError().build();
	}
	
//	@GET
//	@Path("/lastagendamentoconsulta")
//	@Produces(MediaType.APPLICATION_JSON)
//	public ServicoEspecialidade getUltimoAgendamentoConsultaByEspecialidade(
//			@QueryParam("idServico") Long idServico,
//			@QueryParam("idEspecialidade") Long idEspecialidade) {
//		
//		return agendamentoService.getUltimoAgendamentoConsultaByEspecialidade(idServico, idEspecialidade);
//	}
}
