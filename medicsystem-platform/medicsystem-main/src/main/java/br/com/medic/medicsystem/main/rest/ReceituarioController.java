package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.ReceituarioService;
import br.com.medic.medicsystem.persistence.dto.ReceituarioDTO;
import br.com.medic.medicsystem.persistence.model.Medicamento;
import br.com.medic.medicsystem.persistence.model.Receituario;
import br.com.medic.medicsystem.persistence.model.ReceituarioOftalmologico;
import br.com.medic.medicsystem.persistence.model.TemplateReceituario;



@Path("/receituarios")
@RequestScoped
public class ReceituarioController {
	
	@Inject
	private ReceituarioService receituarioService;
	
	@POST
	@Path("/{id:[0-9][0-9]*}/save/{idPaciente:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReceituario(
			@PathParam("id") Long idAgendamento,
			@PathParam("idPaciente") Long idPaciente,
			@QueryParam("inTipoPaciente") String inTipoPaciente,
			Receituario receituario) {
		if(idAgendamento != null && idPaciente != null && inTipoPaciente != null && receituario != null ){
			boolean salvo = receituarioService.saveReceituario(idAgendamento, receituario, inTipoPaciente, idPaciente);
			if (salvo) {
				return Response.ok().build();
			} else {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}
		}else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateReceituario(Receituario receituario){
		if(receituario != null){		
			boolean atualizado = receituarioService.updateReceituario(receituario);
			if (atualizado) {
				return Response.ok().build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}
		}else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/receituario")
	@Produces(MediaType.APPLICATION_JSON)
	public ReceituarioDTO getReceituario(@PathParam("id") Long idAgendamento){
		if(idAgendamento != null){
			return receituarioService.getReceituario(idAgendamento);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Medicamento> searchMedicamentos(@QueryParam("nmMedicamento") String nmMedicamento) {
		if(nmMedicamento != null) {
			return receituarioService.searchMedicamentos(nmMedicamento);
		}
		return null;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/receituariooftal")
	@Produces(MediaType.APPLICATION_JSON)
	public ReceituarioOftalmologico getReceituarioOftal(@PathParam("id") Long idAgendamento){
		if(idAgendamento != null){
			return receituarioService.getReceituarioOftal(idAgendamento);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@POST
	@Path("/{id:[0-9][0-9]*}/saveoftal/{idPaciente:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReceituarioOftal(@PathParam("id") Long idAgendamento,
			@PathParam("idPaciente") Long idPaciente, @QueryParam("inTipoPaciente") 
			String inTipoPaciente, ReceituarioOftalmologico receituario) {
		if(idAgendamento != null && idPaciente != null && inTipoPaciente != null && receituario != null ){
			boolean salvo = receituarioService.saveReceituarioOftal(idAgendamento, receituario, inTipoPaciente, idPaciente);
			if (salvo) {
				return Response.ok().build();
			} else {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}
		}else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/updateoftal")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateReceituarioOftal(ReceituarioOftalmologico receituario){
		if(receituario != null){
			boolean atualizado = receituarioService.updateReceituarioOftal(receituario);
			if (atualizado) {
				return Response.ok().build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/historicooftal/{intipopaciente}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ReceituarioOftalmologico> getHistoricoReceituarioOftal(@PathParam("id") Long idPaciente, @PathParam("intipopaciente") String inTipoPaciente ) {
		if(idPaciente != null && inTipoPaciente != null){
			return receituarioService.historicoOftalmologico(idPaciente,inTipoPaciente);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/medicos/{idMedico:[0-9]+}/especialidades/{idEspecialidade:[0-9]+}/templates")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TemplateReceituario> getTemplatesReceituarioForEspecialidadeProfissional(
			@PathParam("idMedico") Long idMedico, @PathParam("idEspecialidade") Long idEspecialidade) {
		if(idMedico == null || idEspecialidade == null) {
			throw new WebApplicationException("Nenhum dado fornecido", 500);
		}
		return receituarioService.getTemplatesReceituarioForEspecialidadeProfissional(idMedico, idEspecialidade);
	}
	
	@GET
	@Path("/templates/{idTemplate:[0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public TemplateReceituario getTemplateReceituario(
			@PathParam("idTemplate") Long idTemplate) {
		if(idTemplate == null) {
			throw new WebApplicationException("Nenhum dado fornecido", 500);
		}
		return receituarioService.getTemplateReceituario(idTemplate);
	}
	
	@PUT
	@Path("/templates/{idTemplate:[0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public TemplateReceituario updateTemplateReceituario(
			@PathParam("idTemplate") Long idTemplate,
			TemplateReceituario template) {
		if(idTemplate == null || template == null) {
			throw new WebApplicationException("Nenhum dado fornecido", 500);
		}
		return receituarioService.updateTemplateReceituario(idTemplate, template);
	}
	
	@DELETE
	@Path("/templates/{idTemplate:[0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTemplateReceituario(
			@PathParam("idTemplate") Long idTemplate) {
		if(idTemplate == null) {
			throw new WebApplicationException("Nenhum dado fornecido", 500);
		}
		if(receituarioService.deleteTemplateReceituario(idTemplate) != null) {
			return Response.ok().build();
		} else {
			return Response.serverError().build();
		}
	}
	
	@POST
	@Path("/medicos/{idMedico:[0-9]+}/especialidades/{idEspecialidade:[0-9]+}/templates")
	@Produces(MediaType.APPLICATION_JSON)
	public TemplateReceituario saveTemplateReceituario(@PathParam("idMedico") Long idMedico,
			@PathParam("idEspecialidade") Long idEspecialidade, TemplateReceituario newTemplate) {
		if(idMedico != null && idEspecialidade != null) {
			TemplateReceituario tpl = receituarioService.saveTemplateReceituario(idMedico, 
					idEspecialidade, newTemplate);
			return tpl;
		}
		return null;
	}
	
	@GET
	@Path("/templates/agendamentos/{idAgendamento:[0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TemplateReceituario> getTemplatesReceituarioForAgendamento(
			@PathParam("idAgendamento") Long idAgendamento) {
		if(idAgendamento == null) {
			throw new WebApplicationException("Nenhum dado foi fornecido", 500);
		}
		return receituarioService.getTemplatesReceituarioForAgendamento(idAgendamento);
	}
}
