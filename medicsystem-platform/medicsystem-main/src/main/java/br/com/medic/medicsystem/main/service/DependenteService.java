package br.com.medic.medicsystem.main.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.util.ValidaCPF;
import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;

@Stateless
public class DependenteService {

	@Inject
	private Logger logger;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;

	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;

	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;

	public Dependente getDependente(Long id) {

		logger.debug("Obtendo um dependente...");

		return dependenteDAO.searchByKey(Dependente.class, id);
	}

	public List<Dependente> getDependentesByCliente(Long idCliente) {

		logger.debug("Obtendo dependentes ativos de um cliente...");

		return dependenteDAO.getDependentesByCliente(idCliente);
	}
	
	public Dependente saveOnlyDependenteInCliente(Dependente dependente, Long idCliente){
		
		Cliente cliente = new Cliente();
		cliente = clienteDAO.searchByKey(Cliente.class, idCliente);

		dependente.setNrCodCliente(gerarCodigoDependente(cliente));
		dependente.setNrCep(cleanupNumberOnlyVariables(dependente.getNrCep()));

		if (dependente.getNrCpf() != null && dependente.getNrCpf().length() > 0) {
			dependente.setNrCpf(cleanupNumberOnlyVariables(dependente.getNrCpf()));
			if (!ValidaCPF.isCPF(dependente.getNrCpf()))
				throw new IllegalArgumentException("[CPF Invalido].");
		}

		dependente.setCidade(dependente.getCidade() != null && dependente.getCidade().getId() != null
				? cidadeDAO.getCidade(dependente.getCidade().getId()) : null);
		dependente.setBairro(dependente.getBairro() != null && dependente.getBairro().getId() != null
				? bairroDAO.getBairro(dependente.getBairro().getId()) : null);

		dependente.setCliente(cliente);

		dependente = dependenteDAO.persist(dependente);
		
		return dependente;
	}

	public Dependente saveDependente(Dependente dependente, Long idCliente) {

		Cliente cliente = new Cliente();
		cliente = clienteDAO.searchByKey(Cliente.class, idCliente);

		dependente.setNrCodCliente(gerarCodigoDependente(cliente));
		dependente.setNrCep(cleanupNumberOnlyVariables(dependente.getNrCep()));

		if (dependente.getNrCpf() != null) {
			dependente.setNrCpf(cleanupNumberOnlyVariables(dependente.getNrCpf()));
			if (!ValidaCPF.isCPF(dependente.getNrCpf()))
				throw new IllegalArgumentException("[CPF Invalido].");
		}

		dependente.setCidade(dependente.getCidade() != null && dependente.getCidade().getId() != null
				? cidadeDAO.getCidade(dependente.getCidade().getId()) : null);
		dependente.setBairro(dependente.getBairro() != null && dependente.getBairro().getId() != null
				? bairroDAO.getBairro(dependente.getBairro().getId()) : null);

		dependente.setCliente(cliente);

		dependente = dependenteDAO.persist(dependente);

		// Adicionar dependente no contrato do cliente nao fazendo uso
		Contrato contrato = contratoDAO.getContratoPorCliente(cliente.getId());

		if (contrato != null) {

			ContratoDependente condep = new ContratoDependente();
			condep.setCreditoConveniada(0);
			condep.setBlAutorizaDebAut(false);
			condep.setContrato(contrato);
			condep.setInSituacao(0);
			condep.setDependente(dependente);
			condep.setBoFazUsoPlano(false);
			contrato.addContratoDependente(condep);

			contratoDAO.persist(contrato);
		}

		return dependente;

		// verifica se dependente já existe
		// Cliente encontrato = clienteDAO.getClientePorCPF(cliente.getNrCPF());
		//
		// if (encontrato != null) {
		// throw new IllegalArgumentException("[CPF Invalido/Cliente
		// existente].");
		// }

	}

	public Dependente updateDependente(Long idDependente, Dependente dependente) {

		Dependente dependenteAtualizar = getDependente(idDependente);

		if (dependenteAtualizar.getCliente() != null && dependenteAtualizar.getCliente().getId() != null) {

			dependente.setCliente(dependenteAtualizar.getCliente());
			if (dependente.getFotoDependenteEncoded() != null) {

				try {
					dependente.setFotoDependente(dependente.getFotoDependenteEncoded().getBytes("ISO-8859-1"));

				} catch (UnsupportedEncodingException e) {

					return null;
				}

			}

			Dependente dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, dependente.getId());
			if (dependenteOriginal.getFotoDependente() != null && dependente.getFotoDependenteEncoded() == null)
				dependente.setFotoDependente(dependenteOriginal.getFotoDependente());

			dependente = dependenteDAO.update(dependente);

		} else {

			return null;
		}

		return dependente;
	}

	private String cleanupNumberOnlyVariables(String var) {
		if (var != null) {
			var = var.replaceAll("\\s", "");
			var = var.trim();
			var = var.replaceAll("[^0-9]", "");

			return var;
		}

		return null;
	}

	private String gerarCodigoDependente(Cliente cliente) {

		Long qtdDependentes = dependenteDAO.getCountDependentesByCliente(cliente.getId());
		String codCliente = cliente.getNrCodCliente();
		String codDependente = "";

		// Gera a numeração a ser mostrada no cadastro de dependente.
		if (qtdDependentes.equals(0))
			codDependente = codCliente.concat("/01");
		else {

			Integer seq = (int) (qtdDependentes + 1);

			if (seq < 10)
				codDependente = codCliente.concat("/0").concat(String.valueOf(seq));
			else
				codDependente = codCliente.concat("/").concat(String.valueOf(seq));
		}

		return codDependente;
	}
	
	public Dependente getDependenteByNameAndDtNascimento(String nmDependente, Date dtNascimento) {
		return dependenteDAO.getDependenteByNameAndDtNascimento(nmDependente, dtNascimento);
	}
	
	public Dependente getDependenteByCPFOrCode(String cpf, String codStr) {
		cpf = cleanupNumberOnlyVariables(cpf);
//		codStr = cleanupNumberOnlyVariables(codStr);
		if(cpf != null || codStr != null) {
			return dependenteDAO.getDependenteByCPFOrCode(cpf, codStr);
		}
		return null;
	}
}
