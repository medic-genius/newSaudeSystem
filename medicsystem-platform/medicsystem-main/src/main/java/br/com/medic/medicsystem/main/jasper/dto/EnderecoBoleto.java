package br.com.medic.medicsystem.main.jasper.dto;

import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.persistence.dto.BoletoDTO;

public class EnderecoBoleto implements JRDataSource{
	

	private Iterator<BoletoDTO> itrBoletos;
	
	private BoletoDTO valorAtual;
	
	private boolean irParaProximoBoleto = true;
	
	
	public EnderecoBoleto(List<BoletoDTO> boletos) {
		this.itrBoletos = boletos.iterator();
	}
	
	public boolean next() throws JRException {

		this.valorAtual = this.itrBoletos.hasNext() ? this.itrBoletos
		        .next() : null;
		this.irParaProximoBoleto = (this.valorAtual != null);

		return this.irParaProximoBoleto;
	}
	
	@Override
	public Object getFieldValue(JRField campo) throws JRException {
		
		
		String nomeCampo  = campo.getName();
		
		
		if ("nmCliente".equals(nomeCampo)) {
			return valorAtual.getNmCliente();

		} else if ("endereco".equals(nomeCampo)) {
			return valorAtual.getEndereco();

		} else if ("endereco1".equals(nomeCampo)) {
			return valorAtual.getEnderecoComplemento();

		}else{
			return null;
		}
		
	}
	
	
	
	

}
