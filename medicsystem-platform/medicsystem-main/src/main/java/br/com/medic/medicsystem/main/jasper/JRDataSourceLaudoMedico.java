package br.com.medic.medicsystem.main.jasper;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.samples.AbstractReportGenerator;
import org.pentaho.reporting.libraries.resourceloader.Resource;
import org.pentaho.reporting.libraries.resourceloader.ResourceException;
import org.pentaho.reporting.libraries.resourceloader.ResourceManager;

import br.com.medic.medicsystem.persistence.dto.LaudoAtendimentoDTO;

public class JRDataSourceLaudoMedico extends AbstractReportGenerator{
	
	LaudoAtendimentoDTO laudoDTO;
	/**
	 * Default constructor for this sample report generator
	 */
	public JRDataSourceLaudoMedico()
	{
	 
	}
	 
	public JRDataSourceLaudoMedico( LaudoAtendimentoDTO laudoDTO)
	{
		this.laudoDTO = laudoDTO;
	}
	
	/* Gerar relatorio no PENTAHO - Inicio */
	/**
	 * Retorna a definição do relatório que será usada para gerar o relatório. Neste caso, o 
	 * relatório será carregado e analisado a partir de um arquivo contido neste pacote.
	 *
	 * @return A definição de relatório carregado e analisado para ser usado na geração de relatórios.
	 */
	public MasterReport getReportDefinition()
	{
		try
		{
			// Using the classloader, get the URL to the reportDefinition file
			final ClassLoader classloader = this.getClass().getClassLoader();
			final URL reportDefinitionURL = classloader.getResource("/jasper/LaudoMedico.prpt");

			// Parse the report file
			final ResourceManager resourceManager = new ResourceManager();
			resourceManager.registerDefaults();
			final Resource directly = resourceManager.createDirectly(reportDefinitionURL, MasterReport.class);
			return (MasterReport) directly.getResource();
		}
		catch (ResourceException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/** 
	 * Retorna a fábrica de dados que será usada para gerar os dados usados ​​durante a geração do relatório. Neste exemplo, 
	 * retornaremos nulo uma vez que a fábrica de dados foi definida na definição do relatório. 
	 * 
	 * @retivar a fábrica de dados usada com o gerador de relatórios 
	 */ 
	public DataFactory getDataFactory () 
	{ 
		return null; 
	}
	
	/** 
	 * Retorna o conjunto de parâmetros do relatório de tempo de execução. Este relatório de exemplo usa os seguintes três parâmetros: 
	 * <ul> 
	 * <li> <b> Título do relatório </ b> - O texto do título na parte superior do relatório </ li> 
	 * <li> <b> Nomes dos clientes < / B> 
	 	- uma série de nomes de clientes para mostrar no relatório </ li> * <li> <b> Col ​​Headers BG Color </ b> - a cor de fundo para os cabeçalhos das colunas </ li> 
	 * </ ul> 
	 * 
	 * @ Retornar <code> null </ code> indicando que o gerador de relatório não usa nenhum parâmetro de relatório 
	 */ 
	
//	public Map<String, Object> getReportParameters() 
//	{ 
//		return null; 
//	}
	
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> getReportParameters()
	  {
	    final Map parameters = new HashMap<String, Object>();
	    
	    String html = laudoDTO.getConteudo();
	    String nmPaciente = laudoDTO.getNmPaciente();
	    String nmMedico = laudoDTO.getNmMedico();
	    Integer idade = laudoDTO.getIdade();
	    String especialidade = laudoDTO.getEspecialidade();
	    String dataExame = laudoDTO.getDataExame();
	    String crm = laudoDTO.getCrm();
	    String assinaturaDigitalBase64 = laudoDTO.getAssinaturaDigital();
	    	    	    
	    InputStream caminhoImagemlogo = getClass().getResourceAsStream("/jasper/imagensJasper/LOGO-DR+CONSULTA.png");
	    InputStream caminhoImagemrodape = getClass().getResourceAsStream("/jasper/imagensJasper/2viaExames.jpg");
	    InputStream caminhoImagemwatermark = getClass().getResourceAsStream("/jasper/imagensJasper/DEZPORCENTOTRANSP_LAUDO.jpg");
	   
	    Image watermark = null;
	    Image rodape = null;
	    Image logomarca = null;
	    BufferedImage imgAssinatura = null;
	    byte[] imageByte;
	    
	    try {
	    	//File pathToFile = new File( caminhoImagemlogo.getPath() );
	        logomarca = ImageIO.read(caminhoImagemlogo);
	        
	        //File pathToFile1 = new File( caminhoImagemrodape.getPath() );
	        rodape = ImageIO.read(caminhoImagemrodape);
	        
	        //File pathToFile2 = new File( caminhoImagemwatermark.getPath() );
	        watermark = ImageIO.read(caminhoImagemwatermark);
	        
	        if(assinaturaDigitalBase64 != null){
	        	String[] parts = assinaturaDigitalBase64.split(",");
	    		String imageString = parts[1];
	    		
	    		Base64 decoder = new Base64();
		    	imageByte = decoder.decode(imageString);
		    	ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		    	imgAssinatura = ImageIO.read(bis);
		    	bis.close();
	        }
	        
	    	
	    	
	        
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	    
	    
	    parameters.put("conteudo", html);
	    parameters.put("nmPaciente", nmPaciente);
	    parameters.put("nmMedico", nmMedico);
	    parameters.put("id", idade);
	    parameters.put("especialidade", especialidade);
	    parameters.put("dataExame", dataExame);
	    parameters.put("crm", crm);
	    parameters.put("logomarca", logomarca);
	    parameters.put("rodape", rodape);
	    parameters.put("watermark", watermark);
	    parameters.put("assinatura", imgAssinatura);
	    
	    return parameters;
	  }
	
	/* Gerar relatorio no PENTAHO - Fim */
}
