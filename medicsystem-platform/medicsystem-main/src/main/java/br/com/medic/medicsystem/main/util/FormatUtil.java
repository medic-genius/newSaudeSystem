package br.com.medic.medicsystem.main.util;


public class FormatUtil {

	public FormatUtil() {
	}

	private static String formatar(String valor, String mascara) {
		String dado = "";
		for (int i = 0; i < valor.length(); i++) {
			char c = valor.charAt(i);
			if (Character.isDigit(c))
				dado = (new StringBuilder(String.valueOf(dado))).append(c)
						.toString();
		}

		int indMascara = mascara.length();
		int indCampo;
		for (indCampo = dado.length(); indCampo > 0 && indMascara > 0;)
			if (mascara.charAt(--indMascara) == '#')
				indCampo--;

		String saida = "";
		for (; indMascara < mascara.length(); indMascara++)
			saida = (new StringBuilder(String.valueOf(saida))).append(
					mascara.charAt(indMascara) != '#' ? mascara
							.charAt(indMascara) : dado.charAt(indCampo++))
					.toString();

		return saida;
	}

	public static String formatarCpf(String cpf) {
		if (cpf.length() < 11)
			return "";
		else
			return formatar(cpf, "###.###.###-##");
	}

	public static String formatarCnpj(String cnpj) {
		if (cnpj.length() < 14)
			return "";
		else
			return formatar(cnpj, "##.###.###/####-##");
	}

	public static String formatarCep(String cep) {
		if (cep.length() < 8)
			return "";
		else
			return formatar(cep, "#####-###");
	}
}
