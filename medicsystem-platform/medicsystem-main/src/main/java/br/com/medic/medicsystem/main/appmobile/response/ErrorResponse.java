package br.com.medic.medicsystem.main.appmobile.response;

public class ErrorResponse extends AppResponse {
	private String message;
	
	public ErrorResponse() {
		super("error");
	}
	
	public ErrorResponse(String message) {
		this(null, message);
	}
	
	public ErrorResponse(Integer code, String message) {
		this();
		this.setCode(code);
		this.setMessage(message);
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
