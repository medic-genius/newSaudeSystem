package br.com.medic.medicsystem.main.service;

import java.io.File;
import java.io.InputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.ErrorResponse;
import br.com.medic.medicsystem.main.appmobile.response.SuccessResponse;
import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.main.util.SearchContainer;
import br.com.medic.medicsystem.main.util.SimpleKVGenericDTO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ArquivoFuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoProfissionalDAO;
import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ConfiguracaoSistemaDAO;
import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.ProfissionalDAO;
import br.com.medic.medicsystem.persistence.dao.RegistroChamadaDAO;
import br.com.medic.medicsystem.persistence.dao.UnidadeDAO;
import br.com.medic.medicsystem.persistence.dto.ArquivosFuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.AtividadeAgendamentoDTO;
import br.com.medic.medicsystem.persistence.dto.EspecialidadeDTO;
import br.com.medic.medicsystem.persistence.dto.IntervaloBloqueadoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.ArquivoFuncionario;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Bloqueio;
import br.com.medic.medicsystem.persistence.model.Cidade;
import br.com.medic.medicsystem.persistence.model.ConfiguracaoSistema;
import br.com.medic.medicsystem.persistence.model.Departamento;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.RegistroChamadaProfissional;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.enums.GrupoFuncionario;
import br.com.medic.medicsystem.persistence.model.enums.StaticValuesGlobal;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoProfissionalView;
import br.com.medic.medicsystem.persistence.model.views.ServicoExame;
import br.com.medic.medicsystem.persistence.security.KeycloakHelper;
import br.com.medic.medicsystem.persistence.security.MongoResource;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.security.keycloak.KeycloakUtil;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.UploadFileUtil;
import br.com.medic.medicsystem.persistence.utils.Utils;


/**
 * @author Phillip Furtado
 * @since 01/2016
 * @version 1.2
 * 
 * last modification : 09/05/2016
 * by: Joelton Matos
 */

@Stateless
public class FuncionarioService {
	
	@Inject
	private Logger logger;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	
	@Inject
	@Named("profissional-dao")
	private ProfissionalDAO profissionalDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;

	@Inject
	@Named("atendimentoprofissional-dao")
	private AtendimentoProfissionalDAO atendimentoProfissionalDAO;
	
	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;
	
	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;
	
	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;
	
	@Inject
	@Named("arquivofuncionario-dao")
	private ArquivoFuncionarioDAO arquivoFuncionarioDAO;
	
	@Inject
	@Named("configuracaosistema-dao")
	private ConfiguracaoSistemaDAO configuracaoSistemaDAO;
	
	@Inject
	@Named("registrochamada-dao")
	private RegistroChamadaDAO registroChamadaDAO;
	
	@Inject
	@Named("especialidade-dao")
	private EspecialidadeDAO especialidadeDAO;
	
	@Inject
	private KeycloakHelper khelper;
	
	@Inject
	private SecurityService securityService;
	
	@Inject
	private AgendamentoService agendamentoService;
	
	@Inject
	private MongoResource mongo;
	
	@Inject
	private KeycloakUtil keycloakUtil;
	
	
	Properties props = PropertiesLoader.getInstance().load("message.properties");
	
	

	public Funcionario getMedico(Long id) {
		logger.debug("Obtendo lista de medico...");
		return funcionarioDAO.searchByKey(Funcionario.class, id);
	}

	public List<Funcionario> getMedicos(String nmMedico) {
		logger.debug("Obtendo lista de medicos...");
		return funcionarioDAO.getMedicosPorNome(nmMedico);
	}

	public Collection<Profissional> getProfissionaisPorEspecialidade(Long idEspecialidade, Long idsubEspecialidade, Long idUnidade) {
		logger.debug("Obtendo subEspecialidades de um profissional...");
		return funcionarioDAO.getProfissionaisPorEspecialidade(idEspecialidade, idsubEspecialidade, idUnidade);
	}

	public Collection<Profissional> getProfissionaisPorEspecialidadePorData(Long idEspecialidade, Long idsubEspecialidade, Long idUnidade, String data) {
		logger.debug("Obtendo subEspecialidades de um profissional em um dia...");
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		Date dia = null;

		try {
			dia = inputFormat.parse(data);

		} catch (ParseException e) {
			throw new IllegalArgumentException("Data invalida.");
		}

		// processa data inicio mes
		Calendar calInicio = Calendar.getInstance();
		calInicio.setTime(dia);

		calInicio.set(Calendar.MINUTE, 0);
		calInicio.set(Calendar.SECOND, 0);
		calInicio.set(Calendar.MILLISECOND, 0);

		// processa data fim mes
		Calendar calFim = Calendar.getInstance();
		calInicio.setTime(dia);

		Collection<Profissional> profissionais = getProfissionaisPorEspecialidade(idEspecialidade, idsubEspecialidade, idUnidade);
		
		List<Profissional> profissionaisDisponiveis = new ArrayList<Profissional>();
		for (Profissional profissional : profissionais) {
			//modificar para receber o servico
			List<String> blocked = getDiasSemAtendimentoProfissional(profissional.getId(),null, idEspecialidade, idUnidade, idsubEspecialidade, null, calInicio, calFim, null, null, null);
			if (blocked.size() == 0) {
				profissionaisDisponiveis.add(profissional);
			}
//			for (String string : blocked) {
//				System.out.println(string);
//			}
		}

		if (profissionaisDisponiveis.size() == 0) {
			throw new ObjectNotFoundException("Nenhum profissional disponivel para o dia selecionado");
		}

		return profissionaisDisponiveis;
	}

	public List<AtendimentoProfissionalDTO> getCalendarioSemanalProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta) {

		logger.debug("Obtendo informacoes sobre horario de atendimento de um profissional...");

		return atendimentoProfissionalDAO.getCalendarioSemanalProfissional(
		        idProfissional, idServico, idEspecialidade, idUnidade, idSubEspecialidade,
		        inTipoConsulta, null, null);
	}

	public List<String> getDiasSemAtendimentoProfissional(Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade, Long idSubEspecialidade, Integer inTipoConsulta, Integer ano, Integer mes, Integer inTipo, Long idPaciente, String tipoPaciente) {
		// processa data inicio mes
		Calendar calInicio = Calendar.getInstance();
		calInicio.set(Calendar.YEAR, ano);
		calInicio.set(Calendar.MONTH, mes);
		calInicio.set(Calendar.DAY_OF_MONTH, 1);
		calInicio.set(Calendar.HOUR_OF_DAY, 0);
		calInicio.set(Calendar.MINUTE, 0);
		calInicio.set(Calendar.SECOND, 0);
		calInicio.set(Calendar.MILLISECOND, 0);
		
		calInicio.add(Calendar.MONTH, -2);

		// processa data fim mes
		Calendar calFim = Calendar.getInstance();
		calFim.set(Calendar.YEAR, ano);
		calFim.set(Calendar.MONTH, mes);
		calFim.set(Calendar.DAY_OF_MONTH, 0);
		calFim.add(Calendar.MONTH, 1);

		return getDiasSemAtendimentoProfissional(idProfissional, idServico,  idEspecialidade, idUnidade, idSubEspecialidade, inTipoConsulta, calInicio, calFim, inTipo, idPaciente, tipoPaciente);
	}

	public List<String> getDiasSemAtendimentoProfissional(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade, Long idSubEspecialidade,
	        Integer inTipoConsulta, Calendar calInicio, Calendar calFim, Integer inTipo, Long idPaciente, String tipoPaciente) {
		
		if(inTipo == null){
			inTipo = new Integer(0);
		}
		
		khelper.initialize("keycloak-secondauth.json");
		
		boolean hasRole = khelper.hasRoles(
		        securityService.getUserId(),
		        securityService.getUserToken(), "telemarketing");
		

		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat angularFormatter = new SimpleDateFormat("dd/MM/yyyy");

		String startDate = sqlFormatter.format(calInicio.getTime());
		String endDate = sqlFormatter.format(calFim.getTime());

		ArrayList<String> daysBlocked = new ArrayList<String>();

		// get disponibilidade
		HashMap<String, String> disponibilidadeMes = atendimentoProfissionalDAO
		        .getDisponibilidadeProfissional(idProfissional, idServico,
		                idEspecialidade, idUnidade, idSubEspecialidade,
		                inTipoConsulta, startDate, endDate, null);

		List<Integer> diasDaSemanaAtendimento = atendimentoProfissionalDAO
		        .getDiasDaSemanaAtendimentoProfissional(idProfissional,idServico,
		                idEspecialidade, idUnidade, idSubEspecialidade,
		                inTipoConsulta);

		SearchContainer<Date, IntervaloBloqueadoDTO> bloqueioDoMes = getDiasBloqueadosDoMes(
		        idProfissional, calInicio.getTime(), calFim.getTime(), idUnidade);
		
		List<Integer> diaSemanaProducao =  new ArrayList<Integer>();
		diaSemanaProducao = getDiaSemanaTipoSalario(idProfissional, idServico, idEspecialidade,
				idUnidade, inTipoConsulta, true);

		Boolean boPossuiVagasFixo =  validaTipoAtendimento(idProfissional, idServico, idEspecialidade, 
				idUnidade, inTipoConsulta, bloqueioDoMes);
		
		Calendar nowTime = Calendar.getInstance();
		
		nowTime.set(Calendar.HOUR_OF_DAY, 0);
		nowTime.set(Calendar.MINUTE, 0);
		nowTime.set(Calendar.SECOND, 0);
		nowTime.set(Calendar.MILLISECOND, 0);

		do {

			startDate = sqlFormatter.format(calInicio.getTime());
			Integer dayofweek = calInicio.get(Calendar.DAY_OF_WEEK) - 1;			
			
			String disponibilidade = disponibilidadeMes.get(startDate);
			
			/*
			 * Verifica se o dia é apenas horario de produção
			 */
			Boolean boApenasProducao = validaDiaApenasProducao(idProfissional, idServico, idEspecialidade, 
					idUnidade, idSubEspecialidade, dayofweek, inTipoConsulta);
			/*
			 * Se o dia for menor ou igual ao dia atual 
			 * ou o profissional não atender no dia da semana, irá bloquear.
			 */
			
//			System.out.println("boApenasProducao___"+boApenasProducao);
//			System.out.println("diaSemanaProducao___"+diaSemanaProducao.toString());
//			System.out.println("inTipo___"+inTipo);
//			System.out.println("StaticValuesGlobal.QDTDIASATENDIMENTOFIXO___"+StaticValuesGlobal.QDTDIASATENDIMENTOFIXO);
//			System.out.println("boPossuiVagasFixo___"+boPossuiVagasFixo);
			
			if(DateUtil.isDataMaior(nowTime.getTime(), calInicio.getTime()) || !diasDaSemanaAtendimento.contains(dayofweek)){
				daysBlocked.add(angularFormatter.format(calInicio.getTime()));
			}
			/*
			 * Se não for encaixe e o dia estiver no intervalo estipulado (QDTDIASATENDIMENTOFIXO) e 
			 * ainda haver vagas não produção e o dia ser apenas produção, irá bloquear.
			 */
			else if (!inTipo.equals(1) && DateUtil.calculaPeriodoEmDias(nowTime.getTime(), calInicio.getTime()) <= StaticValuesGlobal.QDTDIASATENDIMENTOFIXO

					&& boPossuiVagasFixo && diaSemanaProducao.contains(dayofweek) && boApenasProducao) {	

				daysBlocked.add(angularFormatter.format(calInicio.getTime()));

			}
			/*
			 * Se o dia estiver no bloqueio do mes, irá bloquear 
			 */
			else if (bloqueioDoMes.contains(calInicio.getTime())) {	

				daysBlocked.add(angularFormatter.format(calInicio.getTime()));

			}
			/*
			 * Se for igual a encaixe e não for igual a data atual, irá bloquear.
			 */
			else if (inTipo.equals(1) && !DateUtil.isIgual(nowTime.getTime(), calInicio.getTime())) {	

				daysBlocked.add(angularFormatter.format(calInicio.getTime()));

			}
			/*
			 * Verifica se ainda ha vaga para o dia 
			 */
			else if (disponibilidade != null && !inTipo.equals(1)) {
				/*
				 * [0] Qtd de agendamentos para o dia
				 * [1] Qtd de pacientes a atender no dia
				 * [2] Se ainda possui vaga nesse dia  (0 - não possui / 1 - ainda possui)
				 */
				String[] split = disponibilidade.split("/");

				if ((Integer.parseInt(split[0]) >= Integer.parseInt(split[1]) || Integer.parseInt(split[2]) == new Integer(0) )) {
					daysBlocked
					        .add(angularFormatter.format(calInicio.getTime()));
				}
			} 

			calInicio.add(Calendar.DAY_OF_YEAR, 1);
		} while (calInicio.getTime().before(calFim.getTime()));
		
		
		//preenche os dias bloqueados apartir do ultimo agendamento faltoso para o perfil telemarketing
		if(hasRole){
			
			Date dtResult = agendamentoDAO.getUltimoAgendamentoFaltoso(idPaciente, tipoPaciente);
			if (dtResult != null){
				
			
					Calendar dtresultFirst = Calendar.getInstance();
					dtresultFirst.setTime(dtResult);
					dtresultFirst.set(Calendar.HOUR_OF_DAY, 0);
					dtresultFirst.set(Calendar.MINUTE, 0);
					dtresultFirst.set(Calendar.SECOND, 0);
					dtresultFirst.set(Calendar.MILLISECOND, 0);
					
					Calendar dtresultLast = Calendar.getInstance();
					dtresultLast.setTime(dtResult);
					dtresultLast.set(Calendar.HOUR_OF_DAY, 0);
					dtresultLast.set(Calendar.MINUTE, 0);
					dtresultLast.set(Calendar.SECOND, 0);
					dtresultLast.set(Calendar.MILLISECOND, 0);
					dtresultLast.add(Calendar.DAY_OF_YEAR,30);
					
					do {
						if(!daysBlocked.contains(angularFormatter.format(dtresultFirst.getTime()))){
							daysBlocked
					        .add(angularFormatter.format(dtresultFirst.getTime()));
						}
						
						dtresultFirst.add(Calendar.DAY_OF_YEAR,1);
						
					} while (dtresultFirst.getTime().before(dtresultLast.getTime()));
			}
		}

		return daysBlocked;
	}
	
	
	/*
	 * Funçao para obter os dias não bloqueados do intervalo 
	 * determinado na classe StaticValuesGlobal
	 */
	private List<String> getDiasIntervaloNaoBloqueadoFixo(SearchContainer<Date, IntervaloBloqueadoDTO> bloqueioDoMes,
			Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade,
	        Integer inTipoConsulta, List<Integer> diasSemana){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(diasSemana == null || diasSemana.size() == 0){
			diasSemana = getDiaSemanaTipoSalario(idProfissional, idServico, idEspecialidade,
					idUnidade, inTipoConsulta, false);
		}
		
		List<String> diasFormatado = new ArrayList<String>();
		
		if(diasSemana != null && diasSemana.size() > 0 ){
			
			
			List<Date> listDataAtendimentoFixo = gerarDias(diasSemana);
			
			if(listDataAtendimentoFixo != null && listDataAtendimentoFixo.size() > 0){
				for (Date date : listDataAtendimentoFixo) {
					if(bloqueioDoMes == null ){
						diasFormatado.add("'"+sdf.format(date)+"'");
					}else if(!bloqueioDoMes.contains(date)){
						diasFormatado.add("'"+sdf.format(date)+"'");
					}
				}
			}
			
			
		}
		
		
		
		return diasFormatado;
		
	}
	
	private List<Date> gerarDias(List<Integer> diasSemana){
				
		return DateUtil.getDiasDaSemana(StaticValuesGlobal.QDTDIASATENDIMENTOFIXO, diasSemana);
	}
	
	
	/*
	 * Função para verificar se ainda tem vaga
	 * no periodo estipulado
	 */
	private Boolean validaTipoAtendimento(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade, Integer inTipoConsulta, 
	        SearchContainer<Date, IntervaloBloqueadoDTO> bloqueioDoMes){
		
		List<AtendimentoProfissionalView> viewAP = atendimentoProfissionalDAO
				.getAtendimentoProfissionalVWByDiaSemana(idProfissional, idServico, idEspecialidade, 
						idUnidade, inTipoConsulta, null, false);
		
		
		
		
		if(viewAP != null && viewAP.size() > 0){
			for (AtendimentoProfissionalView ap : viewAP) {
				
				if(ap.getBoProducao() == null || !ap.getBoProducao().booleanValue()){
					
					List<Integer> diaSemana = new ArrayList<Integer>();
					diaSemana.add(ap.getInDiaSemana() + 1);
					List<String> diasAtendimento = getDiasIntervaloNaoBloqueadoFixo(bloqueioDoMes, idProfissional, 
							idServico, idEspecialidade, idUnidade, inTipoConsulta, diaSemana);
					
					
					if(diasAtendimento != null && diasAtendimento.size() > 0){
						Integer qtdAgendamento = agendamentoDAO.getQtdAgendamentosAtendProf(ap,diasAtendimento);
						if(ap.getQtPacienteAtender() != null && (ap.getQtPacienteAtender() * diasAtendimento.size()) > qtdAgendamento){
							return true;
						}
					}
				}
			}
		}
		
		
		
		return false;
		
	}
	
	/*
	 * retorna os dias da semana o medico atende 
	 */
	private List<Integer> getDiaSemanaTipoSalario(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade,
	        Integer inTipoConsulta, Boolean boProducao){
		
		List<Integer> result = new ArrayList<Integer>();
		
		List<AtendimentoProfissionalView> viewAP = atendimentoProfissionalDAO
				.getAtendimentoProfissionalVWByDiaSemana(idProfissional, idServico, idEspecialidade, 
						idUnidade, inTipoConsulta, null, boProducao);
		
		for (AtendimentoProfissionalView ap : viewAP) {
			
			if(!result.contains(ap.getInDiaSemana())){
				result.add(ap.getInDiaSemana());
			}
			
			
		}
		
		return result;
		
	}
	
	private Boolean validaDiaApenasProducao(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade, Long idSubEspecialidade, Integer inDiaSemana,
	        Integer inTipoConsulta){
		
		
		List<AtendimentoProfissionalView> viewAP = atendimentoProfissionalDAO
				.getAtendimentoProfissionalVWByDiaSemana(idProfissional, idServico, idEspecialidade, 
						idUnidade, inTipoConsulta, inDiaSemana, null);
		
		if(viewAP == null || viewAP.size() == 0){
			return false;
		}
		
		for (AtendimentoProfissionalView ap : viewAP) {
			if(ap.getBoProducao() == null || !ap.getBoProducao().booleanValue()){
				return false;
			}
		}
		
		return true;
		
		
		
	}

	private SearchContainer<Date, IntervaloBloqueadoDTO> getDiasBloqueadosDoMes(
	        Long idProfissional, Date inicio, Date fim, Long idUnidade) {

		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");

		String startDate = sqlFormatter.format(inicio);
		String endDate = sqlFormatter.format(fim);

		ArrayList<IntervaloBloqueadoDTO> result = new ArrayList<IntervaloBloqueadoDTO>(
		        funcionarioDAO.getDiasBloqueadosDoMes(idProfissional,
		                startDate, endDate, idUnidade));

		SearchContainer<Date, IntervaloBloqueadoDTO> sc = new SearchContainer<Date, IntervaloBloqueadoDTO>(
		        result) {

			@Override
			public boolean contains(Date key) {

				for (IntervaloBloqueadoDTO ib : result) {
					if (key.after(ib.getDtinicio())
					        && key.before(ib.getDtfim())) {
						return true;
					}

					if (key.equals(ib.getDtinicio())) {
						return true;
					}

					if (key.equals(ib.getDtfim())) {
						return true;
					}

				}
				return false;
			}

		};

		return sc;
	}
	
	private List<Long> getIdsProducaoAtendimentoMedico(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idUnidade, Long idSubEspecialidade, Integer inDiaSemana,
	        Integer inTipoConsulta){
		
		List<Long> listIds = new ArrayList<Long>();
		
		List<AtendimentoProfissionalView> viewAP = atendimentoProfissionalDAO
				.getAtendimentoProfissionalVWByDiaSemana(idProfissional, idServico, idEspecialidade, 
						idUnidade, inTipoConsulta, inDiaSemana, new Boolean(true));
		
		if(viewAP != null && viewAP.size() > 0){
			for (AtendimentoProfissionalView ap : viewAP) {
				
				listIds.add(ap.getIdAtendimentoProfissional());
			}
		}
		
		return listIds;
		
		
	}
	

	public List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>> getHorarioAtendimentoProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade,
	        Long idSubEspecialidade, Integer inTipoConsulta,
	        Integer inTipoAgendamento, String date, Boolean boParticular, Boolean bovalidaHorarioFull) {
		
		if(inTipoAgendamento == null){
			inTipoAgendamento = new Integer(0);
		}

		Calendar nowTime = Calendar.getInstance();
		
		nowTime.set(Calendar.HOUR_OF_DAY, 0);
		nowTime.set(Calendar.MINUTE, 0);
		nowTime.set(Calendar.SECOND, 0);
		nowTime.set(Calendar.MILLISECOND, 0);

		List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>> result = new ArrayList<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>>();
		Locale locale = new Locale("pt", "BR");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy", locale);
		// DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		DateFormat dfH = new SimpleDateFormat("HH:mm");
		DateFormat dfHz = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		dfHz.setTimeZone(TimeZone.getTimeZone("America/Manaus"));
		
		List<String> turnoBloqueados = new ArrayList<String>();
		SimpleDateFormat dfb = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cl = Calendar.getInstance();
		try {
			cl.setTime(df.parse(date));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Bloqueio> bloqueios = funcionarioDAO.getBloqueioDoMes(idProfissional, dfb.format(cl.getTime()), idUnidade);

		if(bloqueios != null){
			for (Bloqueio bloqueio : bloqueios) {
				
				if(bloqueio.getInTurno().equals(0)){
					turnoBloqueados.add("MANHA");
				}else if(bloqueio.getInTurno().equals(1)){
					turnoBloqueados.add("TARDE");
				}else{
					turnoBloqueados.add("NOITE");
				}
				
			}
			
		}

		Calendar calInicio = Calendar.getInstance();
	

		try {
			calInicio.setTime(df.parse(date));

		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
		
		Integer dayofweek = calInicio.get(Calendar.DAY_OF_WEEK) - 1;

		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");

		List<String> consultasMarcadas = atendimentoProfissionalDAO
		        .getHorariosConsultasMarcadasNoDia(idProfissional,
		                idEspecialidade, idUnidade, idSubEspecialidade,
		                inTipoConsulta,
		                sqlFormatter.format(calInicio.getTime()), boParticular);
		
		Calendar dataFim = Calendar.getInstance();
		dataFim.add(Calendar.DAY_OF_MONTH, StaticValuesGlobal.QDTDIASATENDIMENTOFIXO);
		
		SearchContainer<Date, IntervaloBloqueadoDTO> bloqueioDoMes = getDiasBloqueadosDoMes(
		        idProfissional, new Date(), dataFim.getTime(), idUnidade);
		
		
		Boolean boVagasFixo = validaTipoAtendimento(idProfissional, idServico, idEspecialidade, 
				idUnidade, inTipoConsulta, bloqueioDoMes);
		Boolean boIntervaloDias = DateUtil.calculaPeriodoEmDias(nowTime.getTime(), calInicio.getTime()) <= StaticValuesGlobal.QDTDIASATENDIMENTOFIXO;
		List<Long> listIds = new ArrayList<Long>();
		
		if(boVagasFixo && boIntervaloDias && !inTipoAgendamento.equals(1)){

			listIds = getIdsProducaoAtendimentoMedico(idProfissional, idServico, idEspecialidade, idUnidade, idSubEspecialidade, dayofweek, inTipoConsulta);
		}
		
		List<AtendimentoProfissionalDTO> calendarioSemanal = null;

		try {
			
			if(bovalidaHorarioFull != null && !df.format(new Date()).equals(date)){
				bovalidaHorarioFull = new Boolean(false);
			}
			calendarioSemanal = atendimentoProfissionalDAO
			        .getCalendarioSemanalProfissional(idProfissional, idServico,
			                idEspecialidade, idUnidade, idSubEspecialidade,
			                inTipoConsulta, boParticular, bovalidaHorarioFull);
		} catch (ObjectNotFoundException e) {
			logger.error(
			        "Profissional nao atende com esse tipo de atendimento", e);
			throw new AgendamentoException(
			        props.getProperty("agendamento.profissionalnaoatende"),
			        ErrorType.DANGER);
		}
		
		

		

		if (inTipoConsulta == 0) { // hora marcada
			for (AtendimentoProfissionalDTO atp : calendarioSemanal) {
				if (atp.getInDiaSemana() == dayofweek && !turnoBloqueados.contains(atp.getNmTurno()) && !listIds.contains(atp.getIdAtendimentoProfissional())) {

					if (inTipoAgendamento == 0) {

						String[] startHM = atp.getNmHrInicio().split(":");
						String[] endHM = atp.getNmHrFim().split(":");
						String[] shiftHM = atp.getNmHrTempoConsulta()
						        .split(":");
						Integer shiftHour = Integer.parseInt(shiftHM[0]);
						Integer shiftMinute = Integer.parseInt(shiftHM[1]);
						Integer dayLimit = atp.getQtPacienteAtender();

						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(startHM[0]));
						cal.set(Calendar.MINUTE, Integer.parseInt(startHM[1]));
						cal.set(Calendar.SECOND, 0);
						int startHour = cal.get(Calendar.HOUR_OF_DAY) * 60;
						startHour += cal.get(Calendar.MINUTE);

						Calendar calEnd = Calendar.getInstance();
						calEnd.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(endHM[0]));
						calEnd.set(Calendar.MINUTE, Integer.parseInt(endHM[1]));
						calEnd.set(Calendar.SECOND, 0);
						int endHour = calEnd.get(Calendar.HOUR_OF_DAY) * 60;
						endHour += calEnd.get(Calendar.MINUTE);

						int i = 0;
						while ((startHour < endHour) && (i < dayLimit)) {
							String generatedTimeId = dfHz.format(cal.getTime());
							String generatedTimeDescription = dfH.format(cal
							        .getTime());

							if (!consultasMarcadas
							        .contains(generatedTimeDescription)) {
								result.add(new SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>(
								        generatedTimeId,
								        generatedTimeDescription, atp.getBoParticular(), atp.getIdAtendimentoProfissional(),
								        atp.getBoAceitaEncaixe()));
							}

							cal.add(Calendar.HOUR, shiftHour);
							cal.add(Calendar.MINUTE, shiftMinute);
							startHour = cal.get(Calendar.HOUR_OF_DAY) * 60;
							startHour += cal.get(Calendar.MINUTE);
							i++;
						}
					} else {
						String[] endHM = atp.getNmHrFim().split(":");
						Calendar calEnd = Calendar.getInstance();
						calEnd.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(endHM[0]));
						calEnd.set(Calendar.MINUTE, Integer.parseInt(endHM[1]));
						calEnd.set(Calendar.SECOND, 0);
						result.add(new SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>(dfHz
						        .format(calEnd.getTime()), dfH.format(calEnd
						        .getTime()), atp.getBoParticular(), atp.getIdAtendimentoProfissional(),
						        atp.getBoAceitaEncaixe()));
					}
				}
			}
		} else { // ordem de chegada
			for (AtendimentoProfissionalDTO atp : calendarioSemanal) {
				if (atp.getInDiaSemana() == dayofweek && !turnoBloqueados.contains(atp.getNmTurno()) && !listIds.contains(atp.getIdAtendimentoProfissional())) {

					if (inTipoAgendamento == 0) {

						String[] startHM = atp.getNmHrInicio().split(":");
						String[] endHM = atp.getNmHrFim().split(":");
						String[] shiftHM = atp.getNmHrTempoConsulta()
						        .split(":");
						Integer shiftHour = Integer.parseInt(shiftHM[0]);
						Integer shiftMinute = Integer.parseInt(shiftHM[1]);
						Integer dayLimit = atp.getQtPacienteAtender();

						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(startHM[0]));
						cal.set(Calendar.MINUTE, Integer.parseInt(startHM[1]));
						cal.set(Calendar.SECOND, 0);
						int startHour = cal.get(Calendar.HOUR_OF_DAY) * 60;
						startHour += cal.get(Calendar.MINUTE);

						Calendar calEnd = Calendar.getInstance();
						calEnd.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(endHM[0]));
						calEnd.set(Calendar.MINUTE, Integer.parseInt(endHM[1]));
						calEnd.set(Calendar.SECOND, 0);
						int endHour = calEnd.get(Calendar.HOUR_OF_DAY) * 60;
						endHour += calEnd.get(Calendar.MINUTE);

						int i = 0;
						while ((startHour < endHour) && (i < dayLimit)) {
							String generatedTimeId = dfHz.format(cal.getTime());
							String generatedTimeDescription = dfH.format(cal
							        .getTime());

							if (!consultasMarcadas
							        .contains(generatedTimeDescription)) {
								result.add(new SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>(
								        generatedTimeId, "Sequencia: "
								                + (i + 1)
								                + " - Hora Prevista: "
								                + generatedTimeDescription, atp.getBoParticular(), atp.getIdAtendimentoProfissional(),
										        atp.getBoAceitaEncaixe()));
								return result;
							}
							
							cal.add(Calendar.HOUR, shiftHour);
							cal.add(Calendar.MINUTE, shiftMinute);
							startHour = cal.get(Calendar.HOUR_OF_DAY) * 60;
							startHour += cal.get(Calendar.MINUTE);
							i++;
						}
					} else {
						String[] endHM = atp.getNmHrFim().split(":");
						Calendar calEnd = Calendar.getInstance();
						calEnd.set(Calendar.HOUR_OF_DAY,
						        Integer.parseInt(endHM[0]));
						calEnd.set(Calendar.MINUTE, Integer.parseInt(endHM[1]));
						calEnd.set(Calendar.SECOND, 0);
						result.add(new SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>(dfHz
						        .format(calEnd.getTime()), dfH.format(calEnd
						        .getTime()), atp.getBoParticular(), atp.getIdAtendimentoProfissional(),
						        atp.getBoAceitaEncaixe()));
					}
				}
			}
		}

		if (result.size() == 0) {
			throw new AgendamentoException(
			        props.getProperty("agendamento.naoatende"),
			        ErrorType.DANGER);
		}

		return result;
	}
	
	public Collection<Profissional> getProfissionaisPorUnidade(Long idUnidade){
		return atendimentoProfissionalDAO. getProfissionaisPorUnidade(idUnidade);
	}
		
	/**
	 * @author Joelton
	 * @since 05-05-2016
	 * @return funcionarios ativos
	 */
	public List<Funcionario> getListaFuncionario(Integer inGrupoFuncionario) {
		
		return funcionarioDAO.getListaFuncionario(inGrupoFuncionario);
	}
	
	public List<Funcionario> getListaFuncionarioInativo() {
		
		return funcionarioDAO.getListaFuncionarioInativo();
	}
		
	/**
	 * @author Joelton 
	 * @since 02/05/2016
	 * @param input - request with files content
	 * @param idFuncionario - employee id
	 */ 
 	public void importaArquivosFuncionario( MultipartFormDataInput input, Long idFuncionario ) {
		
		String fileName = "";	
		String fileType = "";
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		
		Funcionario funcionario = new Funcionario();
		funcionario = funcionarioDAO.searchByKey(Funcionario.class, idFuncionario );
		
		if( funcionario != null ) {	
							
			for(int i=0; i<uploadForm.size(); i++){	
										
				List<InputPart> inputParts = uploadForm.get("file["+i+"]");
				String subDiretorio = "";
				String diretorio =  getFullPathFile() + funcionario.getId();
				String separador = getSeparador();
				
				for (InputPart inputPart : inputParts) {		
					
					try {
						
						MultivaluedMap<String, String> header = inputPart.getHeaders();
						
						fileName = UploadFileUtil.getFileName(header);		
						fileType = UploadFileUtil.getFileType(header);
						
						InputStream inputStream = inputPart.getBody(InputStream.class,null);					
						
						byte[] bytes = IOUtils.toByteArray(inputStream);
						String fileNameClean = fileName;
						
						subDiretorio = fileType.toLowerCase().trim().equals("pdf") ? separador + "documentos" : separador + "imagens";
						diretorio = diretorio + subDiretorio ;
						fileName = diretorio + separador + fileName;		
						
						boolean isSuccess = UploadFileUtil.writeFile( bytes,fileName, diretorio );		
						if( isSuccess ) {
							
							String urlRelativa = funcionario.getId() + subDiretorio + separador + fileNameClean; 
							
							ArquivoFuncionario arquivo = new ArquivoFuncionario();
							arquivo.setFuncionario(funcionario);
							arquivo.setDataUpload( new Date() );
							arquivo.setExtensao( fileType );
														
							fileNameClean = UploadFileUtil.retirarExtensao( fileNameClean );
							arquivo.setNome( fileNameClean );
							arquivo.setUrl( urlRelativa );
							arquivoFuncionarioDAO.persist(arquivo);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}		
				}
			}	
		}
	}	

	/**
	 * @author Joelton
	 * @since 03/05/2016
	 * @param idFuncionario
	 * @return ArquivosFuncionarioDTO
	 */
	public ArquivosFuncionarioDTO getArquivosFuncionario( Long idFuncionario ) {
		
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, idFuncionario);
		
		String query = " FROM ArquivoFuncionario file WHERE file.funcionario.id = ?1 ORDER BY file.nome ";
		Collection<?> arquivos = arquivoFuncionarioDAO.findByQuery( query, idFuncionario );
		
		ArquivosFuncionarioDTO arquivosFuncionario = new ArquivosFuncionarioDTO();
		arquivosFuncionario.setFuncionario( funcionario );
		arquivosFuncionario.setArquivos( arquivos );
		
		return arquivosFuncionario;
		
		
	}
	
	public List<EspecialidadeDTO>  getEspecialidadesProfissional( Long idFuncionario ) {
		
		return funcionarioDAO.getEspecialidadesProfissional(idFuncionario);
	}
	
	
	/**
	 * @author Joelton
	 * @since 03-05-2016
	 * @param idArquivo
	 * @return Employee File
	 */
	public File getArquivoFuncionario(Long idArquivo) {
		
		return new File(getFullPathFileName(idArquivo));
		
	}

	/**
	 * @author Joelton
	 * @since 03-05-2016
	 * @param id - file's id
	 * @return file's path
	 */
	private String getFullPathFileName(Long id) {
	
		ArquivoFuncionario arquivoFuncionario = arquivoFuncionarioDAO.searchByKey(ArquivoFuncionario.class, id);

		return arquivoFuncionario.getUrl();
		
	}

	/** 
	 * 
	* @author Joelton
	* 
	* @since 03-05-2016
	* 
	* @return system path to put file employee
	*/
	private String getFullPathFile() {

		ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
		
		return configuraSistema.getNmPathArquivoFuncionario();
	}
	
	/**
	 * @author Joelton
	 * @since 07-05-2016
	 * @return separator
	 */
	private String getSeparador() {
		
		ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
		
		return configuraSistema.getNmSeparador();
	}
	
	
	
	public Funcionario getFuncionarioPorLogin(String loginFuncionario ) {
		return funcionarioDAO.getFuncionarioPorLogin(loginFuncionario);
		
	}
	
	public List<ServicoExame> getServicoExame(Long idEspecialidade ) {
		
		return  funcionarioDAO.getAtendimentomedicoservicosexameView(idEspecialidade);
		
		
	}
	
	public Collection<Profissional> getProfissionaisPorEspecialidadeTriagem(List<Long> idEspecialidade, Long idsubEspecialidade, Long idUnidade) {
		logger.debug("Obtendo subEspecialidades de um profissional...");
		return funcionarioDAO.getProfissionaisPorEspecialidadeTriagem(idEspecialidade, idsubEspecialidade, idUnidade);
	}
	
	public List<AtividadeAgendamentoDTO> getAtividadeAgendamento(String dtInicio, String dtFim ) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date dateInicio = null;
		Date dateFim = null;
		
		 List<DayOfWeek> ignore = Arrays.asList(DayOfWeek.SUNDAY);
		
		Calendar calendar = Calendar.getInstance();
		
		try {
			dateInicio = format.parse(dtInicio);
			dateFim = format.parse(dtFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
		
		calendar.setTime(dateInicio);
		
		final LocalDate start = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH )+ 1, calendar.get(Calendar.DAY_OF_MONTH));
		calendar.setTime(dateFim);
        final LocalDate end = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
		
//        System.out.println(simpleDaysBetween(start, end));
        int diasUteis = betterDaysBetween(start, end, ignore);
//        System.out.println(bestDaysBetween(start, end, ignore));
		
        List<AtividadeAgendamentoDTO> listAta =  funcionarioDAO.getAtividadeAgendamento(dateInicio, dateFim );
        
        if(listAta != null && listAta.size() > 0){
        	return mediaAtividadeAgendamento(listAta,diasUteis,dateInicio,dateFim);
        	
        }else{
        	return null;
        }
		
		
	}
	
	public List<AtividadeAgendamentoDTO> getMedicosAtividadeAtendimento(String dtInicio, String dtFim ) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date dateInicio = null;
		Date dateFim = null;
			
		try {
			dateInicio = format.parse(dtInicio);
			dateFim = format.parse(dtFim);
		} catch (ParseException e) {
			throw new WebApplicationException("Erro no formato da data");
		}
				
        List<AtividadeAgendamentoDTO> listAta =  funcionarioDAO.getMedicosAtividadeAtendimento(dateInicio, dateFim );
		
        if(listAta != null && listAta.size() > 0)
        	return listAta;
        else
        	return null;
	}
	
	private List<AtividadeAgendamentoDTO> mediaAtividadeAgendamento(List<AtividadeAgendamentoDTO> ata, int diasUteis, Date inicio, Date fim){
		
		List<AtividadeAgendamentoDTO> ataAux = new ArrayList<AtividadeAgendamentoDTO>();
		HashMap<Long, Integer> atvidadesUsuarios = mongo.getAtividadesUsuarios(inicio, fim);
		
		for (AtividadeAgendamentoDTO atividadeAgendamentoDTO : ata) {
			atividadeAgendamentoDTO.setMedia(new Double(atividadeAgendamentoDTO.getQtdAgendamento() / diasUteis));
			atividadeAgendamentoDTO.setQtdAtividade(atvidadesUsuarios.get(atividadeAgendamentoDTO.getIdFuncionario()) != null ? atvidadesUsuarios.get(atividadeAgendamentoDTO.getIdFuncionario()) : new Integer(0));
			ataAux.add(atividadeAgendamentoDTO);
		}
	
		return ataAux;
		
	}
	
	
    public  int simpleDaysBetween(final LocalDate start,
            final LocalDate end) {
        return (int) ChronoUnit.DAYS.between(start, end);
    }
    
    public int betterDaysBetween(final LocalDate start,
            final LocalDate end, final List<DayOfWeek> ignore) {
        int count = 1;
        LocalDate curr = start.plusDays(0);

        while (curr.isBefore(end)) {
            if (!ignore.contains(curr.getDayOfWeek())) {
                count++;
            }
            curr = curr.plusDays(1); // Increment by a day.
        }

        return count;
    }
    
    public  int bestDaysBetween(final LocalDate start,
            final LocalDate end, final List<DayOfWeek> ignore) {
        int days = simpleDaysBetween(start, end);

        if (days == 0) {
            return 0;
        }

        if (!ignore.isEmpty()) {
            int weeks = days / 7;
            int startDay = start.getDayOfWeek().getValue();
            int endDay = end.getDayOfWeek().getValue();
            int diff = weeks * ignore.size();

            for (DayOfWeek day : ignore) {
                int currDay = day.getValue();
                if (startDay <= currDay) {
                    diff++;
                }
                if (endDay > currDay) {
                    diff++;
                }
            }

            if (endDay > startDay) {
                diff -= endDay - startDay;
            }

            return days - diff;
        }

        return days;
    }
    
	public Funcionario updateAutorizacaoTotem(Long idFuncionario, Boolean autorizacao, String digitalFuncionario, Integer idDedoDigital) {
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, idFuncionario);
		
		if(autorizacao != null)
			funcionario.setBoAutorizacaoTotem(autorizacao);
		
		if(digitalFuncionario != null)
			funcionario.setDigitalFuncionario(digitalFuncionario);
		
		if(idDedoDigital != null)
			funcionario.setIdDedoDigital(idDedoDigital);

		return  funcionarioDAO.update(funcionario);
	}

	public Funcionario updateAutorizacaoTotem(Long idFuncionario, Boolean autorizacao ) {
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, idFuncionario);
		funcionario.setBoAutorizacaoTotem(autorizacao);
		
		return  funcionarioDAO.update(funcionario);
					
		}
	
	public Funcionario getFuncionarioById(Long idFuncionario) {
		return funcionarioDAO.searchByKey(Funcionario.class, idFuncionario);
	}

	public Funcionario updateFuncionario(Funcionario funcionario) {
		funcionario.setNrCep(Utils.removeSpecialChars(funcionario.getNrCep()));
		funcionario.setNrCelular(Utils.removeSpecialChars(funcionario.getNrCelular()));
		funcionario.setNrTelefone(Utils.removeSpecialChars(funcionario.getNrTelefone()));
		funcionario.setNrCpf(Utils.removeSpecialChars(funcionario.getNrCpf()));
		funcionario.setNrRg(Utils.removeSpecialChars(funcionario.getNrRg()));
		
		funcionario.setDtAlteracao(new Date());
		funcionario.setIdOperadorAlteracao(securityService.getFuncionarioLogado().getId());		
		
		funcionario = funcionarioDAO.update(funcionario);
		
		return funcionario;
	}

	public Funcionario saveFuncionario(Funcionario funcionario) {
		funcionario.setNrCep(Utils.removeSpecialChars(funcionario.getNrCep()));
		funcionario.setNrCelular(Utils.removeSpecialChars(funcionario.getNrCelular()));
		funcionario.setNrTelefone(Utils.removeSpecialChars(funcionario.getNrTelefone()));
		funcionario.setNrCpf(Utils.removeSpecialChars(funcionario.getNrCpf()));
		funcionario.setNrRg(Utils.removeSpecialChars(funcionario.getNrRg()));

		Unidade unidade = unidadeDAO.searchByKey(Unidade.class, funcionario.getUnidade().getId());
		funcionario.setUnidade(unidade);
		
		Departamento departamento = unidadeDAO.getDepartamentoByUnidade(unidade.getId());
		funcionario.setDepartamento(departamento);
		
		Cidade cidade = cidadeDAO.searchByKey(Cidade.class, funcionario.getCidade().getId());
		funcionario.setCidade(cidade);
		
		Bairro bairro = bairroDAO.searchByKey(Bairro.class, funcionario.getBairro().getId());
		funcionario.setBairro(bairro);
		
		funcionario.setDtCadastro(new Date());
		funcionario.setIdOperadorCadastro(securityService.getFuncionarioLogado().getId());
				
		funcionarioDAO.persist(funcionario);

		return funcionario;
	}

	public void inativarFuncionario(Long id) {
		
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, id);
		funcionario.setDtExclusao(new Date());
		funcionario.setIdOperadorExclusao(securityService.getFuncionarioLogado().getId());
		
	}

	public void ativarFuncionario(Long id) {
		
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, id);
		funcionario.setDtExclusao(null);
		funcionario.setIdOperadorExclusao(null);
		funcionario.setIdOperadorAlteracao(securityService.getFuncionarioLogado().getId());
		funcionario.setDtAlteracao(new Date());
		
	}
	private void registrarHora(RegistroChamadaProfissional registro) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("America/Manaus"));
        calendar.set(Calendar.MILLISECOND,0);
        registro.setHoraChamada(new Time( calendar.getTime().getTime() ));
    }
	
	private boolean verificaRegistroExistente(Long idUnidade, Long idAgendamento){
		Funcionario funcionario = securityService.getFuncionarioLogado();
		
		List<RegistroChamadaProfissional> registros = registroChamadaDAO.
				verificaRegistroExistente(funcionario.getId(), idAgendamento, idUnidade);
		
		if(registros != null && registros.size() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	public RegistroChamadaProfissional RegistrarChamadaProfissional(Long idUnidade, Long idAgendamento){
		
		if(!verificaRegistroExistente(idUnidade, idAgendamento)){
			Agendamento agendamento = agendamentoService.getAgendamento(idAgendamento);
			Unidade unidade = unidadeDAO.searchByKey(Unidade.class, idUnidade);
			Funcionario funcionario = securityService.getFuncionarioLogado();
			
			RegistroChamadaProfissional registro  = new RegistroChamadaProfissional();
			registro.setAgendamento(agendamento);
			registro.setUnidade(unidade);
			registro.setFuncionario(funcionario);
			registro.setDataChamada(new Date());
			registrarHora(registro);
			
			return registroChamadaDAO.persist(registro);
		}
		
		return null;
		
	}
	
	public AppResponse getListaDatasNaoAtende(Long idProfissional, Long idServico,
	        Long idEspecialidade, Long idSubEspecialidade,
	        Integer inTipoConsulta, Long idUnidade, Integer ano, Integer mes) {
		
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		Integer inTipo = 0; // tipo agendamento padrão para app 
		String tipoPaciente;
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			tipoPaciente = "C";
		} else {
			tipoPaciente = "D";
		}
		List<String> result = getDiasSemAtendimentoProfissional(idProfissional,
				idServico, idEspecialidade, idUnidade, idSubEspecialidade,
				inTipoConsulta, ano, mes, inTipo, user.getIdUsuario(), tipoPaciente);
		return new SuccessResponse(200, result);
	}
	
	public Funcionario updateAssinaturaDigital(Long idFuncionario, String assinaturaDigital ) {
		Funcionario funcionario = funcionarioDAO.searchByKey(Funcionario.class, idFuncionario);
		funcionario.setAssinaturaDigital(assinaturaDigital);

		return  funcionarioDAO.update(funcionario);

	}
	
	public List<EspecialidadeDTO> getEspecialidadesByProfissional(Long idProfissional) {
		List<Especialidade> list = especialidadeDAO.getEspecialidadesByProfissional(idProfissional);
		if(list != null && !list.isEmpty()) {
			List<EspecialidadeDTO> result = new ArrayList<EspecialidadeDTO>();
			for(Especialidade e : list) {
				EspecialidadeDTO esp = new EspecialidadeDTO();
				esp.setIdEspecialidade(e.getId());
				esp.setNmEspecialidade(e.getNmEspecialidade());
				result.add(esp);
			}
			return result;
		}
		return null;
	}
	
	public List<GrupoFuncionario> getGruposFuncionarios() {
		return GrupoFuncionario.getGruposFunc();
	}
	
}
