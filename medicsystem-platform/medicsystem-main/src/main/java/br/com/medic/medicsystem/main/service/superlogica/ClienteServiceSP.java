package br.com.medic.medicsystem.main.service.superlogica;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONObject;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.webservice.superlogica.WebServiceCliente;
import br.com.medic.medicsystem.persistence.dao.ClienteCompartinhadoDentalDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteCompartinhadoPerformanceDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.superlogica.ClienteSP;
import br.com.medic.medicsystem.persistence.model.superlogica.Cobranca;





@Stateless
public class ClienteServiceSP {
	
	@Inject
	WebServiceCliente ws;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("clientecompartinhadodental-dao")
	private ClienteCompartinhadoDentalDAO clienteCompartinhadoDentalDAO;
	
	@Inject
	@Named("clientecompartinhadoperformance-dao")
	private ClienteCompartinhadoPerformanceDAO clienteCompartinhadoPerformanceDAO;
	
	
	private <E> E searchByKeyCliente (EmpresaGrupoCompartilhado empresa, Class<E> clazz, Long id){
		Integer baseDadosSistema = empresa.getInSistemaExterno();
		
		switch ( baseDadosSistema ) {
		case 0:
			return clienteDAO.searchByKey(clazz, id);
		case 1:
			return clienteCompartinhadoPerformanceDAO.searchByKey(clazz, id);
		case 2:
			return clienteCompartinhadoDentalDAO.searchByKey(clazz, id);
		default:
			return null;
		}
	}
	
	
	public List<JSONObject> cadastrarCliente(List<ClienteSP> clientes){
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		JSONObject json = null;
		Cliente clienteOriginal = null;
		for (ClienteSP cliente : clientes) {
			json = new JSONObject();
			json = validarDados(cliente);
			clienteOriginal = clienteDAO.searchByKey(Cliente.class, Long.parseLong(cliente.getIn_idCliente().toString()));
			if(json == null && clienteOriginal.getIdClienteBoleto_sl() == null){	
				json = ws.cadastrarCliente(cliente);
				if(json != null && json.getInt("status") == 200){
					clienteOriginal = clienteDAO.searchByKey(Cliente.class, Long.parseLong(cliente.getIn_idCliente().toString()));
					clienteOriginal.setIdClienteBoleto_sl(json.getJSONObject("data").getLong("id_sacado_sac"));
					clienteDAO.persist(clienteOriginal);
					jsons.add(json);
				}else{

					jsons.add(json);
				}
				
			}else{
				if(json == null){
					json = new JSONObject();
					json.put("data", new JSONObject());
					json.getJSONObject("data").put("id_sacado_sac", clienteOriginal.getIdClienteBoleto_sl().toString());
					json.put("in_CodCliente", cliente.getSt_CodCliente());
					json.put("in_idCliente", cliente.getIn_idCliente());
					json.put("st_nome_sac", cliente.getSt_nome_sac());
					json.put("idContrato", cliente.getIdContrato());
					json.put("status", "200");
					jsons.add(json);
				}else{
					jsons.add(json);
				}				
			}		
		}
		
		return jsons;
	}
	
	
	public List<JSONObject> cadastrarClienteCompartilhado(List<ClienteSP> clientes, EmpresaGrupoCompartilhado empresa){
		List<JSONObject> jsons = new ArrayList<JSONObject>();
		JSONObject json = null;
		Cliente clienteOriginal = null;
		for (ClienteSP cliente : clientes) {
			json = new JSONObject();
			json = validarDados(cliente);
			clienteOriginal = searchByKeyCliente(empresa , Cliente.class, Long.parseLong(cliente.getIn_idCliente().toString()));
			if(json == null && clienteOriginal.getIdClienteBoleto_sl() == null){	
				json = ws.cadastrarCliente(cliente);
				if(json != null && json.getInt("status") == 200){
					clienteOriginal.setIdClienteBoleto_sl(json.getJSONObject("data").getLong("id_sacado_sac"));
					if(empresa.getInSistemaExterno() == 0){
						clienteDAO.updateCliente(clienteOriginal);
					}else if(empresa.getInSistemaExterno() == 1){
						clienteCompartinhadoPerformanceDAO.updateCliente(clienteOriginal);
					}else if(empresa.getInSistemaExterno() == 2){
						clienteCompartinhadoDentalDAO.updateCliente(clienteOriginal);
					}else{
						throw new AgendamentoException("Não foi possível encontrar o cliente, empresa não reconhecida", ErrorType.DANGER);
					}
					
					jsons.add(json);
				}else{

					jsons.add(json);
				}
				
			}else{
				if(json == null){
					json = new JSONObject();
					json.put("data", new JSONObject());
					json.getJSONObject("data").put("id_sacado_sac", clienteOriginal.getIdClienteBoleto_sl().toString());
					json.put("in_CodCliente", cliente.getSt_CodCliente());
					json.put("in_idCliente", cliente.getIn_idCliente());
					json.put("st_nome_sac", cliente.getSt_nome_sac());
					json.put("idContrato", cliente.getIdContrato());
					json.put("status", "200");
					jsons.add(json);
				}else{
					jsons.add(json);
				}
				
				
			}
		
		}
		
		return jsons;
	}
	
	
	private JSONObject validarDados(ClienteSP cliente){
		
		JSONObject json = null;
		
		if(cliente.getSt_endereco_sac() == null || cliente.getSt_endereco_sac().trim().length() == 0){
			json = new JSONObject();
			json.put("in_CodCliente", cliente.getSt_CodCliente());
			json.put("in_idCliente", cliente.getIn_idCliente());
			json.put("st_nome_sac", cliente.getSt_nome_sac());
			json.put("idContrato", cliente.getIdContrato());
			json.put("status", "500");
			json.put("msg", "Invalido o campo endereço");
			
		}
		
		if(cliente.getSt_numero_sac() == null || cliente.getSt_numero_sac().trim().length() == 0){
			if(json != null){
				json.put("msg", json.getString("msg")+", o campo número da casa");
			}else{
				
				json = new JSONObject();
				json.put("in_CodCliente", cliente.getSt_CodCliente());
				json.put("in_idCliente", cliente.getIn_idCliente());
				json.put("st_nome_sac", cliente.getSt_nome_sac());
				json.put("idContrato", cliente.getIdContrato());
				json.put("status", "500");
				json.put("msg", "Invalido o campo número da casa");
				
			}
			
		} 
		if(cliente.getSt_bairro_sac() == null || cliente.getSt_bairro_sac().trim().length() == 0){
			
			if(json != null){
				json.put("msg", json.getString("msg")+", o campo bairro");
			}else{
				
				json = new JSONObject();
				json.put("in_CodCliente", cliente.getSt_CodCliente());
				json.put("in_idCliente", cliente.getIn_idCliente());
				json.put("st_nome_sac", cliente.getSt_nome_sac());
				json.put("idContrato", cliente.getIdContrato());
				json.put("status", "500");
				json.put("msg", "Invalido o campo bairro");
				
			}
			
		}
		if(cliente.getSt_cep_sac() == null || cliente.getSt_cep_sac().trim().length() == 0){
			
			if(json != null){
				json.put("msg", json.getString("msg")+", o campo CEP");
			}else{
				
				json = new JSONObject();
				json.put("in_CodCliente", cliente.getSt_CodCliente());
				json.put("in_idCliente", cliente.getIn_idCliente());
				json.put("st_nome_sac", cliente.getSt_nome_sac());
				json.put("idContrato", cliente.getIdContrato());
				json.put("status", "500");
				json.put("msg", "Invalido o campo CEP");
				
			}
			
		}
		if(cliente.getSt_cidade_sac()== null || cliente.getSt_cidade_sac().trim().length() == 0){
			
			if(json != null){
				json.put("msg", json.getString("msg")+", o campo cidade");
			}else{
				
				json = new JSONObject();
				json.put("in_CodCliente", cliente.getSt_CodCliente());
				json.put("in_idCliente", cliente.getIn_idCliente());
				json.put("st_nome_sac", cliente.getSt_nome_sac());
				json.put("idContrato", cliente.getIdContrato());
				json.put("status", "500");
				json.put("msg", "Invalido o campo cidade");
				
			}
			
		}
		if(cliente.getSt_estado_sac() == null || cliente.getSt_estado_sac().trim().length() == 0){
			
			if(json != null){
				json.put("msg", json.getString("msg")+", o campo estado");
			}else{
				
				json = new JSONObject();
				json.put("in_CodCliente", cliente.getSt_CodCliente());
				json.put("in_idCliente", cliente.getIn_idCliente());
				json.put("st_nome_sac", cliente.getSt_nome_sac());
				json.put("idContrato", cliente.getIdContrato());
				json.put("status", "500");
				json.put("msg", "Invalido o campo estado");
				
			}
			
		}
		return json;
	}
	
	public Cliente cadastrarCliente(Cliente clienteOriginal){
				
		JSONObject json = new JSONObject();
		
		ClienteSP clienteSP = formatCliente(clienteOriginal);
		json = validarDados(clienteSP);
							
		json = ws.cadastrarCliente(clienteSP);
		if(json != null && json.getInt("status") == 200){		
			clienteOriginal.setIdClienteBoleto_sl(json.getJSONObject("data").getLong("id_sacado_sac"));		
			
			return clienteOriginal;
		}		
				
		return null;
	}
	
	
	private ClienteSP formatCliente(Cliente cliente){
		
		ClienteSP clienteSP = new ClienteSP();
		
		clienteSP.setSt_CodCliente(cliente.getNrCodCliente());
		clienteSP.setSt_nome_sac(cliente.getNmCliente());
		clienteSP.setSt_nomeref_sac(cliente.getNmCliente());
		
		clienteSP.setSt_diavencimento_sac(10);
		
		clienteSP.setSt_cgc_sac(cliente.getNrCPF());
		clienteSP.setSt_rg_sac(cliente.getNrRG());
		
		clienteSP.setSt_email_sac(cliente.getNmEmail() != null ? cliente.getNmEmail() : "odontomed.ti@gmail.com");
		
		clienteSP.setSt_endereco_sac(cliente.getNmLogradouro());
		clienteSP.setSt_numero_sac(cliente.getNrNumero());
		clienteSP.setSt_complemento_sac(cliente.getNmComplemento());
		clienteSP.setSt_cep_sac(cliente.getNrCEP());
		clienteSP.setSt_estado_sac(cliente.getCidade().getNmUF());
		clienteSP.setSt_cidade_sac(cliente.getCidade().getNmCidade());
		clienteSP.setSt_bairro_sac(cliente.getBairro().getNmBairro());
		
		clienteSP.setFl_mesmoend_sac(1);

//		clienteSP.setIn_idCliente(in_idCliente);
//		clienteSP.setIdContrato(idContrato);		
		
		return clienteSP;
		
	}

	
	
}
