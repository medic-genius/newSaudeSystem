package br.com.medic.medicsystem.main.mapper;

public enum ErrorMessage {

	INTERNAL_ERROR_STR("Requisicao invalida, ou nao pode ser completada. Verifique os dados de envio."),

	INVALID_PARAMS_STR("Parametros invalidos, verifique a documentacao ou requisite suporte de sistema."),
	
	INVALID_PARAMS_STR_WITHDATA("Parametros invalidos, verifique os campos: ");

	private String message;

	ErrorMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
