package br.com.medic.medicsystem.main.appmobile.response;

public abstract class AppResponse {
	private String status;
	private Integer code;
	
	public AppResponse(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
