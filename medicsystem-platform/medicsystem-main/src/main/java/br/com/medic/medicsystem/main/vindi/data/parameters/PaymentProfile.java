package br.com.medic.medicsystem.main.vindi.data.parameters;

public class PaymentProfile {
	private Integer id;
	private String holder_name;
	private String registry_code;
	private String bank_branch;
	private String bank_account;
	private String card_expiration;
	private String card_number;
	private String card_cvv;
	private String payment_method_code;
	private String payment_company_code;
	private String gateway_token;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getHolder_name() {
		return holder_name;
	}
	public void setHolder_name(String holder_name) {
		this.holder_name = holder_name;
	}
	public String getRegistry_code() {
		return registry_code;
	}
	public void setRegistry_code(String registry_code) {
		this.registry_code = registry_code;
	}
	public String getBank_branch() {
		return bank_branch;
	}
	public void setBank_branch(String bank_branch) {
		this.bank_branch = bank_branch;
	}
	public String getBank_account() {
		return bank_account;
	}
	public void setBank_account(String bank_account) {
		this.bank_account = bank_account;
	}
	public String getCard_expiration() {
		return card_expiration;
	}
	public void setCard_expiration(String card_expiration) {
		this.card_expiration = card_expiration;
	}
	public String getCard_number() {
		return card_number;
	}
	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}
	public String getCard_cvv() {
		return card_cvv;
	}
	public void setCard_cvv(String card_cvv) {
		this.card_cvv = card_cvv;
	}
	public String getPayment_method_code() {
		return payment_method_code;
	}
	public void setPayment_method_code(String payment_method_code) {
		this.payment_method_code = payment_method_code;
	}
	public String getPayment_company_code() {
		return payment_company_code;
	}
	public void setPayment_company_code(String payment_company_code) {
		this.payment_company_code = payment_company_code;
	}
	public String getGateway_token() {
		return gateway_token;
	}
	public void setGateway_token(String gateway_token) {
		this.gateway_token = gateway_token;
	}
}
