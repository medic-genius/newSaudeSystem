package br.com.medic.medicsystem.main.vindi.data.payment;

import br.com.medic.medicsystem.main.vindi.data.summary.Customer;
import br.com.medic.medicsystem.main.vindi.data.summary.PaymentMethod;

public class PaymentProfile {
	private Integer id;
	private String status;
	private String holder_name;
	private String registry_code;
	private String bank_branch;
	private String bank_account;
	private String card_expiration;
	private String card_number_first_six;
	private String card_number_last_four;
	private String token;
	private String gateway_token;
	private String type;
	private String created_at;
	private String updated_at;
	private PaymentCompany payment_company;
	private PaymentMethod payment_method;
	private Customer customer;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHolder_name() {
		return holder_name;
	}
	public void setHolder_name(String holder_name) {
		this.holder_name = holder_name;
	}
	public String getRegistry_code() {
		return registry_code;
	}
	public void setRegistry_code(String registry_code) {
		this.registry_code = registry_code;
	}
	public String getBank_branch() {
		return bank_branch;
	}
	public void setBank_branch(String bank_branch) {
		this.bank_branch = bank_branch;
	}
	public String getBank_account() {
		return bank_account;
	}
	public void setBank_account(String bank_account) {
		this.bank_account = bank_account;
	}
	public String getCard_expiration() {
		return card_expiration;
	}
	public void setCard_expiration(String card_expiration) {
		this.card_expiration = card_expiration;
	}
	public String getCard_number_first_six() {
		return card_number_first_six;
	}
	public void setCard_number_first_six(String card_number_first_six) {
		this.card_number_first_six = card_number_first_six;
	}
	public String getCard_number_last_four() {
		return card_number_last_four;
	}
	public void setCard_number_last_four(String card_number_last_four) {
		this.card_number_last_four = card_number_last_four;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getGateway_token() {
		return gateway_token;
	}
	public void setGateway_token(String gateway_token) {
		this.gateway_token = gateway_token;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public PaymentCompany getPayment_company() {
		return payment_company;
	}
	public void setPayment_company(PaymentCompany payment_company) {
		this.payment_company = payment_company;
	}
	public PaymentMethod getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(PaymentMethod payment_method) {
		this.payment_method = payment_method;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}