package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.main.service.GrupoOrgaoService;
import br.com.medic.medicsystem.persistence.model.GrupoOrgao;

@Path("/gruposOrgaos")
@RequestScoped
public class GrupoOrgaoController extends BaseController {
	
	@Inject
	private GrupoOrgaoService grupoOrgaoService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<GrupoOrgao> getAllGrupoOrgao() {

		return grupoOrgaoService.getAllGrupoOrgao();
	}
	
}
