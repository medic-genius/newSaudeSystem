package br.com.medic.medicsystem.main.rest;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.service.ClienteService;
import br.com.medic.medicsystem.main.service.PreAnamneseService;
import br.com.medic.medicsystem.persistence.appmobile.dto.ClienteInfoDTO;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioLogin;
import br.com.medic.medicsystem.persistence.dto.AnamneseDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoAnamneseDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteFpDTO;
import br.com.medic.medicsystem.persistence.dto.CoberturaPlanoDTO;
import br.com.medic.medicsystem.persistence.dto.CobrancaDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteSearchDTO;
import br.com.medic.medicsystem.persistence.dto.SituacaoDTO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Foto;
import br.com.medic.medicsystem.persistence.model.ImagemCliente;
import br.com.medic.medicsystem.persistence.model.NewProspect;
import br.com.medic.medicsystem.persistence.model.ObservacaoCliente;
import br.com.medic.medicsystem.persistence.model.PreAnamnese;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedico;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedicoAnamneseView;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;
import br.com.medic.medicsystem.persistence.model.views.DespesaView;

@Path("/clientes")
@RequestScoped
public class ClienteController extends BaseController {

	@Inject
	private ClienteService clienteService;
	
	
	@Inject
	private PreAnamneseService preAnamneseService;

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente getCliente(@PathParam("id") Long id) {
		final Cliente cliente = clienteService.getCliente(id);

		if (cliente == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return cliente;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/foto/cliente")
	@Produces(MediaType.APPLICATION_JSON)
	public Foto getFotoCliente(@PathParam("id") Long id) throws IOException {
		final Cliente cliente = clienteService.getCliente(id);
		
		
		Foto fotocliente = new Foto();
		if(cliente != null && cliente.getFotoCliente()!= null)
		{
			fotocliente.setFoto(cliente.getFotoCliente());
			return 	fotocliente;
		}
	    return fotocliente;
//	return new String(DatatypeConverter.parseBase64Binary(cliente.getFotoClienteDecoded()));
//		File of = new File("suaImagem.png");
//		FileOutputStream osf = new FileOutputStream(of);
//		osf.write(btDataFile);
//		osf.flush();
//		
//		if (cliente.getFotoClienteDecoded() == null) {
//			throw new WebApplicationException(Response.Status.NOT_FOUND);
//		}
//		String fotoCliente = cliente.getFotoClienteDecoded();
		
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClienteView> getClientes() {

		return clienteService.getClientes(
		        getQueryParam("nrCodCliente", String.class),
		        getQueryParam("nmCliente", String.class),
		        getQueryParam("nrCPF", String.class),
		        getQueryParam("nrRG", String.class),
		        getQueryParam("start", Integer.class),
		        getQueryParam("size", Integer.class));
	}

	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCliente(@Valid Cliente cliente,
			@QueryParam("tipoCliente") Integer tipoCliente) {
		
		Cliente salvo = clienteService.saveCliente(cliente, tipoCliente);
		return Response.created(URI.create("/clientes/" + salvo.getId()))
		        .build();
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCliente(@PathParam("id") Long id,
	        @Valid Cliente cliente) {

		if (cliente != null && cliente.getId() != null) {
			Cliente clienteAtualizado = clienteService.updateCliente(cliente);

			if (clienteAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/arquivos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ImagemCliente> listFileCliente(@PathParam("id") Long id) {

		List<ImagemCliente> imagensCliente = clienteService
		        .listImagemCliente(id);
		if (imagensCliente.isEmpty() || imagensCliente == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return imagensCliente;
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/situacao")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SituacaoDTO> getSituacao(@PathParam("id") Long id) {

		return clienteService.getSituacaoCliente(id);

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/arquivos/{idimagem:[0-9][0-9]*}/imagem")
	@Produces("images/jpeg")
	public Response getFileCliente(@PathParam("idimagem") Long idimagem) {
		File fileCliente = clienteService.getFileImagemCliente(idimagem);
		if (fileCliente != null && fileCliente.isFile()) {
			ResponseBuilder response = Response.ok((Object) fileCliente);
			response.header("Content-Disposition",
			        "filename=" + fileCliente.getName());
			return response.build();
		} else {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/agendamentos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoView> getAgendamentosNaoAtendidos(
	        @PathParam("id") Long idCliente, @QueryParam("todos") boolean todos) {

		if (idCliente != null) {
			List<AgendamentoView> ls = clienteService.getAgendamentos(idCliente, todos);
			return ls;
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/dependentes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dependente> getDependente(@PathParam("id") Long idCliente) {

		if (idCliente != null) {

			return clienteService.getDependentes(idCliente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@GET
	@Path("/{idcliente:[0-9][0-9]*}/dependentes/{iddependente:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Dependente getDependenteById(@PathParam("idcliente") Long idCliente,
	        @PathParam("iddependente") Long idDependente) {

		if (idCliente != null && idDependente != null) {

			return clienteService.getDependenteById(idDependente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/contratos")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Contrato> getContrato(@PathParam("id") Long idCliente) {

		if (idCliente != null) {

			return clienteService.getContratos(idCliente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/dependentes/{iddependente:[0-9][0-9]*}/contratos")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<ContratoDependente> getDependentesContratos(
	        @NotNull @PathParam("id") Long idCliente,
	        @NotNull @PathParam("iddependente") Long idDependente) {

		if (idCliente != null) {

			return clienteService.getDependentesContratos(idCliente,
			        idDependente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/observacoes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ObservacaoCliente> getObservacaoCliente(
	        @PathParam("id") Long idCliente) {

		if (idCliente != null) {

			return clienteService.getObservacaoCliente(idCliente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/coberturas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CoberturaPlanoDTO> getCoberturaCliente(
	        @PathParam("id") Long idCliente) {

		if (idCliente != null) {

			return clienteService.getCoberturaPlano(idCliente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/despesas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DespesaView> getDespesas(
	        @PathParam("id") Long idCliente) {

		if (idCliente != null) {

			return clienteService.getClienteDespesas(idCliente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/somaValorDebito")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSomaValorDebito(
	        @PathParam("id") Long idCliente) {

		if (idCliente != null) {
			
			HashMap<String, Double> map = new HashMap<String, Double>();
			map.put("soma", clienteService.getSomaValorDebito(idCliente));

			return Response.ok(map).build();
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/agendamento/{idEspecialidade:[0-9][0-9]*}/historico")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentoMedico> getAtendimentoMedicoHistoricoView(
	        @PathParam("id") Long idPaciente, @PathParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("tipoPaciente") String tipoPaciente) {
		if (idPaciente != null && idEspecialidade != null) {

			Character tipoPacienteChar = tipoPaciente.charAt(0);
			return clienteService.getAtendimentoMedicoHistoricoView(idPaciente, idEspecialidade, tipoPacienteChar);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/anamnese")
	@Produces(MediaType.APPLICATION_JSON)
	public AnamneseDTO getAnamnese(
	        @PathParam("id") Long idPaciente,
	        @QueryParam("inTipoPaciente") String inTipoPaciente) {
		if (idPaciente != null) {

			
			return clienteService.getAnamnese(idPaciente, inTipoPaciente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/preanamnese")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentoMedicoAnamneseView> getAtendimentoMedicoAnamneseView(
	        @PathParam("id") Long idPaciente,
	        @QueryParam("inTipoPaciente") String inTipoPaciente) {
		if (idPaciente != null) {

			
			return clienteService.getAtendimentoMedicoAnamneseView(idPaciente, inTipoPaciente);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
	@PUT
	@Path("/updateAnamnese")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAnamnese(AtendimentoAnamneseDTO atendimentoAnamneseDTO) {
	
		AnamneseDTO anamnese = null;
		List<AtendimentoMedicoAnamneseView> listatendimentomedicoanamneseview = null;
		
	if(atendimentoAnamneseDTO != null){
		anamnese = atendimentoAnamneseDTO.getAnamnese();
		listatendimentomedicoanamneseview = atendimentoAnamneseDTO.getAtendimentomedicoanamneseview();
		
	}
		
		if (anamnese != null && listatendimentomedicoanamneseview != null) {
			boolean atualizada = clienteService.updateAnamnese(anamnese, listatendimentomedicoanamneseview);

			if (atualizada) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	
	@POST
	@Path("/saveanamnese")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveAnamnese(AtendimentoAnamneseDTO atendimentoAnamneseDTO) {
		
		AnamneseDTO anamnese = null;
		List<AtendimentoMedicoAnamneseView> listatendimentomedicoanamneseview = null;
		
	if(atendimentoAnamneseDTO != null){
		anamnese = atendimentoAnamneseDTO.getAnamnese() != null ? atendimentoAnamneseDTO.getAnamnese() : null;
		listatendimentomedicoanamneseview = atendimentoAnamneseDTO.getAtendimentomedicoanamneseview() != null ? 
							atendimentoAnamneseDTO.getAtendimentomedicoanamneseview(): null;
		
		}
		boolean salva = clienteService.saveAnamnese(anamnese, listatendimentomedicoanamneseview);
		
		if (salva) {
			return Response.ok().build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GET
	@Path("/anamnese/questionario")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PreAnamnese> getAnamneseQuestionario() {
			return preAnamneseService.getAnamneseQuestionario();
		 
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/situacaofinanceira/quitada")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CobrancaDTO> getSituacaoFinanceiraQuitada(
			@PathParam("id") Long id,
			 @QueryParam("nrContrato") String nrContrato) {

		return clienteService.getSituacaoFinanceiraQuitada(id, nrContrato);

	}
	
	
	@GET
	@Path("/mapa")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClienteDTO> getClienteDTO(
			 @QueryParam("inTipo") Integer inTipo,
			 @QueryParam("dtInicio") String dtInicio,
			 @QueryParam("dtFim") String dtFim,
			 @QueryParam("idUnidade") List<Long> idUnidade) {

		return clienteService.getClienteDTO(inTipo, dtInicio, dtFim, idUnidade);

	}
	
	@GET
	@Path("/latitudelongitude")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClienteDTO> getClienteDTO() {

		return clienteService.getClienteDTOLatLng();

	}
	
	
	@PUT
	@Path("/updatelatitudelongitude")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateLatitudeLongitude(
			List<ClienteDTO> clientes){
		
		
		if (clientes != null && clientes.size() > 0) {
			Boolean str =  clienteService.updateLatitudeLongitude(clientes);
			if(str == true){
			return Response.ok(new ClienteDTO()).build();
			}
		return null;
		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("/{cpf:[0-9][0-9]*}/clientefp")
	@Produces(MediaType.APPLICATION_JSON)
	public ClienteFpDTO getClienteFpPorCPFMatricula(
			@PathParam("cpf") String nrCPF) {

		return clienteService.getClienteFpPorCPFMatricula(nrCPF, null);

	}

	@GET
	@Path("/{cpf:[0-9][0-9]*}/clientecpf")
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente ClienteByCPF(
			@PathParam("cpf") String nrCPF) {

		return clienteService.ClienteByCPF(nrCPF);

	}
	
	@GET
	@Path("/{id:[0-9]*}/contrato")
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente clienteByContrato(
			@PathParam("id") Long idContrato) {
		return clienteService.getClienteByContrato(idContrato);
	}
	
	@GET
	@Path("/{cpf:[0-9][0-9]*}/newprospect")
	@Produces(MediaType.APPLICATION_JSON)
	public NewProspect getNewProspect(@PathParam("cpf") String nrCpf) {

		return clienteService.getNewProspectbyCpf(nrCpf);
	}
	
	@GET
	@Path("/search/{cpf:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public PacienteSearchDTO getUserByCPF(@PathParam("cpf") String nrCpf) {
		if(nrCpf != null && nrCpf.length() == 11) {
			return clienteService.searchUserByCPF(nrCpf);
		}
		return null;
	}
	

	//-------------------------------------------------------------------
	// Aplicativo
	@GET
	@Path("/basic_info")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getClienteInfo() {
		return clienteService.getClienteInfo();
	}
	
	@PUT
	@Path("/basic_info/update")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse updateClienteInfo(ClienteInfoDTO info) {
		if(info != null) {
			return clienteService.updateClienteInfo(info);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@GET
	@Path("/foto_cliente")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getClienteFoto() throws IOException {
		return clienteService.getUsuarioFoto();
	}
	
	@PUT
	@Path("/update_foto")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse updateClienteFoto(Foto newFoto) {
		if(newFoto != null) {
			return clienteService.updateClienteFoto(newFoto);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@GET
	@Path("/digitalcard")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getClienteDigitalCard(
			@QueryParam("code") Boolean QRCodeOnly) {
		QRCodeOnly = QRCodeOnly != null && QRCodeOnly;
		return clienteService.generateClienteDigitalCard(QRCodeOnly);
	}
	
	@GET
	@Path("/check_situacao")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse checkSituacaoCliente() {
		return clienteService.checkSituacaoCliente();
	}
	
	@GET
	@Path("/lista_contratos")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getContratosCliente() {
		return clienteService.getContratosUsuarioApp();
	}
	
	@POST
	@Path("/register_login")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse registerLoginUsuario(UsuarioLogin userLogin) {
		if(userLogin != null) {
			return clienteService.persistUserLogin(userLogin);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@GET
	@Path("/login/devices/{deviceId}/check")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse checkLoginRegister(@PathParam("deviceId") String deviceId) {
		if(deviceId != null) {
			return clienteService.checkUsuarioLoginRegister(deviceId);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@PUT
	@Path("/login/devices/{deviceId}/last_view")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse updateUserLastView(@PathParam("deviceId") String deviceId) {
		if(deviceId != null) {
			return clienteService.updateUserLastView(deviceId);
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@GET
	@Path("/pacientes")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getPacienteForUser() {
		return clienteService.getPacientesForUser();
	}
}