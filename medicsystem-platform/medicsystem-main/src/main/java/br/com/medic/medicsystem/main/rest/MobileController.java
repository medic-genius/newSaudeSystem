package br.com.medic.medicsystem.main.rest;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.main.appmobile.dto.UserSearchResultDTO;
import br.com.medic.medicsystem.main.appmobile.dto.UserRegisterDTO;
import br.com.medic.medicsystem.main.appmobile.dto.UserSearchDTO;
import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.ErrorResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.appmobile.response.SuccessResponse;
import br.com.medic.medicsystem.main.service.MobileService;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;

@Path("/mobile")
@RequestScoped
public class MobileController extends BaseController{
	@Inject
	private MobileService mobileService;
	
	
	@POST
	@Path("/search_user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse searchUser(@QueryParam("tipo") Integer tipoCliente, 
			UserSearchDTO user) {
		if(user != null && tipoCliente != null) {
			if((tipoCliente.intValue() == TipoUsuario.CLIENTE.valorTipo && user.getCPF() != null) 
					|| (tipoCliente.intValue() == TipoUsuario.DEPENDENTE.valorTipo && 
					(user.getCPF() != null || (user.getCodPaciente() != null))  
					)) {
				UserSearchResultDTO userResult = mobileService.searchUser(tipoCliente, user);
				return new SuccessResponse(200, userResult);
			}
		}
		Map<String, Object> r = new HashMap<String, Object>();
		r.put("mensagem", "Nenhum dado foi fornecido");
		FailResponse response = new FailResponse(-10, r);
		return response;
	}
	
	@POST
	@Path("/clientes/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse createCliente(@Valid UserRegisterDTO register,
			@QueryParam("tipoCliente") Integer tipoCliente) {
		Map<String, Object> r = new HashMap<String, Object>();
		if(register.getCliente() != null && tipoCliente != null) {
			Integer result = mobileService.createUser(register, tipoCliente);
			if(result == null || result.equals(500)) {
				return new ErrorResponse(500, "Houve um erro ao processar a requisição");
			} else if(result.equals(200)) {
				r.put("created", true);
				return new SuccessResponse(200, r);
			} else {
				if(result.equals(204)) {
					r.put("message", "Campos obrigatórios não fornecidos");
				} else if(result.equals(409)) {
					r.put("message", "Usuário já registrado");
				}
				return new FailResponse(result, r);
			}
			
		}
		r.put("mensagem", "Nenhum dado foi fornecido");
		return new FailResponse(-10, r);
	}
	
	@GET
	@Path("/username/check")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse checkUserName(@QueryParam("name") String userName) {
		Map<String, Object> response = new HashMap<String, Object>();
		if(userName != null) {
			Boolean isAvailable = mobileService.isUsernameAvailable(userName);
			response.put("available", isAvailable);
			return new SuccessResponse(200, response);
		}
		response.put("mensagem", "Nenhum dado foi fornecido");
		return new FailResponse(-10, response);
	}
	
	@GET
	@Path("/password_reset")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse requestResetPwdEmail(@QueryParam("email") String email) {
		
		if(email != null) {
			Boolean hadSuccess = mobileService.requestResetPasswordEmail(email);
			AppResponse response = null;
			if(hadSuccess != null && hadSuccess.booleanValue()) {
				response = new SuccessResponse(200, hadSuccess);
			} else {
				response = new ErrorResponse(404, "Usuário não encontrado");
			}
			return response;
		}
		return new ErrorResponse(-10, "Nenhum dado foi fornecido");
	}
}
