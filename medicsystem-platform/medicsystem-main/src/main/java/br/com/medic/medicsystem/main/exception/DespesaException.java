package br.com.medic.medicsystem.main.exception;

import javax.ejb.ApplicationException;

import br.com.medic.medicsystem.main.mapper.ErrorType;

@ApplicationException(rollback = true)
public class DespesaException extends GeneralException {

	public DespesaException(String message, ErrorType errorType) {
		super(message, errorType);
	}
	
	public DespesaException(String message, ErrorType errorType, Throwable e) {
		super(message, errorType, e);
	}

	private static final long serialVersionUID = 2752602977746710855L;

}
