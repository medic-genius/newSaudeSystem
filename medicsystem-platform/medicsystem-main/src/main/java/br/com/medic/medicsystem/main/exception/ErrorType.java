package br.com.medic.medicsystem.main.exception;

public enum ErrorType {
	
	WARNING,
	ERROR,
	DANGER

}
