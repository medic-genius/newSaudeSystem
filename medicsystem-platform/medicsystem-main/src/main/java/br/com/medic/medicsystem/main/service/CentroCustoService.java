package br.com.medic.medicsystem.main.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.CentroCustoDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dto.CentroCustoDTO;
import br.com.medic.medicsystem.persistence.model.CentroCusto;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;

@Stateless
public class CentroCustoService {

	@Inject
	private Logger logger;
	
	@Inject
	@Named("centrocusto-dao")
	private CentroCustoDAO centroCustoDAO;
	
	@Inject
	@Named("empresafinanceira-dao")
	private EmpresaFinanceiroDAO empresaFinanceiroDAO;
	
	public CentroCusto saveCentroCusto(CentroCusto centroCusto){

		if(centroCusto.getId() == null){
			centroCusto  = centroCustoDAO.persist(centroCusto);
		}else{
			centroCusto  = centroCustoDAO.update(centroCusto);
		}
		return centroCusto;
	}
	
	public CentroCusto getCentroCustoById(Long id){
		CentroCusto centroCusto = centroCustoDAO.searchByKey(CentroCusto.class, id);

		if(centroCusto.getDtExclusao() == null){
			return centroCusto;
		} else{
			return null;
		}
	}
	
	public List<CentroCusto> getAllCentrosCusto( ){
		List<CentroCusto> list = centroCustoDAO.getAllCentrosCusto(null);

		return list;
	}
	

	public List<CentroCusto> getAllCentrosCusto(int mesSplit, int anoSplit, List<String> idEmpresa, Long idCentroCusto, int tipoStatus){
		String idEmpresaConver;
		if(idEmpresa.get(0).equals("0")) {
			List<EmpresaFinanceiro> listEmpresas = empresaFinanceiroDAO.getEmpresasFinanceiro(true);
			List<String> listStringEmp = new ArrayList<String>();
			for(int i =0; i < listEmpresas.size(); i++) {
				listStringEmp.add(i, listEmpresas.get(i).getIdEmpresaFinanceiro().toString());
			}
			 idEmpresaConver = converterListaEmString(listStringEmp);
		}else {
			 idEmpresaConver = converterListaEmString(idEmpresa);
		}
		List<CentroCusto> list = centroCustoDAO.getAllCentrosCusto(idCentroCusto);
			somaValorCC(list, 0, list.size(), mesSplit, anoSplit, idEmpresaConver, tipoStatus);
			setOrdemCentroCusto(list, null);
		return list;
	}
	
	
	public String converterListaEmString(List<String> lista) {
		StringBuilder str = new StringBuilder();
		for (String elemento : lista) {
			str.append(elemento).append(",");
		}
		
		if(str.toString().isEmpty()) {
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
		
		
	
	}
	private BigDecimal getValorCC(Long idCC, Integer mes, Integer ano, String idEmpresa, Integer tipoStatus){
		
		List<CentroCustoDTO> resulta = centroCustoDAO.getRelatorioTodosCentroCusto(mes, ano, idCC, idEmpresa, tipoStatus);
		Double valorTotal = new Double(0);
		for (CentroCustoDTO centroCustoDTO : resulta) {
			valorTotal+= centroCustoDTO.getTotalDespesa();
		}
	
		BigDecimal vv = new BigDecimal (valorTotal);
		//System.out.println("aqui: " + vv.setScale(2, RoundingMode.DOWN));
		
		return vv.setScale(2, RoundingMode.HALF_EVEN);
				
	}
	
	private BigDecimal somaValorCC(List<CentroCusto> list, Integer i, Integer size, Integer mesSplit, Integer anoSplit, String idEmpresa, Integer tipoStatus){
		
		if(list != null && i < size) {
		  	if(list.get(i).getCentroCustos()== null || list.get(i).getCentroCustos().size() == 0) {
		  		list.get(i).setMesAno(mesSplit+"-"+anoSplit);
		  		list.get(i).setMes(mesSplit);
		  		list.get(i).setAno(anoSplit);
		  		list.get(i).setValorCentroCusto(getValorCC(list.get(i).getId(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		    	return list.get(i).getValorCentroCusto().add(somaValorCC(list, i+1, list.size(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		    } 
		  	list.get(i).setMesAno(mesSplit+"-"+anoSplit);
	  		list.get(i).setMes(mesSplit);
	  		list.get(i).setAno(anoSplit);
	  		list.get(i).setValorCentroCusto(getValorCC(list.get(i).getId(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		  	list.get(i).setValorCentroCusto( list.get(i).getValorCentroCusto().add(somaValorCC(list.get(i).getCentroCustos(), 0, list.get(i).getCentroCustos().size(), mesSplit, anoSplit, idEmpresa, tipoStatus)));
		    return list.get(i).getValorCentroCusto().add(somaValorCC(list, i+1, list.size(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		  }
		BigDecimal zero = new BigDecimal(0);
		return zero;
	}
	
	public void deleteCentroCusto(Long idCentroCusto){
		CentroCusto centroCusto = getCentroCustoById(idCentroCusto);
		centroCusto.setDtExclusao(new Date());
		setDtExclusaoCentrosDeCusto(centroCusto);
	}
	
	private void setDtExclusaoCentrosDeCusto(CentroCusto centroCusto){
		for (CentroCusto c : centroCusto.getCentroCustos()) {
			c.setDtExclusao(centroCusto.getDtExclusao());
			if(c.getCentroCustos() != null && c.getCentroCustos().size() > 0){
				setDtExclusaoCentrosDeCusto(c);
			}
		}
	}
	
	private void setOrdemCentroCusto(List<CentroCusto> centros, String str){
		
		int count = 1;

		if(str == null){
			str = "";
		}
		
		for (CentroCusto c : centros) {
			c.setNrOrdem(str + count);
			if(c.getCentroCustos() != null && c.getCentroCustos().size() > 0){
				setOrdemCentroCusto(c.getCentroCustos(), c.getNrOrdem() + ".");
			}
			count++;
		}
		
	}
	

	public List<CentroCusto> getAllCentroCusto() {
		List<CentroCusto> list = centroCustoDAO.getAllCentroCusto() ;
		setOrdemCentroCusto(list, null);
		return list;
	}

	public List<CentroCusto> getBuscaCentroCusto(String nmCentroCusto, Integer startPosition, Integer maxResults) {
		
		logger.debug("Obtendo lista de centros....");
		
		return centroCustoDAO.getBuscaCentroCusto(nmCentroCusto, startPosition, maxResults);
	}

	

}
