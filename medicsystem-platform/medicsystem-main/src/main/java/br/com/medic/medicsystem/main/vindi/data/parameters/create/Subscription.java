package br.com.medic.medicsystem.main.vindi.data.parameters.create;

import java.util.List;
import java.util.Map;

import br.com.medic.medicsystem.main.vindi.data.parameters.PaymentProfile;
import br.com.medic.medicsystem.main.vindi.data.parameters.ProductItem;

public class Subscription {
	private String start_at;
	private Integer plan_id;
	private Integer customer_id;
	private String code;
	private String payment_method_code;
	private Integer installments;
	private String billing_trigger_type;
	private Integer billing_trigger_day;
	private Integer billing_cycles;
	private Map<String, String> metadata;
	private List<ProductItem> product_items;
	private PaymentProfile payment_profile;
	public String getStart_at() {
		return start_at;
	}
	public void setStart_at(String start_at) {
		this.start_at = start_at;
	}
	public Integer getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}
	public Integer getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPayment_method_code() {
		return payment_method_code;
	}
	public void setPayment_method_code(String payment_method_code) {
		this.payment_method_code = payment_method_code;
	}
	public Integer getInstallments() {
		return installments;
	}
	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
	public String getBilling_trigger_type() {
		return billing_trigger_type;
	}
	public void setBilling_trigger_type(String billing_trigger_type) {
		this.billing_trigger_type = billing_trigger_type;
	}
	public Integer getBilling_trigger_day() {
		return billing_trigger_day;
	}
	public void setBilling_trigger_day(Integer billing_trigger_day) {
		this.billing_trigger_day = billing_trigger_day;
	}
	public Integer getBilling_cycles() {
		return billing_cycles;
	}
	public void setBilling_cycles(Integer billing_cycles) {
		this.billing_cycles = billing_cycles;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public List<ProductItem> getProduct_items() {
		return product_items;
	}
	public void setProduct_items(List<ProductItem> product_items) {
		this.product_items = product_items;
	}
	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}
	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
