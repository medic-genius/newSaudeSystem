package br.com.medic.medicsystem.main.vindi.data.parameters.create;

import br.com.medic.medicsystem.main.vindi.data.parameters.PricingSchema;

public class BillItem {
	private Integer product_id;
	private String product_code;
	private Number amount;
	private String description;
	private Integer quantity;
	private PricingSchema pricing_schema;
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public PricingSchema getPricing_schema() {
		return pricing_schema;
	}
	public void setPricing_schema(PricingSchema pricing_schema) {
		this.pricing_schema = pricing_schema;
	}
}
