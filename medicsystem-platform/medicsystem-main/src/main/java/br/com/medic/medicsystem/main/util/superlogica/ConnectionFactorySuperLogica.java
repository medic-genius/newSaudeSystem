package br.com.medic.medicsystem.main.util.superlogica;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.medic.dashboard.main.enums.superlogica.Method;

public class ConnectionFactorySuperLogica {
	
	public static HttpURLConnection getConnection(URL url, String urlParameters, Method method){
		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod(method.name()); 
			connection.setRequestProperty("app_token", "pdwubYzGXWMH");
			connection.setRequestProperty("access_token", "KyioEp4TGTzj");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches (false);
			return connection;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
