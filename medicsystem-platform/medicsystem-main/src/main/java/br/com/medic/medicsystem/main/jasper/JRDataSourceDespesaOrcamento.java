package br.com.medic.medicsystem.main.jasper;

import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaOrcamento;

public class JRDataSourceDespesaOrcamento implements JRDataSource {

	private Iterator<RelatorioDespesaOrcamento> it;
	private RelatorioDespesaOrcamento current;
	private boolean gotToNext = true;

	public JRDataSourceDespesaOrcamento(List<RelatorioDespesaOrcamento> list) {

		this.it = list.iterator();
	}

	public boolean next() throws JRException {
		current = it.hasNext() ? it.next() : null;
		gotToNext = (current != null);

		return gotToNext;
	}

	public Object getFieldValue(JRField field) throws JRException {
		Object valor = null;

		if ("nrMatricula".equals(field.getName())) {
			valor = current.getNrMatricula();

		} else if ("nmCliente".equals(field.getName())) {

			valor = current.getNmCliente();
		} else if ("nmOrgao".equals(field.getName())) {

			valor = current.getNmOrgao();

		} else if ("nmPlano".equals(field.getName())) {

			valor = current.getNmPlano();

		} else if ("nmUsuario".equals(field.getName())) {

			valor = current.getNmUsuario();

		} else if ("vlDespesa".equals(field.getName())) {

			valor = current.getVlDespesa();

		} else if ("servicosDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(current.getServicosDespesa());

		} else if ("parcelasDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(current.getParcelasDespesa());

		} else if ("encaminhamentosDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(
			        current.getEncaminhamentosDespesa());
		}

		return valor;
	}
}
