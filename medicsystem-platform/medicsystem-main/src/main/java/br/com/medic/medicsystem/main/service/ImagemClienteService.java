package br.com.medic.medicsystem.main.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.logging.Logger;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ConfiguracaoSistemaDAO;
import br.com.medic.medicsystem.persistence.dao.ImagemClienteDAO;
import br.com.medic.medicsystem.persistence.model.ConfiguracaoSistema;
import br.com.medic.medicsystem.persistence.model.ImagemCliente;

@Stateless
public class ImagemClienteService {

	@Inject
	private Logger logger;

	@Inject
	@Named("imagemcliente-dao")
	private ImagemClienteDAO imagemClienteDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("configuracaosistema-dao")
	private ConfiguracaoSistemaDAO configuracaoSistemaDAO;
	
	public ImagemCliente getImagemCliente(Long id) {
		
		logger.debug("Obtendo uma imagem...");

		return imagemClienteDAO.searchByKey(ImagemCliente.class, id);
	}

	public List<ImagemCliente> getImagensPorCliente(Long id) {
		
		logger.debug("Obtendo lista de imagens por cliente...");
		
		return imagemClienteDAO.getImagensPorCliente(id);
	}

	public ImagemCliente saveImagemCliente(ImagemCliente imagemCliente) {
		imagemCliente = imagemClienteDAO.persist(imagemCliente);
		return imagemCliente;
	}
	
	public ImagemCliente updateImagemCliente(ImagemCliente imagemCliente) {
		imagemCliente = imagemClienteDAO.update(imagemCliente);
		return imagemCliente;
	}
	
	public String getFullPathFileName(Long id){
		
        ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
        ImagemCliente imagemCliente = imagemClienteDAO.searchByKey(ImagemCliente.class, id);

		return configuraSistema.getNmPathImagem()
				+ configuraSistema.getNmSeparador()
				+ imagemCliente.getCliente().getNrCodCliente()
				+ configuraSistema.getNmSeparador();
	}
	
}
