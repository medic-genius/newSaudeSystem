package br.com.medic.medicsystem.main.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.RegistroAgendamentoDAO;
import br.com.medic.medicsystem.persistence.model.RegistroAgendamento;

@Stateless
public class RegistroAgendamentoService {
	
	@Inject
	@Named("registroagendamento-dao")
	private RegistroAgendamentoDAO registroAgendamentoDAO;
	
	public List<RegistroAgendamento> updateRAFaturamentoLaudo(Long idAgendamento, String dtFatudamentoLaudo){
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		List<RegistroAgendamento> listRegAgendamento = registroAgendamentoDAO.getRegistroAgendamentoByAgendamento(idAgendamento);
		
		if(listRegAgendamento != null && listRegAgendamento.size() > 0){
			for(RegistroAgendamento ra : listRegAgendamento){
				try {
					ra.setDtFaturamentoLaudo(df.parse(dtFatudamentoLaudo));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				registroAgendamentoDAO.update(ra);
			}
			
			return listRegAgendamento;
		}
		
		return null;		
	}

}
