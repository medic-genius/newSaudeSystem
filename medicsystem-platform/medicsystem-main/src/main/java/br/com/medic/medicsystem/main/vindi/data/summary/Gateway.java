package br.com.medic.medicsystem.main.vindi.data.summary;

public class Gateway {
	private Integer id;
	private String connector;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
}
