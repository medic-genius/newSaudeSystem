package br.com.medic.medicsystem.main.service.pjbank;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.util.EmailUtil;
import br.com.medic.medicsystem.main.webservice.pjbank.WebServiceBoleto;
import br.com.medic.medicsystem.persistence.dao.BoletoBancarioDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.NegociacaoDAO;
import br.com.medic.medicsystem.persistence.dao.ParcelaDAO;
import br.com.medic.medicsystem.persistence.dto.EmailBoletoDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Negociacao;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusAgendamento;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Stateless
public class BoletoServicePJB extends BaseController {

	@Inject
	private WebServiceBoleto wsb;
	
	@Inject
	@Named("boletobancario-dao")
	private BoletoBancarioDAO boletobancarioDAO;
	
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;
	
	@Inject
	@Named("parcela-dao")
	private ParcelaDAO parcelaDAO;
	
	@Inject
	@Named("negociacao-dao")
	private NegociacaoDAO negociacaoDAO;
	
	@SuppressWarnings("static-access")
	public BoletoBancario registrarBoleto(Object documento, Object cliente) throws Exception{
		
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		JSONObject json = null;
		List<BoletoBancario> listBoleto = new ArrayList<BoletoBancario>();		
		
		try {
			
			if(documento instanceof Mensalidade){
				mensalidade = (Mensalidade) documento;
				listBoleto = boletobancarioDAO.getBoletoBancario(mensalidade);
			}
			else if(documento instanceof Parcela){
				parcela = (Parcela) documento;
				listBoleto = boletobancarioDAO.getBoletoBancario(parcela);
			}
			
			json = wsb.criarEditarBoleto(documento, cliente);	
		
			if(json.getInt("status") == 200){

				if(listBoleto != null && listBoleto.size() > 0){
					
					listBoleto.get(0).setNrBarra(json.getString("linhaDigitavel"));
					listBoleto.get(0).setNrNossoNumero(json.getString("nossonumero"));
					listBoleto.get(0).setLinkBoleto(json.getString("linkBoleto"));
					
					boletobancarioDAO.update(listBoleto.get(0));
					
					return listBoleto.get(0);
				
				}else{
										
					if( json.has("credencial") && wsb.CREDENCIAL_KEY.equals(json.get("credencial")) ){
						
						BoletoBancario boletoBancario = new BoletoBancario();
						boletoBancario.setNrBoleto( Long.valueOf(json.getString("id_unico")) );
						boletoBancario.setDtInclusao( new Date() );
						boletoBancario.setCdBarra(json.getString("token_facilitador"));
						boletoBancario.setNrBarra(json.getString("linhaDigitavel"));
						boletoBancario.setNrNossoNumero(json.getString("nossonumero"));
						boletoBancario.setLinkBoleto(json.getString("linkBoleto"));
						
						if(mensalidade != null)
							boletoBancario.setIdMensalidade(mensalidade.getId());
						else if(parcela != null)
							boletoBancario.setIdParcela(parcela.getId());
						
						boletobancarioDAO.persist(boletoBancario);
						
						return boletoBancario;						
					}										
				}				
			}
			
			return null;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}		
		
	}
	
	public Integer liquidarCobrancaSl(Integer periodoDias){
		
		//inTipoPago = null -boletos nao pagos
		//inTipoPago = 1 -boletos pagos
						
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		Date dtPagamento;
		Boolean despesaPaga = true;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;		
		String dtInicio, dtFim;
		Integer numPagina = 1;
		JSONArray listJsonTotal = new JSONArray();
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		dtFim = sdf.format(calendar.getTime());
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.add(Calendar.DAY_OF_MONTH, -periodoDias);
		dtInicio = sdf.format(calendar2.getTime());		
		
		try {
			
			JSONArray listJson = wsb.extratoBoleto(dtInicio, dtFim, 1);
			
			while(listJson != null && listJson.length() > 0){								
				numPagina += 1;				
				
				if(listJsonTotal != null && listJsonTotal.length() > 0){
					for (Object object : listJson) {
						listJsonTotal.put(object);
					}
				}else{
					listJsonTotal = listJson;
				}
				
				listJson = wsb.extratoBoleto(dtInicio, dtFim, 1, numPagina);
				System.out.println("Pagina boleto: " + numPagina);	
				
			}
			System.out.println("Total paginas boleto: " + numPagina);			
			int count = 0;
			
			if(listJsonTotal != null){
				
				int len = listJsonTotal.length();		
				
				for(int i = 0; i < len; ++i) {				
					JSONObject jsonObj = listJsonTotal.getJSONObject(i);
					BoletoBancario bb = boletobancarioDAO.getBoletoByNossoNumero( jsonObj.getString("nosso_numero") );	
					
					if(bb != null){
						
						if(bb.getIdMensalidade() != null)
							mensalidade = mensalidadeDAO.searchByKey(Mensalidade.class, bb.getIdMensalidade());
						else if(bb.getIdParcela() != null)
							parcela = parcelaDAO.getParcela(bb.getIdParcela());
									
						dtPagamento = sdf.parse(jsonObj.getString("data_pagamento"));
	//					vlJuros = json.has("data[compo_recebimento][1][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][1][st_valor_comp]") : 0.0;
	//					vlMulta = json.has("data[compo_recebimento][2][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][2][st_valor_comp]") : 0.0;
						
						
						if(parcela != null && parcela.getDtPagamento() == null){
							
							List<Negociacao> listnegociacoes = new ArrayList<Negociacao>();
							
							parcela.setDtPagamento(dtPagamento);
							parcela.setVlPago(Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")));
							parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							parcela.setInFormaPagamento(5);
							parcela.setVlJuros( vlJuros );
							parcela.setVlMulta( vlMulta );				
											
							for(Parcela pc: parcela.getDespesa().getParcelas())					
								if(pc.getDtPagamento() == null && (pc.getBoExcluida() == null || pc.getBoExcluida() == false))
									despesaPaga = false;			
											
							if(despesaPaga)
								parcela.getDespesa().setInSituacao(1);
							
							listnegociacoes = negociacaoDAO.getNegociacaoByDespesa(parcela.getDespesa().getId());
							if(listnegociacoes != null && listnegociacoes.size() > 0)
								for(Negociacao neg : listnegociacoes){
									neg.getMensalidade().setDataPagamento(dtPagamento);
									neg.getMensalidade().setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
									neg.getMensalidade().setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
									neg.getMensalidade().setInStatus(1);
									neg.getMensalidade().setVlJuros(vlJuros);
									neg.getMensalidade().setVlMulta(vlMulta);
									neg.getMensalidade().setBoletoNegociada(true);
									neg.getMensalidade().setDtNegociacao(parcela.getDespesa().getDtDespesa());
									
									mensalidadeDAO.update(neg.getMensalidade());
								}

							Agendamento ag = parcela.getDespesa().getAgendamento();
							if (ag != null && 
									ag.getInStatus().equals(StatusAgendamento.BLOQUEADO.getId())) {
								ag.setInStatus(StatusAgendamento.LIBERADO.getId());
							}
							
							parcelaDAO.update(parcela);
							count++;
						} else if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
											
							mensalidade.setDataPagamento(dtPagamento);
							mensalidade.setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
							mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							mensalidade.setInStatus(1);
							mensalidade.setVlJuros(vlJuros);
							mensalidade.setVlMulta(vlMulta);
							
							mensalidadeDAO.update(mensalidade);
							count++;
										
						}
					
					}
				
				}
			}
			
			return count;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return null;		
					
	}
	
	public Integer liquidarCobrancaSl(String dtInicio, String dtFim){
		
		//inTipoPago = null -boletos nao pagos
		//inTipoPago = 1 -boletos pagos
						
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade = null;
		Parcela parcela = null;
		Date dtPagamento;
		Boolean despesaPaga = true;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;		
		Integer numPagina = 1;
		JSONArray listJsonTotal = new JSONArray();
				
		try {
			
			JSONArray listJson = wsb.extratoBoleto(dtInicio, dtFim, 1);
			
			while(listJson != null && listJson.length() > 0){								
				numPagina += 1;				
				
				if(listJsonTotal != null && listJsonTotal.length() > 0){
					for (Object object : listJson) {
						listJsonTotal.put(object);
					}
				}else{
					listJsonTotal = listJson;
				}
				
				listJson = wsb.extratoBoleto(dtInicio, dtFim, 1, numPagina);
				System.out.println("Pagina boleto: " + numPagina);	
				
			}
			System.out.println("Total paginas boleto: " + numPagina);			
			int count = 0;
			
			if(listJsonTotal != null){
				
				int len = listJsonTotal.length();		
				
				for(int i = 0; i < len; ++i) {				
					JSONObject jsonObj = listJsonTotal.getJSONObject(i);
					BoletoBancario bb = boletobancarioDAO.getBoletoByNossoNumero( jsonObj.getString("nosso_numero") );	
					
					if(bb != null){
						
						if(bb.getIdMensalidade() != null)
							mensalidade = mensalidadeDAO.searchByKey(Mensalidade.class, bb.getIdMensalidade());
						else if(bb.getIdParcela() != null)
							parcela = parcelaDAO.getParcela(bb.getIdParcela());
									
						dtPagamento = sdf.parse(jsonObj.getString("data_pagamento"));
	//					vlJuros = json.has("data[compo_recebimento][1][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][1][st_valor_comp]") : 0.0;
	//					vlMulta = json.has("data[compo_recebimento][2][st_valor_comp]") == true ? json.getDouble("data[compo_recebimento][2][st_valor_comp]") : 0.0;
						
						
						if(parcela != null && parcela.getDtPagamento() == null){
							
							List<Negociacao> listnegociacoes = new ArrayList<Negociacao>();
							
							parcela.setDtPagamento(dtPagamento);
							parcela.setVlPago(Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")));
							parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							parcela.setInFormaPagamento(5);
							parcela.setVlJuros( vlJuros );
							parcela.setVlMulta( vlMulta );				
											
							for(Parcela pc: parcela.getDespesa().getParcelas())					
								if(pc.getDtPagamento() == null && (pc.getBoExcluida() == null || pc.getBoExcluida() == false))
									despesaPaga = false;			
											
							if(despesaPaga)
								parcela.getDespesa().setInSituacao(1);
							
							listnegociacoes = negociacaoDAO.getNegociacaoByDespesa(parcela.getDespesa().getId());
							if(listnegociacoes != null && listnegociacoes.size() > 0)
								for(Negociacao neg : listnegociacoes){
									neg.getMensalidade().setDataPagamento(dtPagamento);
									neg.getMensalidade().setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
									neg.getMensalidade().setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
									neg.getMensalidade().setInStatus(1);
									neg.getMensalidade().setVlJuros(vlJuros);
									neg.getMensalidade().setVlMulta(vlMulta);
									neg.getMensalidade().setBoletoNegociada(true);
									neg.getMensalidade().setDtNegociacao(parcela.getDespesa().getDtDespesa());
									
									mensalidadeDAO.update(neg.getMensalidade());
								}				
							
							
							parcelaDAO.update(parcela);
							count++;
							
							
						}else if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
											
							mensalidade.setDataPagamento(dtPagamento);
							mensalidade.setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
							mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							mensalidade.setInStatus(1);
							mensalidade.setVlJuros(vlJuros);
							mensalidade.setVlMulta(vlMulta);
							
							mensalidadeDAO.update(mensalidade);
							count++;
										
						}
					
					}
				
				}
			}
			
			return count;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return null;		
					
	}
	
	public byte[] imprimirBoleto(List<?> documentos) {
		
		String urlBoleto = wsb.imprimirBoleto(documentos);
				
		URL url;
		InputStream in = null;
		ByteArrayOutputStream out = null;
		byte[] pdf = null;
		
		if(urlBoleto != null){
		
			try {
				url = new URL(urlBoleto);
			
				in = new BufferedInputStream(url.openStream());
				out = new ByteArrayOutputStream();
				
				byte[] buf = new byte[1024];
				int n = 0;
				while (-1!=(n=in.read(buf)))
				{out.write(buf, 0, n);}
		
				pdf = out.toByteArray();			
			
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
				try {
					out.close();		
					in.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
				
			}
		}
		
		return pdf;
	}
	
	public String getLinkCarne(List<?> documentos){
		
		return	wsb.imprimirBoleto(documentos);	
	}
	
	public List<BoletoBancario> getBoletosBancarios(Parcela parcela) {
		return boletobancarioDAO.getBoletoBancario(parcela);
	}
	
	public List<BoletoBancario> getBoletosBancario(Mensalidade mensalidade) {
		return boletobancarioDAO.getBoletoBancario(mensalidade);
	}
	
	
	public String liquidarCobrancaSlPendentes(String dtInicio, String dtFim){
		
		//inTipoPago = null -boletos nao pagos
		//inTipoPago = 1 -boletos pagos
						
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Mensalidade mensalidade = null;
		Date dtPagamento;
		Double vlJuros = 0.0;
		Double vlMulta = 0.0;		
		
		try {
			
			
			JSONArray listJson = new JSONArray();
			JSONArray listJsonTotal = new JSONArray();
			Integer numPagina = 1;
			listJson = wsb.extratoBoleto(dtInicio, dtFim, 1,numPagina);
			
			while(listJson != null && listJson.length() > 0){
				numPagina += 1;
				if(listJsonTotal != null && listJsonTotal.length() > 0){
					for (Object object : listJson) {
						listJsonTotal.put(object);
					}
				}else{
					listJsonTotal = listJson;
				}
				
				listJson = wsb.extratoBoleto(dtInicio, dtFim, 1,numPagina);
				
			}
			
			int count = 0;
				
			if(listJsonTotal != null && listJsonTotal.length() > 0){
				
				int len = listJsonTotal.length();	
				List<EmailBoletoDTO> boletosBaixados = new ArrayList<EmailBoletoDTO>();
				List<EmailBoletoDTO> boletosNaoBaixados = new ArrayList<EmailBoletoDTO>();
				
				for(int i = 0; i < len; ++i) {				
					JSONObject jsonObj = listJsonTotal.getJSONObject(i);
					
					String dataPagamento = jsonObj.getString("data_pagamento");
					
					String nossoNumero = jsonObj.getString("nosso_numero");
					
					String vlPago = jsonObj.getString("valor_liquido").replace(".", ",");
					
					String dataVencimento = jsonObj.getString("data_vencimento");
					
					String nmPagador = jsonObj.getString("pagador");
					
					if(dataVencimento == null  || dataVencimento.trim().length() == 0 || nmPagador == null || nmPagador.trim().length() == 0){
						
						String nomeCliente = nmPagador != null && nmPagador.trim().length() > 0 ? nmPagador : "Nome em branco";
						
						if(dataPagamento != null && dataPagamento.trim().length() > 0){
							Date datePg = DateUtil.parseDate(jsonObj.getString("data_pagamento"), "MM/dd/yyyy"); 
						    dataPagamento = DateUtil.getDateAsString(datePg, "dd/MM/yyyy");
						}else{
							dataPagamento = "Em braco";
						}
						
						if(dataVencimento != null && dataVencimento.trim().length() > 0){
							Date date = DateUtil.parseDate(jsonObj.getString("data_vencimento"), "MM/dd/yyyy");
						    dataVencimento = DateUtil.getDateAsString(date, "yyyy/MM/dd");
						}else{
							dataVencimento = "Em braco";
						}
						
						
						EmailBoletoDTO boleto = new EmailBoletoDTO(nomeCliente, dataPagamento, 
								nossoNumero, dataVencimento, vlPago, "--");	
						
						boletosNaoBaixados.add(boleto);
						
					
					
					}else{
						
						Date datePg = DateUtil.parseDate(jsonObj.getString("data_pagamento"), "MM/dd/yyyy"); 
					    dataPagamento = DateUtil.getDateAsString(datePg, "dd/MM/yyyy");
					    
//					    Date date = DateUtil.parseDate(jsonObj.getString("data_vencimento"), "MM/dd/yyyy");
					    
					    String []datas = dataVencimento.split("/");
					    dataVencimento =  datas[2]+"-"+datas[0]+"-"+datas[1];
						
						
						
						mensalidade = boletobancarioDAO.mensalidadesCliente(nmPagador, dataVencimento);
						
						dtPagamento = sdf.parse(jsonObj.getString("data_pagamento"));
						
						
						if(mensalidade != null && mensalidade.getInStatus().equals(0)){ //Aberto
										
							mensalidade.setDataPagamento(dtPagamento);
							mensalidade.setVlPago( Double.parseDouble(jsonObj.getString("valor_liquido").replace(",", ".")) );
							mensalidade.setFormaPagamentoEfetuada(FormaPagamentoEnum.BOLETO_BANCARIO.getDescription());
							mensalidade.setInStatus(1);
							mensalidade.setVlJuros(vlJuros);
							mensalidade.setVlMulta(vlMulta);
							
							mensalidadeDAO.updateMensalidade(mensalidade);
							count++;
							
							EmailBoletoDTO boleto = new EmailBoletoDTO(nmPagador, dataPagamento, 
									nossoNumero, dataVencimento, vlPago, mensalidade.getNrMensalidade());
							
							boletosBaixados.add(boleto);
							
							
										
						}else{
							EmailBoletoDTO boleto = new EmailBoletoDTO(nmPagador, dataPagamento, 
									nossoNumero, dataVencimento, vlPago, "Não Encontrado");	
							
							boletosNaoBaixados.add(boleto);
						}
						
						
					}
					
					
					
					}
				
					emailRelatorioBaixaBoletoManual(boletosBaixados, boletosNaoBaixados);
				
				}else{
					return "Nenum cliente encontrado";
				}
			
			
			return "Boletos liquidados " + count;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return null;		
					
	}

public void emailRelatorioBaixaBoletoManual(List<EmailBoletoDTO> boletosbaixados, List<EmailBoletoDTO> boletosNaoBaixados){
	
	DateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
	String tableBaixados = "";
	String tableNaoBaixados = "";
	
	
	for(EmailBoletoDTO boleto: boletosbaixados){
		
		tableBaixados +=										
		"<tr>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNmCliente() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getDataPagamento() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getDatavencimento() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNossoNumero() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getVlPago() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNrMensalidade() + "</td>"
		+ "</tr><br/>";
							
	}
for(EmailBoletoDTO boleto: boletosNaoBaixados){
		
		tableNaoBaixados +=										
		"<tr>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNmCliente() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getDataPagamento() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getDatavencimento() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNossoNumero() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getVlPago() + "</td>"
		+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;' align='left' valign='top'><br />" + boleto.getNrMensalidade() + "</td>"
		+ "</tr><br/>";
							
	}
	
	String assunto = "Relatorio de baixas de boletos.";
	
	String mensagem = 
	"<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr>"
	+ "<td style='background-color: #ffffff;' align='center' valign='top' bgcolor='#444444'><br /> <br />"
	+ "<table border='0' width='850' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr>"
	+ "<td style='background-color: #2d639a; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff; padding: 0px 15px 10px 15px;' align='left' valign='top' bgcolor='#f73f9b'>"
	+ "<div style='font-size: 48px;'><strong>T.I. Informa</strong></div>"
	+ "<div>Segue informa&ccedil;&otilde;es da quitação de boletos na data "+format.format(new Date())+".</div>"
	+ "</td>"
	+ "</tr>"
	+ "<tr>"
	+ "<td style='background-color: #353535; padding: 15px;' align='left' valign='top' bgcolor='#1ba5db'>"
	+ "<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr>"
	+ "<td align='left' valign='top' width='100%'>"
	+ "<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr align='center'>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 22px; color: #ffffff;' align='center' valign='top'>BAIXADOS</td>"
	+ "</tr>"
	+ "<tr></tr>"
	+ "<tr>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Cliente</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Pagamento</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Vencimento</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Nosso Numero</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Valor</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Mensalidade</td>"
	+ "</tr>"
	+ tableBaixados 
	+ "</tbody>"
	+ "</table>"
	+ "<table border='0' width='100%' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr align='center'>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 22px; color: #ffffff;' align='center' valign='top'>NÃO BAIXADOS</td>"
	+ "</tr>"
	+ "<tr></tr>"
	+ "<tr>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Cliente</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Pagamento</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Vencimento</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Nosso Numero</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Valor</td>"
	+ "<td style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #ffffff;' align='left' valign='top'>Mensalidade</td>"
	+ "</tr>"
	+ tableNaoBaixados 
	+ "</tbody>"
	+ "</table>"
	+ "</td>"
	+ "</tr>"
	+ "</tbody>"
	+ "</table>"
	+ "</td>"
	+ "</tr>"
	+ "</tbody>"
	+ "</table>"
	+ "<table style='margin-top: 10px;' border='0' width='600' cellspacing='0' cellpadding='0'>"
	+ "<tbody>"
	+ "<tr>"
	+ "<td align='center' valign='middle' bgcolor='#e1e1e1'>"
	+ "<table border='0' width='650' cellspacing='0' cellpadding='0' align='center'>"
	+ "<tbody>"
	+ "<tr>"
	+ "<td style='color: #595959; font-size: 11px; font-family: Arial, Helvetica, sans-serif;' align='center' valign='middle' width='55%'><strong>Para mais detalhes, acesse o nosso sistema.</strong> <br />Link de acesso: <a style='color: #595959; text-decoration: none;' href='li1076-46.members.linode.com'>li1076-46.members.linode.com</a> <br /> <br /> <strong>E-mail autom&aacute;tico, por gentileza n&atilde;o responda. Obrigado</strong></td>"
	+ "</tr>"
	+ "</tbody>"
	+ "</table>"
	+ "</td>"
	+ "</tr>"
	+ "</tbody>"
	+ "</table>"
	+ "</td>"
	+ "</tr>"
	+ "</tbody>"
	+ "</table>";
	
//	EmailUtil.sendEmail("sistemas@medic-sistema.com.br", "", assunto, mensagem);
	EmailUtil.sendEmail("brunoaugustoloc@gmail.com", "", assunto, mensagem);
	
}
}
