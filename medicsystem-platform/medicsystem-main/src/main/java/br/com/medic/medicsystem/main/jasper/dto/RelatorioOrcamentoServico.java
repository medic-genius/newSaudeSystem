package br.com.medic.medicsystem.main.jasper.dto;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.persistence.model.Servico;

public class RelatorioOrcamentoServico  implements JRDataSource{
	
	private String nmMedico;
	
	private String nmPaciente;
	
	private String crm;
	
	private Servico valorAtual;
	
	private boolean irParaProximoEncaminhamento = true;
	
	
	private Iterator<Servico> itrServico;
	
	public RelatorioOrcamentoServico(String nmMedico, String nmPaciente,
			String crm, List<Servico> servicos) {

		this.nmMedico = nmMedico;
		this.nmPaciente = nmPaciente;
		this.crm = crm;
		this.itrServico = servicos.iterator();
	}
	
	
	public boolean next() throws JRException {

		this.valorAtual = this.itrServico.hasNext() ? this.itrServico
		        .next() : null;
		this.irParaProximoEncaminhamento = (this.valorAtual != null);

		return this.irParaProximoEncaminhamento;
	}


	@Override
	public Object getFieldValue(JRField campo) throws JRException {
		Object valor = null;

		Servico servico= (Servico) valorAtual;
		
		
		if ("nmMedico".equals(campo.getName())) {
			valor = this.nmMedico;

		} else if ("nmPaciente".equals(campo.getName())) {
			valor = this.nmPaciente;

		} else if ("crm".equals(campo.getName())) {
			valor = this.crm;

		}else if ("nmServico".equals(campo.getName())) {
			valor = servico.getNmServico();

		}else if ("qtServico".equals(campo.getName())) {
			valor = servico.getQuantidade() != null ? servico.getQuantidade() : 0;

		}
		return valor;
	}

	public Date getData() {
		return new Date();
	}


	
	
	

}
