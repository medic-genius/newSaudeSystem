package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.MovimentacaoContaService;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.MovimentacaoContaDTO;

@Path("/movimentacaoconta")
@RequestScoped
public class MovimentacaoContaController extends BaseController{

	@Inject
	private MovimentacaoContaService movimentacaoContaService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<MovimentacaoContaDTO> getMovimentacoesByDias(@QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim, @QueryParam("nrIdEmpresa") Long idEmpresa, @QueryParam("nrIdConta") Long idConta){
		
		if(dtInicio == null || dtFim == null){
			throw new IllegalArgumentException();
		}
		List<MovimentacaoContaDTO> dtos = movimentacaoContaService.getMovimentacoesDTOByDias(dtInicio, dtFim, idConta, idEmpresa);
		return dtos;		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/savemovimentacaocaixa")
	public Response saveMovimentacaoCaixa( @QueryParam("idContaBancaria") Long idContaBancaria, @QueryParam("valor") Double valor, 
			@QueryParam("idCaixa") Long idCaixaConferido, @QueryParam("nmOperador") String nmOperador, @QueryParam("nmUnidade") String nmUnidade,  @QueryParam("dtCaixa") String dtCaixaFormatada ){
		Long idMovimentacao = movimentacaoContaService.saveMovimentacaoCaixa(idContaBancaria, valor, idCaixaConferido, nmOperador, nmUnidade, dtCaixaFormatada);
		return Response.created(URI.create("/movimentacaoconta/" + idMovimentacao)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/entrada/saldomesanterior")
	public BaseDTO saldomesanterior(@QueryParam("dtInicio") String dtInicio, @QueryParam("nrIdConta") Long idConta){
		
		if(dtInicio == null){
			throw new IllegalArgumentException();
		}
		BaseDTO baseDTO = movimentacaoContaService.saldomesanterior(dtInicio, idConta);
		return baseDTO;		
	}
	
	
}
