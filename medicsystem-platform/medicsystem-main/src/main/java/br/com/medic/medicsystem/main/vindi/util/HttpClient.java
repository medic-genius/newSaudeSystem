package br.com.medic.medicsystem.main.vindi.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import br.com.medic.medicsystem.sms.enumerator.ProtocolEnum;

public class HttpClient {
	private String host;
	private int port;
	private boolean usingSSL;
	private ProtocolEnum protocol;
	private CloseableHttpClient httpClient;
	
	public HttpClient() {
		this.usingSSL = false;
		this.httpClient = HttpClients.createDefault();
	}
	
	public HttpClient(CloseableHttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isUsingSSL() {
		return usingSSL;
	}

	public void setUsingSSL(boolean usingSSL) {
		this.usingSSL = usingSSL;
		if (this.usingSSL) {
			this.port = 443;
			this.setProtocol(ProtocolEnum.SECURE_PROTOCOL);
		} else {
			this.port = 80;
			this.setProtocol(ProtocolEnum.PROTOCOL);
		}
	}

	public ProtocolEnum getProtocol() {
		return protocol;
	}

	public void setProtocol(ProtocolEnum protocol) {
		this.protocol = protocol;
	}

	public CloseableHttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(CloseableHttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	public InputStream request(String url, HttpRequestBase req) throws Exception {
		try {
			HttpHost target = new HttpHost(host, port, protocol.getValue());
			req.setURI(new URI(url));

			CloseableHttpResponse response = httpClient.execute(target, req);
			System.out.println("codstatus: " + response.getStatusLine().getStatusCode());
			System.out.println("frase: " + response.getStatusLine().getReasonPhrase());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK || 
					response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
				return response.getEntity().getContent();
			} else {
				throw new Exception();
			}
		} catch (IOException e) {
			throw new Exception();
		} catch (URISyntaxException e) {
			throw new Exception();
		}
	}
}
