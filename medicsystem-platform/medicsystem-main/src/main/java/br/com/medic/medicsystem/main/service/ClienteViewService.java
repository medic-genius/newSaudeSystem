package br.com.medic.medicsystem.main.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.ClienteViewDAO;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;

@Stateless
public class ClienteViewService {
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("clienteview-dao")
	private ClienteViewDAO clienteViewDAO;
	
	public ClienteView getClienteView(Long idCliente) {

		logger.debug("Obtendo um cliente apartir da view...");
		return clienteViewDAO.getClienteView(idCliente);
	}

}
