package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.GuiaServicoService;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryGuiaServicoByServicoDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryServicoByStatusDTO;
import br.com.medic.medicsystem.persistence.model.GuiaServico;

@Path("/guia_servico")
public class GuiaServicoController {

	@Inject
	private GuiaServicoService guiaServicoService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GenericPaginateDTO list(
			@QueryParam("offset") @DefaultValue("1") int offset, 
			@QueryParam("limit") @DefaultValue("10") int limit,
			@QueryParam("id") Long id,
			@QueryParam("id_guia") Long idGuia,
			@QueryParam("start_date") String startDate,
			@QueryParam("end_date") String endDate,
			@QueryParam("status") final List<Integer> lStatus,
			@QueryParam("id_servico") Long idServico,
			@QueryParam("id_conveniada") Long idConveniada) {
		return guiaServicoService.getItems(offset, limit, id, idGuia, startDate, endDate, lStatus, idServico, idConveniada);
	}
	
	@GET
	@Path("/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public GuiaServico getGuia(@PathParam("id") Long id) {
		return guiaServicoService.getGuiaServico(id);
	}

	@GET
	@Path("/summary_servico_by_status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryServicoByStatus(
			@QueryParam("id") Long id,
			@QueryParam("id_guia") Long idGuia,
			@QueryParam("start_date") String startDate,
			@QueryParam("end_date") String endDate,
			@QueryParam("status") final List<Integer> lStatus,
			@QueryParam("id_conveniada") Long idConveniada) {
		List<SummaryServicoByStatusDTO> lSummaryServicoByStatusDTO = guiaServicoService.getSummaryServicoByStatus(id, idGuia, startDate, endDate, lStatus, idConveniada);
		if (0 < lSummaryServicoByStatusDTO.size()) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(lSummaryServicoByStatusDTO).build();
		} else {
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	@GET
	@Path("/summary_guia_servico_by_servico")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryGuiaServicoByServico(
			@QueryParam("id") Long id,
			@QueryParam("id_guia") Long idGuia,
			@QueryParam("start_date") String startDate,
			@QueryParam("end_date") String endDate,
			@QueryParam("status") final List<Integer> lStatus,
			@QueryParam("id_conveniada") Long idConveniada) {
		List<SummaryGuiaServicoByServicoDTO> lSummaryGuiaServicoByServicoDTO = guiaServicoService.getSummaryGuiaServicoByServico(id, idGuia, startDate, endDate, lStatus, idConveniada);
		if (0 < lSummaryGuiaServicoByServicoDTO.size()) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(lSummaryGuiaServicoByServicoDTO).build();
		} else {
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
}
