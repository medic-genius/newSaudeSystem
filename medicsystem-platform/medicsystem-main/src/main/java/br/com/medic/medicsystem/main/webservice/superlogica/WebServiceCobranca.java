package br.com.medic.medicsystem.main.webservice.superlogica;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.medic.dashboard.main.enums.superlogica.Method;
import br.com.medic.medicsystem.main.util.superlogica.ConnectionFactorySuperLogica;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.superlogica.Cobranca;

public class WebServiceCobranca {
	
	String request = "https://api.superlogica.net/v2/financeiro";
	
	public JSONObject cadastrarCobrancaCliente(Cobranca cobranca, Date dtVencimento){
	
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		String line = "";
		List<String> response = new ArrayList<String>();
		HttpURLConnection connection = null;
		
		final String complementoURL = "/cobranca";
		
		try {
			String urlParameters = "ID_SACADO_SAC="+cobranca.getId_sacado_sac()
									+"&COMPO_RECEBIMENTO[0][ID_PRODUTO_PRD]="+cobranca.getProdutoServicoGeneric().getId_produto_prd()
									+"&COMPO_RECEBIMENTO[0][NM_QUANTIDADE_COMP]="+cobranca.getProdutoServicoGeneric().getNm_quantidade_comp()
									+"&COMPO_RECEBIMENTO[0][VL_UNITARIO_PRD]="+cobranca.getProdutoServicoGeneric().getVl_unitario_prd()
									+"&DT_VENCIMENTO_RECB="+sdf.format(dtVencimento)
									+"&ST_OBSERVACAOEXTERNA_RECB="+cobranca.getSt_observacaoexterna_recb();
									

									
									
									
			URL url = new URL(request+complementoURL);
			connection = ConnectionFactorySuperLogica.getConnection(url,urlParameters,Method.POST);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			while ((line = reader.readLine()) != null) {
			    response.add(line);
			}
			reader.close();
		} catch (Exception e) {
			System.out.println("Erro ao fazer a requisição");
			return null;
		} finally{
			connection.disconnect();

		}
		
		JSONObject json = new JSONObject(response.get(0).substring(1, response.get(0).length()-1));
		json.put("in_CodCliente", cobranca.getIn_CodCliente());
		json.put("in_idCliente", cobranca.getIn_idCliente());
		json.put("st_NomeCliente", cobranca.getSt_NomeCliente());
		json.put("in_idContrato", cobranca.getIdContrato());
		json.put("endereco", cobranca.getEndereco());
		json.put("enderecoComplemento", cobranca.getEnderecoComplemento());
		System.out.println(json);
		return json;
		
		
	}

	@SuppressWarnings("deprecation")
	public JSONObject imprimirCarne(Long idClienteBoletoSL, Contrato contrato, BigInteger idImpressaoBoletoSl){
		
		request = "https://mediclab.superlogica.net/financeiro/atual";
		
		final String complementoURL = "/cobranca/imprimir";
							
		String line = "";
		List<String> response = new ArrayList<String>();
		HttpURLConnection connection = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		Date dtFim = new Date();
		Date dtInicio = new Date();
		
		if(contrato.getDtTerminoContrato() != null && (contrato.getBoEmpresaCliente() == null || !contrato.getBoEmpresaCliente())){
			c.setTime(contrato.getDtTerminoContrato());
			c.add(Calendar.MONTH, -13);
			dtInicio = c.getTime();
			dtFim = contrato.getDtTerminoContrato();
		}else{			
			c.add(Calendar.MONTH, 3);
			dtFim = c.getTime();
			c.add(Calendar.MONTH, -9);
			dtInicio = c.getTime();
		}
			
		
		String emLote = "https%253A%252F%252Fmediclab.superlogica.net%252Fclients%252Ffinanceiro%252Fcobranca%252Findex%253F"
	   			+ "status%253Dpendentes"	   			
	   			+ "%2526apenasColunasPrincipais%253D1"
	   			+ "%2526comDadosDasUnidades%253D1"
	   			+ "%2526apenasCobrancaSemNossoNumeroFixo%253D1"
	   			+ "%2526tipoBoleto%253D1"
	   			+ "%2526salvar%253DImprimircarne"
	   			+ "%2526dtInicio%253D"
	   			+ URLEncoder.encode(sdf.format(dtInicio))
	   			//+ "06%2F03%2F2017"
	   			+ "%2526dtFim%253D"
	   			//+ "02%2F03%2F2018";
	   			+ URLEncoder.encode(sdf.format( dtFim ));
	   			
	   	if(idClienteBoletoSL != null)
			emLote += "%2526CLIENTES%255B%255D%253D" + idClienteBoletoSL;
	   		//emLote += "%2526CLIENTES%255B%255D%253D6612";
	   	
		//if(contrato.getPlano().getIdPlano_sl() != null)
		//	emLote += "%2526PLANOS%255B%255D%253D" + contrato.getPlano().getIdPlano_sl(); 
	  	  		
		try {
			String urlParameters = "ID_IMPRESSORA_IMPR=pdf"
								   + "&VERSO=0"
								   + "&OFICIO=1"
								   + "&emLote=" + emLote
								   + "&tipoBoleto=1"
								   + "&identificadorImpressao=" + idImpressaoBoletoSl;
								   
												
			System.out.println(urlParameters);						
			URL url = new URL(request+complementoURL);
			connection = ConnectionFactorySuperLogica.getConnection(url,urlParameters,Method.POST);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();			
			
			System.out.println(connection.getResponseCode());			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			while ((line = reader.readLine()) != null) {
			    //System.out.println(line);
			    response.add(line);
			}
			reader.close();
		} catch (Exception e) {
			System.out.println("Erro ao fazer a requisição");
			return null;
		} finally{
			connection.disconnect();
		}
		
		JSONObject json;
		try {
			json = new JSONObject(response.get(0));		
			System.out.println(json);
			return json;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
				
	}
	
	public byte[] downloadImpressao( Long idImpressao, BigInteger idimpressaoBoletoSl ){
		
		request = "https://mediclab.superlogica.net/clients/financeiro/documentos";
		
		final String complementoURL = "/download";

		HttpURLConnection connection = null;
				  		
		try {
			String urlParameters = "";
			if(idImpressao != null)
				urlParameters = "id="+idImpressao;
			else if(idimpressaoBoletoSl != null)
				urlParameters = "identificadorImpressao="+idimpressaoBoletoSl;
								
			URL url = new URL(request+complementoURL);
			connection = ConnectionFactorySuperLogica.getConnection(url,urlParameters,Method.GET);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			byte[] bytes = IOUtils.toByteArray(connection.getInputStream());
			
			return bytes;
		} catch (Exception e) {
			System.out.println("Erro ao fazer a requisição");
			return null;
		} finally{
			connection.disconnect();
		}		   			
					
	}

}
