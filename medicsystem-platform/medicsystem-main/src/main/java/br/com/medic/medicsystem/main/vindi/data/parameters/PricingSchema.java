package br.com.medic.medicsystem.main.vindi.data.parameters;

import java.util.List;

public class PricingSchema {
	private Number price;
	private Number minimum_price;
	private String schema_type;
	private List<PricingRange> pricing_ranges;
	
	public Number getPrice() {
		return price;
	}
	public void setPrice(Number price) {
		this.price = price;
	}
	public Number getMinimum_price() {
		return minimum_price;
	}
	public void setMinimum_price(Number minimum_price) {
		this.minimum_price = minimum_price;
	}
	public String getSchema_type() {
		return schema_type;
	}
	public void setSchema_type(String schema_type) {
		this.schema_type = schema_type;
	}
	public List<PricingRange> getPricing_ranges() {
		return pricing_ranges;
	}
	public void setPricing_ranges(List<PricingRange> pricing_ranges) {
		this.pricing_ranges = pricing_ranges;
	}
}
