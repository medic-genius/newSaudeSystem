package br.com.medic.dashboard.main.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.dashboard.main.service.MonitoramentoService;
import br.com.medic.dashboard.persistence.dto.MonitoramentoDTO;

@ApplicationScoped
@Path("dashboard/monitoramento")
public class MonitoramentoController {
	
	@Inject
	private MonitoramentoService monitoramentoService;
		
	@GET
	@Path("/profissional")
	@Produces(MediaType.APPLICATION_JSON)
	public MonitoramentoDTO getSmProfissional(
			@QueryParam("tolerancia") Integer tolerancia){
		
		return monitoramentoService.getMonitorProfissional(tolerancia);
	}
	
	@GET
	@Path("/atendimento")
	@Produces(MediaType.APPLICATION_JSON)
	public MonitoramentoDTO getSmAtendimento(
			@QueryParam("tolerancia") Integer tolerancia){
		
		return monitoramentoService.getMonitorAtendimento(tolerancia);
	}

}
