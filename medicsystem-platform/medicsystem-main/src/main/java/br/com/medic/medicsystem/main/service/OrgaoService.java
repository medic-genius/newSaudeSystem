package br.com.medic.medicsystem.main.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.GrupoOrgaoDAO;
import br.com.medic.medicsystem.persistence.dao.OrgaoDAO;
import br.com.medic.medicsystem.persistence.model.GrupoOrgao;
import br.com.medic.medicsystem.persistence.model.Orgao;

@Stateless
public class OrgaoService {

	@Inject
	private Logger logger;

	@Inject
	@Named("grupoOrgao-dao")
	private GrupoOrgaoDAO grupoOrgaoDAO;
	
	@Inject
	@Named("orgao-dao")
	private OrgaoDAO orgaoDAO;

	public List<Orgao> getAllOrgaos() {

		logger.debug("Obtendo lista de orgaos...");

		return orgaoDAO.getAllOrgaos();
	}

	public List<Orgao> getOrgaosByEsfera(Integer inEsfera) {

		logger.debug("Obtendo lista de orgaos...");

		return orgaoDAO.getOrgaosByEsfera(inEsfera);
	}

	public Orgao updateOrgao(Orgao orgao) {
		
		GrupoOrgao grupoOrgao = grupoOrgaoDAO.getGrupo(orgao.getGrupoOrgao().getId());
		orgao.setGrupoOrgao(grupoOrgao);
		
		orgao = orgaoDAO.update(orgao);
		
		return orgao;
	}

	public Orgao saveOrgao(Orgao orgao) {
		
		orgaoDAO.persist(orgao);

		return orgao;
	}

	public void inativarOrgao(Long id) {
		
		Orgao orgao = orgaoDAO.searchByKey(Orgao.class, id);
		orgao.setDtExclusao(new Date());
		
		orgao = orgaoDAO.update(orgao);
	}
	
}
