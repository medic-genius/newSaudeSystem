package br.com.medic.medicsystem.main.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.bson.Document;
import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import br.com.medic.medicsystem.main.util.ResponseData;
import br.com.medic.medicsystem.main.util.TipoMensagem;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.EnvioSmsDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dto.InformativoDTO;
import br.com.medic.medicsystem.persistence.dto.SMSDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.EnvioSMS;
import br.com.medic.medicsystem.persistence.security.MongoResource;
import br.com.medic.medicsystem.persistence.security.dto.AuditMessageObject;
import br.com.medic.medicsystem.sms.bean.ListResource;
import br.com.medic.medicsystem.sms.bean.Response;
import br.com.medic.medicsystem.sms.enumerator.LayoutTypeEnum;
import br.com.medic.medicsystem.sms.enumerator.TypeService;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;
import br.com.medic.medicsystem.sms.service.MenssageBarateiroService;
import br.com.medic.medicsystem.sms.service.MultipleMessageService;
import br.com.medic.medicsystem.sms.service.base.BaseService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
public class SMSService {

	@Inject
	private Logger logger;
	
	private final String AVALIACAO_CONSULTA = "Ola %s, o que achou do seu atendimento medico? Responda gratis este SMS com uma nota de 0 a 10. Obrigado";

	private final String BR_CODE = "55";

	private final String MAO_CODE = "92";

	private final String NINE_DIGIT = "9";
	
	private final String MAISCONSULTA = "MAIS CONSULTA";
	
	private final Integer DIAS_ADIANTE_ENVIO_SMS = 0;

	@Inject
	MultipleMessageService mms;

	@Inject
	MenssageBarateiroService mmsBarateiro;
	
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("enviosms-dao")
	private EnvioSmsDAO envioSmsDao;
	

	SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");

	@Inject
	private MongoResource mongo;

	public void setMensalidadeDAO(MensalidadeDAO mensalidadeDAO) {
		this.mensalidadeDAO = mensalidadeDAO;
	}
	
	private String getDataAgendamentoEnvioSMSBarateiro(Integer incremento)
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Date proximo = getProximoDia(incremento);
		Calendar proximoDiaDataMarcada = Calendar.getInstance(locale);
		proximoDiaDataMarcada.setTime(proximo);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		return format.format(proximoDiaDataMarcada.getTime());

	}
	
	private String geHoraAgendamentoEnvioSMSBarateiro()
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Calendar horaEnvio = Calendar.getInstance(locale);
		horaEnvio.setTime(new Date());
//		horaEnvio.add(Calendar.HOUR_OF_DAY, 2);
		horaEnvio.add(Calendar.MINUTE, 1);

		SimpleDateFormat format = new SimpleDateFormat("HH:mm");

		return format.format(horaEnvio.getTime());

	}

	@SuppressWarnings("unchecked")
	private void persistOnLog(AuditMessageObject aMsg) {
		HashMap<String, Object> result;
		try {
			ObjectMapper mapper = new ObjectMapper();
			// Object to JSON in String
			String jsonInString = mapper.writeValueAsString(aMsg);
			result = new ObjectMapper().readValue(jsonInString, HashMap.class);
			mongo.saveDocumentLogSms(new Document(result));
		} catch (IOException e) {
			logger.error("Nao foi possivel registrar o log no mongo", e);
		}
	}

	private boolean isSuccessfulResponse(int tipoEnvio, Object response) {
		if(tipoEnvio == 0) {
			String res = (String) response;
			try {
				JSONParser parser = new JSONParser();
				JSONObject result = (JSONObject) parser.parse(res);
				if(!result.containsKey("status")) {
					return false;
				}
				Integer stt = Integer.valueOf(result.get("status").toString());
				return stt.intValue() == 1;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		} else if(tipoEnvio == 1) {
			Response r = (Response) response;
			return !r.isError();
		}
		return false;
	}

	private void logMessageSendingInformativoGeneral(TipoMensagem tipo, int tipoEnvio, 
			List<InformativoDTO> receivers, String content, List<?> response) {
		String params[] = content.split("\n");
		int index = 0;
		for(InformativoDTO rcv : receivers) {
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getIdCliente());
			aMsg.setTipoMensagem(tipo.ordinal());
			
			String num = null;
			if(rcv.getNrCelular() != null) {
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					String p[] = params[index].split(";");
					
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrCelular());
					String msg = p[1];
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					++index;
				}
			}
			if(rcv.getNrTelefone() != null) {
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					String p[] = params[index].split(";");
					
					aMsg.setEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					aMsg.setNrTelefone(rcv.getNrTelefone());
					String msg = p[1];
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					++index;
				}
			}
		}
	}
	
	public ResponseData<String, List<?>> enviarInformativoSMS(
			List<InformativoDTO> listaInformativo, Integer tipoEnvio)
			throws ClientHumanException, ParseException {
		return this.enviarInformativoSMS(listaInformativo, tipoEnvio, null);
	}
	
	public ResponseData<String, List<?>> enviarInformativoSMS(
			List<InformativoDTO> listaInformativo, Integer tipoEnvio,
			String nmEmpresa) throws ClientHumanException, ParseException {
		logger.info("Preparando e Enviando mensagens");

		List<InformativoDTO> result = listaInformativo;

		if (result != null && result.size() > 0) {
			
			if(tipoEnvio == null || tipoEnvio.equals(1)) {
				String content = constroiMMSInformativos(result, nmEmpresa);
				if (content == null){
					content = "ErroCadastro";
					List<Response> response = new ArrayList<Response>();
					String sms = "001...Erro no cadastro";
					Response resp = new Response(sms);
					response.add(resp);
					return new ResponseData<String, List<?>>(content, response);
				}else{
					ListResource message = new ListResource(content,
							LayoutTypeEnum.TYPE_E.getType());

					logger.debug("Mensagens Enviadas: " + message);

					List<Response> response = mms.send(message);
					
					if(response != null && response.size() > 0){
//						logMessageSendingInformativoGeneral(TipoMensagem.INFORMATIVO, 
//								1, result, content, response);
					}
					

				//	persistMessageSendingCliente(TipoMensagem.BOAS_VINDAS_CLIENTE, 1, result, content, response);

					logger.debug("SMS Gateway Response: " + response);

					return new ResponseData<String, List<?>>(content, response);
				}
			}else{
				
				StringBuffer content = new StringBuffer();
				List<String> response = new ArrayList<String>();
				
				for (InformativoDTO objects : listaInformativo) {
					

					String numeroCelular = phoneNumberCleanupBarateiro(objects.getNrCelular());
					if(numeroCelular != null) {
						try {
							String mensagem = null;
							if(nmEmpresa != null && !nmEmpresa.isEmpty()) {
								mensagem = nmEmpresa + ": ";
							} else {
								mensagem = MAISCONSULTA + ": ";
							}
							mensagem += getSMSMessage(objects.getNmInformativo());
							
							String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
							String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

							String r = mmsBarateiro.send(mensagem, numeroCelular, dtEnvio, hrEnvio,TypeService.SHORTCODE_2W);
//							Thread.sleep(5000);
							response.add(r);
							content.append(numeroCelular+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					String numeroTelefone = phoneNumberCleanupBarateiro(objects.getNrTelefone());
					if (numeroTelefone != null) {
						if (!numeroTelefone.equals(numeroCelular)) {
							try {
								String mensagem = null;
								if(nmEmpresa != null && !nmEmpresa.isEmpty()) {
									mensagem = nmEmpresa + ": ";
								} else {
									mensagem = MAISCONSULTA + ": ";
								}
								mensagem += getSMSMessage(objects.getNmInformativo());
								
								String dtEnvio = getDataAgendamentoEnvioSMSBarateiro(DIAS_ADIANTE_ENVIO_SMS);
								String hrEnvio = geHoraAgendamentoEnvioSMSBarateiro();

								String r = mmsBarateiro.send(mensagem, numeroTelefone, dtEnvio, hrEnvio,TypeService.SHORTCODE_2W);
//								Thread.sleep(5000);
								response.add(r);
								content.append(numeroTelefone+";" + mensagem+";" + dtEnvio+";" + hrEnvio+"\n");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}	
				}
				
				String ctnt = content.toString();
				return new ResponseData<String, List<?>>(ctnt, response);
				
				
			}
			
			
		}
		logger.info("Nenhum informativo foi encontrado");
		return null;
	}

	private Date getProximoDia(Integer incremento) {
		Calendar vencimento = Calendar.getInstance();
		vencimento.add(Calendar.DAY_OF_MONTH, incremento);
		return vencimento.getTime();
	}
	
	private String constroiMMSInformativos(List<InformativoDTO> result) throws ParseException {
		return this.constroiMMSInformativos(result, null);
	}
	
	private String constroiMMSInformativos(List<InformativoDTO> result, String nmEmpresa)
			throws ParseException {
		StringBuffer content = new StringBuffer();
		for (InformativoDTO object : result) {
			String numeroCelular = phoneNumberCleanup(object.getNrCelular());
			if (numeroCelular != null) {
				constroiMMSTypeInformativo(content, numeroCelular, object, nmEmpresa);
			}

			String numeroTelefone = phoneNumberCleanup(object
					.getNrTelefone());
			if (numeroTelefone != null) {
				if (!numeroTelefone.equals(numeroCelular)) {
					constroiMMSTypeInformativo(content, numeroTelefone,
							object, nmEmpresa);
				}
			}
		}

		if (!content.toString().isEmpty()) {
			return content.substring(0, content.length() - 2);
		}
		return null;
	}
	
	private String constroiMMSAvaliacaoAtendimento(Collection<Agendamento> result) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		StringBuffer content =  new StringBuffer();
		
		for(Agendamento ag : result) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(sdf.format(new Date())));
			cal.add(Calendar.MINUTE, 1);
			SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String dataAgendamentoFormatado = formater.format(cal.getTime());
			
			String numeroCelular = phoneNumberCleanup(ag.getCliente().getNrCelular());
			if (numeroCelular != null) {
				constroiMMSTypeAvaliacaoAtendimento(content, numeroCelular, ag, dataAgendamentoFormatado);
			}

			String numeroTelefone = phoneNumberCleanup(ag.getCliente().getNrTelefone());
			if (numeroTelefone != null && !numeroTelefone.equals(numeroCelular)) {
				constroiMMSTypeAvaliacaoAtendimento(content, numeroTelefone, ag, dataAgendamentoFormatado);
			}
		}
		String ctnt = content.toString();
		if(!ctnt.isEmpty()) {
			return ctnt.substring(0, ctnt.length()-2);
		}
		return null;
	}
	
	private StringBuffer constroiMMSTypeAvaliacaoAtendimento(StringBuffer content, String numeroCelular, 
			Agendamento object, String dataAgendamentoFormatado) throws ParseException {
		String nmCliente;
		if(object.getDependente() != null) {
			nmCliente = object.getDependente().getNmDependente();
		} else {
			nmCliente = object.getCliente().getNmCliente();	
		}
		String msg = getSMSMessage(AVALIACAO_CONSULTA, getNomeSimplificado(nmCliente));
		return buildGenericMMSTypeE(
				content,
				numeroCelular,
				msg,
				dataAgendamentoFormatado,
				MAISCONSULTA);
	}
	
	private StringBuffer constroiMMSTypeInformativo(StringBuffer content,
			String numero, InformativoDTO object, String nmEmpresa) throws ParseException {
		return buildGenericMMSTypeE(
				content,
				numero,
				object.getNmInformativo(),
				getEnvioSMSInformativo(0),
				nmEmpresa);
	}

	private StringBuffer buildGenericMMSTypeE(StringBuffer content,
			String number, String message, Object schedule, String nmEmpresa)
					throws ParseException {
		content.append(number);
		content.append(";");
		content.append(message);
		content.append(";");
		content.append(generateShortRandomUUID());
		if(nmEmpresa != null && !nmEmpresa.isEmpty()) {
			content.append(";"+nmEmpresa+";");
		} else {
			content.append(";"+MAISCONSULTA+";");
		}
		content.append(schedule);
		content.append("\n");
		return content;
	}

	private Object getEnvioSMSInformativo(Integer incremento)
			throws ParseException {
		Locale locale = new Locale("pt", "BR");
		Date proximo = getProximoDia(incremento);
		Calendar proximoDiaDataMarcada = Calendar.getInstance(locale);
		proximoDiaDataMarcada.setTime(proximo);
		//proximoDiaDataMarcada.set(Calendar.HOUR_OF_DAY, 10);
		proximoDiaDataMarcada.add(Calendar.MINUTE, 1);
		//proximoDiaDataMarcada.set(Calendar.SECOND, 0);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		return format.format(proximoDiaDataMarcada.getTime());
	}

	public static Object getNomeSimplificado(String nomeCliente) {
		nomeCliente = nomeCliente.trim();
		if (nomeCliente != null) {
			if (nomeCliente.indexOf(" ") != -1) {
				return nomeCliente.split(" ")[0];
			}
		}
		return nomeCliente;
	}

	public String phoneNumberCleanup(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");
			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {
				if (numberStr.length() == 8) {
					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					return BR_CODE + MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					return BR_CODE + MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					return BR_CODE + numberStr;
				}
			}
//			System.out.println("Numero não reconhecido: "+numberStr);
		}
		return null;
	}

	public String phoneNumberCleanupBarateiro(String numberStr) {
		if (numberStr != null) {
			numberStr = numberStr.replaceAll("\\s", "");
			numberStr = numberStr.trim();
			numberStr = numberStr.replaceAll("[^0-9]", "");
			if (numberStr.length() >= 8
					&& Integer.parseInt(String.valueOf(numberStr.charAt(0))) > 7) {
				if (numberStr.length() == 8) {
					return  MAO_CODE + NINE_DIGIT + numberStr;
				} else if (numberStr.length() == 9 && Integer.parseInt(String.valueOf(numberStr.charAt(1))) > 7) {
					return  MAO_CODE + numberStr;
				} else if (numberStr.length() == 10 && Integer.parseInt(String.valueOf(numberStr.charAt(2))) > 7) {
					return MAO_CODE + NINE_DIGIT + numberStr.substring(2);
				} else if (numberStr.length() == 11 && Integer.parseInt(String.valueOf(numberStr.charAt(3))) > 7) {
					return  numberStr;
				}
			}
//			System.out.println("Numero não reconhecido: "+numberStr);
		}
		return null;
	}

	// TODO Sistema de recuperacao de mensagem aprimorado
	private String getSMSMessage(String message, Object... args) {

		return String.format(message, args);
	}

	private String generateShortRandomUUID() {
		UUID uuid = UUID.randomUUID();
		long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
		return Long.toString(l, Character.MAX_RADIX);
	}

	public static String generateNextRandomID() {
		Random rand = new Random();
		return Long.toString(rand.nextInt(BaseService.ID_MAX_LENGTH));
	}
	
	public List<JSONObject> enviarInformativoFaltaMedico(
			List<InformativoDTO> lista) throws ParseException, ClientHumanException {
		if(lista != null && lista.size() > 0) {
			String content = constroiMMSInformativos(lista);
			if(content != null) {
				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());

				logger.debug("Mensagens Enviadas: " + message);

				List<Response> response = mms.send(message);
//				List<Response> response = new ArrayList<Response>();
//				String res[] = content.split("\n");
//				for(int i = 0; i < res.length; ++i) {
//					Response r = new Response();
//					r.setError(false);
//					r.setReturnCode("000");
//					r.setReturnDescription("No error");
//					response.add(r);
//				}
				
				logger.debug("SMS Gateway Response: " + response);
				
				return persistAndLogEnvioSms(TipoMensagem.NOTIFICACAO_FALTA_MEDICO, 1, 
						lista, content, response);
				
//				persistMessageSendingInformativoGeneral(
//						TipoMensagem.NOTIFICACAO_FALTA_MEDICO, 1, lista, content, response);
//
//				return saveEnvioSms(TipoMensagem.NOTIFICACAO_FALTA_MEDICO, 1, 
//						lista, content, response);
			}
		}
		logger.info("Nenhum informativo foi encontrado");
		return null;
	}
	
	@SuppressWarnings({"unchecked","unused"})
	private List<JSONObject> saveEnvioSms(TipoMensagem tipo, int tipoEnvio, 
			List<InformativoDTO> receivers, String content, List<?> response) {
		String params[] = content.split("\n");
		int index = 0;
		List<JSONObject> info = new ArrayList<JSONObject>();

		for(InformativoDTO rcv : receivers) {
			JSONObject agend = new JSONObject();
			agend.put("idAgendamento", rcv.getIdAgendamento());
			agend.put("idCliente", rcv.getIdCliente());
			JSONArray contacts = new JSONArray();
			String num = null;
			if(rcv.getNrCelular() != null) {
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(rcv.getIdAgendamento());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipo.ordinal());
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					String p[] = params[index].split(";");
					envio.setBoEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(num);
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(rcv.getNrCelular());
				}
				envio.setMensagem(rcv.getNmInformativo());
				envioSmsDao.persist(envio);
				
				JSONObject contact = new JSONObject();
				contact.put("nrTelefone", rcv.getNrCelular());
				contact.put("boEnviado", envio.getBoEnviado());
				contacts.add(contact);
			}
			if(rcv.getNrTelefone() != null) {
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(rcv.getIdAgendamento());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipo.ordinal());
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					String p[] = params[index].split(";");
					envio.setBoEnviado(isSuccessfulResponse(tipoEnvio, response.get(index)));
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(telNum);
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(rcv.getNrTelefone());
				}
				envio.setMensagem(rcv.getNmInformativo());
				envioSmsDao.persist(envio);
				
				JSONObject contact = new JSONObject();
				contact.put("nrTelefone", rcv.getNrTelefone());
				contact.put("boEnviado", envio.getBoEnviado());
				contacts.add(contact);
			}
			agend.put("contatos", contacts);
			info.add(agend);
		}
		return info;
	}
	
	@SuppressWarnings("unchecked")
	private List<JSONObject> persistAndLogEnvioSms(TipoMensagem tipo, int tipoEnvio, 
			List<InformativoDTO> receivers, String content, List<?> response) {
		String params[] = content.split("\n");
		int index = 0;
		List<JSONObject> info = new ArrayList<JSONObject>();

		for(InformativoDTO rcv : receivers) {
			JSONObject agend = new JSONObject();
			agend.put("idAgendamento", rcv.getIdAgendamento());
			agend.put("idCliente", rcv.getIdCliente());
			JSONArray contacts = new JSONArray();
			
			// Objeto de log SMS
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setIdCliente(rcv.getIdCliente());
			aMsg.setTipoMensagem(tipo.ordinal());
			String num = null;
			if(rcv.getNrCelular() != null) {
				// Objeto de envio SMS
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(rcv.getIdAgendamento());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipo.ordinal());
				num = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrCelular()) : phoneNumberCleanupBarateiro(rcv.getNrCelular());
				if(num != null) {
					// indices de p: 0 = numero de cel.; 1 = mensagem enviada; 2 = id campanha; 
					String p[] = params[index].split(";");
					boolean hasSuccess = isSuccessfulResponse(tipoEnvio, response.get(index));
					
					// faz log de envio apenas se numero for válido
					aMsg.setEnviado(hasSuccess);
					aMsg.setNrTelefone(num);
					String msg = p[1];
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					
					envio.setBoEnviado(hasSuccess);
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(num);
					
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(rcv.getNrCelular());
				}
				envio.setMensagem(rcv.getNmInformativo());
				envioSmsDao.persist(envio);
				
				JSONObject contact = new JSONObject();
				contact.put("nrTelefone", rcv.getNrCelular());
				contact.put("boEnviado", envio.getBoEnviado());
				contacts.add(contact);
			}
			
			if(rcv.getNrTelefone() != null) {
				// objeto de envio SMS
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(rcv.getIdAgendamento());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipo.ordinal());
				
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(rcv.getNrTelefone()) : phoneNumberCleanupBarateiro(rcv.getNrTelefone());
				if(telNum != null && !telNum.equals(num)) {
					String p[] = params[index].split(";");
					boolean hasSuccess = isSuccessfulResponse(tipoEnvio, response.get(index));
					
					// faz log de envio apenas se numero for válido
					aMsg.setEnviado(hasSuccess);
					aMsg.setNrTelefone(telNum);
					String msg = p[1];
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					
					envio.setBoEnviado(hasSuccess);
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(telNum);
					
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(rcv.getNrTelefone());
				}
				envio.setMensagem(rcv.getNmInformativo());
				envioSmsDao.persist(envio);
				
				JSONObject contact = new JSONObject();
				contact.put("nrTelefone", rcv.getNrTelefone());
				contact.put("boEnviado", envio.getBoEnviado());
				contacts.add(contact);
			}
			agend.put("contatos", contacts);
			info.add(agend);
		}
		return info;
	}
	
	public void enviarSmsAvaliacaoConsulta(Agendamento agendamento) throws ParseException, ClientHumanException {
		ArrayList<Agendamento> list = new ArrayList<Agendamento>();
		list.add(agendamento);
		this.enviarSmsAvaliacaoConsulta(list);
	}
	
	public void enviarSmsAvaliacaoConsulta(List<Agendamento> lista) throws ParseException, ClientHumanException {
		if(lista != null && lista.size() > 0) {
			String content = constroiMMSAvaliacaoAtendimento(lista);
			if(content != null) {
				ListResource message = new ListResource(content,
						LayoutTypeEnum.TYPE_E.getType());
				
				List<Response> response = mms.send(message);
				logger.debug("Mensagens Enviadas: " + message);
				
//				List<Response> response = new ArrayList<Response>();
//				for(int i = 0; i < lista.size(); ++i) {
//					Response r = new Response();
//					r.setError(false);
//					r.setReturnCode("000");
//					r.setReturnDescription("No error");
//					response.add(r);
//				}
				
				logger.debug("SMS Gateway Response: " + response);
				
				if(response != null)
					persistAndLogEnvioSmsAvaliacao(TipoMensagem.AVALIACAO_ATENDIMENTO, 1, 
							lista, content, response);
			}
		}
		logger.info("Nenhum informativo foi encontrado");
	}
	
	private void persistAndLogEnvioSmsAvaliacao(TipoMensagem tipoMensagem, int tipoEnvio, List<Agendamento> list,
			String content, List<?> response) {
		String params[] = content.split("\n");
		int index = 0;

		for(Agendamento ag : list) {
			// Objeto de log SMS
			AuditMessageObject aMsg = new AuditMessageObject();
			aMsg.setTipoEnvio(tipoEnvio);
			aMsg.setTipoMensagem(tipoMensagem.ordinal());
			
			String numCell, numTel, msg;
			if(ag.getDependente() != null) {
				aMsg.setIdCliente(ag.getDependente().getId());
				numCell = ag.getDependente().getNrCelular();
				numTel = ag.getDependente().getNrTelefone();
				msg = getSMSMessage(AVALIACAO_CONSULTA, getNomeSimplificado(ag.getDependente().getNmDependente()));
			} else {
				aMsg.setIdCliente(ag.getCliente().getId());
				numCell = ag.getCliente().getNrCelular();
				numTel = ag.getCliente().getNrTelefone();
				msg = getSMSMessage(AVALIACAO_CONSULTA, getNomeSimplificado(ag.getCliente().getNmCliente()));
			}
			
			String num = null;
			if(numCell != null) {
				// Objeto de envio SMS
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(ag.getId());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipoMensagem.ordinal());
				
				num = tipoEnvio == 1 ? phoneNumberCleanup(numCell) : phoneNumberCleanupBarateiro(numCell);
				
				if(num != null) {
					// indices de p: 0 = numero de cel.; 1 = mensagem enviada; 2 = id campanha;
					String p[] = params[index].split(";");
					
					boolean hasSuccess = isSuccessfulResponse(tipoEnvio, response.get(index));	
					// faz log de envio apenas se numero for válido
					aMsg.setEnviado(hasSuccess);
					aMsg.setNrTelefone(num);
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					
					envio.setBoEnviado(hasSuccess);
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(num);
					
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(numCell);
				}
				envio.setMensagem(msg);
				envioSmsDao.persist(envio);
			}
			
			if(numTel != null) {
				// objeto de envio SMS
				EnvioSMS envio = new EnvioSMS();
				envio.setIdAgendamento(ag.getId());
				envio.setDtEnvio(new Date());
				envio.setInTipoEnvio(tipoEnvio);
				envio.setInTipoMensagem(tipoMensagem.ordinal());
				
				String telNum = tipoEnvio == 1 ? phoneNumberCleanup(numTel) : phoneNumberCleanupBarateiro(numTel);
				
				if(telNum != null && !telNum.equals(num)) {
					// indices de p: 0 = numero de cel.; 1 = mensagem enviada; 2 = id campanha;
					String p[] = params[index].split(";");
					
					boolean hasSuccess = isSuccessfulResponse(tipoEnvio, response.get(index));	
					// faz log de envio apenas se numero for válido
					aMsg.setEnviado(hasSuccess);
					aMsg.setNrTelefone(telNum);
					aMsg.setConteudoMensagem(msg);
					// persiste em log Mongo
					persistOnLog(aMsg);
					
					envio.setBoEnviado(hasSuccess);
					envio.setIdCampanha(p[2]);
					envio.setNrTelefone(telNum);
					++index;
				} else {
					envio.setBoEnviado(false);
					envio.setIdCampanha(null);
					envio.setNrTelefone(numTel);
				}
				envio.setMensagem(msg);
				envioSmsDao.persist(envio);
			}
		}
	}
	
	private void processJsonObject(JSONObject obj) {
		System.out.println("smsresponsebody_________________________________"+obj.toJSONString());
		if(obj.containsKey("callbackMoRequest")) {
			JSONObject responseJson = (JSONObject) obj.get("callbackMoRequest");
			System.out.println("smsresponsebody_________________________________"+responseJson.toJSONString());
			
			String idCampanha = (String) responseJson.get("correlatedMessageSmsId");
			String responseTxt = (String) responseJson.get("body");
			envioSmsDao.persistSmsResponse(idCampanha, responseTxt);
		}
	}

	public void registerSmsResponse(String textBody) {
		try {
			org.json.JSONArray arr = new org.json.JSONArray(textBody);
			System.out.println("--> Array de json " + arr.toString());
			int arrLen = arr.length();
			for(int i = 0; i < arrLen; ++i) {
				Object o = arr.get(i);
				JSONObject obj = (JSONObject) (new JSONParser()).parse(o.toString());
				processJsonObject(obj);
			}
			
		} catch (Exception e) {
			try {
				JSONObject obj = (JSONObject) (new JSONParser()).parse(textBody);
				System.out.println("--> Objeto json  " + textBody);
				processJsonObject(obj);
			} catch (org.json.simple.parser.ParseException e1) {
				
			}
			
		}
	}
	

	
	public List<SMSDTO> getSMSByEspProfissional (Long idProfissional, Long idEspecialidade, Long idUnidade){
		return envioSmsDao.getAgendamentosAtendidosProfissional(idProfissional, idEspecialidade, idUnidade);
	}
}
