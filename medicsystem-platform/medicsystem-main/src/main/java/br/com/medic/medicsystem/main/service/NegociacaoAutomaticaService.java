package br.com.medic.medicsystem.main.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.NegociacaoAutomaticaViewDAO;
import br.com.medic.medicsystem.persistence.model.views.NegociacaoAutomaticaView;

public class NegociacaoAutomaticaService {
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("negociacaoautomaticaview-dao")
	private NegociacaoAutomaticaViewDAO negociacaoAutomaticaViewDAO;
	
		
	public List<NegociacaoAutomaticaView> getClientesNegociacaoView(Integer inFormaPagamento, Integer qtdMensalidade, 
			String dtInicio, String dtFim, Boolean boNegociadoAuto, Integer inTipoDesconto, Float vlDesconto) {

		logger.debug("Obtendo clientes e seus contratos a partir da view...");
		
		List<NegociacaoAutomaticaView> negociacaoAutomaticaView = 
				negociacaoAutomaticaViewDAO.getClientesNegociacaoView(inFormaPagamento, qtdMensalidade, dtInicio, dtFim, boNegociadoAuto);
		
		BigDecimal valorDesconto;
		MathContext mc = new MathContext(4); // 2 precision
		
		for(NegociacaoAutomaticaView nav : negociacaoAutomaticaView ){
			
			if(inTipoDesconto.equals(0)){ //Valor Fixo
				valorDesconto = new BigDecimal(vlDesconto.toString());
				nav.setVlDesconto(valorDesconto);
			}
			else{ // Valor em Porcentagem
				valorDesconto = new BigDecimal( nav.getVlTotal().toString() ).subtract( new BigDecimal(nav.getVlTotal() * vlDesconto), mc);
				nav.setVlDesconto(valorDesconto);
			}
		}
		
		return negociacaoAutomaticaView;
	}

}
