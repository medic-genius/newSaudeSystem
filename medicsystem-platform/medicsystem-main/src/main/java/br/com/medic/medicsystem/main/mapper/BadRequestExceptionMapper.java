package br.com.medic.medicsystem.main.mapper;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;
import org.jboss.resteasy.spi.BadRequestException;

/**
 * @author Phillip Furtado
 * 
 */
@Provider
public class BadRequestExceptionMapper implements
        ExceptionMapper<BadRequestException> {

	@Inject
	private Logger logger;

	@Override
	public Response toResponse(final BadRequestException exception) {

		logger.error(exception.getMessage(), exception);

		return Response
		        .status(Status.BAD_REQUEST)
		        .type(MediaType.APPLICATION_JSON)
		        .entity(new ExceptionMessage(ErrorType.DANGER, "Bad Request.",
		                ErrorMessage.INVALID_PARAMS_STR.getMessage())).build();

	}

}
