package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.AgenciaDAO;
import br.com.medic.medicsystem.persistence.dao.BancoDAO;
import br.com.medic.medicsystem.persistence.model.Agencia;
import br.com.medic.medicsystem.persistence.model.Banco;

@Stateless
public class BancarioService {

	@Inject
	private Logger logger;

	@Inject
	@Named("banco-dao")
	private BancoDAO bancoDAO;

	@Inject
	@Named("agencia-dao")
	private AgenciaDAO agenciaDAO;

	public List<Banco> getAllBancos() {

		logger.debug("Obtendo lista de bancos...");

		return bancoDAO.getAllBancos();
	}

	public List<Agencia> getAgencias(Long idBanco) {

		logger.debug("Obtendo lista de agencia de um banco...");

		return agenciaDAO.getAgencias(idBanco);
	}
	
	public Banco saveBanco(Banco banco) {
		
		bancoDAO.persist(banco);

		return banco;

	}
	

}
