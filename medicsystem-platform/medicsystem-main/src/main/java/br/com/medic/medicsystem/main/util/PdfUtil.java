package br.com.medic.medicsystem.main.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.pdfbox.multipdf.PDFMergerUtility;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.exception.BoletoException;
import br.com.medic.medicsystem.main.jasper.dto.EnderecoBoleto;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.persistence.dto.BoletoDTO;

public class PdfUtil {

	/**
	 * Pasta para armazenar os pdf temporariamente. Esta pasta deve ter permissão de escrita.
	 * */
//	private static final String PDF_FOLDER_PATH = "C:\\Users\\medicsaude_dev\\Documents\\testexls\\";
//	private static final String PDF_FOLDER_PATH = "C:\\Users\\MARCOS\\Documents\\boletos\\";
	private static final String PDF_FOLDER_PATH = "/opt/sistema/arquivos/boletos/";
	private static final String BASE_PDF_NAME = "BOLETO";
	
	/**
	 * Exemplo de funcionamento.
	 * */
	public static void main(String[] args)  {
		List<String> links = new ArrayList<String>();
		String pdfComposto = "lista_pdfs.pdf";
		links.add("http://mediclab.superlogica.net/clients/extranet/cobranca/id/30?filename=mediclab&accesskey=be9c730b76acd8d61f6d149f0e32f7c7&idCliente=30");
		links.add("http://mediclab.superlogica.net/clients/extranet/cobranca/id/30?filename=mediclab&accesskey=be9c730b76acd8d61f6d149f0e32f7c7&idCliente=30");
		links.add("http://mediclab.superlogica.net/clients/extranet/cobranca/id/30?filename=mediclab&accesskey=be9c730b76acd8d61f6d149f0e32f7c7&idCliente=30");
		links.add("http://mediclab.superlogica.net/clients/extranet/cobranca/id/30?filename=mediclab&accesskey=be9c730b76acd8d61f6d149f0e32f7c7&idCliente=30");
//		String nomeDoArquivoComTodosOsPdfsCombinados = getPdfMergedPathFromLinkList(links, PDF_FOLDER_PATH + pdfComposto, 10L);

	}
	
	/**
	 * Método para baixar uma lista de pdfs de acordo com os links enviados e combinar todos os pdfs em um só arquivo.
	 * 
	 * @param links - lista com os links dos arquivos em pdf.
	 * @param pdfMergeFilePath - nome do arquivo (com diretório) a ser gerado contendo toda a lista de pdfs <code> Ex.: C:/pfds/lista_pdfs.pdf</code>
	 * @return mergedFileName - nome do arquivo gerado <code> Ex.: C:/pfds/lista_pdfs.pdf</code>
	 * */	
	public static String getPdfMergedPathFromLinkList(List<BoletoDTO> links, String pdfMergeFilePath, Long idFuncionario){
		List<String> pathsPdfsBaixados = new ArrayList<String>();
		pathsPdfsBaixados = downloadPdfList(links, PDF_FOLDER_PATH, BASE_PDF_NAME, idFuncionario);
		String mergedFileName = mergePdfList(pathsPdfsBaixados, pdfMergeFilePath);
		deleteFileList(pathsPdfsBaixados);
		return mergedFileName;
	}
	
	public static byte[] getPdfMergedArrayFromLinkList(List<BoletoDTO> links, String pdfMergeFilePath, Long idFuncionario){
		String mergedFileName = getPdfMergedPathFromLinkList(links, pdfMergeFilePath, idFuncionario);
		byte[] array;
		try{
			File boletoFile = new File(mergedFileName);
			array = Files.readAllBytes(boletoFile.toPath());
			boletoFile.delete();
		}catch (Exception ex){
			ex.printStackTrace();
			throw new BoletoException("Erro na geração do PDF", ErrorType.DANGER);
		}
		return array;
	}
	
	private static void deleteFileList(List<String> listFiles){
		for (String string : listFiles) {
			File file = new File(string);
			if(file.exists()){
				file.delete();
			}
		}
	}
	
	private static List<String> downloadPdfList(List<BoletoDTO> listBoletos, String folder, String baseName, Long idFuncionario){
		List<String> pathsPdfsBaixados = new ArrayList<String>();
		for (BoletoDTO boleto : listBoletos) {
			
			String pdfIndexName = idFuncionario + "_" +System.currentTimeMillis();
			String link = boleto.getLink() != null ? boleto.getLink().replaceAll("http", "https") : null;		
			
			if(link != null){
				String pathPdfBaixado = downloadPdfFromLink(link, folder + baseName + pdfIndexName + ".pdf");
				pathsPdfsBaixados.add(pathPdfBaixado);
			}
			
			String pathPdfBaixadoEndereco = new PdfUtil().creatPdfEndereco(boleto, idFuncionario);			
			pathsPdfsBaixados.add(pathPdfBaixadoEndereco);
		}
		return pathsPdfsBaixados;
	}
	
	private  String creatPdfEndereco(BoletoDTO boletoDTO, Long idFuncionario){
		
		List<BoletoDTO>listBoletoDTO = new ArrayList<BoletoDTO>();
		listBoletoDTO.add(boletoDTO);
		EnderecoBoleto endereco = new EnderecoBoleto(listBoletoDTO);
		String namePath = PDF_FOLDER_PATH+"endereco"+idFuncionario+System.currentTimeMillis()+".pdf";
		
		ByteArrayOutputStream output = null;
		byte[] pdf = null;

		try {
			
			InputStream inputStream = getClass().getResourceAsStream(
			        "/jasper/EnvelopeNegociacaoAuto.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
			        .compileReport(design);

			HashMap<String, Object> params = new HashMap<String, Object>();
			
			

			JasperPrint jasperPrint = JasperFillManager.fillReport(
			        jasperReport, params, endereco);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);
			

			pdf = output.toByteArray();
			
			
			
			try {
				
				FileOutputStream fileOuputStream = new FileOutputStream(namePath);
				fileOuputStream.write(pdf);
				fileOuputStream.close();
				return namePath;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (JRException e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Orcamento",
			        ErrorType.ERROR, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e) {
				throw new AgendamentoException(
				        "Nao foi possivel gerar o relatorio em PDF - Orcamento",
				        ErrorType.ERROR, e);
			}

		}
		
		return null;
	}
	
	
	private static String downloadPdfFromLink(String pdfLink, String pdfFilePath){
		try{
			URL url = new URL(pdfLink);
			InputStream in = url.openStream();
			Files.copy(in, Paths.get(pdfFilePath), StandardCopyOption.REPLACE_EXISTING);
			in.close();
			return pdfFilePath;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Erro ao gerar boleto: " + pdfLink);
		}
	}
	
	private static String mergePdfList(List<String> pdfPathsToMerge, String mergedPdfPath) {
		PDFMergerUtility mergePdf = new PDFMergerUtility();

		try {
			for (String path : pdfPathsToMerge) {
				mergePdf.addSource(path);
			}
			mergePdf.setDestinationFileName(mergedPdfPath);
			mergePdf.mergeDocuments();
			return mergedPdfPath;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BoletoException("Erro ao criar lista de boletos", ErrorType.DANGER);
		}
		
	}

}
