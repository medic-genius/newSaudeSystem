package br.com.medic.medicsystem.main.appmobile.dto;

import java.util.List;

public class PagedListDTO {
	private List<Object> list;
	private Boolean hasMoreData;
	private Integer totalValuePages;

	
	
	public List<Object> getList() {
		return list;
	}
	public void setList(List<Object> list) {
		this.list = list;
	}
	public Boolean getHasMoreData() {
		return hasMoreData;
	}
	public void setHasMoreData(Boolean hasMoreData) {
		this.hasMoreData = hasMoreData;
	}
	public Integer getTotalValuePages() {
		return totalValuePages;
	}
	public void setTotalValuePages(Integer totalValuePages) {
		this.totalValuePages = totalValuePages;
	}
	
}
