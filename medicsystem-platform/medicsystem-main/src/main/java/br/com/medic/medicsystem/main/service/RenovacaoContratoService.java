package br.com.medic.medicsystem.main.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.jasper.JRDataSourceBoletoBancarioRenovacao;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;
import br.com.medic.medicsystem.persistence.dao.BoletoBancarioDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dto.BoletosImprimirDTO;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.views.MensalidadeBoletoImprimirView;
import br.com.medic.medicsystem.persistence.security.KeycloakHelper;
import br.com.medic.medicsystem.persistence.security.dto.KeycloakTokenDTO;
import br.com.medic.medicsystem.persistence.security.dto.LoginDTO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Stateless
public class RenovacaoContratoService {
	
	@Inject
	private Logger logger;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
		
	@Inject
	@Named("boletobancario-dao")
	private BoletoBancarioDAO boletoBancarioDAO;
	
	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	private BoletoServicePJB boletoServicePJB;
	
	@Inject
	private KeycloakHelper khelper;
	
	public Integer renovacaoAutomaticaJob(Long idEmpresaGrupo, LoginDTO loginDTO){
		
		logger.debug("Iniciando JOB " + idEmpresaGrupo);
		
		if(loginJobUser(loginDTO)){
		
			return contratoDAO.renovacaoAutomaticaJob(idEmpresaGrupo);
		}
		
		throw new WebApplicationException(Response.Status.FORBIDDEN);
	}
	
	private boolean loginJobUser(LoginDTO loginDTO) {

		logger.info("Autorizando execução do JOB...");

		khelper.initialize("keycloak-secondauth.json");

		KeycloakTokenDTO token = khelper.login(loginDTO.getUsername(),
		        loginDTO.getPassword());

		if (token == null) {
			return false;
		}

		String userID = khelper.getUserID(loginDTO.getUsername(),
		        token.getAccess_token());

		if (userID == null) {
			return false;
		}

		return khelper.hasRoles(userID, token.getAccess_token(), "job");

	}
	
	public Mensalidade getMensalidade(Long idMensalidade){
		
		return mensalidadeDAO.searchByKey(Mensalidade.class, idMensalidade);
	}
	
	public List<BoletosImprimirDTO> getMensalidadeBoletoImprimir(Long idContrato, Long idEmpresaGrupo) {
		
		List<MensalidadeBoletoImprimirView> mensalidadesBoletoImprimirView = boletoBancarioDAO.getMensalidadeBoletoImprimir(idContrato);
		
		List<BoletosImprimirDTO> boletosImprimirDTO = new ArrayList<BoletosImprimirDTO>();
		
		for (MensalidadeBoletoImprimirView mensalidadeBoletoImprimirView : mensalidadesBoletoImprimirView) {

			BoletosImprimirDTO boletoImprimirDTO = new BoletosImprimirDTO();
			
			boletoImprimirDTO.setIdCliente(mensalidadeBoletoImprimirView.getIdCliente());
			boletoImprimirDTO.setIdContrato(mensalidadeBoletoImprimirView.getIdContrato());
			boletoImprimirDTO.setIdMensalidade(mensalidadeBoletoImprimirView.getId());
			boletoImprimirDTO.setDtContrato(dateFormatada(mensalidadeBoletoImprimirView.getDtContrato()));
			boletoImprimirDTO.setDtVencimento(dateFormatada(mensalidadeBoletoImprimirView.getDtVencimento()));
			boletoImprimirDTO.setNmCliente(mensalidadeBoletoImprimirView.getNmCliente());
			boletoImprimirDTO.setNmPlano(mensalidadeBoletoImprimirView.getNmPlano());
			boletoImprimirDTO.setNrContrato(mensalidadeBoletoImprimirView.getNrContrato());
			boletoImprimirDTO.setNrMensalidade(mensalidadeBoletoImprimirView.getNrMensalidade());
			boletoImprimirDTO.setNrTalao(mensalidadeBoletoImprimirView.getNrTalao());
			boletoImprimirDTO.setSituacao(mensalidadeBoletoImprimirView.getSituacaoFormatado());
			boletoImprimirDTO.setVlMensalidade(mensalidadeBoletoImprimirView.getVlMensalidade());
			boletosImprimirDTO.add(boletoImprimirDTO);	
		}
		
		return boletosImprimirDTO;
	}
	
	public byte[] imprimirBoletosBancariosRenovacao(Long idCliente, Long idContrato, Long[] idMensalidades){
		
		ByteArrayOutputStream output = null;

		byte[] pdf = null;

		try {

			List<BoletoBancario> boletosBancarios = generateBoletosBancariosRenovacao(
					idCliente, idContrato, idMensalidades);

			JRDataSourceBoletoBancarioRenovacao boletoBancarioRelatorio = new JRDataSourceBoletoBancarioRenovacao(
					boletosBancarios);

			InputStream inputStream = getClass().getResourceAsStream(
					"/jasper/BoletoBancario_1.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
					.compileReport(design);

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logomarca", getClass().getResource("/figuras/header_caixa.jpg").getFile());

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, params, boletoBancarioRelatorio);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);

			pdf = output.toByteArray();

		} catch (JRException e) {

			throw new AgendamentoException(
					"Nao foi possivel gerar o relatorio em PDF - Boleto Mensalidade",
					ErrorType.ERROR, e);
		} catch (ParseException e) {

			e.printStackTrace();
		} finally {
			try {
				if (output != null) {

					output.close();
				}
			} catch (Exception e) {

				throw new AgendamentoException(
						"Nao foi possivel gerar o relatorio em PDF - Boleto Mensalidade",
						ErrorType.ERROR, e);
			}
		}

		return pdf;
	}
	private List<BoletoBancario> generateBoletosBancariosRenovacao(Long idCliente, Long idContrato, Long[] idMensalidades)
			throws ParseException {
		
		List<Mensalidade> mensalidades = null;
		
		if(idMensalidades.length > 0){
			
			mensalidades = getMensalidadesByClienteContrato(idCliente, idContrato, idMensalidades);
		} else {
			
			mensalidades = getMensalidadesByClienteContrato(idCliente, idContrato, null);
		}
		
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
		
		List<BoletoBancario> boletos = new ArrayList<BoletoBancario>();
		
		DateFormat formatoData = new SimpleDateFormat( "dd/MM/yyyy" );
		DecimalFormat formatadorDecimal = new DecimalFormat();
		formatadorDecimal.setMinimumFractionDigits( 2 );
		
		//Gerar Boleto Real Saude
		if ((!mensalidades.isEmpty())
				&& mensalidades.get(0).getContrato().getEmpresaGrupo().getId() == 9) {
			
			Date dtAtual = new Date();
			String cdBanco = "104", CodMoeda = "9", CodCedente = "247479", carteira = "1";
			String emissaoCedente = "4";
			String fator_venc = "";
			String vlDocumento = "";
			String nrNossoNumero = "";
			Long dtVencimento;
			String calc_cod = "";
			String cod_barra = "";
			Integer calc_dv = 0;
			String dvNossoNumero = "";
			String dvCodCedente = "4";
			String dvCampoLivre = "";
			String dvGeral = "";
			String campo1 = "", campo2 = "", campo3 = "", campo5 = "";
			String num_barra = "";
			String c1 = "", c2 = "", c3 = "";
			char[] vetor;
			char[] vetorAux;
			int i;
			int j;
			
			Long dtComparacao = formatoData.parse( "03/07/2000" ).getTime();
			dtComparacao /= 86400000;
			BoletoBancario boletoBancario;
			
			long nrBoleto = contratoDAO.getUltimoNumeroBoletoGerado();
			
			for (Mensalidade mensalidade : mensalidades) {
				
				if (mensalidade.getContrato().getInformaPagamento().equals(0)) {
					
					dtVencimento = mensalidade.getDataVencimento().getTime();
					dtVencimento /= 86400000;
					fator_venc = String.valueOf( ( dtVencimento - dtComparacao ) + 1000 );
					if ( fator_venc.trim().length() != 4 )
						fator_venc = "0000";
					
					vlDocumento = formatadorDecimal.format( mensalidade.getValorMensalidade() ).replace( ".", "" ).replace( ",", "" );
					while ( vlDocumento.trim().length() < 10 ) {
						vlDocumento = "0" + vlDocumento;
					}
					
					// Inserir Boleto Banc�rio
					boletoBancario = new BoletoBancario();
					boletoBancario.setDtInclusao(dtAtual);
					boletoBancario.setMensalidade(mensalidade);
					boletoBancario.setInTipoBoleto(0); // Mensalidade
					boletoBancario.setIdChaveBoleto(mensalidade.getId()); // Id da Mensalidade
					boletoBancario.setNrBoleto(nrBoleto);
					nrNossoNumero = String.valueOf(nrBoleto);
					
					while ( nrNossoNumero.trim().length() < 15 ) {
						nrNossoNumero = "0" + nrNossoNumero;
					}
					
					nrNossoNumero = "14" + nrNossoNumero;
					
					// In�cio do C�lculo do D�gito Verificador do Campo Livre
					calc_cod = CodCedente + dvCodCedente + nrNossoNumero.substring( 2,5 ) + carteira + nrNossoNumero.substring( 5, 8 ) + emissaoCedente + nrNossoNumero.substring( 8, 17 );
					
					i = calc_cod.length() - 1;
					j = 2;
					calc_dv = 0;
					vetor = calc_cod.toCharArray();
					while ( i >= 0 ) {
						if ( vetor[ i ] != '0' )
							calc_dv = calc_dv + ( Integer.parseInt( String.valueOf( vetor[ i ] ) ) * j );
						
						i--;
						j++;
						if ( j == 10 )
							j = 2;
					}
					
					calc_dv = calc_dv % 11;
					calc_dv = 11 - calc_dv;
					
					if ( calc_dv > 9 )
						calc_dv = 0;
					
					dvCampoLivre = String.valueOf( calc_dv );
					
					// Fim do C�lculo do D�gito Verificador do Campo Livre
					
					calc_cod = cdBanco + CodMoeda + fator_venc + vlDocumento
							+ CodCedente + dvCodCedente
							+ nrNossoNumero.substring(2, 5) + carteira
							+ nrNossoNumero.substring(5, 8) + emissaoCedente
							+ nrNossoNumero.substring(8, 17) + dvCampoLivre;
					
					// In�cio do C�lculo do D�gito Verificador Geral
					i = calc_cod.length() - 1;
					j = 2;
					calc_dv = 0;
					vetor = calc_cod.toCharArray();
					while (i >= 0) {
						if (vetor[i] != '0') {
							calc_dv = calc_dv
									+ (Integer.parseInt(String
											.valueOf(vetor[i])) * j);
						}
						
						i--;
						j++;
						
						if (j == 10){
							j = 2;
						}
					}
					
					calc_dv = calc_dv % 11;
					calc_dv = 11 - calc_dv;
					
					if ((calc_dv > 9) || (calc_dv.equals(0))) {
						calc_dv = 1;
					}
					
					dvGeral = String.valueOf( calc_dv );
					
					// Fim do C�lculo do D�gito Verificador Geral				
					
					// In�cio do C�lculo do D�gito Verificador do Nosso N�mero
					calc_cod = /*carteira + "4" +*/ nrNossoNumero;
					
					vetor = calc_cod.toCharArray();
					i = calc_cod.length() - 1;
					j = 2;
					calc_dv = 0;
					while (i >= 0) {
						if (vetor[i] != '0') {
							calc_dv = calc_dv
									+ (Integer.parseInt(String
											.valueOf(vetor[i])) * j);
						}
						i--;
						j++;
						if (j == 10) {
							j = 2;
						}
					}
					
					calc_dv = calc_dv % 11;
					calc_dv = 11 - calc_dv;
					
					if (calc_dv > 9){
						calc_dv = 0;
					}
					
					dvNossoNumero = String.valueOf( calc_dv );
					
					// Fim do C�lculo do D�gito Verificador do Nosso N�mero
					cod_barra = cdBanco + CodMoeda + dvGeral + fator_venc
							+ vlDocumento + CodCedente + dvCodCedente
							+ nrNossoNumero.substring(2, 5) + carteira
							+ nrNossoNumero.substring(5, 8) + emissaoCedente
							+ nrNossoNumero.substring(8, 17) + dvCampoLivre;
					
					// D�gito Verificador do Campo 1
					c1 = cod_barra.substring( 0, 4 ) + cod_barra.substring( 19, 24 );
					vetor = c1.toCharArray();
					i = c1.length() - 1;
					j = 2;
					calc_dv = 0;
					while (i >= 0) {
						if (vetor[i] != '0') {
							if ((Integer.parseInt(String.valueOf(vetor[i])) * j) < 10) {
								calc_dv = calc_dv
										+ (Integer.parseInt(String
												.valueOf(vetor[i])) * j);
							} else {
								vetorAux = String.valueOf(
										(Integer.parseInt(String
												.valueOf(vetor[i])) * j))
												.toCharArray();
								calc_dv = calc_dv
										+ Integer.parseInt(String
												.valueOf(vetorAux[0]));
								calc_dv = calc_dv
										+ Integer.parseInt(String
												.valueOf(vetorAux[1]));
							}
						}
						
						i--;
						if (j == 2) {
							j = 1;
						} else {
							j = 2;
						}
					}
					
					if (calc_dv < 10) {
						calc_dv = 10 - calc_dv;
					} else {
						calc_dv = calc_dv % 10;
						if (!calc_dv.equals(0)) {
							calc_dv = 10 - calc_dv;
						}
					}
					
					campo1 = c1.substring( 0, 5 ) + "." + c1.substring( 5, 9 ) + calc_dv.toString();
					// Fim Campo 1
					
					// D�gito Verificador do Campo 2
					c2 = cod_barra.substring( 24, 34 );
					vetor = c2.toCharArray();
					i = c2.length() - 1;
					j = 2;
					calc_dv = 0;
					while ( i >= 0 ) {
						if ( vetor[ i ] != '0' ) {
							if ( ( Integer.parseInt( String.valueOf( vetor[ i ] ) ) * j ) < 10 ){
								calc_dv = calc_dv + ( Integer.parseInt( String.valueOf( vetor[ i ] ) ) * j );
							} else {
								vetorAux = String.valueOf( ( Integer.parseInt( String.valueOf( vetor[ i ] ) ) * j ) ).toCharArray();
								calc_dv = calc_dv + Integer.parseInt( String.valueOf( vetorAux[ 0 ] ) );
								calc_dv = calc_dv + Integer.parseInt( String.valueOf( vetorAux[ 1 ] ) );
							}
						}
						
						i--;
						if ( j == 2 ){
							j = 1;
						} else{
							j = 2;
						}
					}
					
					if (calc_dv < 10) {
						calc_dv = 10 - calc_dv;
					} else {
						calc_dv = calc_dv % 10;
						if (!calc_dv.equals(0)) {
							calc_dv = 10 - calc_dv;
						}
					}
					
					campo2 = c2.substring( 0, 5 ) + "." + c2.substring( 5, 10 ) + calc_dv.toString();
					// Fim Campo 2
					
					// D�gito Verificador do Campo 3
					c3 = cod_barra.substring( 34, 44 );
					vetor = c3.toCharArray();
					i = c3.length() - 1;
					j = 2;
					calc_dv = 0;
					while (i >= 0) {
						if (vetor[i] != '0') {
							if ((Integer.parseInt(String.valueOf(vetor[i])) * j) < 10) {
								calc_dv = calc_dv
										+ (Integer.parseInt(String
												.valueOf(vetor[i])) * j);
							} else {
								vetorAux = String.valueOf(
										(Integer.parseInt(String
												.valueOf(vetor[i])) * j))
												.toCharArray();
								calc_dv = calc_dv
										+ Integer.parseInt(String
												.valueOf(vetorAux[0]));
								calc_dv = calc_dv
										+ Integer.parseInt(String
												.valueOf(vetorAux[1]));
							}
						}
						  
						i--;
						if (j == 2) {
							j = 1;
						} else {
							j = 2;
						}
					}
					
					if ( calc_dv < 10 ){
						calc_dv = 10 - calc_dv;
					} else {
						calc_dv = calc_dv % 10;
						if ( !calc_dv.equals( 0 ) ){
							calc_dv = 10 - calc_dv;
						}
					}
					
					campo3 = c3.substring( 0, 5 ) + "." + c3.substring( 5, 10 ) + calc_dv.toString();
					// Fim Campo 3
					
					campo5 = cod_barra.substring( 5, 9 ) + cod_barra.substring( 9, 19 );
					num_barra = campo1 + " " + campo2 + " " + campo3 + " " + dvGeral + " " + campo5;
					
					boletoBancario.setNrNossoNumero( nrNossoNumero + "-" + dvNossoNumero );
					boletoBancario.setNrCedente( CodCedente );
					boletoBancario.setNrCarteira( carteira );
					boletoBancario.setCdBarra( cod_barra );
					boletoBancario.setNrBarra( num_barra );
					boletoBancario.setMensalidade(mensalidade);
					boletoBancario.setIdChaveBoleto( mensalidade.getId() ); // Id da Mensalidade
					
					boletoBancarioDAO.persist(boletoBancario);
					nrBoleto++;
					boletoBancario.setCliente(cliente); // adicionar dados do cliente
					
					boletos.add( boletoBancario );
				}
			}
		}
		
		return boletos;
	}
	
	private List<Mensalidade> getMensalidadesByClienteContrato(Long idCliente, Long idContrato, Long[] idMensalidades){
		
		List<Mensalidade> mensalidades = mensalidadeDAO.listMensalidadeByClienteByContrato(idCliente, idContrato, idMensalidades); 
		
		return mensalidades;
	}

	public Collection<Contrato> getContratosRenovados(Long idEmpresaGrupo, String dtInicio, String dtFinal) {
		
		DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		
		Collection<Contrato> contratos = null;
		
		try {
			
			String dtInicioFormatado = null;
			String dtFimFormatado = null;
			
			if(dtInicio != null 
					&& dtFinal != null){
				dtInicioFormatado = outputFormat.format(inputFormat.parse(dtInicio));
				dtFimFormatado = outputFormat.format(inputFormat.parse(dtFinal));
			}
			
			contratos = boletoBancarioDAO.getContratos(idEmpresaGrupo, dtInicioFormatado, dtFimFormatado); 
		} catch (ParseException e) {
			
			throw new IllegalArgumentException(e.getMessage());
		}

		return contratos;
	}
	
	private String dateFormatada(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (date != null) {
			return sdf.format(date);
		}
		return null;
	}

	public String getBoletosRenovacao(Long idCliente, Long idContrato) {
		List<Mensalidade> listMensalidade = new ArrayList<Mensalidade>();
		listMensalidade = mensalidadeDAO.getMensalidadesBoletoForContratoRenovado(idContrato);
		
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente);
		try{
			for(Mensalidade m : listMensalidade){
				boletoServicePJB.registrarBoleto(m, cliente);
			}
			String linkCarne = boletoServicePJB.getLinkCarne(listMensalidade);
			if(linkCarne != null) {
				Contrato contrato = contratoDAO.getContrato(idContrato);
				if(contrato != null) {
					contrato.setBoAcaoBoleto(true);
					contratoDAO.update(contrato);
				}
			}
			return linkCarne;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
}

