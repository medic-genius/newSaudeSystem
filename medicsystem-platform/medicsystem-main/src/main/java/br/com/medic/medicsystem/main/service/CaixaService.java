package br.com.medic.medicsystem.main.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.exception.DespesaFinanceiroException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.CaixaCompartilhadoViewAcademiaDAO;
import br.com.medic.medicsystem.persistence.dao.CaixaCompartilhadoViewDAO;
import br.com.medic.medicsystem.persistence.dao.CaixaCompartilhadoViewDentalDAO;
import br.com.medic.medicsystem.persistence.dao.CaixaDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoCompartilhadoDAO;
import br.com.medic.medicsystem.persistence.dao.MovimentacaoContaDAO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.CaixaRetiradaDTO;
import br.com.medic.medicsystem.persistence.dto.MovimentacaoCaixaDTO;
import br.com.medic.medicsystem.persistence.model.Caixa;
import br.com.medic.medicsystem.persistence.model.CaixaCompartilhadoView;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.views.ConferenciaMensalidade;
import br.com.medic.medicsystem.persistence.model.views.ConferenciaParcela;
import br.com.medic.medicsystem.persistence.model.views.FormaPagamentoCaixa;
import br.com.medic.medicsystem.persistence.model.views.FormaPagamentoCartao;
import br.com.medic.medicsystem.persistence.model.views.MovimentacaoCaixaComportilhadoView;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Stateless
public class CaixaService {

	@Inject
	@Named("caixa-dao")
	private CaixaDAO caixaDAO;
	
	@Inject
	@Named("caixacompartilhadoview-dao")
	private CaixaCompartilhadoViewDAO caixaCompartilhadoMedicDAO;

	@Inject
	@Named("caixacompartilhadoviewdental-dao")
	private CaixaCompartilhadoViewDentalDAO caixaCompartilhadoDentalDAO;
	
	@Inject
	@Named("caixacompartilhadoviewacademia-dao")
	private CaixaCompartilhadoViewAcademiaDAO caixaCompartilhadoAcademiaDAO;
	
	@Inject
	@Named("empresagrupocompartilhado-dao")
	private EmpresaGrupoCompartilhadoDAO empresaGrupoCompartilhadoDAO;
	
	@Inject
	@Named("movimentacaoconta-dao")
	private MovimentacaoContaDAO movimentacaoContaDAO;

	@Inject
	private SecurityService securityService;
	
	public EmpresaGrupoCompartilhado getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(Long id){
		EmpresaGrupoCompartilhado empresa = empresaGrupoCompartilhadoDAO.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(id);
		return empresa;
	}
	
	public List<EmpresaGrupoCompartilhado> getEmpresasGrupoCompartilhado(){
		List<EmpresaGrupoCompartilhado> empresas = empresaGrupoCompartilhadoDAO.getAllEmpresasGrupoCompartilhado();
		return empresas;
	}
	
	public List<CaixaCompartilhadoView> getConferenciasCaixa(EmpresaGrupoCompartilhado empresa, String dtInicial, String dtFinal, Long idEmpresaFinanceiro, Long idUnidade, Long idConferente, Long idOperador){
		List<CaixaCompartilhadoView> list = null;
		
		Long idEmpresaGrupo = empresa.getIdEmpresaGrupoExterno();
		Date dtInicio = DateUtil.parseDate(dtInicial, DateUtil.DatePattern.DDMMAA.getPattern());
		Date dtFim = DateUtil.parseDate(dtFinal, DateUtil.DatePattern.DDMMAA.getPattern());
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();
		
		switch (baseDadosSistema) {
		case 0:
			list = caixaCompartilhadoMedicDAO.getConferenciasCaixa(dtInicio, dtFim, idEmpresaGrupo, idUnidade, idConferente, idOperador);
			break;
		case 1:
			list = caixaCompartilhadoAcademiaDAO.getConferenciasCaixa(dtInicio, dtFim, idEmpresaGrupo, idUnidade, idConferente, idOperador);
			break;
		case 2:
			list = caixaCompartilhadoDentalDAO.getConferenciasCaixa(dtInicio, dtFim, idEmpresaGrupo, idUnidade, idConferente, idOperador);
			break;
		default:
			break;
		}
		
		return list;
	}
	
	
	public List<BaseDTO> getUnidadesByEmpresaGrupo(Long idEmpresaGrupo, Integer baseDadosSistema){
		List<Unidade> list = null;
		
		switch (baseDadosSistema) {
		case 0:
			list = caixaCompartilhadoMedicDAO.getUnidadesByEmpresaGrupo(idEmpresaGrupo);
			break;
		case 1:
			list = caixaCompartilhadoAcademiaDAO.getUnidadesByEmpresaGrupo(idEmpresaGrupo);
			break;
		case 2:
			list = caixaCompartilhadoDentalDAO.getUnidadesByEmpresaGrupo(idEmpresaGrupo);
			break;
		default:
			break;
		}
		
		return getBaseDTOListFromUnidades(list);
	}
	
	private List<BaseDTO> getBaseDTOListFromUnidades(List<Unidade> unidades){
		if(unidades == null || unidades.isEmpty()){
			return null;
		}
		
		List<BaseDTO> dtos = new ArrayList<BaseDTO>();
		
		for (Unidade u : unidades) {
			dtos.add(new BaseDTO(u.getId(), u.getNmUnidade()));
		}
		
		return dtos;
	}

	private MovimentacaoCaixaDTO getMovimentacaoCaixaDTO(List<FormaPagamentoCaixa> pagamentosCaixa, List<FormaPagamentoCartao> pagamentosCartao,
			List<ConferenciaMensalidade> conferenciasMensalidade, List<ConferenciaParcela> conferenciasParcela, List<MovimentacaoCaixaComportilhadoView> movimentacoesCaixaCompartilhado){
		
		MovimentacaoCaixaDTO dto = new MovimentacaoCaixaDTO();
		dto.setConferenciaMensalidadeList(conferenciasMensalidade);
		dto.setConferenciaParcelaList(conferenciasParcela);
		dto.setMovimentacaoCaixaList(movimentacoesCaixaCompartilhado);
		
		Float vlAbertura = 0F;
		Float vlAbastecimento = 0F;
		Float vlRetirada = 0F;
		Float vlDevolucao = 0F;
		Float vlFechamento = 0F;
		Float totalEspecie = 0F;
		Float totalDebitoAutomatico = 0F;
		Float totalDinheiro = 0F;
		Float totalCheque = 0F;
		Float totalCartaoCredito = 0F;
		Float totalCartaoDebito = 0F;
		Float totalBaixa = 0F;
		Float totalConvenio = 0F;
		Float totalPagamentos = 0F;
		Float total = 0F;
		
		Float totalCreditoVisa = 0F;
		Float totalCreditoMasterCard = 0F;
		Float totalCreditoAmericanExpress = 0F;
		Float totalCreditoElo = 0F;
		Float totalCreditoDiners = 0F;
		Float totalCreditoMaestro = 0F;
		Float totalCreditoRedShop = 0F;
		
		Float totalDebitoVisa = 0F;
		Float totalDebitoMasterCard = 0F;
		Float totalDebitoElo = 0F;
		Float totalDebitoRedShop = 0F;
		
		for (MovimentacaoCaixaComportilhadoView m : movimentacoesCaixaCompartilhado) {
			
			switch (m.getInTipoOperacao()) {
			case ABERTURA:
				vlAbertura += m.getVlMovimentacaoCaixa();
				break;
			case ABASTECIMENTO:
				vlAbastecimento += m.getVlMovimentacaoCaixa();
				break;	
			case RETIRADA:
				vlRetirada += m.getVlMovimentacaoCaixa();
				break;
			case FECHAMENTO:
				vlFechamento += m.getVlMovimentacaoCaixa();
				break;
			case DEVOLUCAO:
				vlDevolucao += m.getVlMovimentacaoCaixa();
				break;
			default:
				break;
			}
			
			totalEspecie = ( vlAbertura + vlAbastecimento + totalDinheiro ) - ( vlRetirada + vlDevolucao );
			
		}
		
		if( pagamentosCaixa.size() > 0 ) {
			
			for (FormaPagamentoCaixa formaPagamentoCaixa : pagamentosCaixa) {
				switch (formaPagamentoCaixa.getInFormaPagamento() ) {
				case 1: // debito automatico
					totalDebitoAutomatico = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 2: //dinheiro
					totalDinheiro = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 3: //cheque
					totalCheque = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 4: // cartao de credito
					totalCartaoCredito = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 9: // cartao de debito
					totalCartaoDebito = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 14: // baixa no sistema
					totalBaixa = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				case 15: //convenio
					totalConvenio = formaPagamentoCaixa.getVlMovdetalhamento();
					break;
				default:
					break;
				}
			}
			
			totalEspecie = ( vlAbertura + vlAbastecimento + totalDinheiro ) - ( vlRetirada + vlDevolucao);
			totalPagamentos = ( totalDebitoAutomatico + totalDinheiro + totalCheque  + totalCartaoCredito + totalCartaoDebito + totalBaixa + totalConvenio );
			total = ( vlAbertura + vlAbastecimento + totalPagamentos ) - ( vlRetirada + vlDevolucao );
			
			if( pagamentosCartao.size() > 0 ) {
				for ( FormaPagamentoCartao formaPagamentoCartao: pagamentosCartao ) {
					if( formaPagamentoCartao.getInFormaPagamento().intValue() == 4 ) { // pagamento cartao de credito
						switch ( formaPagamentoCartao.getInAdministradoraCartaoCredito()  ) {
						case 0:
							totalCreditoAmericanExpress = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 1:
							totalCreditoDiners = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 2:
							totalCreditoMaestro = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 3:
							totalCreditoMasterCard = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 4:
							totalCreditoRedShop = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 5:
							totalCreditoVisa = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 6:
							totalCreditoElo = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						default:
							break;
						}
					} else if( formaPagamentoCartao.getInFormaPagamento().intValue() == 9 ) { // forma pagamento cartao de debito
						switch ( formaPagamentoCartao.getInAdministradoraCartaoDebito()  ) {
						case 0:
							totalDebitoMasterCard = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 1:
							totalDebitoRedShop = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 2:
							totalDebitoVisa = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						case 3:
							totalDebitoElo = formaPagamentoCartao.getVlMovdetalhamento();
							break;
						default:
							break;
						}
					}
				}
			}
		}
		
		dto.setVlAbertura(getFloat2DecimalPlaces(vlAbertura));
		dto.setVlAbastecimento(getFloat2DecimalPlaces(vlAbastecimento));
		dto.setVlRetirada(getFloat2DecimalPlaces(vlRetirada));
		dto.setVlDevolucao(getFloat2DecimalPlaces(vlDevolucao));
		dto.setVlFechamento(getFloat2DecimalPlaces(vlFechamento));
		
		dto.setTotalEspecie(getFloat2DecimalPlaces(totalEspecie));
		dto.setTotalDebitoAutomatico(getFloat2DecimalPlaces(totalDebitoAutomatico));
		dto.setTotalDinheiro(getFloat2DecimalPlaces(totalDinheiro));
		dto.setTotalCheque( getFloat2DecimalPlaces( totalCheque ) );
		dto.setTotalCartaoCredito(getFloat2DecimalPlaces(totalCartaoCredito));
		dto.setTotalCartaoDebito(getFloat2DecimalPlaces(totalCartaoDebito));
		dto.setTotalBaixa(getFloat2DecimalPlaces(totalBaixa));
		dto.setTotalConvenio(getFloat2DecimalPlaces(totalConvenio));
		dto.setTotalPagamentos(getFloat2DecimalPlaces(totalPagamentos));
		dto.setTotal(getFloat2DecimalPlaces(total));
		
		dto.setTotalCreditoVisa(getFloat2DecimalPlaces(totalCreditoVisa));
		dto.setTotalCreditoMasterCard(getFloat2DecimalPlaces(totalCreditoMasterCard));
		dto.setTotalCreditoAmericanExpress(getFloat2DecimalPlaces(totalCreditoAmericanExpress));
		dto.setTotalCreditoElo(getFloat2DecimalPlaces(totalCreditoElo));
		dto.setTotalCreditoDiners(getFloat2DecimalPlaces(totalCreditoDiners));
		dto.setTotalCreditoMaestro(getFloat2DecimalPlaces(totalCreditoMaestro));
		dto.setTotalCreditoRedShop(getFloat2DecimalPlaces(totalCreditoRedShop));
		
		dto.setTotalDebitoVisa(getFloat2DecimalPlaces(totalDebitoVisa));
		dto.setTotalDebitoMasterCard(getFloat2DecimalPlaces(totalDebitoMasterCard));
		dto.setTotalDebitoElo(getFloat2DecimalPlaces(totalDebitoElo));
		dto.setTotalDebitoRedShop(getFloat2DecimalPlaces(totalDebitoRedShop));
		
		return dto;
	}
	
	private float getFloat2DecimalPlaces(Float n){
		return (float) (Math.round(n * 100.0) / 100.00) ;
	}
	
	private MovimentacaoCaixaDTO getMovimentacaoCaixaMedic(Long idCaixa) {
		List<ConferenciaMensalidade> conferenciasMensalidade = caixaCompartilhadoMedicDAO.getMovimentacaoMensalidade(idCaixa);
		List<ConferenciaParcela> conferenciasParcela = caixaCompartilhadoMedicDAO.getMovimentacaoParcela(idCaixa );
		List<FormaPagamentoCaixa> pagamentosCaixa =  caixaCompartilhadoMedicDAO.getFormaPagamentoCaixa(idCaixa);
		List<FormaPagamentoCartao> pagamentosCartao = caixaCompartilhadoMedicDAO.getFormaPagamentoCartao(idCaixa);
		List<MovimentacaoCaixaComportilhadoView> movimentacoes = caixaCompartilhadoMedicDAO.getMovimentaoesCaixaCompartilhado(idCaixa);
		MovimentacaoCaixaDTO movCaixaDTO = getMovimentacaoCaixaDTO(pagamentosCaixa, pagamentosCartao, conferenciasMensalidade, conferenciasParcela, movimentacoes);
		return movCaixaDTO;
	}
	
	private MovimentacaoCaixaDTO getMovimentacaoCaixaAcademia(Long idCaixa) {
		List<ConferenciaMensalidade> conferenciasMensalidade = caixaCompartilhadoAcademiaDAO.getMovimentacaoMensalidade( idCaixa );
		List<ConferenciaParcela> conferenciasParcela = caixaCompartilhadoAcademiaDAO.getMovimentacaoParcela( idCaixa  );
		List<FormaPagamentoCaixa> pagamentosCaixa =  caixaCompartilhadoAcademiaDAO.getFormaPagamentoCaixa(idCaixa);
		List<FormaPagamentoCartao> pagamentosCartao = caixaCompartilhadoAcademiaDAO.getFormaPagamentoCartao(idCaixa);
		List<MovimentacaoCaixaComportilhadoView> movimentacoes = caixaCompartilhadoAcademiaDAO.getMovimentaoesCaixaCompartilhado(idCaixa);
		MovimentacaoCaixaDTO movCaixaDTO = getMovimentacaoCaixaDTO(pagamentosCaixa, pagamentosCartao, conferenciasMensalidade, conferenciasParcela, movimentacoes);
		return movCaixaDTO;
	}
	
	private MovimentacaoCaixaDTO getMovimentacaoCaixaDental(Long idCaixa) {
		List<ConferenciaMensalidade> conferenciasMensalidade = caixaCompartilhadoDentalDAO.getMovimentacaoMensalidade( idCaixa );
		List<ConferenciaParcela> conferenciasParcela = caixaCompartilhadoDentalDAO.getMovimentacaoParcela( idCaixa  );
		List<FormaPagamentoCaixa> pagamentosCaixa =  caixaCompartilhadoDentalDAO.getFormaPagamentoCaixa(idCaixa);
		List<FormaPagamentoCartao> pagamentosCartao = caixaCompartilhadoDentalDAO.getFormaPagamentoCartao(idCaixa);
		List<MovimentacaoCaixaComportilhadoView> movimentacoes = caixaCompartilhadoDentalDAO.getMovimentaoesCaixaCompartilhado(idCaixa);
		MovimentacaoCaixaDTO movCaixaDTO = getMovimentacaoCaixaDTO(pagamentosCaixa, pagamentosCartao, conferenciasMensalidade, conferenciasParcela, movimentacoes);
		return movCaixaDTO;
	}
	
	public List<BaseDTO> getOperadoresCaixa(Integer baseDadosSistema){
		List<BaseDTO> operadores = null;
		switch (baseDadosSistema) {
		case 0:
			operadores = caixaCompartilhadoMedicDAO.getOperadoresCaixa();
			break;
		case 1:
			operadores = caixaCompartilhadoAcademiaDAO.getOperadoresCaixa();
			break;
		case 2:
			operadores = caixaCompartilhadoDentalDAO.getOperadoresCaixa();
			break;
		default:
			break;
		}
		
		return operadores;
	}
	
	public MovimentacaoCaixaDTO getMovimentacaoCaixa(Integer baseDadosSistema, Long idCaixa) {
		MovimentacaoCaixaDTO movCaixaDTO = null;
		switch (baseDadosSistema) {
		case 0:
			movCaixaDTO = getMovimentacaoCaixaMedic(idCaixa);
			break;
		case 1:
			movCaixaDTO = getMovimentacaoCaixaAcademia(idCaixa);
			break;
		case 2:
			movCaixaDTO = getMovimentacaoCaixaDental(idCaixa);
			break;
		default:
			break;
		}
		return movCaixaDTO;
	}

	@SuppressWarnings("unused")
	private void conferirCaixa(Caixa caixa){
		caixa.setInstatus(1);
		caixa.setDtconferencia(new Date());
		Funcionario conferente = securityService.getFuncionarioLogado();
		caixa.setTbfuncionario1(conferente);
	}
	
	public Caixa saveConferenciaCaixa(Long idCaixa, Integer baseDadosSistema){
		Caixa caixa = null;
		switch (baseDadosSistema) {
		case 0:
			caixa  = caixaCompartilhadoMedicDAO.searchByKey(Caixa.class, idCaixa);
			caixaCompartilhadoMedicDAO.conferirCaixa( caixa, securityService.getUserLogin() );
			break;
		case 1:
			caixa  = caixaCompartilhadoAcademiaDAO.searchByKey(Caixa.class, idCaixa);
			caixaCompartilhadoAcademiaDAO.conferirCaixa( caixa, securityService.getUserLogin() );
			break;
		case 2:
			caixa  = caixaCompartilhadoDentalDAO.searchByKey(Caixa.class, idCaixa);
			caixaCompartilhadoDentalDAO.conferirCaixa( caixa, securityService.getUserLogin() );
			break;
		default:
			break;
		}
		//conferirCaixa(caixa);
		return caixa;
	}

	public Caixa desconferirCaixa(Long idCaixa, EmpresaGrupoCompartilhado empresa ) {
		
		Caixa caixa = null;
		
		switch ( empresa.getInSistemaExterno() ) {
			case 0:
				caixa = caixaCompartilhadoMedicDAO.searchByKey(Caixa.class, idCaixa);
				caixaCompartilhadoMedicDAO.desconferirCaixa( caixa );
				break;
			case 1:
				caixa = caixaCompartilhadoAcademiaDAO.searchByKey(Caixa.class, idCaixa);
				caixaCompartilhadoAcademiaDAO.desconferirCaixa( caixa );
				break;
			case 2:
				caixa = caixaCompartilhadoDentalDAO.searchByKey(Caixa.class, idCaixa);
				caixaCompartilhadoDentalDAO.desconferirCaixa( caixa );
				break;
	
			default:
				break;
		}
			
		return caixa;
	
	}
	
	public void excluirMovimentacaoDeCaixa( List<MovimentacaoConta> movimentacoes, Funcionario funcionarioLogado ) {
		movimentacaoContaDAO.excluirMovimentacaoDeCaixa( movimentacoes, funcionarioLogado );
	}
	
	public List<MovimentacaoConta> getMovimentacaoEntradaPorCaixa(Long idEmpresaFinanceiro, Long idCaixa) {
		
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		List<MovimentacaoConta> movimentacao = movimentacaoContaDAO.getMovimentacaoEntradaPorCaixa( idEmpresaFinanceiro, idCaixa);
		
		if( movimentacao != null && movimentacao.size() > 0  ) {
			return movimentacao;
		} else
			throw new DespesaFinanceiroException(props.getProperty("conferencia.movimentacaonotfound"), ErrorType.DANGER);
		
	}

	public List<CaixaRetiradaDTO> getRetiradaDetalhes(Long id, Integer dadosSistema) {
		
		List<CaixaRetiradaDTO> detalhes = null;
		switch ( dadosSistema ) {
		case 0:
			detalhes = caixaCompartilhadoMedicDAO.getRetiradaDetalhes(id);
			break;
		case 1:
			detalhes = caixaCompartilhadoAcademiaDAO.getRetiradaDetalhes(id);
			break;
		case 2:
			detalhes = caixaCompartilhadoDentalDAO.getRetiradaDetalhes(id);
			break;

		default:
			break;
		}
		return detalhes;
	}

	public List<CaixaRetiradaDTO> getRetiradaGeralDetalhes(Long idContaBancaria, Long idEmpresa, String dtInicio,
			String dtFim, Integer dadosSistema, String caixas) {
		List<CaixaRetiradaDTO> detalhesGeral = null;
		switch ( dadosSistema ) {
		case 0:
			detalhesGeral = caixaCompartilhadoMedicDAO.getRetiradaGeralDetalhes(idContaBancaria, idEmpresa, dtInicio, dtFim, caixas);
			break;
		case 1:
			 detalhesGeral = caixaCompartilhadoAcademiaDAO.getRetiradaGeralDetalhes(idContaBancaria, idEmpresa, dtInicio, dtFim, caixas);
			break;
		case 2:
			detalhesGeral = caixaCompartilhadoDentalDAO.getRetiradaGeralDetalhes(idContaBancaria, idEmpresa, dtInicio, dtFim, caixas);
			break;

		default:
			break;
	}
		return detalhesGeral;
	}
	
	public String getListaCaixas(Long idContaBancaria, Long idEmpresa, String dtInicio, String dtFim) {
		List<BigInteger> listCaixa = caixaDAO.getRetiradaCaixas(idContaBancaria, idEmpresa, dtInicio, dtFim);
		String caixas = converterListaEmString(listCaixa);
		return caixas;
	}

	public String converterListaEmString(List<BigInteger> lista) {
		StringBuilder str = new StringBuilder();
		for (BigInteger elem : lista) {
			str.append(elem.longValue()).append(",");
		}
		if(str.toString().isEmpty()){
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
		
	}
	

}
