package br.com.medic.medicsystem.main.rest;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.EmpresaService;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Plano;

@Path("/empresagrupos")
@RequestScoped
public class EmpresaGrupoController extends BaseController {

	@Inject
	private EmpresaService empresaService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<EmpresaGrupo> getEmpresaGrupo() {
		return empresaService.getEmpresaGrupos();

	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/vendedores")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Funcionario> getVendedores(@PathParam("id") Long idEmpresa) {

		List<Funcionario> list = empresaService.getVendedores(idEmpresa);
		if (list.isEmpty() || list == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return list;
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/planos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Plano> getPlanos(@PathParam("id") Long idEmpresa) {

		List<Plano> list = empresaService.getPlanos(idEmpresa);
		if (list.isEmpty() || list == null) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);

		} else {
			return list;
		}
	}
	
	//add 19-04-2018
	@GET
	@Path("/planos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Plano> getPlanosByEmpresa(@QueryParam("id") Long idEmpresa) {

		List<Plano> list = empresaService.getPlanosByEmpresa(idEmpresa);
		if (list.isEmpty() || list == null) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);

		} else {
			return list;
		}
	}

}