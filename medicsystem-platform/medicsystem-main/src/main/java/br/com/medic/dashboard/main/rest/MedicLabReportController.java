package br.com.medic.dashboard.main.rest;

import java.net.HttpURLConnection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.dashboard.persistence.model.AgendamentoUnidade;
import br.com.medic.dashboard.persistence.model.ClientePlano;
import br.com.medic.dashboard.persistence.model.FaturamentoMesAMes;
import br.com.medic.dashboard.persistence.model.FaturamentoMovimentacaoContratos;
import br.com.medic.dashboard.persistence.model.FaturamentoPlano;
import br.com.medic.dashboard.persistence.model.FaturamentoTipoPagamento;
import br.com.medic.dashboard.persistence.model.MovimentacaoCliente;
import br.com.medic.dashboard.main.model.Report;
import br.com.medic.dashboard.main.service.MedicLabReportService;

@ApplicationScoped
@Path("dashboard/reports")
public class MedicLabReportController {

	@Inject
	private MedicLabReportService reportService;

	@GET
	@Path("/report0001")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPorTipoPagamento(
	        @QueryParam("ano") String ano, @QueryParam("mes") String mes) {

		if (ano == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'ano' obrigatorio\" }")
			        .build());
		}

		if (mes == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'mes' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoPorTipoPagamento(ano, mes);

	}

	@GET
	@Path("/report0002")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratos(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoMovimentacaoContratos(inicio,fim);

	}

	@GET
	@Path("/report0003")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<MovimentacaoCliente>> getReportClientesNovosFaturamento(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportClientesNovosFaturamento(inicio, fim);

	}

	@GET
	@Path("/report0004")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoPlano>> getReportFaturamentoPlanos() {

		return reportService.getReportFaturamentoPlanos();

	}

	@GET
	@Path("/report0005")
	@Produces("text/csv")
	public List<ClientePlano> getReportListaClientesPorPlano(
	        @QueryParam("plano") Integer idPlano, 
	        @QueryParam("qtdAtendimento") Integer qtdAtendimento,
	        @QueryParam("qtdAtendimentoFim") Integer qtdAtendimentoFim, 
	        @QueryParam("custoVida") Float custoVida) {

		if (idPlano == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'plano' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportListaClientesPorPlano(idPlano, qtdAtendimento, qtdAtendimentoFim, custoVida, null, null);

	}

	@GET
	@Path("/report0006")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FaturamentoMesAMes> getReportFaturamentoMesAMes() {

		return reportService.getReportFaturamentoMesAMes();

	}
	
	@GET
	@Path("/report0007")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPagoPorTipoPagamento(
	        @QueryParam("ano") String ano, @QueryParam("mes") String mes) {

		if (ano == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'ano' obrigatorio\" }")
			        .build());
		}

		if (mes == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'mes' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoPagoPorTipoPagamento(ano, mes);

	}
	
	@GET
	@Path("/report0008/{idPlano:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClientePlano> getDbListaClientesPorPlano(
	        @PathParam("idPlano") Integer idPlano, @QueryParam("qtdAtendimento") Integer qtdAtendimento, 
	        @QueryParam("qtdAtendimentoFim") Integer qtdAtendimentoFim, @QueryParam("custoVida") Float custoVida, 
	        @QueryParam("ultimoPagamento") Float ultimoPagamento, @QueryParam("inFormaPagamento")Integer informaPagamento) {

		if (idPlano == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'plano' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportListaClientesPorPlano(idPlano, qtdAtendimento, qtdAtendimentoFim, custoVida, informaPagamento, ultimoPagamento );

	}
	
	@GET
	@Path("/report0009")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<MovimentacaoCliente>> getReportClientesFaturamento(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportClientesFaturamento(inicio, fim);

	}
	
	@GET
	@Path("/report0010")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratosDetalhado(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoMovimentacaoContratosDetalhado(inicio,fim);

	}
	
	@GET
	@Path("/report0011")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<FaturamentoPlano>> getReportFaturamentoPlanosEmpresarial() {

		return reportService.getReportFaturamentoPlanosEmpresarial();

	}
	
	@GET
	@Path("/report0012")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<MovimentacaoCliente>> getReportClienteAtivos() {

		return reportService.getReportClienteAtivos();

	}
	
	@GET
	@Path("/report0013")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<AgendamentoUnidade>> getReportAgendamentoUnidade(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportAgendamentoUnidade(inicio, fim);

	}	
	
	@GET
	@Path("/report0014")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<MovimentacaoCliente>> getReportFaturamentoDespesaAssociadoUnidade(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoDespesaAssociadoUnidade(inicio, fim);

	}	
	
	@GET
	@Path("/report0015")
	@Produces(MediaType.APPLICATION_JSON)
	public Report<List<MovimentacaoCliente>> getReportFaturamentoDespesaNAssociadoUnidade(
	        @QueryParam("inicio") String inicio, @QueryParam("fim") String fim) {

		if (inicio == null) {
			throw new WebApplicationException(
			        Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
			                .entity("{ \"error\": \"parametro 'inicio' obrigatorio\" }")
			                .build());
		}

		if (fim == null) {
			throw new WebApplicationException(Response
			        .status(HttpURLConnection.HTTP_BAD_REQUEST)
			        .entity("{ \"error\": \"parametro 'fim' obrigatorio\" }")
			        .build());
		}

		return reportService.getReportFaturamentoDespesaNAssociadoUnidade(inicio, fim);

	}
	

}