package br.com.medic.medicsystem.main.vindi.data.summary;

import java.util.Map;

public class Transaction {
	private Integer id;
	private String transaction_type;
	private String status;
	private Number amount;
	private Integer installments;
	private String gateway_message;
	private String gateway_response_code;
	private String gateway_authorization;
	private String gateway_transaction_id;
	private Map<String, String> gateway_response_fields;
	private Number fraud_detector_score;
	private String fraud_detector_status;
	private String fraud_detector_id;
	private String created_at;
	private Gateway gateway;
	private PaymentProfile payment_profile;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public Integer getInstallments() {
		return installments;
	}
	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
	public String getGateway_message() {
		return gateway_message;
	}
	public void setGateway_message(String gateway_message) {
		this.gateway_message = gateway_message;
	}
	public String getGateway_response_code() {
		return gateway_response_code;
	}
	public void setGateway_response_code(String gateway_response_code) {
		this.gateway_response_code = gateway_response_code;
	}
	public String getGateway_authorization() {
		return gateway_authorization;
	}
	public void setGateway_authorization(String gateway_authorization) {
		this.gateway_authorization = gateway_authorization;
	}
	public String getGateway_transaction_id() {
		return gateway_transaction_id;
	}
	public void setGateway_transaction_id(String gateway_transaction_id) {
		this.gateway_transaction_id = gateway_transaction_id;
	}
	public Map<String, String> getGateway_response_fields() {
		return gateway_response_fields;
	}
	public void setGateway_response_fields(Map<String, String> gateway_response_fields) {
		this.gateway_response_fields = gateway_response_fields;
	}
	public Number getFraud_detector_score() {
		return fraud_detector_score;
	}
	public void setFraud_detector_score(Number fraud_detector_score) {
		this.fraud_detector_score = fraud_detector_score;
	}
	public String getFraud_detector_status() {
		return fraud_detector_status;
	}
	public void setFraud_detector_status(String fraud_detector_status) {
		this.fraud_detector_status = fraud_detector_status;
	}
	public String getFraud_detector_id() {
		return fraud_detector_id;
	}
	public void setFraud_detector_id(String fraud_detector_id) {
		this.fraud_detector_id = fraud_detector_id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public Gateway getGateway() {
		return gateway;
	}
	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}
	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}
	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
