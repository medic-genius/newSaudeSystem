package br.com.medic.medicsystem.main.appmobile.dto;

import br.com.medic.medicsystem.persistence.model.Cliente;

public class UserRegisterDTO {
	private Cliente cliente;
	private String login;
	private String password;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
