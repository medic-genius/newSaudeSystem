package br.com.medic.medicsystem.main.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;

import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dao.TemplateProfissionalDAO;
import br.com.medic.medicsystem.persistence.model.EspProfissional;
import br.com.medic.medicsystem.persistence.model.TemplateProfissional;

@Stateless
public class TemplateProfissionalService {

	@Inject
	@Named("templateprofissional-dao")
	private TemplateProfissionalDAO templateProfissionalDAO;
	
	@Inject
	@Named("especialidade-dao")
	private EspecialidadeDAO especialidadeDAO;
	
	public TemplateProfissional updateTemplateProfissional(Long idTemplate, TemplateProfissional template) {
		TemplateProfissional templateOriginal = templateProfissionalDAO.searchByKey(TemplateProfissional.class, idTemplate);
		if(templateOriginal == null) {
			throw new WebApplicationException("Template a ser atualizado não existe", 500);
		}
		templateOriginal.setNmTemplate(template.getNmTemplate());
		templateOriginal.setNmConteudo(template.getNmConteudo());
		templateOriginal.setDtAtualizacaoLog(new Date());
		
		try {
			return templateProfissionalDAO.update(templateOriginal);
		} catch (Exception e) {
		}
		return null;		
	}

	public TemplateProfissional getTemplateProfissional(Long idTemplateProfissional){
		TemplateProfissional listTemplate = templateProfissionalDAO
				.getTemplateProfissional(idTemplateProfissional);	
		if(listTemplate != null){
			return listTemplate;
		}
		return null;
	}


	public List<TemplateProfissional> getListTemplateProfissionalPorFuncionario(Long idFuncionario){
		List<TemplateProfissional> listTemplate = templateProfissionalDAO
				.getTemplateProfissionalPorFuncionario(idFuncionario);	
		if(listTemplate != null){
			return listTemplate;
		}
		return null;
	}

	public List<TemplateProfissional> getListTemplateProfissionalPorEspecialidade(Long idEspecialidade){
		List<TemplateProfissional> listTemplate = templateProfissionalDAO
				.getTemplateProfissionalPorEspecialidade(idEspecialidade);
		if(listTemplate != null){
			return listTemplate;
		}
		return null;		
	}

	public List<TemplateProfissional> getListTemplateProfissionalPorEspProf(Long idEspecialidade, 
			Long idFuncionario){
		List<TemplateProfissional> listTemplate = templateProfissionalDAO.getTemplateProfissionalPorEspProf(
				idEspecialidade, idFuncionario);

		if(listTemplate != null) {			
			return listTemplate;
		}
		return null;
	}

	public TemplateProfissional saveTemplateProfissional(Long idEspecialidade, Long idFuncionario, 
			TemplateProfissional template) {
		EspProfissional espProf = especialidadeDAO.getEspProfissional(idFuncionario, idEspecialidade);
		if(espProf == null) {
			throw new WebApplicationException("Profissional não cadastrado para essa especialidade", 500);
		}
		template.setEspProfissional(espProf);
		template.setDtInclusaoLog(new Date());
		return templateProfissionalDAO.persist(template);
	}
	
	public TemplateProfissional deleteTemplateProfissional(Long idTemplate) {
		TemplateProfissional tpl = templateProfissionalDAO.getTemplateProfissional(idTemplate);
		if(tpl == null) {
			throw new WebApplicationException("Template a ser excluído não existe", 500);
		}
		tpl.setDtExclusao(new Date());
		return templateProfissionalDAO.update(tpl);
	}
}
