package br.com.medic.medicsystem.main.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.json.JSONObject;

import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Cidade;

@Stateless
public class BairroService {

	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;

	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;

	public Bairro saveBairro(Bairro bairro) {

		Cidade cidade = cidadeDAO.getCidade(bairro.getCidade().getId());
		bairro.setCidade(cidade);

		bairroDAO.persist(bairro);

		return bairro;

	}

	public Bairro updateBairro(Bairro bairro) {

		Cidade cidade = cidadeDAO.getCidade(bairro.getCidade().getId());
		bairro.setCidade(cidade);

		bairro = bairroDAO.update(bairro);

		return bairro;
	}

	public Bairro getBairroByNm(String nmBairro, Long idCidade) {
		return bairroDAO.getBairro(nmBairro, idCidade);
	}

	public JSONObject getAddressByCep(String cep) {
		HttpURLConnection c = null;
		String url = "https://viacep.com.br/ws/" + cep + "/json/";
		int timeout = 15000;
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				JSONObject json = new JSONObject(sb.toString());
				return json;
			}
		} catch (MalformedURLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.DEBUG, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(getClass().getName()).log(Level.DEBUG, null, ex);
		} finally {
			if (c != null) {
				try {
					c.disconnect();
				} catch (Exception ex) {
					Logger.getLogger(getClass().getName()).log(Level.DEBUG, null, ex);
				}
			}
		}
		return null;
	}
}
