package br.com.medic.medicsystem.main.vindi.response;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.subscription.Subscription;
import br.com.medic.medicsystem.main.vindi.data.subscription.SubscriptionResponse;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;

public class SubscriptionParser extends ResponseParser<Subscription> {
	public SubscriptionParser() {
		super(Subscription.class);
	}
	
	public SubscriptionResponse parseCreationResponse(String response) 
			throws VindiException, Exception {
		JSONObject responseJson = new JSONObject(response);
		ObjectMapper mapper = new ObjectMapper();
		if(responseJson.has("errors")) {
			throw (new VindiException(response));
		} else {
			return mapper.readValue(response, SubscriptionResponse.class);
		}
	}
}
