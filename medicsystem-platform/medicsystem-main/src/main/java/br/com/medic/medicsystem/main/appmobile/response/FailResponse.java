package br.com.medic.medicsystem.main.appmobile.response;

public class FailResponse extends AppResponse {
	private Object data;
	
	public FailResponse() {
		super("fail");
	}
	
	public FailResponse(Integer code, Object data) {
		this();
		this.setCode(code);
		this.setData(data);
	}
	
	public FailResponse(Object data) {
		this(null, data);
	}
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
