package br.com.medic.medicsystem.main.vindi.exception;

public class VindiException extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VindiException() {
		super();
	}
	
	public VindiException(String errors) {
		super(errors);
	}
}
