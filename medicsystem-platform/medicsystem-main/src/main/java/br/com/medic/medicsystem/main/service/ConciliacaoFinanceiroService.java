package br.com.medic.medicsystem.main.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.main.exception.DespesaFinanceiroException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ConciliacaoFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dao.MovimentacaoContaDAO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ArqConciliacaoBancaria;
import br.com.medic.medicsystem.persistence.model.DespesaFinanceiro;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.model.MovimentacaoItemConta;
import br.com.medic.medicsystem.persistence.model.enums.AcaoMovimentacao;
import br.com.medic.medicsystem.persistence.model.enums.StatusPagamentoDespesa;
import br.com.medic.medicsystem.persistence.model.enums.TipoMovimentacao;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class ConciliacaoFinanceiroService {
	
	@Inject
	@Named("conciliacaofinanceiro-dao")
	private ConciliacaoFinanceiroDAO conciliacaoFinanceiroDAO;
	
	@Inject
	@Named("despesafinanceiro-dao")
	private DespesaFinanceiroDAO despesaFinanceiroDAO;
	
	@Inject
	@Named("movimentacaoconta-dao")
	private MovimentacaoContaDAO movimentacaoContaDAO;
	
	@Inject
	private SecurityService securityService;
	
	public List<String> importaArquivosOFX(String input, Integer idEmpresa, Integer idContaSelected) throws ParseException{		
		
		List<String> listFront = new ArrayList<String>();
		String allConteudo = "";
		
		byte[] decodeBytes = Base64.decodeBase64(input);
		InputStream is = null;
        BufferedReader bfReader = null;
        
        try {
        	
            is = new ByteArrayInputStream(decodeBytes);
            bfReader = new BufferedReader(new InputStreamReader(is));
            String vartemp = null;
                   		
            StringBuffer str = new StringBuffer();
            while((vartemp = bfReader.readLine()) != null){
            	str.append(vartemp + " ");
            } 
            
            allConteudo = str.toString();
          
            listFront =  conciliacaoFinanceiroDAO.saveArquivo(idEmpresa, idContaSelected, allConteudo);
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
	            try{
	                if(is != null) is.close();
	            } catch (Exception ex){
                 
            }
        }
		
		return listFront;
	}
	
	public List<ArqConciliacaoBancaria> getInfoExtratos(Long idEmpresa, Long idContaBancaria){
		return conciliacaoFinanceiroDAO.getInfoExtratos(idEmpresa, idContaBancaria);
	}

	  
	public List<String> gerarConciliacao(Integer idTransBancaria, List<String> idsDespesa, List<DespesaFinanceiro> despesaFinanceiro){
		String listIdsDespesas = "";
		
		for(int i = 0; i < despesaFinanceiro.size(); i++){
			//pega a despesa como esta no banco
			
			if(!isDespesaQuitada(despesaFinanceiro.get(i).getId())){
				DespesaFinanceiro despesaAntiga = getDespesaFinanceiroById(despesaFinanceiro.get(i).getId());
				
				despesaAntiga.setVlPago(despesaFinanceiro.get(i).getVlPago());
				despesaAntiga.setDtPagamento(despesaFinanceiro.get(i).getDtPagamento());
				despesaAntiga.setEmpresaFinanceiroPagante(despesaFinanceiro.get(i).getEmpresaFinanceiroPagante());
				despesaAntiga.setContaBancaria(despesaFinanceiro.get(i).getContaBancaria());
				despesaAntiga.setInFormaPagamento(despesaFinanceiro.get(i).getInFormaPagamento());
				despesaAntiga.setVlMulta(despesaFinanceiro.get(i).getVlMulta());
				despesaAntiga.setVlDesconto(despesaFinanceiro.get(i).getVlDesconto());
				despesaAntiga.setInStatus(despesaFinanceiro.get(i).getInStatus());
				
				despesaFinanceiroDAO.update(despesaAntiga);
			}
			
			if(i == despesaFinanceiro.size()-1){
				salvarDespesaEMovimentacao(despesaFinanceiro,AcaoMovimentacao.PAGAMENTO, TipoMovimentacao.SAIDA);
			}
			
			//ao pagar a despesa é obrigatório enviar sua data de pagamento
			if(despesaFinanceiro.get(i).getInStatus() == StatusPagamentoDespesa.PAGO.ordinal() && despesaFinanceiro.get(i).getDtPagamento() == null){
				throw new IllegalArgumentException("Data não especificada");
			}
			
		}
		
		listIdsDespesas = idsDespesa.toString().replaceAll(", ","][");
		
		List<String> resultadoConciliacao = conciliacaoFinanceiroDAO.gerarConciliacao(idTransBancaria, listIdsDespesas);
		return resultadoConciliacao;
	}
	
	private boolean isDespesaQuitada(Long id){
		try{
			if(id == null)
				return false;

			DespesaFinanceiro despesa = getDespesaFinanceiroById(id);
			if(despesa != null && despesa.getInStatus() == StatusPagamentoDespesa.PAGO.ordinal()){
				return true;
			}
		}catch(ObjectNotFoundException | NullPointerException ex){
			ex.printStackTrace();
		}
		return false;
	}
	
	public DespesaFinanceiro getDespesaFinanceiroById(Long id){
		DespesaFinanceiro despesaFinanceiro = despesaFinanceiroDAO.searchByKey(DespesaFinanceiro.class, id); //uma parcela
		
		if(despesaFinanceiro.getDtExclusao() != null){
			throw new IllegalArgumentException("Despesa Inativa");
		}
		return despesaFinanceiro;
	}
	
	private List<DespesaFinanceiro> salvarDespesaEMovimentacao(List<DespesaFinanceiro> despesaFinanceiro, AcaoMovimentacao acao, TipoMovimentacao tipoMovimentacao){
		
		for(int i=0; i < despesaFinanceiro.size(); i++){
			if(despesaFinanceiro.get(i).getId() != null){
				despesaFinanceiroDAO.update(despesaFinanceiro.get(i));
			}else{
				despesaFinanceiroDAO.persist(despesaFinanceiro.get(i));
			}
			
			//ao pagar a despesa salva a movimentação
			if(despesaFinanceiro.get(i).getInStatus() == StatusPagamentoDespesa.PAGO.ordinal()){
				List<MovimentacaoConta> movConta = movimentacaoContaDAO.getMovimentacaoContaByIdDespesa( despesaFinanceiro.get(i).getId(), AcaoMovimentacao.PAGAMENTO.getId() );
				if(movConta == null){
					saveMovimentacao(despesaFinanceiro.get(i), acao, tipoMovimentacao);
				}
				
			}
		}
		
		
		
		return despesaFinanceiro;
	}
	
	private void saveMovimentacao(DespesaFinanceiro despesaFinanceiro, AcaoMovimentacao acao, TipoMovimentacao tipoMovimentacao){
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		
		MovimentacaoConta movimentacao = new MovimentacaoConta();
		MovimentacaoItemConta itemConta = criaItemConta(despesaFinanceiro);
		itemConta.setDespesaFinanceiro(despesaFinanceiro);

		movimentacao.setItemsConta(new ArrayList<MovimentacaoItemConta>());
		
		if( acao.getId() == AcaoMovimentacao.ESTORNO.getId() ) {
			List<MovimentacaoConta> movContaPagamentoList = movimentacaoContaDAO.getMovimentacaoContaByIdDespesa( despesaFinanceiro.getId(), AcaoMovimentacao.PAGAMENTO.getId() );
			DateFormat dataformat = new SimpleDateFormat( "dd/MM/yyyy" );
			if( movContaPagamentoList != null) {
				MovimentacaoConta movContaPagamento = movContaPagamentoList.get(0);
				movContaPagamento.setHaveEstorno(true);
				movContaPagamento.setInfoEstorno( "Dt Estorno: " + dataformat.format(new Date())  + "\n" + "Op. Estorno: " + securityService.getFuncionarioLogado().getNmFuncionario()      );
				
				movimentacaoContaDAO.update(movContaPagamento);
				
			}
				
			movimentacao.setDtMovimentacao(despesaFinanceiro.getDtPagamento() );
		}			
		else 
			movimentacao.setDtMovimentacao(despesaFinanceiro.getDtPagamento());
		
		
		movimentacao.setContaBancaria(despesaFinanceiro.getContaBancaria());
		movimentacao.setAcaoMovimentacao(acao);
		movimentacao.setNrValor(despesaFinanceiro.getVlPago());
		movimentacao.setFuncionarioMovimentacao(securityService.getFuncionarioLogado());
		movimentacao.setInTipo(tipoMovimentacao);
		movimentacao.addMovimentacaoItemConta(itemConta);
		movimentacaoContaDAO.persist(movimentacao);
		
		
		Integer inSucesso = movimentacaoContaDAO.ajustarMovimentacaoContaPosPagamentoDespesa( despesaFinanceiro.getContaBancaria().getId(), movimentacao.getDtMovimentacao()  );
		
		if( inSucesso == 0)
			throw new DespesaFinanceiroException(props.getProperty("movimentacao.ajustarmovimentacao"), ErrorType.DANGER);
		
		
	}
	
	private MovimentacaoItemConta criaItemConta(DespesaFinanceiro despesaFinanceiro){
		MovimentacaoItemConta itemConta = new MovimentacaoItemConta();
		itemConta.setDespesaFinanceiro(despesaFinanceiro);
		return itemConta;
	}

	
	public List<String> desconciliarDados(Integer idTransBancaria){
		
		List<String> resultadoDesconciliacao = conciliacaoFinanceiroDAO.desconciliarDados(idTransBancaria);
		
		return resultadoDesconciliacao;
	}
	
	/*public List<SujestaoConciliacaoBancaria> getSujestao(Integer idEmpresa, Integer idBanco, int offset, int limit){
		if(limit > 25){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		Funcionario funcionario = securityService.getFuncionarioLogado();
		List<String> prepSujestao = conciliacaoFinanceiroDAO.prepararSujestao(idEmpresa, funcionario.getId(), idBanco);
		List<SujestaoConciliacaoBancaria> listWithSujestion = new ArrayList<SujestaoConciliacaoBancaria>();
		Long total = conciliacaoFinanceiroDAO.getTotal(funcionario.getId());
		System.out.println(total);
		if(prepSujestao.size() == 1){
			 listWithSujestion = conciliacaoFinanceiroDAO.getSujestao(funcionario.getId(), offset, limit);
		}else if(prepSujestao.isEmpty()){
			return null;
		}

		return listWithSujestion;
	}*/
	
	public GenericPaginateDTO getSugestao(Integer idEmpresa, Integer idBanco, int offset, int limit){
		if(limit > 25){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		Funcionario funcionario = securityService.getFuncionarioLogado();
		List<String> prepSugestao = conciliacaoFinanceiroDAO.prepararSugestao(idEmpresa, funcionario.getId(), idBanco);
		List<?> listWithSugestion = null;
		Long total = conciliacaoFinanceiroDAO.getTotal(funcionario.getId());
		if(prepSugestao.size() == 1){
			listWithSugestion = conciliacaoFinanceiroDAO.getSugestao(funcionario.getId(), offset, limit);
		}else if(prepSugestao.isEmpty()){
			return null;
		}
		
		GenericPaginateDTO sujDTO = new GenericPaginateDTO();
		sujDTO.setOffset(offset);
		sujDTO.setLimit(limit);
		sujDTO.setTotal(total);
		sujDTO.setData(listWithSugestion);
		sujDTO.prepare();
		return sujDTO;
	}
}
