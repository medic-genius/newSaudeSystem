package br.com.medic.medicsystem.main.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ConferenciaDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaClienteDAO;
import br.com.medic.medicsystem.persistence.dao.RegistroAgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dto.ConferenciaEmpresaClienteDTO;
import br.com.medic.medicsystem.persistence.dto.DetalhesFaturaDTO;
import br.com.medic.medicsystem.persistence.dto.FaturamentoConveniadaDTO;
import br.com.medic.medicsystem.persistence.dto.FaturamentoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.RegistroAgendamentoDTO;
import br.com.medic.medicsystem.persistence.dto.RegistroAtaDTO;
import br.com.medic.medicsystem.persistence.dto.UnidadeDTO;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.SalarioMd;
import br.com.medic.medicsystem.persistence.model.enums.TipoAtendimento;
import br.com.medic.medicsystem.persistence.model.views.ClienteDependenteConvenioView;
import br.com.medic.medicsystem.persistence.model.views.DespesaConvenioView;
import br.com.medic.medicsystem.persistence.model.views.MensalidadeParcelaView;

@Stateless
public class FaturamentoService {
	
	@Inject
	@Named("registroagendamento-dao")
	private RegistroAgendamentoDAO registroAgendamentoDAO;
	
	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;
	
	@Inject
	@Named("empresaCliente-dao")
	private EmpresaClienteDAO empresaClienteDAO;
	
	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
	@Inject
	@Named("conferencia-dao")
	private ConferenciaDAO conferenciaDAO;
	
	@Inject
	private FuncionarioService funcionarioService;
	
//FATURAMENTO CONVENIADA
	public List<FaturamentoConveniadaDTO> getFaturamentoConveniada(String dtInicio, String dtFim){
		
		List<FaturamentoConveniadaDTO> listFaturamentoConveniadaDTO = new ArrayList<FaturamentoConveniadaDTO>();			
		List<EmpresaCliente> listEmpresaCliente = new ArrayList<EmpresaCliente>();
		DecimalFormat formato = new DecimalFormat("#.##");	
		
		listEmpresaCliente = empresaClienteDAO.getListaEmpresasAtivas();
		
		for(EmpresaCliente ec : listEmpresaCliente){
			
			FaturamentoConveniadaDTO faturamentoConveniadaDTO = new FaturamentoConveniadaDTO();
			Double vlTotalServico = 0.0;
			Double vlTotalVida = 0.0;
			
			List<DespesaConvenioView> listDespesaServicoConveniada = servicoDAO.getDespesaServicoConvenio(ec.getId(), dtInicio, dtFim);
			List<ClienteDependenteConvenioView> listClienteDependenteConveniada = empresaClienteDAO.getClienteDependenteConveniada(ec.getId());			
			Contrato contrato = contratoDAO.getContratoEmpresaClienteById(ec.getId());			
			
			if(listDespesaServicoConveniada != null && listDespesaServicoConveniada.size() > 0){
				for(DespesaConvenioView dcview : listDespesaServicoConveniada){			
					vlTotalServico += dcview.getVlServico();
				}
				
				faturamentoConveniadaDTO.setListDespesaServico(listDespesaServicoConveniada);
				faturamentoConveniadaDTO.setTotalServico(listDespesaServicoConveniada.size());
			}
			else
				faturamentoConveniadaDTO.setTotalServico(0);
				
			
			if(listClienteDependenteConveniada != null && listClienteDependenteConveniada.size() > 0){
				for(ClienteDependenteConvenioView cdcview : listClienteDependenteConveniada){
					vlTotalVida += cdcview.getVlContrato();			
				}
				
				faturamentoConveniadaDTO.setListClienteDependente(listClienteDependenteConveniada);
				faturamentoConveniadaDTO.setTotalVidas(listClienteDependenteConveniada.size());
			}			
			
			faturamentoConveniadaDTO.setVlTotalServico(Double.valueOf(formato.format(vlTotalServico).replace(",", ".")));
			faturamentoConveniadaDTO.setIdEmpresaCliente(ec.getId());
			faturamentoConveniadaDTO.setNmEmpresaCliente(ec.getNmFantasia());
			faturamentoConveniadaDTO.setBoBloqueado(contrato.getBoBloqueado());
			faturamentoConveniadaDTO.setInTipo(ec.getInTipoPagamento());
			
			if(ec.getInTipoPagamento().equals(1))//Despesa				
				faturamentoConveniadaDTO.setVlTotal(faturamentoConveniadaDTO.getVlTotalServico());
			
			else if(ec.getInTipoPagamento().equals(2)){//Mensalidade
				
				if(ec.getInTipoCobranca() != null && ec.getInTipoCobranca().equals(0)){ //Fixo
					
					faturamentoConveniadaDTO.setVlTotal(contrato.getVlTotal());
				}
				else
					faturamentoConveniadaDTO.setVlTotal(vlTotalVida);
									
			}								
			
			listFaturamentoConveniadaDTO.add(faturamentoConveniadaDTO);
		}				
		
		return listFaturamentoConveniadaDTO;		
	}
		
	public List<MensalidadeParcelaView> getMensalidadeParcelaConveniada(long idEmpresaCliente){
		
		Contrato contrato = contratoDAO.getContratoEmpresaClienteById(idEmpresaCliente);
		List<MensalidadeParcelaView> listMensalidadeParcelaConveniada = contratoDAO.getMensalidadeParcelaConveniada(contrato.getId());	
		
		return listMensalidadeParcelaConveniada;
	}
	
	public List<ConferenciaEmpresaClienteDTO> getPeriodoFaturamento(long idEmpresaCliente){
		
		return conferenciaDAO.getPeriodoFaturamento(idEmpresaCliente);
	}
	
	/**
	 * 
	 * @param idConveniada
	 * @param period; String that contains period in format YYYYMM
	 * @return
	 */
	public DetalhesFaturaDTO getFatura(Long idConveniada, String period) {
		DetalhesFaturaDTO detalhesFaturaDTO = new DetalhesFaturaDTO();
		EmpresaCliente ec = empresaClienteDAO.searchByKey(EmpresaCliente.class, idConveniada);
		Double vlTotalServico = 0.0;
		String dtInicio =  this.getStartOfPeriod(period);
		String dtFim =  this.getEndOfPeriod(period);
		List<DespesaConvenioView> listDespesaServicoConveniada = servicoDAO.getDespesaServicoConvenio(ec.getId(), dtInicio , dtFim);
		detalhesFaturaDTO.setlDespesaServicoConvenio(listDespesaServicoConveniada);
		if(listDespesaServicoConveniada != null && listDespesaServicoConveniada.size() > 0){
			for(DespesaConvenioView dcview : listDespesaServicoConveniada){
				vlTotalServico += dcview.getVlServico();
			}
		}
		detalhesFaturaDTO.setConferencia(conferenciaDAO.getConferenciaEmpresaClienteByMesAno(idConveniada, getPeriodWithCrazyFormated(period)));
		return detalhesFaturaDTO;
	}

	private String getStartOfPeriod(String period) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyyMM");
		YearMonth yearMonth = YearMonth.parse(period, inputFormat);
		final Date convertedFromYearMonth = Date.from(yearMonth.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String reportDate = df.format(convertedFromYearMonth);
		return reportDate;
	}

	private String getEndOfPeriod(String period) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyyMM");
		YearMonth yearMonth = YearMonth.parse(period, inputFormat);
		final Date convertedFromYearMonth = Date.from(yearMonth.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(convertedFromYearMonth);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        df.format(cal.getTime());
        String reportDate = df.format(cal.getTime());        
        return reportDate;
	}
	
	private String getPeriodWithCrazyFormated(String period) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyyMM");
		YearMonth yearMonth = YearMonth.parse(period, inputFormat);
		return (Integer.toString(yearMonth.getMonthValue()) + "/" + Integer.toString(yearMonth.getYear()));
	}
	
	
	
//FATURAMENTO PROFISSIONAL
	public List<UnidadeDTO> getUnidadeTrabalhadaByTipoAtendimento(Long idFuncionario, String dtInicio, String dtFim){
		return registroAgendamentoDAO.getUnidadeTrabalhadaByTipoAtendimento(idFuncionario, dtInicio, dtFim, null);
	}

	public FaturamentoProfissionalDTO getFaturamentoProfissional(Long idFuncionario, String dtInicio, String dtFim){
		
		FaturamentoProfissionalDTO faturamentoProfissionalDTO = new FaturamentoProfissionalDTO();
		Double totalVlSalario = 0.0;
		Integer totalServico = 0;
		
		faturamentoProfissionalDTO.setRegistrosFixo(getFaturamentoProfissionalFixo(
				idFuncionario, dtInicio, dtFim, TipoAtendimento.FIXO.getId()));
		
		faturamentoProfissionalDTO.setRegistrosHorista(getFaturamentoProfissionalHorista(
				idFuncionario, dtInicio, dtFim, TipoAtendimento.HORISTA.getId()));
		
		faturamentoProfissionalDTO.setRegistrosProducao(getFaturamentoProfissionalProducao(
				idFuncionario, dtInicio, dtFim, TipoAtendimento.PRODUCAO.getId()));
		
		faturamentoProfissionalDTO.setRespTecnico(getFaturamentoProfissionalRespTecnico(
						idFuncionario, dtInicio));

		totalVlSalario = faturamentoProfissionalDTO.getRegistrosFixo().getVlSalario()	
				+ faturamentoProfissionalDTO.getRegistrosHorista().getVlSalario()
				+ faturamentoProfissionalDTO.getRegistrosProducao().getVlSalario()
				+ faturamentoProfissionalDTO.getRespTecnico().getVlSalario();
		
		totalServico = faturamentoProfissionalDTO.getRegistrosFixo().getTotalServico()
				+ faturamentoProfissionalDTO.getRegistrosHorista().getTotalServico()
				+ faturamentoProfissionalDTO.getRegistrosProducao().getTotalServico();

		faturamentoProfissionalDTO.setVlSalario(totalVlSalario);
		faturamentoProfissionalDTO.setTotalServico(totalServico);
									
		return faturamentoProfissionalDTO;
	}
	
	
	/**
	 * Obtem registro(s) do tipo horista, para faturamento profissional. 
	 * @param idFuncionario
	 * @param dtInicio
	 * @param dtFim
	 * @param tipoAtendimento
	 * @return
	 */
	private RegistroAgendamentoDTO getFaturamentoProfissionalHorista( Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){
						
		List<RegistroAtaDTO> listAtendimentoCliente = registroAgendamentoDAO.getRegistrosAgendamentoRealizados(idFuncionario, dtInicio, dtFim, tipoAtendimento);	
			
		List<RegistroAtaDTO> listHorarioAtendimento = registroAgendamentoDAO.getRegistrosAgendamentoHora(idFuncionario, dtInicio, dtFim, tipoAtendimento);
				
		RegistroAgendamentoDTO registroAgendDTO = getTotalHoraTrabalhada(listHorarioAtendimento, idFuncionario);
		
		registroAgendDTO.setListAtendimentoCliente(listAtendimentoCliente);
		registroAgendDTO.setListHorarioAtendimento(listHorarioAtendimento);		
		registroAgendDTO.setTotalServico(listAtendimentoCliente.size());
		
		return registroAgendDTO;
		
	}
	
	private RegistroAgendamentoDTO getTotalHoraTrabalhada(List<RegistroAtaDTO> listHorarioAtendimento, Long idFuncionario){
		
		Properties props = PropertiesLoader.getInstance().load("tablesexagesimal.properties");		
		RegistroAgendamentoDTO registroAgendDTO = new RegistroAgendamentoDTO();		
		SimpleDateFormat fta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss");
		Calendar c = new GregorianCalendar();
		Double totalVlSalario = 0.0;
		Double vlHora = 0.0;
		
		try {
			c.setTime(fta.parse("2000-01-01 00:00:00"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		Funcionario funcionario = funcionarioService.getFuncionarioById(idFuncionario);		 		
		String[] splitHrToleranciaFunc = 
				ft.format(funcionario.getHrToleranciaPonto() != null ? funcionario.getHrToleranciaPonto() : c.getTime()).split(":");		
		
		for(RegistroAtaDTO reg : listHorarioAtendimento){
			
			String[] splitHrAtendimento = null;
			String[] splitHrPonto = null;
			String hrPontoFinal = "";
			long totalHrParcial = 0l;
						
			splitHrAtendimento = reg.getHrAtendimento().split("-");
			splitHrPonto = reg.getHrPonto().split("-");			
			
			for(int i = 1; i < splitHrAtendimento.length; i+=2){
								
				Date hrAtendCompIni = null;
				Date hrAtendCompFim = null;				
				String hrParcial = "";				
								
				try {
					hrAtendCompIni = ft.parse(splitHrAtendimento[i-1]);
					hrAtendCompFim = ft.parse(splitHrAtendimento[i]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
									
				// 1 - Cobinacao de horas de atendimento com horas de ponto
				for(int j = 1; j < splitHrPonto.length; j+=2){
					
					if(hrPontoFinal.length() > 0){
						String[] splithrParcial = hrPontoFinal.substring(0, hrPontoFinal.length() - 1).split("-");
						Date hrLastPoint = null;
						Date hrFirstRegPoint = null;
						Date hrSecondRegPoint = null;
						try {
							hrLastPoint = ft.parse(splithrParcial[splithrParcial.length - 1]);
							hrFirstRegPoint = ft.parse(splitHrPonto[j-1]);
							hrSecondRegPoint = ft.parse(splitHrPonto[j]);
							
							if(hrFirstRegPoint.before(hrLastPoint) && hrLastPoint.before(hrSecondRegPoint))
								splitHrPonto[j-1] = splithrParcial[splithrParcial.length - 1];							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
					Date hrPontoCompIni = null;
					Date hrPontoCompFim = null;
					String hrParcialAtend = "";
					String hrParcialPonto = "";
					String[] splitHrAtendimentoParcial = null;
					String[] splitHrPontoParcial = null;
					
					try {
						hrPontoCompIni = ft.parse(splitHrPonto[j-1]);	
						hrPontoCompFim = ft.parse(splitHrPonto[j]);	
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(hrAtendCompIni.after(hrPontoCompIni) && hrAtendCompIni.before(hrPontoCompFim)){
						hrParcialAtend = ft.format(hrAtendCompIni) + "-" + ft.format(hrAtendCompFim);
						hrParcialPonto = ft.format(hrPontoCompIni) + "-" + ft.format(hrPontoCompFim);
					}
					else if(hrAtendCompFim.after(hrPontoCompIni) && hrAtendCompFim.before(hrPontoCompFim)){
						hrParcialAtend = ft.format(hrAtendCompIni) + "-" + ft.format(hrAtendCompFim);
						hrParcialPonto = ft.format(hrPontoCompIni) + "-" + ft.format(hrPontoCompFim);
					}
					else if(hrPontoCompIni.after(hrAtendCompIni) && hrPontoCompIni.before(hrAtendCompFim)){
						hrParcialAtend = ft.format(hrAtendCompIni) + "-" + ft.format(hrAtendCompFim);
						hrParcialPonto = ft.format(hrPontoCompIni) + "-" + ft.format(hrPontoCompFim);
					}
					
					if(hrParcialAtend.length() > 0  && hrParcialPonto.length() > 0){
					
						// 2 - Obtem a hrinicio e hrfim parciais conforme regra de combinacao de horastendimento e horasponto									
						splitHrAtendimentoParcial = hrParcialAtend.split("-");
						splitHrPontoParcial = hrParcialPonto.split("-");
						long diffdata = 0;
						Calendar cHrAtendIni = new GregorianCalendar();
						Calendar cHrAtendFim = new GregorianCalendar();				
						
						try {
							cHrAtendIni.setTime(ft.parse(splitHrAtendimentoParcial[0]));
							cHrAtendFim.setTime(ft.parse(splitHrAtendimentoParcial[1]));
							
							diffdata = cHrAtendFim.getTimeInMillis() - cHrAtendIni.getTimeInMillis();
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}						
												
						for(int x=0; x<splitHrAtendimentoParcial.length; x++){
							
							Date hrAtend = null;				
							Date hrPonto = null;
							Calendar cldAnt = new GregorianCalendar();
							Calendar cldDep = new GregorianCalendar();
														
							try {
								hrAtend = ft.parse(splitHrAtendimentoParcial[x]);
								hrPonto = ft.parse(splitHrPontoParcial[x]);
								
								cldAnt.setTime(hrAtend);
								cldDep.setTime(hrAtend);
								cldAnt.add(Calendar.MINUTE, -Integer.valueOf(splitHrToleranciaFunc[1]));
								cldDep.add(Calendar.MINUTE, Integer.valueOf(splitHrToleranciaFunc[1]));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if( (x+1) % 2 == 0){
								
								Calendar cp = new GregorianCalendar();
								try {
									cp.setTime(ft.parse(hrParcial.replace("-", "")));
									cp.setTimeInMillis(cp.getTimeInMillis() + diffdata);									
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								if(hrPonto.after(cldDep.getTime())){
									if(cldDep.getTime().after(cp.getTime()))
										hrParcial += ft.format(cp.getTime()) + "-";
									else
										hrParcial += ft.format(cldDep.getTime()) + "-";
								}
								else{
									if(hrPonto.after(cp.getTime()))
										hrParcial += ft.format(cp.getTime()) + "-";
									else
										hrParcial += ft.format(hrPonto) + "-";
								}						
							}
							else{					
								if(hrPonto.after(cldAnt.getTime()))					
									hrParcial += ft.format(hrPonto) + "-";					
								else
									hrParcial += ft.format(cldAnt.getTime()) + "-";								
																			
							}
						}
						
						hrParcial = hrParcial.length() > 0 ? hrParcial : "";
						hrPontoFinal += hrParcial;
									            						
						String[] splitHrParcial = null;
						splitHrParcial = hrParcial.split("-");					
						
						// 3 - Obtem parcialmente a quantidade de tempo trabalhada 
						long hrInicioParcial = 0;
						long hrFimParcial = 0;	
						int k = 1;						
									        			
						while (k < splitHrParcial.length){
							Date hrBegin = null;				
							Date hrEnd = null;
											
							if(k>1){
								
								try {
									hrEnd = ft.parse(splitHrParcial[k]);
									hrBegin = ft.parse(splitHrParcial[k - 1]);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								hrFimParcial = hrEnd.getTime() - hrBegin.getTime();
								k = k + 2;
							}
							else{
								
								try {
									hrEnd = ft.parse(splitHrParcial[k]);
									hrBegin = ft.parse(splitHrParcial[k - 1]);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								hrInicioParcial = hrEnd.getTime() - hrBegin.getTime();					
								k= k + 2;
								
							}
						}
						
						totalHrParcial += (hrFimParcial + hrInicioParcial);
					}
				}				
				
							
			}
			ft.setTimeZone(TimeZone.getTimeZone("UTC"));	
			reg.setHrPontoFinal(hrPontoFinal.length() > 0 ? hrPontoFinal.substring(0, hrPontoFinal.length() - 1) : "");
			reg.setHrTotal(ft.format(totalHrParcial));			
//			reg.setVlTotal( (totalHrParcial / 1000) * (((reg.getVlHora() != null ? reg.getVlHora() : 0)  / 60) / 60) );
			
			vlHora = reg.getVlHora() != null ? reg.getVlHora() : 0;  
			String[] splithrtotal = null;
			if(reg.getHrTotal() != null){
				splithrtotal = reg.getHrTotal().split(":");
				reg.setVlTotal( ( Integer.valueOf(splithrtotal[0]) * vlHora ) + ( Float.valueOf(props.getProperty(splithrtotal[1])) * vlHora ) );
			}
						          
            totalVlSalario += reg.getVlTotal();   
        		
			String splitHora[] = ft.format(totalHrParcial).split(":");
			c.add(Calendar.HOUR_OF_DAY, Integer.valueOf(splitHora[0]));
			c.add(Calendar.MINUTE, Integer.valueOf(splitHora[1]));
//			c.add(Calendar.SECOND, Integer.valueOf(splitHora[2]));
					
		}
		
		Integer dia = c.get(Calendar.DAY_OF_MONTH) - 1;
		Integer hora = c.get(Calendar.HOUR_OF_DAY);
		Integer minuto = c.get(Calendar.MINUTE);
//		Integer segundo = c.get(Calendar.SECOND);
		
		String horaTotal = String.format("%02d", (dia*24) + hora ).concat(":").concat(String.format("%02d", minuto)).concat(":").concat(String.format("%02d", 0));
		
		registroAgendDTO.setTotalHr(horaTotal);		
		registroAgendDTO.setVlSalario(totalVlSalario);		
		
		return registroAgendDTO;
		
	}
	
	/**
	 * Obtem registro(s) do tipo fixo, para faturamento profissional. 
	 * @param idFuncionario
	 * @param dtInicio
	 * @param dtFim
	 * @param tipoAtendimento
	 * @return
	 */
	private RegistroAgendamentoDTO getFaturamentoProfissionalFixo(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){
				
		List<UnidadeDTO> listUnidade = new ArrayList<UnidadeDTO>();
		Double totalVlSalario = 0.0;
		
		List<RegistroAtaDTO> listAtendimentoCliente = registroAgendamentoDAO.getRegistrosAgendamentoRealizados(idFuncionario, dtInicio, dtFim, tipoAtendimento);
		
		List<RegistroAtaDTO> listHorarioAtendimento = registroAgendamentoDAO.getRegistrosAgendamentoHora(idFuncionario, dtInicio, dtFim, tipoAtendimento);
		RegistroAgendamentoDTO registroAgendDTO = getTotalHoraTrabalhada(listHorarioAtendimento, idFuncionario);

		List<RegistroAtaDTO> listFixoAtendimento = new ArrayList<RegistroAtaDTO>();
		if(tipoAtendimento.equals(TipoAtendimento.FIXO.getId())){
			listUnidade = registroAgendamentoDAO.getUnidadeTrabalhadaByTipoAtendimento(idFuncionario, dtInicio, dtFim, tipoAtendimento);		
			for(UnidadeDTO unidDTO: listUnidade)
				listFixoAtendimento.addAll(registroAgendamentoDAO.getRegistrosAgendamentoFixo(idFuncionario, dtInicio, dtFim, tipoAtendimento, unidDTO.getIdUnidade()));
		}
		
		for(RegistroAtaDTO lfa : listFixoAtendimento){
			
			String[] splitTurno;
			String[] splitTurnoDatail;
			double vlTurno = 0.0;						
			double vlDiariaFinal = 0.0;
			
			splitTurno = lfa.getStatusTurno().split(",");
			vlDiariaFinal = lfa.getVlDiaria() != null ? lfa.getVlDiaria() : 0;
			
			if(lfa.getStatusDesconto().equals(2)){		
				
				if(splitTurno.length <= 1)
					lfa.setStatusDesconto(1);
				else{				
					vlDiariaFinal = lfa.getVlDiaria() * splitTurno.length;
					vlTurno = lfa.getVlDiaria();				
					
					int x = 0;
					while(x < splitTurno.length){
						splitTurnoDatail = splitTurno[x].split("-");
						if(splitTurnoDatail[1].equals("1")) // 1 - Descontar turno
							vlDiariaFinal += -vlTurno;
						
						x++;
					}
					
					lfa.setVlDiaria(vlDiariaFinal);
										
				}
			}
			else
				if(splitTurno.length > 1){
					vlDiariaFinal = lfa.getVlDiaria() * splitTurno.length;
					lfa.setVlDiaria(vlDiariaFinal);
				}
			
			if(lfa.getStatusDesconto().equals(0) || lfa.getStatusDesconto().equals(2)) //Diaria Aprovada ou Parcial
				totalVlSalario += vlDiariaFinal;
		}
				
		registroAgendDTO.setListAtendimentoCliente(listAtendimentoCliente);		
		registroAgendDTO.setListFixoAtendimento(listFixoAtendimento);
		registroAgendDTO.setTotalServico(listAtendimentoCliente.size());		
		registroAgendDTO.setVlSalario(totalVlSalario);
		
		return registroAgendDTO;
	}
	
	private RegistroAgendamentoDTO getFaturamentoProfissionalProducao(Long idFuncionario, String dtInicio, String dtFim, Integer tipoAtendimento){
						
		Double totalVlSalario = 0.0;
				
		List<RegistroAtaDTO> listAtendimentoCliente = registroAgendamentoDAO.getRegistrosAgendamentoRealizados(idFuncionario, dtInicio, dtFim, tipoAtendimento);		
		List<RegistroAtaDTO> listAtendimentoClienteParticular = registroAgendamentoDAO.getRegistrosAgendamentoRealizados(idFuncionario, dtInicio, dtFim, TipoAtendimento.PARTICULAR.getId());
		
		if(listAtendimentoClienteParticular != null && listAtendimentoClienteParticular.size() > 0){
			for(RegistroAtaDTO rata : listAtendimentoClienteParticular){
				listAtendimentoCliente.add(rata);
			}
		}		
		
		List<RegistroAtaDTO> listHorarioAtendimento = registroAgendamentoDAO.getRegistrosAgendamentoHora(idFuncionario, dtInicio, dtFim, tipoAtendimento);
		RegistroAgendamentoDTO registroAgendDTO = getTotalHoraTrabalhada(listHorarioAtendimento, idFuncionario);
				
		for(RegistroAtaDTO lac : listAtendimentoCliente)			
				totalVlSalario += lac.getVlComissao() != null ? lac.getVlComissao() : 0;
		
		registroAgendDTO.setListAtendimentoCliente(listAtendimentoCliente);		
		registroAgendDTO.setTotalServico(listAtendimentoCliente.size());		
		registroAgendDTO.setVlSalario(totalVlSalario);
		
		return registroAgendDTO;
	}
	
	private RegistroAgendamentoDTO getFaturamentoProfissionalRespTecnico(Long idFuncionario, String dtInicio){
		
		Double totalVlSalario = 0.0;
		List<RegistroAtaDTO> listRt = new ArrayList<RegistroAtaDTO>();
		RegistroAgendamentoDTO registroAgendDTO = new RegistroAgendamentoDTO();
				
		List<SalarioMd> listSalarioMd = registroAgendamentoDAO.getSalarioMd(idFuncionario, dtInicio);
		
		if(listSalarioMd != null && listSalarioMd.size() > 0){
			for(SalarioMd smd : listSalarioMd){
				
				RegistroAtaDTO rAta = new RegistroAtaDTO();
				rAta.setIdUnidade(smd.getIdUnidade());
				rAta.setVlTotal(smd.getVlSalario());
				listRt.add(rAta);
			}
									
			for(RegistroAtaDTO lac : listRt)			
					totalVlSalario += lac.getVlTotal() != null ? lac.getVlTotal() : 0;
		}
		
		registroAgendDTO.setListRespTecnico(listRt);		
		registroAgendDTO.setVlSalario(totalVlSalario);
		registroAgendDTO.setTotalServico(0);
		registroAgendDTO.setTotalHr("00:00:00");
		
		return registroAgendDTO;
	}
}
