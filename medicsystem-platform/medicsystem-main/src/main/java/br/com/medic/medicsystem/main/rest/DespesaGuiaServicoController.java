package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.DespesaGuiaServicoService;
import br.com.medic.medicsystem.persistence.model.Guia;

@Path("/despesa_guia_servico")
@RequestScoped
public class DespesaGuiaServicoController {

	@Inject
	private DespesaGuiaServicoService despesaGuiaServicoService;
	
	@GET
	@Path("/search_single/despesa_servico/{id:[0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getGuiaByIdDespesaServico(@PathParam("id") Long idDespesaServico) {
		Guia guia = despesaGuiaServicoService.getGuiaByIdDespesaServico(idDespesaServico);
		if(null != guia) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(guia).build();
		} else {
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
}
