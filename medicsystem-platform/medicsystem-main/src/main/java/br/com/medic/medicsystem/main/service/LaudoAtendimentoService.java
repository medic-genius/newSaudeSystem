package br.com.medic.medicsystem.main.service;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.samples.AbstractReportGenerator;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.jasper.JRDataSourceLaudoMedico;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoDAO;
import br.com.medic.medicsystem.persistence.dao.LaudoAtendimentoDAO;
import br.com.medic.medicsystem.persistence.dao.LaudoAtendimentoViewDAO;
import br.com.medic.medicsystem.persistence.dto.LaudoAtendimentoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.LaudoAtendimento;
import br.com.medic.medicsystem.persistence.model.views.LaudoAtendimentoView;

@Stateless
public class LaudoAtendimentoService {
	
	@Inject
	@Named("laudoatendimento-dao")
	private LaudoAtendimentoDAO laudoAtendimentoDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("atendimento-dao")
	private AtendimentoDAO atendimentoDAO;
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("laudoatendimentoview-dao")
	private LaudoAtendimentoViewDAO laudoAtendimentoViewDAO;


	public LaudoAtendimento getLaudoAtendimento(Long idLaudoAtendimento){
		
		LaudoAtendimento laudoAtendimento = laudoAtendimentoDAO.getLaudoAtendimento(idLaudoAtendimento);	
		
		if(laudoAtendimento != null){
			
			return laudoAtendimento;
		}else
			return null;
		
	}
	
	public LaudoAtendimento getLaudoAtendimentoPorAgendamento(Long idAgendamento){
		
		LaudoAtendimento laudoAtendimento = laudoAtendimentoDAO.getLaudoAtendimentoPorAgendamento(idAgendamento);	
		
		if(laudoAtendimento != null){
			
			return laudoAtendimento;
		}else
			return null;
		
	}

	public List<LaudoAtendimentoView> getLaudoAtendimentoPorCliente(List<Long>listIdPaciente, String tipoPaciente){

		List<LaudoAtendimentoView> listLaudo = laudoAtendimentoViewDAO.getLaudoAtendimentoPorCliente(listIdPaciente, tipoPaciente);	

		if(listLaudo != null){

		return listLaudo;
		}else
		return null;

		}
	
	public byte[] saveLaudoAtendimento(Long idAgendamento, LaudoAtendimento laudoAtendimento) {

		logger.debug("salvando laudo do atendimento...");
		
		ByteArrayOutputStream output = null;
		byte[] pdf = null;		
		boolean receituarioSalvo = false;
		
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);				
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());		
		String conteudo = laudoAtendimento.getNmConteudo();
		
		LaudoAtendimentoDTO laudoDto = new LaudoAtendimentoDTO();
		laudoDto.setNmMedico(agendamentoOriginal.getProfissional().getNmFuncionario());
		laudoDto.setNmPaciente(agendamentoOriginal.getDependente() != null ? agendamentoOriginal.getDependente().getNmDependente() : agendamentoOriginal.getCliente().getNmCliente());
		laudoDto.setCrm(agendamentoOriginal.getProfissional().getNrCrmCro());
		laudoDto.setIdade(agendamentoOriginal.getDependente() != null ? agendamentoOriginal.getDependente().getIdade() : agendamentoOriginal.getCliente().getIdade());
		laudoDto.setDataExame(agendamentoOriginal.getDtAgendamentoFormatado());
		laudoDto.setEspecialidade(agendamentoOriginal.getEspecialidade().getNmEspecialidade());
		laudoDto.setConteudo(conteudo);
		laudoDto.setAssinaturaDigital(agendamentoOriginal.getProfissional().getAssinaturaDigital());
		
		try {
						
			output = new ByteArrayOutputStream();
			
		    // Generate the report		    
			new JRDataSourceLaudoMedico(laudoDto).generateReport( AbstractReportGenerator.OutputType.PDF , output);
			
			pdf = output.toByteArray();			   
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		}
		

		try {
			if (output != null) {
				output.close();
			}
		} catch (Exception e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		} 
			
		
		if(atendimentoOriginal != null ){
			
			laudoAtendimento.setAtendimento(atendimentoOriginal);
			laudoAtendimento.setArqLaudo(Base64.encodeBase64(pdf));
			
			try {
				laudoAtendimentoDAO.persist(laudoAtendimento);
				receituarioSalvo = true;
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao registrar laudo");
			}
		}else{
			
			Atendimento novoAtendimento = new Atendimento();
			novoAtendimento.setNmQueixaPaciente("");
			novoAtendimento.setDtAtendimento(Calendar.getInstance().getTime());
			novoAtendimento.setBoFinalizado(new Boolean(false));			
			novoAtendimento.setAgendamento(agendamentoOriginal);
			
			try {
				
				atendimentoDAO.persist(novoAtendimento);
				laudoAtendimento.setAtendimento(novoAtendimento);
				laudoAtendimento.setArqLaudo(Base64.encodeBase64(pdf));
				
				try {
					laudoAtendimentoDAO.persist(laudoAtendimento);
					receituarioSalvo = true;
				} catch (Exception e) {
					throw new ObjectNotFoundException(
							"Erro ao registrar laudo");
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new ObjectNotFoundException(
						
						"Erro ao registrar o novo atendimento");
			}
		}
		
		if (receituarioSalvo)
		return pdf;
		else
		return null;

	}
	
	public byte[] updateLaudoAtendimento(LaudoAtendimento laudoAtendimento) {
		
		logger.debug("atualizando laudo do atendimento...");
		
		ByteArrayOutputStream output = null;
		boolean laudoSalvo = false;
		byte[] pdf = null;

		String conteudo = laudoAtendimento.getNmConteudo();		
		LaudoAtendimentoDTO laudoDto = new LaudoAtendimentoDTO();
		
		laudoDto.setNmMedico(laudoAtendimento.getAtendimento().getAgendamento().getProfissional().getNmFuncionario());
		laudoDto.setNmPaciente(laudoAtendimento.getAtendimento().getAgendamento().getDependente() != null ? laudoAtendimento.getAtendimento().getAgendamento().getDependente().getNmDependente() : laudoAtendimento.getAtendimento().getAgendamento().getCliente().getNmCliente());
		laudoDto.setCrm(laudoAtendimento.getAtendimento().getAgendamento().getProfissional().getNrCrmCro());
		laudoDto.setIdade(laudoAtendimento.getAtendimento().getAgendamento().getDependente() != null ? laudoAtendimento.getAtendimento().getAgendamento().getDependente().getIdade() : laudoAtendimento.getAtendimento().getAgendamento().getCliente().getIdade());
		laudoDto.setDataExame(laudoAtendimento.getAtendimento().getAgendamento().getDtAgendamentoFormatado());
		laudoDto.setEspecialidade(laudoAtendimento.getAtendimento().getAgendamento().getEspecialidade().getNmEspecialidade());
		laudoDto.setConteudo(conteudo);
		laudoDto.setAssinaturaDigital(laudoAtendimento.getAtendimento().getAgendamento().getProfissional().getAssinaturaDigital());
		
		
		try {
			
			output = new ByteArrayOutputStream();
			
		    // Generate the report		    
			new JRDataSourceLaudoMedico(laudoDto).generateReport( AbstractReportGenerator.OutputType.PDF , output);
			
			pdf = output.toByteArray();			   
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		}
		
		try {
			if (output != null) {
			}
		} catch (Exception e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Laudo",
			        ErrorType.ERROR, e);
		} 
			
		
		LaudoAtendimento laudoOriginal = laudoAtendimentoDAO.getLaudoAtendimento(laudoAtendimento.getId());
						
		if(laudoOriginal != null){
			
			laudoOriginal.setNmConteudo(laudoAtendimento.getNmConteudo());
			laudoOriginal.setArqLaudo(Base64.encodeBase64(pdf));
			
			try {
				
				laudoAtendimentoDAO.update(laudoOriginal);				
				laudoSalvo = true;
				
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao atualizar o laudo");
			}
			
		}
		
		if (laudoSalvo)
			return pdf;
		else 
			return null;
		
	}	
	

}