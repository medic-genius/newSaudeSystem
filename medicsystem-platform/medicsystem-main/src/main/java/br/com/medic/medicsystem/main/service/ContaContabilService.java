package br.com.medic.medicsystem.main.service;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.ContaContabilDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dto.ContaContabilDTO;
import br.com.medic.medicsystem.persistence.model.ContaContabil;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;

@Stateless
public class ContaContabilService {
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("contacontabil-dao")
	private ContaContabilDAO contaContabilDAO;
	
	@Inject
	@Named("empresafinanceira-dao")
	private EmpresaFinanceiroDAO empresaFinanceiroDAO;
	
	public ContaContabil saveContaContabil(ContaContabil contaContabil){

		if(contaContabil.getId() == null){
			contaContabil  = contaContabilDAO.persist(contaContabil);
		}else{
			contaContabil  = contaContabilDAO.update(contaContabil);
		}
		return contaContabil;
	}
	
	public ContaContabil getContaContabilById(Long id){
		ContaContabil contaContabil = contaContabilDAO.searchByKey(ContaContabil.class, id);

		if(contaContabil.getDtExclusao() != null){
			throw new IllegalArgumentException("Conta inativa");
		}
		return contaContabil;
	}
	
	public List<ContaContabil> getAllContasContabil( ){
		List<ContaContabil> list = contaContabilDAO.getAllContasContabil(null);
		return list;
	}	
	
	public List<ContaContabil> getBuscaContasContabil(String nmContaContabil, Integer startPosition, Integer maxResults){
		
		logger.debug("Obtendo lista de servicos...");
		return contaContabilDAO.getBuscaContasContabil(nmContaContabil, startPosition, maxResults);
	}

	public List<ContaContabil> getAllContasContabil( int mesSplit, int anoSplit, List<String> idEmpresa, Long idContaContabil, int tipoStatus){
		String idEmpresaConver;
		if(idEmpresa.get(0).equals("0")) {
			List<EmpresaFinanceiro> listEmpresas = empresaFinanceiroDAO.getEmpresasFinanceiro(true);
			List<String> listStringEmp = new ArrayList<String>();
			for(int i =0; i < listEmpresas.size(); i++) {
				listStringEmp.add(i, listEmpresas.get(i).getIdEmpresaFinanceiro().toString());
			}
			 idEmpresaConver = converterListaEmString(listStringEmp);
		}else {
			 idEmpresaConver = converterListaEmString(idEmpresa);
		}
		List<ContaContabil> list = contaContabilDAO.getAllContasContabil(idContaContabil);
			somaValorCC(list, 0, list.size(), mesSplit, anoSplit, idEmpresaConver, tipoStatus);
			setOrdemContaContabil(list, null);
		
		return list;
	}
	
	private BigDecimal getValorCC(Long idCC, Integer mes, Integer ano, String idEmpresa, Integer tipoStatus){
		
		List<ContaContabilDTO> resulta = contaContabilDAO.getRelatorioTodasContaContabil(mes, ano, idCC, null, idEmpresa, tipoStatus);
		Double valorTotal = new Double(0);
				
		for (ContaContabilDTO contaContabilDTO : resulta) {
			valorTotal+= contaContabilDTO.getTotalDespesa();
		}
				
		BigDecimal vv = new BigDecimal (valorTotal);
		
		return vv.setScale(2, RoundingMode.HALF_EVEN);
		
	}
	
	private BigDecimal somaValorCC(List<ContaContabil> list, Integer i, Integer size, Integer mesSplit, Integer anoSplit, String idEmpresa, Integer tipoStatus){
		
		if(list != null && i < size) {
		  	if(list.get(i).getContasContabil() == null || list.get(i).getContasContabil().size() == 0) {
		  		list.get(i).setMesAno(mesSplit+"-"+anoSplit);
		  		list.get(i).setMes(mesSplit);
		  		list.get(i).setAno(anoSplit);
		  		list.get(i).setValorContaContabil(getValorCC(list.get(i).getId(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		    	return list.get(i).getValorContaContabil().add(somaValorCC(list, i+1, list.size(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		    } 
		  	list.get(i).setMesAno(mesSplit+"-"+anoSplit);
		  	list.get(i).setMes(mesSplit);
	  		list.get(i).setAno(anoSplit);
	  		list.get(i).setValorContaContabil(getValorCC(list.get(i).getId(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		  	list.get(i).setValorContaContabil(list.get(i).getValorContaContabil().add(somaValorCC(list.get(i).getContasContabil(), 0, list.get(i).getContasContabil().size(), mesSplit, anoSplit, idEmpresa, tipoStatus)));
		    return list.get(i).getValorContaContabil().add(somaValorCC(list, i+1, list.size(), mesSplit, anoSplit, idEmpresa, tipoStatus));
		  }
		BigDecimal zero = new BigDecimal(0);
		return zero;
	}

	public void deleteContaContabil(Long idContaContabil){
		ContaContabil contaContabil = getContaContabilById(idContaContabil);
		contaContabil.setDtExclusao(new Date());
		setDtExclusaoContasContabil(contaContabil);
	}
	
	private void setDtExclusaoContasContabil(ContaContabil contaContabil){
		for (ContaContabil c : contaContabil.getContasContabil()) {
			c.setDtExclusao(contaContabil.getDtExclusao());
			if(c.getContasContabil() != null && c.getContasContabil().size() > 0){
				setDtExclusaoContasContabil(c);
			}
		}
	}
	
	private void setOrdemContaContabil(List<ContaContabil> contas, String str){
		int count = 1;
		if(str == null)
			str = "";
		
		for (ContaContabil c : contas) {
			c.setNrOrdem(str + count);
			if(c.getContasContabil() != null && c.getContasContabil().size() > 0){
				setOrdemContaContabil(c.getContasContabil(), c.getNrOrdem() + ".");
			}
			count++;
		}
	}


	
	public List<ContaContabil> getAllContaContabil() {
		List<ContaContabil> list = contaContabilDAO.getAllContaContabil();
		setOrdemContaContabil(list, null);
		return list;
	}
	
	
	public List<Double> getAnosDespesas(){
		
		List<Double> listAnos = contaContabilDAO.getAnosDespesas();
		return listAnos;
	}
	
	public List<ContaContabil> getDespesasAnaliseH(List<String> idEmpresa, Long idContaContabil, String anoForm) {
		
		String resultIds = converterListaEmString(idEmpresa);
		List<ContaContabil> list = contaContabilDAO.getAllContasContabil(idContaContabil);
		somaValorCCAH(list,0,list.size(),resultIds, anoForm);
		setOrdemContaContabil(list, null);
				
	
		return list;
	}
	
	public String converterListaEmString(List<String> lista) {
		StringBuilder str = new StringBuilder();
		for (String elemento : lista) {
			str.append(elemento).append(",");
		}
		
		if(str.toString().isEmpty()) {
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
	
	}
	
	private BigDecimal somaValorCCAH(List<ContaContabil> list, Integer i, Integer size, String resultIds, String anoForm){
		
		Double anoBase = contaContabilDAO.getAnoBaseDespesa();
		DecimalFormat abf = new DecimalFormat("#");
		String anoB = abf.format(anoBase);
		
		
		if(list != null && i < size) {
		  	if(list.get(i).getContasContabil() == null || list.get(i).getContasContabil().size() == 0) {
		  		list.get(i).setAnoAH(anoForm);
		  		if(anoB.equals(anoForm)) {
		  			list.get(i).setValorContaContabilBase(getValorCCAH(list.get(i).getId(), resultIds, anoForm));
		  		} else {
		  			list.get(i).getValorContaContabilBase();
		  		}
		  		list.get(i).setValorContaContabil(getValorCCAH(list.get(i).getId(), resultIds, anoForm));
		  		list.get(i).setAnoBase(anoB);
		    	return list.get(i).getValorContaContabil().add(somaValorCCAH(list, i+1, list.size(), resultIds, anoForm));
		    } 
		  	list.get(i).setAnoAH(anoForm);
		  	list.get(i).setAnoBase(anoB);
		  	if(anoB.equals(anoForm)) {
		  		list.get(i).setValorContaContabilBase(getValorCCAH(list.get(i).getId(), resultIds, anoForm));
			  	list.get(i).setValorContaContabilBase(list.get(i).getValorContaContabilBase().add(somaValorCCAH(list.get(i).getContasContabil(), 0, list.get(i).getContasContabil().size(), resultIds, anoForm)));
		  	} else {
		  		list.get(i).getValorContaContabilBase();
		  	}
	  		list.get(i).setValorContaContabil(getValorCCAH(list.get(i).getId(), resultIds, anoForm));
		  	list.get(i).setValorContaContabil(list.get(i).getValorContaContabil().add(somaValorCCAH(list.get(i).getContasContabil(), 0, list.get(i).getContasContabil().size(), resultIds, anoForm)));
		    return list.get(i).getValorContaContabil().add(somaValorCCAH(list, i+1, list.size(), resultIds, anoForm));
		}
		BigDecimal zero = new BigDecimal(0);
		return zero;
	}

	
	private BigDecimal getValorCCAH(Long idCC, String idEmpresa, String anoForm){
		
		List<ContaContabilDTO> resulta = contaContabilDAO.getRelatorioAnaliseHorizontal(idCC, idEmpresa, anoForm);
		Double valorTotal = new Double(0);
				
		for (ContaContabilDTO contaContabilDTO : resulta) {
			valorTotal+= contaContabilDTO.getTotalDespesa();
		}
				
		BigDecimal vv = new BigDecimal (valorTotal);
		
		return vv.setScale(2, RoundingMode.HALF_EVEN);
		
	}

}
