package br.com.medic.medicsystem.main.rest;

import java.text.ParseException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;

import br.com.medic.medicsystem.main.service.ClienteService;
import br.com.medic.medicsystem.main.service.SMSService;
import br.com.medic.medicsystem.main.util.ResponseData;
import br.com.medic.medicsystem.persistence.dto.InformativoDTO;
import br.com.medic.medicsystem.persistence.dto.SMSDTO;
import br.com.medic.medicsystem.persistence.model.Informativo;
import br.com.medic.medicsystem.sms.exception.ClientHumanException;


@Path("/sms")
@RequestScoped
public class SMSController extends BaseController{
	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private SMSService smsService;
	
	
	@GET
	@Path("/informativo")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Informativo> getInformativo() {

		return clienteService.getInformativo();

	}
	
	@POST
	@Path("/informativoinfo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseData<String, List<?>> enviarInformativoSMS(List<InformativoDTO> listaInformativo,
			@QueryParam("tipoenvio")Integer tipoEnvio)
	        throws ClientHumanException, ParseException {
		return smsService.enviarInformativoSMS(listaInformativo, tipoEnvio);
	}
	
	@POST
	@Path("/agendamentos/faltamedico")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<JSONObject> sendInformativoFaltaMedico(List<InformativoDTO> list) {
		try {
			return smsService.enviarInformativoFaltaMedico(list);
		} catch (ParseException | ClientHumanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/api_callback")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject receivedResponse(String body) {

		if(body != null && !body.isEmpty()) {
			smsService.registerSmsResponse(body);
		}
		
		JSONObject response = new JSONObject();
		response.put("status", 200);
		response.put("mensagem", "Resposta do servidor Medic");
		return response;
	}
	
	@GET
	@Path("/espmedico/{idFuncionario:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SMSDTO> getAgendamentosAtendidosProfissional(
			@QueryParam("idEspecialidade") Long idEspecialidade,
			@PathParam("idFuncionario")Long idFuncionario,
			@QueryParam("idUnidade")Long idUnidade ) {

		return smsService.getSMSByEspProfissional(idFuncionario, idEspecialidade, idUnidade);

	}
}
