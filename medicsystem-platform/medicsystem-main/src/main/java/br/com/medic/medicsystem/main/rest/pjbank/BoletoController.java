package br.com.medic.medicsystem.main.rest.pjbank;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;

@Path("/boleto")
@RequestScoped
public class BoletoController extends BaseController{
	
	@Inject
	private BoletoServicePJB boletoServicePJB;

	@GET
	@Path("pjbank/liquidar")
	@Produces(MediaType.TEXT_PLAIN)
	public String liquidarBoletos(@QueryParam("periodoDias")Integer periodoDias){
		
		Integer nrBoletosLiquidados = 0;
		
		nrBoletosLiquidados = boletoServicePJB.liquidarCobrancaSl(periodoDias);
		
		return "Boletos liquidados " + nrBoletosLiquidados;
		
	}
	
	@GET
	@Path("pjbank/liquidar/pordata")
	@Produces(MediaType.TEXT_PLAIN)
	public String liquidarBoletos(
			@QueryParam("dataInicio")String dataInicio,
			@QueryParam("dataFim")String dataFim){
		
		Integer nrBoletosLiquidados = 0;
		
		nrBoletosLiquidados = boletoServicePJB.liquidarCobrancaSl(dataInicio, dataFim);
		
		return "Boletos liquidados " + nrBoletosLiquidados;
		
	}
	
	@GET
	@Path("pjbank/liquidar/pendente")
	@Produces(MediaType.TEXT_PLAIN)
	public String liquidarBoletosPendentes(
			@QueryParam("dataInicio")String dataInicio,
			@QueryParam("dataFim")String dataFim){
		
		
		String msg = boletoServicePJB.liquidarCobrancaSlPendentes(dataInicio,dataFim);
		
		return msg;
		
	}
}
