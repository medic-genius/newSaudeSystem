package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;


import br.com.medic.medicsystem.persistence.dao.PreAnamneseDAO;
import br.com.medic.medicsystem.persistence.model.PreAnamnese;


@Stateless
public class PreAnamneseService {
	
	@Inject
	private Logger logger;
	
	@Inject
	@Named("preanamnese-dao")
	private PreAnamneseDAO preAnamneseDAO;
	
	public List<PreAnamnese>  getAnamneseQuestionario() {
		return preAnamneseDAO.getPreAnamnese();
	 }
	

	

}
