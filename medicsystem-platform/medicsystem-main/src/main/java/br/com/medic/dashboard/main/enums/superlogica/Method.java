package br.com.medic.dashboard.main.enums.superlogica;

public enum Method {
	 POST, GET, PUT, DELETE;
}
