package br.com.medic.medicsystem.main.vindi.data.parameters.create;

import java.util.Map;

import br.com.medic.medicsystem.main.vindi.data.parameters.PricingSchema;

public class Product {
	private String name;
	private String code;
	private String unit;
	private String status;
	private String description;
	private String invoice;
	private PricingSchema pricing_schema;
	private Map<String, String> metadata;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public PricingSchema getPricing_schema() {
		return pricing_schema;
	}
	public void setPricing_schema(PricingSchema pricing_schema) {
		this.pricing_schema = pricing_schema;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
}
