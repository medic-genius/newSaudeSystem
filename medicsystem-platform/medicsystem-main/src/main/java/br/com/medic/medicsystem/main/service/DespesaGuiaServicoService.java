package br.com.medic.medicsystem.main.service;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.DespesaGuiaServicoDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaServicoDAO;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Guia;

public class DespesaGuiaServicoService {

	@Inject
	@Named("despesaguiaservico-dao")
	private DespesaGuiaServicoDAO despesaGuiaServicoDAO;
	
	@Inject
	@Named("despesaservico-dao")
	private DespesaServicoDAO despesaServicoDAO;

	public Guia getGuiaByIdDespesaServico(Long idDespesaServico) {
		DespesaServico despesaServico = despesaGuiaServicoDAO.searchByKey(DespesaServico.class, idDespesaServico);
		return despesaGuiaServicoDAO.getGuiaByDespesaServico(despesaServico);
	}
}
