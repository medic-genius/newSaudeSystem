package br.com.medic.medicsystem.main.util;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.com.medic.medicsystem.main.exception.EmailException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.persistence.model.EmailMessage;

public class EmailUtil {	

	public static final String ADDRESS_EMAIL_SYSTEM = "nao-responda@mail.consultasmais.com.br";
	
	public static final String CLIENT_COMPANY = "Mais Consulta";
	
	private static final String HOST = "smtp.gmail.com";
	private static final String LOGIN = "noreplaydrmaisconsulta@gmail.com";
	private static final String PASSWORD = "@medicti2017";
	
	public static void sendEmail(String emailCliente, String nomeCliente, String assunto, String mensagem){

		try {
			String host = "smtp.gmail.com";
			String login = "noreplaydrmaisconsulta@gmail.com";
	        String Password = "@medicti2017";
	        String from = "nao-responda@mail.consultasmais.com.br";
	        String toAddress = emailCliente;
	        
	        // Propriedades do sistema
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        Session session = Session.getInstance(props, null);
	
	        MimeMessage message = new MimeMessage(session);
	
	        try {
				message.setFrom(new InternetAddress(from, CLIENT_COMPANY + " - Não Responda"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        message.setRecipients(Message.RecipientType.TO, toAddress);
	        message.setSubject(assunto);
	        BodyPart messageBodyPart = new MimeBodyPart();

	        messageBodyPart.setContent(mensagem, "text/html; charset=utf-8");
	
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        messageBodyPart = new MimeBodyPart();
	        
	        message.setContent(multipart, "text/html; charset=utf-8");

            Transport tr = session.getTransport("smtps");
            tr.connect(host, login, Password);
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
        } catch (SendFailedException sfe) {
        	sfe.printStackTrace();
			throw new EmailException("Erro ao enviar por email", ErrorType.DANGER);
        } catch (MessagingException e) {
			e.printStackTrace();
			throw new EmailException("Erro ao enviar por email", ErrorType.DANGER);
		}
	}
	
	public static void sendEmailBoletoPagamentoAgendamento(String emailCliente, String assunto, 
			String mensagem, String boletoUrl, String boletoNome) {
		try {
			String host = "smtp.gmail.com";
			String login = "noreplaydrmaisconsulta@gmail.com";
	        String Password = "@medicti2017";
	        String from = "nao-responda@mail.consultasmais.com.br";
	        String toAddress = emailCliente;
	        
	        // Propriedades do sistema
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        Session session = Session.getInstance(props, null);
	
	        MimeMessage message = new MimeMessage(session);
	
	        try {
				message.setFrom(new InternetAddress(from, CLIENT_COMPANY + " - Não Responda"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        message.setRecipients(Message.RecipientType.TO, toAddress);
	        message.setSubject(assunto);
	        BodyPart messageBodyPart = new MimeBodyPart();

	        messageBodyPart.setContent(mensagem, "text/html; charset=utf-8");
	
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        messageBodyPart = new MimeBodyPart();
	        
	        try {
	        	MimeBodyPart attachPart = new MimeBodyPart();
	        	URL url = new URL(boletoUrl);
	        	attachPart.setDataHandler(new DataHandler(url));
	        	attachPart.setDisposition(MimeBodyPart.ATTACHMENT);
	        	attachPart.setFileName(boletoNome);
	        	multipart.addBodyPart(attachPart);
	        } catch (Exception ex) {
	        	ex.printStackTrace();
	        }
	        
	        message.setContent(multipart, "text/html; charset=utf-8");

            Transport tr = session.getTransport("smtps");
            tr.connect(host, login, Password);
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
        } catch (SendFailedException sfe) {
        	sfe.printStackTrace();
			throw new EmailException("Erro ao enviar por email", ErrorType.DANGER);
        } catch (MessagingException e) {
			e.printStackTrace();
			throw new EmailException("Erro ao enviar por email", ErrorType.DANGER);
		}
	}
	
	
	public static void sendEmailGeneric(InternetAddress[] lRecipients, EmailMessage emailMessage){
		try {
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("user", "noreplaydrmaisconsulta@gmail.com");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("pass", "@medicti2017");
	        Session session = Session.getInstance(props, null);
	        MimeMessage message1 = new MimeMessage(session);
	        try {
				message1.setFrom(new InternetAddress(ADDRESS_EMAIL_SYSTEM, CLIENT_COMPANY + " - Não Responda"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        message1.setRecipients(Message.RecipientType.TO, lRecipients);
	        message1.setSubject(emailMessage.getSubject());
	        if(emailMessage.getReplyTo() != null)
	        {
		        InternetAddress[] replyTo = {
		        	new InternetAddress(emailMessage.getReplyTo())
				};
		        message1.setReplyTo(replyTo);
	        }
	        BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(emailMessage.getMessage(), "text/html; charset=utf-8");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        messageBodyPart = new MimeBodyPart();
	        message1.setContent(multipart, "text/html; charset=utf-8");
            Transport tr = session.getTransport("smtps");
            tr.connect((String) props.get("mail.smtp.host"), (String) props.get("user"), (String) props.get("pass"));
            tr.sendMessage(message1, message1.getAllRecipients());
            tr.close();
        } catch (SendFailedException sfe) {
        	sfe.printStackTrace();
			throw new EmailException("Error on email sending; ", ErrorType.DANGER);
        } catch (MessagingException e) {
			e.printStackTrace();
			throw new EmailException("Error on email sending; ", ErrorType.DANGER);
		}
	}
	
	public static boolean sendEmailGeneric(InternetAddress[] listTo,
			InternetAddress[] listCC, InternetAddress[] listCCo,
			String subject, String emailMessage) {
		try {
			Properties props = System.getProperties();
			props.put("mail.smtp.host", HOST);
			props.put("mail.smtps.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");

			props.put("user", LOGIN);
			props.put("pass", PASSWORD);

			Session session = Session.getInstance(props, null);
			MimeMessage mmessage = new MimeMessage(session);
			mmessage.setFrom(new InternetAddress(ADDRESS_EMAIL_SYSTEM,
					CLIENT_COMPANY + " - Não Responda"));

			if (listTo != null && listTo.length > 0) {
				mmessage.setRecipients(Message.RecipientType.TO, listTo);
			}
			if (listCC != null && listCC.length > 0) {
				mmessage.setRecipients(Message.RecipientType.CC, listCC);
			}
			if (listCCo != null && listCCo.length > 0) {
				mmessage.setRecipients(Message.RecipientType.BCC, listCCo);
			}

			mmessage.setSubject(subject);

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart
					.setContent(emailMessage, "text/html; charset=utf-8");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			mmessage.setContent(multipart, "text/html; charset=utf-8");

			Transport tr = session.getTransport("smtps");
			tr.connect(HOST, LOGIN, PASSWORD);
			tr.sendMessage(mmessage, mmessage.getAllRecipients());
			tr.close();
			return true;
		} catch (SendFailedException sfe) {
			sfe.printStackTrace();
			throw new EmailException("Erro ao enviar por email",
					ErrorType.DANGER);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new EmailException("Erro ao enviar por email",
					ErrorType.DANGER);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return false;
	}
}



