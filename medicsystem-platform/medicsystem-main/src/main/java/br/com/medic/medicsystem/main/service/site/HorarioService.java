package br.com.medic.medicsystem.main.service.site;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.service.FuncionarioService;
import br.com.medic.medicsystem.main.util.SimpleKVGenericDTO;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoProfissionalDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dto.VagaDTO;
import br.com.medic.medicsystem.persistence.model.AtendimentoProfissional;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoProfissionalView;
import br.com.medic.medicsystem.persistence.utils.DateUtil;


@Stateless
public class HorarioService {
	
	@Inject
	@Named("atendimentoprofissional-dao")
	private AtendimentoProfissionalDAO atendimentoProfissionalDAO;
	
	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	private FuncionarioService funcionarioService;
	
	
	private Date getDateConfig(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public List<AtendimentoProfissionalView> getDiasAtendimentoProfissional(Long idEspecialidade) {
		
		List<AtendimentoProfissionalView> result = atendimentoProfissionalDAO.getProfissionaisByEspecialidade(idEspecialidade, null, false);
		List<AtendimentoProfissionalView> atendimento = new ArrayList<AtendimentoProfissionalView>();
		
		if(result != null && result.size() > 0){
			for (AtendimentoProfissionalView at : result) {
				
				List<VagaDTO> vagasNormal =  getDiasAtendimentoProfissional(at.getIdProfissional(), at.getIdServico(), at.getIdEspProfissional(), 
			    at.getInTipoConsulta(), 0, false);
				
				List<VagaDTO> vagasParticular =  getDiasAtendimentoProfissional(at.getIdProfissional(), at.getIdServico(), at.getIdEspProfissional(), 
					    at.getInTipoConsulta(), 0, true);
				
				if(vagasNormal != null && vagasNormal.size() > 0){
					at.setVagas(vagasNormal);
					
					if(vagasParticular != null && vagasParticular.size() > 0){
						at.setVagasParticular(vagasParticular);
					}
					
					atendimento.add(at);
				}
				
				if((vagasNormal == null || vagasNormal.size() == 0) && (vagasParticular != null && vagasParticular.size() > 0)){
					at.setVagasParticular(vagasParticular);
						atendimento.add(at);
				}
				
			}
			
		}

		return atendimento;
	}
	
	
	public List<VagaDTO> getDiasAtendimentoProfissional(Long idProfissional, Long idServico,
	        Long idEspecialidade, Integer inTipoConsulta, Integer inTipo, boolean boPaticular) {
		
		
		SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat angularFormatter = new SimpleDateFormat("dd/MM/yyyy");

		Calendar nowTime = Calendar.getInstance();
		nowTime.setTime(getDateConfig());
		
		Calendar limitConsulta = Calendar.getInstance();
		limitConsulta.setTime(getDateConfig());
		limitConsulta.add(Calendar.MONTH, 3);
		
		int qtdvagas = 0;

		
		List<VagaDTO> vagas = new ArrayList<VagaDTO>();
		
		do {
			VagaDTO vaga = new VagaDTO();
			String StrDiaVaga = sqlFormatter.format(nowTime.getTime());
			String dataFormatada = angularFormatter.format(nowTime.getTime());
			List<Long> bloqueiosUnidade = funcionarioDAO.getBloqueioByprofissional(idProfissional, StrDiaVaga);
			
			String diasDaSemanaAtendimento = atendimentoProfissionalDAO
			        .getDiasDaSemanaAtendimentoProfissional(idProfissional,idServico,
			                idEspecialidade, bloqueiosUnidade,
			                inTipoConsulta, StrDiaVaga, boPaticular);
			
			if(diasDaSemanaAtendimento != null && diasDaSemanaAtendimento.length() > 0){
				
				Long idUnidadeAtendimento = Long.parseLong(diasDaSemanaAtendimento.split("/")[1]);
				String nmUnidadeAtendimento = diasDaSemanaAtendimento.split("/")[2];
				String nmLogradouro = diasDaSemanaAtendimento.split("/")[3];
				String nrNumero = diasDaSemanaAtendimento.split("/")[4];
				String nmBairro = diasDaSemanaAtendimento.split("/")[5];
				String nmCidade = diasDaSemanaAtendimento.split("/")[6];
				
				HashMap<String, String> disponibilidadeMes = atendimentoProfissionalDAO
				        .getDisponibilidadeProfissional(idProfissional, idServico,
				                idEspecialidade, idUnidadeAtendimento, null,
				                inTipoConsulta, StrDiaVaga, StrDiaVaga, boPaticular);
				
					if(disponibilidadeMes != null ){
						
						String disponibilidade = disponibilidadeMes.get(StrDiaVaga);
						if(disponibilidade != null){
							String[] split = disponibilidade.split("/");
							if ((Integer.parseInt(split[0]) < Integer.parseInt(split[1]) || Integer.parseInt(split[2]) != new Integer(0) )) {
								vaga = new VagaDTO(idUnidadeAtendimento, nmUnidadeAtendimento, dataFormatada, 
										nmLogradouro, nrNumero, nmBairro, nmCidade, boPaticular);
								vagas.add(vaga);
								qtdvagas++;
							}
						}else{
							vaga = new VagaDTO(idUnidadeAtendimento, nmUnidadeAtendimento, dataFormatada, 
									nmLogradouro, nrNumero, nmBairro, nmCidade, boPaticular);
							vagas.add(vaga);
							qtdvagas++;
						}
						
					}else{
						vaga = new VagaDTO(idUnidadeAtendimento, nmUnidadeAtendimento, dataFormatada, 
								nmLogradouro, nrNumero, nmBairro, nmCidade, boPaticular);
						vagas.add(vaga);
						qtdvagas++;
					}
			}
			
			nowTime.add(Calendar.DAY_OF_YEAR, 1);
			
		} while (qtdvagas < 5 && DateUtil.isDataPosteriorOuIgual(limitConsulta.getTime(), nowTime.getTime()));	

		return vagas;
	}
	
	private Calendar truncarCalendar(Calendar calendar){
		
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar;
		
	}
	
	
	public HashMap<String, List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>>> getHorariosAtendimentoProfissional(
	        Long idProfissional, Long idServico, Long idEspecialidade, Long idUnidade, String date, Boolean boParticular){
		
		HashMap<String, List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>>> rangeHorarios = 
				new HashMap<String,List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>>>();
		
		Calendar calendar = Calendar.getInstance();
		
		List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>> horarios = funcionarioService.getHorarioAtendimentoProfissional(
                idProfissional, idServico, idEspecialidade, idUnidade, null, 0, 0, date, boParticular, new Boolean(true));
		
		for (SimpleKVGenericDTO<String, String, Boolean, Long, Boolean> horario : horarios) {
			
			AtendimentoProfissional atend = atendimentoProfissionalDAO.
					searchByKey(AtendimentoProfissional.class, horario.getIdAtendimentoProfissional());
			
			if(atend != null){
				SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
				Integer hora = Integer.parseInt(horario.getData().split(":")[0]);
				Integer minuto = Integer.parseInt(horario.getData().split(":")[1]);
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, hora);
				cal.set(Calendar.MINUTE, minuto);
				
				
				Date dataAgendamentoHora = DateUtil.configDate(cal.getTime().getTime());
				Date dataInitProf = DateUtil.configDate(atend.getHrInicio().getTime());
				Date dataFimProf = DateUtil.configDate(atend.getHrFim().getTime());
				Date dataInitProfMin = DateUtil.configDate(atend.getHrOrdemInicio().getTime());
				Date dataFimProfMax = DateUtil.configDate(atend.getHrOrdemFim().getTime());
				
				calendar.setTimeInMillis(DateUtil.mapDatas(dataAgendamentoHora, dataInitProf, dataFimProf, dataInitProfMin, dataFimProfMax));
				calendar = truncarCalendar(calendar);
				String keyHora = sdf.format(calendar.getTime());
				calendar.add(Calendar.HOUR_OF_DAY, 1);
				
				if(calendar.getTime().after(dataFimProfMax)){
				  keyHora += "-"+sdf.format(dataFimProfMax.getTime());
				}else{
				  keyHora += "-"+sdf.format(calendar.getTime());
				}
				
				List<SimpleKVGenericDTO<String, String, Boolean, Long, Boolean>> horariosDia = rangeHorarios.get(keyHora);
				if(horariosDia != null){
					horariosDia.add(horario);
				}else{
					horariosDia = new ArrayList<SimpleKVGenericDTO<String,String,Boolean,Long, Boolean>>();
					horariosDia.add(horario);
					rangeHorarios.put(keyHora, horariosDia);
				}
				
			}
			
		}
		
		
		
		return rangeHorarios;
		
		
		
		
	}
	

}
