package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Cidade;

@Stateless
public class CidadeService {

	@Inject
	private Logger logger;

	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;
	
	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;

	public Cidade getCidade(Long id) {
		
		//TODO Need implements
		return null;
	}

	public List<Cidade> getAllCidades(){
		
		logger.debug("Obtendo lista de cidades...");

		return cidadeDAO.getAllCidades();
	}
	
	public List<Bairro> getBairrosPorCidade(Long idCidade){
		
		logger.debug("Obtendo lista de bairros...");

		return bairroDAO.getBairrosPorCidade(idCidade);
	}	

	public Cidade saveCidade(Cidade cidade) {
		
		cidadeDAO.persist(cidade);

		return cidade;

	}

	public Cidade getCidadeByNm(String nmCidade, String nmUf) {
		return cidadeDAO.getCidadeByNmCidadeAndUf(nmCidade, nmUf);
	}

}
