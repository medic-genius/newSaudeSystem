package br.com.medic.medicsystem.main.webservice.superlogica;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParamConfig;
import org.json.JSONObject;

import br.com.medic.dashboard.main.enums.superlogica.Method;
import br.com.medic.medicsystem.main.util.superlogica.ConnectionFactorySuperLogica;
import br.com.medic.medicsystem.persistence.model.superlogica.ClienteSP;


@Stateless
public class WebServiceCliente {
	
	
	String request = "https://api.superlogica.net/v2/financeiro";
	
	
	
	
	
	public JSONObject cadastrarCliente(ClienteSP cliente){
		
		String line = "";
		List<String> response = new ArrayList<String>();
		HttpURLConnection connection = null;
		final String complementoURL = "/clientes";
		
		
		try {
			String urlParameters = "ST_NOME_SAC="+cliente.getSt_nome_sac()
									+"&ST_NOMEREF_SAC="+cliente.getSt_nomeref_sac()
									+"&ST_DIAVENCIMENTO_SAC="+cliente.getSt_diavencimento_sac()
									+"&ST_CGC_SAC="+cliente.getSt_cgc_sac()
									+"&ST_RG_SAC="+cliente.getSt_rg_sac()
									+"&ST_EMAIL_SAC="+cliente.getSt_email_sac()
									+"&ST_CEP_SAC="+cliente.getSt_cep_sac()
									+"&ST_ENDERECO_SAC="+cliente.getSt_endereco_sac()
									+"&ST_NUMERO_SAC="+cliente.getSt_numero_sac()
									+"&ST_BAIRRO_SAC="+cliente.getSt_bairro_sac()
									+"&ST_COMPLEMENTO_SAC="+cliente.getSt_complemento_sac()
									+"&ST_CIDADE_SAC="+cliente.getSt_cidade_sac()
									+"&ST_ESTADO_SAC="+cliente.getSt_estado_sac()
									+"&FL_MESMOEND_SAC="+cliente.getFl_mesmoend_sac()
									+"&identificador="+cliente.getSt_CodCliente();
			

									
									
									
			URL url = new URL(request+complementoURL);
			connection = ConnectionFactorySuperLogica.getConnection(url,urlParameters,Method.POST);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			while ((line = reader.readLine()) != null) {
			    response.add(line);
			}
			reader.close();
		} catch (Exception e) {
			System.out.println("Erro ao fazer a requisição");
			//return null;
		} finally{
			connection.disconnect();

		}
		
		JSONObject json = new JSONObject(response.get(0).substring(1, response.get(0).length()-1));
		json.put("in_CodCliente", cliente.getSt_CodCliente());
		json.put("in_idCliente", cliente.getIn_idCliente());
		json.put("st_nome_sac", cliente.getSt_nome_sac());
		json.put("idContrato", cliente.getIdContrato());
		System.out.println(json);
		return json;
		
		
	}
	
	
	

}
