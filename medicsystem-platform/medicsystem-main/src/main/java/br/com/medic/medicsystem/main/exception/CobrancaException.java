package br.com.medic.medicsystem.main.exception;

import br.com.medic.medicsystem.main.mapper.ErrorType;

public class CobrancaException extends GeneralException {


	private static final long serialVersionUID = 1L;

	public CobrancaException(String message, ErrorType errorType) {
		super(message, errorType);
	}

}
