package br.com.medic.dashboard.main.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import br.com.medic.dashboard.persistence.model.GraphWrapper;

public class Report<T extends List<? extends GraphWrapper>> {

	private String id;

	private String name;

	private T metadata;

	private List<GraphData> graphData;

	public Report() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T getMetadata() {
		return metadata;
	}

	public void setMetadata(T metadata) {
		this.metadata = metadata;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<GraphData> getGraphData() {
		return graphData;
	}

	public void setGraphData(List<GraphData> graphData) {
		this.graphData = graphData;
	}

	public void wrapGraphData() {

		graphData = new ArrayList<GraphData>();

		metadata.forEach(data -> graphData.add(new GraphData(data
		        .getGraphLabel(), data.getGraphValue())));

	}
}
