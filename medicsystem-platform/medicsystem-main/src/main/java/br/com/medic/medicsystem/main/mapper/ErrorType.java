package br.com.medic.medicsystem.main.mapper;

public enum ErrorType {
	
	WARNING,
	ERROR,
	DANGER

}
