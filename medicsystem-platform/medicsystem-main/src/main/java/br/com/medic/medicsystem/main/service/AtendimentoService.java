package br.com.medic.medicsystem.main.service;

import java.util.Locale;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dto.TriagemDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedico;

@Stateless
public class AtendimentoService {
	
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("atendimento-dao")
	private AtendimentoDAO atendimentoDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
	
	@Inject
	private Logger logger;
	
	
	public Atendimento getAtendimento(Long id) {

		logger.debug("Obtendo um Atendimento...");

		return agendamentoDAO.searchByKey(Atendimento.class, id);
	}
	
	
	public boolean saveAtendimento(Long idAgendamento, Atendimento atendimento){
			boolean atendimentoAtualizado = false;
			Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
			
			try {
				
				agendamentoOriginal.setBoSolicitouRetorno(atendimento.getBoSolicitouRetorno());
				agendamentoDAO.update(agendamentoOriginal);
				
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao atualizar agendamento");
			}
				
				
				
				
			atendimento.setAgendamento(agendamentoOriginal);
			try {
				atendimentoDAO.persist(atendimento);
				atendimentoAtualizado = true;
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao salvar o atendimento");
			}
			
			return atendimentoAtualizado;
			
		
	}
	
	
	public AtendimentoMedico getAtendimentoPorIdAgendamento(Long idAgendamento){
		 AtendimentoMedico atendimentoMedico = new AtendimentoMedico();
		 Atendimento atendimento = atendimentoDAO.getAtendimentoPorIdAgendamento(idAgendamento);
		 if(atendimento != null){
			 atendimentoMedico.setIdAtendimento(atendimento.getId());
			 atendimentoMedico.setNmCondutaMedica(atendimento.getNmCondutaMedica());
			 atendimentoMedico.setNmDiagnostico(atendimento.getNmDiagnostico());
			 atendimentoMedico.setNmQueixaPaciente(atendimento.getNmQueixaPaciente());
			 atendimentoMedico.setBoSolicitouRetorno(atendimento.getBoSolicitouRetorno());
			 atendimentoMedico.setDtAtendimento(atendimento.getDtAtendimento());
			 atendimentoMedico.setIdAgendamento(atendimento.getAgendamento().getId());
			 atendimentoMedico.setIdPaciente(atendimento.getAgendamento().getDependente() != null ? 
			 atendimento.getAgendamento().getDependente().getId() : atendimento.getAgendamento().getCliente().getId());
			 atendimentoMedico.setNmMedico(atendimento.getAgendamento().getProfissional().getNmFuncionario());
			 atendimentoMedico.setNmEspecialidade(atendimento.getAgendamento().getEspecialidade().getNmEspecialidade());
			 atendimentoMedico.setNmServico(atendimento.getAgendamento().getServico().getNmServico());
			 atendimentoMedico.setInTipoPaciente(atendimento.getAgendamento().getDependente() != null ? 'D' : 'C');
			 
			 return atendimentoMedico ;
		 }
		 
		 return null;
 
	}
	
	public boolean updateAtendimento(Long idAgendamento, Atendimento atendimento){
		
		boolean atendimentoAtualizado = false;
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
		
		agendamentoOriginal.setBoSolicitouRetorno(atendimento.getBoSolicitouRetorno());
		
		agendamentoDAO.update(agendamentoOriginal);
		
		Atendimento atendimentoOriginal = atendimentoDAO.searchByKey(Atendimento.class, atendimento.getId());
		atendimentoOriginal.setAgendamento(agendamentoOriginal);
		atendimentoOriginal.setNmCondutaMedica(atendimento.getNmCondutaMedica());
		atendimentoOriginal.setNmDiagnostico(atendimento.getNmDiagnostico());
		atendimentoOriginal.setNmQueixaPaciente(atendimento.getNmQueixaPaciente());
		atendimentoOriginal.setDtAtendimento(atendimento.getDtAtendimento());
		atendimentoOriginal.setBoSolicitouRetorno(atendimento.getBoSolicitouRetorno());
		
		try {
			atendimentoDAO.update(atendimentoOriginal);
			atendimentoAtualizado = true;
		} catch (Exception e) {
			throw new ObjectNotFoundException(
					"Erro ao atualizar o atendimento");
		}
		
		return atendimentoAtualizado;
		
		
		
	}
	
	
	public TriagemDTO getDadosTriagemPaciente(Long idPaciente, String inTipoPaciente){
		
		Cliente cliente = new Cliente();
		Dependente dependente = new Dependente();
		TriagemDTO triagemDTO = new TriagemDTO();
		
		if(inTipoPaciente.equals("C")){
		
			cliente = clienteDAO.searchByKey(Cliente.class, idPaciente);
			triagemDTO.setNmPressaoArterial(cliente.getNmPressaoArterial() != null ? cliente.getNmPressaoArterial() : null);
			triagemDTO.setNmPressaoArterialExcelente(cliente.getNmPressaoArterialExcelente() != null ? cliente.getNmPressaoArterialExcelente() : null);
			triagemDTO.setNmTipoSanguineo(cliente.getNmTipoSanguineo() != null ? cliente.getNmTipoSanguineo() : null);
			triagemDTO.setTxGlicose(cliente.getTxGlicose() != null ? cliente.getTxGlicose() : null);
			triagemDTO.setVlAltura(cliente.getVlAltura() != null ? Double.valueOf(String.format(Locale.US, "%.2f", cliente.getVlAltura())) : null);
			triagemDTO.setVlPeso(cliente.getVlPeso() != null ? Double.valueOf(String.format(Locale.US, "%.1f", cliente.getVlPeso())) : null);
			triagemDTO.setVlTemperatura(cliente.getVlTemperatura() != null ? cliente.getVlTemperatura() : null );
		}else{
			
			dependente = dependenteDAO.searchByKey(Dependente.class, idPaciente);
			triagemDTO.setNmPressaoArterial(dependente.getNmPressaoArterial() != null ? dependente.getNmPressaoArterial() : null);
			triagemDTO.setNmPressaoArterialExcelente(dependente.getNmPressaoArterialExcelente() != null ? dependente.getNmPressaoArterialExcelente() : null);
			triagemDTO.setNmTipoSanguineo(dependente.getNmTipoSanguineo() != null ? dependente.getNmTipoSanguineo() : null);
			triagemDTO.setTxGlicose(dependente.getTxGlicose() != null ? dependente.getTxGlicose() : null);
			triagemDTO.setVlAltura(dependente.getVlAltura() != null ? Double.valueOf(String.format(Locale.US, "%.2f", dependente.getVlAltura())) : null);
			triagemDTO.setVlPeso(dependente.getVlPeso() != null ?  Double.valueOf(String.format(Locale.US, "%.1f", dependente.getVlPeso())) : null);
			triagemDTO.setVlTemperatura(cliente.getVlTemperatura() != null ? dependente.getVlTemperatura() : null );
			
		}
		
		return triagemDTO;
		
		
	}
	
	public boolean updateTriagem(Long idPaciente, String inTipoPaciente, TriagemDTO triagemDTO, Long idAgendamento){
		
		Cliente cliente = new Cliente();
		Dependente dependente = new Dependente();
		
		boolean atualizado = false;
		
		try {
		
				Agendamento agendamentoOriginal = agendamentoDAO.searchByKey(Agendamento.class, idAgendamento);
				
				if(inTipoPaciente.equals("C")){
				
					cliente = clienteDAO.searchByKey(Cliente.class, idPaciente);
					cliente.setNmPressaoArterial(triagemDTO.getNmPressaoArterial() != null ? triagemDTO.getNmPressaoArterial() : null);
					cliente.setNmPressaoArterialExcelente(triagemDTO.getNmPressaoArterialExcelente() != null ? triagemDTO.getNmPressaoArterialExcelente() : null);
					cliente.setNmTipoSanguineo(triagemDTO.getNmTipoSanguineo() != null ? triagemDTO.getNmTipoSanguineo() : null);
					cliente.setTxGlicose(triagemDTO.getTxGlicose() != null ? triagemDTO.getTxGlicose() : null);
					cliente.setVlAltura(triagemDTO.getVlAltura() != null ? Double.valueOf(String.format(Locale.US, "%.2f", triagemDTO.getVlAltura())) : null);
					cliente.setVlPeso(triagemDTO.getVlPeso() != null ? Double.valueOf(String.format(Locale.US, "%.1f", triagemDTO.getVlPeso())) : null);
					cliente.setVlTemperatura(triagemDTO.getVlTemperatura() != null ? triagemDTO.getVlTemperatura() : null );
					
					clienteDAO.update(cliente);
					//agendamentoOriginal.setBoStatusTriagem(new Boolean(true));
					agendamentoDAO.update(agendamentoOriginal);
					atualizado = true;
				}else{
					
					dependente = dependenteDAO.searchByKey(Dependente.class, idPaciente);
					dependente.setNmPressaoArterial(triagemDTO.getNmPressaoArterial() != null ? triagemDTO.getNmPressaoArterial() : null);
					dependente.setNmPressaoArterialExcelente(triagemDTO.getNmPressaoArterialExcelente() != null ? triagemDTO.getNmPressaoArterialExcelente() : null);
					dependente.setNmTipoSanguineo(triagemDTO.getNmTipoSanguineo() != null ? triagemDTO.getNmTipoSanguineo() : null);
					dependente.setTxGlicose(triagemDTO.getTxGlicose() != null ? triagemDTO.getTxGlicose() : null);
					dependente.setVlAltura(triagemDTO.getVlAltura() != null ? triagemDTO.getVlAltura() : null);
					dependente.setVlPeso(triagemDTO.getVlPeso() != null ? triagemDTO.getVlPeso() : null);
					dependente.setVlTemperatura(triagemDTO.getVlTemperatura() != null ? triagemDTO.getVlTemperatura() : null );
					
					dependenteDAO.update(dependente);
					//agendamentoOriginal.setBoStatusTriagem(new Boolean(true));
					agendamentoDAO.update(agendamentoOriginal);
					atualizado = true;
				}
				
			
		} catch (Exception e) {
			System.out.println(e);
			atualizado = false;
		}
	
		
		return atualizado;	
	}
	
	public Boolean checkFinalizacaoAtendimento(Long idAgendamento) {
		return atendimentoDAO.checkFinalizacaoAtendimento(idAgendamento);
	}
}
