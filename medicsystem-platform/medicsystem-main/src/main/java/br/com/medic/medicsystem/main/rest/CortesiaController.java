package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.CortesiaService;
import br.com.medic.medicsystem.persistence.model.Cortesia;
import br.com.medic.medicsystem.persistence.utils.DateUtil;


@Path("/cortesias")
@RequestScoped
public class CortesiaController {
	
	@Inject
	CortesiaService cortesiaService;
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Cortesia> getAllCortesia(
			@QueryParam("instatuscortesia") Integer inStatusCortesia,
			@QueryParam("boutilizada") Boolean boUtilizada,
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim) {

		return cortesiaService.getAllCortesia(inStatusCortesia, boUtilizada,
				DateUtil.parseDate(dtInicio, DateUtil.DatePattern.DDMMAA.getPattern()),
				DateUtil.parseDate(dtFim, DateUtil.DatePattern.DDMMAA.getPattern()));

	}
	
	@PUT
	@Path("/liberarcortesia")
	@Produces(MediaType.APPLICATION_JSON)
	public Cortesia updateLiberarCortesia(Cortesia cortesia) {

		
		return cortesiaService.updateLiberarCortesia(cortesia);

	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/updatestatuscortesia/{instatuscortesia}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cortesia updateStatusCortesia(
			@PathParam("id")Long idCortesia,
			@PathParam("instatuscortesia")Integer inStatusCortesia) {

		if(idCortesia != null && inStatusCortesia != null){
			
			return cortesiaService.updateStatusCortesia(idCortesia, inStatusCortesia);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		

	}


}
