package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.DespesaService;

@Path("/parcelas")
@RequestScoped
public class ParcelaController extends BaseController {

	@Inject
	private DespesaService despesaService;

	@PUT
	@Path("/{id:[0-9][0-9]*}/quitar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response quitarParcela(@PathParam("id") Long idParcela, String motivoQuitacao) {

		despesaService.quitarParcela(idParcela, motivoQuitacao);
		return Response.ok().build();

	}

	@PUT
	@Path("/{id:[0-9][0-9]*}/desquitar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response desquitarParcela(@PathParam("id") Long idParcela) {

		despesaService.desquitarParcela(idParcela);
		return Response.ok().build();

	}

}