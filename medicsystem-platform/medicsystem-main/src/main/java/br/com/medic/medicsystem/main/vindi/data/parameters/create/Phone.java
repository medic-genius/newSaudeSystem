package br.com.medic.medicsystem.main.vindi.data.parameters.create;

public class Phone {
	private String phone_type;
	private String number;
	
	public String getPhone_type() {
		return phone_type;
	}
	public void setPhone_type(String phone_type) {
		this.phone_type = phone_type;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
