package br.com.medic.medicsystem.main.exception;

import javax.ejb.ApplicationException;

import br.com.medic.medicsystem.main.mapper.ErrorType;

@ApplicationException(rollback = true)
public class AgendamentoException extends GeneralException {

	public AgendamentoException(String message, ErrorType errorType) {
		super(message, errorType);
	}
	
	public AgendamentoException(String message, ErrorType errorType, Throwable e) {
		super(message, errorType, e);
	}

	private static final long serialVersionUID = 2752602977746710855L;

}
