package br.com.medic.medicsystem.main.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.exception.AgendamentoException;
import br.com.medic.medicsystem.main.exception.DespesaException;
import br.com.medic.medicsystem.main.jasper.JRDataSourceDespesaCliente;
import br.com.medic.medicsystem.main.jasper.JRDataSourceDespesaOrcamento;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaCliente;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaEncaminhamento;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaOrcamento;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaParcela;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaServico;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioEncaminhamentoContinuo;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.service.pjbank.BoletoServicePJB;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.CarenciaDAO;
import br.com.medic.medicsystem.persistence.dao.ConferenciaDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoClienteDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDependenteDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaGuiaServicoDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaServicoDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaClienteDAO;
import br.com.medic.medicsystem.persistence.dao.EncaminhamentoDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.LogGerenteDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ParcelaDAO;
import br.com.medic.medicsystem.persistence.dao.ProcedimentoDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dao.TituloDaDAO;
import br.com.medic.medicsystem.persistence.dto.DespesaDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaViewDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;
import br.com.medic.medicsystem.persistence.model.Carencia;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Conferencia;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Despesa;
import br.com.medic.medicsystem.persistence.model.DespesaGuiaServico;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Encaminhamento;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.LogGerente;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Negociacao;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.Procedimento;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.TituloDa;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.enums.ParcelaPagamentoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusGuiaServicoEnum;
import br.com.medic.medicsystem.persistence.model.enums.StatusPagamentoDespesa;
import br.com.medic.medicsystem.persistence.model.views.ClienteDependenteConvenioView;
import br.com.medic.medicsystem.persistence.model.views.DespesaConvenioView;
import br.com.medic.medicsystem.persistence.security.KeycloakHelper;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class DespesaService {

	@Inject
	private Logger logger;

	@Inject
	private ClienteService clienteService;

	@Inject
	@Named("despesa-dao")
	private DespesaDAO despesaDAO;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;

	@Inject
	@Named("encaminhamento-dao")
	private EncaminhamentoDAO encaminhamentoDAO;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
	@Inject
	@Named("contratodependente-dao")
	private ContratoDependenteDAO contratoDependenteDAO;

	@Inject
	@Named("parcela-dao")
	private ParcelaDAO parcelaDAO;

	@Inject
	@Named("procedimento-dao")
	private ProcedimentoDAO procedimentoDAO;

	@Inject
	@Named("carencia-dao")
	private CarenciaDAO carenciaDAO;

	@Inject
	@Named("tituloda-dao")
	private TituloDaDAO tituloDaDAO;

	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeDAO;

	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("despesaguiaservico-dao")
	private DespesaGuiaServicoDAO despesaGuiaServicoDAO;

	@Inject
	@Named("log-gerente-dao")
	private LogGerenteDAO logGerenteDAO;
	
	@Inject
	@Named("empresaCliente-dao")
	private EmpresaClienteDAO empresaClienteDAO;
	
	@Inject
	@Named("conferencia-dao")
	private ConferenciaDAO conferenciaDAO;
	
	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;
	
	@Inject
	@Named("despesaservico-dao")
	private DespesaServicoDAO despesaServicoDAO;
	
	@Inject
	@Named("contratocliente-dao")
	private ContratoClienteDAO contratoClienteDAO;

	@Inject
	private SecurityService securityService;

	@Inject
	private KeycloakHelper khelper;
	
	@Inject
	private GuiaService guiaService;
	
	@Inject
	private BoletoServicePJB boletoServicePJB;
	
	

	public String gerarCodigoDespesa() {

		logger.debug("Gerando novo codigo de despesa");

		Long novoCodigo = null;

		try {
			novoCodigo = despesaDAO.getNovoCodigoDespesa();
		} catch (ObjectNotFoundException e) {
			return "000001";
		}

		++novoCodigo;
		String nrNovoSeq = novoCodigo.toString();

		// Preenche o sequencial para o formato de 4 digitos
		while (nrNovoSeq.trim().length() < 6) {
			nrNovoSeq = "0".concat(nrNovoSeq);
		}

		return nrNovoSeq;

	}
	
//	public String gerarCodigoDespesa(EmpresaGrupoCompartilhado empresa) {
//
//		logger.debug("Gerando novo codigo de despesa");
//
//		Long novoCodigo = null;
//
//		try {
//			if(empresa.getInSistemaExterno() == 0){
//				novoCodigo = despesaDAO.getNovoCodigoDespesa();
//			}else if(empresa.getInSistemaExterno() == 1){
//				novoCodigo = despesaPerformanceDAO.getNovoCodigoDespesa();
//			}else if(empresa.getInSistemaExterno() == 2){
//				novoCodigo = despesaDentalDAO.getNovoCodigoDespesa();
//			}
//			
//		} catch (ObjectNotFoundException e) {
//			return "000001";
//		}
//
//		++novoCodigo;
//		String nrNovoSeq = novoCodigo.toString();
//
//		// Preenche o sequencial para o formato de 4 digitos
//		while (nrNovoSeq.trim().length() < 6) {
//			nrNovoSeq = "0".concat(nrNovoSeq);
//		}
//
//		return nrNovoSeq;
//
//	}

	public DespesaViewDTO getDespesaDTO(Long id) {
		Despesa despesa = despesaDAO.searchByKey(Despesa.class, id);
		if (despesa.getIdOperadorCadastro() != null) {
			despesa.setOperadorCadastro(funcionarioDAO.getFuncionario(despesa
			        .getIdOperadorCadastro()));
		}

		despesa.getDespesaServicos().size();
		despesa.getParcelas().size();

		List<DespesaServico> servicos = despesaDAO.getDespesaServicos(despesa
		        .getId());
		List<Parcela> parcelas = despesaDAO.getDespesaParcelas(despesa.getId());

		return new DespesaViewDTO(despesa, servicos, parcelas);
	}

	public Despesa saveDespesa(Despesa despesa) {

		Date today = new Date();

		if (despesa.getDespesaServicos().size() == 0) {
			throw new AgendamentoException(
			        "Error ao salvar Despesa, nenhum serviço adicionado.",
			        ErrorType.ERROR);
		}

		if (despesa.getVlDespesaAberto() > 0) {
			throw new AgendamentoException(
			        "Despesa com valor em aberto. Fluxo exclusivo para despesas cobertas.",
			        ErrorType.ERROR);
		}

		if (despesa.getContratoDependente() != null
		        && despesa.getContratoDependente().getContrato() != null 
		        && despesa.getContratoDependente().getContrato().getId() != null) {
			despesa.setContratoDependente(contratoDAO
			        .getContratoDependente(despesa.getContratoDependente()
			                .getContrato().getId(), despesa.getContratoDependente().getDependente().getId()));
		}

		if (despesa.getContratoCliente() != null
		        && despesa.getContratoCliente().getContrato().getId() != null) {
			despesa.setContratoCliente(contratoDAO.getContratoCliente(despesa
			        .getContratoCliente().getContrato().getId(), despesa.getContratoCliente().getCliente().getId()));
			if (despesa.getContratoDependente() != null
			        && despesa.getContratoDependente().getId() == null) {
				despesa.setContratoDependente(null);
			}
		}

		// ContratoCliente contratocliente = paciente.getContratoCliente();
		List<DespesaServico> dsList = new ArrayList<DespesaServico>();
		dsList.addAll(despesa.getDespesaServicos());
		despesa.getDespesaServicos().clear();
				
		//Conveniado?
		if(despesa.getContratoDependente() != null)
			if(despesa.getContratoDependente().getContrato().getEmpresaCliente() != null)
				despesa.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
			else			
				despesa.setInFormaPagamento(FormaPagamentoEnum.COBERTURA_DO_PLANO.getId());
		else if(despesa.getContratoCliente() != null)
			if(despesa.getContratoCliente().getContrato().getEmpresaCliente() != null)
				despesa.setInFormaPagamento(FormaPagamentoEnum.CONVENIO.getId());
			else			
				despesa.setInFormaPagamento(FormaPagamentoEnum.COBERTURA_DO_PLANO.getId());
				
		despesa.setInSituacao(StatusPagamentoDespesa.PAGO.getId());
		despesa.setDtDespesa(today);
		despesa.setBoExcluida(false);
		despesa.setBoParcelaDa(false);
		despesa.setUnidade(securityService.getFuncionarioLogado().getUnidade());
		despesa.setNrDespesa(gerarCodigoDespesa());
		despesa = despesaDAO.persist(despesa);

		int encIndex = 0;
		for (DespesaServico despesaServico : dsList) {
			
			Encaminhamento enc = despesaServico.getEncaminhamento();
			enc.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento(++encIndex));
			enc.setDtEncaminhamento(today);
			enc = encaminhamentoDAO.persist(enc);
			
			despesaServico.setEncaminhamento(enc);
			despesaServico.setBoRealizado(false);
			despesaServico.setDespesa(despesa);		
			despesa.addDespesaservico(despesaServico);
			
			if(despesa.getInFormaPagamento().equals(FormaPagamentoEnum.CONVENIO.getId()) 
					&& despesa.getInSituacao().equals(StatusPagamentoDespesa.PAGO.getId())){
				
				GuiaServico guiaServico = guiaService.getGuiaValidabyClienteDependenteServico(despesaServico);
				
				if(guiaServico != null){
					guiaServico.setStatus(StatusGuiaServicoEnum.CONSUMIDO.getId());
					guiaServico.setDtUtilizacao(new Timestamp(new Date().getTime()));
					guiaService.updateGuiaServico(guiaServico);
					
					DespesaGuiaServico despesaGS = new DespesaGuiaServico();
					despesaGS.setDespesa(despesa);
					despesaGS.setGuiaServico(guiaServico);
					despesaGS.setDtInclusaoLog(new Timestamp(new Date().getTime()));
					despesaGuiaServicoDAO.persist(despesaGS);
				}else
					throw new AgendamentoException(
					        "O serviço " + despesaServico.getServico().getNmServico() + " não possui guia para cobertura.", ErrorType.ERROR);					
			}			
			
		}

		despesa = despesaDAO.persist(despesa);

		// Criar as parcelas
		Parcela parcelaDespesa = new Parcela();
		parcelaDespesa.setDespesa(despesa);
		parcelaDespesa.setDtInclusao(today);
		parcelaDespesa.setDtVencimento(today);
		parcelaDespesa.setInFormaPagamento(despesa.getInFormaPagamento());
		parcelaDespesa.setInFormaPagamento(despesa.getInFormaPagamento());
		parcelaDespesa.setNrParcela(despesa.getNrDespesa().concat("/01"));
		parcelaDespesa.setVlParcela(despesa.getVlDespesaTotal());
		parcelaDespesa.setBoAutorizadaGerente(false);
		parcelaDespesa.setBoNegociacaoAutomatica(false);

		if (despesa.getInFormaPagamento() != null) {
			parcelaDespesa.setDtPagamento(today);
			parcelaDespesa.setVlPago(despesa.getVlDespesaAberto());
			parcelaDespesa.setVlJuros(0D);
			parcelaDespesa.setVlMulta(0D);
			if (despesa.getInFormaPagamento() == FormaPagamentoEnum.GRATUITO.getId()) 
				parcelaDespesa.setFormaPagamentoEfetuada(FormaPagamentoEnum.GRATUITO.getDescription());
			else if (despesa.getInFormaPagamento() == FormaPagamentoEnum.COBERTURA_DO_PLANO.getId())
				parcelaDespesa.setFormaPagamentoEfetuada(FormaPagamentoEnum.COBERTURA_DO_PLANO.getDescription());
			else if (despesa.getInFormaPagamento() == FormaPagamentoEnum.CONVENIO.getId())
				parcelaDespesa.setFormaPagamentoEfetuada(FormaPagamentoEnum.CONVENIO.getDescription());
			

		}

		parcelaDAO.persist(parcelaDespesa);

		Contrato contratoaux = null;

		if (despesa.getContratoCliente().getContrato().getEmpresaCliente() != null) {
			contratoaux = contratoDAO.getContratoAtivoEmpresaCliente(despesa
			        .getContratoCliente().getContrato().getEmpresaCliente()
			        .getId());
		} else {
			contratoaux = despesa.getContratoCliente().getContrato();
		}

		for (DespesaServico item : despesa.getDespesaServicos()) {
			if (item.getProcedimento() != null && item.getInCoberto() != null
			        && item.getInCoberto()) {
				item.getProcedimento().setBoAutorizado(true); // Autorizado
				item.getProcedimento().setInSituacao(1); // Lancado

				procedimentoDAO.update(item.getProcedimento());

			}

			List<Carencia> listaCarencia = carenciaDAO.getCarencia(
			        contratoaux.getId(), item.getServico().getId());

			if (listaCarencia != null && listaCarencia.size() > 0) {
				for (Carencia c : listaCarencia) {
					c.setTotalConsulta(c.getTotalConsulta() + 1);
					carenciaDAO.update(c);
				}
			}

		}

		return despesa;

	}

	public byte[] generateDespesaPDF(Long idDespesa) {

		Despesa despesa = despesaDAO.searchByKey(Despesa.class, idDespesa);

		List<RelatorioDespesaCliente> despesas = new ArrayList<RelatorioDespesaCliente>();

		List<RelatorioDespesaServico> despesaServico = new ArrayList<RelatorioDespesaServico>();
		List<RelatorioDespesaParcela> despesaParcela = new ArrayList<RelatorioDespesaParcela>();
		List<RelatorioDespesaEncaminhamento> despesaEncaminhamento = new ArrayList<RelatorioDespesaEncaminhamento>();

		RelatorioDespesaServico rds;
		RelatorioDespesaEncaminhamento rde;

		List<DespesaServico> servicos = despesaDAO.getDespesaServicos(despesa
		        .getId());

		for (DespesaServico ds : servicos) {
			rds = new RelatorioDespesaServico(ds.getServico(),
			        ds.getQtServico(), ds.getVlServico());
			despesaServico.add(rds);

			if (ds.getEncaminhamento() != null) {
				rde = new RelatorioDespesaEncaminhamento(ds.getEncaminhamento());
				despesaEncaminhamento.add(rde);
			}
		}

		List<Parcela> parcelas = despesaDAO.getDespesaParcelas(despesa.getId());

		int i = 0;
		for (Parcela parcela : parcelas) {
			despesaParcela.add(new RelatorioDespesaParcela(parcela, ++i));
		}

		RelatorioDespesaCliente rDespesaCliente = new RelatorioDespesaCliente(
		        despesa, despesaServico, despesaParcela, despesaEncaminhamento);

		despesas.add(rDespesaCliente);

		ByteArrayOutputStream output = null;

		byte[] pdf = null;

		try {

			InputStream inputStream = getClass().getResourceAsStream(
			        "/jasper/DespesaCliente.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
			        .compileReport(design);

			JRDataSourceDespesaCliente despesaRelatorio = new JRDataSourceDespesaCliente(
			        despesas);

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logomarca", "");

			JasperPrint jasperPrint = JasperFillManager.fillReport(
			        jasperReport, params, despesaRelatorio);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);

			pdf = output.toByteArray();

		} catch (JRException e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Despesas do Segurado",
			        ErrorType.ERROR, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e) {
				throw new AgendamentoException(
				        "Nao foi possivel gerar o relatorio em PDF - Despesas do Segurado",
				        ErrorType.ERROR, e);
			}

		}

		return pdf;
	}

	public byte[] generateEncaminhamentoDespesaPDF(Long idDespesa, String path) {

		List<DespesaServico> servicos = despesaDAO
		        .getDespesaServicos(idDespesa);

		RelatorioEncaminhamentoContinuo relatorioEncaminhamento = new RelatorioEncaminhamentoContinuo(
		        servicos, securityService.getFuncionarioLogado()
		                .getNmFuncionario());

		ByteArrayOutputStream output = null;
		byte[] pdf = null;

		try {
			
			InputStream inputStream = getClass().getResourceAsStream(
			        "/jasper/RelatorioEncaminhamentoA4.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
			        .compileReport(design);

			HashMap<String, Object> params = new HashMap<String, Object>();
			
			InputStream caminhoImagemlogo = getClass().getResourceAsStream("/jasper/imagensJasper/LOGO-DR+CONSULTA.png");
			InputStream caminhoImagemlinha = getClass().getResourceAsStream("/jasper/imagensJasper/linha1.png");
			InputStream caminhoImagemlinha2 = getClass().getResourceAsStream("/jasper/imagensJasper/linha1.png");
			InputStream caminhoImagemrodape = getClass().getResourceAsStream("/jasper/imagensJasper/rodape1.png");
			params.put("logomarca", caminhoImagemlogo);
			params.put("linha", caminhoImagemlinha);
			params.put("linha2", caminhoImagemlinha2);
			params.put("rodape", caminhoImagemrodape);
			

			JasperPrint jasperPrint = JasperFillManager.fillReport(
			        jasperReport, params, relatorioEncaminhamento);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);

			pdf = output.toByteArray();

		} catch (JRException e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Encaminhamento",
			        ErrorType.ERROR, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e) {
				throw new AgendamentoException(
				        "Nao foi possivel gerar o relatorio em PDF - Encaminhamento",
				        ErrorType.ERROR, e);
			}

		}
		return pdf;

	}

	public Boolean isAutorizacaoGerenteRequired(DespesaDTO despesaDTO) {

		Cliente cliente = despesaDTO.getDespesa().getContratoCliente()
		        .getCliente();

		if (cliente.getVlLimite() != null
		        && cliente.getVlLimite() > 0
		        && (despesaDTO.getBoAutorizadoGerente() == null || !despesaDTO
		                .getBoAutorizadoGerente())) {

			if (despesaDTO.getInFormaPagamento().equals(17)
			        || despesaDTO.getInFormaPagamento().equals(15)
			        || despesaDTO.getInFormaPagamento().equals(1)) {

				if (((despesaDTO.getVlLanctoValorDespesa() + despesaDTO
				        .getVlLanctoDebitoUsado()) > despesaDTO
				        .getVlLanctoLimite())
				        && !despesaDTO.getBoAutorizadoGerente()) {

					return true;
				}
			}
		}

		return false;

	}

	public Despesa saveLancamentoDespesa(DespesaDTO despesaDTO) {

		Date today = new Date();
		Despesa despesa = despesaDTO.getDespesa();

		despesa.setInSituacao(0);
		
		ContratoDependente contdep = new ContratoDependente();
		if(despesa.getContratoDependente().getDependente() != null && despesa.getContratoDependente().getContrato().getId() == null ){
			contdep = contratoDependenteDAO.getContratoDependenteById(despesa.getContratoDependente().getDependente().getId(), despesa.getContratoCliente().getContrato().getId());			
			despesa.setContratoDependente(contdep);
		}else{	
			if (despesa.getContratoDependente() != null
			        && despesa.getContratoDependente().getContrato() != null 
			        && despesa.getContratoDependente().getContrato().getId() != null) {
				despesa.setContratoDependente(contratoDAO
				        .getContratoDependente(despesa.getContratoDependente()
				                .getContrato().getId(), despesa.getContratoDependente().getDependente().getId()));
			}
		}

		if (despesa.getContratoCliente() != null
		        && despesa.getContratoCliente().getContrato().getId() != null) {
			despesa.setContratoCliente(contratoDAO.getContratoCliente(despesa
			        .getContratoCliente().getContrato().getId(), despesa.getContratoCliente().getCliente().getId()));
			if (despesa.getContratoDependente() != null
			        && despesa.getContratoDependente().getId() == null) {
				despesa.setContratoDependente(null);
			}
		}

		despesa.setDtDespesa(today);
		despesa.setBoExcluida(false);
		despesa.setBoParcelaDa(false);

		List<DespesaServico> ds = new ArrayList<DespesaServico>();
		ds.addAll(despesa.getDespesaServicos());
		despesa.getDespesaServicos().clear();

		int encIndex = 0;
		for (DespesaServico despesaServico : ds) {
			despesaServico.setDespesa(despesa);
			despesaServico.setBoRealizado(false);
			despesaServico.setDtInclusaoLog(new Timestamp(new Date().getTime()));

			Encaminhamento encaminhamento = despesaServico.getEncaminhamento();

			if (encaminhamento != null && encaminhamento.getId() == null) {

				encaminhamento.setDtEncaminhamento(today);
				encaminhamento.setNrEncaminhamento(encaminhamentoDAO.geraCodigoEncaminhamento(++encIndex));
				encaminhamento = encaminhamentoDAO.persist(encaminhamento);
				despesaServico.setEncaminhamento(encaminhamento);
			}

			despesa.addDespesaservico(despesaServico);
		}

		despesa.setNrDespesa(gerarCodigoDespesa());
		despesa.setUnidade(securityService.getFuncionarioLogado().getUnidade());

		Parcela parcelaDespesa = new Parcela();

		parcelaDespesa.setDtInclusao(today);
		parcelaDespesa.setDtVencimento(despesaDTO.getDtVencimento());
		parcelaDespesa.setInFormaPagamento(despesaDTO.getInFormaPagamento());
		parcelaDespesa.setNrParcela(despesa.getNrDespesa().concat("/01"));
		parcelaDespesa.setVlParcela(despesaDTO.getVlLanctoValorDespesa());
		parcelaDespesa.setBoAutorizadaGerente(despesaDTO
		        .getBoAutorizadoGerente());
		parcelaDespesa.setBoNegociacaoAutomatica(false);
		parcelaDespesa.setDtInclusao(today);
		despesa.addParcela(parcelaDespesa);

		despesa.setInFormaPagamento(despesaDTO.getInFormaPagamento());
		despesa = despesaDAO.persist(despesa);// Atualiza despesa

		return despesa;
	}

	public void removeDespesa(Long idDespesa, String motivo) {

		Properties props = PropertiesLoader.getInstance().load(
		        "message.properties");

		Despesa despesa = despesaDAO.getDespesa(idDespesa);

		boolean boAutorizarExcluir = false;

		/* Verifica se a despesa esta ligada a alguma conferencia */
		List<DespesaServico> despesaServicoList;
		try {
			despesaServicoList = despesaDAO
			        .getDespesaServicosConnectedToConferencia(idDespesa);

			for (DespesaServico despesaServico : despesaServicoList) {

				if (despesaServico.getConferencia().getDtFechamento() != null) {

					khelper.initialize("keycloak-secondauth.json");

					boolean hasRole = khelper.hasRoles(
					        securityService.getUserId(),
					        securityService.getUserToken(), "administrador",
					        "gerenteadm", "faturamento");

					if (hasRole) {
						boAutorizarExcluir = true;

					} else {
						throw new DespesaException(
						        props.getProperty("despesa.sempermissao"),
						        ErrorType.DANGER);
					}
				}

			}

		} catch (ObjectNotFoundException e) {
			boAutorizarExcluir = true;
		}

		if (!boAutorizarExcluir) {
			throw new DespesaException(props.getProperty("despesa.naodeleta"),
			        ErrorType.DANGER);
		}

		List<Negociacao> listNegociacao;
		try {
			listNegociacao = despesaDAO.getNegociacao(idDespesa);
		} catch (ObjectNotFoundException e1) {
			listNegociacao = null;
		}

		if (listNegociacao != null) {

			for (Negociacao neg : listNegociacao) {

				if (neg.getMensalidade() != null) {

					Mensalidade mensalidade = neg.getMensalidade();
					mensalidade.setBoletoNegociada(false);

					mensalidadeDAO.update(mensalidade);
				}
			}
		}

		despesa.setBoExcluida(true);
		List<Parcela> parcelas = new ArrayList<Parcela>();
		parcelas.addAll(despesa.getParcelas());
		despesa.getParcelas().clear();
		for (Parcela parcela : parcelas) {
			parcela.setBoExcluida(true);

			if (parcela.getBoNegociada() != null
			        && parcela.getBoNegociada() == true) {
				parcela.setBoNegociada(false); // alteracao

			}

			if (parcela.getInFormaPagamento() == 1) {

				List<TituloDa> titulos = parcelaDAO.getTitulos(parcela.getId());

				for (TituloDa titulo : titulos) {
					titulo.setInExcluido(true);
					tituloDaDAO.update(titulo);
				}
			}

			despesa.addParcela(parcela);

		}

		despesaDAO.update(despesa);

		if (despesa.getAgendamento() != null) {
			Agendamento ag = agendamentoDAO.getAgendamento(despesa
			        .getAgendamento().getId());
			ag.setBoDespesaGerada(false);
			ag.setInStatus(0);
			agendamentoDAO.update(ag);
		}
		
		//Desvincula a guia da despesa
		List<DespesaGuiaServico> listDespesaGuiaServico = despesaGuiaServicoDAO.getDespesaGuiaServicobyDespesa(despesa.getId());
		
		if(listDespesaGuiaServico != null && listDespesaGuiaServico.size() > 0){			
			for(DespesaGuiaServico dgs : listDespesaGuiaServico){
				if(dgs.getDtExclusao() == null){
					
					dgs.setDtExclusao(new Timestamp(new Date().getTime()));
					dgs.getGuiaServico().setStatus(StatusGuiaServicoEnum.CRIADO.getId());						
					despesaGuiaServicoDAO.update(dgs);
				}				
			}			
		}	
		

		// log gerente
		LogGerente log = new LogGerente();
		log.setAcao(3);
		Funcionario func = securityService.getFuncionarioLogado();
		if (func != null) {
			log.setIdFuncionario(func.getId());
		}
		log.setIdCliente(despesa.getContratoCliente().getCliente().getId());
		log.setIdContrato(despesa.getContratoCliente().getContrato().getId());
		log.setIdDocumento(despesa.getId());
		log.setInSituacao(despesa.getInSituacao());
		log.setIdPlano(despesa.getContratoCliente() != null
		        && despesa.getContratoCliente().getPlano() != null ? despesa
		        .getContratoCliente().getPlano().getId() : null);
		log.setDtAcao(new Date());
		log.setObservacao(motivo);

		logGerenteDAO.saveLogGerente(log);

	}

	public void quitarParcela(Long idParcela, String motivoQuitacao) {

		Parcela parcela = parcelaDAO.getParcela(idParcela);

		Despesa despesa = parcela.getDespesa();

		// quita parcela
		parcela.setDtPagamento(new Date());
		parcela.setVlPago(parcela.getVlParcela());
		parcela.setFormaPagamentoEfetuada(FormaPagamentoEnum.BAIXA_NO_SISTEMA
		        .getDescription());
		parcela.setInFormaPagamento(14);

		parcelaDAO.update(parcela);

		// salvar no log gerente o ato de quitar despesa.
		LogGerente log = new LogGerente();
		log.setAcao(5);
		Funcionario func = securityService.getFuncionarioLogado();
		if (func != null) {
			log.setIdFuncionario(func.getId());
		}
		log.setIdCliente(despesa.getContratoCliente().getCliente().getId());
		log.setIdContrato(despesa.getContratoCliente().getContrato().getId());
		log.setIdDocumento(despesa.getId());
		log.setInSituacao(despesa.getInSituacao());
		log.setIdPlano(despesa.getContratoCliente() != null
		        && despesa.getContratoCliente().getPlano() != null ? parcela
		        .getDespesa().getContratoCliente().getPlano().getId() : null);
		log.setDtAcao(new Date());
		log.setObservacao(motivoQuitacao);

		logGerenteDAO.saveLogGerente(log);

		// exclui titulos
		if (parcela.getInFormaPagamento() == ParcelaPagamentoEnum.FORMA_PAGAMENTO_DEBITOAUTOMATICO
		        .getId()) {
			try {
				TituloDa titulo = tituloDaDAO.getTituloPorParcela(parcela
				        .getId());

				titulo.setInExcluido(true);
				tituloDaDAO.update(titulo);
			} catch (NoResultException e) {
				logger.warn("Nao foi encontrado Titulo DA associada a parcela",
				        e);
			}

		}

		// quita despesa
		try {
			List<Parcela> ps = parcelaDAO.getParcelaAbertasPorDespesa(despesa
			        .getId());
			if (ps.isEmpty()) {
				despesa.setInSituacao(1);
				despesa = despesaDAO.update(despesa);
			}
		} catch (ObjectNotFoundException e) {
			despesa.setInSituacao(1);
			despesa = despesaDAO.update(despesa);
		}

		// libera agendamento
		if (despesa.getAgendamento() != null) {
			Agendamento agendamento = agendamentoDAO.getAgendamento(despesa
			        .getAgendamento().getId());
			agendamento.setInStatus(1);
			agendamentoDAO.update(agendamento);
		}

		// lanca procedimento
		despesa.getDespesaServicos().size();
		List<DespesaServico> dsList = despesa.getDespesaServicos();

		for (DespesaServico ds : dsList) {
			if (ds.getProcedimento() != null) {
				Procedimento proc = ds.getProcedimento();
				proc.setInSituacao(1); // Procedimento lancado
				procedimentoDAO.update(proc);
			}
		}

		clienteService.desbloquearCliente(despesa.getContratoCliente()
		        .getCliente().getId());

	}

	public void desquitarParcela(Long idParcela) {

		Parcela parcela = parcelaDAO.getParcela(idParcela);

		Despesa despesa = parcela.getDespesa();

		parcela.setDtPagamento(null);
		parcela.setVlPago(null);
		parcela.setFormaPagamentoEfetuada(null);
		parcela.setInFormaPagamento(despesa.getInFormaPagamento());

		parcelaDAO.update(parcela);

		// de-exclui titulos
		if (parcela.getInFormaPagamento() == ParcelaPagamentoEnum.FORMA_PAGAMENTO_DEBITOAUTOMATICO
		        .getId()) {
			try {
				TituloDa titulo = tituloDaDAO.getTituloPorParcela(parcela
				        .getId());

				titulo.setInExcluido(false);
				tituloDaDAO.update(titulo);
			} catch (NoResultException e) {
				logger.warn("Nao foi encontrado Titulo DA associada a parcela",
				        e);
			}

		}

		// des-quita despesa
		try {
			List<Parcela> ps = parcelaDAO.getParcelaPagasPorDespesa(despesa
			        .getId());
			if (ps.isEmpty()) {
				despesa.setInSituacao(0);
				despesa = despesaDAO.update(despesa);
			}
		} catch (ObjectNotFoundException e) {
			despesa.setInSituacao(0);
			despesa = despesaDAO.update(despesa);
		}

		// bloqueia agendamento
		if (despesa.getAgendamento() != null) {
			Agendamento agendamento = agendamentoDAO.getAgendamento(despesa
			        .getAgendamento().getId());
			if (despesa.getContratoCliente().getContrato()
			        .getBoNaoFazUsoPlano()) {
				agendamento.setInStatus(0);
			} else if (despesa.getContratoCliente().getCliente()
			        .getBoBloqueado()) {
				agendamento.setInStatus(0);
			} else if (despesa.getInFormaPagamento() != 10) {
				agendamento.setInStatus(0);
			} else
				agendamento.setInStatus(1);
			agendamentoDAO.update(agendamento);
		}

		clienteService.bloquearCliente(despesa.getContratoCliente()
		        .getCliente().getId());

	}

	public byte[] generateOrcamentoPDF(Despesa despesa) {
		
		despesa.setUnidade(securityService.getFuncionarioLogado().getUnidade());

		List<RelatorioDespesaOrcamento> despesas = new ArrayList<RelatorioDespesaOrcamento>();

		List<RelatorioDespesaServico> despesaServico = new ArrayList<RelatorioDespesaServico>();

		RelatorioDespesaServico rds;

		List<DespesaServico> servicos = despesa.getDespesaServicos();

		for (DespesaServico ds : servicos) {
			rds = new RelatorioDespesaServico(ds.getServico(),
			        ds.getQtServico(), ds.getVlAberto());
			despesaServico.add(rds);

		}

		RelatorioDespesaOrcamento rDespesaOrcamento = new RelatorioDespesaOrcamento(
		        despesa, despesaServico);

		despesas.add(rDespesaOrcamento);

		ByteArrayOutputStream output = null;

		byte[] pdf = null;

		try {

			InputStream inputStream = getClass().getResourceAsStream(
			        "/jasper/DespesaOrcamento.jrxml");

			JasperDesign design = JRXmlLoader.load(inputStream);

			JasperReport jasperReport = JasperCompileManager
			        .compileReport(design);

			JRDataSourceDespesaOrcamento despesaRelatorio = new JRDataSourceDespesaOrcamento(
			        despesas);

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logomarca", "");

			JasperPrint jasperPrint = JasperFillManager.fillReport(
			        jasperReport, params, despesaRelatorio);

			output = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, output);

			pdf = output.toByteArray();

		} catch (JRException e) {
			throw new AgendamentoException(
			        "Nao foi possivel gerar o relatorio em PDF - Despesas do Segurado",
			        ErrorType.ERROR, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e) {
				throw new AgendamentoException(
				        "Nao foi possivel gerar o relatorio em PDF - Despesas do Segurado",
				        ErrorType.ERROR, e);
			}

		}

		return pdf;
	}

	public Despesa getDespesaByGuiaServico(Long idGuiaServico) {
		DespesaGuiaServico despesaGuiaServico = despesaGuiaServicoDAO.getDespesaGuiaServicoByGuiaServico(idGuiaServico);
		if(null != despesaGuiaServico && null != despesaGuiaServico.getDespesa())
		{
			return despesaGuiaServico.getDespesa();
		}
		return null;
	}
	
	public Boolean reabrirFaturaEmpresaCliente(Long idEmpresaCliente, String periodo){
				 
		String[] mesAnoSplit = periodo.split("/");
		int mes = Integer.valueOf(mesAnoSplit[0]);
		int ano = Integer.valueOf(mesAnoSplit[1]);
		String mesAno = mes + "/" + ano;
		Conferencia conferenciaEC = conferenciaDAO.getConferenciaEmpresaClienteByMesAno(idEmpresaCliente, mesAno);
		
		try{
			
			if(conferenciaEC != null){
				
				conferenciaEC.setDesconto(0.0);
				conferenciaEC.setDtFechamento(null);
				conferenciaEC.setFuncionario1(null);
				conferenciaEC.setFuncionario2(null);
				conferenciaEC.setObservacao(null);
				conferenciaEC.setVlTotalCusto(0.0);
				conferenciaEC.setVlTotalVenda(0.0);
	 			
				conferenciaDAO.update(conferenciaEC);	
				
				//Utilizar para reabrir a fatura, excluindo a mensalidade
				Contrato contrato = contratoDAO.getContratoEmpresaClienteById(idEmpresaCliente);	
				
				//Exclui mensalidades
			    List<Mensalidade> listMensalidade = mensalidadeDAO.getMensalidadeByContratoNrMensalidade(contrato.getId(), mesAno);
			    if(listMensalidade != null && listMensalidade.size() > 0){
				    for(Mensalidade m : listMensalidade){		    	
				    	m.setDataExclusao(new Date());
				    	mensalidadeDAO.update(m);
				    }
			    }
			    else{
			    	//Exclui despesa
				    ContratoCliente cc = contratoClienteDAO.getContratoClienteByContrato(contrato.getId());
				    List<Despesa> listDespesa = despesaDAO.getDespesaByContratoClienteMesAno(cc, mesAno);
				    for(Despesa d : listDespesa){
				    	d.setBoExcluida(true);
				    	for(Parcela p : d.getParcelas()){
				    		p.setBoExcluida(true);
				    	}
				    	
				    	despesaDAO.update(d);
				    }
			    }			    
			    		    		    	
				return true;
				
			}
			else 
				return false;
			
 		}catch( Exception e){
 			e.printStackTrace();
 			
 			return false;
 		}
	}	
	
	public String gerarFaturaEmpresaCliente(Long idEmpresaCliente, String periodo, Double vlDesconto, String obsFatura){
		
		EmpresaCliente empresaCliente = empresaClienteDAO.getEmpresaCliente(idEmpresaCliente);
		
		
		String[] splitMesAno = periodo.split("/");
		String mesFatura = splitMesAno[0];
		String anoFatura = splitMesAno[1];
		String mesAno = Integer.valueOf(mesFatura) + "/" + Integer.valueOf(anoFatura);
		
//		List<BoletoBancario> listbb = new ArrayList<BoletoBancario>(); 
	    Conferencia conferenciaEC = conferenciaDAO.getConferenciaEmpresaClienteByMesAno(empresaCliente.getId(), mesAno);
	    Contrato contrato = contratoDAO.getContratoEmpresaClienteById(empresaCliente.getId());	    
	    BoletoBancario bb = null;
	    
	    if(conferenciaEC == null || (conferenciaEC != null && conferenciaEC.getDtFechamento() == null)){
		
			if(empresaCliente.getInTipoPagamento().equals(1)){//Despesa
				
				try {
					Despesa despesa = gerarDespesaEmpresaCliente(empresaCliente, conferenciaEC, Integer.valueOf(mesFatura), Integer.valueOf(anoFatura), vlDesconto, obsFatura, contrato);
					for(Parcela p : despesa.getParcelas())					
						bb = boletoServicePJB.registrarBoleto(p, empresaCliente);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
						
			}
			else if (empresaCliente.getInTipoPagamento().equals(2)){//Mensalidade				
				
				try {
					Mensalidade mensalidade = gerarMensalidadeEmpresaCliente(empresaCliente, conferenciaEC, Integer.valueOf(mesFatura), Integer.valueOf(anoFatura), vlDesconto, obsFatura, contrato);				
					bb = boletoServicePJB.registrarBoleto(mensalidade, empresaCliente);					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			return bb.getLinkBoleto();
	    }
	    else				
	    	return null;
	}
	
	
	
	private Despesa gerarDespesaEmpresaCliente(EmpresaCliente empresaCliente, Conferencia conferenciaEC, Integer mesFatura, Integer anoFatura, Double vlDesconto, String obsFatura, Contrato contrato){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar c = new GregorianCalendar(anoFatura, mesFatura - 1, 1);
		
		String dtInicio = sdf.format(c.getTime());		
	    int ultimoDia = c.getActualMaximum(Calendar.DATE);
	    c.set(Calendar.DAY_OF_MONTH, ultimoDia);
	    String dtFim = sdf.format(c.getTime());
		
	    String mesAno = mesFatura + "/" + anoFatura;
	    
	    try {
	    	Double totalCobranca = 0.0;
	    	
	    	List<DespesaConvenioView> listDespesaServicoConveniada = servicoDAO.getDespesaServicoConvenio(empresaCliente.getId(), dtInicio, dtFim);
	    	
	    	if(listDespesaServicoConveniada != null && listDespesaServicoConveniada.size() > 0){
				for(DespesaConvenioView dcview : listDespesaServicoConveniada){			
					totalCobranca += dcview.getVlServico();
				}
									    		    				
		    	if (conferenciaEC == null){
					
					conferenciaEC = new Conferencia();
					conferenciaEC.setMesAno( mesAno );
					conferenciaEC.setEmpresaCliente(empresaCliente);
					conferenciaEC.setObservacao("BOLETO DESPESA");
					
					conferenciaEC.setVlTotalVenda(totalCobranca);
					conferenciaEC.setDesconto(vlDesconto);
					conferenciaEC.setObsDadosFinanceiros(obsFatura);
					
					conferenciaEC.setDtFechamento(new Date());
					conferenciaEC.setFuncionario1(securityService.getFuncionarioLogado());
					conferenciaEC.setFuncionario2(securityService.getFuncionarioLogado());
					
					conferenciaEC = conferenciaDAO.persist(conferenciaEC);			
				}
		    	else{
		    		conferenciaEC.setVlTotalVenda(totalCobranca);
		    		conferenciaEC.setDesconto(vlDesconto);
		    		conferenciaEC.setObsDadosFinanceiros(obsFatura);
		    		
		    		conferenciaEC.setDtFechamento(new Date());
		    		conferenciaEC.setFuncionario1(securityService.getFuncionarioLogado());
					conferenciaEC.setFuncionario2(securityService.getFuncionarioLogado());
		    	}
		    	
		    	for(DespesaConvenioView dcv : listDespesaServicoConveniada){
					DespesaServico ds = despesaServicoDAO.searchByKey(DespesaServico.class, dcv.getIdDespesaServico());
					ds.setInStatus(3);	//status fechado
					ds.setBoRealizado(true); //  realizado
					ds.setConferencia(conferenciaEC);
					
					despesaServicoDAO.update(ds);
				}
		    	
		    	// objetos relacionados a despesa
				Despesa despesa = new Despesa();
				despesa.setUnidade(securityService.getFuncionarioLogado().getUnidade());
				despesa.setNrDespesa(empresaCliente.getId() + "/" + mesAno);
				despesa.setContratoCliente(contratoDAO.getContratoCliente(contrato.getId()));
				despesa.setDtDespesa(new Date());
				despesa.setBoParcelaDa(false);
				despesa.setInSituacao(0);
				despesa.setInFormaPagamento(5);
				
				//validação para desconto em boleto
				if (vlDesconto != null && vlDesconto > 0.0){				
					despesa.setVlDespesaAberto(totalCobranca - vlDesconto);
					despesa.setVlDespesaTotal(totalCobranca - vlDesconto);
					despesa.setNmObservacao(obsFatura); //motivo do desconto do boleto				
				}else{
					despesa.setVlDespesaAberto(totalCobranca );
					despesa.setVlDespesaTotal(totalCobranca );
				}
				
				DespesaServico despesaServico =  new DespesaServico();
				Servico servico = servicoDAO.getServico(23356L); //(Servico) getGenericServiceLocator().getGenericService().getObjeto( Servico.class, 23356L ); //23575 : "DESPESA CONVENIADA"
				despesaServico.setServico(servico);
				despesaServico.setDespesa(despesa);
				despesaServico.setQtServico(1);
				despesaServico.setVlServico(despesa.getVlDespesaTotal());
				despesaServico.setInCoberto(false);
				despesa.getDespesaServicos().add(despesaServico);
												
				Calendar calendar = Calendar.getInstance();
		        int day = contrato.getEmpresaCliente().getDiaVencimento();
		        int month = mesFatura;
		        int year = anoFatura;
	
		        year = (month<12)? (year) : (year+1);
		        month = (month<12)? month : 0; // 0 - representa mes de janeiro
		        calendar.set(year,month,day);
				
		        //acrescimo de dias ao vencimento do boleto
				if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() != null ){
		        
		         if (  contrato.getEmpresaCliente().getInAcrescmoVencimento()  == 0 )
				       calendar.add(Calendar.MONTH, 1);  // adição de 1 mes no vencimento da mensalidade do contrato
				 else if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() == 1 )
				       calendar.add(Calendar.MONTH, 2); // adição de 2 meses no vencimento da mensalidade do contrato
				 else if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() == 2 )
				       calendar.add(Calendar.MONTH, 3); // adição de 3 mes no vencimento da mensalidade do contrato
				}
				
		        Date dtVencimento = calendar.getTime(); // data de vencimento final da parcela
		        
		        Parcela parcelaDespesa = new Parcela();
				parcelaDespesa.setDespesa(despesa);
				parcelaDespesa.setDtInclusao(new Date());
				parcelaDespesa.setDtVencimento(dtVencimento);  // data de vencimento conforme dias de acrescimo 
				parcelaDespesa.setInFormaPagamento(5);
				parcelaDespesa.setNrParcela(despesa.getNrDespesa().concat( "/01" ));
				
				//validação para desconto em boleto
				if ( vlDesconto != null && vlDesconto > 0.0 ){
					parcelaDespesa.setVlParcela(totalCobranca - vlDesconto);
					parcelaDespesa.setObservacao(obsFatura);					
				}
				else
					parcelaDespesa.setVlParcela(totalCobranca);
							
				despesa.getParcelas().add(parcelaDespesa);	
				
				despesaDAO.persist(despesa);
				
				return despesa;
	    	}
	    	else	    	
	    		return null;
	    	
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
		
	}
	
	private Mensalidade gerarMensalidadeEmpresaCliente(EmpresaCliente empresaCliente, Conferencia conferenciaEC, Integer mesFatura, Integer anoFatura, Double vlDesconto, String obsFatura, Contrato contrato){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar c = new GregorianCalendar(anoFatura, mesFatura - 1, 1);		
		
		String dtInicio = sdf.format(c.getTime());		
	    int ultimoDia = c.getActualMaximum(Calendar.DATE);
	    c.set(Calendar.DAY_OF_MONTH, ultimoDia);
	    String dtFim = sdf.format(c.getTime());
				
	    String mesAno = mesFatura + "/" + anoFatura;  
		
	    try {
	    		    	
	    	double totalCobranca = 0.0;				
			if(empresaCliente.getInTipoCobranca() != null && empresaCliente.getInTipoCobranca().equals(0)){ //Fixo				
				totalCobranca = contrato.getVlTotal();
			}
			else{ //Por vida
				List<ClienteDependenteConvenioView> listClienteDependenteConveniada = empresaClienteDAO.getClienteDependenteConveniada(empresaCliente.getId());
				if(listClienteDependenteConveniada != null && listClienteDependenteConveniada.size() > 0){
					for(ClienteDependenteConvenioView cdcview : listClienteDependenteConveniada){
						totalCobranca += cdcview.getVlContrato();			
					}					
				}	
			}				
									
			if (conferenciaEC == null){
				
				conferenciaEC = new Conferencia();
				conferenciaEC.setMesAno( mesAno );
				conferenciaEC.setEmpresaCliente(empresaCliente);
				conferenciaEC.setObservacao("BOLETO MENSALIDADE");
				
				conferenciaEC.setVlTotalVenda(totalCobranca);
				conferenciaEC.setDesconto(vlDesconto);
				conferenciaEC.setObsDadosFinanceiros(obsFatura);
				
				conferenciaEC.setDtFechamento(new Date());
				conferenciaEC.setFuncionario1(securityService.getFuncionarioLogado());
				conferenciaEC.setFuncionario2(securityService.getFuncionarioLogado());
				
				conferenciaEC = conferenciaDAO.persist(conferenciaEC);			
			}
	    	else{
	    		conferenciaEC.setVlTotalVenda(totalCobranca);
	    		conferenciaEC.setDesconto(vlDesconto);
	    		conferenciaEC.setObsDadosFinanceiros(obsFatura);
	    		
	    		conferenciaEC.setDtFechamento(new Date());
	    		conferenciaEC.setFuncionario1(securityService.getFuncionarioLogado());
				conferenciaEC.setFuncionario2(securityService.getFuncionarioLogado());
	    	}
			
			List<DespesaConvenioView> listDespesaServicoConveniada = servicoDAO.getDespesaServicoConvenio(empresaCliente.getId(), dtInicio, dtFim);
			for(DespesaConvenioView dcv : listDespesaServicoConveniada){
				DespesaServico ds = despesaServicoDAO.searchByKey(DespesaServico.class, dcv.getIdDespesaServico());
				ds.setInStatus(3);	//status fechado
				ds.setBoRealizado(true); //  realizado
				ds.setConferencia(conferenciaEC);
				
				despesaServicoDAO.update(ds);
			}
						
			// periodo alvo de geracao mensalidade	 
			Calendar calendar = Calendar.getInstance();
	        int day = contrato.getEmpresaCliente().getDiaVencimento();
	        int month = mesFatura;
	        int year = anoFatura;

	        year = (month<12)? (year) : (year+1);
	        month = (month<12)? month : 0;
	        calendar.set(year,month,day);
			
	        //acrescimo de dias ao vencimento do boleto
			if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() != null ){
	        
	         if (  contrato.getEmpresaCliente().getInAcrescmoVencimento()  == 0 )
			       calendar.add(Calendar.MONTH, 1);  // adição de 1 mes no vencimento da mensalidade do contrato
			 else if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() == 1 )
			       calendar.add(Calendar.MONTH, 2); // adição de 2 meses no vencimento da mensalidade do contrato
			 else if ( contrato.getEmpresaCliente().getInAcrescmoVencimento() == 2 )
			       calendar.add(Calendar.MONTH, 3); // adição de 3 mes no vencimento da mensalidade do contrato
			}
			
	        Date dtVencimento = calendar.getTime();
			
			Mensalidade mensalidade = new Mensalidade();
			mensalidade.setContrato(contrato);
			mensalidade.setDataVencimento( dtVencimento ); // data de vencimento conforme dias de acrescimo
			mensalidade.setInStatus(0);
			mensalidade.setNrMensalidade(mesAno);
			mensalidade.setInformaPagamento(5);
			
			if (vlDesconto != null && vlDesconto > 0.0){				
				mensalidade.setValorMensalidade(totalCobranca - vlDesconto); // valor da mensalidade com desconto
				mensalidade.setObservacao(obsFatura); // motivo do desconto do boleto				
			}else{
				mensalidade.setValorMensalidade( totalCobranca );  // valor da mensalidade sem desconto
			}
			
			mensalidadeDAO.persist(mensalidade);
			
			return mensalidade;
			
		} catch (Exception e) {
			// TODO: handle exception
			
			return null;
		}	    
		
	}
	
}
