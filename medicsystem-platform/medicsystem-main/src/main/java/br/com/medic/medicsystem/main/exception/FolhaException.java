package br.com.medic.medicsystem.main.exception;

import javax.ejb.ApplicationException;

import br.com.medic.medicsystem.main.mapper.ErrorType;

@ApplicationException(rollback = true)
public class FolhaException extends GeneralException {
	
	private static final long serialVersionUID = 1L;

	public FolhaException(String message, ErrorType errorType) {
		super(message, errorType);
	}
	
	public FolhaException(String message, ErrorType errorType, Throwable e) {
		super(message, errorType, e);
	}
}
