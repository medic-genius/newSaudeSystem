package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.service.AgenciaService;
import br.com.medic.medicsystem.persistence.model.Agencia;

@Path("/agencias")
@RequestScoped
public class AgenciaController extends BaseController {
	
	@Inject
	private AgenciaService agenciaService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAgencia(Agencia agencia) {
		
		Agencia salvo = agenciaService.saveAgencia(agencia);
		
		return Response.created(URI.create("/agencias/"+ salvo.getId())).build();
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAgencia(Agencia agencia) {

		if (agencia != null) {
			Agencia agenciaAtualizada = agenciaService.updateAgencia(agencia);

			if (agenciaAtualizada != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Agencia> getAllAgencias() {

		return agenciaService.getAllAgencias();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/banco/{idBanco:[0-9][0-9]*}")
	public List<Agencia> getAgenciaByBanco(@PathParam("idBanco") Long idBanco){
		List<Agencia> result = agenciaService.getAgenciaByBanco(idBanco);
		return result;
	}

}
