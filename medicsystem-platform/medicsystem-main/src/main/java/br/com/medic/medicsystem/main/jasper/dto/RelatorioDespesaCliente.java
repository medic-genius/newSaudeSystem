package br.com.medic.medicsystem.main.jasper.dto;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.medic.medicsystem.persistence.model.Despesa;


public class RelatorioDespesaCliente {

	private String nrMatricula;
	
	private String nmCliente;  
	
	private String nmOrgao;
	
	private String nmPlano;
	
	private String vlMensalidade;
	
	private String nmUsuario;
	
	private String nrDespesa;
	
	private Long idDespesa;
	
	private String dtDespesa;
	
	private String vlDespesa;
	
	private String nmObsevacao;
	
	private List< RelatorioDespesaServico > servicosDespesa;
	
	private List< RelatorioDespesaParcela > parcelasDespesa;
	
	private List< RelatorioDespesaEncaminhamento > encaminhamentosDespesa;
	
	public RelatorioDespesaCliente(Despesa despesa, 
			List< RelatorioDespesaServico > servicosDespesa, 
			List< RelatorioDespesaParcela > parcelasDespesa, 
			List< RelatorioDespesaEncaminhamento > encaminhamentosDespesa	){
		
		DecimalFormat formatador = new DecimalFormat();
		formatador.setMinimumFractionDigits( 2 );
		
		DateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		this.nrMatricula = despesa.getContratoCliente().getCliente().getNrCodCliente();
		this.nmCliente = despesa.getContratoCliente().getCliente().getNmCliente();
		this.nmOrgao = (despesa.getContratoCliente()!=null && despesa.getContratoCliente().getContrato()!=null && despesa.getContratoCliente().getContrato().getEmpresaGrupo()!=null 
				? despesa.getContratoCliente().getContrato().getEmpresaGrupo().getNmFantasia() : despesa.getUnidade().getEmpresaGrupo().getNmFantasia());
		this.nmPlano = (despesa.getContratoCliente().getPlano() != null ? despesa.getContratoCliente().getPlano().getNmPlano() : "N�O ASSOCIADO");
		this.vlMensalidade = formatador.format( despesa.getContratoCliente().getContrato().getVlTotal() );
		this.nmUsuario = (despesa.getContratoDependente() != null 
					? despesa.getContratoDependente().getDependente().getNmDependente()
					: despesa.getContratoCliente().getCliente().getNmCliente());
		
		this.nrDespesa = despesa.getNrDespesa();
		this.idDespesa = despesa.getId();
		this.dtDespesa = (despesa.getDtDespesa() == null ? "":  dataFormat.format( despesa.getDtDespesa() )  ) ;
		this.vlDespesa = formatador.format( despesa.getVlDespesaTotal() );
		this.nmObsevacao = despesa.getNmObservacao();
		
		this.servicosDespesa = servicosDespesa;
		this.parcelasDespesa = parcelasDespesa;
		this.encaminhamentosDespesa = encaminhamentosDespesa;
		
	}
	
	public String getDtDespesa() {
	
		return dtDespesa;
	}
 
	
	public void setDtDespesa( String dtDespesa ) {
	
		this.dtDespesa = dtDespesa;
	}

	
	public String getNmCliente() {
	
		return nmCliente;
	}

	
	public void setNmCliente( String nmCliente ) {
	
		this.nmCliente = nmCliente;
	}

	
	public String getNmObsevacao() {
	
		return nmObsevacao;
	}

	
	public void setNmObsevacao( String nmObsevacao ) {
	
		this.nmObsevacao = nmObsevacao;
	}

	
	public String getNmOrgao() {
	
		return nmOrgao;
	}

	
	public void setNmOrgao( String nmOrgao ) {
	
		this.nmOrgao = nmOrgao;
	}

	
	public String getNmPlano() {
	
		return nmPlano;
	}

	
	public void setNmPlano( String nmPlano ) {
	
		this.nmPlano = nmPlano;
	}

	
	public String getNmUsuario() {
	
		return nmUsuario;
	}

	
	public void setNmUsuario( String nmUsuario ) {
	
		this.nmUsuario = nmUsuario;
	}

	
	public String getNrDespesa() {
	
		return nrDespesa;
	}

	
	public void setNrDespesa( String nrDespesa ) {
	
		this.nrDespesa = nrDespesa;
	}

	
	public String getNrMatricula() {
	
		return nrMatricula;
	}

	
	public void setNrMatricula( String nrMatricula ) {
	
		this.nrMatricula = nrMatricula;
	}

	
	public String getVlDespesa() {
	
		return vlDespesa;
	}

	
	public void setVlDespesa( String vlDespesa ) {
	
		this.vlDespesa = vlDespesa;
	}

	
	public String getVlMensalidade() {
	
		return vlMensalidade;
	}

	
	public void setVlMensalidade( String vlMensalidade ) {
	
		this.vlMensalidade = vlMensalidade;
	}

	
	public List< RelatorioDespesaEncaminhamento > getEncaminhamentosDespesa() {
	
		return encaminhamentosDespesa;
	}

	
	public void setEncaminhamentosDespesa( List< RelatorioDespesaEncaminhamento > encaminhamentosDespesa ) {
	
		this.encaminhamentosDespesa = encaminhamentosDespesa;
	}

	
	public List< RelatorioDespesaParcela > getParcelasDespesa() {
	
		return parcelasDespesa;
	}

	
	public void setParcelasDespesa( List< RelatorioDespesaParcela > parcelasDespesa ) {
	
		this.parcelasDespesa = parcelasDespesa;
	}

	
	public List< RelatorioDespesaServico > getServicosDespesa() {
	
		return servicosDespesa;
	}

	
	public void setServicosDespesa( List< RelatorioDespesaServico > servicosDespesa ) {
	
		this.servicosDespesa = servicosDespesa;
	}
	
	public Long getIdDespesa() {
		
		return idDespesa;
	}

	
	public void setIdDespesa( Long idDespesa ) {
	
		this.idDespesa = idDespesa;
	}
	
}
