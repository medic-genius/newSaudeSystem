package br.com.medic.medicsystem.main.vindi.data.subscription;

import java.util.List;

import br.com.medic.medicsystem.main.vindi.data.summary.PaymentProfile;

public class Bill {
	private Integer id;
	private String code;
	private Number amount;
	private Integer installments;
	private String status;
	private String billing_at;
	private String due_at;
	private String url;
	private String created_at;
	private List<BillCharge> charges;
	private PaymentProfile payment_profile;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Number getAmount() {
		return amount;
	}
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	public Integer getInstallments() {
		return installments;
	}
	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBilling_at() {
		return billing_at;
	}
	public void setBilling_at(String billing_at) {
		this.billing_at = billing_at;
	}
	public String getDue_at() {
		return due_at;
	}
	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public List<BillCharge> getCharges() {
		return charges;
	}
	public void setCharges(List<BillCharge> charges) {
		this.charges = charges;
	}
	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}
	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
