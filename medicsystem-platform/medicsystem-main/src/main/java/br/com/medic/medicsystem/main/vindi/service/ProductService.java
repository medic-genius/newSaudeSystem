package br.com.medic.medicsystem.main.vindi.service;

import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.product.Product;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.response.ResponseParser;
import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;

public class ProductService extends BaseHttpService {
	private final String BASE_URL = "/api/v1/products";
	private ResponseParser<Product> parser = new ResponseParser<Product>(Product.class);
	
	public ProductService() {
		super();
		setUseSSL(true);
	}
	
	public List<Product> getProducts() {
		String url = BASE_URL;
		HttpGet get = new HttpGet();
		get.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String response = sendRequest(url, get);
			return parser.parseResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Product createProduct(
			br.com.medic.medicsystem.main.vindi.data.parameters.create.Product product) throws VindiException {
		String url = BASE_URL;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String jsonStr = (new ObjectMapper()).writeValueAsString(product);
			post.setEntity(new StringEntity(jsonStr));
			String response = sendRequest(url, post);
			return parser.parseSingleResponse(response);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
