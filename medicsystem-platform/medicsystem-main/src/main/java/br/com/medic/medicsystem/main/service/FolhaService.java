package br.com.medic.medicsystem.main.service;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.PatternSyntaxException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVReader;

import br.com.medic.medicsystem.main.exception.FolhaException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ImportaFolhaDAO;
import br.com.medic.medicsystem.persistence.dao.ImportaFolhaFuncDAO;
import br.com.medic.medicsystem.persistence.dao.ImportaFolhaFuncFluxoDAO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.FolhaImportResult;
import br.com.medic.medicsystem.persistence.dto.FuncionarioFolha;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaDescontosTotalDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaGraficoDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaTotaisGrupoDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaUnidadesDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;
import br.com.medic.medicsystem.persistence.model.ImportaFolha;
import br.com.medic.medicsystem.persistence.model.ImportaFolhaFunc;
import br.com.medic.medicsystem.persistence.model.ImportaFolhaFuncFluxo;
import br.com.medic.medicsystem.persistence.model.UnidadeFinanceiro;
	
/**
 * @author Phillip
 * @since 01/2016
 * @lastModification 29/05/2016
 * @version 1.4
 */

@Stateless
public class FolhaService {

	@Inject
	private Logger logger;

	private CSVReader reader;

	@Inject
	@Named("importafolha-dao")
	private ImportaFolhaDAO ifolhaDAO;

	@Inject
	@Named("importafolhafunc-dao")
	private ImportaFolhaFuncDAO ifolhafuncDAO;

	@Inject
	@Named("importafolhafuncfluxo-dao")
	private ImportaFolhaFuncFluxoDAO ifolhafuncfluxoDAO;
	

	private String connectToPythonConverter(String content) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:5000");
		Response res = target.request().post(Entity.entity(content, "text/plain"));
		if (res.getStatus() == 200) {
			return res.readEntity(String.class);
		}

		return null;
	}

	public FolhaImportResult importarFolha(String content) {

		logger.debug("Convertendo folha" + content);
		String response = connectToPythonConverter(content);
		Properties props = PropertiesLoader.getInstance().load("messageFolha.properties");

		FolhaImportResult fir = new FolhaImportResult();

		JSONObject json;
		try {
			json = (JSONObject) new JSONParser().parse(response);
		} catch (ParseException e) {
			throw new FolhaException(props.getProperty("erroconverterfolha"), ErrorType.DANGER); 
		}

		fir.setErros((String) json.get("errors"));
		fir.setMesReferencia((String) json.get("mesreferencia"));
		fir.setNmEmpresaFinanceiro( (String) json.get("nmempresafinanceiro")  );
		fir.setNmUnidade((String) json.get("nmunidade"));
		if (json.get("nroimportados") != null) {
			fir.setNroImportados((Long) json.get("nroimportados"));
		}
		String output = (String) json.get("output");
		try {
			converterOutputToJSONObject(output, fir);
		} catch (ParseException | IOException e) {
			throw new FolhaException(props.getProperty("erroconverterfolha"), ErrorType.DANGER); 
		}

		return fir;
	}

	private void converterOutputToJSONObject(String output, FolhaImportResult fir) throws ParseException, IOException {

		reader = new CSVReader(new StringReader(output));

		List<String[]> myEntries = reader.readAll();

		for (int j = 1; j < myEntries.size(); j++) {

			List<String> item = Arrays.asList(myEntries.get(j));

			FuncionarioFolha ff = new FuncionarioFolha();
			ff.setCod(item.get(0));
			ff.setName(item.get(1));
			ff.setCargo(item.get(2));
			ff.setAdmissao(item.get(3));
			ff.setNrodep(Long.valueOf(item.get(4)));
			ff.setSalario(item.get(5));

			JSONObject jsonFluxo = (JSONObject) new JSONParser().parse(item.get(6));
			ff.setFluxo(jsonFluxo);

			ff.setLiquido(item.get(7));
			ff.setBase_irrf(item.get(8));
			ff.setContr_inss(item.get(9));
			ff.setInss_segurado(item.get(10));
			ff.setBase_fgts(item.get(11));
			ff.setFgts(item.get(12));

			fir.getOutput().add(ff);
		}

	}

	public ImportaFolhaFunc getFuncionarioMesAnterior(String cod, String mesanoreferencia, Long idEmpresafinanceiro, Boolean isFolhaAvulsa) {

		String dateString[];
		try {
			dateString = mesanoreferencia.split("/");
		} catch (PatternSyntaxException e) {
			return null;
		}

		Integer month = Integer.parseInt(dateString[0]) - 1;
		Integer year = Integer.parseInt(dateString[1]);

		Calendar mesAtual = Calendar.getInstance();
		mesAtual.set(Calendar.MONTH, month);
		mesAtual.set(Calendar.YEAR, year);
		mesAtual.set(Calendar.DAY_OF_MONTH, 1);

		if (mesAtual.get(Calendar.MONTH) == 0) {
			mesAtual.roll(Calendar.MONTH, false);
			mesAtual.roll(Calendar.YEAR, false);
		} else {
			mesAtual.roll(Calendar.MONTH, false);
		}

		String mesanterior = "";
		String monthcalc = String.valueOf(mesAtual.get(Calendar.MONTH) + 1);
		if (monthcalc.length() == 1) {
			monthcalc = "0" + monthcalc;
		}
		mesanterior = mesanterior.concat(monthcalc);
		mesanterior = mesanterior.concat("/");
		mesanterior = mesanterior.concat(String.valueOf(mesAtual.get(Calendar.YEAR)));

		List<ImportaFolhaFunc> col = new ArrayList<>();

		try {
			col = (List<ImportaFolhaFunc>) ifolhafuncDAO.findByTypedQueryWithParams(
					"from ImportaFolhaFunc oiff where oiff.codigo = ?1 "
					+ "and oiff.tbimportafolha.mesanoreferencia = ?2 "
					+ "and oiff.empresafinanceiro.idEmpresaFinanceiro = ?3 "
					+ "and oiff.tbimportafolha.isFolhaAvulsa is ?4 "  ,
					ImportaFolhaFunc.class, cod, mesanterior, idEmpresafinanceiro, isFolhaAvulsa);
		} catch (ObjectNotFoundException e) {
			return null;
		}

		return col.get(0);

	}

	Locale ptBR = new Locale("pt", "BR");
	NumberFormat nf = NumberFormat.getInstance(ptBR);

	public ImportaFolha salvarFolha(FolhaImportResult content) {

		UnidadeFinanceiro ud = new UnidadeFinanceiro();
		EmpresaFinanceiro ef = new EmpresaFinanceiro();
		Properties props = PropertiesLoader.getInstance().load("messageFolha.properties");
		
		ud = getUnidadeByNickName( content.getNmUnidade(), content.getNmEmpresaFinanceiro() );
		ef = getEmpresaByNickName( content.getNmEmpresaFinanceiro() );
		
		if( ud == null )
			throw new FolhaException(props.getProperty("unidadenotfound"), ErrorType.DANGER); 
		
		if( ef == null )
			throw new FolhaException(props.getProperty("empresanotfound"), ErrorType.DANGER);
		
		ImportaFolha oif = getImportaFolhaPorMes(content.getMesReferencia(), ud, content.getIsFolhaAvulsa());

		if (oif == null) {
			oif = new ImportaFolha();
			oif.setUnidadeFinanceiro(ud);
			oif.setMesanoreferencia(content.getMesReferencia());
			oif.setIsFolhaAvulsa( content.getIsFolhaAvulsa() );
		} else {
			if(  oif.getIsFolhaAvulsa() != content.getIsFolhaAvulsa()  ) {
				oif = new ImportaFolha();
				oif.setUnidadeFinanceiro(ud);
				oif.setMesanoreferencia(content.getMesReferencia());
				oif.setIsFolhaAvulsa( content.getIsFolhaAvulsa() );
			}
		}

		oif.setNroimportados(content.getNroImportados().intValue());

		Double somavalorapagar = 0D;
		Double somasalariobase = 0D;
		Double somadescontos = 0D;
		Double somaacrescimos = 0D;
		Double somasalarioFolha = 0D;

		List<ImportaFolhaFunc> fflist = new ArrayList<>();

		for (FuncionarioFolha ff : content.getOutput()) {

			ImportaFolhaFunc funcMA = getFuncionarioMesAnterior(ff.getCod(), oif.getMesanoreferencia(), ef.getIdEmpresaFinanceiro(), oif.getIsFolhaAvulsa() );

			ImportaFolhaFunc iff = getFuncionarioMesAtual(ff.getCod(), oif.getMesanoreferencia(), ef.getIdEmpresaFinanceiro(), oif.getIsFolhaAvulsa() );

			if (iff == null) {
				iff = new ImportaFolhaFunc();
			} else {
				iff.getFluxos().clear();
			}

			if (funcMA != null) {
				
				iff.setVlmesanterior(funcMA.getLiquido());
				iff.setVlmesanteriorBase( funcMA.getSalario() );
				iff.setVlAcrescimoSalarioMesAnterior( funcMA.getVlAcrescimoSalario() != null ? funcMA.getVlAcrescimoSalario() : new Double(0) );
				
			}
			
			if(iff.getId() != null && (iff.getStatus() != null && iff.getStatus() != 1)) {
				iff.setStatus(0); // pendente
			}
			
			iff.setCodigo(ff.getCod());
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date date = null;
			try {
				date = format.parse(ff.getAdmissao());
			} catch (java.text.ParseException e) {
				throw new IllegalArgumentException(e);
			}
			iff.setAdmissao(date);
			iff.setNome(ff.getName());
			iff.setCargo(ff.getCargo());
			iff.setNrodep(ff.getNrodep().intValue());

			Double salario = null;
			Double liquido = null;
			Double basefgts = null;
			Double baseirrf = null;
			Double contrinss = null;
			Double fgts = null;
			Double insssegurado = null;
			try {
				salario = nf.parse(ff.getSalario()).doubleValue();
				liquido = nf.parse(ff.getLiquido()).doubleValue();
				basefgts = nf.parse(ff.getBase_fgts()).doubleValue();
				baseirrf = nf.parse(ff.getBase_irrf()).doubleValue();
				contrinss = nf.parse(ff.getContr_inss()).doubleValue();
				fgts = nf.parse(ff.getFgts()).doubleValue();
				insssegurado = nf.parse(ff.getInss_segurado()).doubleValue();
			} catch (java.text.ParseException e) {
				throw new IllegalArgumentException(e);
			}

			somasalariobase += salario;
			iff.setSalario(salario);

			somavalorapagar += liquido;
			iff.setLiquido(liquido);

			iff.setBasefgts(basefgts);
			iff.setBaseirrf(baseirrf);
			iff.setContrinss(contrinss);
			iff.setFgts(fgts);
			iff.setInsssegurado(insssegurado);
			
			iff.setEmpresafinanceiro( ef );

			@SuppressWarnings("unchecked")
			HashMap<String, ?> fluxo = (HashMap<String, ?>) ff.getFluxo();

			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo1", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo1", "Right", true);
						
			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo2", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo2", "Right", true);

			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo3", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo3", "Right", true);

			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo4", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo4", "Right", true);

			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo5", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo5", "Right", true);

			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo6", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo6", "Right", true);
			
			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo7", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo7", "Right", true);
 
			somaacrescimos += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo8", "Left", true);
			somadescontos  += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo8", "Right", true);
						
			somasalarioFolha += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo1", "Left", false);
			somasalarioFolha += addFluxoDetail(iff.getFluxos(), fluxo, "fluxo2", "Left", false);
						
			fflist.add(iff);			
		
		}		

		oif.setSomaacrescimos(somaacrescimos);
		oif.setSomadescontos(somadescontos);
		oif.setSomasalariobase(somasalariobase);
		oif.setSomavalorapagar(somavalorapagar);
		oif.setSomasalarioFolha(somasalarioFolha);
		

		ifolhaDAO.persist(oif);

		for (ImportaFolhaFunc iff : fflist) {
			List<ImportaFolhaFuncFluxo> listFluxos = iff.getFluxos();
			iff.setTbimportafolha(oif);
			if (iff.getId() == null) {
				ifolhafuncDAO.persist(iff);
			} else {
				ifolhafuncDAO.update(iff);
			}

			for (ImportaFolhaFuncFluxo importaFolhaFuncFluxo : listFluxos) {
			 importaFolhaFuncFluxo.setTbimportafolhafunc(iff);
			 ifolhafuncfluxoDAO.persist(importaFolhaFuncFluxo);
			}
		}

		return oif;

	}

	public List<BaseDTO> verificarFuncionarioMesmoCodigo( FolhaImportResult content ) {
		
		EmpresaFinanceiro ef = getEmpresaByNickName( content.getNmEmpresaFinanceiro() );
		Properties props = PropertiesLoader.getInstance().load("messageFolha.properties");
		List<BaseDTO> resultadoList;
		
		if( ef != null ) {
			resultadoList =  ifolhaDAO.verificarFuncionarioMesmoCodigo( ef.getIdEmpresaFinanceiro(), content.getMesReferencia(), content.getOutput() );
			return resultadoList.size() > 0 ? resultadoList:null;
		} else
			throw new FolhaException(props.getProperty("empresanotfound"), ErrorType.DANGER); 
	}

	/**
	 * 
	 * @author Joelton
	 * 
	 * @param nmEmpresa
	 * 
	 * @return
	 */
	private EmpresaFinanceiro getEmpresaByNickName( String nmEmpesaFinanceiro ) {
		
		EmpresaFinanceiro empresaFinanceiro = ifolhaDAO.getEmpresaFinanceiroByNickName( nmEmpesaFinanceiro );
		
		return empresaFinanceiro;
	}

	/**
	 * 
	 * @author Joelton
	 * 
	 * @param nmUnidade
	 * 
	 * @return
	 * 		Unidade
	 */
	private UnidadeFinanceiro getUnidadeByNickName(String unidadeNickName, String nmEmpresaFinanceiro) {
		
		UnidadeFinanceiro unidadeFinanceiro = ifolhaDAO.getUnidadeFinanceiroByNickName( unidadeNickName, nmEmpresaFinanceiro );
		
		return unidadeFinanceiro;
	}

	private ImportaFolhaFunc getFuncionarioMesAtual(String cod, String mesanoreferencia, Long idEmpresaFinanceiro, Boolean isFolhaAvulsa ) {

		List<ImportaFolhaFunc> col = new ArrayList<>();

		try {
			col = (List<ImportaFolhaFunc>) ifolhafuncDAO.findByTypedQueryWithParams(
					"from ImportaFolhaFunc oiff where oiff.codigo = ?1 "
					+ "and oiff.tbimportafolha.mesanoreferencia = ?2 "
				    + "and oiff.empresafinanceiro.idEmpresaFinanceiro = ?3 "
				    + "and oiff.tbimportafolha.isFolhaAvulsa = ?4 ",
					ImportaFolhaFunc.class, cod, mesanoreferencia, idEmpresaFinanceiro,isFolhaAvulsa);
		} catch (ObjectNotFoundException e) {
			return null;
		}

		return col.get(0);

	}

	private ImportaFolha getImportaFolhaPorMes(String mesReferencia, UnidadeFinanceiro ud, Boolean isFolhaAvulsa) {
		List<ImportaFolha> col = new ArrayList<>();

		try {
			col = (List<ImportaFolha>) ifolhaDAO.findByTypedQueryWithParams(
					"from ImportaFolha oif where oif.mesanoreferencia = ?1 "
					+ "and unidadeFinanceiro.idUnidadeFinanceiro = ?2 "
					+ "and oif.isFolhaAvulsa is ?3 ",
					ImportaFolha.class,
					mesReferencia, ud.getIdUnidadeFinanceiro(), isFolhaAvulsa);
		} catch (ObjectNotFoundException e) {
			return null;
		}

		return col.get(0);

	}
	

	private Double addFluxoDetail(List<ImportaFolhaFuncFluxo> fluxos, HashMap<String, ?> fluxo, String fluxoId,
			String direction, Boolean isAcrescimoDesconto ) {

		
		if (fluxo.get(fluxoId) == null) {
			return 0D;
		}

		@SuppressWarnings("unchecked")
		LinkedHashMap<String, ?> fluxoDetail = (LinkedHashMap<String, ?>) fluxo.get(fluxoId);

		ImportaFolhaFuncFluxo ifff = new ImportaFolhaFuncFluxo();

		if (fluxoDetail.get("fcod" + direction) == null) {
			return 0D;
		} 

		ifff.setCodigo((String) fluxoDetail.get("fcod" + direction));
		ifff.setDescricao((String) fluxoDetail.get("fdesc" + direction));

		Double ifffFvl = null;
		Double ifffFvt = null;
		try {
			ifffFvl = nf.parse((String) fluxoDetail.get("fvl" + direction)).doubleValue();
			ifffFvt = nf.parse((String) fluxoDetail.get("fvt" + direction)).doubleValue();
		} catch (java.text.ParseException e) {
			throw new IllegalArgumentException(e);
		}

		ifff.setVlunitario(ifffFvl);
		ifff.setVltotal(ifffFvt);

		if( isAcrescimoDesconto ) {
			
			if( ("fcod" + direction).equals("fcodRight") && ("fdesc" + direction).equals("fdescRight") ) {
				ifff.setTipo("descontos");
			} else if( ("fcod" + direction).equals("fcodLeft") && ("fdesc" + direction).equals("fdescLeft")  ) {
				ifff.setTipo("proventos");
			}
			
			fluxos.add(ifff);
		}
			
		
		if ("SALARIO".equals(ifff.getDescricao()) ||
			"PRESTACAO DE SERVICO".equals(ifff.getDescricao()) ||
			"BOLSA AUXILIO EDUCACIONAL".equals(ifff.getDescricao()) ||
			"AUTONOMO".equals(ifff.getDescricao())) {
			
			if( isAcrescimoDesconto )
				return 0D;
			else
				return ifff.getVltotal();
		}

		return  isAcrescimoDesconto ? ifff.getVltotal() : 0D;

	}

	public ImportaFolha getImportaFolha(Long id) {
		return ifolhaDAO.searchByKey(ImportaFolha.class, id);
	}

	public ImportaFolhaDTO getImportaFolhaDTO(Long id) {
		ImportaFolha oif = getImportaFolha(id);

		Collection<?> funcs = ifolhafuncDAO.findByQuery("from ImportaFolhaFunc oiff where oiff.tbimportafolha.id = ?1 "
				+ " order by oiff.nome",
				oif.getId());

		ImportaFolhaDTO dto = new ImportaFolhaDTO();
		dto.setOif(oif);
		dto.setFuncs(funcs);
		return dto;
	}

	public ImportaFolhaUnidadesDTO getImportaFolhaPorMes(String data) {

		Collection<ImportaFolha> col = ifolhaDAO.findByTypedQueryWithParams(
				"from ImportaFolha oif where oif.mesanoreferencia = ?1", ImportaFolha.class, data);

		ArrayList<ImportaFolha> list = new ArrayList<ImportaFolha>(col);

		ImportaFolhaUnidadesDTO dto = new ImportaFolhaUnidadesDTO();
		dto.setMesanoreferencia(list.get(0).getMesanoreferencia());

		Integer nroimportados = 0;
		double somaacrescimos = 0D;
		double somadescontos = 0D;
		double somasalariobase = 0D;
		double somavalorapagar = 0D;

		for (ImportaFolha importaFolha : list) {
			nroimportados += importaFolha.getNroimportados();
			somaacrescimos += importaFolha.getSomaacrescimos();
			somadescontos += importaFolha.getSomadescontos();
			somasalariobase += importaFolha.getSomasalariobase();
			somavalorapagar += importaFolha.getSomavalorapagar();

			ImportaFolhaDTO dtof = new ImportaFolhaDTO();
			dtof.setOif(importaFolha);
			dto.getFolhas().add(dtof);

		}

		dto.setNroimportados(nroimportados);
		dto.setSomaacrescimos(somaacrescimos);
		dto.setSomadescontos(somadescontos);
		dto.setSomasalariobase(somasalariobase);
		dto.setSomavalorapagar(somavalorapagar);

		return dto;

	}
	
	public ImportaFolhaUnidadesDTO getImportaFolhaPorMes(Long idEmpresaFinanceiro,String data) {

		Collection<ImportaFolha> col = ifolhaDAO.findByTypedQueryWithParams(
				"from ImportaFolha oif where oif.mesanoreferencia = ?1"
				+ " and oif.unidadeFinanceiro.empresaFinanceiro.idEmpresaFinanceiro = ?2", ImportaFolha.class, data, idEmpresaFinanceiro);

		ArrayList<ImportaFolha> list = new ArrayList<ImportaFolha>(col);

		ImportaFolhaUnidadesDTO dto = new ImportaFolhaUnidadesDTO();
		dto.setMesanoreferencia(list.get(0).getMesanoreferencia());

		Integer nroimportados = 0;
		double somaacrescimos = 0D;
		double somadescontos = 0D;
		double somasalariobase = 0D;
		double somavalorapagar = 0D;
		double somasalariofolha = 0D;

		for (ImportaFolha importaFolha : list) {
			nroimportados += importaFolha.getNroimportados();
			somaacrescimos += importaFolha.getSomaacrescimos();
			somadescontos += importaFolha.getSomadescontos();
			somasalariobase += importaFolha.getSomasalariobase();
			somavalorapagar += importaFolha.getSomavalorapagar();
			somasalariofolha += importaFolha.getSomasalarioFolha();

			ImportaFolhaDTO dtof = new ImportaFolhaDTO();
			dtof.setOif(importaFolha);
			dto.getFolhas().add(dtof);

		}

		dto.setNroimportados(nroimportados);
		dto.setSomaacrescimos(somaacrescimos);
		dto.setSomadescontos(somadescontos);
		dto.setSomasalariobase(somasalariobase);
		dto.setSomavalorapagar(somavalorapagar);
		dto.setSomasalariofolha(somasalariofolha);

		return dto;

	}

	public void aprovaFolhaFunc(Long idfunc) {
		ImportaFolhaFunc iff = ifolhafuncDAO.searchByKey(ImportaFolhaFunc.class, idfunc);
		iff.setStatus(1);
		ifolhafuncDAO.update(iff);
	}

	public void reprovaFolhaFunc(Long idfunc, String comentario) {
		ImportaFolhaFunc iff = ifolhafuncDAO.searchByKey(ImportaFolhaFunc.class, idfunc);
		iff.setStatus(2);
		iff.setComentario(comentario);
		ifolhafuncDAO.update(iff);
	}

	public ImportaFolhaUnidadesDTO getMultipleImportaFolhaDTO(Long idEmpresaFinanceiro,String data) {

		ImportaFolhaUnidadesDTO dto = getImportaFolhaPorMes(idEmpresaFinanceiro,data);
		List<ImportaFolhaDTO> folhas = new ArrayList<>(dto.getFolhas());
		dto.getFolhas().clear();
		for (ImportaFolhaDTO importaFolhaDTO : folhas) {

			dto.getFolhas().add(getImportaFolhaDTO(importaFolhaDTO.getOif().getId()));

		}
		return dto;
	}

	public List<ImportaFolhaGraficoDTO> getImportaFolhaUltimos12Meses(Long idEmpresaFinanceiro,String data) {

		String dateString[];
		try {
			dateString = data.split("/");
		} catch (PatternSyntaxException e) {
			return null;
		}

		Integer month = Integer.parseInt(dateString[0]) - 1;
		Integer year = Integer.parseInt(dateString[1]);

		Calendar mesAtual = Calendar.getInstance();
		mesAtual.set(Calendar.MONTH, month);
		mesAtual.set(Calendar.YEAR, year);
		mesAtual.set(Calendar.DAY_OF_MONTH, 1);

		data = "'" + data + "'";

		for (int i = 0; i < 11; i++) {
			if (mesAtual.get(Calendar.MONTH) == 0) {
				mesAtual.roll(Calendar.MONTH, false);
				mesAtual.roll(Calendar.YEAR, false);
			} else {
				mesAtual.roll(Calendar.MONTH, false);
			}

			data = data.concat(",");
			String monthcalc = String.valueOf(mesAtual.get(Calendar.MONTH) + 1);
			if (monthcalc.length() == 1) {
				monthcalc = "0" + monthcalc;
			}
			data = data.concat("'");
			data = data.concat(monthcalc);
			data = data.concat("/");
			data = data.concat(String.valueOf(mesAtual.get(Calendar.YEAR)));
			data = data.concat("'");
		}
		
	
		List<ImportaFolhaGraficoDTO> list = new ArrayList<>();
		list = ifolhaDAO.getImportaFolhaUltimos12Meses(idEmpresaFinanceiro, data);
		

		return list;
		
//impo:versao antiga consulta folhas do mes
		
//		@SuppressWarnings("unchecked")
//		Collection<Object[]> col = (Collection<Object[]>) ifolhaDAO
//				.findByQuery("select oif.mesanoreferencia, oif.unidadeFinanceiro.idUnidadeFinanceiro, sum(oif.somavalorapagar) "
//						+ "from ImportaFolha oif where oif.mesanoreferencia in (" + data + ")  "
//						+ "and oif.unidadeFinanceiro.empresaFinanceiro.idEmpresaFinanceiro = " + idEmpresaFinanceiro + " "
//						+ "group by oif.mesanoreferencia, oif.unidadeFinanceiro.idUnidadeFinanceiro, oif.somavalorapagar "
//						+ "order by oif.mesanoreferencia desc");
//
//		List<ImportaFolhaGraficoDTO> list = new ArrayList<>();
//		for (Object[] objects : col) {
//			ImportaFolhaGraficoDTO grf = new ImportaFolhaGraficoDTO();
//			grf.setMes((String) objects[0]);
//			grf.setFolha((Double) objects[2]);
//			list.add(grf);
//		}
	
	}

	public List<ImportaFolhaDescontosTotalDTO> getDescontosTotais(Long idEmpresaFinanceiro, String data) {
		
		List<ImportaFolhaDescontosTotalDTO> list = new ArrayList<>();
		list = ifolhaDAO.getDescontosTotais(idEmpresaFinanceiro, data);
		
		return list;
	}

	public List<ImportaFolhaFuncFluxo> getFluxosFuncionarioByDate(String codigo, Long idEmpresaFinanceiro, String data, Boolean isFolhaAvulsa) {
		
		Collection<ImportaFolhaFuncFluxo> col = ifolhafuncfluxoDAO.findByTypedQueryWithParams(
				"from ImportaFolhaFuncFluxo ifff  where ifff.tbimportafolhafunc.codigo = ?1"
				+ " and ifff.tbimportafolhafunc.empresafinanceiro.idEmpresaFinanceiro = ?2"
				+ " and ifff.tbimportafolhafunc.tbimportafolha.mesanoreferencia = ?3"
				+ " and ifff.tbimportafolhafunc.tbimportafolha.isFolhaAvulsa is ?4 ", ImportaFolhaFuncFluxo.class, codigo, idEmpresaFinanceiro ,data, isFolhaAvulsa);

		ArrayList<ImportaFolhaFuncFluxo> list = new ArrayList<ImportaFolhaFuncFluxo>(col);
		
		
		return list;
	}

	public ImportaFolhaTotaisGrupoDTO getTotaisGrupoByPeriodo(Integer inTipoGrupo, String data) {
		
		ImportaFolhaTotaisGrupoDTO totaisGrupo = new ImportaFolhaTotaisGrupoDTO();
		totaisGrupo = ifolhaDAO.getTotaisGrupoByPeriodo( inTipoGrupo, data );
		
		return totaisGrupo;
	}
	
	public List<ImportaFolhaGraficoDTO> getImportaGrupoUltimos12Meses(Long idGrupoFinanceiro,String data) {

		String dateString[];
		try {
			dateString = data.split("/");
		} catch (PatternSyntaxException e) {
			return null;
		}

		Integer month = Integer.parseInt(dateString[0]) - 1;
		Integer year = Integer.parseInt(dateString[1]);

		Calendar mesAtual = Calendar.getInstance();
		mesAtual.set(Calendar.MONTH, month);
		mesAtual.set(Calendar.YEAR, year);
		mesAtual.set(Calendar.DAY_OF_MONTH, 1);

		data = "'" + data + "'";

		for (int i = 0; i < 11; i++) {
			if (mesAtual.get(Calendar.MONTH) == 0) {
				mesAtual.roll(Calendar.MONTH, false);
				mesAtual.roll(Calendar.YEAR, false);
			} else {
				mesAtual.roll(Calendar.MONTH, false);
			}

			data = data.concat(",");
			String monthcalc = String.valueOf(mesAtual.get(Calendar.MONTH) + 1);
			if (monthcalc.length() == 1) {
				monthcalc = "0" + monthcalc;
			}
			data = data.concat("'");
			data = data.concat(monthcalc);
			data = data.concat("/");
			data = data.concat(String.valueOf(mesAtual.get(Calendar.YEAR)));
			data = data.concat("'");
		}
		
	
		List<ImportaFolhaGraficoDTO> list = new ArrayList<>();
		list = ifolhaDAO.getImportaGrupoUltimos12Meses(idGrupoFinanceiro, data);
		

		return list;
	
	}
	
	public ImportaFolha acrescimoSalarioFolhaFuncionario( Long idFolha, List<BaseDTO> funcModificados) {
		
		ImportaFolha folhaModificada = null; 
		
		if ( funcModificados.size() > 0 ) {
			
			ImportaFolha folha = ifolhaDAO.searchByKey(ImportaFolha.class, idFolha);
			Double vlAcrescimo = 0D;
			
			for ( BaseDTO dadosFunc : funcModificados) {
				
				Long idFuncionario = dadosFunc.getId();
				Double vlAcrescimoFuncionario = Double.parseDouble( dadosFunc.getDescription() );
				
				ImportaFolhaFunc func = ifolhafuncDAO.searchByKey(ImportaFolhaFunc.class, idFuncionario  );
				
				if ( func.getVlAcrescimoSalario() != null ) {
					folha.setSomavalorapagar( folha.getSomavalorapagar() - func.getVlAcrescimoSalario() );
				}
				
				func.setVlAcrescimoSalario( vlAcrescimoFuncionario  );
				vlAcrescimo += vlAcrescimoFuncionario;
				
				ifolhafuncDAO.update( func );
			}
			
			folha.setSomavalorapagar( folha.getSomavalorapagar() + vlAcrescimo );
			ifolhaDAO.update(folha);
			
			folhaModificada = folha;
		}
		
		return folhaModificada;
	}

}