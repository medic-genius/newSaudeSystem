package br.com.medic.medicsystem.main.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONObject;

import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.TotemDAO;
import br.com.medic.medicsystem.persistence.dto.AgendamentoTotemDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaDTO;
import br.com.medic.medicsystem.persistence.dto.DigitalClienteDTO;
import br.com.medic.medicsystem.persistence.dto.FuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteViewDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Foto;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.RegistroPonto;
import br.com.medic.medicsystem.persistence.model.Unidade;
import br.com.medic.medicsystem.persistence.model.enums.TipoPessoa;

@Stateless
public class TotemService {

	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;

	@Inject
	@Named("totem-dao")
	private TotemDAO totemDAO;

	@Inject
	private AgendamentoService agendamentoService;

	@Inject
	private RegistroPontoService registroPontoService;

	public List<AgendamentoTotemDTO> getAgendamentoPorIdPaciente(
			String nruniqueid) throws ParseException {

		String inTipoPaciente = nruniqueid.substring(0, 1);

		Long Idpaciente = Long.parseLong(nruniqueid.substring(1,
				nruniqueid.length()));

		List<AgendamentoTotemDTO> agendamentoTotemDTO = totemDAO
				.getAgendamentosPorIdPaciente(inTipoPaciente, Idpaciente);

		return agendamentoTotemDTO;
	}

	public AgendamentoTotemDTO getAgendamentosPorIdAgendamento(
			Long idAgendamento) throws ParseException {

		AgendamentoTotemDTO agendamentoTotemDTO = totemDAO
				.getAgendamentosPorIdAgendamento(idAgendamento);

		return agendamentoTotemDTO;
	}

/*	public Agendamento updateStatusPresente(Long idAgendamento, boolean triado,
			boolean prioridade) {
*/
	public Agendamento updateStatusPresente(Long idAgendamento, boolean triado,
			Integer prioridade, Integer statusPrioridade, String obsUrgencia) {
		Agendamento agendamento = agendamentoService
				.getAgendamento(idAgendamento);

		if (agendamento.getBoDespesaGerada() != null
				&& !agendamento.getBoDespesaGerada()) {

			DespesaDTO despesaDTO = agendamentoService
					.validaGerarDespesa(agendamento);

			if (!(despesaDTO != null && despesaDTO.getBoQuitada() != null && despesaDTO
					.getBoQuitada().booleanValue())) {
				return null;
			} else {
				// Long totemJoaquinNabuco = new Long(4429);

				Long IdUsuarioTotem = new Long(4467);
				agendamento.setDtAtualizacaoLog(new Timestamp(new Date()
						.getTime()));
				agendamento.setIdOperadorAlteracao(IdUsuarioTotem);

				Agendamento atualizado = agendamentoService
						.updateStatusPresente(agendamento, triado, prioridade, null, null);
				return atualizado;
			}

		}

		Long IdUsuarioTotem = new Long(4467);
		agendamento.setDtAtualizacaoLog(new Timestamp(new Date().getTime()));
		agendamento.setIdOperadorAlteracao(IdUsuarioTotem);
		
	/*	agendamento.setInTipoPrioridade(prioridade);
		agendamento.setInStatusPrioridade(null);
		agendamento.setObsUrgencia(null); preciso por isso?*/
		
		Agendamento atualizado = agendamentoService.updateStatusPresente(
				idAgendamento, triado, prioridade,false, null, null);

		return atualizado;
	}

	public List<DigitalClienteDTO> getDigitalPorCpf(String cpf)
			throws ParseException {

		return totemDAO.getDigitalPorCpf(cpf);
	}

	// override para separação de funcionario e cliente
	public List<DigitalClienteDTO> getDigitalPorCpf(String cpf,
			boolean isFuncionario) throws ParseException {
		return totemDAO.getDigitalPorCpf(cpf, isFuncionario);
	}

	public List<PacienteViewDTO> pacienteByCpfCod(String cpf, String cod)
			throws ParseException {

		return totemDAO.pacienteByCpfCod(cpf, cod);
	}

	public List<PacienteViewDTO> pacienteByIdPaciente(String nrUniqueId)
			throws ParseException {

		String inTipoPaciente = nrUniqueId.substring(0, 1);

		String Idpaciente = nrUniqueId.substring(1, nrUniqueId.length());

		return totemDAO.pacienteByIdPaciente(Idpaciente, inTipoPaciente);
	}

	public List<AgendamentoTotemDTO> agendamentoByPaciente(List<String> ids)
			throws ParseException {

		return totemDAO.agendamentoByPaciente(ids);
	}

	public RegistroPonto saveRegistroPonto(Long idFuncionario, Long idUnidade) {

		DateFormat ft = new SimpleDateFormat("HH:mm:ss");

		Funcionario funcionario = funcionarioDAO.getFuncionario(idFuncionario);
		RegistroPonto regPonto = registroPontoService
				.getUltimoRegistroPontoFuncionarioDia(idFuncionario, idUnidade);

		if (regPonto != null && funcionario.getHrToleranciaPonto() != null) {
			String[] timeSplit = ft.format(funcionario.getHrToleranciaPonto())
					.split(":");

			Calendar cUltimahrAtual = Calendar.getInstance();
			cUltimahrAtual.add(Calendar.HOUR_OF_DAY,
					-Integer.valueOf(timeSplit[0]));
			cUltimahrAtual.add(Calendar.MINUTE, -Integer.valueOf(timeSplit[1]));
			cUltimahrAtual.add(Calendar.SECOND, 0);
			cUltimahrAtual.add(Calendar.MILLISECOND, 0);

			String[] timeSplit2 = ft.format(regPonto.getHrRegistro())
					.split(":");

			Calendar cUltimoRegistro = Calendar.getInstance();
			cUltimoRegistro.set(Calendar.HOUR_OF_DAY,
					Integer.valueOf(timeSplit2[0]));
			cUltimoRegistro
					.set(Calendar.MINUTE, Integer.valueOf(timeSplit2[1]));
			cUltimoRegistro.set(Calendar.SECOND, 0);
			cUltimoRegistro.set(Calendar.MILLISECOND, 0);

			if (cUltimahrAtual.getTime().after(cUltimoRegistro.getTime()))
				return registroPontoService.saveRegistroPonto(idFuncionario,
						idUnidade);
			else
				return null;
		} else
			return registroPontoService.saveRegistroPonto(idFuncionario,
					idUnidade);
	}

	public RegistroPonto saveRegistroPonto(Long idFuncionario, Double latitude,
			Double longitude) {

		Unidade unidade = totemDAO.getUnidadeByLatitudeAndLongitude(latitude,
				longitude);

		if (unidade != null && unidade.getId() != null) {

			DateFormat ft = new SimpleDateFormat("HH:mm:ss");
			Funcionario funcionario = funcionarioDAO
					.getFuncionario(idFuncionario);
			RegistroPonto regPonto = registroPontoService
					.getUltimoRegistroPontoFuncionarioDia(idFuncionario,
							unidade.getId());

			if (regPonto != null && funcionario.getHrToleranciaPonto() != null) {
				String[] timeSplit = ft.format(
						funcionario.getHrToleranciaPonto()).split(":");

				Calendar cUltimahrAtual = Calendar.getInstance();
				cUltimahrAtual.add(Calendar.HOUR_OF_DAY,
						-Integer.valueOf(timeSplit[0]));
				cUltimahrAtual.add(Calendar.MINUTE,
						-Integer.valueOf(timeSplit[1]));
				cUltimahrAtual.add(Calendar.SECOND, 0);
				cUltimahrAtual.add(Calendar.MILLISECOND, 0);

				String[] timeSplit2 = ft.format(regPonto.getHrRegistro())
						.split(":");

				Calendar cUltimoRegistro = Calendar.getInstance();
				cUltimoRegistro.set(Calendar.HOUR_OF_DAY,
						Integer.valueOf(timeSplit2[0]));
				cUltimoRegistro.set(Calendar.MINUTE,
						Integer.valueOf(timeSplit2[1]));
				cUltimoRegistro.set(Calendar.SECOND, 0);
				cUltimoRegistro.set(Calendar.MILLISECOND, 0);

				if (cUltimahrAtual.getTime().after(cUltimoRegistro.getTime()))
					return registroPontoService.saveRegistroPonto(
							idFuncionario, unidade.getId());
				else
					return null;
			} else
				return registroPontoService.saveRegistroPonto(idFuncionario,
						unidade.getId());

		}
		return null;
	}

	public boolean updateFotoPaciente(List<String> nrUniqueIds,
			Foto fotoPaciente) {
		boolean atualizacaoValida = false;
		try {
			for (String nrUniqueId : nrUniqueIds) {

				String inTipoPaciente = nrUniqueId.substring(0, 1);

				Long idPaciente = Long.parseLong(nrUniqueId.substring(1,
						nrUniqueId.length()));

				if (inTipoPaciente.equals("1")) {// cliente

					Cliente cliente = clienteDAO.searchByKey(Cliente.class,
							idPaciente);
					cliente.setFotoCliente(fotoPaciente.getFotoEncoded()
							.getBytes("ISO-8859-1"));
					clienteDAO.update(cliente);
					atualizacaoValida = true;
				} else {// dependente
					Dependente dependente = dependenteDAO.searchByKey(
							Dependente.class, idPaciente);
					dependente.setFotoDependente(fotoPaciente.getFotoEncoded()
							.getBytes("ISO-8859-1"));
					dependenteDAO.update(dependente);
					atualizacaoValida = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return atualizacaoValida;
	}

	public List<DigitalClienteDTO> getDigitalAll(TipoPessoa tipoPessoa) {

		return totemDAO.getDigitalAll(tipoPessoa);
	}

	public List<JSONObject> getFuncionarioByCelular(String nrCelular,
			String imeiCelular) {
		List<JSONObject> responses = new ArrayList<JSONObject>();
		List<FuncionarioDTO> listFuncionario = totemDAO
				.getFuncionarioByCelular(nrCelular);

		if (listFuncionario != null && listFuncionario.size() > 0) {
			for (FuncionarioDTO funcionarioDTO : listFuncionario) {
				if (funcionarioDTO.getImeiCelular() != null) {
					if (funcionarioDTO.getImeiCelular().equals(imeiCelular)) {
						JSONObject response = new JSONObject();
						response.put("status", 200);
						response.put("mensagem", "Funcionário encontrado");
						response.put("data", new JSONObject(funcionarioDTO));
						responses.add(response);
					} else {
						JSONObject response = new JSONObject();
						response.put("status", 300);
						response.put("mensagem",
								"Imei cadastrado não compatível");
						responses.add(response);
					}

				} else {
					Funcionario funcionario = funcionarioDAO.searchByKey(
							Funcionario.class,
							funcionarioDTO.getIdFuncionario());
					funcionario.setImeiCelular(imeiCelular);
					funcionarioDTO.setImeiCelular(imeiCelular);
					funcionarioDAO.update(funcionario);
					JSONObject response = new JSONObject();
					response.put("status", 200);
					response.put("mensagem", "Funcionário encontrado");
					response.put("data", new JSONObject(funcionarioDTO));
					responses.add(response);
				}
			}

		} else {
			JSONObject response = new JSONObject();
			response.put("status", 404);
			response.put("mensagem", "Funcionário não encontrado");
			responses.add(response);

		}

		return responses;

	}
	
	public List<PacienteViewDTO> getDependentesFromIdsTitulares(List<Long> idsTitulares) {
		return totemDAO.getDependentesByIdsTitular(idsTitulares);
	}
}
