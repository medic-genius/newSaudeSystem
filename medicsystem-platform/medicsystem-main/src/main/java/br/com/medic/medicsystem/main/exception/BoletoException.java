package br.com.medic.medicsystem.main.exception;

import br.com.medic.medicsystem.main.mapper.ErrorType;

public class BoletoException extends GeneralException{

	public BoletoException(String message, ErrorType errorType) {
		super(message, errorType);
	}

	private static final long serialVersionUID = 1L;

}
