package br.com.medic.medicsystem.main.rest;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import br.com.medic.medicsystem.main.service.CobrancaFinanceiroService;
import br.com.medic.medicsystem.main.service.CobrancaService;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteContatoCobrancaDTO;
import br.com.medic.medicsystem.persistence.dto.ClientesPaginadosDTO;
import br.com.medic.medicsystem.persistence.dto.MensalidadeDTO;
import br.com.medic.medicsystem.persistence.dto.TipoContatoDTO;
import br.com.medic.medicsystem.persistence.dto.TitulosAbertosClienteDTO;
import br.com.medic.medicsystem.persistence.model.CobrancaFinanceiroView;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.Parcela;

@Path("/cobranca")
@RequestScoped
public class CobrancaController extends BaseController{

	@Inject
	CobrancaService<?> cobrancaService;
	
	@Inject
	CobrancaFinanceiroService cobrancaFinanceiro;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/clientes")
	public ClientesPaginadosDTO getClientesInadimplentes( @QueryParam("boAdimplente") Boolean boAdimpente) {

		ClientesPaginadosDTO dto = cobrancaService.getClientesInadimplentes(
		        getQueryParam("nrIdEmpresa", Long.class),
		        getQueryParam("nrIdFormaPagamento", Long.class),
		        getQueryParam("nrOpcaoData", Integer.class),
		        getQueryParam("nmCliente", String.class),
		        getQueryParam("dataInicio", String.class),
		        getQueryParam("dataFim", String.class),
		        getQueryParam("pagina", Integer.class),
		        getQueryParam("itensPagina", Integer.class),
		        boAdimpente
				);

		return dto;
	}
		

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/clientes/mensalidades")
	public List<MensalidadeDTO> getMensalidadesByClienteInadimplente ( @QueryParam("nrIdEmpresaFinanceiro") Long idEmpresa, 
			@QueryParam("idCliente") Long idCliente, @QueryParam("dataInicio") String dtInicio, @QueryParam("dataFim") String dtFim){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresa);
				
		return cobrancaService.getMensalidadesByClienteInadimplente(empresa, idCliente, dtInicio, dtFim);			
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/clientes/mensalidadesperiodo")
	public List<MensalidadeDTO> getMensalidadesByClienteInadimplentePorPeriodo ( @QueryParam("nrIdEmpresaFinanceiro") Long idEmpresa, 
			@QueryParam("idCliente") Long idCliente, @QueryParam("dataInicio") String dtInicio, @QueryParam("dataFim") String dtFim){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresa);
				
		return cobrancaService.getMensalidadesByClienteInadimplentePorPeriodo(empresa, idCliente, dtInicio, dtFim);			
	}
	
	
	@GET
	@Produces("application/vnd.ms-excel")
	@Path("/clientes/xls")
	public Response  getClientesInadimplentesXLS( @QueryParam("boAdimplente") Boolean boAdimpente) {

		File file = cobrancaService.getClientesInadimplentesXLS(
		        getQueryParam("nrIdEmpresa", Long.class),
		        getQueryParam("nrIdFormaPagamento", Long.class),
		        getQueryParam("nrOpcaoData", Integer.class),
		        getQueryParam("nmCliente", String.class),
		        getQueryParam("dataInicio", String.class),
		        getQueryParam("dataFim", String.class),
		        getQueryParam("pagina", Integer.class),
		        getQueryParam("itensPagina", Integer.class),
		        boAdimpente
				);
		
		  ResponseBuilder response = Response.ok((Object) file);
	      response.header("Content-Disposition", "attachment; filename=relatorio-inadimplentes.xls");
	      return response.build();

	}
	
	@POST
	@Path("/contato/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveContato(@PathParam("id") Long idCliente, @Valid ClienteContatoCobrancaDTO contatoDTO) {
		
		contatoDTO = cobrancaService.saveContatoCobrancaCliente(contatoDTO, idCliente);
		return Response.created(URI.create("/cobranca/contatos/" + contatoDTO.getIdSac()))
		        .build();
	}

	@GET
	@Path("/contato/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ClienteContatoCobrancaDTO getClienteContatoCobrancaDTOByIdSac(@PathParam("id") Long idSac) {
		ClienteContatoCobrancaDTO contato = cobrancaService.getClienteContatoCobrancaByIdSac(idSac);
		return contato;
	}
	
	@GET
	@Path("/contatos/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClienteContatoCobrancaDTO> getClienteContatoList(@PathParam("id") Long idCliente) {
		List<ClienteContatoCobrancaDTO> list = cobrancaService.getClientesContatoCobrancaList(idCliente);
		return list;
	}
	
	@GET
	@Path("/contato/aberto/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ClienteContatoCobrancaDTO getSacCobrancaDTOEmAbertoByIdCliente(@PathParam("id") Long idCliente) {
		ClienteContatoCobrancaDTO contato = cobrancaService.getContatoCobrancaEmAbertoByIdCliente(idCliente);
		return contato;
	}

	@POST
	@Path("/contato/finalizar/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response finalizarContatoCobrancaByIdSac(@PathParam("id") Long idSac) {
		cobrancaService.finalizarCobranca(idSac, getQueryParam("duracaoContato", String.class));
		return Response.ok().build();
	}
	
	@GET
	@Path("/contatosoptions")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TipoContatoDTO> getContatosOptions() { 
		List<TipoContatoDTO> list = cobrancaService.getTipoContatoOptions();
		return list;
	}
	
	@GET
	@Path("/situacaofinanceira/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CobrancaFinanceiroView> getNegociacaoViewList(@PathParam("id") Long idCliente) {
		List<CobrancaFinanceiroView> list = cobrancaFinanceiro.getNegociacaoViewListByIdCliente(idCliente);
		return list;
	}
	
	@GET
	@Path("/situacaofinanceira/dependentes/{cliente:[0-9][0-9]*}/{contrato:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseDTO> getDependentesClienteByContrato(@PathParam("cliente") Long idCliente, @PathParam("contrato") Long idContrato) {
		List<BaseDTO> list = cobrancaFinanceiro.getDependentesClienteByIdContrato(idCliente, idContrato);
		return list;
	}
	
	@GET
	@Path("/situacaofinanceira/titulos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TitulosAbertosClienteDTO> getTitulosEmAbertoByContrato() {
		
		List<TitulosAbertosClienteDTO> list = cobrancaFinanceiro.getTitulosEmAbertoDTO(getQueryParam("nrContrato", String.class));
		return list;
	}
	
	@GET
	@Path("/quantidadefuncionario")
	@Produces(MediaType.APPLICATION_JSON)
	public BaseDTO getQuantidadeCobrancaByFuncionario() {
		
		Long quantidade = cobrancaService.getQuantidadeCobrancaByFuncionario(getQueryParam("login", String.class),
		        getQueryParam("dataInicio", String.class), getQueryParam("dataFim", String.class));
		
		BaseDTO dto = new BaseDTO(quantidade);
		return dto;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/compartilhado/inadimplentes")
	public ClientesPaginadosDTO getClientesInadimplentesPorSistema( @QueryParam("boAdimplente") Boolean boAdimpente) {
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(  getQueryParam("nrIdEmpresaFinanceiro", Long.class));
		
		ClientesPaginadosDTO dto = cobrancaService.getClientesInadimplentesPorSistema(
				empresa,
		        getQueryParam("nrIdFormaPagamento", Long.class),
		        getQueryParam("nrOpcaoData", Integer.class),
		        getQueryParam("nmCliente", String.class),
		        getQueryParam("dataInicio", String.class),
		        getQueryParam("dataFim", String.class),
		        getQueryParam("pagina", Integer.class),
		        getQueryParam("itensPagina", Integer.class),
		        boAdimpente
				);

		return dto;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/mensalidade/{id:[0-9][0-9]*}/cobranca")
	public Collection<Parcela> getMensalidadesByClienteInadimplente (@PathParam("id") Long idMensalidade){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(  getQueryParam("nrIdEmpresaFinanceiro", Long.class));
		
				
		return cobrancaService.getCobrancaByMensalidade(empresa, idMensalidade);			
	}
	
}
