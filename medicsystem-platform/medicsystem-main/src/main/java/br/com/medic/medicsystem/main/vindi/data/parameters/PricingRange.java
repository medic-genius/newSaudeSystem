package br.com.medic.medicsystem.main.vindi.data.parameters;

public class PricingRange {
	private Integer start_quantity;
	private Integer end_quantity;
	private Number price;
	private Number overage_price;
	
	public Integer getStart_quantity() {
		return start_quantity;
	}
	public void setStart_quantity(Integer start_quantity) {
		this.start_quantity = start_quantity;
	}
	public Integer getEnd_quantity() {
		return end_quantity;
	}
	public void setEnd_quantity(Integer end_quantity) {
		this.end_quantity = end_quantity;
	}
	public Number getPrice() {
		return price;
	}
	public void setPrice(Number price) {
		this.price = price;
	}
	public Number getOverage_price() {
		return overage_price;
	}
	public void setOverage_price(Number overage_price) {
		this.overage_price = overage_price;
	}
}
