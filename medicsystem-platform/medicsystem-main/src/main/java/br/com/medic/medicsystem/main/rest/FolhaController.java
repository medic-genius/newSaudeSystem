package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.FolhaService;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.FolhaImportResult;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaDescontosTotalDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaGraficoDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaTotaisGrupoDTO;
import br.com.medic.medicsystem.persistence.dto.ImportaFolhaUnidadesDTO;
import br.com.medic.medicsystem.persistence.model.ImportaFolha;
import br.com.medic.medicsystem.persistence.model.ImportaFolhaFuncFluxo;

@Path("/folha")
@RequestScoped
public class FolhaController extends BaseController {

	@Inject
	private FolhaService folhaService;

	@POST
	@Path("/converter")
	@Produces(MediaType.APPLICATION_JSON)
	public FolhaImportResult uploadFolhaRaw(@Valid String content) {
		
		return folhaService.importarFolha(content);
	}

	@POST
	@Path("/salvar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ImportaFolha uploadFolhaRaw(@Valid FolhaImportResult content) {
		ImportaFolha oif = folhaService.salvarFolha(content);
		return oif;
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolha getImportaFolha(@PathParam("id") Long id) {
		final ImportaFolha oif = folhaService.getImportaFolha(id);

		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return oif;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}/func/{idfunc:[0-9][0-9]*}/aprova")
	@Produces(MediaType.APPLICATION_JSON)
	public Response aprovarReprovaFolhaFuncionario(@PathParam("id") Long id, @PathParam("idfunc") Long idfunc) {
		folhaService.aprovaFolhaFunc(idfunc);

		return Response.ok().build();
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/func/{idfunc:[0-9][0-9]*}/reprova")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reprovarReprovaFolhaFuncionario(@PathParam("id") Long id, @PathParam("idfunc") Long idfunc, String comentario) {
		folhaService.reprovaFolhaFunc(idfunc, comentario);

		return Response.ok().build();
	}

	@GET
	@Path("/widget/empresa/{idempresafinanceiro:[0-9][0-9]*}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolhaUnidadesDTO getImportaFolhaEmpresaPorMes( @PathParam("idempresafinanceiro") Long idEmpresaFinanceiro ,@PathParam("data") String data) {
		final ImportaFolhaUnidadesDTO oif = folhaService.getImportaFolhaPorMes(idEmpresaFinanceiro,data);
		
		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return oif;
	}
		
	@GET
	@Path("/widget/dozemeses/empresa/{idempresafinanceiro:[0-9][0-9]*}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ImportaFolhaGraficoDTO> getImportaFolhaEmpresaUltimos12Meses(@PathParam("idempresafinanceiro") Long idEmpresaFinanceiro,@PathParam("data") String data) {
		final List<ImportaFolhaGraficoDTO> oif = folhaService.getImportaFolhaUltimos12Meses( idEmpresaFinanceiro ,data);

		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return oif;
	}
	
	@GET
	@Path("/dadosvalidacao/empresa/{idempresafinanceiro:[0-9][0-9]*}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolhaUnidadesDTO getMultipleImportaFolhaEmpresaPorMes(@PathParam("idempresafinanceiro") Long idEmpresaFinanceiro,@PathParam("data") String data) {
		final ImportaFolhaUnidadesDTO oif = folhaService.getMultipleImportaFolhaDTO( idEmpresaFinanceiro,data);

		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return oif;
	}

	@GET
	@Path("/dto/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolhaDTO getImportaFolhaDTO(@PathParam("id") Long id) {
		final ImportaFolhaDTO oif = folhaService.getImportaFolhaDTO(id);

		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return oif;
	}
	
	@GET
	@Path("/widget/descontostotais/empresa/{idempresafinanceiro:[0-9][0-9]*}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ImportaFolhaDescontosTotalDTO> getDescontosTotais( @PathParam("idempresafinanceiro") Long idEmpresaFinanceiro, @PathParam("data") String data ) {	
		final List<ImportaFolhaDescontosTotalDTO> descontosTotais = 
				folhaService.getDescontosTotais( idEmpresaFinanceiro, data);
		
		if (descontosTotais == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return descontosTotais;
		
	}
	
	@GET
	@Path("/fluxos/{idempresafinanceiro}/funcionario/{codigo}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ImportaFolhaFuncFluxo> getFluxosFuncionarioByDate( @PathParam("codigo") String codigo ,@PathParam("idempresafinanceiro") Long idEmpresaFinanceiro, @PathParam("data") String data,  @QueryParam("isFolhaAvulsa") Boolean isFolhaAvulsa  ) {
		final List<ImportaFolhaFuncFluxo> fluxos =  folhaService.getFluxosFuncionarioByDate( codigo, idEmpresaFinanceiro, data, isFolhaAvulsa  );
		
		if (fluxos == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return fluxos;
		
	}
	
	@GET
	@Path("/grupo/{intipogrupo:[0-9][0-9]*}/{data : .+}/totais")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolhaTotaisGrupoDTO getTotaisGrupoByPeriodo( @PathParam("intipogrupo") Integer inTipoGrupo, @PathParam("data") String data ) {	
		final ImportaFolhaTotaisGrupoDTO totaisGrupo = 
				folhaService.getTotaisGrupoByPeriodo( inTipoGrupo, data);
		
		if (totaisGrupo == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return totaisGrupo;
		
	}
	
	@GET
	@Path("/widget/dozemeses/grupo/{idGrupoFinanceiro:[0-9][0-9]*}/{data : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ImportaFolhaGraficoDTO> getImportaGrupoUltimos12Meses(@PathParam("idGrupoFinanceiro") Long idEmpresaFinanceiro,@PathParam("data") String data) {
		final List<ImportaFolhaGraficoDTO> oif = folhaService.getImportaGrupoUltimos12Meses( idEmpresaFinanceiro ,data);

		if (oif == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return oif;
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/acrescimoFolha")
	@Produces(MediaType.APPLICATION_JSON)
	public ImportaFolha acrescimoSalarioFolhaFuncionario(@PathParam("id") Long id, List<BaseDTO> funcModificados) {
		ImportaFolha folha = folhaService.acrescimoSalarioFolhaFuncionario(id , funcModificados);
		
		return folha;
	}
	
	@PUT
	@Path("/verificar/duplicados")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<BaseDTO> verificarFuncionarioMesmoCodigo(@Valid FolhaImportResult content) {
		 List<BaseDTO> list = folhaService.verificarFuncionarioMesmoCodigo( content );
		return list;
	}

	
}