package br.com.medic.dashboard.main.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.dashboard.main.model.Report;
import br.com.medic.dashboard.persistence.dao.academia.APReportClienteDAO;
import br.com.medic.dashboard.persistence.dao.academia.APReportFaturamentoDAO;
import br.com.medic.dashboard.persistence.dao.academia.APReportPlanoDAO;
import br.com.medic.dashboard.persistence.model.ClientePlano;
import br.com.medic.dashboard.persistence.model.FaturamentoMesAMes;
import br.com.medic.dashboard.persistence.model.FaturamentoMovimentacaoContratos;
import br.com.medic.dashboard.persistence.model.FaturamentoPlano;
import br.com.medic.dashboard.persistence.model.FaturamentoTipoPagamento;
import br.com.medic.dashboard.persistence.model.MovimentacaoCliente;

@ApplicationScoped
public class APReportService {

	@Inject
	private Logger logger;

	@Inject
	@Named("ap-report-faturamento-dao")
	private APReportFaturamentoDAO reportFaturamentoDAO;

	@Inject
	@Named("ap-report-cliente-dao")
	private APReportClienteDAO reportClienteDAO;

	@Inject
	@Named("ap-report-plano-dao")
	private APReportPlanoDAO reportPlanoDAO;
	
	public void setReportFaturamentoDAO(
			APReportFaturamentoDAO reportFaturamentoDAO) {
		this.reportFaturamentoDAO = reportFaturamentoDAO;
	}

		
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoPorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
		
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoUnidadePorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento Unidade por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoUnidadePorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		//relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
		
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoUnidadeDetailPorTipoPagamento(
	        String ano, String mes, Long idUnidade) {

		String nomeRelatorio = "Faturamento Unidade Detail por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoUnidadeDetailPorTipoPagamento(ano, mes, idUnidade));
		
		relatorio.wrapGraphData();
		
		//relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
		
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPagoPorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento Pago por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoPagoPorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
				
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoUnidadePagoPorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento Unidade Pago por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoUnidadePagoPorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		//relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
				
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoUnidadeDetailPagoPorTipoPagamento(
	        String ano, String mes, Long idUnidade) {

		String nomeRelatorio = "Faturamento Unidade Detail Pago por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoUnidadeDetailPagoPorTipoPagamento(ano, mes, idUnidade));
		
		relatorio.wrapGraphData();
		
		//relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
				
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratos(
	        String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Contratos/Faturamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoMovimentacaoContratos>> relatorio = new Report<List<FaturamentoMovimentacaoContratos>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoMovimentacaoContratos(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratosDetalhado(
	        String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Contratos/Faturamento detalhado";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoMovimentacaoContratos>> relatorio = new Report<List<FaturamentoMovimentacaoContratos>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoMovimentacaoContratosDetalhado(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public List<FaturamentoMesAMes> getReportFaturamentoMesAMes() {
		logger.info("Gerando relatorio: Faturamento Mes a Mes");
		
		Calendar cal = Calendar.getInstance();		
		cal.add(Calendar.MONTH, -13);		
		cal.set(Calendar.DAY_OF_MONTH, 1);		
		Date start = cal.getTime(); //first day of the year
		Date end = Calendar.getInstance().getTime(); //actual day of the month
//		System.out.println(start);
//		System.out.println(end);
		
		List<FaturamentoMesAMes> relatorio = reportFaturamentoDAO.getReportFaturamentoMesAMes(start, end);
		logger.info("Relatorio gerado: Faturamento Mes a Mes");
		
		return relatorio;
	}

	public Report<List<MovimentacaoCliente>> getReportClienteNovos(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Clientes Novos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClienteNovos(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportClienteAtivos() {

		String nomeRelatorio = "Clientes Ativos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClienteAtivos());
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportClientesFaturamento(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Clientes Faturamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClientesFaturamento(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}

	public Report<List<FaturamentoPlano>> getReportFaturamentoPlanos() {

		String nomeRelatorio = "Faturamento por Planos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoPlano>> relatorio = new Report<List<FaturamentoPlano>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportPlanoDAO.getReportFaturamentoPlanos());
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public List<ClientePlano> getReportListaClientesPorPlano(Integer idPlano, Integer inFormaPagamento) {

		logger.info("Gerando relatorio...");
		List<ClientePlano> list = reportPlanoDAO.getReportListaClientesPorPlano(idPlano, inFormaPagamento);
		logger.info("Relatorio Gerado...");
		return list;
	}
}
