package br.com.medic.medicsystem.main.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaClienteDAO;
import br.com.medic.medicsystem.persistence.dao.GuiaDAO;
import br.com.medic.medicsystem.persistence.dao.GuiaServicoDAO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class GuiaService {

	@Inject
	private SecurityService securityService;

	@Inject
	@Named("empresaCliente-dao")
	private EmpresaClienteDAO empresaClienteDAO;

	@Inject
	@Named("guia-dao")
	private GuiaDAO guiaDAO;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
	
	@Inject
	@Named("guiaServico-dao")
	private GuiaServicoDAO guiaServicoDAO;

	public Guia saveGuia(Guia guia) {
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, guia.getCliente().getId());
		List<Long> ids = new LinkedList<Long>();
		if(guia.getDependente() != null)
		{
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, guia.getDependente().getId());
			guia.setDependente(dependente);
		}
		Contrato contrato = this.getContetoByIdCliente(cliente.getId());
		guia.setCliente(cliente);
		guia.setContrato(contrato);
		Calendar cal = Calendar.getInstance();
		String user = securityService.getUserLogin();
		EmpresaCliente empresaCliente = empresaClienteDAO.getEmpresaCliente(user);
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.DATE, empresaCliente.getValidadeGuia() != null ? empresaCliente.getValidadeGuia() : 0);
		guia.setDtInicio(cal.getTime());
		guia.setDtValidade(calendarEnd.getTime());
		List<GuiaServico> dsList = new ArrayList<GuiaServico>();
		dsList.addAll(guia.getGuiaServicos());
		guia.getGuiaServicos().clear();

		for (GuiaServico guiaServico : dsList)
		{
			ids.add(guiaServico.getServico().getId());
		}
		List<Servico> lGuiasPersisteds = guiaDAO.getListServicoByIds(ids);
		GuiaServico guiaServico;
		for (Servico servicoPersisted : lGuiasPersisteds)
		{
			guiaServico = new GuiaServico();
			guiaServico.setGuia(guia);
			guiaServico.setServico(servicoPersisted);
			guiaServico.setStatus(0);
			guia.addGuiaServico(guiaServico);
		}
		guia = guiaDAO.persist(guia);
		return guia;
	}

	public Contrato getContetoByIdCliente(Long idCliente){
		String user = securityService.getUserLogin();
		EmpresaCliente empre = empresaClienteDAO.getEmpresaCliente(user);
		Contrato contratoClienteEmpresa = empresaClienteDAO.getContratoEmpresaClienteByCliente(idCliente, empre.getId());
		return contratoClienteEmpresa;
	}

	public GenericPaginateDTO getGuias(int offset, int limit, Long id, String nmCliente) {
		if(limit > 25)
		{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		GenericPaginateDTO guiaDTO = new GenericPaginateDTO();
		guiaDTO.setOffset(offset);
		guiaDTO.setLimit(limit);
		guiaDTO.setTotal(guiaDAO.getTotal());
		List<?> lGuia = guiaDAO.getListGuia(offset, limit, id, nmCliente);
		guiaDTO.setData(lGuia);
		return guiaDTO;
	}

	public Guia getGuia(Long id){
		return guiaDAO.getGuia(id);
	}

	public GenericPaginateDTO getGuiasByCliente(int offset, int limit, Long idCliente) {
		if(limit > 25)
		{
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("Limit per page can't be bigger 25!").build());
		}
		GenericPaginateDTO paginator = new GenericPaginateDTO();
		paginator.setOffset(offset);
		paginator.setLimit(limit);
		paginator = guiaDAO.getListByCliente(paginator, idCliente);
		paginator.prepare();
		return paginator;
	}
	
	public GuiaServico getGuiaValidabyClienteDependenteServico(Agendamento agendamento){
					
		return guiaDAO.getGuiaValidabyClienteDependenteServico(agendamento);
	}
	
	public GuiaServico getGuiaValidabyClienteDependenteServico(DespesaServico despesaServico){
		
		return guiaDAO.getGuiaValidabyClienteDependenteServico(despesaServico);
	}
	
	public GuiaServico updateGuiaServico(GuiaServico guiaServico){
		
		return guiaServicoDAO.update(guiaServico);
	}
	

	public Guia deleteGuia(Long id) {
		Guia guia = guiaDAO.searchByKey(Guia.class, id);
		if(null != guia)
		{
			if(this.getEmpresaCliente().getId() == guia.getContrato().getEmpresaCliente().getId())
			{
				if(guia.getDtExclusao() == null)
				{
					guia.setDtExclusao(new Timestamp(System.currentTimeMillis()));
					this.deleteItensGuiaChildren(guia);
					return guia;
				} else
				{
					throw new RuntimeException("This record deleted!");
				}
			} else
			{
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		return null;
	}

	public EmpresaCliente getEmpresaCliente(){
		String user = securityService.getUserLogin();
		EmpresaCliente empresaCliente = empresaClienteDAO.getEmpresaCliente(user);
		return empresaCliente;
	}
	
	private void deleteItensGuiaChildren(Guia guia) {
		for(GuiaServico guiaServico : guia.getGuiaServicos())
		{
			if(guiaServico.getStatus() == 0)
			{
				guiaServico.setStatus(4);
				guiaServico.setDtExclusao(new Timestamp(System.currentTimeMillis()));
			}
		}
	}

	public Guia reactive(Long id) {
		Guia guia = guiaDAO.searchByKey(Guia.class, id);
		EmpresaCliente empresaCliente = this.getEmpresaCliente();
		if(guia == null) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		if(empresaCliente.getId() != guia.getContrato().getEmpresaCliente().getId()) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
		Guia newGuia = new Guia();
		newGuia.setDependente(guia.getDependente());
		newGuia.setCliente(guia.getCliente());
		newGuia.setContrato(guia.getContrato());
		Calendar cal = Calendar.getInstance();
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.DATE, empresaCliente.getValidadeGuia() != null ? empresaCliente.getValidadeGuia() : 0);
		newGuia.setDtInicio(cal.getTime());
		newGuia.setDtValidade(calendarEnd.getTime());
		List<GuiaServico> lGuiaServicos = guia.getGuiaServicos();
		GuiaServico newGuiaServico;
		for (GuiaServico guiaServico: lGuiaServicos) {
			if(guiaServico.getStatus() == 5) {
				newGuiaServico = new GuiaServico();
				newGuiaServico.setGuia(newGuia);
				newGuiaServico.setServico(guiaServico.getServico());
				newGuiaServico.setStatus(0);
				newGuia.addGuiaServico(newGuiaServico);
			}
		}
		if(newGuia.getGuiaServicos().isEmpty()) {
			throw new WebApplicationException("Não há itens");
		}
		newGuia = guiaDAO.persist(newGuia);
		return newGuia;
	}
}
