package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.PathParam;

import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dto.CalAgendaMedicoDTO;
import br.com.medic.medicsystem.persistence.dto.CalEspecialidadeDTO;





@Stateless
public class CalendarioService {
	

@Inject
@Named("especialidade-dao")
private EspecialidadeDAO especialidadeDAO;

@Inject
@Named("agendamento-dao")
private AgendamentoDAO agendamentoDAO;
	
	public List<CalEspecialidadeDTO> getCalEspecialidadeDTO() {
		
		return  especialidadeDAO.getCalEspecialidadeDTO();
		
	}
	
	
	public List <CalAgendaMedicoDTO> getAgendaMedico(List<Long> ids, String dtInicio, String dtFim, List<Long> idsUnidade,List<Long> idsFuncionario){
		
		return  agendamentoDAO.getAgendaMedico(ids, dtInicio, dtFim, idsUnidade,idsFuncionario);
	}
	
	
	

}
