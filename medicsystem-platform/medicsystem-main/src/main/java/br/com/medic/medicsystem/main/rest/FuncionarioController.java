package br.com.medic.medicsystem.main.rest;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.service.FuncionarioService;
import br.com.medic.medicsystem.main.service.ServicoService;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dto.ArquivosFuncionarioDTO;
import br.com.medic.medicsystem.persistence.dto.AtendimentoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.AtividadeAgendamentoDTO;
import br.com.medic.medicsystem.persistence.dto.EspecialidadeDTO;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.RegistroChamadaProfissional;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;
import br.com.medic.medicsystem.persistence.model.enums.GrupoFuncionario;
import br.com.medic.medicsystem.persistence.model.views.ServicoExame;

/**
 * @author Phillip
 * @since 01-01-2016
 * @version 1.3
 * 
 * last modification: 09/05/2016
 * by: Joelton Matos
 */

@Path("/funcionarios")
@RequestScoped
public class FuncionarioController extends BaseController {

	@Inject
	private ServicoService servicoService;

	@Inject
	private FuncionarioService funcionarioService;

	@Context
	protected UriInfo info;
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	

	@GET
	@Path("medicos/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario getMedico(@PathParam("id") Long id) {
		final Funcionario medico = funcionarioService.getMedico(id);

		if (medico == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return medico;
	}

	@GET
	@Path("/medicos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Funcionario> getMedicos() {
		return funcionarioService.getMedicos(getQueryParam("nmMedico", String.class));
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/servicos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServicoProfissional> getServicosPorProfissional(@PathParam("id") Long idProfissional) {
		return servicoService.getServicosPorProfissional(idProfissional);
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/calendarioSemanal")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentoProfissionalDTO> getCalendarioSemanalProfissional(
	        @PathParam("id") Long idProfissional,
	        @QueryParam("idServico") Long idServico,
	        @QueryParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("idSubEspecialidade") Long idSubEspecialidade,
	        @QueryParam("inTipoConsulta") Integer inTipoConsulta,
	        @QueryParam("idUnidade") Long idUnidade) {
		if (idProfissional != null) {

			return funcionarioService.getCalendarioSemanalProfissional(
			        idProfissional, idServico, idEspecialidade, idUnidade,
			        idSubEspecialidade, inTipoConsulta);
		} else {

			throw new IllegalArgumentException();
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/horarios")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHorarioAtendimentoProfissional(
	        @NotNull @PathParam("id") Long idProfissional,
	        @QueryParam("idServico") Long idServico,
	        @NotNull @QueryParam("data") String date,
	        @NotNull @QueryParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("idSubEspecialidade") Long idSubEspecialidade,
	        @NotNull @QueryParam("inTipoConsulta") Integer inTipoConsulta,
	        @NotNull @QueryParam("inTipoAgendamento") Integer inTipoAgendamento,
	        @NotNull @QueryParam("idUnidade") Long idUnidade) {

		return Response
		        .status(Status.OK)
		        .entity(funcionarioService.getHorarioAtendimentoProfissional(
		                idProfissional, idServico, idEspecialidade, idUnidade,
		                idSubEspecialidade, inTipoConsulta, inTipoAgendamento,
		                date, null, null)).build();

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/horarios/naoAtende")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getDiasSemAtendimentoProfissional(
	        @PathParam("id") Long idProfissional,
	        @QueryParam("idServico") Long idServico,
	        @QueryParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("inTipo") Integer inTipo,
	        @QueryParam("idSubEspecialidade") Long idSubEspecialidade,
	        @QueryParam("inTipoConsulta") Integer inTipoConsulta,
	        @QueryParam("idUnidade") Long idUnidade,
	        @QueryParam("ano") Integer ano, @QueryParam("mes") Integer mes,
	        @QueryParam("idPaciente") Long idPaciente,
	        @QueryParam("tipoPaciente") String tipoPaciente) {
		if (idProfissional != null && inTipoConsulta != null
		        && idEspecialidade != null) {

			return funcionarioService.getDiasSemAtendimentoProfissional(
			        idProfissional, idServico,idEspecialidade, idUnidade,
			        idSubEspecialidade, inTipoConsulta, ano, mes,inTipo, idPaciente, tipoPaciente);
		} else {

			throw new IllegalArgumentException();
		}

	}
	@GET
	@Path("/medicos/unidade/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Profissional> getProfissionaisPorUnidade (@PathParam("id") Long idUnidade) {
		Collection<Profissional> profissionais = funcionarioService
		        .getProfissionaisPorUnidade(idUnidade);
	
		if (profissionais.isEmpty() || profissionais == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return profissionais;
		}
	}
	
	@POST
	@Path("/{idfuncionario:[0-9][0-9]*}/upload")
	@Consumes("multipart/form-data")
	public Response importaAnexosFuncionario( MultipartFormDataInput input , @PathParam("idfuncionario") Long idFuncionario ) {
		funcionarioService.importaArquivosFuncionario( input, idFuncionario );
				
		return Response.ok().build();
	}
	
	@GET
	@Path("/ativos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Funcionario> getListaFuncionario(
			@QueryParam("ingrupofuncionario") Integer inGrupoFuncionario) {
		return funcionarioService.getListaFuncionario(inGrupoFuncionario);
	}
	
	@GET
	@Path("/inativos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Funcionario> getListaFuncionarioInativo() {
		return funcionarioService.getListaFuncionarioInativo();
	}
	
	@PUT
	@Path("/inativar/{id:[0-9][0-9]*}")
	public Response inativarFuncionario(@PathParam("id") Long id){
		
		funcionarioService.inativarFuncionario(id);
		return Response.ok().build();
		
	}
	
	@PUT
	@Path("/ativar/{id:[0-9][0-9]*}")
	public Response ativarFuncionario(@PathParam("id") Long id){
		
		funcionarioService.ativarFuncionario(id);
		return Response.ok().build();
		
	}
	
	
	@GET
	@Path("/{idFuncionario:[0-9][0-9]*}/arquivos")
	@Produces(MediaType.APPLICATION_JSON)
	public ArquivosFuncionarioDTO getListaArquivosFuncionario( @PathParam("idFuncionario") Long idFuncionario ) {
		
		final ArquivosFuncionarioDTO arquivosFuncionario = funcionarioService.getArquivosFuncionario( idFuncionario );

		if (arquivosFuncionario == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return arquivosFuncionario;
	}
		
/** FILES TYPES **/
	
	@GET
	@Path("/{id:[0-9][0-9]*}/arquivo/imagePNG/{idArquivo:[0-9][0-9]*}")
	@Produces("image/png")
	public Response getImagePNGFuncionario( @PathParam("idArquivo") Long idArquivo ) {
		File fileEmployee = funcionarioService.getArquivoFuncionario(idArquivo);
		if (fileEmployee != null && fileEmployee.isFile()) {
			ResponseBuilder response = Response.ok((Object) fileEmployee);
			response.header("Content-Disposition", "filename=" + fileEmployee.getName());
			return response.build();
		} else {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}
		
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/arquivo/imageJPG/{idArquivo:[0-9][0-9]*}")
	@Produces("image/jpeg")
	public Response getImageJPGFuncionario( @PathParam("idArquivo") Long idArquivo ) {
		File fileEmployee = funcionarioService.getArquivoFuncionario(idArquivo);
		if (fileEmployee != null && fileEmployee.isFile()) {
			ResponseBuilder response = Response.ok((Object) fileEmployee);
			response.header("Content-Disposition", "filename=" + fileEmployee.getName());
			return response.build();
		} else {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}
		
	}	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/arquivo/pdf/{idArquivo:[0-9][0-9]*}")
	@Produces("application/pdf")
	public Response getPDFFuncionario( @PathParam("idArquivo") Long idArquivo ) {
		File fileEmployee = funcionarioService.getArquivoFuncionario(idArquivo);
		if (fileEmployee != null && fileEmployee.isFile()) {
			ResponseBuilder response = Response.ok((Object) fileEmployee);
			response.header("Content-Disposition", "filename=" + fileEmployee.getName());
			return response.build();
		} else {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}
		
	}
	
	@GET
	@Path("/{idFuncionario:[0-9][0-9]*}/especialidade")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EspecialidadeDTO> getEspecialidadesProfissional( @PathParam("idFuncionario") Long idFuncionario ) {
		
		List<EspecialidadeDTO> especialidades = funcionarioService.getEspecialidadesProfissional( idFuncionario );

		if (especialidades == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return especialidades;
	}
	
	@GET
	@Path("/{login}/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario getFuncionarioPorLogin( @PathParam("login") String loginFuncionario ) {
		
		if(loginFuncionario != null )
			return funcionarioService.getFuncionarioPorLogin(loginFuncionario);
		
		else
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
			
		
	}
	
	@GET
	@Path("/funcionario")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario getFuncionarioById (
			@QueryParam("id") Long idFuncionario) {
		
		if(idFuncionario != null )
			return funcionarioService.getFuncionarioById(idFuncionario);
		
		else
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
			
		
	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/especialidade/exames")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ServicoExame> getServicoExame( @PathParam("id") Long idEspecialidade ) {
		
		
		List<ServicoExame> resultList = funcionarioService.getServicoExame(idEspecialidade);
		
		if(resultList != null){
			
			return resultList;
		}else
			throw new WebApplicationException(Response.Status.NOT_FOUND);
			
	}
	
	@GET
	@Path("/relatorio/agendamentos/{dtInicio}/{dtFim}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtividadeAgendamentoDTO> getAtividadeAgendamento( 
			@PathParam("dtInicio")String dtInicio,
			@PathParam("dtFim")String dtFim) {
		
		
		List<AtividadeAgendamentoDTO> resultList = funcionarioService.getAtividadeAgendamento(dtInicio, dtFim);
		
		if(resultList != null){
			
			return resultList;
		}else
			return null;
			
	}
	
	@GET
	@Path("/relatoriomedicoativo")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtividadeAgendamentoDTO> getMedicosAtividadeAtendimento( 
			 @QueryParam("dataInicio") String dataInicio, 
			 @QueryParam("dataFim")	String dataFim) {
				
		List<AtividadeAgendamentoDTO> resultList = funcionarioService.getMedicosAtividadeAtendimento(dataInicio, dataFim);
		
		if(resultList != null){
			
			return resultList;
		}else
			return null;
			
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/boautorizacaototem")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario updateAutorizacaoTotem( 
			@PathParam("id") Long idFuncionario,
			@QueryParam("autorizacao") Boolean autorizacao,
			@QueryParam("digital") String digitalFuncionario,
			@QueryParam("idDedoDigital") Integer idDedoDigital) {
		
		return funcionarioService.updateAutorizacaoTotem(idFuncionario, autorizacao, digitalFuncionario, idDedoDigital);
		
			
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateFuncionario(Funcionario funcionario) {

		if (funcionario != null) {
			Funcionario funcionarioAtualizado = funcionarioService.updateFuncionario(funcionario);

			if (funcionarioAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createFuncionario(Funcionario funcionario) {
		
		Funcionario salvo = funcionarioService.saveFuncionario(funcionario);
		
		return Response.created(URI.create("/funcionarios/"+ salvo.getId())).build();
		
	}
	
	@POST
	@Path("/registrarchamada")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegistroChamadaProfissional RegistrarChamadaProfissional(
			@QueryParam("idUnidade") Long idUnidade,
			@QueryParam("idAgendamento") Long idAgendamento) {
		
		return funcionarioService.RegistrarChamadaProfissional(idUnidade, idAgendamento);
		
	}
	
	@GET
	@Path("/medicos/{id:[0-9]+}/especialidades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EspecialidadeDTO> getEspecialidadeByProfissional(@PathParam("id") Long idProfissional) {
		if(idProfissional != null) {
			return funcionarioService.getEspecialidadesByProfissional(idProfissional);
		}
		return null;
	}
	
	@GET
	@Path("/grupofuncionario")
	@Produces(MediaType.APPLICATION_JSON)
	public List<GrupoFuncionario> getGruposFuncionarios(){
		
		List<GrupoFuncionario> result = funcionarioService.getGruposFuncionarios();
		
		return result;
	}
	
	/*@GET
	@Path("/funcionario")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario getFuncionarioById (
			@QueryParam("id") Long idFuncionario) {
		
		
			
		
	}*/
	//--------------------------------------------------------------
	// APLICATIVO
	@GET
	@Path("/{id:[0-9][0-9]*}/horarios/sem_atendimento")
	@Produces(MediaType.APPLICATION_JSON)
	public AppResponse getDiasSemAtendimentoProfissional(
	        @PathParam("id") Long idProfissional,
	        @QueryParam("idServico") Long idServico,
	        @QueryParam("idEspecialidade") Long idEspecialidade,
	        @QueryParam("idSubEspecialidade") Long idSubEspecialidade,
	        @QueryParam("inTipoConsulta") Integer inTipoConsulta,
	        @QueryParam("idUnidade") Long idUnidade,
	        @QueryParam("ano") Integer ano,
	        @QueryParam("mes") Integer mes) {
		return funcionarioService.getListaDatasNaoAtende(idProfissional, 
				idServico, idEspecialidade, idSubEspecialidade, 
				inTipoConsulta, idUnidade, ano, mes);
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/assinaturadigital")
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario updateAssinaturaDigital( 
			@PathParam("id") Long idFuncionario,	
			String assinaturaDigital) {
		return funcionarioService.updateAssinaturaDigital(idFuncionario, assinaturaDigital);
	}
}