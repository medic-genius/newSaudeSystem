package br.com.medic.medicsystem.main.util;

public class SimpleKVGenericDTO<K, V, W, Z, T> {

	private T boAceitaEncaixe;
	
	private K id;

	private V data;
	
	private W boParticular;
	
	private Z idAtendimentoProfissional;

	public SimpleKVGenericDTO(K id, V data, W boParticular, Z idAtendimentoProfissional, T boAceitaEncaixe ) {
		this.id = id;
		this.data = data;
		this.boParticular = boParticular;
		this.idAtendimentoProfissional = idAtendimentoProfissional;
		this.boAceitaEncaixe = boAceitaEncaixe;
	}

	public V getData() {
		return data;
	}

	public void setData(V data) {
		this.data = data;
	}

	public K getId() {
		return id;
	}

	public void setId(K id) {
		this.id = id;
	}

	public W getBoParticular() {
		return boParticular;
	}

	public void setBoParticular(W boParticular) {
		this.boParticular = boParticular;
	}

	public Z getIdAtendimentoProfissional() {
		return idAtendimentoProfissional;
	}

	public void setIdAtendimentoProfissional(Z idAtendimentoProfissional) {
		this.idAtendimentoProfissional = idAtendimentoProfissional;
	}

	public T getBoAceitaEncaixe() {
		return boAceitaEncaixe;
	}

	public void setBoAceitaEncaixe(T boAceitaEncaixe) {
		this.boAceitaEncaixe = boAceitaEncaixe;
	}



	
	


	
	
	
	
}
