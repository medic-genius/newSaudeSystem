package br.com.medic.medicsystem.main.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import br.com.medic.medicsystem.persistence.dao.CobrancaFinanceiroViewDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaViewDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.TitulosAbertosClienteDTO;
import br.com.medic.medicsystem.persistence.model.CobrancaFinanceiroView;
import br.com.medic.medicsystem.persistence.model.CobrancaView;
import br.com.medic.medicsystem.persistence.model.Dependente;

@Stateless
public class CobrancaFinanceiroService {

	@Inject
	@Named("cobrancafinanceiroview-dao")
	private CobrancaFinanceiroViewDAO cobrancaFinanceiroViewDAO;
	
	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
	
	@Inject
	@Named("cobrancaview-dao")
	private CobrancaViewDAO cobrancaViewDAO;
	
	public List<CobrancaFinanceiroView> getNegociacaoViewListByIdCliente(Long idCliente){
		List<CobrancaFinanceiroView> negociacaoList = null;
		try{
			negociacaoList = cobrancaFinanceiroViewDAO.getNegociacaoViewListByIdCliente(idCliente);
			for (CobrancaFinanceiroView cobrancaFinanceiroView : negociacaoList) {
				cobrancaFinanceiroView.setQuantidadeDependentes((int)(long) dependenteDAO.getQuantidadeDependentesClienteByContrato(idCliente, cobrancaFinanceiroView.getIdContrato()));
			}
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return negociacaoList;
	}
	
	public List<BaseDTO> getDependentesClienteByIdContrato(Long idCliente, Long idContrato){
		List<Dependente> dependentes = dependenteDAO.getDependentesClienteByContrato(idCliente, idContrato);
		
		List<BaseDTO> dtos = new ArrayList<BaseDTO>();
		
		for (Dependente dependente : dependentes) {
			dtos.add(new BaseDTO(dependente.getId(), dependente.getNmDependente(), dependente.getParentescoFormatado()));
		}
		
		return dtos;
	}
	
	public List<TitulosAbertosClienteDTO> getTitulosEmAbertoDTO(String nrContrato){
		List<CobrancaFinanceiroView> list = cobrancaFinanceiroViewDAO.getTitulosAbertosByContrato(nrContrato);
		List<TitulosAbertosClienteDTO> result = new ArrayList<TitulosAbertosClienteDTO>();
		
		for (CobrancaFinanceiroView financeiroView : list) {
			result.add(new TitulosAbertosClienteDTO(financeiroView));
		}
		return result;
	}
}
