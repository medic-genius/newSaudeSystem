package br.com.medic.medicsystem.main.rest;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.medic.medicsystem.main.service.ClienteService;
import br.com.medic.medicsystem.main.service.DependenteService;
import br.com.medic.medicsystem.main.service.TotemService;
import br.com.medic.medicsystem.persistence.dto.AgendamentoTotemDTO;
import br.com.medic.medicsystem.persistence.dto.DigitalClienteDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteViewDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Foto;
import br.com.medic.medicsystem.persistence.model.RegistroPonto;
import br.com.medic.medicsystem.persistence.model.enums.TipoPessoa;


@Path("/totem")
@RequestScoped
public class TotemController extends BaseController {
	@Inject
	private TotemService totemService;
	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private DependenteService dependenteService;
	
	@GET
	@Path("/agendamento/{id}/paciente")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoTotemDTO> getAgendamentoPorIdPaciente(
			@PathParam("id") String nruniqueid) throws ParseException {
		return totemService.getAgendamentoPorIdPaciente(nruniqueid);
	}

	@GET
	@Path("/agendamento/paciente/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgendamentoTotemDTO> agendamentoByPaciente(
			@QueryParam("ids") List<String> ids) throws ParseException {

		if (ids.size() > 0)
			return totemService.agendamentoByPaciente(ids);
		else
			return null;
	}
	
	@GET
	@Path("/agendamento/{id}/agendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public AgendamentoTotemDTO getAgendamentoPorIdAgendamento(@PathParam("id") Long idAgendamento) throws ParseException {

		return totemService.getAgendamentosPorIdAgendamento(idAgendamento);
	}
		
	@PUT
	@Path("/agendamento/{id:[0-9][0-9]*}/presente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusPresente(@PathParam("id") Long idAgendamento,
			@QueryParam("triado") boolean triado,
			@QueryParam("prioridade") Integer prioridade) {

		if (idAgendamento != null) {
			try {

				Agendamento ag = totemService.updateStatusPresente(
						idAgendamento, triado, prioridade, null, null);
			if(ag != null)	
				return Response.ok(ag).build();
			else 
				return null;

			} catch (Exception e) {
				return null;
			}
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id}/foto/cliente")
	@Produces(MediaType.APPLICATION_JSON)
	public Foto getFotoCliente(@PathParam("id") String nrUniqueId) throws IOException {
		
		
		String inTipoPaciente = nrUniqueId.substring(0, 1);
		
		 Long Idpaciente = Long.parseLong(nrUniqueId.substring(1,nrUniqueId.length()));
		 
		 Foto fotoPaciente = new Foto();
		 
		 if(inTipoPaciente.equals("1")){
			 final Cliente cliente = clienteService.getCliente(Idpaciente);
			 
				if(cliente != null)
				{
					if(cliente.getFotoCliente()!= null){
						fotoPaciente.setFoto(cliente.getFotoCliente());
					}
					
					fotoPaciente.setNmPaciente(cliente.getNmCliente());
					return fotoPaciente;
				}
			 
			 
		 }else{
			 
			 final Dependente dependente = dependenteService.getDependente(Idpaciente);
			 
				if(dependente != null)
				{
					if(dependente.getFotoDependente()!= null){
						fotoPaciente.setFoto(dependente.getFotoDependente());
					}
					
					fotoPaciente.setNmPaciente(dependente.getNmDependente());
					return fotoPaciente;
				}
		 }
		 
	
	    return fotoPaciente;
		
		
	}
	
	@GET
	@Path("/digitaiscpf/{cpf}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DigitalClienteDTO> getDigitaisPorCpf(@PathParam("cpf") String cpf, 
			@QueryParam("funcionario") boolean isFuncionario) throws ParseException {
		
		if(cpf != null ){
			return totemService.getDigitalPorCpf(cpf, isFuncionario);
		}
		else{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
			
	}
	
	@GET
	@Path("/paciente/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PacienteViewDTO> pacienteByCpfCod(
			@QueryParam("cpf") String cpf,
			@QueryParam("cod") String cod) throws ParseException {
		
		if((cpf != null && cpf.length() > 0) || (cod != null && cod.length() > 0)){
			return totemService.pacienteByCpfCod(cpf, cod);
		} else{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/paciente/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PacienteViewDTO> pacienteByIdPaciente(
			@PathParam("id") String nrUniqueId) throws ParseException {
		
		if(nrUniqueId != null && nrUniqueId.length() > 0){
			return totemService.pacienteByIdPaciente(nrUniqueId);
		}
		else{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
			
	}
	
	@POST
	@Path("/registroponto/{idFuncionario:[0-9][0-9]*}/{idUnidade:[0-9][0-9]*}/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveRegistroPonto(@PathParam("idFuncionario") Long idFuncionario, @PathParam("idUnidade") Long idUnidade){

		if (idFuncionario != null) {
			try {				
				RegistroPonto pontoRegistrado = totemService.saveRegistroPonto(idFuncionario, idUnidade);
				if(pontoRegistrado != null)	
					return Response.ok(pontoRegistrado).build();
				else 
					return null;

			} catch (Exception e) {
				return null;
			}
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@POST
	@Path("aplicativo/registroponto/{idFuncionario:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveRegistroPonto(@PathParam("idFuncionario") Long idFuncionario, 
			@QueryParam("latitude") Double latitude,
			@QueryParam("longitude") Double longitude){

		if (idFuncionario != null && latitude != null && longitude != null) {
			try {

				RegistroPonto pontoRegistrado = totemService.saveRegistroPonto(idFuncionario, latitude, longitude);
			if(pontoRegistrado != null)	
				return Response.ok(pontoRegistrado).build();
			else 
				return null;

			} catch (Exception e) {
				return null;
			}
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/paciente/foto")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updateFotoPaciente(
			@QueryParam("ids") List<String> nrUniqueIds, Foto fotoPaciente ) throws ParseException {
		if(fotoPaciente != null && nrUniqueIds.size() > 0) {
			return totemService.updateFotoPaciente(nrUniqueIds, fotoPaciente);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
	@GET
	@Path("/paciente/digitais/{tipopessoa}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DigitalClienteDTO> getDigitalAll(
			@PathParam("tipopessoa") Integer tipoPessoa) throws ParseException {
		TipoPessoa enumPessoa = TipoPessoa.getTipoPessoaById(tipoPessoa);
		if(tipoPessoa != null) {
			return totemService.getDigitalAll(enumPessoa);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@POST
	@Path("/funcionario/celular/{nrcelular}")
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> getDigitalAll(
			@PathParam("nrcelular") String nrCelular, String imeiCelular)
			throws ParseException {
		if (nrCelular != null && imeiCelular != null) {
			return totemService.getFuncionarioByCelular(nrCelular, imeiCelular);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/pacientes/dependentes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PacienteViewDTO> getDependentesPaciente(
			@QueryParam("idsPaciente") List<Long> listIds) throws Exception {
		if (listIds != null && !listIds.isEmpty()) {
			return totemService.getDependentesFromIdsTitulares(listIds);
		} else {
			throw new Exception("Nenhum parâmetro foi fornecido");
		}
	}
}