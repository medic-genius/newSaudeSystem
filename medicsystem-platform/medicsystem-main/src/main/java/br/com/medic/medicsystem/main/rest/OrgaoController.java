package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.service.OrgaoService;
import br.com.medic.medicsystem.persistence.model.Orgao;

@Path("/orgaos")
@RequestScoped
public class OrgaoController extends BaseController {

	@Inject
	private OrgaoService orgaoService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createOrgao(Orgao orgao) {
		
		Orgao salvo = orgaoService.saveOrgao(orgao);
		
		return Response.created(URI.create("/orgaos/"+ salvo.getId())).build();
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Orgao> getAllOrgaos() {

		return orgaoService.getAllOrgaos();
	}

	@GET
	@Path("/inEsfera/{inEsfera:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Orgao> getOrgaosByEsfera(@PathParam("inEsfera") Integer inEsfera) {

		return orgaoService.getOrgaosByEsfera(inEsfera);
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrgao(Orgao orgao) {

		if (orgao != null) {
			Orgao OrgaoAtualizado = orgaoService.updateOrgao(orgao);

			if (OrgaoAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@PUT
	@Path("/inativar/{id:[0-9][0-9]*}")
	public Response inativarOrgao(@PathParam("id") Long id){
		
		orgaoService.inativarOrgao(id);
		return Response.ok().build();
		
	}

}