package br.com.medic.medicsystem.main.vindi.data.subscription;

import java.util.List;
import java.util.Map;

import br.com.medic.medicsystem.main.vindi.data.product.ProductItem;
import br.com.medic.medicsystem.main.vindi.data.summary.Customer;
import br.com.medic.medicsystem.main.vindi.data.summary.PaymentMethod;
import br.com.medic.medicsystem.main.vindi.data.summary.PaymentProfile;
import br.com.medic.medicsystem.main.vindi.data.summary.Period;
import br.com.medic.medicsystem.main.vindi.data.summary.Plan;

public class Subscription {
	private Integer id;
	private String status;
	private String start_at;
	private String end_at;
	private String next_billing_at;
	private String overdue_since;
	private String code;
	private String cancel_at;
	private String interval;
	private Integer interval_count;
	private String billing_trigger_type;
	private Integer billing_trigger_day;
	private Integer billing_cycles;
	private Integer installments;
	private String created_at;
	private String updated_at;
	private Customer customer;
	private Plan plan;
	private List<ProductItem> product_items;
	private PaymentMethod payment_method;
	private Period current_period;
	private Map<String, String> metadata;
	private PaymentProfile payment_profile;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStart_at() {
		return start_at;
	}
	public void setStart_at(String start_at) {
		this.start_at = start_at;
	}
	public String getEnd_at() {
		return end_at;
	}
	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}
	public String getNext_billing_at() {
		return next_billing_at;
	}
	public void setNext_billing_at(String next_billing_at) {
		this.next_billing_at = next_billing_at;
	}
	public String getOverdue_since() {
		return overdue_since;
	}
	public void setOverdue_since(String overdue_since) {
		this.overdue_since = overdue_since;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCancel_at() {
		return cancel_at;
	}
	public void setCancel_at(String cancel_at) {
		this.cancel_at = cancel_at;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public Integer getInterval_count() {
		return interval_count;
	}
	public void setInterval_count(Integer interval_count) {
		this.interval_count = interval_count;
	}
	public String getBilling_trigger_type() {
		return billing_trigger_type;
	}
	public void setBilling_trigger_type(String billing_trigger_type) {
		this.billing_trigger_type = billing_trigger_type;
	}
	public Integer getBilling_trigger_day() {
		return billing_trigger_day;
	}
	public void setBilling_trigger_day(Integer billing_trigger_day) {
		this.billing_trigger_day = billing_trigger_day;
	}
	public Integer getBilling_cycles() {
		return billing_cycles;
	}
	public void setBilling_cycles(Integer billing_cycles) {
		this.billing_cycles = billing_cycles;
	}
	public Integer getInstallments() {
		return installments;
	}
	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public List<ProductItem> getProduct_items() {
		return product_items;
	}
	public void setProduct_items(List<ProductItem> product_items) {
		this.product_items = product_items;
	}
	public PaymentMethod getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(PaymentMethod payment_method) {
		this.payment_method = payment_method;
	}
	public Period getCurrent_period() {
		return current_period;
	}
	public void setCurrent_period(Period current_period) {
		this.current_period = current_period;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}
	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
