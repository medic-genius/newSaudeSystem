package br.com.medic.medicsystem.main.service;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.exception.DespesaFinanceiroException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.FornecedorDAO;
import br.com.medic.medicsystem.persistence.model.Fornecedor;

@Stateless
public class FornecedorService {
	
	@Inject
	@Named("fornecedor-dao")
	private FornecedorDAO fornecedorDAO;

	
	public Fornecedor save(Fornecedor fornecedor){
		
		if(fornecedor.getId() == null){
			fornecedorDAO.persist( validarCadastroFornecedor(fornecedor)  );
		} else {
			fornecedorDAO.update(fornecedor);
		}
		return fornecedor;
	}
	
	public Fornecedor getFornecedorById(Long idFornecedor){
		Fornecedor fornecedor = fornecedorDAO.searchByKey(Fornecedor.class, idFornecedor);

		return fornecedor;
	}
	
	public List<Fornecedor> getAllFornecedores(Boolean isInativo){
		List<Fornecedor> list = fornecedorDAO.getAllFornecedores( isInativo);
		return list;
	}
	
	public void deleteFornecedor(Long id){
		Fornecedor fornecedor = getFornecedorById(id);
		fornecedor.setDtExclusao(new Date());
	}
	
	private Fornecedor validarCadastroFornecedor( Fornecedor fornecedor ) {
		
		boolean cpfExiste = false;
		boolean cnpjExiste = false;
		boolean nmRazaoSocialExiste = false;
		
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		
		if( fornecedor.getNrCpf() != null ) 
			cpfExiste = fornecedorDAO.verificarCpfExistente( fornecedor.getNrCpf() );
			
		if( fornecedor.getNrCnpj() != null )
			cnpjExiste = fornecedorDAO.verificarCnjpExistente( fornecedor.getNrCnpj() ); 
		
		if( fornecedor.getNmRazaoSocial() != null )
			nmRazaoSocialExiste = fornecedorDAO.verificarNmRazaoSocialExistente( fornecedor.getNmRazaoSocial()  );
		
		
		if( cpfExiste )
			throw new DespesaFinanceiroException(props.getProperty("fornecedor.cpfexistente"), ErrorType.DANGER);
		
		if( cnpjExiste )
			throw new DespesaFinanceiroException(props.getProperty("fornecedor.cnpjexistente"), ErrorType.DANGER);
		
		if( nmRazaoSocialExiste )
			throw new DespesaFinanceiroException(props.getProperty("fornecedor.nmrazaosocial"), ErrorType.DANGER);
		
		
		return fornecedor;
		
	}

	public void reativarFornecedor(Long id) {
		Fornecedor fornecedor = getFornecedorById(id);
		fornecedor.setDtExclusao(null);
	}
}
