package br.com.medic.medicsystem.main.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.AgenciaDAO;
import br.com.medic.medicsystem.persistence.dao.BancoDAO;
import br.com.medic.medicsystem.persistence.dao.ContaBancariaDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.model.ContaBancaria;

@Stateless
public class ContaBancariaService {
	
	@Inject
	@Named("contabancaria-dao")
	private ContaBancariaDAO contaBancariaDAO;
	
	@Inject
	@Named("empresafinanceira-dao")
	private EmpresaFinanceiroDAO empresaFinanceiroDAO;
	
	@Inject
	@Named("banco-dao")
	private BancoDAO bancoDAO;
	
	@Inject
	@Named("agencia-dao")
	private AgenciaDAO agenciaDAO;
	
	public ContaBancaria saveContaBancaria(ContaBancaria contaBancaria){

		if(contaBancaria.getId() == null){
			contaBancaria  = contaBancariaDAO.persist(contaBancaria);
		}else{
			contaBancaria  = contaBancariaDAO.update(contaBancaria);
		}
		return contaBancaria;
	}
	
	public ContaBancaria getContaBancariaById(Long id){
		ContaBancaria contaContabil = contaBancariaDAO.searchByKey(ContaBancaria.class, id);

		if(contaContabil.getDtExclusao() != null){
			throw new IllegalArgumentException("Conta inativa");
		}
		return contaContabil;
	}
		
	public void deleteContaBancaria(Long idContaBancaria){
		ContaBancaria contaContabil = getContaBancariaById(idContaBancaria);
		contaContabil.setDtExclusao(new Date());
	}

	public List<ContaBancaria> getContasBancariaByEmpresaFinanceiro(Long idEmpresaFinanceiro) {
		return contaBancariaDAO.getContasBancariaByEmpresaFinanceiro( idEmpresaFinanceiro );
	}
	
	public List<ContaBancaria> getContasBancariaDinheiroByEmpresaFinanceiro(Long idEmpresaFinanceiro) {
		Integer tipoDinheiro = 2;
		return contaBancariaDAO.getContasBancariaByEmpresaFinanceiroTipo(idEmpresaFinanceiro, tipoDinheiro);
	}
	
	public List<ContaBancaria> getContasByEmpresaConciliacao(Long idEmpresaFinanceiro){
		return contaBancariaDAO.getContasByEmpresaConciliacao(idEmpresaFinanceiro);
	}

	public void createContaBancaria(ContaBancaria contaBancaria) {
	
		contaBancaria.setEmpresaFinanceiro(empresaFinanceiroDAO.getEmpresaById(contaBancaria.getEmpresaFinanceiro().getIdEmpresaFinanceiro()));
		if(contaBancaria.getInTipo() != 2){
			contaBancaria.setBanco(bancoDAO.getBancoById(contaBancaria.getBanco().getId()));
			contaBancaria.setAgencia(agenciaDAO.getAgenciaByID(contaBancaria.getAgencia().getId()));
		}else {
			contaBancaria.setNrContaBancaria("000000");
			contaBancaria.setNrDigitoVerificador("0");
			contaBancaria.setBanco(bancoDAO.getBancoById((long) 3));
			contaBancaria.setAgencia(agenciaDAO.getAgenciaByID((long) 1));
		}
		contaBancariaDAO.persist(contaBancaria);
	}
}
