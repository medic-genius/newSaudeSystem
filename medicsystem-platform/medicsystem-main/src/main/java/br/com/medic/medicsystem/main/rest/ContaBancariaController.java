package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.ContaBancariaService;
import br.com.medic.medicsystem.persistence.model.ContaBancaria;

@Path("/contabancaria")
public class ContaBancariaController {
	
	@Inject
	private ContaBancariaService contaBancariaService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveContaBancaria(@Valid ContaBancaria contaBancaria){
		ContaBancaria salvo = contaBancariaService.saveContaBancaria(contaBancaria);
		return Response.created(URI.create("/contabancaria/" + salvo.getId())).build();
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateContaBancaria(@Valid ContaBancaria contaBancaria, @PathParam("id") Long id){
		ContaBancaria salvo = contaBancariaService.saveContaBancaria(contaBancaria);
		return Response.created(URI.create("/contabancaria/" + salvo.getId())).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	public ContaBancaria getContaBancariaById(@PathParam("id") Long id){
		ContaBancaria contaBancaria = contaBancariaService.getContaBancariaById(id);
		return contaBancaria;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empresafinanceiro/{idEmpresaFinanceiro:[0-9][0-9]*}")
	public List<ContaBancaria> getContasBancariaByEmpresaFinanceiro(@PathParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro){
		final List<ContaBancaria> contaBancariaList = contaBancariaService.getContasBancariaByEmpresaFinanceiro( idEmpresaFinanceiro );
		
		if( contaBancariaList.isEmpty() || contaBancariaList == null ) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {
			return contaBancariaList;
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empresafinanceirodinheiro/{idEmpresaFinanceiro:[0-9][0-9]*}")
	public List<ContaBancaria> getContasBancariaDinheiroByEmpresaFinanceiro(@PathParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro){
		final List<ContaBancaria> contaBancariaList = contaBancariaService.getContasBancariaDinheiroByEmpresaFinanceiro( idEmpresaFinanceiro );
		
		if( contaBancariaList.isEmpty() || contaBancariaList == null ) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {
			return contaBancariaList;
		}
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteContaBancaria(@PathParam("id") Long id){
		contaBancariaService.deleteContaBancaria(id);
		return Response.ok().build();
	}
	
	//Conciliaçao Bancária
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empfinanceiroconc/{idEmpresaFinanceiro:[0-9][0-9]*}")
	public List<ContaBancaria> getContasByEmpresaConciliacao(@PathParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro){
		final List<ContaBancaria> contaBancariaList = contaBancariaService.getContasByEmpresaConciliacao( idEmpresaFinanceiro );
		
		if( contaBancariaList.isEmpty() || contaBancariaList == null ) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {
			return contaBancariaList;
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create")
	public Response createContaBancaria(ContaBancaria contaBancaria){
		
		if(contaBancaria == null){
			throw new IllegalArgumentException();
		}
		contaBancariaService.createContaBancaria(contaBancaria);
		return Response.ok().build();
	}
	
	
	
}
