package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.appmobile.dto.UserSearchResultDTO;
import br.com.medic.medicsystem.main.appmobile.dto.UserRegisterDTO;
import br.com.medic.medicsystem.main.appmobile.dto.UserSearchDTO;
import br.com.medic.medicsystem.persistence.appmobile.dao.UsuarioRegisterDAO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;
import br.com.medic.medicsystem.persistence.security.keycloak.KeycloakUsers;

@Stateless
public class MobileService {	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private DependenteService dependenteService;
	
	@Inject
	private EmpresaGrupoDAO empresaGrupoDao;
	
	@Inject
	@Named("usuario-register-dao")
	private UsuarioRegisterDAO usuarioRegisterDAO;
	
	@Inject
	private KeycloakUsers keycloakUsers;
	
	public UserSearchResultDTO searchUser(Integer tipoCliente, UserSearchDTO user) {
		UserSearchResultDTO userResult = new UserSearchResultDTO();
		UsuarioRegister usr = null;
		if(user.getCPF() != null || user.getCodPaciente() != null) {
			if(tipoCliente.intValue() == TipoUsuario.CLIENTE.valorTipo) {
				// busca cliente
				try {
					List<ClienteView> clientes = clienteService.getClientes(
							user.getCPF() == null ? user.getCodPaciente() : null,
							null, user.getCPF(), null, 0, 1);
					if(clientes != null && !clientes.isEmpty()) {
						ClienteView cli = clientes.get(0);
						userResult.setIdCliente(cli.getId());
						userResult.setNmCliente(cli.getNmCliente());
						usr = usuarioRegisterDAO.getUsuarioRegisterByKey(cli.getId(), tipoCliente);
					}
				} catch(Exception e) { }
			} else if(tipoCliente.intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
				Dependente dependente = dependenteService.getDependenteByCPFOrCode(user.getCPF(), user.getCodPaciente());
				if(dependente != null) {
					userResult.setIdCliente(dependente.getId());
					userResult.setNmCliente(dependente.getNmDependente());
					usr = usuarioRegisterDAO.getUsuarioRegisterByKey(dependente.getId(), tipoCliente);
				}
			}
		}
		userResult.setIsRegistered(usr != null);
		return userResult;
	}
	
	private String getNumberOnly(String var) {
		if (var != null) {
			var = var.replaceAll("\\s", "");
			var = var.trim();
			var = var.replaceAll("[^0-9]", "");
			return var;
		}
		return null;
	}
	
	public Integer createUser(UserRegisterDTO register, Integer tipoCliente) {
		if(register.getCliente() != null) {
			register.getCliente().setNmCliente(register.getCliente().getNmCliente().toUpperCase());
			register.getCliente().setNrCPF(getNumberOnly(register.getCliente().getNrCPF()));
			register.getCliente().setNrCelular(getNumberOnly(register.getCliente().getNrCelular()));
		}
		
		if(tipoCliente.equals(TipoUsuario.CLIENTE.valorTipo)) {
			Integer result = null;
			if(register.getCliente().getId() != null) {
				Cliente cliente = clienteService.getCliente(register.getCliente().getId());
				cliente.setNmEmail(register.getCliente().getNmEmail());
				if(register.getCliente().getNrCelular() != null) {
					cliente.setNrCelular(register.getCliente().getNrCelular());
				}
				cliente = clienteService.updateCliente(cliente);
				if(cliente != null) {
					result = createKCUserAndRegister(cliente.getId(), tipoCliente, register, 
							cliente.getNmCliente(), cliente.getNmEmail()); 
				}
			} else {
				Cliente cliente = register.getCliente();
				if(cliente.getNmCliente() == null || cliente.getNrCPF() == null || 
						cliente.getNrCelular() == null || cliente.getNmEmail() == null) {
					return 204;
				}
				EmpresaGrupo defaultEmpresa = empresaGrupoDao.searchByKey(EmpresaGrupo.class, 9L);
				Cliente savedCliente = clienteService.saveCliente(register.getCliente(), 0, defaultEmpresa);
				if(savedCliente != null) {
					result = createKCUserAndRegister(savedCliente.getId(), tipoCliente, register, 
							savedCliente.getNmCliente(), savedCliente.getNmEmail());
					if(result != 200) {
						// remover novo cliente
					}
				}
			}
			return result;
		} else if(tipoCliente.equals(TipoUsuario.DEPENDENTE.valorTipo)) {
			if(register.getCliente().getId() == null) {
				return 204;
			}
			Dependente dependente = dependenteService.getDependente(register.getCliente().getId());
			dependente.setNmEmail(register.getCliente().getNmEmail());
			if(register.getCliente().getNrCelular() != null) {
				dependente.setNrCelular(register.getCliente().getNrCelular());
			}
			dependente = dependenteService.updateDependente(dependente.getId(), dependente);
			if(dependente != null) {
				return createKCUserAndRegister(dependente.getId(), tipoCliente, register, 
						dependente.getNmDependente(), dependente.getNmEmail());
			}
		}
		return 500;
	}
	
	private Integer createKCUserAndRegister(Long userId, Integer tipoCliente, 
			UserRegisterDTO register, String completeName, String email) {
		if(usuarioRegisterDAO.getUsuarioRegisterByKey(userId, tipoCliente) != null) {
			return 409;
		}
		String[] nameArr = completeName.split(" ");
		String fName = nameArr[0];
		String lName = nameArr.length > 1 ? nameArr[nameArr.length-1] : null;
		
		String kcUserId = keycloakUsers.createUser(email, fName, lName, email, register.getPassword()); 
		if(kcUserId != null) {
			UsuarioRegister usrRegister = new UsuarioRegister();
			usrRegister.setIdUsuario(userId);
//			usrRegister.setLogin(register.getLogin());
			// login é o email
			usrRegister.setLogin(email);
			usrRegister.setTipoUsuario(tipoCliente.intValue());
			usrRegister.setKcUserId(kcUserId);
			UsuarioRegister newRegistered = usuarioRegisterDAO.persist(usrRegister);
			if(newRegistered != null) {
				return 200;
			}
			keycloakUsers.deleteUserById(kcUserId);
		}
		return 500;
	}
	
	public Boolean isUsernameAvailable(String userName) {
		return usuarioRegisterDAO.getUsuarioByUsername(userName) == null;
	}
	
	public Boolean requestResetPasswordEmail(String email) {
		UsuarioRegister user = usuarioRegisterDAO.getUsuarioByUsername(email);
		if(user != null && user.getKcUserId() != null) {
			return keycloakUsers.requestResetPasswordEmail(user.getKcUserId());
		}
		return null;
	}
}
