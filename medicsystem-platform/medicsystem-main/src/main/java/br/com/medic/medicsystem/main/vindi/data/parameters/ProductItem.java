package br.com.medic.medicsystem.main.vindi.data.parameters;

import java.util.List;

public class ProductItem {
	private Integer product_id;
	private Integer cycles;
	private Integer quantity;
	private PricingSchema pricing_schema;
	private List<Discount>discounts;
	
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public Integer getCycles() {
		return cycles;
	}
	public void setCycles(Integer cycles) {
		this.cycles = cycles;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public PricingSchema getPricing_schema() {
		return pricing_schema;
	}
	public void setPricing_schema(PricingSchema pricing_schema) {
		this.pricing_schema = pricing_schema;
	}
	public List<Discount> getDiscounts() {
		return discounts;
	}
	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}
}
