package br.com.medic.medicsystem.main.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.ContaBancariaDAO;
import br.com.medic.medicsystem.persistence.dao.MovimentacaoContaDAO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.MovimentacaoContaDTO;
import br.com.medic.medicsystem.persistence.dto.MovimentacaoItensContaDTO;
import br.com.medic.medicsystem.persistence.model.ContaBancaria;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.model.MovimentacaoItemConta;
import br.com.medic.medicsystem.persistence.model.enums.AcaoMovimentacao;
import br.com.medic.medicsystem.persistence.model.enums.TipoMovimentacao;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.Utils;

@Stateless
public class MovimentacaoContaService {

	@Inject
	@Named("movimentacaoconta-dao")
	private MovimentacaoContaDAO movimentacaoContaDAO;

	@Inject
	@Named("contabancaria-dao")
	private ContaBancariaDAO contaBancariaDAO;

	@Inject
	private SecurityService securityService;

	public List<MovimentacaoContaDTO> getMovimentacoesDTOByDias(
			String dataInicial, String dataFinal, Long idContaBancaria,
			Long idEmpresa) {
		Date dtInicio = null;
		Date dtFim = null;
		dtInicio = DateUtil.parseDate(dataInicial,
				DateUtil.DatePattern.DDMMAA.getPattern());
		dtFim = DateUtil.parseDate(dataFinal,
				DateUtil.DatePattern.DDMMAA.getPattern());

		List<MovimentacaoContaDTO> dtos = new ArrayList<MovimentacaoContaDTO>();

		while (dtInicio.before(dtFim) || dtInicio.equals(dtFim)) {
			MovimentacaoContaDTO movimentacao = new MovimentacaoContaDTO();
			movimentacao = getMovimentacoesDTO(dtInicio, dtInicio,
					idContaBancaria, idEmpresa);
			if (movimentacao != null) {
				dtos.add(movimentacao);
			}
			dtInicio = DateUtil.getDatePlusDays(dtInicio, 1);
		}

		return dtos;
	}

	private MovimentacaoContaDTO getMovimentacoesDTO(Date dataInicial,
			Date dataFinal, Long idContaBancaria, Long idEmpresa) {
		MovimentacaoContaDTO dtos = null;
		List<MovimentacaoConta> movimentacoes = movimentacaoContaDAO
				.getMovimentacoes(dataInicial, dataFinal, idContaBancaria,
						idEmpresa);

		if (movimentacoes != null) {
			dtos = getMovimentacaoContaDTOFromMovimentacoes(movimentacoes);
			dtos.setData(dataFinal);
		}

		if (dtos.getItens() == null || dtos.getItens().size() == 0)
			return null;

		return dtos;
	}

	private MovimentacaoContaDTO getMovimentacaoContaDTOFromMovimentacoes(
			List<MovimentacaoConta> movimentacoes) {
		MovimentacaoContaDTO dtos = new MovimentacaoContaDTO();
		List<MovimentacaoItensContaDTO> itens = new ArrayList<MovimentacaoItensContaDTO>();

		Double saldoConta = 0D;
		Double totalEntrada = 0D;
		Double totalSaida = 0D;
		Double totalEstorno = 0D;

		for (MovimentacaoConta movimentacaoConta : movimentacoes) {
			Double debito = 0D;
			Double credito = 0D;
			// debito
			if (movimentacaoConta.getInTipo() == TipoMovimentacao.ENTRADA) {
				debito = 0D;
				credito = movimentacaoConta.getNrValor();
			} else if (movimentacaoConta.getInTipo() == TipoMovimentacao.SAIDA) {
				// credito
				debito = movimentacaoConta.getNrValor();
				credito = 0D;
			}

			// calculando totais
			if (movimentacaoConta.getAcaoMovimentacao().getId() == AcaoMovimentacao.RECEBIMENTO
					.getId()
					|| movimentacaoConta.getAcaoMovimentacao().getId() == AcaoMovimentacao.RECEBIMENTO_CAIXA
							.getId())
				totalEntrada = totalEntrada + movimentacaoConta.getNrValor();
			else if (movimentacaoConta.getAcaoMovimentacao().getId() == AcaoMovimentacao.PAGAMENTO
					.getId()
					&& (movimentacaoConta.getItemsConta() != null && movimentacaoConta
							.getItemsConta().get(0).getDespesaFinanceiro()
							.getDtExclusao() == null))
				totalSaida = totalSaida + movimentacaoConta.getNrValor();
			else if (movimentacaoConta.getAcaoMovimentacao().getId() == AcaoMovimentacao.ESTORNO
					.getId())
				totalEstorno = totalEstorno + movimentacaoConta.getNrValor();

			saldoConta = saldoConta + credito - debito;

			String documentos = getNumerosDocumentos(movimentacaoConta
					.getItemsConta());
			String fornecedores = getNmFornecedores(movimentacaoConta
					.getItemsConta());
			String descricao = movimentacaoConta.getItemsConta() != null
					&& movimentacaoConta.getItemsConta().size() > 0 ? movimentacaoConta
					.getItemsConta().get(0).getDespesaFinanceiro()
					.getNmDespesa()
					: null;
			// String descricao = movimentacaoConta.getNmDescricao() != null ?
			// movimentacaoConta.getNmDescricao().toLowerCase() : null;
			String descCaixa = movimentacaoConta.getDescCaixa() != null ? movimentacaoConta
					.getDescCaixa() : null;

			MovimentacaoItensContaDTO item = new MovimentacaoItensContaDTO(
					movimentacaoConta.getDtMovimentacao(), documentos,
					movimentacaoConta.getInTipo().ordinal(), movimentacaoConta
							.getAcaoMovimentacao().getDescription(),
					Utils.getDoubleDecimalPlaces(debito, 2),
					Utils.getDoubleDecimalPlaces(credito, 2),
					Utils.getDoubleDecimalPlaces(saldoConta, 2), fornecedores,
					descricao, descCaixa, movimentacaoConta.getHaveEstorno(),
					movimentacaoConta.getInfoEstorno(), movimentacaoConta.getIdCaixaConferido(), movimentacaoConta.getAcaoMovimentacao().getId());
			itens.add(item);
		}
		dtos.setItens(itens);
		dtos.setNrSaldo(Utils.getDoubleDecimalPlaces(saldoConta, 2));
		dtos.setTotalEntrada(Utils.getDoubleDecimalPlaces(totalEntrada, 2));
		dtos.setTotalSaida(Utils.getDoubleDecimalPlaces(totalSaida, 2));
		dtos.setTotalEstorno(Utils.getDoubleDecimalPlaces(totalEstorno, 2));
		return dtos;
	}

	private String getNumerosDocumentos(List<MovimentacaoItemConta> itemsConta) {
		String documentos = "";

		if (itemsConta != null) {
			if (itemsConta.size() == 1)
				documentos = itemsConta.get(0).getDespesaFinanceiro()
						.getNrDocumento();
			else
				for (MovimentacaoItemConta it : itemsConta) {
					documentos += it.getDespesaFinanceiro().getNrDocumento()
							+ ";";
				}
		}

		return documentos;
	}

	private String getNmFornecedores(List<MovimentacaoItemConta> itemsConta) {
		String nmFornecedores = "";
		if (itemsConta != null) {
			if (itemsConta.size() == 1)
				nmFornecedores = itemsConta.get(0).getDespesaFinanceiro()
						.getFornecedor() != null ? itemsConta.get(0)
						.getDespesaFinanceiro().getFornecedor()
						.getNmNomeFantasia() : "";
			else
				for (MovimentacaoItemConta it : itemsConta) {
					nmFornecedores += it.getDespesaFinanceiro().getFornecedor() != null ? it
							.getDespesaFinanceiro().getFornecedor()
							.getNmNomeFantasia()
							+ ";"
							: null;
				}
		}

		return nmFornecedores;
	}

	public Long saveMovimentacaoCaixa(Long idContaBancaria, Double valor,
			Long idCaixaConferido, String nmOperador, String nmUnidade,
			String dtCaixaFormatada) {
		MovimentacaoConta movimentacao = new MovimentacaoConta();
		ContaBancaria conta = contaBancariaDAO.searchByKey(ContaBancaria.class,
				idContaBancaria);
		Funcionario funcionario = securityService.getFuncionarioLogado();

		Date dtCaixa = null;
		dtCaixa = DateUtil.parseDate(dtCaixaFormatada,
				DateUtil.DatePattern.DDMMAA.getPattern());

		movimentacao.setDtMovimentacao(dtCaixa);
		movimentacao.setContaBancaria(conta);
		movimentacao.setAcaoMovimentacao(AcaoMovimentacao.RECEBIMENTO_CAIXA);
		movimentacao.setNrValor(valor);
		movimentacao.setFuncionarioMovimentacao(funcionario);
		movimentacao.setInTipo(TipoMovimentacao.ENTRADA);
		movimentacao.setIdCaixaConferido(idCaixaConferido);
		movimentacao.setDescCaixa(nmOperador + "-" + nmUnidade);
		movimentacaoContaDAO.persist(movimentacao);
		return movimentacao.getId();
	}

	public BaseDTO saldomesanterior(String dtInicio, Long idConta) {

		BaseDTO baseDTO = null;
		Date dtReferencia = DateUtil.parseDate(dtInicio);

		MovimentacaoConta lasMovSaldoAnterior = movimentacaoContaDAO
				.getUltimoSaldoAnteriorDePeriodoDeContaBancaria(dtReferencia,
						idConta);

		if (lasMovSaldoAnterior != null) {
			Double nrValor = Utils.getDoubleDecimalPlaces(
					lasMovSaldoAnterior.getNrValor(), 2);
			baseDTO = new BaseDTO();
			baseDTO.setDescription(nrValor.toString());
			baseDTO.setOpcionalInfo(DateUtil.getDateAsString(
					lasMovSaldoAnterior.getDtMovimentacao(), "dd/MM/YYYY"));
		}

		return baseDTO;
	}

}
