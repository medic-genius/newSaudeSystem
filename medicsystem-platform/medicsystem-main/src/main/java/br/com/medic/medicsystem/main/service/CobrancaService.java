package br.com.medic.medicsystem.main.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import br.com.medic.medicsystem.main.exception.CobrancaException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaCompartilhadoViewAcademiaDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaCompartilhadoViewDentalDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaCompartilhadoViewMedicDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaContatoViewDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaDAO;
import br.com.medic.medicsystem.persistence.dao.CobrancaViewDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeCompartilhadoDentalDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeCompartilhadoPeformanceDAO;
import br.com.medic.medicsystem.persistence.dao.MensalidadeDAO;
import br.com.medic.medicsystem.persistence.dao.SacDAO;
import br.com.medic.medicsystem.persistence.dto.ClienteContatoCobrancaDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteInadimplenteDTO;
import br.com.medic.medicsystem.persistence.dto.ClientesPaginadosDTO;
import br.com.medic.medicsystem.persistence.dto.MensalidadeDTO;
import br.com.medic.medicsystem.persistence.dto.TipoContatoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Cobranca;
import br.com.medic.medicsystem.persistence.model.CobrancaContatoView;
import br.com.medic.medicsystem.persistence.model.CobrancaView;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.Sac;
import br.com.medic.medicsystem.persistence.model.enums.SituacaoSacCobranca;
import br.com.medic.medicsystem.persistence.model.enums.TipoContato;
import br.com.medic.medicsystem.persistence.model.views.CobrancaCompartilhadoView;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.DateUtil.DatePattern;

@Stateless
public class CobrancaService<E> {

	@Inject
	@Named("cobrancaview-dao")
	private CobrancaViewDAO cobrancaViewDAO;
	
	@Inject
	@Named("cobrancacompartilhadoviewmedic-dao")
	private CobrancaCompartilhadoViewMedicDAO cobrancaCompartilhaViewMedicDAO;
	
	@Inject
	@Named("cobrancacompartilhadoviewacademia-dao")
	private CobrancaCompartilhadoViewAcademiaDAO cobrancaCompartilhaViewAcademiaDAO;
	
	@Inject
	@Named("cobrancacompartilhadoviewdental-dao")
	private CobrancaCompartilhadoViewDentalDAO cobrancaCompartilhaViewDentalDAO;
	
	@Inject
	@Named("cobrancacontatoview-dao")
	private CobrancaContatoViewDAO cobrancaContatoViewDAO;
	
	@Inject
	@Named("cobranca-dao")
	private CobrancaDAO cobrancaDAO;
	
	@Inject
	@Named("sac-dao")
	private SacDAO saqDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;

	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	
	@Inject
	private SecurityService securityService;
	
	private Properties props = PropertiesLoader.getInstance().load(
	        "message.properties");
	
	@Inject
	@Named("mensalidadecompartilhadodental-dao")
	private MensalidadeCompartilhadoDentalDAO mensalidadeCompartilhadoDentalDAO;
	
	@Inject
	@Named("mensalidadecompartilhadopeformance-dao")
	private MensalidadeCompartilhadoPeformanceDAO mensalidadeCompartilhadoPeformanceDAO;

	@Inject
	@Named("mensalidade-dao")
	private MensalidadeDAO mensalidadeMedicDAO;
	
	
	public ClientesPaginadosDTO getClientesInadimplentes(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, String dataInicio, String dataFim, Integer pagina, Integer itensPagina, Boolean boAdimplente){
		
		Integer startValuePage = null, endValuePage = null;
		Date dtInicio = null;
		Date dtFim = null;
		dtInicio = DateUtil.parseDate(dataInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		dtFim = DateUtil.parseDate(dataFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
		ClientesPaginadosDTO paginado = new ClientesPaginadosDTO();		
		Long quantidadeCliente;
				
		if(pagina != null && itensPagina != null){
			startValuePage = ((pagina-1) * itensPagina);
			endValuePage = itensPagina;
		}
		
		quantidadeCliente = getQuantidadeClientesByTipoData(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
		paginado.setQuantidadeClientesTotal(quantidadeCliente);
		if(quantidadeCliente == null || quantidadeCliente <= 0)
			return paginado;
		
		List<ClienteInadimplenteDTO> clientesInadimplentesDTO = getClientesInadimplentesByTipoData(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
		paginado.setClientes(clientesInadimplentesDTO);
		
		return paginado;
	}
	
	public File getClientesInadimplentesXLS(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, String dataInicio, String dataFim, Integer pagina, Integer itensPagina, Boolean boAdimplente){
		
		Integer startValuePage = null, endValuePage = null;
		Date dtInicio = null;
		Date dtFim = null;
		dtInicio = DateUtil.parseDate(dataInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		dtFim = DateUtil.parseDate(dataFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
//		if(pagina != null && itensPagina != null){
//			startValuePage = ((pagina-1) * itensPagina);
//			endValuePage = itensPagina;
//		}

//		ClientesPaginadosDTO paginado = new ClientesPaginadosDTO();
		
//		Long quantidadeCliente;
		
//		quantidadeCliente = getQuantidadeClientesByTipoData(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
//		paginado.setQuantidadeClientesTotal(quantidadeCliente);
//		if(quantidadeCliente == null || quantidadeCliente <= 0)
//			return paginado;
		
		File file = null;
		
		List<ClienteInadimplenteDTO> clientesInadimplentesDTO = getClientesInadimplentesByTipoDataXLS(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
		try {
			 file = criarArq(clientesInadimplentesDTO);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return file;
	}
	
	 private File criarArq( List<ClienteInadimplenteDTO> listDTO) throws IOException {
		   
          HSSFWorkbook workbook = new HSSFWorkbook();
          HSSFSheet sheetAlunos = workbook.createSheet("ClienteInadimplenteDTO");
           
          int rownum = 0;
          Row row = sheetAlunos.createRow(rownum++);
          
          row.createCell(0).setCellValue("Titular");
          row.createCell(1).setCellValue("CPF");
          row.createCell(2).setCellValue("CONTATO");
          row.createCell(3).setCellValue("CIDADE");
          row.createCell(4).setCellValue("BAIRRO");
          row.createCell(5).setCellValue("LOGRADOURO");
          row.createCell(6).setCellValue("Nº");
          row.createCell(7).setCellValue("COMPLEMENTO");
          row.createCell(8).setCellValue("P. REFERENCIA");
          row.createCell(9).setCellValue("CEP");
          row.createCell(10).setCellValue("TITULOS ABERTOS");
          row.createCell(11).setCellValue("TOTAL DIVIDA");
          row.createCell(12).setCellValue("TOTAL DIVIDA C/ JUROS");
          
          for (ClienteInadimplenteDTO cliInaDTO : listDTO) {
        	  row = sheetAlunos.createRow(rownum++);
              int cellnum = 0;
              Cell cellNome = row.createCell(cellnum++);
              cellNome.setCellValue(cliInaDTO.getNmCliente());
              Cell cellCPF = row.createCell(cellnum++);
              cellCPF.setCellValue(cliInaDTO.getNrCPF());
              Cell cellContato = row.createCell(cellnum++);
              cellContato.setCellValue((cliInaDTO.getNrCelular() != null ? cliInaDTO.getNrCelular(): "")+"/"+cliInaDTO.getNrTelefone() != null ? cliInaDTO.getNrTelefone() : "");
              Cell cellCidade = row.createCell(cellnum++);
              cellCidade.setCellValue(cliInaDTO.getNmCidade());
              Cell cellBairro = row.createCell(cellnum++);
              cellBairro.setCellValue(cliInaDTO.getNmBairro());
              Cell cellLogradouro = row.createCell(cellnum++);
              cellLogradouro.setCellValue(cliInaDTO.getNmLogradouro());
              Cell cellNumero = row.createCell(cellnum++);
              cellNumero.setCellValue(cliInaDTO.getNrNumero());
              Cell cellComplemento = row.createCell(cellnum++);
              cellComplemento.setCellValue(cliInaDTO.getNmComplemento());
              Cell cellPReferencia = row.createCell(cellnum++);
              cellPReferencia.setCellValue(cliInaDTO.getNmPontoReferencia());
              Cell cellCEP = row.createCell(cellnum++);
              cellCEP.setCellValue(cliInaDTO.getNrCEP());
              Cell cellTitulosAberto = row.createCell(cellnum++);
              cellTitulosAberto.setCellValue(cliInaDTO.getTotalDocumentoAberto());
              Cell cellTotalDivida = row.createCell(cellnum++);
              cellTotalDivida.setCellValue(cliInaDTO.getTotalDivida());
              Cell cellTotalDividaJuros =row.createCell(cellnum++);
              cellTotalDividaJuros.setCellValue(cliInaDTO.getTotalDividaComJuros());
          }
           
          try {
        	  
        	  File file = new File("teste.xls");
              FileOutputStream out = 
                      new FileOutputStream(file);
              workbook.write(out);
              out.close();
              System.out.println("Arquivo Excel criado com sucesso!");

              return file;
               
          } catch (FileNotFoundException e) {
              e.printStackTrace();
                 System.out.println("Arquivo não encontrado!");
          } catch (IOException e) {
              e.printStackTrace();
                 System.out.println("Erro na edição do arquivo!");
          }
          
          return null;

    }
	
	
	private Long getQuantidadeClientesByTipoData(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, Date dataInicio, Date dataFim, Integer startValuePage, Integer endValuePage, Boolean boAdimplente){
		Long quantidadeCliente = null;
		
		if(nomeCliente != null){
			quantidadeCliente = cobrancaViewDAO.getQuantidadeClientesInadimplentes(idEmpresa, idFormaPagamento, null, nomeCliente, dataInicio, dataFim, boAdimplente);
			return quantidadeCliente;
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			switch (opcaoData) {
			
			//se a opção de data for por previsao de pagamento
			case 0:
				quantidadeCliente = cobrancaContatoViewDAO.getQuantidadeClientesInadimplentes(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
				break;

			//se a opção de data for por próxima ligação
			case 1:
				quantidadeCliente = cobrancaContatoViewDAO.getQuantidadeClientesInadimplentes(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
				break;

			//se a opção de data for por periodo inadimplencia
			case 2:
				quantidadeCliente = cobrancaViewDAO.getQuantidadeClientesInadimplentes(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
				break;

			//se a opção de data for por inadimplencia recente
			case 3:
				Integer dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
				
				if(dias == null){
					throw new IllegalArgumentException("Data inválida");
				}
				quantidadeCliente = cobrancaViewDAO.getQuantidadeClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, dias);				
				break;
				
			default:
				throw new IllegalArgumentException();
			}
		}
		return quantidadeCliente;
		

	}
	
	
	private List<Double> somaInadimplenciaCobranca(List<ClienteInadimplenteDTO> cobrancasSoma){
		Double valorComJuros = new Double(0D);
		Double valorSemJuros = new Double(0D);
		List<Double> listaValores = new ArrayList<Double>();
		for (ClienteInadimplenteDTO cobrancaContatoView : cobrancasSoma) {
			valorSemJuros += cobrancaContatoView.getTotalDivida();
			valorComJuros += cobrancaContatoView.getTotalDividaComJuros();
		}
		listaValores.add(valorSemJuros);
		listaValores.add(valorComJuros);
		
		return listaValores;
	}
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesByTipoData(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, Date dataInicio, Date dataFim, Integer startValuePage, Integer endValuePage, Boolean boAdimplente){
		List<CobrancaView> cobrancasList = new ArrayList<CobrancaView>();
		List<CobrancaView> somaCobrancasList =  new ArrayList<CobrancaView>();
		List<CobrancaContatoView> cobrancasContatoList;
		List<CobrancaContatoView> cobrancasSoma;
		List<ClienteInadimplenteDTO> clientesInadimplentes = new ArrayList<ClienteInadimplenteDTO>();
		List<ClienteInadimplenteDTO> dtos = new ArrayList<ClienteInadimplenteDTO>();
		List<Double> somaValores = new ArrayList<Double>();
		
		if(nomeCliente != null){
			cobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(true));
			if(cobrancasList != null && cobrancasList.size() > 0){
				somaCobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(false));
				
//					somaValores = somaInadimplenciaCobranca(somaCobrancasList);
					clientesInadimplentes = getClientesInadimplentesFromCobrancaList(cobrancasList);
					dtos = getClientesInadimplentesFromCobrancaList(somaCobrancasList);
					somaValores = somaInadimplenciaCobranca(dtos);
					clientesInadimplentes.get(0).setSomaTotalDivida(somaValores.get(0));
					clientesInadimplentes.get(0).setSomaTotalDividaComJuros(somaValores.get(1));
					return clientesInadimplentes;

			}
			return getClientesInadimplentesFromCobrancaList(cobrancasList);
			
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			
			switch (opcaoData) {
			
			//se a opção de data for por previsao de pagamento
			case 0:
				cobrancasContatoList = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(true));
				if(cobrancasContatoList != null && cobrancasContatoList.size() > 0){
					cobrancasSoma = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(false));
						if(cobrancasSoma != null && cobrancasSoma.size() > 0){
							clientesInadimplentes = getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);
							dtos = getClientesInadimplentesFromCobrancaContatoList(cobrancasSoma);
							somaValores = somaInadimplenciaCobranca(dtos);
							clientesInadimplentes.get(0).setSomaTotalDivida(somaValores.get(0));
							clientesInadimplentes.get(0).setSomaTotalDividaComJuros(somaValores.get(1));
							return clientesInadimplentes;
						}
					return clientesInadimplentes;
				}
			
				return getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);

			//se a opção de data for por próxima ligação
			case 1:
				cobrancasContatoList = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(true));
				if(cobrancasContatoList != null && cobrancasContatoList.size() > 0){
					cobrancasSoma = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(false));
						if(cobrancasSoma != null && cobrancasSoma.size() > 0){
							clientesInadimplentes = getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);
							dtos = getClientesInadimplentesFromCobrancaContatoList(cobrancasSoma);
							somaValores = somaInadimplenciaCobranca(dtos);
							clientesInadimplentes.get(0).setSomaTotalDivida(somaValores.get(0));
							clientesInadimplentes.get(0).setSomaTotalDividaComJuros(somaValores.get(1));
							return clientesInadimplentes;
						}
					return clientesInadimplentes;
				}
			
				return getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);

			//se a opção de data for por periodo inadimplencia
			case 2:
				cobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(true));
				if(cobrancasList != null && cobrancasList.size() > 0){
					somaCobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente, new Boolean(false));
						clientesInadimplentes = getClientesInadimplentesFromCobrancaList(cobrancasList);
						dtos = getClientesInadimplentesFromCobrancaList(somaCobrancasList);
						somaValores = somaInadimplenciaCobranca(dtos);
						clientesInadimplentes.get(0).setSomaTotalDivida(somaValores.get(0));
						clientesInadimplentes.get(0).setSomaTotalDividaComJuros(somaValores.get(1));
						return clientesInadimplentes;

				}
				return getClientesInadimplentesFromCobrancaList(cobrancasList);

			//se a opção de data for por inadimplencia recente
			case 3:
				Integer dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
				
				if(dias == null){
					throw new IllegalArgumentException("Data inválida");
				}
				cobrancasList = cobrancaViewDAO.getClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias, new Boolean(true));
//				if(cobrancasList != null && cobrancasList.size() > 0){
//					somaCobrancasList = cobrancaViewDAO.getClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias, new Boolean(false));
//					clientesInadimplentes = getClientesInadimplentesFromCobrancaList(cobrancasList);
//					dtos = getClientesInadimplentesFromCobrancaList(somaCobrancasList);
//					somaValores = somaInadimplenciaCobranca(dtos);
//					clientesInadimplentes.get(0).setSomaTotalDivida(somaValores.get(0));
//					clientesInadimplentes.get(0).setSomaTotalDividaComJuros(somaValores.get(1));
//					return clientesInadimplentes;
//				
//				}
				
				return getClientesInadimplentesFromCobrancaList(cobrancasList);
				
			default:
				throw new IllegalArgumentException();
			}
		}
		return null;
	}
	
	//para gerar o arquivo xls
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesByTipoDataXLS(Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, Date dataInicio, Date dataFim, Integer startValuePage, Integer endValuePage, Boolean boAdimplente){
		List<CobrancaView> cobrancasList = null;
		List<CobrancaContatoView> cobrancasContatoList = new ArrayList<CobrancaContatoView>();
		
		if(nomeCliente != null){
			cobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaViewXLS(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
			return getClientesInadimplentesFromCobrancaListXLS(cobrancasList);
			
		}
		
		if(opcaoData != null && dataInicio != null && dataFim != null){
			switch (opcaoData) {
			
			//se a opção de data for por previsao de pagamento
			case 0:
				//cobrancasContatoList = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);

			//se a opção de data for por próxima ligação
			case 1:
				//cobrancasContatoList = cobrancaContatoViewDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaContatoList(cobrancasContatoList);

			//se a opção de data for por periodo inadimplencia
			case 2:
				cobrancasList = cobrancaViewDAO.getClientesInadimplentesFromCobrancaViewXLS(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaListXLS(cobrancasList);

			//se a opção de data for por inadimplencia recente
			case 3:
				Integer dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
				
				if(dias == null){
					throw new IllegalArgumentException("Data inválida");
				}
				cobrancasList = cobrancaViewDAO.getClientesInadimplentesByInadimplenciaRecenteXLS(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias);
				return getClientesInadimplentesFromCobrancaListXLS(cobrancasList);
				
			default:
				throw new IllegalArgumentException();
			}
		}
		return null;
	}
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesFromCobrancaList(List<CobrancaView> views){
		List<ClienteInadimplenteDTO> dtos = new ArrayList<ClienteInadimplenteDTO>();
	
		if(views != null){
			for (CobrancaView view : views) {
				dtos.add(new ClienteInadimplenteDTO(view));
			}
		}
		return dtos;
	}
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesFromCobrancaListXLS(List<CobrancaView> views){
		List<ClienteInadimplenteDTO> dtos = new ArrayList<ClienteInadimplenteDTO>();
	
		if(views != null){
			for (CobrancaView view : views) {
				ClienteInadimplenteDTO aux = new ClienteInadimplenteDTO(view);
				aux.setNmCidade(view.getNmCidade());
				aux.setNmBairro(view.getNmBairro());
				aux.setNmLogradouro(view.getNmLogradouro());
				aux.setNrNumero(view.getNrNumero());
				aux.setNmComplemento(view.getNmComplemento());
				aux.setNmPontoReferencia(view.getNmPontoReferencia());
				aux.setNrCEP(view.getNrCEP());
				dtos.add(aux);
			}
		}
		return dtos;
	}
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesFromCobrancaContatoList(List<CobrancaContatoView> views){
		List<ClienteInadimplenteDTO> dtos = new ArrayList<ClienteInadimplenteDTO>();
	
		if(views != null){
			for (CobrancaContatoView view : views) {
				dtos.add(new ClienteInadimplenteDTO(view));
			}
		}
		return dtos;
	}
	
	public List<ClienteContatoCobrancaDTO> getClientesContatoCobrancaList(Long idCliente){
		List<ClienteContatoCobrancaDTO> list = new ArrayList<ClienteContatoCobrancaDTO>();
		
	 	List<Sac> sacList = saqDAO.getSacCobrancaListByCliente(idCliente);
		
	 	for (Sac sac : sacList) {
			list.add(new ClienteContatoCobrancaDTO(sac));
		}
		
		return list;
	}
	
	public ClienteContatoCobrancaDTO getClienteContatoCobrancaByIdSac(Long idSac){
		Sac sac = saqDAO.searchByKey(Sac.class, idSac);
		
		ClienteContatoCobrancaDTO dto = null;

		if(sac != null){
			dto = new ClienteContatoCobrancaDTO(sac);
		}
		
		return dto;
	}
	
	public ClienteContatoCobrancaDTO getContatoCobrancaEmAbertoByIdCliente(Long idCliente){
		Sac sac = saqDAO.getSacCobrancaEmAbertoByCliente(idCliente);
		
		ClienteContatoCobrancaDTO dto = null;

		if(sac != null){
			dto = new ClienteContatoCobrancaDTO(sac);
		}
		
		return dto;
	}
	
	public List<TipoContatoDTO> getTipoContatoOptions(){
		List<TipoContatoDTO> list = new ArrayList<TipoContatoDTO>();
		
		for (TipoContato tipo : TipoContato.values()) {
			list.add(new TipoContatoDTO(tipo.getId(), tipo.getDescription()));
		}
		return list;
	}
	
	public ClienteContatoCobrancaDTO saveContatoCobrancaCliente(ClienteContatoCobrancaDTO contatoCobrancaDTO, Long idCliente){
		Sac sac = getSacFromCobrancaDTO(contatoCobrancaDTO);
		sac.setInSituacao(SituacaoSacCobranca.ABERTO.getId());
		
		Sac sacEmAberto = saqDAO.getSacCobrancaEmAbertoByCliente(idCliente);
		if(sacEmAberto != null && sac.getId() == null){
			sac = sacEmAberto;
		}
		
		ClienteContatoCobrancaDTO cobrancaDTO = null;
		Cliente cliente = clienteDAO.searchByKey(Cliente.class, idCliente); 
		
		sac.setCliente(cliente);	
		sac.getCobranca().setInStatus(SituacaoSacCobranca.ABERTO.getId());
		
		if(sac.getId() == null){
			//antes de criar um novo atendimento devemos fechar a cobrança.
			//o status é uma flag para informar se a cobrança foi ou não efetuada
			//dizemos que uma cobrança foi efetuada quando um novo atendimento é iniciado
			Cobranca cobranca = cobrancaDAO.getCobrancaStatusAbertoByCliente(idCliente);
			
			if(cobranca != null)
				cobranca.setInStatus(SituacaoSacCobranca.FECHADO.getId());
	
			sac.setFuncionario1(securityService.getFuncionarioLogado());
			saqDAO.persist(sac);
		}else{
			sac.setFuncionario2(securityService.getFuncionarioLogado());
			saqDAO.update(sac);
		}
		
		cobrancaDTO = new ClienteContatoCobrancaDTO(sac);
		return cobrancaDTO;
	}
	
	public void finalizarCobranca(Long idSac, String duracaoContato){
		try{
			Sac sac = saqDAO.searchByKey(Sac.class, idSac);
			
			Date currentDate = new Date();
			Time currentTime = new Time(currentDate.getTime());	
			Time duracao = new Time(DateUtil.parseDate(duracaoContato, DatePattern.HHMMSS.getPattern()).getTime());
			
			String horaInicioAtendimentoString = DateUtil.calculaDiferencaEntreHorariosAsString(duracao, currentTime);
			Time horaInicioAtendimento = new Time ((DateUtil.parseDate(horaInicioAtendimentoString, DateUtil.DatePattern.HHMMSS.getPattern())).getTime());
			
			sac.setHrFinal(currentTime);
			sac.setHrInicial(horaInicioAtendimento);
			
			sac.setInSituacao(SituacaoSacCobranca.FECHADO.getId());
		} catch (ObjectNotFoundException ex ){
			throw new CobrancaException(
			        props.getProperty("cobranca.nao.criada"),
			        ErrorType.WARNING);
		}
	}
	
	public ClienteContatoCobrancaDTO getClienteContatoCobrancaById(Long idSac){
		ClienteContatoCobrancaDTO dto;
		Sac sac = saqDAO.searchByKey(Sac.class, idSac);
		
		dto = new ClienteContatoCobrancaDTO(sac);
		
		return dto;
	}
	
	private Sac getSacFromCobrancaDTO(ClienteContatoCobrancaDTO dto){
		Sac sac = new Sac();
		Cobranca cobranca = new Cobranca();

		Date currentDate = new Date();
		Time currentTime = new Time(currentDate.getTime());	
		
		sac.setData(currentDate);
		sac.setHrInicial(currentTime);
		sac.setHrFinal(currentTime);
		sac.setId(dto.getIdSac());
		sac.setTipoSac(dto.getTipoContato().getId());
		sac.setObservacao(dto.getNmConversa());
		cobranca.setPrevisaoPagamento(dto.getDtPrevistaPagamento());
		cobranca.setProximaLigacao(dto.getDtProximaLigacao());

		cobranca.setSac(sac);
		sac.setCobranca(cobranca);
		return sac;
	}
	
	public Long getQuantidadeCobrancaByFuncionario(String login, String dataInicio, String dataFim){
		Date dtInicio = null;
		Date dtFim = null;
		
		if(dataInicio != null && dataFim != null){
			dtInicio = DateUtil.parseDate(dataInicio, DateUtil.DatePattern.DDMMAA.getPattern());
			dtFim = DateUtil.parseDate(dataFim, DateUtil.DatePattern.DDMMAA.getPattern());
		}

		Funcionario funcionario = funcionarioDAO.getFuncionarioPorLogin(login);
		Long quantidade = cobrancaDAO.getQuantidadeCobrancaByFuncionario(funcionario.getId(), dtInicio, dtFim);
		
		return quantidade;
	}
	
	/* INICIO COBRANCA COMPARTILHADA */
	
	public EmpresaGrupoCompartilhado getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(Long id){
		EmpresaGrupoCompartilhado empresa = cobrancaDAO.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(id);
		return empresa;
	}
	
	private List<ClienteInadimplenteDTO> getClientesInadimplentesFromCobrancaCompartilhadaList(List<CobrancaCompartilhadoView> views){
		List<ClienteInadimplenteDTO> dtos = new ArrayList<ClienteInadimplenteDTO>();
	
		if(views != null){
			for (CobrancaCompartilhadoView view : views) {
				dtos.add(new ClienteInadimplenteDTO(view));
			}
		}
		return dtos;
	}
	
	private Long getQuantidadeClientesInadimplentesPorSistema(Integer baseDadosSistema, Long idEmpresaGrupo ,Long idFormaPagamento, Integer opcaoData, String nomeCliente, Date dataInicio, Date dataFim, Integer startValuePage, Integer endValuePage, Boolean boAdimplente){
	
		Long quantidadeCliente = null;
		Integer dias = null;
		Boolean isSearchForOptionData = false;
		Boolean isSearchByCliente = nomeCliente != null ? true:false;
		
		if(opcaoData != null && dataInicio != null && dataFim != null) {
			isSearchForOptionData = true;
			dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
		}
		
		switch ( baseDadosSistema ) {
			case 0:
				if( isSearchByCliente ) {
					quantidadeCliente = cobrancaCompartilhaViewMedicDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, null, nomeCliente, dataInicio, dataFim, boAdimplente);
					return quantidadeCliente;
				}
				
				if( isSearchForOptionData ) {
					if( opcaoData == 2  ) // periodo de inadimplencia
						quantidadeCliente = cobrancaCompartilhaViewMedicDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
					else if( opcaoData == 3 ) // inadimplencia recente
						quantidadeCliente = cobrancaCompartilhaViewMedicDAO.getQuantidadeClientesInadimplentesByInadimplenciaRecente(idEmpresaGrupo, idFormaPagamento, dias);
				}
				break;
			case 1:
				if( isSearchByCliente ) {
					quantidadeCliente = cobrancaCompartilhaViewAcademiaDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, null, nomeCliente, dataInicio, dataFim, boAdimplente);
					return quantidadeCliente;
				}
				
				if( isSearchForOptionData ) {
					if( opcaoData == 2  ) 
						quantidadeCliente = cobrancaCompartilhaViewAcademiaDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
					else if( opcaoData == 3 )
						quantidadeCliente = cobrancaCompartilhaViewAcademiaDAO.getQuantidadeClientesInadimplentesByInadimplenciaRecente(idEmpresaGrupo, idFormaPagamento, dias);
				}
				break;
			case 2:
				if( isSearchByCliente ) {
					quantidadeCliente = cobrancaCompartilhaViewDentalDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, null, nomeCliente, dataInicio, dataFim, boAdimplente);
					return quantidadeCliente;
				}
				
				if( isSearchForOptionData ) {
					if( opcaoData == 2  )
						quantidadeCliente = cobrancaCompartilhaViewDentalDAO.getQuantidadeClientesInadimplentes(idEmpresaGrupo, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, boAdimplente);
					else if( opcaoData == 3 )
						quantidadeCliente = cobrancaCompartilhaViewDentalDAO.getQuantidadeClientesInadimplentesByInadimplenciaRecente(idEmpresaGrupo, idFormaPagamento, dias);
				}
				break;
			default:
				break;
		}
		
		

		return quantidadeCliente;
	}
	
	private List<ClienteInadimplenteDTO> getListClientesInadimplentesPorSistema (Integer baseDadosSistema, Long idEmpresa, Long idFormaPagamento, Integer opcaoData, String nomeCliente, Date dataInicio, Date dataFim, Integer startValuePage, Integer endValuePage, Boolean boAdimplente){
		List<CobrancaCompartilhadoView> cobrancasList = null;
		
		Integer dias = null;
		Boolean isSearchForOptionData = false;
		Boolean isSearchByCliente = nomeCliente != null ? true:false;
		
		if(opcaoData != null && dataInicio != null && dataFim != null) {
			isSearchForOptionData = true;
			dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
		}
		
		switch ( baseDadosSistema ) {
		case 0:
			if( isSearchByCliente ) {
				cobrancasList = cobrancaCompartilhaViewMedicDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
			}
			
			if( isSearchForOptionData ) {
				if( opcaoData == 2  ) { // periodo de inadimplencia
					cobrancasList = cobrancaCompartilhaViewMedicDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				} else if( opcaoData == 3 ) { // inadimplencia recente
					cobrancasList = cobrancaCompartilhaViewMedicDAO.getClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				}
			}
			break;
		case 1:
			if( isSearchByCliente ) {
				cobrancasList = cobrancaCompartilhaViewAcademiaDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
			}
			
			if( isSearchForOptionData ) {
				if( opcaoData == 2  ) { // periodo de inadimplencia
					cobrancasList = cobrancaCompartilhaViewAcademiaDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				} else if( opcaoData == 3 ) { // inadimplencia recente
					cobrancasList = cobrancaCompartilhaViewAcademiaDAO.getClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				}
			}
			break;
		case 2:
			if( isSearchByCliente ) {
				cobrancasList = cobrancaCompartilhaViewDentalDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
				return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
			}
			
			if( isSearchForOptionData ) {
				if( opcaoData == 2  ) { // periodo de inadimplencia
					cobrancasList = cobrancaCompartilhaViewDentalDAO.getClientesInadimplentesFromCobrancaView(idEmpresa, idFormaPagamento, opcaoData, nomeCliente, dataInicio, dataFim, startValuePage, endValuePage, boAdimplente);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				} else if( opcaoData == 3 ) { // inadimplencia recente
					cobrancasList = cobrancaCompartilhaViewDentalDAO.getClientesInadimplentesByInadimplenciaRecente(idEmpresa, idFormaPagamento, startValuePage, endValuePage, dias);
					return getClientesInadimplentesFromCobrancaCompartilhadaList(cobrancasList);
				}
			}
			break;
		default:
			break;
		}
		
		return null;
	}
	
	public ClientesPaginadosDTO getClientesInadimplentesPorSistema(EmpresaGrupoCompartilhado empresa , Long idFormaPagamento, Integer opcaoData, 
			String nomeCliente, String dataInicio, String dataFim, Integer pagina, Integer itensPagina, Boolean boAdimplente){
		
		Integer startValuePage = null, endValuePage = null;
		Date dtInicio = null, dtFim = null;
		
		Long quantidadeCliente = null;
		List<ClienteInadimplenteDTO> clientesInadimplentesDTO = null;
		
		dtInicio = DateUtil.parseDate(dataInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		dtFim = DateUtil.parseDate(dataFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
		Long idEmpresaGrupo = empresa.getIdEmpresaGrupoExterno();
		Integer baseDadosSistema = empresa.getInSistemaExterno();
		
		ClientesPaginadosDTO paginado = new ClientesPaginadosDTO();
		
		
		if(pagina != null && itensPagina != null){
			startValuePage = ((pagina-1) * itensPagina);
			endValuePage = itensPagina;
		}
		
		quantidadeCliente = getQuantidadeClientesInadimplentesPorSistema(baseDadosSistema, idEmpresaGrupo ,idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
		paginado.setQuantidadeClientesTotal(quantidadeCliente);
		
		if(quantidadeCliente == null || quantidadeCliente <= 0)
			return paginado;
		
		clientesInadimplentesDTO = getListClientesInadimplentesPorSistema(baseDadosSistema, idEmpresaGrupo, idFormaPagamento, opcaoData, nomeCliente, dtInicio, dtFim, startValuePage, endValuePage, boAdimplente);
		paginado.setClientes(clientesInadimplentesDTO);
		
		return paginado;
	}
	
	
	public List<MensalidadeDTO> getMensalidadesByClienteInadimplente (EmpresaGrupoCompartilhado empresa, Long idCliente, String dtInicio, String dtFim){
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();		
		Integer dias = 0;
		
		try {
		
			Date dataInicio = DateUtil.parseDate(dtInicio, DateUtil.DatePattern.DDMMAA.getPattern());
			Date dataFim = DateUtil.parseDate(dtFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
			dias = DateUtil.calculaPeriodoEmDias(dataInicio, dataFim);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		switch ( baseDadosSistema ) {
		case 0:
			return mensalidadeMedicDAO.getMensalidadeByClienteInadimplente(idCliente, dias);
		case 1:
			return mensalidadeCompartilhadoPeformanceDAO.getMensalidadeByClienteInadimplente(idCliente, dias);
		case 2:
			return mensalidadeCompartilhadoDentalDAO.getMensalidadeByClienteInadimplente(idCliente, dias);
		default:
			return null;
		}
	}
	
	public List<MensalidadeDTO> getMensalidadesByClienteInadimplentePorPeriodo (EmpresaGrupoCompartilhado empresa, Long idCliente, String dtInicio, String dtFim){
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();		
						
		switch ( baseDadosSistema ) {
		case 0:
			return  mensalidadeMedicDAO.getMensalidadeByClienteInadimplentePorPeriodo(idCliente, dtInicio, dtFim);
		default:
			return null;
		}
	}
	
	public Collection<Parcela> getCobrancaByMensalidade (EmpresaGrupoCompartilhado empresa, Long idMensalidade){
		
		Integer baseDadosSistema = empresa.getInSistemaExterno();		
		
		try {
		
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		switch ( baseDadosSistema ) {
		case 0:
			return mensalidadeMedicDAO.getCobrancaByMensalidade(idMensalidade);
		case 1:
			return mensalidadeCompartilhadoPeformanceDAO.getCobrancaByMensalidade(idMensalidade);
		case 2:
			return mensalidadeCompartilhadoDentalDAO.getCobrancaByMensalidade(idMensalidade);
		default:
			return null;
		}
	}
	
}
