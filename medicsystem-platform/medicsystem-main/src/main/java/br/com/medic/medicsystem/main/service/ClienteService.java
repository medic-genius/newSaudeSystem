package br.com.medic.medicsystem.main.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.ErrorResponse;
import br.com.medic.medicsystem.main.appmobile.response.FailResponse;
import br.com.medic.medicsystem.main.appmobile.response.SuccessResponse;
import br.com.medic.medicsystem.main.util.ValidaCPF;
import br.com.medic.medicsystem.persistence.dao.AgenciaDAO;
import br.com.medic.medicsystem.persistence.appmobile.dao.UsuarioLoginDAO;
import br.com.medic.medicsystem.persistence.appmobile.dto.ClienteInfoDTO;
import br.com.medic.medicsystem.persistence.appmobile.dto.DigitalCardDTO;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioLogin;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AnamneseDAO;
import br.com.medic.medicsystem.persistence.dao.BairroDAO;
import br.com.medic.medicsystem.persistence.dao.BancoDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.CoberturaPlanoDAO;
import br.com.medic.medicsystem.persistence.dao.ConfiguracaoSistemaDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.CredenciadaDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.dao.EnderecoCobrancaDAO;
import br.com.medic.medicsystem.persistence.dao.ImagemClienteDAO;
import br.com.medic.medicsystem.persistence.dao.NewProspectDAO;
import br.com.medic.medicsystem.persistence.dao.OrgaoDAO;
import br.com.medic.medicsystem.persistence.dao.PreAnamneseDAO;
import br.com.medic.medicsystem.persistence.dao.PreAnamnesePacienteDAO;
import br.com.medic.medicsystem.persistence.dao.TotemDAO;
import br.com.medic.medicsystem.persistence.dto.AnamneseDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteDTO;
import br.com.medic.medicsystem.persistence.dto.ClienteFpDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteSearchDTO;
import br.com.medic.medicsystem.persistence.dto.CoberturaPlanoDTO;
import br.com.medic.medicsystem.persistence.dto.CobrancaDTO;
import br.com.medic.medicsystem.persistence.dto.DependenteCadastroDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteCadastroDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteViewDTO;
import br.com.medic.medicsystem.persistence.dto.SituacaoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agencia;
import br.com.medic.medicsystem.persistence.model.Anamnese;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Banco;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.ConfiguracaoSistema;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.EnderecoCobranca;
import br.com.medic.medicsystem.persistence.model.Foto;
import br.com.medic.medicsystem.persistence.model.ImagemCliente;
import br.com.medic.medicsystem.persistence.model.Informativo;
import br.com.medic.medicsystem.persistence.model.NewProspect;
import br.com.medic.medicsystem.persistence.model.ObservacaoCliente;
import br.com.medic.medicsystem.persistence.model.Orgao;
import br.com.medic.medicsystem.persistence.model.PreAnamnese;
import br.com.medic.medicsystem.persistence.model.PreAnamnesePaciente;
import br.com.medic.medicsystem.persistence.model.views.AgendamentoView;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedico;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedicoAnamneseView;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;
import br.com.medic.medicsystem.persistence.model.views.DespesaView;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.security.keycloak.KeycloakUtil;
import br.com.medic.medicsystem.persistence.utils.Utils;

@Stateless
public class ClienteService {

	@Inject
	private Logger logger;

	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;

	@Inject
	@Named("credenciada-dao")
	private CredenciadaDAO credenciadaDAO;

	@Inject
	@Named("preanamnesepaciente-dao")
	private PreAnamnesePacienteDAO preanamnesepacienteDAO;

	@Inject
	@Named("preanamnese-dao")
	private PreAnamneseDAO preAnamneseDAO;

	@Inject
	@Named("imagemcliente-dao")
	private ImagemClienteDAO imagemClienteDAO;

	@Inject
	@Named("configuracaosistema-dao")
	private ConfiguracaoSistemaDAO configuracaoSistemaDAO;

	@Inject
	@Named("coberturaplano-dao")
	private CoberturaPlanoDAO coberturaPlanoDAO;

	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;

	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;

	@Inject
	@Named("bairro-dao")
	private BairroDAO bairroDAO;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;

	@Inject
	@Named("endereco-cobranca-dao")
	private EnderecoCobrancaDAO enderecoCobrancaDAO;

	@Inject
	@Named("anamnese-dao")
	AnamneseDAO anamneseDAO;

	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("newprospect-dao")
	private NewProspectDAO newprospectDAO;
	
	@Inject
	@Named("orgao-dao")
	private OrgaoDAO orgaoDAO;
	
	@Inject
	@Named("banco-dao")
	private BancoDAO bancoDAO;
	
	@Inject
	@Named("agencia-dao")
	private AgenciaDAO agenciaDAO;
	
	@Inject
	@Named("totem-dao")
	private TotemDAO totemDAO;
	
	@Inject
	private SecurityService securityService;
	
	@Inject
	private KeycloakUtil keycloakUtil;
	
	@Inject
	@Named("usuario-login-dao")
	private UsuarioLoginDAO usuarioLoginDAO;
	
	@Inject
	@Named("empresagrupo-dao")
	private EmpresaGrupoDAO empresaGrupoDAO;

	public Cliente getCliente(Long id) {

		logger.debug("Obtendo um cliente...");

		return clienteDAO.searchByKey(Cliente.class, id);
	}

	public List<ClienteView> getClientes(String nrCodCliente, String nmCliente, String nrCPF, String nrRG,
			Integer startPosition, Integer maxResults) {

		logger.debug("Obtendo lista de clientes...");
		nrCPF = cleanupNumberOnlyVariables(nrCPF);
		return clienteDAO.getClientes(nrCodCliente, nmCliente, nrCPF, nrRG, startPosition, maxResults);
	}
	
	public Cliente saveCliente(Cliente cliente, Integer tipoCliente) {
		return this.saveCliente(cliente, tipoCliente, null);
	}

	public Cliente saveCliente(Cliente cliente, Integer tipoCliente, EmpresaGrupo empresaGrupo) {
		cliente.setNrCPF(cleanupNumberOnlyVariables(cliente.getNrCPF()));
		cliente.setNrRG(cleanupNumberOnlyVariables(cliente.getNrRG()));
		cliente.setNrCEP(cleanupNumberOnlyVariables(cliente.getNrCEP()));

		if (!ValidaCPF.isCPF(cliente.getNrCPF())) {
			throw new IllegalArgumentException("[CPF Invalido].");
		}

		Cliente encontrato = clienteDAO.getClientePorCPF(cliente.getNrCPF());

		if (encontrato != null) {
			throw new IllegalArgumentException("[CPF Invalido/Cliente existente].");
		}

		cliente.setCidade(cliente.getCidade() != null && cliente.getCidade().getId() != null
				? cidadeDAO.getCidade(cliente.getCidade().getId()) : null);
		cliente.setBairro(cliente.getBairro() != null && cliente.getBairro().getId() != null
				? bairroDAO.getBairro(cliente.getBairro().getId()) : null);
		cliente.setDtInclusao(new Date());

		EnderecoCobranca enderecoCobranca = new EnderecoCobranca();
		// TODO get info from logged user or from default place
		enderecoCobranca.setNmLogradouro("AV. JOAQUIM NABUCO");
		enderecoCobranca.setNrNumero("2213");
		enderecoCobranca.setNmComplemento("");
		enderecoCobranca.setNrCep("69020031");
		enderecoCobranca.setNmPontoReferencia("");
		enderecoCobranca.setBoEnderecoIgual(false);
		enderecoCobranca.setCidade(cidadeDAO.getCidade(29L)); // Manaus
		enderecoCobranca.setBairro(bairroDAO.getBairro(878L)); // Centro

		enderecoCobranca = enderecoCobrancaDAO.persist(enderecoCobranca);
		cliente.setEnderecoCobranca(enderecoCobranca);

		cliente.setNrCodCliente(gerarCodigoCliente());
		
		if(tipoCliente.equals(0)) {
			cliente.setBoNaoAssociado(true);
		}

		cliente = clienteDAO.persist(cliente);

		// Cria contrato que nao tera mensalidade e nem plano fixo
		if(tipoCliente.equals(0)){ //Cliente nao associado			
		
			Contrato contrato = new Contrato();
			contrato.setBoCadastradoBancoBrasil(false);
			contrato.setBoNaoFazUsoPlano(false);
			contrato.setDtContrato(new Date());
			contrato.setInformaPagamento(0);
			contrato.setVlContrato(0D);
			contrato.setVlDesconto(0D);
			contrato.setVlTotal(0D);
			contrato.setSituacao(0);
			contrato.setBoBloqueado(false);
	
			// TODO get from logged user
//			EmpresaGrupo eg = new EmpresaGrupo();
//			eg.setId(3L);
//			contrato.setEmpresaGrupo(eg);
			if(empresaGrupo != null) {
				contrato.setEmpresaGrupo(empresaGrupo);
			} else {
				contrato.setEmpresaGrupo(securityService.getFuncionarioLogado().getUnidade().getEmpresaGrupo());
			}
	
			// Associa o contrato criado ao cliente
			ContratoCliente contratocliente = new ContratoCliente();
			contratocliente.setCliente(cliente);
			contratocliente.setContrato(contrato);
			contratocliente.setBoAtivo(true);
			contratocliente.setPlano(null);
			contratocliente.setInTipoCliente(4); // Cliente Não Associado
			contrato.addContratoCliente(contratocliente);
	
			contrato.setNrContrato(gerarCodigoContrato());
	
			contratoDAO.persist(contrato);
			
		}
		
		return cliente;
	}
	
	public ContratoCliente createContratoForCliente(Cliente cliente, EmpresaGrupo eg) {		
		Contrato contrato = new Contrato();
		contrato.setBoCadastradoBancoBrasil(false);
		contrato.setBoNaoFazUsoPlano(false);
		contrato.setDtContrato(new Date());
		contrato.setInformaPagamento(0);
		contrato.setVlContrato(0D);
		contrato.setVlDesconto(0D);
		contrato.setVlTotal(0D);
		contrato.setSituacao(0);
		contrato.setBoBloqueado(false);

		// TODO get from logged user
		if(eg == null) {
			eg = empresaGrupoDAO.searchByKey(EmpresaGrupo.class, 9L);
		}
		contrato.setEmpresaGrupo(eg);
		//					contrato.setEmpresaGrupo(securityService.getFuncionarioLogado().getUnidade().getEmpresaGrupo());

		// Associa o contrato criado ao cliente
		ContratoCliente contratocliente = new ContratoCliente();
		contratocliente.setCliente(cliente);
		contratocliente.setContrato(contrato);
		contratocliente.setBoAtivo(true);
		contratocliente.setPlano(null);
		contratocliente.setInTipoCliente(4); // Cliente Não Associado
		contrato.addContratoCliente(contratocliente);

		contrato.setNrContrato(gerarCodigoContrato());

		Contrato c = contratoDAO.persist(contrato);
		if(c != null) {
			return contratocliente;
		}
		return null;
	}

	public List<Informativo> getInformativo() {
		List<Informativo> listaInformativosAux = clienteDAO.getInformativo();

		return listaInformativosAux;
	}

	private String cleanupNumberOnlyVariables(String var) {
		if (var != null) {
			var = var.replaceAll("\\s", "");
			var = var.trim();
			var = var.replaceAll("[^0-9]", "");

			return var;
		}

		return null;
	}
	
	public Boolean updateLatitudeLongitude(List<ClienteDTO> clientes){
		for (ClienteDTO clienteDTO : clientes) {
			Cliente cliente = clienteDAO.searchByKey(Cliente.class, clienteDTO.getIdCliente());
			cliente.setLongitude(clienteDTO.getLongitude());
			cliente.setLatitude(clienteDTO.getLatitude());
			clienteDAO.update(cliente);
		}
		
		return new Boolean (true);
	}

	public Cliente updateCliente(Cliente cliente) {

		cliente.setNrCEP(Utils.removeSpecialChars(cliente.getNrCEP()));
		if (cliente.getFotoClienteEncoded() != null) {

			try {
				cliente.setFotoCliente(cliente.getFotoClienteEncoded().getBytes("ISO-8859-1"));

			} catch (UnsupportedEncodingException e) {

				return null;
			}
		}

		EnderecoCobranca ec = cliente.getEnderecoCobranca();

		if (ec != null) {

			if (ec.getBairro() != null && ec.getBairro().getId() != null) {
				Bairro bairro = bairroDAO.getBairro(ec.getBairro().getId());
				ec.setBairro(bairro);
			}
			if (ec.getId() != null) {
				enderecoCobrancaDAO.update(ec);
			} else {
				ec = enderecoCobrancaDAO.persist(ec);
			}

			cliente.setEnderecoCobranca(ec);
		}

		cliente.setDtAlteracao(new Date());

		Cliente clienteOriginal = clienteDAO.searchByKey(Cliente.class, cliente.getId());

		if (clienteOriginal.getFotoCliente() != null && cliente.getFotoClienteEncoded() == null) {
			cliente.setFotoCliente(clienteOriginal.getFotoCliente());
		}

		if (cliente.getDigitalCliente() == null && clienteOriginal.getDigitalCliente() != null) {

			cliente.setDigitalCliente(clienteOriginal.getDigitalCliente());

		}

		cliente = clienteDAO.update(cliente);

		return cliente;
	}

	public List<ImagemCliente> listImagemCliente(Long id) {
		return imagemClienteDAO.getImagensPorCliente(id);
	}

	public File getFileImagemCliente(Long id) {

		return new File(getFullPathFileName(id));

	}

	private String getFullPathFileName(Long id) {

		ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
		ImagemCliente imagemCliente = imagemClienteDAO.searchByKey(ImagemCliente.class, id);

		return configuraSistema.getNmPathImagem() + configuraSistema.getNmSeparador()
				+ imagemCliente.getCliente().getNrCodCliente() + configuraSistema.getNmSeparador() + "/"
				+ imagemCliente.getNmImagemCliente();
	}

	public List<AgendamentoView> getAgendamentos(Long idCliente, boolean todos) {
		return clienteDAO.getAgendamentos(idCliente, todos);
	}

	public List<Dependente> getDependentes(Long idCliente) {
		return clienteDAO.getDependentes(idCliente);
	}

	public Collection<Contrato> getContratos(Long idCliente) {
		return clienteDAO.getContratos(idCliente);
	}

	public List<ObservacaoCliente> getObservacaoCliente(Long idCliente) {
		return clienteDAO.getObservacaoCliente(idCliente);
	}

	public List<CoberturaPlanoDTO> getCoberturaPlano(Long idCliente) {
		return coberturaPlanoDAO.getCoberturaPlanoPorCliente(idCliente);
	}

	public Collection<ContratoDependente> getDependentesContratos(Long idCliente, Long idDependente) {
		return clienteDAO.getDependentesContratos(idCliente, idDependente);
	}

	public Dependente getDependente(Long id) {
		logger.debug("Obtendo um cliente...");

		return dependenteDAO.searchByKey(Dependente.class, id);
	}

	private String gerarCodigoCliente() {

		Integer codigo = clienteDAO.getUltimoCodigoGerado();

		return String.format("%06d", codigo == 0 ? 1 : codigo + 1);
	}

	private String gerarCodigoContrato() {

		String ultimoNumeroGerado = contratoDAO.getUltimoCodigoGerado();

		Calendar calendar = Calendar.getInstance();
		int anoAtual = calendar.get(Calendar.YEAR);

		if (ultimoNumeroGerado == null)
			return String.format("%06d/%d", 1, anoAtual);

		int sequencial = Integer.parseInt(ultimoNumeroGerado.split("/")[0]);
		int ano = Integer.parseInt(ultimoNumeroGerado.split("/")[1]);

		return String.format("%06d/%d", ano == anoAtual ? sequencial + 1 : 1, anoAtual);
	}

	public List<SituacaoDTO> getSituacaoCliente(Long idCliente) {
		return clienteDAO.listSituacaoCliente(idCliente);
	}


	public List<CobrancaDTO> getSituacaoFinanceiraQuitada(Long idCliente, String nrContrato) {
		return clienteDAO.getSituacaoFinanceiraQuitada(idCliente, nrContrato);
	}

	public List<ClienteDTO> getClienteDTO(Integer inTipo,String dtInicio,String dtFim, List<Long> idUnidade) {
		return clienteDAO.getClienteDTO(inTipo, dtInicio, dtFim, idUnidade);
	}
	
	public List<ClienteDTO> getClienteDTOLatLng() {
		return clienteDAO.getClienteDTOLatLng();
	}
	
	public Dependente getDependenteById(Long idDependente) {
		return dependenteDAO.searchByKey(Dependente.class, idDependente);
	}

	public List<DespesaView> getClienteDespesas(Long idCliente) {
		return clienteDAO.getClienteDespesas(idCliente);
	}

	public Double getSomaValorDebito(Long idCliente) {
		Double vlDebito = 0D;

		Double fp1 = agendamentoDAO.getSomaValorDebitoBoleto(1, idCliente.toString());

		Double fp15 = agendamentoDAO.getSomaValorDebitoBoleto(15, idCliente.toString());

		Double fp17 = agendamentoDAO.getSomaValorDebitoBoleto(17, idCliente.toString());

		vlDebito += fp1 != null ? fp1 : 0D;
		vlDebito += fp15 != null ? fp15 : 0D;

		vlDebito += fp17 != null ? fp17 : 0D;

		return vlDebito;
	}

	public Integer desbloquearCliente(Long idCliente) {
		return clienteDAO.desbloquearCliente(idCliente);
	}

	public Integer bloquearCliente(Long idCliente) {
		return clienteDAO.bloquearCliente(idCliente);
	}

	public List<AtendimentoMedico> getAtendimentoMedicoHistoricoView(Long idPaciente, Long idEspecialidade,
			Character tipoPaciente) {

		return clienteDAO.getAtendimentoMedicoHistoricoView(idPaciente, idEspecialidade, tipoPaciente);
	}

	public List<AtendimentoMedicoAnamneseView> getAtendimentoMedicoAnamneseView(Long idPaciente,
			String inTipoPaciente) {

		return clienteDAO.getAtendimentoMedicoAnamneseView(idPaciente, inTipoPaciente);
	}

	public AnamneseDTO getAnamnese(Long idPaciente, String inTipoPaciente) {
		AnamneseDTO anaDTO = new AnamneseDTO();
		Anamnese ana = clienteDAO.getAnamnese(idPaciente, inTipoPaciente);
		if (ana != null) {
			anaDTO.setIdAnamnese(ana.getId() != null ? ana.getId() : null);
			anaDTO.setNmDoencaAnterior(ana.getNmDoencaAnterior() != null ? ana.getNmDoencaAnterior() : "");
			anaDTO.setNmDoencaFamilia(ana.getNmDoencaFamilia() != null ? ana.getNmDoencaFamilia() : "");
			anaDTO.setNmTipoAlergia(ana.getNmTipoAlergia() != null ? ana.getNmTipoAlergia() : "");
		}
		return anaDTO;
	}

	public boolean updateAnamnese(AnamneseDTO anamnese,
			List<AtendimentoMedicoAnamneseView> atendimentomedicoanamneseview) {
		boolean anamneseAtualizada = true;
		if (anamnese != null) {
			Anamnese anamneseOriginal = clienteDAO.getAnamnesePorId(anamnese.getIdAnamnese());

			anamneseOriginal.setNmDoencaAnterior(anamnese.getNmDoencaAnterior());
			anamneseOriginal.setNmDoencaFamilia(anamnese.getNmDoencaAnterior());
			anamneseOriginal.setNmTipoAlergia(anamnese.getNmTipoAlergia());

			try {
				anamneseDAO.update(anamneseOriginal);

			} catch (Exception e) {
				anamneseAtualizada = false;
				throw new ObjectNotFoundException("Erro ao atualizar Anamnese");
			}
		}

		if (atendimentomedicoanamneseview.size() > 0) {

			if (atendimentomedicoanamneseview.get(0).getIdPreanamnesePaciente() != null) {
				for (AtendimentoMedicoAnamneseView atview : atendimentomedicoanamneseview) {
					PreAnamnesePaciente preAnamnesePacienteOriginal = preanamnesepacienteDAO
							.searchByKey(PreAnamnesePaciente.class, atview.getIdPreanamnesePaciente());
					preAnamnesePacienteOriginal.setInResposta(atview.getInResposta());

					try {
						preanamnesepacienteDAO.update(preAnamnesePacienteOriginal);
					} catch (Exception e) {
						anamneseAtualizada = false;
						throw new ObjectNotFoundException("Erro ao atualizar Preanamnese Paciente");
					}
				}
			} else {

				for (AtendimentoMedicoAnamneseView atview : atendimentomedicoanamneseview) {

					PreAnamnesePaciente preAnamnesePacienteOriginal = new PreAnamnesePaciente();
					PreAnamnese preAnamneseOriginal = new PreAnamnese();
					Cliente clienteOriginal = new Cliente();
					Dependente dependenteOriginal = new Dependente();

					if (atview.getIdDependente() != null) {
						dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, atview.getIdDependente());
						preAnamnesePacienteOriginal.setDependente(dependenteOriginal);

					} else {
						clienteOriginal = clienteDAO.searchByKey(Cliente.class, atview.getIdCliente());
						preAnamnesePacienteOriginal.setCliente(clienteOriginal);
					}

					preAnamneseOriginal = preAnamneseDAO.searchByKey(PreAnamnese.class, atview.getIdPreAnamnese());

					preAnamnesePacienteOriginal.setPreAnamnese(preAnamneseOriginal);
					preAnamnesePacienteOriginal.setInResposta(atview.getInResposta());

					try {
						preanamnesepacienteDAO.persist(preAnamnesePacienteOriginal);
					} catch (Exception e) {
						anamneseAtualizada = false;
						throw new ObjectNotFoundException("Erro ao registrar Preanamnese Paciente");
					}
				}

			}
		}

		return anamneseAtualizada;

	}

	public boolean saveAnamnese(AnamneseDTO anamnese,
			List<AtendimentoMedicoAnamneseView> atendimentomedicoanamneseview) {
		Anamnese anamneseOriginal = new Anamnese();
		Cliente clienteOriginal = new Cliente();
		Dependente dependenteOriginal = new Dependente();
		boolean anamneseSalva = true;

		if (anamnese != null) {
			if (anamnese.getInTipoPaciente().equals("D")) {
				dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, anamnese.getIdPaciente());
				anamneseOriginal.setDependente(dependenteOriginal);

			} else {
				clienteOriginal = clienteDAO.searchByKey(Cliente.class, anamnese.getIdPaciente());
				anamneseOriginal.setCliente(clienteOriginal);
			}

			anamneseOriginal.setNmDoencaAnterior(anamnese.getNmDoencaAnterior());
			anamneseOriginal.setNmDoencaFamilia(anamnese.getNmDoencaFamilia());
			anamneseOriginal.setNmTipoAlergia(anamnese.getNmTipoAlergia());

			try {
				anamneseDAO.persist(anamneseOriginal);
			} catch (Exception e) {
				anamneseSalva = false;
				throw new ObjectNotFoundException("Erro ao registrar anamnese");
			}
		}

		if (atendimentomedicoanamneseview != null && atendimentomedicoanamneseview.size() > 0) {

			for (AtendimentoMedicoAnamneseView atview : atendimentomedicoanamneseview) {

				PreAnamnesePaciente preAnamnesePacienteOriginal = new PreAnamnesePaciente();
				PreAnamnese preAnamneseOriginal = new PreAnamnese();
				clienteOriginal = new Cliente();
				dependenteOriginal = new Dependente();

				if (atview.getIdDependente() != null) {
					dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, atview.getIdDependente());
					preAnamnesePacienteOriginal.setDependente(dependenteOriginal);

				} else {
					clienteOriginal = clienteDAO.searchByKey(Cliente.class, atview.getIdCliente());
					preAnamnesePacienteOriginal.setCliente(clienteOriginal);
				}

				preAnamneseOriginal = preAnamneseDAO.searchByKey(PreAnamnese.class, atview.getIdPreAnamnese());

				preAnamnesePacienteOriginal.setPreAnamnese(preAnamneseOriginal);
				preAnamnesePacienteOriginal.setInResposta(atview.getInResposta());

				try {
					preanamnesepacienteDAO.persist(preAnamnesePacienteOriginal);
				} catch (Exception e) {
					anamneseSalva = false;
					throw new ObjectNotFoundException("Erro ao registrar Preanamnese Paciente");
				}
			}

		}

		return anamneseSalva;

	}
	
	public ClienteFpDTO getClienteFpPorCPFMatricula(String nrCPF, String nrMatricula) {

		logger.debug("Obtendo cliente(funcionario publico)...");

		return clienteDAO.getClienteFpPorCPFMatricula(nrCPF, nrMatricula);
	}
	
	public Cliente ClienteByCPF(String nrCPF) {


		return clienteDAO.getClientePorCPF(nrCPF);
	}
	
	public NewProspect getNewProspectbyCpf(String nrCpf){
		
		NewProspect newProspect = newprospectDAO.getNewProspectbyCpf(nrCpf);
		
		if(newProspect != null){
			Orgao orgao = orgaoDAO.getOrgaobyCod(newProspect.getNrOrgao());
			if(orgao != null)
				newProspect.setOrgao( orgao );
			
			Banco banco = bancoDAO.getBancobyCod(newProspect.getNrBanco());
			if(banco != null){
				newProspect.setBanco(banco);
			
				Agencia agencia = agenciaDAO.getAgenciabyCod(newProspect.getNrAgencia(), banco.getId());
				if(agencia != null)
					newProspect.setAgencia(agencia);
			}
			
		}
		return newProspect;
	}
	
	public PacienteSearchDTO searchUserByCPF(String nrCpf) {
		// busca todos os cadastros do paciente (como cliente e dependente)
		List<PacienteViewDTO> list = totemDAO.pacienteByCpfCod(nrCpf, null);
		if(list == null || list.isEmpty()) {
			return null;
		}
		
		PacienteSearchDTO searchResult = new PacienteSearchDTO();
		searchResult.setNome(list.get(0).getNmPaciente());
		searchResult.setDtNascimento(list.get(0).getDtNascimento());
		searchResult.setInSexo(list.get(0).getInSexo());
		searchResult.setNmEmail(list.get(0).getNmEmail());
		searchResult.setNrCelular(list.get(0).getNrCelular());
		
		// lista de ids em cadastros como cliente
		List<Long> clienteIds = new LinkedList<Long>();
		for(PacienteViewDTO p : list) {
			boolean isCliente = p.getNrUniqueId().startsWith("1");
			String idStr = p.getNrUniqueId().substring(1);
			
			PacienteCadastroDTO cadastro = new PacienteCadastroDTO();
			cadastro.setIdPaciente(Long.parseLong(idStr));
			cadastro.setDtNascimento(p.getDtNascimento());
			cadastro.setInSexo(p.getInSexo());
			cadastro.setNmEmail(p.getNmEmail());
			cadastro.setNrCelular(p.getNrCelular());
			
			if(isCliente) {
				clienteIds.add(cadastro.getIdPaciente());
				cadastro.setTipo(1);
			} else {
				cadastro.setTipo(2);
			}
			searchResult.addCadastroPaciente(cadastro);
		}
		// paciente possui cadastro como Dependente
		if(!clienteIds.isEmpty()) {
			List<Dependente> dependentes = dependenteDAO.getDependentesFromClienteList(clienteIds);
			if(dependentes != null) {
				for(Dependente d : dependentes) {
					DependenteCadastroDTO depCadastro = new DependenteCadastroDTO();
					depCadastro.setIdPaciente(d.getId());
					depCadastro.setNome(d.getNmDependente());
					
					depCadastro.setDtNascimento(d.getDtNascimento());
					depCadastro.setInSexo(d.getInSexo());
					depCadastro.setNmEmail(d.getNmEmail());
					depCadastro.setNrCelular(d.getNrCelular());
					
					searchResult.addDependente(depCadastro);
				}
			}
		}
		return searchResult;
	}
	
	public ClienteView clienteViewById(Long idCliente) {
		return clienteDAO.clienteViewById(idCliente);
	}
	
	//----------------------------------------------------------
	// APLICATIVO
	public AppResponse getClienteInfo() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user != null) {
			ClienteInfoDTO result = clienteDAO.getClienteInfo(user.getIdUsuario(), user.getTipoUsuario());
			if(result != null) {
				SuccessResponse response = new SuccessResponse(200, result);
				return response;
			}
			return new ErrorResponse(-1, "Houve um erro ao processar solicitação");
		}
		return new ErrorResponse(403, "Usuário não logado ou autorizado");
	}
	
	public AppResponse updateClienteInfo(ClienteInfoDTO info) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user != null) {
			boolean updated = false;
			if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
				Cliente c = clienteDAO.updateClienteInfo(user.getIdUsuario(), info);
				updated = c != null;
			} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
				Dependente d = dependenteDAO.updateDependenteInfo(user.getIdUsuario(), info);
				updated = d != null;
			}
			if(updated) {
				return new SuccessResponse(200, info);
			}
			Map<String, Object> r = new HashMap<String, Object>();
			r.put("message", "Não foi possível atualizar informações");
			return new FailResponse(-8, r);
		}
		return new ErrorResponse(403, "Usuário não logado ou autorizado");
	}
	
	public AppResponse getUsuarioFoto() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuario não logado ou autorizado");
		}
		Foto foto = new Foto();
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			Cliente cliente = clienteDAO.searchByKey(Cliente.class, user.getIdUsuario());
			if(cliente != null) {
				foto.setFoto(cliente.getFotoCliente());
			}
		} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, user.getIdUsuario());
			if(dependente != null) {
				foto.setFoto(dependente.getFotoDependente());
			}
		}
		return new SuccessResponse(200, foto);
	}
	
	public AppResponse updateClienteFoto(Foto newFoto) {
		boolean updated = false;
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user != null) {
			try {
				if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
					Cliente cliente = clienteDAO.searchByKey(Cliente.class, user.getIdUsuario());
					if(newFoto.getFotoEncoded() != null) {
						cliente.setFotoCliente(newFoto.getFotoEncoded().getBytes("ISO-8859-1"));
					} else {
						cliente.setFotoCliente(null);
					}
					updated = clienteDAO.update(cliente) != null;
				} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
					Dependente dependente = dependenteDAO.searchByKey(Dependente.class, user.getIdUsuario());
					if(newFoto.getFotoEncoded() != null) {
						dependente.setFotoDependente(newFoto.getFotoEncoded().getBytes("ISO-8859-1"));
					} else {
						dependente.setFotoDependente(null);
					}
					updated = dependenteDAO.update(dependente) != null;
				}
				if(updated) {
					return new SuccessResponse(200, null);
				}
			} catch(Exception e) {
//				e.printStackTrace();
			}
			return new ErrorResponse(-1, "Houve um erro ao processar solicitação");
		}
		return new ErrorResponse(403, "Usuário não logado ou autorizado");
	}
	
	public AppResponse generateClienteDigitalCard(Boolean onlyQRCode) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		DigitalCardDTO card = new DigitalCardDTO();
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			Cliente cliente = clienteDAO.searchByKey(Cliente.class, user.getIdUsuario());
			if(cliente == null) {
				return new ErrorResponse(-300, "Cliente não encontrado");
			}
			if(!onlyQRCode) {
				card.setIdCliente(cliente.getId());
				card.setNmCliente(cliente.getNmCliente());
				card.setNrCodCliente(cliente.getNrCodCliente());
				card.setNrCPF(cliente.getNrCPF());
				card.setNrRG(cliente.getNrRG());
				if(cliente.getDtNascimento() != null) {
					card.setDtNascimento((new SimpleDateFormat("dd/MM/yyyy")).format(cliente.getDtNascimento()));
				}
				card.setIsAssociado(!cliente.getBoNaoAssociado());
				
				// gerar QR Code com id de usuario
				return new SuccessResponse(200, card);
			} else {
				// gerar QR Code com id de usuario
				return new SuccessResponse(200, null);
			}
		} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, user.getIdUsuario());
			if(dependente == null) {
				return new ErrorResponse(-300, "Cliente não encontrado");
			}
			if(!onlyQRCode) {
				card.setIdCliente(dependente.getId());
				card.setNmCliente(dependente.getNmDependente());
				card.setNrCodCliente(dependente.getNrCodCliente());
				card.setNrCPF(dependente.getNrCpf());
				if(dependente.getDtNascimento() != null) {
					card.setDtNascimento((new SimpleDateFormat("dd/MM/yyyy")).format(dependente.getDtNascimento()));
				}
				card.setIsAssociado(!dependente.getCliente().getBoNaoAssociado());
				// gerar QR Code com id de usuario
				return new SuccessResponse(200, card);
			} else {
				// gerar QR Code com id de usuario
				return new SuccessResponse(200, null);
			}
		}
		return new ErrorResponse(-1, "Houve um erro ao processar a solicitação");
	}
	
	public AppResponse checkSituacaoCliente() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		ClienteView c = null;
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			c = clienteDAO.getClienteView(user.getIdUsuario());
		} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, user.getIdUsuario());
			c = clienteDAO.getClienteView(dependente.getCliente().getId());
		}
		Map<String, Object> data = null;
		if(c != null) {
			data = new HashMap<String, Object>();
			data.put("statusGeral", c.getStatusGeral());
			data.put("nmStatusGeral", c.getStatusGeralFormatado());
		}
		return new SuccessResponse(200, data);
	}
	
	public AppResponse getContratosUsuarioApp() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		Object result = null;
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			result = getContratos(user.getIdUsuario());
		} else if(user.getTipoUsuario().intValue() == TipoUsuario.DEPENDENTE.valorTipo) {
			try {
				Dependente dependente = dependenteDAO.searchByKey(Dependente.class, 
						user.getIdUsuario());
				if(dependente != null) {
					result = getDependentesContratos(dependente.getCliente().getId(), 
							dependente.getId());
				}
			} catch(Exception e) {
				
			}
		}
		if(result != null) {
			return new SuccessResponse(200, result);
		}
		return new FailResponse(-1, "Nenhum dado foi encontrado");
	}
	
	public AppResponse persistUserLogin(UsuarioLogin login) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		login.setIdUsuario(user.getIdUsuario());
		login.setTipoUsuario(user.getTipoUsuario());
		login.setLastLogin(new Date());
		try {
			usuarioLoginDAO.update(login);
			Map<String, Object> r = new HashMap<String, Object>();
			r.put("saved", true);
			return new SuccessResponse(200, r);
		} catch(Exception e) {
			
		}
		return new ErrorResponse(-1, "Houve um erro ao processar a requisição");
	}
	
	public AppResponse checkUsuarioLoginRegister(String deviceId) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		UsuarioLogin login = usuarioLoginDAO.getUsuarioLoginByPk(user.getIdUsuario(), 
				user.getTipoUsuario(), deviceId);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("loginRegistered", login != null);
		response.put("notificationRegistered", (login != null && login.getNotificationRegId() != null));
		return new SuccessResponse(200, response);
	}
	
	public AppResponse updateUserLastView(String deviceId) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		UsuarioLogin login = usuarioLoginDAO.getUsuarioLoginByPk(user.getIdUsuario(), 
				user.getTipoUsuario(), deviceId);
		if(login != null) {
			login.setLastLogin(new Date());
			usuarioLoginDAO.update(login);
			return new SuccessResponse(200, null);
		}
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("mensagem", "Usuário não encontrato");
		return new FailResponse(-20, response);
	}
	
	public AppResponse getPacientesForUser() {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		PacienteSearchDTO paciente = null;
		if(user.getTipoUsuario().equals(TipoUsuario.CLIENTE.valorTipo) ) {
			Cliente cliente = clienteDAO.searchByKey(Cliente.class, user.getIdUsuario());
			if(cliente != null) {
				paciente = this.searchUserByCPF(cliente.getNrCPF());
			}
		} else if(user.getTipoUsuario().equals(TipoUsuario.DEPENDENTE.valorTipo)) {
			Dependente dependente = dependenteDAO.searchByKey(Dependente.class, user.getIdUsuario());
			if(dependente != null) {
				PacienteCadastroDTO cad = new PacienteCadastroDTO();
				cad.setIdPaciente(dependente.getId());
				cad.setTipo(2);
				
				paciente = new PacienteSearchDTO();
				paciente.setNome(dependente.getNmDependente());
				paciente.setTipo(2);
				paciente.addCadastroPaciente(cad);
			}
		}
		if(paciente != null) {
			return new SuccessResponse(200, paciente);
		}
		return new FailResponse();
	}

	public Cliente getClienteByContrato(Long idContrato) {
		Cliente cliente = clienteDAO.getClienteByContrato(idContrato);
		return cliente;
	}
}
