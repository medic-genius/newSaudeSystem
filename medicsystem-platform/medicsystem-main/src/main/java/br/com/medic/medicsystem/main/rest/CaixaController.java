package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.CaixaService;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.CaixaRetiradaDTO;
import br.com.medic.medicsystem.persistence.dto.MovimentacaoCaixaDTO;
import br.com.medic.medicsystem.persistence.model.Caixa;
import br.com.medic.medicsystem.persistence.model.CaixaCompartilhadoView;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Path("/caixa")
@RequestScoped
public class CaixaController {
	
	@Inject
	private CaixaService caixaService;
	
	@Inject
	private SecurityService securityService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CaixaCompartilhadoView> getConfereciasCaixa(@QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro, @QueryParam("idUnidade") Long idUnidade,
			@QueryParam("idSistema") Long idSistema, @QueryParam("idConferente") Long idConferente,@QueryParam("idOperador") Long idOperador, 
			@QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim){
		
		if(idEmpresaFinanceiro == null){
			throw new IllegalArgumentException();
		}
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		List<CaixaCompartilhadoView> list = caixaService.getConferenciasCaixa(empresa, dtInicio, dtFim, idEmpresaFinanceiro, idUnidade, idConferente, idOperador);
		
		return list;
	}
	
	@GET
	@Path("/empresas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaGrupoCompartilhado> getEmpresasGrupoCompartilhado(){
		List<EmpresaGrupoCompartilhado> empresas = caixaService.getEmpresasGrupoCompartilhado();
		return empresas;
	}
	
	@GET
	@Path("/unidades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseDTO> getUnidadesByEmpresaFinanceiro(@QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro){
		
		if(idEmpresaFinanceiro == null){
			throw new IllegalArgumentException();
		}
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		List<BaseDTO> unidades = caixaService.getUnidadesByEmpresaGrupo(empresa.getIdEmpresaGrupoExterno(), empresa.getInSistemaExterno());
		
		return unidades;
	}
	
	@GET
	@Path("/movimentacoes")
	@Produces(MediaType.APPLICATION_JSON)
	public MovimentacaoCaixaDTO getMovimentacaoCaixa( @QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro, @QueryParam("idCaixa") Long idCaixa) {
		
		if( idEmpresaFinanceiro == null ) {
			throw new IllegalArgumentException(); 
		}
		
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		MovimentacaoCaixaDTO movCaixaDTO = caixaService.getMovimentacaoCaixa(empresa.getInSistemaExterno(), idCaixa);
		
		return movCaixaDTO;
		
	}
	
	@GET
	@Path("/operadores")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseDTO> getOperadoresCaixa( @QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro) {
		
		if( idEmpresaFinanceiro == null ) {
			throw new IllegalArgumentException(); 
		}
		
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		List<BaseDTO> operadores = caixaService.getOperadoresCaixa(empresa.getInSistemaExterno());
		
		return operadores;
		
	}
	
	@GET
	@Path("/conferir")
	@Produces(MediaType.APPLICATION_JSON)
	public Response conferirCaixa( @QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro, @QueryParam("idCaixa") Long idCaixa) {
		
		if( idEmpresaFinanceiro == null ) {
			throw new IllegalArgumentException(); 
		}
		
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		Caixa caixa = caixaService.saveConferenciaCaixa(idCaixa, empresa.getInSistemaExterno());
		
		return Response.created(URI.create("/caixa/conferir/" + caixa.getIdcaixa())).build();
		
	}
	
	@GET
	@Path("/desconferir")
	@Produces(MediaType.APPLICATION_JSON)
	public Response desconferirCaixa( @QueryParam("idEmpresaFinanceiro") Long idEmpresaFinanceiro, @QueryParam("idCaixa") Long idCaixa) {
		
		if( idEmpresaFinanceiro == null ) {
			throw new IllegalArgumentException(); 
		}
		
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresaFinanceiro);
		Funcionario funcionarioLogado = securityService.getFuncionarioLogado();
		
		List<MovimentacaoConta> movimentacaoEntradaCaixa = caixaService.getMovimentacaoEntradaPorCaixa( empresa.getEmpresaFinanceiro().getIdEmpresaFinanceiro(), idCaixa );
		Caixa caixa = caixaService.desconferirCaixa(idCaixa, empresa);
		
		if( caixa != null  )
			caixaService.excluirMovimentacaoDeCaixa(movimentacaoEntradaCaixa, funcionarioLogado);
		
		return Response.created(URI.create("/caixa/desconferir/" + caixa.getIdcaixa())).build();
		
	}
	
	//detalhes retirada individual
	@GET
	@Path("/detalhesretirada")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CaixaRetiradaDTO> getRetiradaDetalhes(@QueryParam("idCaixa") Long idCaixa, @QueryParam("idEmpresa") Long idEmpresa){
		
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresa);
		List<CaixaRetiradaDTO> detalhesRetirada = caixaService.getRetiradaDetalhes(idCaixa, empresa.getInSistemaExterno());
		return detalhesRetirada;
	}
	
	//detalhes retirada geral
	
	@GET
	@Path("/detalhesretiradageral")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CaixaRetiradaDTO> getRetiradaGeralDetalhes(@QueryParam("idContaBancaria") Long idContaBancaria,
															@QueryParam("idEmpresa") Long idEmpresa,
															@QueryParam("dataInicio") String dtInicio,
															@QueryParam("dataFim") String dtFim){
		
		if( idEmpresa == null ) {
			throw new IllegalArgumentException(); 
		}
		String caixas = caixaService.getListaCaixas(idContaBancaria, idEmpresa, dtInicio, dtFim);
		EmpresaGrupoCompartilhado empresa = caixaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(idEmpresa);
		List<CaixaRetiradaDTO> detalhesRetirada = caixaService.getRetiradaGeralDetalhes(idContaBancaria, idEmpresa, dtInicio, dtFim, empresa.getInSistemaExterno(), caixas);
		return detalhesRetirada;
	}

}
