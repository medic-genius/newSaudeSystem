package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.text.DecimalFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.ContaContabilService;
import br.com.medic.medicsystem.persistence.model.ContaContabil;

@Path("/contacontabil")
public class ContaContabilController extends BaseController{

	@Inject
	private ContaContabilService contaContabilService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveContaContabil(@Valid ContaContabil contaContabil){
		ContaContabil salvo = contaContabilService.saveContaContabil(contaContabil);
		return Response.created(URI.create("/contacontabil/" + salvo.getId())).build();
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateContaContabil(@Valid ContaContabil contaContabil, @PathParam("id") Long id){
		ContaContabil salvo = contaContabilService.saveContaContabil(contaContabil);
		return Response.created(URI.create("/contacontabil/" + salvo.getId())).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	public ContaContabil getContaContabilById(@PathParam("id") Long id){
		ContaContabil contaContabil = contaContabilService.getContaContabilById(id);
		return contaContabil;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContaContabil> getAllContasContabil(){
		List<ContaContabil> list = contaContabilService.getAllContasContabil();
		return list;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/busca")
	public List<ContaContabil> getBuscaContasContabil(){
		 return contaContabilService.getBuscaContasContabil(
				 getQueryParam("nmContaContabil", String.class),
				 getQueryParam("start", Integer.class),
				 getQueryParam("size", Integer.class)
				 );
	}
	
	@GET	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List<ContaContabil> getAllContasContabil(
			@QueryParam ("dtInicio") String dtInicio,
			@QueryParam ("dtFim") String dtFim,
			@QueryParam("idEmpresa") List<String> idEmpresa,
			@QueryParam("idContaContabil") Long idContaContabil,
			@QueryParam("tipoStatus") Integer tipoStatus){
		
		
		DateTimeFormatter dtForm = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter mesForm = DateTimeFormatter.ofPattern("MM/yyyy");
		YearMonth mesFinal = YearMonth.parse(dtFim, dtForm);
		List<ContaContabil> listResult = new ArrayList<ContaContabil>();
		for(YearMonth mes = YearMonth.parse(dtInicio, dtForm);
				! mes.isAfter(mesFinal);
				mes = mes.plusMonths(1)) {
			String[] splitMesAno =  mes.format(mesForm).split("/");
			int mesSplit = Integer.valueOf(splitMesAno[0]);
			int anoSplit = Integer.valueOf(splitMesAno[1]);
			listResult.addAll(contaContabilService.getAllContasContabil(mesSplit, anoSplit, idEmpresa, idContaContabil, tipoStatus));
		}
		
		return listResult;
	}
	
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteContaContabil(@PathParam("id") Long id){
		contaContabilService.deleteContaContabil(id);
		return Response.ok().build();
	}
	
	@GET
	@Path("/allcontacontabil") 
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContaContabil> getAllContaContabil(){
		List<ContaContabil> list = contaContabilService.getAllContaContabil();
		return list;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/analisehorizontal")
	public List<ContaContabil> getDespesasAnaliseH(@QueryParam("idEmpresa") List<String> idEmpresa, 
			@QueryParam("idContaContabil") Long idContaContabil){
	
			List<Double> listAnos = contaContabilService.getAnosDespesas();
			List<ContaContabil> analise = new ArrayList<ContaContabil>();
			DecimalFormat af = new DecimalFormat("#");
			for (Double ano : listAnos) {
				String anoForm = af.format(ano);	
				analise.addAll(contaContabilService.getDespesasAnaliseH(idEmpresa, idContaContabil, anoForm));
			}
		
			 
		return analise;
	}

}
