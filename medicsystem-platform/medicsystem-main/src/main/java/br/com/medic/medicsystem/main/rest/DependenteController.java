package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.service.DependenteService;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.Foto;

@Path("/dependentes")
@RequestScoped
public class DependenteController extends BaseController {

	@Inject
	private DependenteService dependenteService;

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Dependente getDependente(@PathParam("id") Long id) {
		final Dependente dep = dependenteService.getDependente(id);

		if (dep == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return dep;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/foto/dependente")
	@Produces(MediaType.APPLICATION_JSON)
	public Foto getFotoDependente(@PathParam("id") Long id) {
		final Dependente dep = dependenteService.getDependente(id);

		Foto fotodep = new Foto();
		if(dep != null && dep.getFotoDependente()!=null)
		{
			 fotodep.setFoto(dep.getFotoDependente());
			 return fotodep;
		}
		return fotodep;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/dependentescliente")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dependente> getDependentes(@PathParam("id") Long idCliente){
		
		if (idCliente != null) {
			
			return dependenteService.getDependentesByCliente(idCliente);
		
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}	
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createDependente(@Valid Dependente dependente, 
			@QueryParam("id") Long idCliente) {
		
		Dependente salvo = dependenteService.saveDependente(dependente, idCliente);
		
		return Response.created(URI.create("/dependentes/" + salvo.getId())).build();
	}
		
	
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDependente(@PathParam("id") Long idDependente, Dependente dependente) {

		if (dependente != null && dependente.getId() != null) {
			
			Dependente dependenteAtualizado = dependenteService.updateDependente(idDependente, dependente);

			if (dependenteAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

}