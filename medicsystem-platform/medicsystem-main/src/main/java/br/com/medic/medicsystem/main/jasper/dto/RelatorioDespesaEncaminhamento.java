package br.com.medic.medicsystem.main.jasper.dto;

import br.com.medic.medicsystem.persistence.model.Encaminhamento;

public class RelatorioDespesaEncaminhamento {

	private String nrEncaminhamento;

	private String nmConveniada;

	public RelatorioDespesaEncaminhamento(Encaminhamento encaminhamento) {
		this.nrEncaminhamento = encaminhamento.getNrEncaminhamento();
		this.nmConveniada = (encaminhamento.getProfissional() != null ? encaminhamento
		        .getProfissional().getNmFuncionario() : encaminhamento
		        .getCredenciada().getNmRazaoSocial());
	}

	public String getNmConveniada() {

		return nmConveniada;
	}

	public void setNmConveniada(String nmConveniada) {

		this.nmConveniada = nmConveniada;
	}

	public String getNrEncaminhamento() {

		return nrEncaminhamento;
	}

	public void setNrEncaminhamento(String nrEncaminhamento) {

		this.nrEncaminhamento = nrEncaminhamento;
	}

}
