package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.FaturamentoService;
import br.com.medic.medicsystem.persistence.dto.ConferenciaEmpresaClienteDTO;
import br.com.medic.medicsystem.persistence.dto.DetalhesFaturaDTO;
import br.com.medic.medicsystem.persistence.dto.FaturamentoConveniadaDTO;
import br.com.medic.medicsystem.persistence.dto.FaturamentoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.UnidadeDTO;
import br.com.medic.medicsystem.persistence.model.views.MensalidadeParcelaView;

@Path("/faturamento")
@RequestScoped
public class FaturamentoController {
	
	@Inject
	private FaturamentoService faturamentoService;
	
	@GET
	@Path("/profissional")
	@Produces(MediaType.APPLICATION_JSON)
	public FaturamentoProfissionalDTO getRelarioAtendimentoMedicoPagamentoPorTipo(
			@QueryParam("idFuncionario") Long idFuncionario,
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim) {
		
		if (dtInicio == null || dtFim == null || idFuncionario == null) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return faturamentoService.getFaturamentoProfissional(idFuncionario, dtInicio, dtFim);

	}
	
	@GET
	@Path("/unidadetrabalhada")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UnidadeDTO> getUnidadeTrabalhadaByTipoAtendimento(
			@QueryParam("idFuncionario") Long idFuncionario,
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim) {
		
		if (dtInicio == null || dtFim == null || idFuncionario == null ) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return faturamentoService.getUnidadeTrabalhadaByTipoAtendimento(idFuncionario, dtInicio, dtFim);

	}
	
	@GET
	@Path("/conveniada")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FaturamentoConveniadaDTO> getFaturamentoConveniada(			
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFim") String dtFim) {
		
		if (dtInicio == null || dtFim == null) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return faturamentoService.getFaturamentoConveniada(dtInicio, dtFim);

	}	
	
	@GET
	@Path("/conveniada/mp")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MensalidadeParcelaView> getMensalidadeParcelaConveniada(			
			@QueryParam("idEmpresaCliente") Long idEmpresaCliente) {
		
		if (idEmpresaCliente == null) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return faturamentoService.getMensalidadeParcelaConveniada(idEmpresaCliente);

	}
	
	@GET
	@Path("/conveniada/periodofatura")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ConferenciaEmpresaClienteDTO> getPeriodoFaturamento(
			@QueryParam("idEmpresaCliente") Long idEmpresaCliente){
		
		if (idEmpresaCliente == null) { 
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return faturamentoService.getPeriodoFaturamento(idEmpresaCliente);
	}

	
	@GET
	@Path("/conveniada/detalhes_fatura/{billPeriod: \\d+}/{idConveniada:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public DetalhesFaturaDTO getFatura(
			@PathParam("billPeriod") String billPeriod,
			@PathParam("idConveniada") Long idConveniada) {
		
		return faturamentoService.getFatura(idConveniada, billPeriod);

	}
}
