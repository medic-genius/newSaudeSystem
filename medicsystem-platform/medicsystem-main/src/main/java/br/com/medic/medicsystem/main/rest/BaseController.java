package br.com.medic.medicsystem.main.rest;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

public abstract class BaseController {

	@Context
	protected UriInfo info;
	
	@SuppressWarnings("unchecked")
	protected <T> T getQueryParam(String paramName, Class<T> clazz) {

		if (info.getQueryParameters().containsKey(paramName)) {
			if (clazz.equals(String.class)) {
				return (T) info.getQueryParameters().getFirst(paramName);
			} else if (clazz.equals(Integer.class)) {
				return (T) Integer.valueOf(info.getQueryParameters().getFirst(
				        paramName));
			} else if (clazz.equals(Long.class)) {
				return (T) Long.valueOf(info.getQueryParameters().getFirst(
				        paramName));
			} else if (clazz.equals(Double.class)) { // TODO double check this
				return (T) Double.valueOf(info.getQueryParameters().getFirst(
				        paramName));
			} // TODO add more
		}
		return null;
	}

}
