package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.AgenciaDAO;
import br.com.medic.medicsystem.persistence.dao.BancoDAO;
import br.com.medic.medicsystem.persistence.dao.CidadeDAO;
import br.com.medic.medicsystem.persistence.model.Agencia;
import br.com.medic.medicsystem.persistence.model.Banco;
import br.com.medic.medicsystem.persistence.model.Cidade;

@Stateless
public class AgenciaService {

	@Inject
	private Logger logger;
	
	@Inject
	@Named("agencia-dao")
	private AgenciaDAO agenciaDAO;
	
	@Inject
	@Named("banco-dao")
	private BancoDAO bancoDAO;
	
	@Inject
	@Named("cidade-dao")
	private CidadeDAO cidadeDAO;

	public Agencia saveAgencia(Agencia agencia) {
		
		Banco banco = bancoDAO.getBancoById(agencia.getBanco().getId());
		agencia.setBanco(banco);
		agenciaDAO.persist(agencia);

		return agencia;

	}
	
	public Agencia updateAgencia(Agencia agencia){
		Banco banco = bancoDAO.getBancoById(agencia.getBanco().getId());
		agencia.setBanco(banco);
		
		Cidade cidade = cidadeDAO.getCidade(agencia.getCidade().getId());
		agencia.setCidade(cidade);
		
		agencia = agenciaDAO.update(agencia);
		
		return agencia;
	}

	public List<Agencia> getAllAgencias() {
		logger.debug("Obtendo lista de bancos...");

		return agenciaDAO.getAllAgencias();
	}

	public List<Agencia> getAgenciaByBanco(Long idBanco) {
		
		return agenciaDAO.getAgenciaByBanco(idBanco);
	}

}
