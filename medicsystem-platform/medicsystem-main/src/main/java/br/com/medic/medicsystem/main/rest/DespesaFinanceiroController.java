package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.medic.medicsystem.main.service.DespesaFinanceiroService;
import br.com.medic.medicsystem.persistence.dto.ArquivoDespesaFinanceiroDTO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroAgrupadaReportDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaGraficoDTO;
import br.com.medic.medicsystem.persistence.model.DespesaFinanceiro;
import br.com.medic.medicsystem.persistence.model.enums.FormaPagamentoFinanceiroEnum;
import br.com.medic.medicsystem.persistence.model.enums.FrenquenciaDespesa;
import br.com.medic.medicsystem.persistence.model.views.DespesaFinanceiroView;

@Path("/despesafinanceiro")
public class DespesaFinanceiroController extends BancoController{

	@Inject
	DespesaFinanceiroService despesaFinanceiroService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveDespesaFinanceiro(@Valid DespesaFinanceiro despesaFinanceiro, @QueryParam("tipoFrequencia") Integer tipoFrequencia, @QueryParam("ocorrencia") Integer ocorrencia){
		
		if(despesaFinanceiro == null){
			throw new IllegalArgumentException();
		}
		
		DespesaFinanceiro salvo = despesaFinanceiroService.saveDespesaFinanceiro(despesaFinanceiro, tipoFrequencia, ocorrencia);
		return Response.created(URI.create("/despesafinanceiro/" + salvo.getId())).build();
	}
	
	
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDespesaFinanceiro(@Valid DespesaFinanceiro despesaFinanceiro, @PathParam("id") Long id, @QueryParam("tipoFrequencia") Integer tipoFrequencia, @QueryParam("ocorrencia") Integer ocorrencia){
		
		if(despesaFinanceiro == null){
			throw new IllegalArgumentException();
		}
		
		DespesaFinanceiro salvo = despesaFinanceiroService.saveDespesaFinanceiro(despesaFinanceiro, tipoFrequencia, ocorrencia);
		return Response.created(URI.create("/despesafinanceiro/" + salvo.getId())).build();
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaFinanceiro getDespesaFinanceiroById(@PathParam("id") Long id){
		DespesaFinanceiro despesa = despesaFinanceiroService.getDespesaFinanceiroById(id);
		return despesa;
	}
	
	@GET
	@Path("/documento/{documento}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DespesaFinanceiro> getDespesaFinanceiroByNrDocumento(@PathParam("documento") String documento){
		List<DespesaFinanceiro> despesas = despesaFinanceiroService.getDespesaFinanceiroByNumeroDocumento(documento);
		return despesas;
	}
	
	@GET
	@Path("/servico/{servico}")
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaFinanceiroAgrupadaReportDTO getDespesaFinanceiroByNrServico(@PathParam("servico") String servico,
			@QueryParam("idempresa") Long idEmpresaFinanceiroPagamente,
			@QueryParam("dataInicio") String dtInicio,
			@QueryParam("dataFim") String dtFim,
			@QueryParam("idContaBancaria") Long idContaBancaria){
		
		DespesaFinanceiroAgrupadaReportDTO dto = despesaFinanceiroService.getDespesasDTOByServico(servico, idEmpresaFinanceiroPagamente, dtInicio, dtFim, idContaBancaria);
		return dto;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<DespesaFinanceiro> getDespesasFinanceiro(@QueryParam("nrIdEmpresa") Long idEmpresa, @QueryParam("nrOpcaoConsulta") Integer opcaoConsulta, 
			@QueryParam("inSituacao") Integer inStatus, @QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim){

		List<DespesaFinanceiro> despesas = despesaFinanceiroService.getDespesas(idEmpresa, opcaoConsulta, inStatus, dtInicio, dtFim, null);
			
		return despesas;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/frequencia")
	public List<BaseDTO> getFrequenciaOptions(){
		List<BaseDTO> list = new ArrayList<BaseDTO>();
		
		for (FrenquenciaDespesa f : FrenquenciaDespesa.values()) {
			list.add(new BaseDTO(((long) f.getId()), f.getDescription()));
		}
		
		return list;
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteDespesaFinanceiro(@PathParam("id") Long id){
		despesaFinanceiroService.deleteDespesaFinanceiro(id);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/arquivo/{id:[0-9][0-9]*}")
	public Response deleteArquivoDespesaFinanceiro(@PathParam("id") Long id){
		despesaFinanceiroService.deleteArquivoDespesaFinanceiro(id);
		return Response.ok().build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/quantidade")
	public BaseDTO getQuantidadeDespesa(@QueryParam("nrIdEmpresa") Long idEmpresa, @QueryParam("nrOpcaoConsulta") Integer opcaoConsulta){
		BaseDTO dto = despesaFinanceiroService.getQuantidadeDespesas(idEmpresa, opcaoConsulta);
		return dto;
	}
	
	
	@GET
	@Path("/empresafinanceiro/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DespesaFinanceiro> getDespesasFinanceiroByEmpresaFinanceiro(@PathParam("id") Long id){
		List<DespesaFinanceiro> despesas = despesaFinanceiroService.getDespesasFinanceiroByEmpresaFinanceiro(id);
		return despesas;
	}
	
	@GET
	@Path("/graficobyempresa/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DespesaGraficoDTO> getGraficoByEmpresaFinanceiro(@PathParam("id") Long id){
		List<DespesaGraficoDTO> grafico = despesaFinanceiroService.getGraficoAnualDTO(id);
		return grafico;
	}
	
	@POST
	@Path("/{iddespesa:[0-9][0-9]*}/upload")
	@Consumes("multipart/form-data")
	public Response importaAnexosDespesa( MultipartFormDataInput input , @PathParam("iddespesa") Long idDespesa ) {
		despesaFinanceiroService.importaArquivosDespesa( input, idDespesa );
		return Response.ok().build();
	}
	
	@GET
	@Path("/{iddespesa:[0-9][0-9]*}/arquivos")
	@Produces(MediaType.APPLICATION_JSON)
	public ArquivoDespesaFinanceiroDTO getListaArquivosFuncionario( @PathParam("iddespesa") Long idDespesa ) {
		ArquivoDespesaFinanceiroDTO arquivosDespesa = despesaFinanceiroService.getArquivosFuncionario(idDespesa);
		return arquivosDespesa;
	}
	
	/**/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/formapagamento")
	public List<BaseDTO> getFormaPagamento(){
		List<BaseDTO> list = new ArrayList<BaseDTO>();
		
		for (FormaPagamentoFinanceiroEnum f : FormaPagamentoFinanceiroEnum.values()) {
			list.add(new BaseDTO(((long) f.getId()), f.getDescription()));
		}
		
		return list;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/salvardespesafin")
	public List<DespesaFinanceiro> saveDespesaFinanceiro2(DespesaFinanceiro despesaFinanceiro, 
			@QueryParam("tipoFrequencia") Integer tipoFrequencia,
			@QueryParam("ocorrencia") Integer ocorrencia){
		
		if(despesaFinanceiro == null){
			throw new IllegalArgumentException();
		}
		
		List<DespesaFinanceiro> salvo = despesaFinanceiroService.saveDespesaFinanceiro2(despesaFinanceiro, tipoFrequencia, ocorrencia);
		/*StringBuilder str = new StringBuilder();
		for (DespesaFinanceiro elemento : salvo) {
			str.append(elemento.getId()).append("/");
		}
		
		return Response.created(URI.create("/despesafinanceiro/" + str.toString().substring(0, str.length()-1))).build();*/
		return salvo;
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/salvardespesafin")
	public List<DespesaFinanceiro> getDespesasFinanceiro2(@QueryParam("nrIdEmpresa") Long idEmpresa, @QueryParam("nrOpcaoConsulta") Integer opcaoConsulta, 
			@QueryParam("inSituacao") Integer inStatus, @QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim, @QueryParam("inModalidade") Integer inModalidade){

		List<DespesaFinanceiro> despesas = despesaFinanceiroService.getDespesas(idEmpresa, opcaoConsulta, inStatus, dtInicio, dtFim, inModalidade);
			
		return despesas;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/despesascontacontabil")
	public List<DespesaFinanceiroDTO> getDespesasByCC(@QueryParam("idEmpresa") List<String> idEmpresa,
			@QueryParam("dtAno") Long dtAno,
			@QueryParam("dtMes") Long dtMes,
			@QueryParam("inStatus") Integer inStatus,
			@QueryParam("idContaContabil") Integer idContaContabil){
		
			List<DespesaFinanceiroDTO> despesas = despesaFinanceiroService.getDespesasByCC(idEmpresa, dtAno, dtMes, inStatus, idContaContabil);
		return despesas;
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/despesascentrocusto")
	public List<DespesaFinanceiroDTO> getDespesasByCentroCusto(@QueryParam("idEmpresa") List<String> idEmpresa,
			@QueryParam("dtAno") Long dtAno,
			@QueryParam("dtMes") Long dtMes,
			@QueryParam("inStatus") Integer inStatus,
			@QueryParam("idCentroCusto") Integer idCentroCusto){
		
			List<DespesaFinanceiroDTO> despesas = despesaFinanceiroService.getDespesasByCentroCusto(idEmpresa, dtAno, dtMes, inStatus, idCentroCusto);
		return despesas;
		
	}
	

	
	//anexos despesas
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/anexos")
	public Response salvarArquivoDespesa(String bodyStr, @QueryParam("iddespesa") List<Long> idDespesa ) {
		despesaFinanceiroService.salvarArquivoDespesa(bodyStr, idDespesa );
	
		return Response.ok().build();
	}
	
	//busca das despesas financeiro
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/busca")
	public List<DespesaFinanceiroView> getBuscaDespesaFinanceiro(){
		return despesaFinanceiroService.getBuscaDespesaFinanceiro(
				getQueryParam("nmDespesa", String.class),
				getQueryParam("idEmpresa", Long.class),
				getQueryParam("dtInicio", String.class),
				getQueryParam("dtFim", String.class),
				getQueryParam("nrDocumento", String.class),
				getQueryParam("idContaContabil", Long.class),
				getQueryParam("start", Integer.class),
				getQueryParam("size", Integer.class),
				getQueryParam("idContaBancaria", Long.class)
				);
	}
	/*apagar*/
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update")
	public DespesaFinanceiro updateDespesa(@QueryParam("idDespesa") Long idDespesa, 
			@QueryParam("valor") Double valor,
			@QueryParam("vlJurosMulta") Double vlJurosMulta,
			@QueryParam("vlDesconto") Double vlDesconto){
			if(idDespesa != null){
				DespesaFinanceiro  despesaAtualizado = despesaFinanceiroService.updateDespesaFinanceiro(idDespesa, valor, vlJurosMulta, vlDesconto);
				return despesaAtualizado;
			}else {

				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
		
	}

	
	
	
}
