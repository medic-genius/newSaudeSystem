package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.ClienteViewService;
import br.com.medic.medicsystem.persistence.model.views.ClienteView;

@Path("/clienteview")
@RequestScoped
public class ClienteViewController extends BaseController {

	@Inject
	private ClienteViewService clienteViewService;
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public ClienteView getClienteView(
	        @PathParam("id") Long idCliente) {

		final ClienteView clienteView = clienteViewService.getClienteView(idCliente);

		if (clienteView == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return clienteView;
	}
}