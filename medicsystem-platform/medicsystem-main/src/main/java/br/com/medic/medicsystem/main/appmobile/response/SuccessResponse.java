package br.com.medic.medicsystem.main.appmobile.response;

public class SuccessResponse extends AppResponse{
	private Object data;
	
	public SuccessResponse() {
		super("success");
	}
	
	public SuccessResponse(Object data) {
		this(null, data);
		this.data = data;
	}
	
	public SuccessResponse(Integer code, Object data) {
		this();
		this.setCode(code);
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
