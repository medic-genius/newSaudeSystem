package br.com.medic.medicsystem.main.exception;

import br.com.medic.medicsystem.main.mapper.ErrorType;

public abstract class GeneralException extends RuntimeException {

	private static final long serialVersionUID = -4719656546952109829L;

	protected ErrorType errorType;

	public GeneralException(String message, ErrorType errorType) {
		super(message);
		this.errorType = errorType;
	}

	public GeneralException(String message, ErrorType errorType, Throwable t) {
		super(message, t);
		this.errorType = errorType;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

}
