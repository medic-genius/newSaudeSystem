package br.com.medic.medicsystem.main.service;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.persistence.dao.EmpresaClienteDAO;
import br.com.medic.medicsystem.persistence.dao.GuiaServicoDAO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryGuiaServicoByServicoDTO;
import br.com.medic.medicsystem.persistence.dto.SummaryServicoByStatusDTO;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.security.SecurityService;

@Stateless
public class GuiaServicoService {

	@Inject
	@Named("guiaServico-dao")
	private GuiaServicoDAO guiaServicoDAO;
	
	@Inject
	private SecurityService securityService;

	@Inject
	@Named("empresaCliente-dao")
	private EmpresaClienteDAO empresaClienteDAO;

	public GuiaServico deleteGuiaServico(Long id) {
		GuiaServico guiaServico = guiaServicoDAO.searchByKey(GuiaServico.class, id);
		
		if(guiaServico != null)
		{
			if(this.getEmpresaCliente().getId() != guiaServico.getGuia().getContrato().getEmpresaCliente().getId())
			{
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			} else
			{
				guiaServico.setStatus(4);
				guiaServico.setDtExclusao(new Timestamp(System.currentTimeMillis()));
			}
		}
		return guiaServico;
	}
	
	private EmpresaCliente getEmpresaCliente(){
		String user = securityService.getUserLogin();
		EmpresaCliente empresaCliente = empresaClienteDAO.getEmpresaCliente(user);
		return empresaCliente;
	}

	public GenericPaginateDTO getItems(int offset, int limit, Long id, Long idGuia, String startDate, String endDate, List<Integer> lStatus, Long idServico, Long idConveniada) {
		if(limit > 25)
		{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		GenericPaginateDTO paginator = new GenericPaginateDTO();
		paginator.setOffset(offset);
		paginator.setLimit(limit);
		EmpresaCliente empresaCliente = this.getEmpresaCliente();
		if(empresaCliente != null)
		{
			idConveniada = empresaCliente.getId();
		}
		return this.getItemsByConveniada(paginator, idConveniada, id, idGuia, startDate, endDate, lStatus, idServico);
	}

	private GenericPaginateDTO getItemsByConveniada(GenericPaginateDTO paginator, Long idEmpresaCliente, Long id, Long idGuia, String startDate, String endDate, List<Integer> lStatus, Long idServico) {
		paginator = guiaServicoDAO.getGuiaServicosDTO(paginator, idEmpresaCliente, id, idGuia, startDate, endDate, lStatus, idServico);
		paginator.prepare();
		return paginator;
	}

	public GuiaServico getGuiaServico(Long id) {
		GuiaServico guiaServico = guiaServicoDAO.searchByKey(GuiaServico.class, id);
		
		if(guiaServico != null)
		{
			if(this.getEmpresaCliente().getId() != guiaServico.getGuia().getContrato().getEmpresaCliente().getId())
			{
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			} else
			{
				return guiaServico;
			}
		}
		return null;
	}

	public List<SummaryServicoByStatusDTO> getSummaryServicoByStatus(Long id, Long idGuia, String startDate, String endDate, List<Integer> lStatus, Long idConveniada) {
		if(null != this.getEmpresaCliente())
		{
			idConveniada = this.getEmpresaCliente().getId(); 
		}
		return guiaServicoDAO.getSummaryServicoByStatus(idConveniada, id, idGuia, startDate, endDate, lStatus);
		
	}
	
	public List<SummaryGuiaServicoByServicoDTO> getSummaryGuiaServicoByServico(Long id, Long idGuia, String startDate, String endDate, List<Integer> lStatus, Long idConveniada) {
		if(null != this.getEmpresaCliente())
		{
			idConveniada = this.getEmpresaCliente().getId();
		}
		return guiaServicoDAO.getSummaryGuiaServicoByServico(idConveniada, id, idGuia, startDate, endDate, lStatus);
	}
}
