package br.com.medic.medicsystem.main.service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.appmobile.response.AppResponse;
import br.com.medic.medicsystem.main.appmobile.response.ErrorResponse;
import br.com.medic.medicsystem.main.appmobile.response.SuccessResponse;
import br.com.medic.medicsystem.persistence.appmobile.enums.TipoUsuario;
import br.com.medic.medicsystem.persistence.appmobile.model.UsuarioRegister;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.ContratoDependenteDAO;
import br.com.medic.medicsystem.persistence.dao.ServicoDAO;
import br.com.medic.medicsystem.persistence.dto.DataCoberturaDTO;
import br.com.medic.medicsystem.persistence.dto.PacienteDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoCredenciadaDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoDTO;
import br.com.medic.medicsystem.persistence.dto.ServicoProfissionalDTO;
import br.com.medic.medicsystem.persistence.dto.TipoConsultaDTO;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.DespesaServico;
import br.com.medic.medicsystem.persistence.model.Encaminhamento;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.ServicoCredenciada;
import br.com.medic.medicsystem.persistence.model.ServicoProfissional;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.security.keycloak.KeycloakUtil;
import br.com.medic.medicsystem.persistence.utils.DateUtil;

@Stateless
public class ServicoService {

	@Inject
	private Logger logger;

	@Inject
	@Named("servico-dao")
	private ServicoDAO servicoDAO;

	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;
	
	@Inject
	@Named("contratodependente-dao")
	private ContratoDependenteDAO contratoDependenteDAO;
	
	@Inject
	private MobileService mobileService;
	
	@Inject
	private KeycloakUtil keycloakUtil;

	@Inject
	private ClienteService clienteService;

	@Inject
	private DependenteService dependenteService;

	@Inject
	private ContratoService contratoService;

	@Inject
	private AgendamentoService agendamentoService;
	
	@Inject
	private SecurityService securityService;

	public List<ServicoProfissional> getServicosPorProfissional(
	        Long idProfissional) {
		logger.debug("Obtendo servicos por profissional...");

		return servicoDAO.getServicosPorProfissional(idProfissional);
	}

	public List<Servico> getServicos(String nmServico, Integer startPosition,
	        Integer maxResults) {
		logger.debug("Obtendo lista de servicos...");

		return servicoDAO.getServicos(nmServico, startPosition, maxResults);
	}
	
	public List<Servico> getServicosByTipo(Integer inTipo) {
		logger.debug("Obtendo lista de servicos...");

		return servicoDAO.getServicosByTipo(inTipo);
	}

	public List<ServicoCredenciadaDTO> getCredenciadas(String idServico,
	        Long idCliente, Long idDependente, Long idContrato,
	        Integer tipoCredenciada) {

		List<ServicoCredenciadaDTO> result = new ArrayList<ServicoCredenciadaDTO>();

		List<ServicoCredenciada> credenciadas = servicoDAO
		        .getServicoCredenciadas(idServico, tipoCredenciada);

		String[] servicos = idServico.split(",");

		for (String string : servicos) {

			Servico servico = servicoDAO.getServico(Long.parseLong(string));

			Agendamento agendamento = new Agendamento();
			PacienteDTO<?> paciente = null;

			Contrato contrato = contratoService.getContrato(idContrato);
			Cliente cliente = clienteService.getCliente(idCliente);

			if (idDependente != null) {
				agendamento.setDependente(clienteService.getDependente(idDependente));
				paciente = new PacienteDTO<Dependente>(agendamento.getDependente());
				
				ContratoDependente contDependente = contratoDependenteDAO.getContratoDependenteById(idDependente, idContrato);
				
				if (contDependente == null){
					// Adicionar dependente no contrato do cliente nao fazendo uso
					Contrato contratocli = contratoDAO.getContratoPorCliente(idCliente);
					ContratoDependente condep = new ContratoDependente();
					
					if (contratocli != null) {		
						
						condep.setCreditoConveniada(0);
						condep.setBlAutorizaDebAut(false);
						condep.setContrato(contratocli);
						condep.setInSituacao(0);
						condep.setDependente(agendamento.getDependente());
						condep.setBoFazUsoPlano(false);
						//contratocli.addContratoDependente(condep);
		
						contratoDependenteDAO.persist(condep);
					}
				
//					ContratoDependente contratoDependente = contratoDAO.getContratoDependente(contratocli.getId(),
//							agendamento.getDependente().getId());
					paciente.setContratoDependente(condep);
				}
				else{					
					paciente.setContratoDependente(contDependente);
				}
			} else {
				paciente = new PacienteDTO<Cliente>(cliente);
			}
			
			
			
			ContratoCliente cc = contratoDAO.getContratoCliente(idContrato, idCliente);
			paciente.setContratoCliente(cc);
			paciente.setCliente(cliente);

			agendamento.setPaciente(paciente);
			agendamento.setCliente(cliente);
			if (paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null){
				agendamento.setContrato(paciente.getContratoDependente().getContrato());
			}else{
				agendamento.setContrato(paciente.getContratoCliente().getContrato());
			}
			
			agendamento.setServico(servico);

			Map<String, Boolean> boos = agendamentoService.verificarCobertura(
			        servico, agendamento);

			Map<String, Double> mapDoubleValues = agendamentoService
			        .getValoresPorProfissional(servico.getId(), null, paciente,
			                boos.get("isLiberado"),
			                boos.get("isServicoProfissional"), agendamento.getBoParticular());

			boolean boServicoAberto = (servico.getBoValorAberto() != null && servico
			        .getBoValorAberto());

			Locale locale = new Locale("pt", "BR");
			NumberFormat currencyFormatter = NumberFormat
			        .getCurrencyInstance(locale);

			// CREDENCIADA INTERNA
			if (credenciadas.size() > 0 && tipoCredenciada == 1) {

				for (ServicoCredenciada scred : credenciadas) {
					ServicoCredenciadaDTO dto = new ServicoCredenciadaDTO();
					dto.setCredenciada(scred.getCredenciada());
					String valor = null;
										
					dto.setCusto(scred.getVlCusto());
					
					if (boos.get("isConvenio")) {
						valor = (!boServicoAberto ? " | "
						        + currencyFormatter.format(mapDoubleValues.get("vlServico")) : "");
						dto.setValor(mapDoubleValues.get("vlServico"));
						dto.setCobertura(boos.get("isGuia"));
						
					} else if (boos.get("isLiberado")) {
						valor = (!boServicoAberto ? " | "
						        + currencyFormatter.format(scred.getVlCredenciadaAssociado()) : "");
						dto.setValor(scred.getVlCredenciadaAssociado());
						dto.setCobertura(boos.get("isCobertura"));
					} else {
						valor = (!boServicoAberto ? " | "
						        + currencyFormatter.format(scred.getVlCredenciada()) : "");
						dto.setValor(scred.getVlCredenciada());
						dto.setCobertura(boos.get("isCobertura"));
					}
										
					dto.setDescricao(scred.getCredenciada().getNmFantasia()+ valor);					
					
					if(contrato.getPlano() != null)
						dto.setIdPlano(contrato.getPlano().getId());
						
					result.add(dto);
				}

				return result;
				
			// CREDENCIADA EXTERNA
			} else if (credenciadas.size() > 0 && tipoCredenciada == 0) { 

				for (ServicoCredenciada scred : credenciadas) {

					ServicoCredenciadaDTO dto = new ServicoCredenciadaDTO();
					dto.setCredenciada(scred.getCredenciada());
					
					if (boos.get("isLiberado")) {
						dto.setDescricao(scred.getCredenciada().getNmFantasia()
						        + (!boServicoAberto ? " | "
						                + currencyFormatter.format(scred.getVlCredenciadaAssociado()) : ""));
						dto.setValor(scred.getVlCredenciadaAssociado());
					} else {
						dto.setDescricao(scred.getCredenciada().getNmFantasia()
						        + (!boServicoAberto ? " | "
						                + currencyFormatter.format(scred.getVlCredenciada()) : ""));
						dto.setValor(scred.getVlCredenciada());
					}
					
					dto.setCobertura(boos.get("isCobertura"));
					dto.setCusto(scred.getVlCusto());
					
					if(contrato.getPlano() != null)
					dto.setIdPlano(contrato.getPlano().getId());
					
					result.add(dto);

				}

				return result;

			}
		}

		return null;
	}

	public List<ServicoProfissionalDTO> getProfissionais(String idsServico,
	        Long idCliente, Long idDependente, Long idContrato) {
		List<ServicoProfissionalDTO> result = new ArrayList<ServicoProfissionalDTO>();

		String[] servicos = idsServico.split(",");

		for (String string : servicos) {

			Agendamento agendamento = new Agendamento();
			Map<String, Double> mapDoubleValues = new HashMap<String, Double>();
			// setting this weird DTO
			PacienteDTO<?> paciente = null;
			
			Locale locale = new Locale("pt", "BR");
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
			
			Long idServico = Long.parseLong(string);
			Servico servico = servicoDAO.getServico(idServico);
			
			Cliente cliente = clienteService.getCliente(idCliente);
			
			if (idDependente != null) {
				agendamento.setDependente(clienteService.getDependente(idDependente));
				paciente = new PacienteDTO<Dependente>(agendamento.getDependente());
				
				ContratoDependente contDependente = contratoDependenteDAO.getContratoDependenteById(idDependente, idContrato);
				
				if (contDependente == null){
					// Adicionar dependente no contrato do cliente nao fazendo uso
					Contrato contratocli = contratoDAO.getContratoPorCliente(idCliente);
					ContratoDependente condep = new ContratoDependente();
					
					if (contratocli != null) {		
						
						condep.setCreditoConveniada(0);
						condep.setBlAutorizaDebAut(false);
						condep.setContrato(contratocli);
						condep.setInSituacao(0);
						condep.setDependente(agendamento.getDependente());
						condep.setBoFazUsoPlano(false);
	
						contratoDependenteDAO.persist(condep);
					}
				
					paciente.setContratoDependente(condep);
				}
				else				
					paciente.setContratoDependente(contDependente);								
			} else 
				paciente = new PacienteDTO<Cliente>(cliente);
			
			
			ContratoCliente cc = contratoDAO.getContratoCliente(idContrato, idCliente);
			paciente.setContratoCliente(cc);
			paciente.setCliente(cliente);

			agendamento.setPaciente(paciente);
			agendamento.setCliente(cliente);
			agendamento.setServico(servico);
			agendamento.setDtAgendamento(new Date());
			
			if (paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null){
				agendamento.setContrato(paciente.getContratoDependente().getContrato());
			}else{
				agendamento.setContrato(paciente.getContratoCliente().getContrato());
			}			

			Map<String, Boolean> boos = agendamentoService.verificarCobertura(servico, agendamento);			

			boolean boServicoAberto = (servico.getBoValorAberto() != null && servico.getBoValorAberto());

			// para as novas regras de negocio			
			List<ServicoProfissional> lsProfissionais = servicoDAO.getServicoProfissionais(idServico);
						
			if (lsProfissionais.size() > 0) {

				for (ServicoProfissional iProfissional : lsProfissionais) {
					
					ServicoProfissionalDTO dto = new ServicoProfissionalDTO();
					
					mapDoubleValues = agendamentoService
					        .getValoresPorProfissional(servico.getId(), iProfissional.getProfissional().getId(), paciente,
					                boos.get("isLiberado"),
					                boos.get("isServicoProfissional"), agendamento.getBoParticular());					
					
					dto.setProfissional(iProfissional.getProfissional());
					
					if(boos.get("isConvenio"))
						dto.setCobertura(boos.get("isGuia"));
					else					
						dto.setCobertura(boos.get("isCobertura"));
					
					dto.setDescricao((iProfissional.getProfissional().getNmFuncionario() 
							+ (!boServicoAberto ? " | " + currencyFormatter.format(mapDoubleValues.get("vlServico")) : "")));					
					dto.setValor(mapDoubleValues.get("vlServico"));
					
					if(paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null && paciente.getContratoDependente().getBoFazUsoPlano() == false)
						dto.setDescricao((iProfissional.getProfissional()
						        .getNmFuncionario() + (!boServicoAberto ? " | " + currencyFormatter.format(mapDoubleValues.get("vlServicoAux")) : "")));					
						dto.setValor(mapDoubleValues.get("vlServicoAux"));

					result.add(dto);
					mapDoubleValues.clear();
				}
				return result;
			}
		}

		return null;
	}

	public DespesaServico getDespesaServicoDisponivel(Long idServico,
	        Long idCliente, Long idDependente, Long idContrato,
	        DespesaServico despesaServicoAtual) {

		Agendamento agendamento = new Agendamento();
		PacienteDTO<?> paciente = null;
		Encaminhamento enc = new Encaminhamento();
		
		Servico servico = servicoDAO.getServico(idServico);
		Cliente cliente = clienteService.getCliente(idCliente);		
		
		if (idDependente != null) {
			agendamento.setDependente(clienteService.getDependente(idDependente));
			paciente = new PacienteDTO<Dependente>(agendamento.getDependente());
			
			ContratoDependente contDependente = contratoDependenteDAO.getContratoDependenteById(idDependente, idContrato);
			
			if (contDependente == null){
				// Adicionar dependente no contrato do cliente nao fazendo uso
				Contrato contratocli = contratoDAO.getContratoPorCliente(idCliente);
				ContratoDependente condep = new ContratoDependente();
				
				if (contratocli != null) {		
					
					condep.setCreditoConveniada(0);
					condep.setBlAutorizaDebAut(false);
					condep.setContrato(contratocli);
					condep.setInSituacao(0);
					condep.setDependente(agendamento.getDependente());
					condep.setBoFazUsoPlano(false);
	
					contratoDependenteDAO.persist(condep);
				}			
				paciente.setContratoDependente(condep);
			}
			else					
				paciente.setContratoDependente(contDependente);
			
		} else 
			paciente = new PacienteDTO<Cliente>(cliente);
				
				
		ContratoCliente cc = contratoDAO.getContratoCliente(idContrato, idCliente);
		paciente.setContratoCliente(cc);
		paciente.setCliente(cliente);

		agendamento.setPaciente(paciente);
		agendamento.setCliente(cliente);
		
		if (paciente.getContratoDependente() != null && paciente.getContratoDependente().getId() != null){
			agendamento.setContrato(paciente.getContratoDependente().getContrato());
		}else{
			agendamento.setContrato(paciente.getContratoCliente().getContrato());
		}
		
		agendamento.setServico(servico);

		Map<String, Boolean> boos = agendamentoService.verificarCobertura(servico, agendamento);

		Map<String, Double> mapDoubleValues = agendamentoService
		        .getValoresPorProfissional(servico.getId(), null, paciente,
		                boos.get("isLiberado"),
		                boos.get("isServicoProfissional"), agendamento.getBoParticular());
		
		if (despesaServicoAtual.getProfissionalDTO() != null
		        && despesaServicoAtual.getProfissionalDTO().getProfissional() != null) {
			try {
				
				ServicoProfissional sp = servicoDAO.getServicoProfissional(idServico, 
						despesaServicoAtual.getProfissionalDTO().getProfissional().getId());

				despesaServicoAtual.setBoEncaminhado(true);
				despesaServicoAtual.setServico(servico);
				despesaServicoAtual.setQtServico(1);
				despesaServicoAtual.setVlCusto(null);
				enc.setProfissional(sp.getProfissional());
				despesaServicoAtual.setEncaminhamento(enc);

				if ( boos.get("isCobertura") || (boos.get("isConvenio") && boos.get("isGuia")) ) {
					despesaServicoAtual.setInCoberto(true);
					despesaServicoAtual.setVlCoberto(mapDoubleValues.get("vlServico"));
					despesaServicoAtual.setVlServico(mapDoubleValues.get("vlServico"));
					despesaServicoAtual.setVlAberto(0D);					
				} else {					
					despesaServicoAtual.setInCoberto(false);
					despesaServicoAtual.setVlCoberto(0D);
					despesaServicoAtual.setVlServico(mapDoubleValues.get("vlServico"));
					despesaServicoAtual.setVlAberto(mapDoubleValues.get("vlServico"));
				}

			} catch (NoResultException e) {
				return null;
			}
		}

		if (despesaServicoAtual.getCredenciadaInternaDTO() != null
		        && despesaServicoAtual.getCredenciadaInternaDTO().getCredenciada() != null) {
			
			try {
				ServicoCredenciada sc = servicoDAO.getServicoCredenciada(
				        idServico, despesaServicoAtual.getCredenciadaInternaDTO().getCredenciada().getId());

				despesaServicoAtual.setBoEncaminhado(true);
				despesaServicoAtual.setServico(servico);
				despesaServicoAtual.setQtServico(1);
				despesaServicoAtual.setVlCusto(null);
				enc.setCredenciada(sc.getCredenciada());
				despesaServicoAtual.setEncaminhamento(enc);

				Double valor = 0D;

				if (boos.get("isConvenio")) {
					valor = mapDoubleValues.get("vlServico");
				} else if (boos.get("isLiberado")) {
					valor = sc.getVlCredenciadaAssociado();
				} else {
					valor = sc.getVlCredenciada();
				}

				if ( boos.get("isCobertura") || (boos.get("isConvenio") && boos.get("isGuia")) ) {
					despesaServicoAtual.setInCoberto(true);
					despesaServicoAtual.setVlCoberto(valor);
					despesaServicoAtual.setVlServico(valor);
					despesaServicoAtual.setVlAberto(0D);

				} else {
					despesaServicoAtual.setInCoberto(false);
					despesaServicoAtual.setVlCoberto(0D);
					despesaServicoAtual.setVlServico(valor);
					despesaServicoAtual.setVlAberto(valor);
				}

				despesaServicoAtual.setVlCusto(sc.getVlCusto());
			} catch (NoResultException e) {
				return null;
			}
		}

		if (despesaServicoAtual.getCredenciadaExternaDTO() != null
		        && despesaServicoAtual.getCredenciadaExternaDTO()
		                .getCredenciada() != null) {
			try {
				ServicoCredenciada sc = servicoDAO.getServicoCredenciada(idServico, 
						despesaServicoAtual.getCredenciadaExternaDTO().getCredenciada().getId());

				despesaServicoAtual.setBoEncaminhado(true);
				despesaServicoAtual.setServico(servico);
				despesaServicoAtual.setQtServico(1);
				despesaServicoAtual.setVlCusto(null);
				enc.setCredenciada(sc.getCredenciada());
				despesaServicoAtual.setEncaminhamento(enc);

				Double valor = 0D;

				if (boos.get("isConvenio")) {
					valor = mapDoubleValues.get("vlServico");

				} else if (boos.get("isLiberado")) {
					valor = sc.getVlCredenciadaAssociado();
				} else {
					valor = sc.getVlCredenciada();
				}

				if (boos.get("isCobertura")) {
					despesaServicoAtual.setInCoberto(true);
					despesaServicoAtual.setVlCoberto(valor);
					despesaServicoAtual.setVlServico(valor);
					despesaServicoAtual.setVlAberto(0D);

				} else {
					despesaServicoAtual.setInCoberto(false);
					despesaServicoAtual.setVlCoberto(0D);
					despesaServicoAtual.setVlServico(valor);
					despesaServicoAtual.setVlAberto(valor);
				}

				despesaServicoAtual.setVlCusto(sc.getVlCusto());

			} catch (NoResultException e) {
				return null;
			}
		}

		return despesaServicoAtual;
	}

	public List<ServicoDTO> getValoresServico(String idServico) {
	    return servicoDAO.getValoresServico(idServico);
    }
	
	public List<TipoConsultaDTO> getTipoConsulta(Long idServico, Long idFuncionario, Long idEspecialidade) {
	    return servicoDAO.getTipoConsulta(idServico, idFuncionario, idEspecialidade);
    }
	public DataCoberturaDTO getUltimoServicoCobertoValido(Long idServico, Long idPaciente, String tipoPaciente, Long idContrato) {
		Contrato contrato = contratoDAO.getContrato(idContrato);
		
		if(contrato != null && contrato.getPlano() != null && contrato.getPlano().getId() != null && contrato.getEmpresaCliente() == null ){
			
			CoberturaPlano cobertura = servicoDAO.getCoberturaPlano(contrato.getPlano().getId(), idServico);
			
			Long idCliente = null;
			Long idDependente = null;
			
			if(tipoPaciente.equals("D")){
				idDependente = idPaciente;
			}else{
				idCliente = idPaciente;
			}
			
			
			if(cobertura != null && cobertura.getValidadeContratual() != null && cobertura.getValidadeContratual().intValue() > 0){
				DataCoberturaDTO dataDTO = servicoDAO.getUltimoServicoCobertoValido(idServico, idContrato, 
						cobertura.getValidadeContratual(), idCliente, idDependente);
				if(dataDTO != null){
					dataDTO.setValidadeContratual(cobertura.getValidadeContratual());
					dataDTO.setDataProximoServico(DateUtil.getDatePlusDays(new Date(dataDTO.getDataCobertura().getTime()), cobertura.getValidadeContratual().intValue() + 1));
					return dataDTO;
				}
				
			}
			
		}
		
	    
	    return null;
    }
	
	//---------------------------------------------------------------
	// APLICATIVO
	
	public AppResponse getUltimoServicoCobertoValido(Long idServico, Long idContrato) {
		UsuarioRegister user = keycloakUtil.getCurrentUserByContext();
		if(user == null) {
			return new ErrorResponse(403, "Usuário não logado ou autorizado"); 
		}
		String tipoPaciente;
		if(user.getTipoUsuario().intValue() == TipoUsuario.CLIENTE.valorTipo) {
			tipoPaciente = "C";
		} else {
			tipoPaciente = "D";
		}
		DataCoberturaDTO result = getUltimoServicoCobertoValido(idServico, 
				user.getIdUsuario(), tipoPaciente, idContrato);
		return new SuccessResponse(200, result);
	}

	public Servico updateServico(Servico servico){
		
		servico = servicoDAO.update(servico);
		
		return servico;
	}

	public Servico saveServico(Servico servico) {
	
		servicoDAO.persist(servico);

		return servico;
	}

	public void inativarServico(Long id) {
		
		Servico servico = servicoDAO.searchByKey(Servico.class, id);
		servico.setDtExclusao(new Date());
		servico.setIdOperadorExclusao(securityService.getFuncionarioLogado().getId());
		
	}

}
