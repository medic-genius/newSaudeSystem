package br.com.medic.medicsystem.main.rest;

import java.net.URI;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;

import br.com.medic.medicsystem.main.service.BairroService;
import br.com.medic.medicsystem.persistence.model.Bairro;

@Path("/bairros")
@RequestScoped
public class BairroController extends BaseController {

	@Inject
	private BairroService bairroService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createBairro(Bairro bairro) {

		Bairro salvo = bairroService.saveBairro(bairro);

		return Response.created(URI.create("/bairros/" + salvo.getId())).build();

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateBairro(Bairro bairro) {

		if (bairro != null) {
			Bairro bairroAtualizado = bairroService.updateBairro(bairro);

			if (bairroAtualizado != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/find_by_nm")
	@Produces(MediaType.APPLICATION_JSON)
	public Bairro getBairro(@QueryParam("nm_bairro") String nmBairro, @QueryParam("id_cidade") Long idCidade) {
		if (nmBairro != null && idCidade != null) {
			return bairroService.getBairroByNm(nmBairro, idCidade);
		} else
		{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}


	@GET
	@Path("/find_address_by_cep")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddress(@QueryParam("cep") String cep) {
		JSONObject address = bairroService.getAddressByCep(cep);
		if(address != null && !address.has("erro"))
		{
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(address.toString()).build();
		} else
		{
			return Response.status(404).type(MediaType.APPLICATION_JSON).build();
		}
	}
}