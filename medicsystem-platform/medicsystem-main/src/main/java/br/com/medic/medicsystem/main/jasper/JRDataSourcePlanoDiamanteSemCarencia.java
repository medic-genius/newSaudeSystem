package br.com.medic.medicsystem.main.jasper;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.samples.AbstractReportGenerator;
import org.pentaho.reporting.libraries.resourceloader.Resource;
import org.pentaho.reporting.libraries.resourceloader.ResourceException;
import org.pentaho.reporting.libraries.resourceloader.ResourceManager;

import br.com.medic.medicsystem.persistence.dto.ContratoDocumentoDTO;

public class JRDataSourcePlanoDiamanteSemCarencia extends AbstractReportGenerator{
	
	ContratoDocumentoDTO contratoDTO;
	/**
	 * Default constructor for this sample report generator
	 */
	public JRDataSourcePlanoDiamanteSemCarencia()
	{
	 
	}
	 
	public JRDataSourcePlanoDiamanteSemCarencia( ContratoDocumentoDTO contratoDTO )
	{
		this.contratoDTO = contratoDTO;
	}
	
	/* Gerar relatorio no PENTAHO - Inicio */
	/**
	 * Retorna a definição do relatório que será usada para gerar o relatório. Neste caso, o 
	 * relatório será carregado e analisado a partir de um arquivo contido neste pacote.
	 *
	 * @return A definição de relatório carregado e analisado para ser usado na geração de relatórios.
	 */
	public MasterReport getReportDefinition()
	{
		try
		{
			// Using the classloader, get the URL to the reportDefinition file
			final ClassLoader classloader = this.getClass().getClassLoader();
			final URL reportDefinitionURL = classloader.getResource("/jasper/PlanoDiamanteSemCarencia.prpt");

			// Parse the report file
			final ResourceManager resourceManager = new ResourceManager();
			resourceManager.registerDefaults();
			final Resource directly = resourceManager.createDirectly(reportDefinitionURL, MasterReport.class);
			return (MasterReport) directly.getResource();
		}
		catch (ResourceException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/** 
	 * Retorna a fábrica de dados que será usada para gerar os dados usados ​​durante a geração do relatório. Neste exemplo, 
	 * retornaremos nulo uma vez que a fábrica de dados foi definida na definição do relatório. 
	 * 
	 * @retivar a fábrica de dados usada com o gerador de relatórios 
	 */ 
	public DataFactory getDataFactory () 
	{ 
		return null; 
	}
	
	/** 
	 * Retorna o conjunto de parâmetros do relatório de tempo de execução. Este relatório de exemplo usa os seguintes três parâmetros: 
	 * <ul> 
	 * <li> <b> Título do relatório </ b> - O texto do título na parte superior do relatório </ li> 
	 * <li> <b> Nomes dos clientes < / B> 
	 	- uma série de nomes de clientes para mostrar no relatório </ li> * <li> <b> Col ​​Headers BG Color </ b> - a cor de fundo para os cabeçalhos das colunas </ li> 
	 * </ ul> 
	 * 
	 * @ Retornar <code> null </ code> indicando que o gerador de relatório não usa nenhum parâmetro de relatório 
	 */ 
	
//	public Map<String, Object> getReportParameters() 
//	{ 
//		return null; 
//	}
	
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> getReportParameters()
	  {
	    final Map parameters = new HashMap<String, Object>();
	    
	    InputStream caminhoImagemlogo = getClass().getResourceAsStream("/jasper/imagensJasper/MEDLAB-NOVO-2016.png");
	    InputStream caminhoImagemAssinaturaContratante = getClass().getResourceAsStream("/jasper/imagensJasper/assinaturaContratadaPlanos.png");

	    Image AssinaturaContratante = null;
	    Image logomarca = null;
	    
	    try {
	    	
	    	//File pathToFile = new File( caminhoImagemlogo.getPath() );
	        logomarca = ImageIO.read(caminhoImagemlogo);
	        
	        //File pathToFile1 = new File( caminhoImagemrodape.getPath() );
	        AssinaturaContratante = ImageIO.read(caminhoImagemAssinaturaContratante);	    	
	    	
	        
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	    
	    String titularNaoFazUso = "";
	    if(contratoDTO.getTitularnaofazuso().equals(true))
	    	titularNaoFazUso = "X";	
	    
	    // 0 = SOLTEIRO / 1 = CASADO / 2 = VIUVO / 3 = DESQUITADO / 4 = DIVORCIADO / 5 = OUTROS
	    String estadoCivil = "";
	    if (contratoDTO.getEstadoCivil().equals("0"))
	    	estadoCivil = "Solteiro (a)";
	    else if (contratoDTO.getEstadoCivil().equals("1"))
	    	estadoCivil = "Casado (a)";
	    else if (contratoDTO.getEstadoCivil().equals("2"))
	    	estadoCivil = "Viúvo (a)";
	    else if (contratoDTO.getEstadoCivil().equals("3"))
	    	estadoCivil = "Desquitado (a)";
	    else if (contratoDTO.getEstadoCivil().equals("4"))
	    	estadoCivil = "Divorciado (a)";
	    else 
	    	estadoCivil = "Outros";
	    
	    
	    String sexoF = "";
	    String sexoM = "";
	    String novoPlano = "X";
	    
	    // SEXO 0 = MASC  1 = FEMININO
	    if (contratoDTO.getSexo().equals("1")){ 
	    	sexoF = "X";
	    } else if (contratoDTO.getSexo().equals("0")){
	    	sexoM = "X";
	    }
	    
	    parameters.put("logoMediclab", logomarca);
	    parameters.put("nrMatricula", contratoDTO.getNrMatricula());
	    parameters.put("estadoCivil", estadoCivil);
	    parameters.put("sexoF", sexoF);
	    parameters.put("sexoM", sexoM);
	    parameters.put("T1", novoPlano);
	    parameters.put("Tn", titularNaoFazUso);
	    //parameters.put("T2", );
	    //parameters.put("T3", );
	    parameters.put("nmTitular", contratoDTO.getNmTitular());
	    //parameters.put("mensTitular", contratoDTO.getMensalidadeTitular());
	    parameters.put("ocupacao", contratoDTO.getOcupacaoTitular());
	    parameters.put("nrRg", contratoDTO.getRgTitular());
	    parameters.put("nrCpf", contratoDTO.getCpfTitular());
	    parameters.put("ruaAv", contratoDTO.getLogradouroTitular());
	    parameters.put("nr", contratoDTO.getNrLogradouro());
	    parameters.put("apto", contratoDTO.getAptoLogradouro());
	    parameters.put("bloco", contratoDTO.getBlocoLogradouro());
	    parameters.put("complemento", contratoDTO.getComplementoLogradouro());
	    parameters.put("nmBairro", contratoDTO.getBairro());
	    parameters.put("nrCep", contratoDTO.getCep());
	    parameters.put("nmCidade", contratoDTO.getNmCidade());
	    parameters.put("UF", contratoDTO.getUf());
	    parameters.put("telefoneResidencial", contratoDTO.getTelefoneResidencial());
	    parameters.put("telefoneComercial", contratoDTO.getTelefoneComercial());
	    parameters.put("nrRamal", contratoDTO.getRamalTelComercial());
	    parameters.put("telefoneCelular", contratoDTO.getTelefoneCelular());
	    //parameters.put("C1", ); INDIVIDUAL
	    //parameters.put("C2", ); FAMILIAR
	    //parameters.put("Gp1", ); ESTADUAL
	    //parameters.put("Gp2", ); MUNICIPAL
	    //parameters.put("Gp3", ); FEDERAL
	    //parameters.put("Gp4", ); EMPRESARIAL
	    //parameters.put("Gp5", ); PARTICULAR
	    parameters.put("nmDependente1", contratoDTO.getNmDependente1());
	    parameters.put("nmDependente2", contratoDTO.getNmDependente2());
	    parameters.put("nmDependente3", contratoDTO.getNmDependente3());
	    parameters.put("nmDependente4", contratoDTO.getNmDependente4());
	    parameters.put("depNasc1", contratoDTO.getDtNascimento1());
	    parameters.put("depNasc2", contratoDTO.getDtNascimento2());
	    parameters.put("depNasc3", contratoDTO.getDtNascimento3());
	    parameters.put("depNasc4", contratoDTO.getDtNascimento4());
	    parameters.put("parent1", contratoDTO.getParentescoDept1());
	    parameters.put("parent2", contratoDTO.getParentescoDept2());
	    parameters.put("parent3", contratoDTO.getParentescoDept3());
	    parameters.put("parent4", contratoDTO.getParentescoDept4());
	    //parameters.put("mensDep1", contratoDTO.getMensalidadeDept1());
	    //parameters.put("mensDep2", contratoDTO.getMensalidadeDept2());
	    //parameters.put("mensDep3", contratoDTO.getMensalidadeDept3());
	    //parameters.put("mensDep4", contratoDTO.getMensalidadeDept4());
	    parameters.put("nmVendedor", contratoDTO.getNmVendedor());
	    parameters.put("formaPagamento", contratoDTO.getFormaPagamento());
	    parameters.put("dtPagt", contratoDTO.getDiaPagamento());
	    parameters.put("totalPlano", contratoDTO.getTotalPlano());
	    parameters.put("nmBanco", contratoDTO.getNmBanco());
	    parameters.put("nrAgencia", contratoDTO.getNrAgencia());
	    parameters.put("nrConta", contratoDTO.getNrConta());
	    parameters.put("dia", contratoDTO.getDia());
	    parameters.put("mes", contratoDTO.getMes());
	    parameters.put("ano", contratoDTO.getAno());
	    parameters.put("assinaturaContratado", AssinaturaContratante);
	    
	    return parameters;
	  }
	
	/* Gerar relatorio no PENTAHO - Fim */
}