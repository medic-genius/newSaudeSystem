package br.com.medic.medicsystem.main.vindi.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.exception.VindiException;

public class ResponseParser<T> {
	final Class<T> classType;
	
	public ResponseParser(Class<T> cType) {
		this.classType = cType;
	}
	
	private String getResponseParamName(boolean isSingle) {
		String path[] = classType.getName().split("\\.");
		String name = path[path.length-1].toLowerCase();
		String result = null;
		if(name.equals("customer")) {
			result = "customer";
		} else if(name.equals("paymentprofile")) {
			result = "payment_profile";
		} else if(name.equals("product")) {
			result = "product";
		} else if(name.equals("subscription")) {
			result = "subscription";
		} else if(name.equals("bill")) {
			result = "bill";
		}
		if(result != null && !isSingle) {
			result += "s";
		}
		return result;
	}
	
	public List<T> parseResponse(String response) throws Exception {
		JSONObject responseJson = new JSONObject(response);
		String paramName = getResponseParamName(false);
		if(responseJson.has(paramName) && !responseJson.isNull(paramName)) {
			List<T> list = new ArrayList<T>();
			JSONArray jsonArr = responseJson.getJSONArray(paramName);
			int len  =jsonArr.length();
			for(int i = 0; i < len; ++i) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);
				list.add(objectFromJSON(jsonObj));
			}
			return list;
		}
		// TODO Auto-generated method stub
		return null;
	};
	
	public T parseSingleResponse(String response) throws VindiException, Exception {
		String paramName = getResponseParamName(true);
		JSONObject jsonResponse = new JSONObject(response);
		if(jsonResponse.has(paramName) && !jsonResponse.isNull(paramName)) {
			return objectFromJSON(jsonResponse.getJSONObject(paramName));
		} else if(jsonResponse.has("errors")) {
			throw (new VindiException(response));
		}
		return null;
	}

	
	private T objectFromJSON(JSONObject obj) 
			throws JsonParseException, JsonMappingException, IOException {
		return (new ObjectMapper()).readValue(obj.toString(), this.classType);
	}
}
