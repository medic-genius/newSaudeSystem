package br.com.medic.medicsystem.main.service;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.exception.DespesaFinanceiroException;
import br.com.medic.medicsystem.main.mapper.ErrorType;
import br.com.medic.medicsystem.main.util.PropertiesLoader;
import br.com.medic.medicsystem.persistence.dao.ArquivoDespesaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dao.ConfiguracaoSistemaDAO;
import br.com.medic.medicsystem.persistence.dao.DespesaFinanceiroDAO;
import br.com.medic.medicsystem.persistence.dao.MovimentacaoContaDAO;
import br.com.medic.medicsystem.persistence.dto.ArquivoDespesaFinanceiroDTO;
import br.com.medic.medicsystem.persistence.dto.BaseDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroAgrupadaReportDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaFinanceiroServicoDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaGraficoDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.ArquivoDespesaFinanceiro;
import br.com.medic.medicsystem.persistence.model.ConfiguracaoSistema;
import br.com.medic.medicsystem.persistence.model.DespesaFinanceiro;
import br.com.medic.medicsystem.persistence.model.MovimentacaoConta;
import br.com.medic.medicsystem.persistence.model.MovimentacaoItemConta;
import br.com.medic.medicsystem.persistence.model.enums.AcaoMovimentacao;
import br.com.medic.medicsystem.persistence.model.enums.StatusPagamentoDespesa;
import br.com.medic.medicsystem.persistence.model.enums.TipoMovimentacao;
import br.com.medic.medicsystem.persistence.model.views.DespesaFinanceiroView;
import br.com.medic.medicsystem.persistence.security.SecurityService;
import br.com.medic.medicsystem.persistence.utils.DateUtil;
import br.com.medic.medicsystem.persistence.utils.UploadFileUtil;
import br.com.medic.medicsystem.persistence.utils.Utils;

@Stateless
public class DespesaFinanceiroService {

	@Inject
	@Named("despesafinanceiro-dao")
	DespesaFinanceiroDAO despesaFinanceiroDAO;
	
	@Inject
	@Named("configuracaosistema-dao")
	private ConfiguracaoSistemaDAO configuracaoSistemaDAO;

	@Inject
	@Named("arquivodespesafinanceiro-dao")
	private ArquivoDespesaFinanceiroDAO arquivoDespesaDAO;

	@Inject
	@Named("movimentacaoconta-dao")
	private MovimentacaoContaDAO movimentacaoContaDAO;
	
	@Inject
	private SecurityService securityService;
	
	public DespesaFinanceiro saveDespesaFinanceiro(DespesaFinanceiro despesaFinanceiro, Integer tipoFrequencia, Integer ocorrencia){
		DespesaFinanceiro despesaToPersist = null;
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		
		//se for enviado um documento com o número já existente não salva a despesa
		if(despesaFinanceiro.getId() == null && isNumeroDocumentoExistente(despesaFinanceiro.getNrDocumento()))
			throw new DespesaFinanceiroException(props.getProperty("nrdocumentoexistente"), ErrorType.DANGER);
		
		if( despesaFinanceiro.getNmDespesa() != null && despesaFinanceiro.getNmDespesa().trim().length() > 201 )
			throw new DespesaFinanceiroException(props.getProperty("descricaomaxlength"), ErrorType.DANGER);
		
		if( despesaFinanceiro.getNmObservacao() != null && despesaFinanceiro.getNmObservacao().trim().length() > 201 )
			throw new DespesaFinanceiroException(props.getProperty("observacaomaxlength"), ErrorType.DANGER);
		
		//se a despesa estiver quitada não será permitido alterar os campos referentes a valor
		if(isDespesaQuitada(despesaFinanceiro.getId())){
			
			DespesaFinanceiro salva = new DespesaFinanceiro(getDespesaFinanceiroById(despesaFinanceiro.getId()));
			despesaFinanceiro.setVlPago(salva.getVlPago());
			despesaFinanceiro.setDtPagamento(despesaFinanceiro.getDtPagamento());
			despesaFinanceiro.setDtVencimento(salva.getDtVencimento());
			despesaFinanceiro.setEmpresaFinanceiroPagante(salva.getEmpresaFinanceiroPagante());
			despesaFinanceiro.setContaBancaria(salva.getContaBancaria());
				
			despesaFinanceiroDAO.update(despesaFinanceiro);
			return despesaFinanceiro;
		}

		//ao pagar a despesa é obrigatório enviar sua data de pagamento
		if(despesaFinanceiro.getInStatus() == StatusPagamentoDespesa.PAGO.ordinal() && despesaFinanceiro.getDtPagamento() == null){
			throw new IllegalArgumentException("Data não especificada");
		}
		
		//se a despesa já estiver salva então não é possível criar recorrência da mesma, logo, apenas salvamos com as alterações 
		if(despesaFinanceiro.getId() != null){
			salvarDespesaEMovimentacao(despesaFinanceiro, AcaoMovimentacao.PAGAMENTO, TipoMovimentacao.SAIDA);
			return despesaFinanceiro;
		}
		
		//caso o tipo de frequencia e ocorrência não sejam preenchidos então apenas uma despesa será criada
		//sem recorrência
		if(tipoFrequencia == null || ocorrencia == null){
			tipoFrequencia = 1;
			ocorrencia = 1;
		}	

		int contadorParcelas = 1;
		
		while (contadorParcelas <= ocorrencia) {
			despesaToPersist = new DespesaFinanceiro(despesaFinanceiro);
			
			despesaToPersist.setNrQuantidadeParcelas(ocorrencia);
			despesaToPersist.setNrOrdemParcela(contadorParcelas);

			salvarDespesaEMovimentacao(despesaToPersist, AcaoMovimentacao.PAGAMENTO, TipoMovimentacao.SAIDA);
			contadorParcelas++;
			
			//seta no objeto despesa financeiro a próxima data, que será na próxima iteração caso houver
			despesaFinanceiro.setDtVencimento(getNextDate(despesaFinanceiro.getDtVencimento(), tipoFrequencia));
		}
		
		return despesaToPersist;
	}
	
	public DespesaFinanceiro getDespesaFinanceiroById(Long id){
		DespesaFinanceiro despesaFinanceiro = despesaFinanceiroDAO.searchByKey(DespesaFinanceiro.class, id); //uma parcela
		
		if(despesaFinanceiro.getDtExclusao() != null){
			throw new IllegalArgumentException("Despesa Inativa");
		}
		return despesaFinanceiro;
	}


	private boolean isDespesaQuitada(Long id){
		try{
			if(id == null)
				return false;

			DespesaFinanceiro despesa = getDespesaFinanceiroById(id);
			if(despesa != null && despesa.getInStatus() == StatusPagamentoDespesa.PAGO.ordinal()){
				return true;
			}
		}catch(ObjectNotFoundException | NullPointerException ex){
			ex.printStackTrace();
		}
		return false;
	}

	public DespesaFinanceiroAgrupadaReportDTO getDespesasDTOByServico(String nrServico, Long idEmpresaFinanceiroPagamente, String dtInicio, String dtFim, Long idContaBancaria){
				
		List<DespesaFinanceiro> despesas = despesaFinanceiroDAO.getDespesasFinanceiroByServico(nrServico, idEmpresaFinanceiroPagamente, dtInicio, dtFim, idContaBancaria);
		List<DespesaFinanceiroServicoDTO> dtos = new ArrayList<DespesaFinanceiroServicoDTO>();
		Double total  = new Double(0);
		Double totalPago = new Double(0);
		
		for (DespesaFinanceiro d : despesas) {
			String fornecedor = d.getFornecedor() == null ? null : d.getFornecedor().getNmNomeFantasia();
			String empresaPagante = d.getEmpresaFinanceiroPagante() == null ? null : d.getEmpresaFinanceiroPagante().getNmFantasia();
			String empresaSolicitante = d.getEmpresaFinanceiroSolicitante() == null ? null : d.getEmpresaFinanceiroSolicitante().getNmFantasia();
			String nmContaBancaria =  d.getContaBancaria() == null ? null : d.getContaBancaria().getNmContaBancaria();
			String nmBanco =  d.getContaBancaria() == null ? null : d.getContaBancaria().getBanco().getNmBanco();
			String centroCusto = d.getCentroCusto() == null ? null : d.getCentroCusto().getNmCentroCusto();
			total += d.getVlDespesa();
			totalPago += d.getVlPago();
		DespesaFinanceiroServicoDTO dto = new DespesaFinanceiroServicoDTO(d.getNmObservacao(), d.getNmDespesa() ,fornecedor, centroCusto, empresaSolicitante, empresaPagante, nmContaBancaria  ,d.getNrDocumento(), Utils.getDoubleDecimalPlaces(d.getVlDespesa(), 2),  Utils.getDoubleDecimalPlaces(d.getVlPago(), 2) ,nmBanco, d.getDtPagamento(), d.getNrQuantidadeParcelas(), d.getNrOrdemParcela() );
			dtos.add(dto);
		}
		
		DespesaFinanceiroAgrupadaReportDTO despesaAgrupadaDTO = new DespesaFinanceiroAgrupadaReportDTO( dtos, Utils.getDoubleDecimalPlaces(total, 2)  , Utils.getDoubleDecimalPlaces(totalPago, 2) );
		
		return despesaAgrupadaDTO;
	}
	

	private boolean isNumeroDocumentoExistente(String nrDocumento){
		
		if(nrDocumento == null){
			return false;
		}
		
		List<DespesaFinanceiro> d = getDespesaFinanceiroByNumeroDocumento(nrDocumento);

		if(d == null || d.isEmpty())
			return false;
		else
			return true;
	}
	
	/*private boolean isNrServicoExistenteMesmaEmpresa( Long idDespesa, String nrServico, Long idEmpresaFinanceiroPagante ) {
		boolean isExistente = false;
		
		List<DespesaFinanceiro> df = despesaFinanceiroDAO.verificarNrServicoExistenteMesmaEmpresa( idDespesa , nrServico, idEmpresaFinanceiroPagante );
		
		if( df != null && df.size() > 0 )
			isExistente = true;
		
		return isExistente;
	}*/
	
	public List<DespesaFinanceiro> getDespesaFinanceiroByNumeroDocumento(String nrDocumento){
		List<DespesaFinanceiro> despesasFinanceiro = despesaFinanceiroDAO.getDespesaFinanceiroByNumeroDocumento(nrDocumento);
		return despesasFinanceiro;
	}
	
	private DespesaFinanceiro salvarDespesaEMovimentacao(DespesaFinanceiro despesaFinanceiro, AcaoMovimentacao acao, TipoMovimentacao tipoMovimentacao){
		
		if(despesaFinanceiro.getId() != null){
			despesaFinanceiroDAO.update(despesaFinanceiro);
		}else{
			despesaFinanceiroDAO.persist(despesaFinanceiro);
		}
		
		//ao pagar a despesa salva a movimentação
		if(despesaFinanceiro.getInStatus() == StatusPagamentoDespesa.PAGO.ordinal()){
			saveMovimentacao(despesaFinanceiro, acao, tipoMovimentacao);
		}
		return despesaFinanceiro;
	}
		
		
	private void saveMovimentacao(DespesaFinanceiro despesaFinanceiro, AcaoMovimentacao acao, TipoMovimentacao tipoMovimentacao){
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		
		MovimentacaoConta movimentacao = new MovimentacaoConta();
		MovimentacaoItemConta itemConta = criaItemConta(despesaFinanceiro);
		itemConta.setDespesaFinanceiro(despesaFinanceiro);

		movimentacao.setItemsConta(new ArrayList<MovimentacaoItemConta>());
		
		if( acao.getId() == AcaoMovimentacao.ESTORNO.getId() ) {
			List<MovimentacaoConta> movContaPagamentoList = movimentacaoContaDAO.getMovimentacaoContaByIdDespesa( despesaFinanceiro.getId(), AcaoMovimentacao.PAGAMENTO.getId() );
			DateFormat dataformat = new SimpleDateFormat( "dd/MM/yyyy" );
			if( movContaPagamentoList != null) {
				MovimentacaoConta movContaPagamento = movContaPagamentoList.get(0);
				movContaPagamento.setHaveEstorno(true);
				movContaPagamento.setInfoEstorno( "Dt Estorno: " + dataformat.format(new Date())  + "\n" + "Op. Estorno: " + securityService.getFuncionarioLogado().getNmFuncionario()      );
				
				movimentacaoContaDAO.update(movContaPagamento);
				
			}
				
			movimentacao.setDtMovimentacao(despesaFinanceiro.getDtPagamento() );
		}			
		else 
			movimentacao.setDtMovimentacao(despesaFinanceiro.getDtPagamento());
		
		
		movimentacao.setContaBancaria(despesaFinanceiro.getContaBancaria());
		movimentacao.setAcaoMovimentacao(acao);
		movimentacao.setNrValor(despesaFinanceiro.getVlPago());
		movimentacao.setFuncionarioMovimentacao(securityService.getFuncionarioLogado());
		movimentacao.setInTipo(tipoMovimentacao);
		movimentacao.addMovimentacaoItemConta(itemConta);
		movimentacaoContaDAO.persist(movimentacao);
		
		
		Integer inSucesso = movimentacaoContaDAO.ajustarMovimentacaoContaPosPagamentoDespesa( despesaFinanceiro.getContaBancaria().getId(), movimentacao.getDtMovimentacao()  );
		
		if( inSucesso == 0)
			throw new DespesaFinanceiroException(props.getProperty("movimentacao.ajustarmovimentacao"), ErrorType.DANGER);
		
		
	}

	
	private MovimentacaoItemConta criaItemConta(DespesaFinanceiro despesaFinanceiro){
		MovimentacaoItemConta itemConta = new MovimentacaoItemConta();
		itemConta.setDespesaFinanceiro(despesaFinanceiro);
		return itemConta;
	}
	
	private List<DespesaFinanceiro> getAllDespesasFinanceiro(){
		List<DespesaFinanceiro> despesas = despesaFinanceiroDAO.getAllDespesasFinanceiro();
		return despesas;
	}
	
	public void deleteDespesaFinanceiro(Long id){
		
		/*DespesaFinanceiro despesaFin = getDespesaFinanceiroById(id);
		String nrDoc = despesaFin.getNrDocumento();
		List<DespesaFinanceiro> listAllParcelasDespesa = despesaFinanceiroDAO.getAllParcelas(nrDoc);
		for(int i=0; i <listAllParcelasDespesa.size(); i++) {*/
			DespesaFinanceiro despesaFinanceiro = getDespesaFinanceiroById(id);
			despesaFinanceiro.setDtExclusao(new Date());
			salvarDespesaEMovimentacao(despesaFinanceiro, AcaoMovimentacao.ESTORNO, TipoMovimentacao.ENTRADA);
		/*}*/
		
	}
	
	
/*	private List<DespesaFinanceiroDTO> excluirAllDespesas(String nrDocumento) {
		List<DespesaFinanceiroDTO> listAllParcelasDespesa = despesaFinanceiroDAO.getAllParcelas(nrDocumento);
		for(int i =0; i < listAllParcelasDespesa.size(); i++) {
			if(listAllParcelasDespesa.get(i).getDtExclusao() != null){
				throw new IllegalArgumentException("Despesa Inativa");
			}
			
		}
		return listAllParcelasDespesa;
	}*/
	public void deleteArquivoDespesaFinanceiro(Long id){
		ArquivoDespesaFinanceiro arquivo = arquivoDespesaDAO.searchByKey(ArquivoDespesaFinanceiro.class, id);
		arquivo.setDtExclusao(new Date());
	}
	
	public BaseDTO getQuantidadeDespesas(Long idEmpresa, Integer opcaoConsulta) {
		
		Long quantidade;
		Double valorTotal;
		Date hoje;
		if(opcaoConsulta == null){
			opcaoConsulta = 0;
		}
		switch (opcaoConsulta) {
		//todas as contas vencidas
		case 0:
			hoje = new Date();
			quantidade = despesaFinanceiroDAO.getQuantidadeDespesas(idEmpresa, null, hoje, 0);
			valorTotal = despesaFinanceiroDAO.getValorTotalDespesas(idEmpresa, null, hoje, 0);
			break;
		//títulos que vencem hoje (ou nos dias não úteis anteriores)	
		case 1:
			hoje = new Date();
			Date dataInicial;
			
			//se for segunda busca pela data de sábado até o dia de hoje
			if(DateUtil.getWeekDayNumber(hoje) == 2){
				dataInicial = DateUtil.getDateMinusDays(hoje, 2);
			}else{
				dataInicial  = hoje;
			}
			quantidade = despesaFinanceiroDAO.getQuantidadeDespesas(idEmpresa, dataInicial, hoje, 0);
			valorTotal = despesaFinanceiroDAO.getValorTotalDespesas(idEmpresa, dataInicial, hoje, 0);
			break;
			
		//títulos que vencem nos próximos 7 dias
		case 2:
			hoje = new Date();
			Date hojeMais7Dias = DateUtil.getDatePlusDays(hoje, 7);
			quantidade = despesaFinanceiroDAO.getQuantidadeDespesas(idEmpresa, hoje, hojeMais7Dias, 0);
			valorTotal = despesaFinanceiroDAO.getValorTotalDespesas(idEmpresa, hoje, hojeMais7Dias, 0);
			break;			
		default:
			throw new IllegalArgumentException("Opção de data inválida");
		}

		String valorTotalString = "0,00";
		if(valorTotal != null){
			valorTotalString = String.format("%.2f", valorTotal);
		}
		
		BaseDTO dto =  new BaseDTO(quantidade, valorTotalString);
		return dto;
	}
	
	public List<DespesaFinanceiro> getDespesas(Long idEmpresa, Integer opcaoConsulta, Integer inStatus, String dtInicio, String dtFim, Integer inModalidade) {

		List<DespesaFinanceiro> despesas = null;
		if(idEmpresa == null){
			despesas = getAllDespesasFinanceiro();
		}else if(idEmpresa != null && opcaoConsulta != null){
			despesas = getDespesasByOpcaoDataDefinida(idEmpresa, opcaoConsulta);
		} else if(idEmpresa != null && opcaoConsulta == null ){
			despesas = getDespesasByPeriodoEStatus(idEmpresa, inStatus, dtInicio, dtFim, inModalidade);
		}
		
		return despesas;
	}	
	
	private List<DespesaFinanceiro> getDespesasByPeriodoEStatus(Long idEmpresa, Integer inStatus, String dtInicio, String dtFim, Integer inModalidade) {
		List<DespesaFinanceiro> despesas = null;
		
		Date dateInicio = DateUtil.parseDate(dtInicio, DateUtil.DatePattern.DDMMAA.getPattern());
		Date dateFim = DateUtil.parseDate(dtFim, DateUtil.DatePattern.DDMMAA.getPattern());
		
		//busca as despesas pagas, caso o status seja nulo trás todas as despesas na data especificada
		if(inStatus == null || inStatus == StatusPagamentoDespesa.PAGO.ordinal() || inStatus == 7){ // instatus = 7 -> conciliado
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, dateInicio, dateFim, inStatus, inModalidade);
		} else if(inStatus == StatusPagamentoDespesa.ABERTO.ordinal()){
			//Para buscar as despesas em aberto, listamos as despesas não pagas e
			//com vencimento a partir do dia corrente
			if(dateInicio != null && dateInicio.before(new Date())){
				dateInicio = DateUtil.getStartOfDay(new Date());
			}
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, dateInicio, dateFim, inStatus, null);
		} else if(inStatus == StatusPagamentoDespesa.VENCIDO.ordinal()){
			//Para buscar as despesas vencidas, listamos todas as despesas não pagas e
			//com o vencimento até o dia corrente
			Date hoje = new Date();
			if(dateFim != null && (dateFim.after(new Date()) || DateUtil.isIgual(dateFim, hoje))){
				dateFim = DateUtil.getDateMinusDays(new Date(), 1);
			}
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, dateInicio, dateFim, 0, null );
		}
		
		return despesas;
	}	
	
	private List<DespesaFinanceiro> getDespesasByOpcaoDataDefinida(Long idEmpresa, Integer opcaoConsulta) {
		
		List<DespesaFinanceiro> despesas;
		Date hoje;
		
		switch (opcaoConsulta) {
		//todas as contas vencidas
		case 0:
			hoje = new Date();
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, null, hoje, 0, null);
			break;
		//títulos que vencem hoje (ou nos dias não úteis anteriores)	
		case 1:
			hoje = new Date();
			Date dataInicial;
			
			//se for segunda busca pela data de sábado até o dia de hoje
			if(DateUtil.getWeekDayNumber(hoje) == 2){
				dataInicial = DateUtil.getDateMinusDays(hoje, 2);
			}else{
				dataInicial  = hoje;
			}
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, dataInicial, hoje, 0,null);
			break;
			
		//títulos que vencem nos próximos 7 dias
		case 2:
			hoje = new Date();
			Date hojeMais7Dias = DateUtil.getDatePlusDays(hoje, 7);
			despesas = despesaFinanceiroDAO.getDespesas(idEmpresa, hoje, hojeMais7Dias, 0, null);
			break;			
		default:
			throw new IllegalArgumentException("Opção de data inválida");
		}

		return despesas;
	}
	
	private Date getNextDate(Date date, int type){
		switch (type) {
		case 0:
			return DateUtil.getDatePlusDays(date, 1);
		case 1:
			return DateUtil.getDatePlusDays(date, 7);
		case 2:
			return DateUtil.getDatePlusMonth(date, 1);
		case 3:
			return DateUtil.getDatePlusMonth(date, 2);			
		case 4:
			return DateUtil.getDatePlusMonth(date, 6);
		case 5:
			return DateUtil.getDatePlusYear(date, 1);
		default:
			return DateUtil.getDatePlusDays(date, 0);
		}
	}

	public List<DespesaFinanceiro> getDespesasFinanceiroByEmpresaFinanceiro(Long id) {
		List<DespesaFinanceiro> despesas = despesaFinanceiroDAO.getDespesasFinanceiroByEmpresaFinanceiro( id );
		return despesas;
	}
	
	public List<DespesaGraficoDTO> getGraficoAnualDTO(Long idEmpresa){
		List<DespesaGraficoDTO> list;
		
		Date dataInicio = DateUtil.getDateWithFirstDayOfMonth(new Date());
		dataInicio = DateUtil.getDateMinusMonths(dataInicio, 6);

    	Date dataFim = DateUtil.getDateWithFirstDayOfMonth(new Date());
    	dataFim = DateUtil.getDatePlusMonth(dataFim, 6);
    	
    	list = despesaFinanceiroDAO.getGraficoDespesa(idEmpresa, dataInicio, dataFim);
		
    	return list;
	}
	
	private String getFullPathFile() {
		ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
		return configuraSistema.getNmPathArquivoDespesa();
	}
	
	private String getSeparador() {
		ConfiguracaoSistema configuraSistema = configuracaoSistemaDAO.getConfiguracaoSistema();
		return configuraSistema.getNmSeparador();
	}
	
 	public void importaArquivosDespesa(MultipartFormDataInput input, Long idDespesa) {
		
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		
		DespesaFinanceiro despesa = getDespesaFinanceiroById(idDespesa);
		
		if(despesa != null) {	
							
			for(int i=0; i<uploadForm.size(); i++){	
										
				List<InputPart> inputParts = uploadForm.get("file");
				String subDiretorio = "";
				String diretorio =  getFullPathFile() + despesa.getId();
				String separador = getSeparador();
				
				for (InputPart inputPart : inputParts) {		
					
					try {
						
						MultivaluedMap<String, String> header = inputPart.getHeaders();
						
						String fileName = UploadFileUtil.getFileName(header);		
						String fileType = UploadFileUtil.getFileType(header);
						
						InputStream inputStream = inputPart.getBody(InputStream.class,null);					
						
						byte[] bytes = IOUtils.toByteArray(inputStream);
						String fileNameClean = UploadFileUtil.retirarExtensao(fileName) + "_" + System.currentTimeMillis();
						fileName = fileNameClean + "." + fileType; 
						
						subDiretorio = fileType.toLowerCase().trim().equals("pdf") ? separador + "documentos" : separador + "imagens";
						diretorio = diretorio + subDiretorio ;
						
						String pathFileName = diretorio + separador + fileName;		
						
						boolean isSuccess = UploadFileUtil.writeFile(bytes, pathFileName, diretorio);		
						if(isSuccess) {
							String urlRelativa = despesa.getId() + subDiretorio + separador + fileName; 
							ArquivoDespesaFinanceiro arquivo = new ArquivoDespesaFinanceiro(fileNameClean, new Date(), urlRelativa, fileType, despesa);
							arquivoDespesaDAO.persist(arquivo);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}		
				}
			}	
		}
	}

	public File getArquivoDespesa(Long idFile) {
		ArquivoDespesaFinanceiro arquivoFuncionario = arquivoDespesaDAO.searchByKey(ArquivoDespesaFinanceiro.class, idFile);
		return new File(arquivoFuncionario.getNmPath());
	}
	
	public ArquivoDespesaFinanceiroDTO getArquivosFuncionario(Long idDespesa) {
		List<ArquivoDespesaFinanceiro> arquivosDespesa = arquivoDespesaDAO.getArquivosByDespesa(idDespesa);
		ArquivoDespesaFinanceiroDTO dto = new ArquivoDespesaFinanceiroDTO();
		
		if(arquivosDespesa != null && arquivosDespesa.size() != 0){
			dto.setIdDespesa(arquivosDespesa.get(0).getId());
			dto.setArquivos(arquivosDespesa);
		}
		
		return dto;
	}	
	
	
	
	/**/
	public List<DespesaFinanceiro> saveDespesaFinanceiro2(DespesaFinanceiro despesaFinanceiro, Integer tipoFrequencia, Integer ocorrencia){
		List<DespesaFinanceiro> listaResult = new ArrayList<DespesaFinanceiro>();
		DespesaFinanceiro despesaToPersist = null;
		Properties props = PropertiesLoader.getInstance().load("messageDespesaFinanceiro.properties");
		
		//se for enviado um documento com o número já existente não salva a despesa
		if(despesaFinanceiro.getId() == null && isNumeroDocumentoExistente(despesaFinanceiro.getNrDocumento()))
			throw new DespesaFinanceiroException(props.getProperty("nrdocumentoexistente"), ErrorType.DANGER);
		
		if( despesaFinanceiro.getNmDespesa() != null && despesaFinanceiro.getNmDespesa().trim().length() > 201 )
			throw new DespesaFinanceiroException(props.getProperty("descricaomaxlength"), ErrorType.DANGER);
		
		if( despesaFinanceiro.getNmObservacao() != null && despesaFinanceiro.getNmObservacao().trim().length() > 201 )
			throw new DespesaFinanceiroException(props.getProperty("observacaomaxlength"), ErrorType.DANGER);

		//se a despesa estiver quitada não será permitido alterar os campos referentes a valor
		if(isDespesaQuitada(despesaFinanceiro.getId())){
			
			DespesaFinanceiro salva = new DespesaFinanceiro(getDespesaFinanceiroById(despesaFinanceiro.getId()));
			despesaFinanceiro.setVlPago(salva.getVlPago());
			despesaFinanceiro.setDtPagamento(despesaFinanceiro.getDtPagamento());
			despesaFinanceiro.setDtVencimento(salva.getDtVencimento());
			despesaFinanceiro.setEmpresaFinanceiroPagante(salva.getEmpresaFinanceiroPagante());
			despesaFinanceiro.setContaBancaria(salva.getContaBancaria());
			despesaFinanceiro.setInFormaPagamento(salva.getInFormaPagamento());
				
			despesaFinanceiroDAO.update(despesaFinanceiro);
			listaResult.add(despesaFinanceiro);
			return listaResult;
		}

		//ao pagar a despesa é obrigatório enviar sua data de pagamento
		if(despesaFinanceiro.getInStatus() == StatusPagamentoDespesa.PAGO.ordinal() && despesaFinanceiro.getDtPagamento() == null){
			throw new IllegalArgumentException("Data não especificada");
		}
		
		//se a despesa já estiver salva então não é possível criar recorrência da mesma, logo, apenas salvamos com as alterações 
		if(despesaFinanceiro.getId() != null){
			salvarDespesaEMovimentacao(despesaFinanceiro, AcaoMovimentacao.PAGAMENTO, TipoMovimentacao.SAIDA);
			listaResult.add(despesaFinanceiro);
			return listaResult;
//			return despesaFinanceiro;
		}
		
		//caso o tipo de frequencia e ocorrência não sejam preenchidos então apenas uma despesa será criada
		//sem recorrência
		if(tipoFrequencia == null || ocorrencia == null){
			tipoFrequencia = 1;
			ocorrencia = 1;
		}	

		int contadorParcelas = 1;
		
		while (contadorParcelas <= ocorrencia) {
			despesaToPersist = new DespesaFinanceiro(despesaFinanceiro);
			
			despesaToPersist.setNrQuantidadeParcelas(ocorrencia);
			despesaToPersist.setNrOrdemParcela(contadorParcelas);

			salvarDespesaEMovimentacao(despesaToPersist, AcaoMovimentacao.PAGAMENTO, TipoMovimentacao.SAIDA);
			contadorParcelas++;
			
			//seta no objeto despesa financeiro a próxima data, que será na próxima iteração caso houver
			despesaFinanceiro.setDtVencimento(getNextDate(despesaFinanceiro.getDtVencimento(), tipoFrequencia));
			
			listaResult.add(despesaToPersist);
		}
		
		return listaResult;
	}

	public List<DespesaFinanceiroDTO> getDespesasByCC(List<String> idEmpresa, Long dtAno, Long dtMes, Integer inStatus,
			Integer idContaContabil) {
		String idEmpresaConver = converterListaEmString(idEmpresa);
		List<DespesaFinanceiroDTO> despesas = despesaFinanceiroDAO.getDespesasByCC(idEmpresaConver, dtAno, dtMes, inStatus, idContaContabil);
		return despesas;
	}
	
	public List<DespesaFinanceiroDTO> getDespesasByCentroCusto(List<String> idEmpresa, Long dtAno, Long dtMes, Integer inStatus,
			Integer idCentroCusto) {
		
		String idEmpresaConver = converterListaEmString(idEmpresa);
		List<DespesaFinanceiroDTO> despesas = despesaFinanceiroDAO.getDespesasByCentroCusto(idEmpresaConver, dtAno, dtMes, inStatus, idCentroCusto);
		return despesas;
	}
	
	public String converterListaEmString(List<String> lista) {
		StringBuilder str = new StringBuilder();
		for (String elemento : lista) {
			str.append(elemento).append(",");
		}
		
		if(str.toString().isEmpty()) {
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
	}
	public String converterListaEmString2(List<Long> lista) {
		StringBuilder str = new StringBuilder();
		for (Long elemento : lista) {
			str.append(elemento).append(",");
		}
		
		if(str.toString().isEmpty()) {
			return null;
		} else {
			return str.toString().substring(0, str.length()-1);
		}
	}

	public void salvarArquivoDespesa(String bodyStr, List<Long> idsDespesa) {
		JSONObject obj = new JSONObject(bodyStr);
		String imagemAnexo  = (String) obj.get("imagemAnexo");
		//String ids = converterListaEmString2(idsDespesa);
		//DespesaFinanceiro despesa = getDespesaFinanceiroById(idDespesa);
		List<DespesaFinanceiro> despesas = getDespesasFinanceiroQrCode(idsDespesa);
		String subDiretorio = "";
		String diretorio =  getFullPathFile() + despesas.get(0).getId();
		String separador = getSeparador();
		try {
			byte[] imageByte = Base64.decodeBase64(imagemAnexo);// Base64.getDecoder().decode(imagemAnexo);
			//String decodeString = new String(imageByte);

			subDiretorio = separador +"anexos";
			diretorio = diretorio + subDiretorio ;
			Long numFile = (Math.round(Math.random()*100));
			String fileName = numFile.toString();
			String fileType = "jpeg";
		
			String pathFileName = diretorio + separador + fileName ;		
			
			boolean isSuccess = UploadFileUtil.writeFile(imageByte, pathFileName +"."+ fileType, diretorio);		
			if(isSuccess) {
				String urlRelativa = despesas.get(0).getId() + subDiretorio + separador + fileName+ "." + fileType; 
				for(int i=0; i < despesas.size(); i++){
					ArquivoDespesaFinanceiro arquivo = new ArquivoDespesaFinanceiro(fileName, new Date(), urlRelativa, fileType, despesas.get(i));
					arquivoDespesaDAO.persist(arquivo);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	
	}

	private List<DespesaFinanceiro> getDespesasFinanceiroQrCode(List<Long> idsDespesa) {
		List<DespesaFinanceiro> result = new ArrayList<DespesaFinanceiro>();
		for(int i =0; i < idsDespesa.size(); i++){
			DespesaFinanceiro despesaFinanceiro = despesaFinanceiroDAO.searchByKey(DespesaFinanceiro.class, idsDespesa.get(i)); //uma parcela
			
			if(despesaFinanceiro.getDtExclusao() != null){
				throw new IllegalArgumentException("Despesa Inativa");
			}
			result.add(despesaFinanceiro);
		}
		
	return result;
	}

	public List<DespesaFinanceiroView> getBuscaDespesaFinanceiro(String nmDespesa, Long idEmpresa, String dtInicio, String dtFim, String nrDocumento, Long idContaContabil, Integer startPosition,
			Integer maxResults, Long idContaBancaria) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		Date dataFim = null;
		try {
			date = (Date)formatter.parse(dtInicio);
			dataFim = (Date)formatter.parse(dtFim);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return despesaFinanceiroDAO.getBuscaDespesaFinanceiro(nmDespesa, idEmpresa, date, dataFim, nrDocumento,idContaContabil, startPosition, maxResults, idContaBancaria);
	}
	
	public DespesaFinanceiro updateDespesaFinanceiro(long idDespesa, Double valor, Double vlJurosMulta, Double vlDesconto){
		DespesaFinanceiro despesa = despesaFinanceiroDAO.searchByKey(DespesaFinanceiro.class, idDespesa);
		Double novoValorDesp = 0D;
		if(valor != null){//qd utilizado o valor do extrato
			despesa.setVlDespesa(valor);
		}else if(vlJurosMulta != null){//quando utilizado para juros
			despesa.setVlMulta(vlJurosMulta);
		}else if(vlDesconto != null){
			despesa.setVlDesconto(vlDesconto);
			novoValorDesp = despesa.getVlDespesa() - despesa.getVlDesconto();
			despesa.setVlDespesa(novoValorDesp);
		}
	
			despesaFinanceiroDAO.update(despesa);
		
		
		return despesa;
	}
}


