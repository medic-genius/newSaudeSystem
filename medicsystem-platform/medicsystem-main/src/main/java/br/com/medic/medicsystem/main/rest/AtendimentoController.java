package br.com.medic.medicsystem.main.rest;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.AtendimentoService;
import br.com.medic.medicsystem.persistence.dto.TriagemDTO;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.views.AtendimentoMedico;



@Path("/atendimentos")
@RequestScoped
public class AtendimentoController {
	
	@Inject
	private AtendimentoService atendimentoService;
	
	
	
	
	
	@POST
	@Path("/{id:[0-9][0-9]*}/save")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveAtendimento(
			@PathParam("id") Long idAgendamento,Atendimento atendimento) {

		boolean salvo = atendimentoService.saveAtendimento(idAgendamento, atendimento);
		
		if (salvo) {
			return Response.ok().build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/atendimento")
	@Produces(MediaType.APPLICATION_JSON)
	public AtendimentoMedico getAtendimentoPorAgendamento(
	        @PathParam("id") Long idagendamento) {
		if (idagendamento != null) {

			 AtendimentoMedico result = atendimentoService.getAtendimentoPorIdAgendamento(idagendamento);		
			
				return result;
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAtendimento(
			@PathParam("id") Long idAgendamento,Atendimento atendimento) {
		
		if(idAgendamento != null && atendimento != null){
			
		boolean atualizado = atendimentoService.updateAtendimento(idAgendamento, atendimento);
		
		if (atualizado) {
			return Response.ok().build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}
			
		}else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/triageminfo/{intipopaciente}")
	@Produces(MediaType.APPLICATION_JSON)
	public TriagemDTO getDadosTriagemPaciente(
	        @PathParam("id") Long idPaciente, @PathParam("intipopaciente") String inTipoPaciente) {
		if (idPaciente != null && inTipoPaciente != null ) {

			TriagemDTO result = atendimentoService.getDadosTriagemPaciente(idPaciente, inTipoPaciente);		
			
				return result;
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/updatetriagem/{idagendamento:[0-9][0-9]*}/{intipopaciente}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTriagem(
	        @PathParam("id") Long idPaciente, 
	        @PathParam("idagendamento") Long idAgendamento, 
	        @PathParam("intipopaciente") String inTipoPaciente,
	        TriagemDTO triagemDTO) {
		if (idPaciente != null && inTipoPaciente != null ) {

			boolean result = atendimentoService.updateTriagem(idPaciente, inTipoPaciente, triagemDTO, idAgendamento);		
			
			if(result){
				return Response.ok().build();
			}else{
				
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/agendamentos/{id:[0-9][0-9]*}/check_finalizacao")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> checkFinalizacaoAtendimento(@PathParam("id") Long idAgendamento) {
		if(idAgendamento != null) {
			Boolean result = atendimentoService.checkFinalizacaoAtendimento(idAgendamento);
			Map<String, Boolean> response = new HashMap<String, Boolean>();
			response.put("lerCodigoDeBarras", result);
			return response;
		}
		return null;
	}
}
