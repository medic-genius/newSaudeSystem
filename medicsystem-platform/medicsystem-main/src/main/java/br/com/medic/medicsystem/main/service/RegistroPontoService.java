package br.com.medic.medicsystem.main.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.RegistroPontoDAO;
import br.com.medic.medicsystem.persistence.dao.UnidadeDAO;
import br.com.medic.medicsystem.persistence.dto.RegistroPontoDTO;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.RegistroPonto;
import br.com.medic.medicsystem.persistence.model.Unidade;

@Stateless
public class RegistroPontoService {
			
	@Inject
	@Named("registroponto-dao")
	private RegistroPontoDAO registroPontoDAO;
	
	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	
	@Inject
	@Named("unidade-dao")
	private UnidadeDAO unidadeDAO;
	
	public RegistroPonto saveRegistroPonto(Long idFuncionario, Long idUnidade){

		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		long datahoraEmMilisegundos = c.getTimeInMillis();
		Timestamp ts = new Timestamp(datahoraEmMilisegundos);
		
		RegistroPonto registroPonto = new RegistroPonto();
		Funcionario funcionario = funcionarioDAO.getFuncionario(idFuncionario);
		Unidade unidade = unidadeDAO.searchByKey(Unidade.class, idUnidade);
		
		registroPonto.setDtRegistro(new Date());
		registroPonto.setHrRegistro(ts);
		registroPonto.setUnidade(unidade);
		registroPonto.setFuncionario(funcionario);
								
		RegistroPonto pontoRegistrado = registroPontoDAO.persist(registroPonto); 
		
		return pontoRegistrado;	
		
	}
	
	public void saveRegistroPonto( List<String> listHorarioPonto, Long idUnidade,Long idFuncionario, String dtRegistro) throws ParseException {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dfA = new SimpleDateFormat("HH:mm:ss");
		Calendar c = new GregorianCalendar();
		
		//dfA.setTimeZone(TimeZone.getTimeZone("UTC"));
		Funcionario funcionario = funcionarioDAO.getFuncionario(idFuncionario);
		Unidade unidade = unidadeDAO.searchByKey(Unidade.class, idUnidade);
				
		for(String rp : listHorarioPonto) {
			
			c.setTime(dfA.parse(rp));
			long datahoraEmMilisegundos = c.getTimeInMillis();
			Timestamp ts = new Timestamp(datahoraEmMilisegundos);
			
			RegistroPonto registroPonto = new RegistroPonto();
			registroPonto.setDtRegistro(df.parse(dtRegistro));
			registroPonto.setHrRegistro(ts);
			registroPonto.setUnidade(unidade);
			registroPonto.setFuncionario(funcionario);		
			
			RegistroPonto saved = registroPontoDAO.persist(registroPonto);

		}
		
		
	}
		
	public Boolean updateRegistroPonto(List<RegistroPonto> listRegistroPonto){
					
		for(RegistroPonto rp : listRegistroPonto){
						
			try {
				if(rp.getId() != null)
					registroPontoDAO.update(rp);

				else {
					registroPontoDAO.persist(rp);
				}
		
			} catch (Exception e) {
				// TODO: handle exception
				return false;
			}
		}	 
		
		return true;
	}
	
	public List<RegistroPontoDTO> getFuncionarioPeriodo(Long idUnidade, Date dtInicio, Date dtFim, Integer grupoFuncionario){
		
		List<RegistroPontoDTO> listRegistroPonto = registroPontoDAO.getFuncionarioPeriodo(idUnidade, dtInicio, dtFim, grupoFuncionario);
		 
		if(listRegistroPonto != null){
				
			return listRegistroPonto;
		}else
			return null;		 
		 
	}
	
	public List<RegistroPontoDTO> getRegistroFuncionarioPeriodo(Long idUnidade, Date dtInicio, Date dtFim, Long idFuncionario){
		
		List<RegistroPontoDTO> listRegistroPonto = registroPontoDAO.getRegistroFuncionarioPeriodo(idUnidade, dtInicio, dtFim, idFuncionario);
						 
		if(listRegistroPonto != null){
			for(RegistroPontoDTO rp : listRegistroPonto)			
				rp.setListRegistros( registroPontoDAO.getAllRegistroFuncionarioDia(rp.getIdFuncionario(), rp.getDtRegistro(), idUnidade) );
		
			return listRegistroPonto;
		}
		else
			return null;		 
		 
	}
		
	
	public RegistroPonto getUltimoRegistroPontoFuncionarioDia(Long idFuncionario, Long idUnidade){
		
		return registroPontoDAO.getUltimoRegistroPontoFuncionarioDia(idFuncionario, idUnidade);
			
	}

	public List<RegistroPonto> getRegistroPonto(Long idFuncionario, String dtRegistro, Long idUnidade) throws ParseException {
		
		SimpleDateFormat dt = new SimpleDateFormat ("yyyy-MM-dd");
		
		return registroPontoDAO.getAllRegistroFuncionarioDia(idFuncionario, dt.parse(dtRegistro), idUnidade);
	}
}
