package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.TemplateProfissionalService;
import br.com.medic.medicsystem.persistence.model.TemplateProfissional;

@Path("/templateprofissional")
@RequestScoped
public class TemplateProfissionalController {
	
	@Inject
	private TemplateProfissionalService templateProfissionalService;
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public TemplateProfissional getTemplateProfissional(@PathParam("id") Long idTemplateProfissional){
		if(idTemplateProfissional != null){
			return templateProfissionalService.getTemplateProfissional(idTemplateProfissional);
		}		
		throw new WebApplicationException(Response.Status.BAD_REQUEST);
	}
	
	@PUT
	@Path("/{id:[0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public TemplateProfissional updateTemplate(@PathParam("id") Long idTemplate, 
			TemplateProfissional templateProfissional){
		if(templateProfissional == null) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		return templateProfissionalService.updateTemplateProfissional(idTemplate, templateProfissional);
	}
	
	@DELETE
	@Path("/{id:[0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteTemplateProfissional(@PathParam("id") Long idTemplate) {
		TemplateProfissional tpl = templateProfissionalService.deleteTemplateProfissional(idTemplate);
		if(tpl != null) {
			return Response.ok().build();
		}
		return Response.serverError().build();
	}
	
	@GET
	@Path("/medicos/{func:[0-9][0-9]*}/especialidades/{esp:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TemplateProfissional> getTemplateProfissional(@PathParam("esp") Long idEspecialidade, @PathParam("func") Long idFuncionario){
		if(idEspecialidade != null && idFuncionario!= null){
			return templateProfissionalService.getListTemplateProfissionalPorEspProf(idEspecialidade, idFuncionario);
		}		
		else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@POST
	@Path("/medicos/{func:[0-9]+}/especialidades/{esp:[0-9]+}")
	public TemplateProfissional saveTemplateProfissional(
			@PathParam("esp") Long idEspecialidade, @PathParam("func") Long idFuncionario,
			TemplateProfissional template) {
		if(idEspecialidade == null || idFuncionario == null || template == null) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		return templateProfissionalService.saveTemplateProfissional(idEspecialidade, idFuncionario, template);
	}
}
