package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.medic.medicsystem.main.service.CidadeService;
import br.com.medic.medicsystem.persistence.model.Bairro;
import br.com.medic.medicsystem.persistence.model.Cidade;

@Path("/cidades")
@RequestScoped
public class CidadeController extends BaseController {

	@Inject
	private CidadeService cidadeService;

	@Context
	protected UriInfo info;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCidade(Cidade cidade) {
		
		Cidade salvo = cidadeService.saveCidade(cidade);
		
		return Response.created(URI.create("/cidades/"+ salvo.getId())).build();
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Cidade> getAllCidades() {

		return cidadeService.getAllCidades();
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/bairros")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Bairro> getBairrosPorCidade(@PathParam("id") Long idCidade) {
		
		if (idCidade != null){

			return cidadeService.getBairrosPorCidade(idCidade);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/find_by_nm")
	@Produces(MediaType.APPLICATION_JSON)
	public Cidade getCidade(@QueryParam("nm_cidade") String nmCidade, @QueryParam("nm_uf") String nmUf)
	{
		if (nmCidade != null && nmUf!= null){
			return cidadeService.getCidadeByNm(nmCidade, nmUf);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
}