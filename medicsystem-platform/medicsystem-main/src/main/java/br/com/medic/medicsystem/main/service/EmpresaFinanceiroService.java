package br.com.medic.medicsystem.main.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

//import br.com.medic.medicsystem.persistence.dao.ContaBancariaDAO;
import br.com.medic.medicsystem.persistence.dao.EmpresaFinanceiroDAO;
//import br.com.medic.medicsystem.persistence.model.ContaBancaria;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;

/**
 * 
 * @author Joelton
 * 
 * @since 28/03/2016
 *
 */

@Stateless
public class EmpresaFinanceiroService {

	@Inject
	private Logger logger;
		
	@Inject
	@Named("empresafinanceira-dao")
	private EmpresaFinanceiroDAO empresafinanceiroDAO;
	
//	@Inject
//	@Named("contabancaria-dao")
//	private ContaBancariaDAO contaBancariaDAO;
	
	public List<EmpresaFinanceiro> getEmpresasFinanceiroAtivas() {
		
		logger.debug("Obtendo lista de empresas financeiras ativas...");
		
		return empresafinanceiroDAO.getEmpresasFinanceiroAtivas();
		
	}

// To next version
	
//	public List<ContaBancaria> getContasBancariaPorEmpresaFinanceira( Long idEmpresaFinanceira ) {
//		
//		logger.debug("Obtendo lista de  contas bancarias de uma empresa financeira...");
//		
//		return contaBancariaDAO.getContasBancariaPorEmpresaFinanceira(idEmpresaFinanceira);
//		
//	}
	
	public EmpresaFinanceiro getEmpresaFinanceiro( Long id) {
		
		logger.debug("Obtendo empresa financeiro...");

		return empresafinanceiroDAO.searchByKey(EmpresaFinanceiro.class, id);
		
	}

	public List<EmpresaFinanceiro> getEmpresasFinanceiro( boolean isEmpresaOficial ) {
		logger.debug("Obtendo empresas financeiro...");

		return empresafinanceiroDAO.getEmpresasFinanceiro( isEmpresaOficial );
	}
	
	
	
}
