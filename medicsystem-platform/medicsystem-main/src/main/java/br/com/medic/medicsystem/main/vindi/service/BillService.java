package br.com.medic.medicsystem.main.vindi.service;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.bill.Bill;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.response.ResponseParser;
import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;

public class BillService extends BaseHttpService {
	private final String BASE_URL = "/api/v1/bills";
	private ResponseParser<Bill> parser = new ResponseParser<Bill>(Bill.class);
	
	public BillService() {
		super();
		setUseSSL(true);
	}
	
	public Bill createBill(br.com.medic.medicsystem.main.vindi.data.parameters.create.Bill bill) throws VindiException {
		String url = BASE_URL;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		try {
			jsonStr = mapper.writeValueAsString(bill);
			post.setEntity(new StringEntity(jsonStr));
			
			String response = sendRequest(url, post);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
