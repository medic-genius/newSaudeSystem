package br.com.medic.dashboard.main.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.dashboard.persistence.dao.ReportAgendamentoDAO;
import br.com.medic.dashboard.persistence.dao.ReportClienteDAO;
import br.com.medic.dashboard.persistence.dao.ReportFaturamentoDAO;
import br.com.medic.dashboard.persistence.dao.ReportPlanoDAO;
import br.com.medic.dashboard.persistence.model.AgendamentoUnidade;
import br.com.medic.dashboard.persistence.model.ClientePlano;
import br.com.medic.dashboard.persistence.model.FaturamentoMesAMes;
import br.com.medic.dashboard.persistence.model.FaturamentoMovimentacaoContratos;
import br.com.medic.dashboard.persistence.model.FaturamentoPlano;
import br.com.medic.dashboard.persistence.model.FaturamentoTipoPagamento;
import br.com.medic.dashboard.persistence.model.MovimentacaoCliente;
import br.com.medic.dashboard.main.model.Report;

@ApplicationScoped
public class MedicLabReportService {

	@Inject
	private Logger logger;

	@Inject
	@Named("ml-report-faturamento-dao")
	private ReportFaturamentoDAO reportFaturamentoDAO;

	@Inject
	@Named("ml-report-cliente-dao")
	private ReportClienteDAO reportClienteDAO;

	@Inject
	@Named("ml-report-plano-dao")
	private ReportPlanoDAO reportPlanoDAO;
	
	@Inject
	@Named("ml-report-agendamento-dao")
	private ReportAgendamentoDAO reportAgendamentoDAO;

	public void setReportFaturamentoDAO(
	        ReportFaturamentoDAO reportFaturamentoDAO) {
		this.reportFaturamentoDAO = reportFaturamentoDAO;
	}

	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoPorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
		
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoTipoPagamento>> getReportFaturamentoPagoPorTipoPagamento(
	        String ano, String mes) {

		String nomeRelatorio = "Faturamento Pago por Tipo de Pagamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoTipoPagamento>> relatorio = new Report<List<FaturamentoTipoPagamento>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoPagoPorTipoPagamento(ano, mes));
		
		relatorio.wrapGraphData();
		
		relatorio.getMetadata().add(reportFaturamentoDAO.getTotalFaturamento(relatorio.getMetadata()));
				
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratos(
	        String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Contratos/Faturamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoMovimentacaoContratos>> relatorio = new Report<List<FaturamentoMovimentacaoContratos>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoMovimentacaoContratos(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoMovimentacaoContratos>> getReportFaturamentoMovimentacaoContratosDetalhado(
	        String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Contratos/Faturamento detalhado";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoMovimentacaoContratos>> relatorio = new Report<List<FaturamentoMovimentacaoContratos>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportFaturamentoDAO
		        .getReportFaturamentoMovimentacaoContratosDetalhado(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public List<FaturamentoMesAMes> getReportFaturamentoMesAMes() {
		logger.info("Gerando relatorio: Faturamento Mes a Mes");
		
		Calendar cal = Calendar.getInstance();		
		cal.add(Calendar.MONTH, -13);		
		cal.set(Calendar.DAY_OF_MONTH, 1);		
		Date start = cal.getTime(); //first day of the year
		Date end = Calendar.getInstance().getTime(); //actual day of the month
//		System.out.println(start);
//		System.out.println(end);
		
		List<FaturamentoMesAMes> relatorio = reportFaturamentoDAO.getReportFaturamentoMesAMes(start, end);
		logger.info("Relatorio gerado: Faturamento Mes a Mes");
		
		return relatorio;
	}

	public Report<List<MovimentacaoCliente>> getReportClientesNovosFaturamento(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Clientes Novos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClientesNovosFaturamento(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportClienteAtivos() {

		String nomeRelatorio = "Clientes Ativos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClienteAtivos());
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportClientesFaturamento(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Clientes Ativos Faturamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClientesFaturamento(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportFaturamentoDespesaAssociadoUnidade(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de faturamento de despesa por unidade - clientes associados";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportFaturamentoDespesaAssociadoUnidade(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<MovimentacaoCliente>> getReportFaturamentoDespesaNAssociadoUnidade(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de faturamento de despesa por unidade - clientes nao associados";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportFaturamentoDespesaNAssociadoUnidade(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	/*
	public Report<List<MovimentacaoCliente>> getReportClienteAtivosENovosFaturamento(String inicio, String fim) {

		String nomeRelatorio = "Movimentação de Clientes Faturamento";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<MovimentacaoCliente>> relatorio = new Report<List<MovimentacaoCliente>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportClienteDAO.getReportClienteAtivosENovosFaturamento(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	*/

	public Report<List<FaturamentoPlano>> getReportFaturamentoPlanos() {

		String nomeRelatorio = "Faturamento por Planos";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoPlano>> relatorio = new Report<List<FaturamentoPlano>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportPlanoDAO.getReportFaturamentoPlanos());
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public Report<List<FaturamentoPlano>> getReportFaturamentoPlanosEmpresarial() {

		String nomeRelatorio = "Faturamento por Planos Empresarial";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<FaturamentoPlano>> relatorio = new Report<List<FaturamentoPlano>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportPlanoDAO.getReportFaturamentoPlanosEmpresarial());
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}
	
	public List<ClientePlano> getReportListaClientesPorPlano(Integer idPlano, Integer qtdAtendimento, Integer qtdAtendimentoFim, 
			Float custoVida, Integer inFormaPagamento, Float ultimoPagamento) {

		logger.info("Gerando relatorio clientes por plano");
		List<ClientePlano> list = reportPlanoDAO.getReportListaClientesPorPlano(idPlano, qtdAtendimento, qtdAtendimentoFim, 
				custoVida, inFormaPagamento, ultimoPagamento);
		logger.info("Relatorio Gerado...");
		return list;
	}
	
	public Report<List<AgendamentoUnidade>> getReportAgendamentoUnidade(String inicio, String fim) {

		String nomeRelatorio = "Agendamentos por unidade";
		logger.info("Gerando relatorio: " + nomeRelatorio);
		Report<List<AgendamentoUnidade>> relatorio = new Report<List<AgendamentoUnidade>>();
		relatorio.setName(nomeRelatorio);
		relatorio.setMetadata(reportAgendamentoDAO.getReportAgendamentoUnidade(inicio, fim));
		logger.debug(relatorio);
		logger.info("Relatorio gerado: " + nomeRelatorio);
		return relatorio;
	}

}
