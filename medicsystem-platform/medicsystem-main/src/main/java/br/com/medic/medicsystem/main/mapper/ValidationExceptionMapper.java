/*
 * This software is confidential and proprietary information of
 * Nokia ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Nokia and the Nokia Institute of Technology.
 */

package br.com.medic.medicsystem.main.mapper;

import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;
import org.jboss.resteasy.api.validation.ResteasyConstraintViolation;
import org.jboss.resteasy.api.validation.ResteasyViolationException;

/**
 * @author Phillip Furtado
 *
 */
@Provider
public class ValidationExceptionMapper implements
        ExceptionMapper<ValidationException> {

	@Inject
	private Logger logger;

	@Override
	public Response toResponse(final ValidationException exception) {

		logger.error(exception.getMessage(), exception);

		if (exception instanceof ResteasyViolationException) {
			ResteasyViolationException ve = (ResteasyViolationException) exception;
			StringBuilder violations = new StringBuilder();
			violations.append("Verifique se os campos [ ");

			for (ResteasyConstraintViolation violation : ve.getViolations()) {
				String violationPath = violation.getPath();
				if (violationPath != null && !violationPath.isEmpty()) {
					String[] path = violationPath.split("\\.");
					violations.append(path[path.length - 1].toUpperCase()
					        + ", ");
				}
			}

			violations.append(" ] foram preenchidos. ");
			violations.append(ErrorMessage.INVALID_PARAMS_STR.getMessage());

			return Response
			        .status(Status.BAD_REQUEST)
			        .type(MediaType.APPLICATION_JSON)
			        .entity(new ExceptionMessage(ErrorType.DANGER,
			                "Bad Request.", violations.toString())).build();
		}

		return Response
		        .status(Status.INTERNAL_SERVER_ERROR)
		        .type(MediaType.APPLICATION_JSON)
		        .entity(new ExceptionMessage(ErrorType.DANGER,
		                "Internal Server Error",
		                ErrorMessage.INTERNAL_ERROR_STR.getMessage())).build();
	}
}