package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.FornecedorService;
import br.com.medic.medicsystem.persistence.model.Fornecedor;

@Path("/fornecedor")
@RequestScoped
public class FornecedorController {

	@Inject
	private FornecedorService fornecedorService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveFornecedor(@Valid Fornecedor fornecedor){
		Fornecedor salvo = fornecedorService.save(fornecedor);
		return Response.created(URI.create("/fornecedor/" + salvo.getId())).build();
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	public Response updateFornecedor(@PathParam("id") Long id, @Valid Fornecedor fornecedor){
		Fornecedor salvo = fornecedorService.save(fornecedor);
		return Response.created(URI.create("/fornecedor/" + salvo.getId())).build();
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Fornecedor getFornecedor(@PathParam("id") Long id){
		Fornecedor fornecedor = fornecedorService.getFornecedorById(id);
		return fornecedor;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Fornecedor> getFornecedores( @QueryParam("inativos") Boolean isInativo ){
		List<Fornecedor> fornecedores = fornecedorService.getAllFornecedores( isInativo );
		return fornecedores;
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteFornecedor(@PathParam("id") Long id){
		fornecedorService.deleteFornecedor(id);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}/reativar")
	public Response reativarFornecedor(@PathParam("id") Long id ){
		fornecedorService.reativarFornecedor(id);
		return Response.ok().build();
	}
}
