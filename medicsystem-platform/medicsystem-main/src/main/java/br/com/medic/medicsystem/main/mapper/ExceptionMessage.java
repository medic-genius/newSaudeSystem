package br.com.medic.medicsystem.main.mapper;

/**
 * 
 * @author Phillip Furtado
 *
 */
public class ExceptionMessage {

	private ErrorType errorType;

	private String message;

	private String causedBy;

	public ExceptionMessage(ErrorType errorType, String message, String causedBy) {
		this.errorType = errorType;
		this.message = message;
		this.causedBy = causedBy;
	}

	public ErrorType getErrorType() {
	    return errorType;
    }
	
	public void setErrorType(ErrorType errorType) {
	    this.errorType = errorType;
    }

	/**
	 * Getter method for causedBy
	 * 
	 * @return the causedBy
	 */
	public String getCausedBy() {
		return causedBy;
	}

	/**
	 * Setter method for causedBy
	 * 
	 * @param causedBy
	 */
	public void setCausedBy(String causedBy) {
		this.causedBy = causedBy;
	}

	/**
	 * Getter method for message
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setter method for message
	 * 
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
