package br.com.medic.medicsystem.main.rest;

import java.text.ParseException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.main.service.ConciliacaoFinanceiroService;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.ArqConciliacaoBancaria;
import br.com.medic.medicsystem.persistence.model.DespesaFinanceiro;

@Path("/conciliacaofinanceiro")
public class ConciliacaoFinanceiroController {
	
	@Inject
	ConciliacaoFinanceiroService conciliacaoFinanceiroService;
	
	@POST
	@Path("/ofxarquivo")
	@Consumes(MediaType.APPLICATION_JSON)
	public List<String> importaArquivosOFX(String input,
			@QueryParam("idEmpresa") Integer idEmpresa, 
			@QueryParam("idContaSelected") Integer idContaSelected) throws ParseException {
		
 		if(idEmpresa != null){
			List<String> result = conciliacaoFinanceiroService.importaArquivosOFX(input, idEmpresa, idContaSelected);
			return result;
		}else {
			return null;
		}

	}
	
	
	@GET
	@Path("/buscainfoextrato") /*alterarnome*/
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArqConciliacaoBancaria> getInfoExtratos(@QueryParam("idEmpresa") Long idEmpresa, @QueryParam("idContaBancaria") Long idContaBancaria){
		if(idEmpresa != null && idContaBancaria != null){
			List<ArqConciliacaoBancaria> result = conciliacaoFinanceiroService.getInfoExtratos(idEmpresa, idContaBancaria);
			return result;
		}else{
			return null;
		}
		
		
	}
	
	@POST
	@Path("/conciliar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> gerarConciliacao(@QueryParam("idTransBancaria") Integer idTransBancaria, @QueryParam("idsDespesa") List<String> idsDespesa, List<DespesaFinanceiro> despesaFinanceiro){
		//parametros pidtransacaobancaria e a lista de iddespesas
		
		List<String> conciliado = conciliacaoFinanceiroService.gerarConciliacao(idTransBancaria, idsDespesa, despesaFinanceiro);

		return conciliado;
	
	}
	
	@PUT
	@Path("/desconciliar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> desfazerConciliacao(@QueryParam("idTransBancaria") Integer idTransBancaria){
		
		List<String> desconciliado = conciliacaoFinanceiroService.desconciliarDados(idTransBancaria);
		return desconciliado;
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sugestao")
	public /*List<SujestaoConciliacaoBancaria> */GenericPaginateDTO getSugestao(@QueryParam("idEmpresa") Integer idEmpresa, @QueryParam("idContaBancaria") Integer idContaBancaria,
			@QueryParam("offset") @DefaultValue("1") int offset,
			@QueryParam("limit") @DefaultValue("10") int limit){
		
		/*List<SujestaoConciliacaoBancaria>*/GenericPaginateDTO listResult = conciliacaoFinanceiroService.getSugestao(idEmpresa, idContaBancaria, offset, limit);
		return listResult;
	}
}
