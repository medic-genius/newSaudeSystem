package br.com.medic.medicsystem.main.appmobile.dto;

import java.io.Serializable;

public class BairroDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idBairro;
	
	private String nmBairro;
	
	public BairroDTO() {
		
	}
	
	public BairroDTO(Long idBairro, String nmBairro) {
		this.idBairro = idBairro;
		this.nmBairro = nmBairro;
	}

	public Long getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Long idBairro) {
		this.idBairro = idBairro;
	}

	public String getNmBairro() {
		return nmBairro;
	}

	public void setNmBairro(String nmBairro) {
		this.nmBairro = nmBairro;
	}
}
