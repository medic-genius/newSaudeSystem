package br.com.medic.medicsystem.main.exception;

import java.io.IOException;

public class PropertiesLoaderException extends RuntimeException {

	public PropertiesLoaderException(String message) {
		super(message);
	}

	public PropertiesLoaderException(String message, IOException e) {
		super(message, e);
	}
	
	public PropertiesLoaderException(String message, NullPointerException e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5550971392760714382L;

}
