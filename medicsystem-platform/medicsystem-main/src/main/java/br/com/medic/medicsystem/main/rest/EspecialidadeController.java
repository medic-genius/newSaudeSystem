package br.com.medic.medicsystem.main.rest;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.medic.medicsystem.main.service.EspecialidadeService;
import br.com.medic.medicsystem.main.service.FuncionarioService;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Profissional;
import br.com.medic.medicsystem.persistence.model.SubEspecialidade;

@Path("/especialidades")
@RequestScoped
public class EspecialidadeController extends BaseController {

	@Inject
	private EspecialidadeService especialidadeService;

	@Inject
	private FuncionarioService funcionarioService;

	@Context
	protected UriInfo info;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Especialidade> getEspecialidades() {

		List<Especialidade> especialidades = especialidadeService
		        .getEspecialidades();
		if (especialidades.isEmpty() || especialidades == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return especialidades;
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/subespecialidades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SubEspecialidade> getSubEspecialidades(@PathParam("id") Long id) {

		List<SubEspecialidade> subEspecialidades = especialidadeService
		        .getSubEspecialidades(id);
		if (subEspecialidades.isEmpty() || subEspecialidades == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return subEspecialidades;
		}
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/profissionais")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Profissional> getProfissionaisPorEspecialidade(
	        @PathParam("id") Long id,
	        @QueryParam("idsub") Long idSubEspecialidade,
	        @QueryParam("idunidade") Long idUnidade) {

		Collection<Profissional> profissionais = funcionarioService
		        .getProfissionaisPorEspecialidade(id, idSubEspecialidade,
		                idUnidade);
		if (profissionais.isEmpty() || profissionais == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return profissionais;
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/profissionaisPorData")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Profissional> getProfissionaisPorEspecialidadePorData(
	        @PathParam("id") Long id,
	        @QueryParam("idsub") Long idSubEspecialidade,
	        @QueryParam("idunidade") Long idUnidade,
	        @QueryParam("data") String data) {

		return funcionarioService
		        .getProfissionaisPorEspecialidadePorData(id, idSubEspecialidade,
		                idUnidade, data);
		
	}
	
	
	@GET
	@Path("/profissionais/triagem")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Profissional> getProfissionaisPorEspecialidadeTriagem(
			@QueryParam("ids") List<Long> ids,
	        @QueryParam("idsub") Long idSubEspecialidade,
	        @QueryParam("idunidade") Long idUnidade) {

		Collection<Profissional> profissionais = funcionarioService
		        .getProfissionaisPorEspecialidadeTriagem(ids, idSubEspecialidade,
		                idUnidade);
		if (profissionais.isEmpty() || profissionais == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return profissionais;
		}
	}
}