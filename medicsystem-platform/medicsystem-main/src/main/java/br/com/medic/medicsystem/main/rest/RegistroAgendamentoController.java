package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.RegistroAgendamentoService;
import br.com.medic.medicsystem.persistence.model.RegistroAgendamento;

@Path("/registroagendamento")
@RequestScoped
public class RegistroAgendamentoController {
	
	@Inject
	RegistroAgendamentoService registroAgendamentoService;
	
	@PUT
	@Path("/registros")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRAFaturamentoLaudo(
			@QueryParam("idagendamento") Long idAgendamento,
			@QueryParam("dtfatlaudo") String dtFatudamentoLaudo) {

		if (idAgendamento != null && dtFatudamentoLaudo != null) {
			
			List<RegistroAgendamento> listRA = registroAgendamentoService.
					updateRAFaturamentoLaudo(idAgendamento, dtFatudamentoLaudo);					

			if (listRA != null) {	
				return Response.ok().build();
			} else {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

}
