/*
 * This software is confidential and proprietary information of
 * Nokia ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Nokia and the Nokia Institute of Technology.
 */

package br.com.medic.medicsystem.main.mapper;

import java.io.EOFException;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.main.exception.GeneralException;
import br.com.medic.medicsystem.persistence.exception.ObjectAlreadyExistsException;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Phillip Furtado
 *
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Exception> {

	@Inject
	private Logger logger;

	@Override
	public Response toResponse(final Exception exception) {

		logger.error(exception.getMessage(), exception);

		final Throwable originalException = exception.getCause();

		if ((exception instanceof ObjectAlreadyExistsException)
				|| (originalException instanceof ObjectAlreadyExistsException)) {
			return Response
					.status(Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.entity(new ExceptionMessage(ErrorType.DANGER, "The object already exists", exception
							.getMessage())).build();
		}

		if ((exception instanceof IllegalArgumentException) || (originalException instanceof IllegalArgumentException)) {
			
			IllegalArgumentException gException = (IllegalArgumentException)exception;
			
			if(gException.getMessage() != null) {
				return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
						.entity(new ExceptionMessage(ErrorType.DANGER, "Bad Request.", ErrorMessage.INVALID_PARAMS_STR_WITHDATA.getMessage() + gException.getMessage())).build();
			}
			
			return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
					.entity(new ExceptionMessage(ErrorType.DANGER, "Bad Request.", ErrorMessage.INVALID_PARAMS_STR.getMessage())).build();
		}

		if (exception instanceof EOFException) {
			return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
					.entity(new ExceptionMessage(ErrorType.DANGER, "Bad Request.", ErrorMessage.INVALID_PARAMS_STR.getMessage())).build();
		}

		if (exception instanceof JsonProcessingException) {
			return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
					.entity(new ExceptionMessage(ErrorType.DANGER, "Bad Request.", ErrorMessage.INVALID_PARAMS_STR.getMessage())).build();
		}
		
		if (originalException == null && exception instanceof GeneralException) {
			
			GeneralException gException = (GeneralException)exception;
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON)
					.entity(new ExceptionMessage(gException.getErrorType(), "Exception", exception.getMessage()))
					.build();
		}

		return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON)
				.entity(new ExceptionMessage(ErrorType.DANGER, "Internal Server Error", ErrorMessage.INTERNAL_ERROR_STR.getMessage()))
				.build();
	}
}