package br.com.medic.medicsystem.main.rest;

import java.net.URI;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.ObservacaoClienteService;
import br.com.medic.medicsystem.persistence.model.ObservacaoCliente;

@Path("/observacoes")
@RequestScoped
public class ObservacaoClienteController extends BaseController {

	@Inject
	private ObservacaoClienteService observacaoClienteService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createObservacao(@Valid ObservacaoCliente obs) {
		ObservacaoCliente salvo = observacaoClienteService
		        .saveObservacaoCliente(obs);
		return Response.created(URI.create("/observacoes/" + salvo.getId()))
		        .build();
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeObservacao(@PathParam("id") Long id) {

		if (id != null) {
			observacaoClienteService.removeObservacao(id);

		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

		return Response.ok().build();

	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ObservacaoCliente getObservacao(@PathParam("id") Long id) {

		if (id != null) {
			return observacaoClienteService.getObservacao(id);

		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

}