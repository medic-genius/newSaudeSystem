package br.com.medic.medicsystem.main.vindi.data.parameters.create;

import java.util.List;
import java.util.Map;

import br.com.medic.medicsystem.main.vindi.data.customer.Address;
import br.com.medic.medicsystem.main.vindi.data.parameters.create.Phone;

public class Customer {
	private String name;
	private String email;
	private String registry_code;
	private String code;
	private String notes;
	private Map<String, String> metadata;
	private Address address;
	private List<Phone> phones;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRegistry_code() {
		return registry_code;
	}
	public void setRegistry_code(String registry_code) {
		this.registry_code = registry_code;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
}
