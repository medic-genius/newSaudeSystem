package br.com.medic.medicsystem.main.vindi.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.client.methods.HttpRequestBase;

import br.com.medic.medicsystem.sms.exception.ClientHumanException;

public abstract class BaseHttpService {
	protected static final String GATEWAY_HOST = "app.vindi.com.br";
	protected static final int GATEWAY_PORT = 80;
	protected static final String AUTHENTICATION_TYPE = "Basic";
	protected static final String AUTHENTICATION_KEY = "QnhTY0QxdzYtMDgzSEYwT3JwZ2hMWkFBT1U0elZXTFM6";
	
	private HttpClient http;
	
	public BaseHttpService() {
		this(new HttpClient());
	}

	public BaseHttpService(HttpClient httpClient) {
		this.http = httpClient;
		this.http.setHost(GATEWAY_HOST);
		this.http.setPort(GATEWAY_PORT);
	}

	public void setUseSSL(boolean flag) {
		this.http.setUsingSSL(flag);
	}
	
	public boolean isUsingSSL() {
		return this.http.isUsingSSL();
	}
	
	public void setPjbank(String host){
		new HttpClient();
		http.setHost(host);
		http.setUsingSSL(true);		
	}
	
	protected String sendRequest(String url, HttpRequestBase request) throws Exception {
		InputStream is = this.http.request(url, request);
		BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
		StringBuffer res = new StringBuffer();
		String line = null;
		try {
			while((line = buffer.readLine()) != null) {
				res.append(line);
			}
		} catch (IOException e) {
			throw new ClientHumanException();
		}
		return res.toString();
	}
}
