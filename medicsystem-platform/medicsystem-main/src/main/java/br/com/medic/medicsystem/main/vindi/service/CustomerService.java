package br.com.medic.medicsystem.main.vindi.service;

import java.util.List;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.medic.medicsystem.main.vindi.data.customer.Customer;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.response.ResponseParser;
import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;

public class CustomerService extends BaseHttpService{
	private final String BASE_URL = "/api/v1/customers";
	private ResponseParser<Customer> parser = new ResponseParser<Customer>(Customer.class);
	
	public CustomerService() {
		super();
		setUseSSL(true);
	}
	
	public List<Customer> getCustomers() {
		String url = BASE_URL;
		HttpGet get = new HttpGet();
		get.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {
			String response = sendRequest(url, get);
			return parser.parseResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Customer createCustomer(
			br.com.medic.medicsystem.main.vindi.data.parameters.create.Customer customer) throws VindiException {
		String url = BASE_URL;
		HttpPost post = new HttpPost();
		post.addHeader("Content-Type", "application/json");
		post.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		try {
			jsonStr = mapper.writeValueAsString(customer);
			post.setEntity(new StringEntity(jsonStr));
			
			String response = sendRequest(url, post);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Customer deleteCustomer(Integer customerId) throws VindiException {
		String url = BASE_URL + "/" + customerId;
		HttpDelete delete = new HttpDelete();
		delete.addHeader("Content-Type", "application/json");
		delete.setHeader("Authorization", "Basic " + AUTHENTICATION_KEY);
		try {			
			String response = sendRequest(url, delete);
			return parser.parseSingleResponse(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
