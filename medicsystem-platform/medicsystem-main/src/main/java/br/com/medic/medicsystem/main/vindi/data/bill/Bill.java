package br.com.medic.medicsystem.main.vindi.data.bill;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.medic.medicsystem.main.vindi.data.subscription.BillCharge;
import br.com.medic.medicsystem.main.vindi.data.summary.Customer;
import br.com.medic.medicsystem.main.vindi.data.summary.PaymentProfile;
import br.com.medic.medicsystem.main.vindi.data.summary.Period;
import br.com.medic.medicsystem.main.vindi.data.summary.Subscription;

@JsonIgnoreProperties({"payment_condition"})
public class Bill {
	private Integer id;
	
	private String code;
	
	private Number amount;
	
	private Integer installments;
	
	private String status;
	
	private String seen_at;
	
	private String billing_at;
	
	private String due_at;
	
	private String url;
	
	private String created_at;
	
	private String updated_at;
	
	private List<BillItem> bill_items;
	
	private List<BillCharge> charges;
	
	private Customer customer;
	
	private Period period;
	
	private Subscription subscription;
	
	private Map<String, String> metadata;
	
	private PaymentProfile payment_profile;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Number getAmount() {
		return amount;
	}

	public void setAmount(Number amount) {
		this.amount = amount;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeen_at() {
		return seen_at;
	}

	public void setSeen_at(String seen_at) {
		this.seen_at = seen_at;
	}

	public String getBilling_at() {
		return billing_at;
	}

	public void setBilling_at(String billing_at) {
		this.billing_at = billing_at;
	}

	public String getDue_at() {
		return due_at;
	}

	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public List<BillItem> getBill_items() {
		return bill_items;
	}

	public void setBill_items(List<BillItem> bill_items) {
		this.bill_items = bill_items;
	}

	public List<BillCharge> getCharges() {
		return charges;
	}

	public void setCharges(List<BillCharge> charges) {
		this.charges = charges;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}

	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
