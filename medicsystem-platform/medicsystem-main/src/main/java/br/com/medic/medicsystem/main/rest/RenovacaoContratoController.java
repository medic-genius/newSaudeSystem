package br.com.medic.medicsystem.main.rest;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.service.RenovacaoContratoService;
import br.com.medic.medicsystem.persistence.dto.BoletosImprimirDTO;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.security.dto.LoginDTO;

@Path("/renovacao")
@RequestScoped
public class RenovacaoContratoController {
	
	@Inject
	private RenovacaoContratoService renovacaoContratoService;
	
	@POST
	@Path("/job")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getRenovacaoAutomatica(@Valid LoginDTO loginDTO,
			@QueryParam("idEmpresaGrupo") Long idEmpresaGrupo) {
		
		Integer qtdMensalidadeRenovacao = null;
		
		if(idEmpresaGrupo != null){
			
			qtdMensalidadeRenovacao = renovacaoContratoService.renovacaoAutomaticaJob(idEmpresaGrupo, loginDTO);
		} else {
			
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		ResponseBuilder response = Response.ok(qtdMensalidadeRenovacao);

		return response.build();
	}
	
	@GET
	@Path("/contratosrenovados")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Contrato> getContratosRenovados(
			@QueryParam("idEmpresaGrupo") Long idEmpresaGrupo,
			@QueryParam("dtInicio") String dtInicio,
			@QueryParam("dtFinal") String dtFinal) {

		if (idEmpresaGrupo != null) {

			return renovacaoContratoService.getContratosRenovados(idEmpresaGrupo, dtInicio, dtFinal);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/mensalidades/contrato")	
	@Produces(MediaType.APPLICATION_JSON)
	public List<BoletosImprimirDTO> getAllMensalidadesBoletos(
			@QueryParam("idContrato") Long idContrato, 
	        @QueryParam("idEmpresaGrupo") Long idEmpresaGrupo) {

		if (idContrato != null 
				&&idEmpresaGrupo != null) {

			return renovacaoContratoService.getMensalidadeBoletoImprimir(idContrato, idEmpresaGrupo);
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/contrato/boletos/imprimir")
	@Produces("application/pdf")
	public Response getDespesaPDF(@QueryParam("idCliente") Long idCliente,
			@QueryParam("idContrato") Long idContrato,
			@QueryParam("idsMensalidade[]") Long idsMensalidade[]) {

		if (idCliente != null 
				&& idContrato != null
				&& idsMensalidade.length > 0) {
			
			byte[] pdf = renovacaoContratoService
					.imprimirBoletosBancariosRenovacao(idCliente, idContrato,
							idsMensalidade);

			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();

		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}
	
	//add 19/04/2018
	@GET
	@Path("/renovacaocontrato/imprimir/boletos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBoletosRenovacao(@QueryParam("idCliente") Long idCliente, @QueryParam("idContrato") Long idContrato){
		if(idCliente != null && idContrato != null){
			String linkBoleto = renovacaoContratoService.getBoletosRenovacao(idCliente, idContrato);
			JSONObject json = new JSONObject();
			json.put("link", linkBoleto);
			return Response.status(200).type(MediaType.TEXT_PLAIN).entity(json).build();
		}else{
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}

}
