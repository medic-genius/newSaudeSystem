package br.com.medic.medicsystem.main.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.main.vindi.data.bill.Bill;
import br.com.medic.medicsystem.main.vindi.data.customer.Customer;
import br.com.medic.medicsystem.main.vindi.data.parameters.PricingSchema;
import br.com.medic.medicsystem.main.vindi.data.parameters.ProductItem;
import br.com.medic.medicsystem.main.vindi.data.parameters.create.BillItem;
import br.com.medic.medicsystem.main.vindi.data.payment.PaymentProfile;
import br.com.medic.medicsystem.main.vindi.data.product.Product;
import br.com.medic.medicsystem.main.vindi.data.subscription.BillCharge;
import br.com.medic.medicsystem.main.vindi.data.subscription.SubscriptionResponse;
import br.com.medic.medicsystem.main.vindi.exception.VindiException;
import br.com.medic.medicsystem.main.vindi.service.BillService;
import br.com.medic.medicsystem.main.vindi.service.CustomerService;
import br.com.medic.medicsystem.main.vindi.service.PaymentProfileService;
import br.com.medic.medicsystem.main.vindi.service.ProductService;
import br.com.medic.medicsystem.main.vindi.service.SubscriptionService;
import br.com.medic.medicsystem.persistence.dao.ContratoDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.LogVendedorDAO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteDataDTO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.PagamentoAgendamentoDTO;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.LogVendedor;
import br.com.medic.medicsystem.persistence.model.views.ViewRenovacaoContrato;
import br.com.medic.medicsystem.persistence.security.SecurityService;

import com.fasterxml.jackson.databind.ObjectMapper;


@Stateless
public class VendasService {
	
	@Inject
	@Named("log-vendedor-dao")
	private LogVendedorDAO logVendedorDao;
	
	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;

	@Inject
	private SecurityService securityService;
	
	@Inject
	@Named("contrato-dao")
	private ContratoDAO contratoDAO;

	public LogVendedor insertIntoLog(LogVendedor logObj) {
		return logVendedorDao.persistLogVendedor(logObj);
	}

	public LogVendedor checkClienteCard(String cardNumber) {
		Integer idEmpresa = 9992;
		return logVendedorDao.checkCliente(cardNumber, idEmpresa);
	}

	public AssinaturaRecorrenteResultDTO criarAssinaturaRecorrente(AssinaturaRecorrenteDataDTO data) {
		// funcionario que está realizando a venda
		Funcionario currentFuncionario = securityService.getFuncionarioLogado();
		// codigo da empresa (no log de vendas)
		Integer empresaCode = getEmpresaIdCode(data.getIdEmpresaGrupo());
		// vindi_customer_id
		Integer customerId = 0;
		// vindi_payment_profile_id
		Integer paymentProfileId = 0;
		
		if(data.getCustomerId() == null) {
			Customer customer = null;
			try {
				customer = createVindiCustomer(data.getCartaoNomePortador());
			} catch(VindiException e) {
				return getResultObject("error", e.getMessage(), null, null);
			}
			if(customer != null && (customer.getId() != 0 || customer.getId() != null)) {
				String customerStr;
				try {
					customerStr = (new ObjectMapper()).writeValueAsString(customer);
				} catch(Exception e) {
					customerStr = "";
				}

				// persist on log
				persistLog(currentFuncionario.getId().intValue(), empresaCode, 
						data.getPlanoIdRecorrencia(), "", "", "", "", customerStr, 
						"customer", customer.getId(), 0, 0, 0, data.getQuantidade(),
						"", "");
				
				// cria pagamento
				PaymentProfile payment;
				try {
					payment = createPaymentProfile(data.getCartaoNomePortador(), 
							data.getCartaoValidade(), data.getCartaoNumero(), 
							data.getCartaoCvv(), customer.getId(), data.getCartaoIsElo());
				} catch(VindiException e) {
					return getResultObject("error", e.getMessage(), null, customer.getId());
				}

				if(payment != null && (payment.getId() != null || payment.getId() != 0)) {
					String paymentStr;
					try {
						paymentStr = (new ObjectMapper()).writeValueAsString(customer);
					} catch(Exception e) {
						paymentStr = "";
					}
					// persist on log
					persistLog(currentFuncionario.getId().intValue(), empresaCode, 
							data.getPlanoIdRecorrencia(), data.getCartaoNomePortador(),
							data.getCartaoNumero(), "", data.getCartaoValidade(),
							paymentStr, "payment", customer.getId(), payment.getId(), 0, 0, 
							data.getQuantidade(), "", "");

					customerId = customer.getId();
					paymentProfileId = payment.getId();
				} else {
					// ERRO AO CRIAR PAYMENT_PROFILE
					return getResultObject("fail", "Não foi possível criar pagamento da assinatura", 
							null, customer.getId());
				}
			} else {
				// ERRO AO CRIAR CUSTOMER
				return getResultObject("fail", "Não foi possível criar cliente da assinatura", 
						null, null);
			}
		} else {
			customerId = data.getCustomerId();
		}

		Product product;
		try {
			String productName = "sistema_vendedor_" + customerId;
			product = createProduct(productName, customerId, data.getValor());
		} catch(VindiException e) {
			return getResultObject("error", e.getMessage(), null, 
					data.getCustomerId() == null ? customerId : null);
		}
		if(product != null && (product.getId() != null || product.getId() != 0)) {
			String productStr;
			try {
				productStr = (new ObjectMapper()).writeValueAsString(product);
			} catch(Exception e) {
				productStr = "";
			}
			persistLog(currentFuncionario.getId().intValue(), empresaCode, 
					data.getPlanoIdRecorrencia(), data.getCartaoNomePortador(), 
					data.getCartaoNumero(), "", data.getCartaoValidade(),
					productStr, "product", customerId, paymentProfileId, product.getId(),
					0, data.getQuantidade(), "", "");

			BigDecimal q = new BigDecimal(data.getQuantidade());

			SubscriptionResponse subscriptionResp;
			try {
				Integer diaVencimento = new Integer(data.getDiaVencimentoInicial());
				subscriptionResp = createSubscription(data.getPlanoIdRecorrencia(),
						customerId, diaVencimento, product.getId(),
						data.getValor().multiply(q).floatValue());
			} catch(VindiException e) {
				return getResultObject("error", e.getMessage(), null, 
						data.getCustomerId() == null ? customerId : null);
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			if(cal.get(Calendar.DAY_OF_MONTH) != Integer.parseInt(data.getDiaVencimentoInicial())) {
				if(subscriptionResp.getSubscription() != null && subscriptionResp.getSubscription().getId() != null) {
					AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
					result.setStatus("success");
					result.setMessage("Assinatura criada");
					result.setSubscriptionId(subscriptionResp.getSubscription().getId());
					return result;
				}
			}
			if(subscriptionResp.getBill() != null && subscriptionResp.getBill().getCharges() != null && 
					subscriptionResp.getBill().getCharges().get(0).getLast_transaction() != null &&
					subscriptionResp.getBill().getCharges().get(0).getLast_transaction().getId() != null) {
				String subscriptStr;
				try {
					subscriptStr = (new ObjectMapper()).writeValueAsString(subscriptionResp.getSubscription());
				} catch(Exception e) {
					subscriptStr = "";
				}
				BillCharge charge = subscriptionResp.getBill().getCharges().get(0);

				persistLog(currentFuncionario.getId().intValue(), empresaCode, 
						data.getPlanoIdRecorrencia(), data.getCartaoNomePortador(), 
						data.getCartaoNumero(), "", data.getCartaoValidade(), 
						subscriptStr, "subscription", customerId, paymentProfileId, product.getId(), 
						subscriptionResp.getSubscription().getId(), data.getQuantidade(),
						charge.getLast_transaction().getGateway_response_code(),
						charge.getLast_transaction().getGateway_message());

				String gResponseCode = charge.getLast_transaction().getGateway_response_code();
				String message = "";
				if(gResponseCode != null) {
					if(gResponseCode.equals("01") || gResponseCode.equals("04") || 
							gResponseCode.equals("05") || gResponseCode.equals("41") || 
							gResponseCode.equals("62") || gResponseCode.equals("63")) {
						message = "Oriente o cliente a contatar o emissor do cartão";
					} else if(gResponseCode.equals("13")) {
						message = "Verifique valor mínimo de R$5,00 para parcelamento";
					} else if(gResponseCode.equals("GA")) {
						message = "Aguarde contato da Cielo";
					} else {
						message = charge.getLast_transaction().getGateway_message();
					}
				}

				AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
				if(charge.getLast_transaction().getGateway_message().toLowerCase().equals("transacao autorizada") 
						|| charge.getLast_transaction().getGateway_message().toLowerCase().equals("transacao capturada com sucesso") ) {
					result.setStatus("success");
					result.setMessage(message);
					result.setSubscriptionId(subscriptionResp.getSubscription().getId());
				} else {
					result.setStatus("fail");
					result.setMessage("Não foi possível criar assinatura");
				}
				return result;
			} else {
				// ERRO AO CRIAR SUBSCRIPTION
				return getResultObject("fail", "Não foi possível criar assinatura", null, 
						data.getCustomerId() == null ? customerId : null);
			}
		} else {
			// ERRO AO CRIAR PRODUCT
			return getResultObject("fail", "Não foi possível criar produto assinatura", null, 
					data.getCustomerId() == null ? customerId : null);
		}
	}
	
	public AssinaturaRecorrenteResultDTO renovarAssinaturaRecorrenteContrato(Integer vindiCustomerId, 
			Integer idRecorrenciaPlano, ViewRenovacaoContrato renovacao) {
		
		// valor do produto (plano)
		BigDecimal valor = new BigDecimal(renovacao.getVlPlano());
		// data de vencimento inicial
		Calendar calendarVencimento = Calendar.getInstance();
		calendarVencimento.setTime(renovacao.getDtTerminoContrato());
		calendarVencimento.add(Calendar.MONTH, 1);
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
		Integer diaCobranca = calendarVencimento.get(Calendar.DAY_OF_MONTH);
		String dtInicioCobranca = formatter.format(calendarVencimento.getTime());
		
		Product product;
		try {
			String productName = "Ren_Automatica_" + renovacao.getNrContrato();
			product = createProduct(productName, vindiCustomerId, valor);
		} catch (VindiException e) {
			return getResultObject("error", e.getMessage(), null);
		}
		if (product != null
				&& (product.getId() != null || product.getId() != 0)) {
			
			BigDecimal q = new BigDecimal(renovacao.getQtdCliente()
					+ renovacao.getQtdDependente());

			SubscriptionResponse subscriptionResp;
			try {
				subscriptionResp = createSubscription(idRecorrenciaPlano,
						vindiCustomerId, diaCobranca, product.getId(),
						valor.multiply(q).floatValue(), dtInicioCobranca);
			} catch(VindiException e) {
				return getResultObject("error", e.getMessage(), null);
			}
			
			
			if(subscriptionResp.getSubscription() != null && subscriptionResp.getSubscription().getId() != null) {
				AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
				result.setStatus("success");
				result.setMessage("Assinatura criada");
				result.setSubscriptionId(subscriptionResp.getSubscription().getId());
				return result;
			}  else {
				// ERRO AO CRIAR SUBSCRIPTION
				return getResultObject("fail", "Não foi possível criar assinatura", null);
			}
		} else {
			// ERRO AO CRIAR PRODUCT
			return getResultObject("fail", "Não foi possível criar produto assinatura", null);
		}
	}
	
	private AssinaturaRecorrenteResultDTO getResultObject(String status, String message, 
			Integer subscriptionId) {
		return this.getResultObject(status, message, subscriptionId, null);
	}
	
	/**
	 * Retorna objeto de resultado da assinatura recorrente, excluindo cliente vindi (customer)
	 * de acordo com a necessidade
	 */
	private AssinaturaRecorrenteResultDTO getResultObject(String status, String message, 
			Integer subscriptionId, Integer customerId) {
		if(customerId != null) {
			try {
				(new CustomerService()).deleteCustomer(customerId);
			} catch (VindiException e1) {
			}
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus(status);
		result.setMessage(message);
		result.setSubscriptionId(subscriptionId);
		return result;
	}
	
	/**
	 * Retorna o codigo da empresa usada na tabela de log
	 * @param empresaId id de EmpresaGrupo que será mapeado
	 * @return Inteiro com o codigo da empresa
	 */
	private Integer getEmpresaIdCode(Long empresaId) {
		if(empresaId.equals(9L)) {
			return 9992;
		}
		return null;
	}
	
	/**
	 * Cria um cliente na plataforma Vindi
	 * @param customerName nome do novo cliente
	 * @return Objeto do novo cliente (Customer).
	 * @throws VindiException Exceção em caso de erro
	 */
	private Customer createVindiCustomer(
			String customerName) throws VindiException {
		br.com.medic.medicsystem.main.vindi.data.parameters.create.Customer newCustomer;
		newCustomer = new br.com.medic.medicsystem.main.vindi.data.parameters.create.Customer();
		newCustomer.setName(customerName);
		CustomerService serv = new CustomerService();
		return serv.createCustomer(newCustomer);
	}
	
	private PaymentProfile createPaymentProfile(String holderName, String expiration,
			String number, String cvv, Integer customerId, Boolean isElo) throws VindiException {
		br.com.medic.medicsystem.main.vindi.data.parameters.create.PaymentProfile payment;
		payment = new br.com.medic.medicsystem.main.vindi.data.parameters.create.PaymentProfile();
		payment.setHolder_name(holderName);
		payment.setCard_expiration(expiration);
		payment.setCard_number(number);
		payment.setCard_cvv(cvv);
		payment.setCustomer_id(customerId);
		payment.setPayment_method_code("credit_card");
		if(isElo != null && isElo.booleanValue()) {
			payment.setPayment_company_code("elo");
		}
		PaymentProfileService serv = new PaymentProfileService();
		return serv.createPaymentProfile(payment);
	}

	private Product createProduct(String productName, Integer customerId, BigDecimal vlPlano) throws VindiException {
		br.com.medic.medicsystem.main.vindi.data.parameters.create.Product product;
		product = new br.com.medic.medicsystem.main.vindi.data.parameters.create.Product();
		product.setName(productName);
		product.setStatus("active");
		PricingSchema pricingSchema = new PricingSchema();
		pricingSchema.setPrice(vlPlano);
		pricingSchema.setSchema_type("flat");
		product.setPricing_schema(pricingSchema);

		ProductService serv = new ProductService();
		return serv.createProduct(product);
	}
	
	private SubscriptionResponse createSubscription(Integer idPlano, Integer customerId,
			Integer diaVencimentoInicial, Integer productId, Number valor) throws VindiException {
		return this.createSubscription(idPlano, customerId, diaVencimentoInicial, productId, valor, null);
	}

	private SubscriptionResponse createSubscription(Integer idPlano,
			Integer customerId, Integer diaVencimentoInicial, Integer productId,
			Number valor, String dataCobrancaInicial) throws VindiException {
		PricingSchema schema = new PricingSchema();
		schema.setPrice(valor);
		schema.setSchema_type("flat");

		ProductItem item = new ProductItem();
		item.setProduct_id(productId);
		item.setPricing_schema(schema);

		List<ProductItem> listItems = new ArrayList<ProductItem>();
		listItems.add(item);

		br.com.medic.medicsystem.main.vindi.data.parameters.create.Subscription subscription;
		subscription = new br.com.medic.medicsystem.main.vindi.data.parameters.create.Subscription();
		subscription.setPlan_id(idPlano);
		subscription.setCustomer_id(customerId);
		subscription.setPayment_method_code("credit_card");
		subscription.setBilling_trigger_day(diaVencimentoInicial);
		subscription.setProduct_items(listItems);
		if(dataCobrancaInicial != null) {
			subscription.setStart_at(dataCobrancaInicial);
		}

		SubscriptionService serv = new SubscriptionService();
		return serv.createSubscription(subscription);
	}
	
	private Bill createBill(Integer customerId, 
			Integer productId, Number valor) throws VindiException {
		PricingSchema schema = new PricingSchema();
		schema.setPrice(valor);
		schema.setSchema_type("flat");
		
		BillItem billItem = new BillItem();
		billItem.setProduct_id(productId);
		billItem.setAmount(valor);
		billItem.setQuantity(1);
		billItem.setPricing_schema(schema);
		
		List<BillItem> billItems = new ArrayList<BillItem>();
		billItems.add(billItem);
		
		br.com.medic.medicsystem.main.vindi.data.parameters.create.Bill bill = new br.com.medic.medicsystem.main.vindi.data.parameters.create.Bill();
		bill.setCustomer_id(customerId);
		bill.setInstallments(1);
		bill.setPayment_method_code("credit_card");
		bill.setBill_items(billItems);
		
		BillService billService = new BillService();
		return billService.createBill(bill);
	}

	private void persistLog(Integer idFuncionario, Integer empresaCode, Integer idPlano,
			String cartaoNomeCliente, String cartaoNumero, String cartaoCvv,
			String cartaoVencimento, String jsonStr, String type, Integer customerId,
			Integer paymentProfileId, Integer productId, Integer subscriptionId,
			Integer quantidade, String gatewayResponseCode, String gatewayResponseMessage) {
		LogVendedor log = new LogVendedor();
		log.setIdVendedor(idFuncionario);
		log.setCpfConsulta("");
		log.setIdEmpresa(empresaCode);
		log.setIdPlano(idPlano);
		log.setCartaoNomeCliente(cartaoNomeCliente);
		log.setCartaoNumero(cartaoNumero);
		log.setCartaoCvv(cartaoCvv);
		log.setCartaoVencimento(cartaoVencimento);
		log.setJsonStr(jsonStr);
		log.setType(type);
		log.setVindiCustomerId(customerId);
		log.setVindiPaymentProfileId(paymentProfileId);
		log.setVindiProductId(productId);
		log.setVindiSubscriptionId(subscriptionId);
		log.setVindiGatewayResponseCode(gatewayResponseCode);
		log.setVindiGatewayMessage(gatewayResponseMessage);
		log.setQuantidade(quantidade);
		log.setLatitude(null);
		log.setLongitude(null);
		log.setMovement("");
		log.setSuccess(1);
		log.setDataLog(new Date());
		logVendedorDao.persistLogVendedor(log);
	}
	
	public AssinaturaRecorrenteResultDTO pagamentoAgendamentoCartao(PagamentoAgendamentoDTO data, String source) {
		// funcionario que está realizando a venda
		Funcionario currentFuncionario = securityService.getFuncionarioLogado();
		if(currentFuncionario == null) {
			if(source != null && source.equals("mobile_app")) {
				currentFuncionario = funcionarioDAO.getFuncionario(4466L);
			} else {
				currentFuncionario = funcionarioDAO.getFuncionario(4467L);
			}
		}
		// codigo da empresa (no log de vendas)
		Integer empresaCode = 9992;
		// vindi_customer_id
		Integer customerId = 0;
		// vindi_payment_profile_id
		Integer paymentProfileId = 0;
		
		LogVendedor log = this.checkClienteCard(data.getCartaoNumero());
		if(log != null && log.getVindiSubscriptionId() != null) {
			customerId = log.getVindiCustomerId();
			paymentProfileId = log.getVindiPaymentProfileId();
		} else {
			Customer customer = null;
			try {
				customer = createVindiCustomer(data.getCartaoNomePortador());
			} catch(VindiException e) {
				return getResultObject("error", e.getMessage(), null, null);
			}
			if(customer != null && (customer.getId() != 0 || customer.getId() != null)) {
				String customerStr;
				try {
					customerStr = (new ObjectMapper()).writeValueAsString(customer);
				} catch(Exception e) {
					customerStr = "";
				}

				// persist on log
				persistLog(currentFuncionario.getId().intValue(), empresaCode, 
						null, "", "", "", "", customerStr, 
						"customer", customer.getId(), 0, 0, 0, 1,
						"", "");

				// cria pagamento
				PaymentProfile payment;
				try {
					payment = createPaymentProfile(data.getCartaoNomePortador(), 
							data.getCartaoValidade(), data.getCartaoNumero(), 
							data.getCartaoCvv(), customer.getId(), data.getCartaoIsElo());
				} catch(VindiException e) {
					return getResultObject("error", e.getMessage(), null, customer.getId());
				}

				if(payment != null && (payment.getId() != null || payment.getId() != 0)) {
					String paymentStr;
					try {
						paymentStr = (new ObjectMapper()).writeValueAsString(customer);
					} catch(Exception e) {
						paymentStr = "";
					}
					// persist on log
					persistLog(currentFuncionario.getId().intValue(), empresaCode, 
							null, data.getCartaoNomePortador(),
							data.getCartaoNumero(), "", data.getCartaoValidade(),
							paymentStr, "payment", customer.getId(), payment.getId(), 0, 0, 
							1, "", "");

					customerId = customer.getId();
					paymentProfileId = payment.getId();
				} else {
					// ERRO AO CRIAR PAYMENT_PROFILE
					return getResultObject("fail", "Erro ao criar pagamento da assinatura", 
							null, customer.getId());
				}
			} else {
				// ERRO AO CRIAR CUSTOMER
				return getResultObject("fail", "Erro ao criar cliente da assinatura", 
						null, null);
			}
		}

		Product product;
		try {
			String prodName = "pag_agend_" + (source != null ? source : "site") + "_" + customerId;
			product = createProduct(prodName, customerId, data.getValor());
		} catch(VindiException e) {
			return getResultObject("error", e.getMessage(), null, 
					data.getCustomerId() == null ? customerId : null);
		}
		if(product != null && (product.getId() != null || product.getId() != 0)) {
			String productStr;
			try {
				productStr = (new ObjectMapper()).writeValueAsString(product);
			} catch(Exception e) {
				productStr = "";
			}
			persistLog(currentFuncionario.getId().intValue(), empresaCode, 
					null, data.getCartaoNomePortador(), 
					data.getCartaoNumero(), "", data.getCartaoValidade(),
					productStr, "product", customerId, paymentProfileId, product.getId(),
					0, 1, "", "");
			
			br.com.medic.medicsystem.main.vindi.data.bill.Bill bill = null;
			try {
				bill = createBill(customerId, product.getId(), data.getValor());
			} catch(VindiException e) {
				return getResultObject("error", e.getMessage(), null, 
						data.getCustomerId() == null ? customerId : null);
			}

			if(bill != null && bill.getCharges() != null && 
					bill.getCharges().get(0).getLast_transaction() != null &&
					bill.getCharges().get(0).getLast_transaction().getId() != null) {
				String billStr;
				try {
					billStr = (new ObjectMapper()).writeValueAsString(bill);
				} catch(Exception e) {
					billStr = "";
				}
				BillCharge charge = bill.getCharges().get(0);

				persistLog(currentFuncionario.getId().intValue(), empresaCode, 
						null, data.getCartaoNomePortador(), 
						data.getCartaoNumero(), "", data.getCartaoValidade(), 
						billStr, "bill", customerId, paymentProfileId, product.getId(), 
						bill.getId(), 1, charge.getLast_transaction().getGateway_response_code(),
						charge.getLast_transaction().getGateway_message());

				String gResponseCode = charge.getLast_transaction().getGateway_response_code();
				String message = null;
				if(gResponseCode != null) {
					if(gResponseCode.equals("01") || gResponseCode.equals("04") || 
							gResponseCode.equals("05") || gResponseCode.equals("41") || 
							gResponseCode.equals("62") || gResponseCode.equals("63")) {
						message = "Contate o emissor do cartão";
					} else if(gResponseCode.equals("13")) {
						message = "Verifique valor mínimo de R$5,00 para parcelamento";
					} else if(gResponseCode.equals("GA")) {
						message = "Aguarde contato da Cielo";
					} else {
						message = charge.getLast_transaction().getGateway_message();
					}
				}

				AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
				if(charge.getLast_transaction().getGateway_message().toLowerCase().equals("transacao autorizada") 
						|| charge.getLast_transaction().getGateway_message().toLowerCase().equals("transacao capturada com sucesso") ) {
					result.setStatus("success");
					result.setMessage(message);
					result.setSubscriptionId(null);
				} else {
					result.setStatus("fail");
					if(message != null) {
						result.setMessage(message);
					} else {
						result.setMessage("Não foi possível criar assinatura");
					}
				}
				return result;
			} else {
				// ERRO AO CRIAR SUBSCRIPTION
				return getResultObject("fail", "Erro ao criar assinatura", null, 
						data.getCustomerId() == null ? customerId : null);
			}
		} else {
			// ERRO AO CRIAR PRODUCT
			return getResultObject("fail", "Erro ao criar assinatura", null, 
					data.getCustomerId() == null ? customerId : null);
		}
	}
	
}
