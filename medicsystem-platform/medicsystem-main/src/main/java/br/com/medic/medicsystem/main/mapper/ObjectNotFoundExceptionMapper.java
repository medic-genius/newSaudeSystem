package br.com.medic.medicsystem.main.mapper;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;

@Provider
public class ObjectNotFoundExceptionMapper implements
        ExceptionMapper<ObjectNotFoundException> {

	@Inject
	private Logger logger;

	@Override
	public Response toResponse(final ObjectNotFoundException exception) {

		//logger.warn(exception);

		return Response.status(Status.NO_CONTENT).build();
	}

}
