package br.com.medic.medicsystem.main.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.CalendarioService;
import br.com.medic.medicsystem.persistence.dto.CalAgendaMedicoDTO;
import br.com.medic.medicsystem.persistence.dto.CalEspecialidadeDTO;



@Path("/calendario")
@RequestScoped
public class CalendarioController extends BaseController{
	
	@Inject
	private CalendarioService calendarioService;
	
	
	@GET
	@Path("/especialidades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CalEspecialidadeDTO> getCalEspecialidadeDTO() {
		
		return calendarioService.getCalEspecialidadeDTO();
		
	}
	
	@GET
	@Path("/agenda/medico")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List <CalAgendaMedicoDTO> getAgendaMedico(
			@QueryParam("dtinicio") String dtInicio,
			@QueryParam("dtfim") String dtFim,
			@QueryParam("idsunidade") List<Long> idsUnidade,
			@QueryParam("idsfuncionario") List<Long> idsFuncionario,
			@QueryParam("ids") List<Long> ids) throws ParseException{
		
//		Calendar c1 = Calendar.getInstance();
//		DateFormat f = DateFormat.getDateInstance(DateFormat.MEDIUM);
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//		
//		Date dtInicioDT = formatter.parse(dtInicio);
//		Date dtFimDT =  formatter.parse(dtFim);
//		String dateNow = f.format(new Date());
//		//Date dtNow = formatter.parse(dateNow);
//		
//		
//		if(dtInicioDT.before(new Date()) && dtFimDT.before(new Date())){
//			
//			return null;
//			
//		}else{
			
			if (ids == null || ids.size() == 0) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			else 
				return  calendarioService.getAgendaMedico(ids,dtInicio,dtFim,idsUnidade,idsFuncionario);
		
		
	}
	

}
