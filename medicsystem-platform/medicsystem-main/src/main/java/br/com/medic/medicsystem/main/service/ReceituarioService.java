package br.com.medic.medicsystem.main.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;

import br.com.medic.medicsystem.persistence.dao.AgendamentoDAO;
import br.com.medic.medicsystem.persistence.dao.AtendimentoDAO;
import br.com.medic.medicsystem.persistence.dao.ClienteDAO;
import br.com.medic.medicsystem.persistence.dao.DependenteDAO;
import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.dao.MedicamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ReceituarioDAO;
import br.com.medic.medicsystem.persistence.dao.ReceituarioMedicamentoDAO;
import br.com.medic.medicsystem.persistence.dao.ReceituarioOftalmologicoDAO;
import br.com.medic.medicsystem.persistence.dao.TemplateReceituarioDAO;
import br.com.medic.medicsystem.persistence.dto.ReceituarioDTO;
import br.com.medic.medicsystem.persistence.exception.ObjectNotFoundException;
import br.com.medic.medicsystem.persistence.model.Agendamento;
import br.com.medic.medicsystem.persistence.model.Atendimento;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EspProfissional;
import br.com.medic.medicsystem.persistence.model.Medicamento;
import br.com.medic.medicsystem.persistence.model.Receituario;
import br.com.medic.medicsystem.persistence.model.MedicamentoReceituario;
import br.com.medic.medicsystem.persistence.model.MedicamentoTemplateReceituario;
import br.com.medic.medicsystem.persistence.model.ReceituarioOftalmologico;
import br.com.medic.medicsystem.persistence.model.TemplateReceituario;



@Stateless
public class ReceituarioService {
	
	@Inject
	@Named("agendamento-dao")
	private AgendamentoDAO agendamentoDAO;
	
	@Inject
	@Named("receituario-dao")
	private ReceituarioDAO receituarioDAO;
	
	
	@Inject
	@Named("atendimento-dao")
	private AtendimentoDAO atendimentoDAO;
	
	@Inject
	@Named("dependente-dao")
	private DependenteDAO dependenteDAO;
	
	@Inject
	@Named("cliente-dao")
	private ClienteDAO clienteDAO;
	
	@Inject
	@Named("medicamento-dao")
	private MedicamentoDAO medicamentoDAO;
	
	@Inject
	@Named("receituariomedicamento-dao")
	private ReceituarioMedicamentoDAO recmedicamentoDAO;
	
	@Inject
	@Named("receituarioOftalmologico-dao")
	private ReceituarioOftalmologicoDAO receituarioOftalmologicoDAO;
	
	@Inject
	@Named("template-receituario-dao")
	private TemplateReceituarioDAO templateReceituarioDAO;
	
	@Inject
	@Named("especialidade-dao")
	private EspecialidadeDAO especialidadeDAO;
	
	@PersistenceContext(unitName = "saudesystem")
    protected EntityManager entityManager;
	
	
//	public boolean saveReceituario(Long idAgendamento, Receituario receituario, String inTipoPaciente, Long idPaciente){
//		
//		boolean receituarioSalvo = false;
//		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
//		
//		
//		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());
//		
//		if(inTipoPaciente.equals("D")){
//			Dependente dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, idPaciente);
//			receituario.setDependente(dependenteOriginal);
//		}else{
//			Cliente clienteOriginal = clienteDAO.searchByKey(Cliente.class, idPaciente);
//			receituario.setCliente(clienteOriginal);
//		}
//
//		if(atendimentoOriginal != null ){
//			
//			receituario.setAtendimento(atendimentoOriginal);
//			try {
//				receituarioDAO.persist(receituario);
//				receituarioSalvo = true;
//			} catch (Exception e) {
//				throw new ObjectNotFoundException(
//						"Erro ao registrar receituario");
//			}
//		}else{
//			Atendimento novoAtendimento = new Atendimento();
//			novoAtendimento.setNmQueixaPaciente("");
//			novoAtendimento.setDtAtendimento(Calendar.getInstance().getTime());
//			novoAtendimento.setBoFinalizado(new Boolean(false));
//			
//			
//			novoAtendimento.setAgendamento(agendamentoOriginal);
//			
//			try {
//				atendimentoDAO.persist(novoAtendimento);
//				receituario.setAtendimento(novoAtendimento);
//				
//						try {
//							receituarioDAO.persist(receituario);
//							receituarioSalvo = true;
//						} catch (Exception e) {
//							throw new ObjectNotFoundException(
//									"Erro ao registrar receituario");
//						}
//				
//				
//			} catch (Exception e) {
//				throw new ObjectNotFoundException(
//						"Erro ao registrar o novo atendimento");
//			}
//		}
//		
//		return receituarioSalvo;
//		
//	}
	
	public boolean saveReceituario(Long idAgendamento, Receituario receituario, String inTipoPaciente, Long idPaciente){
		boolean receituarioSalvo = false;
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());
		
		if(inTipoPaciente.equals("D")) {
			Dependente dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, idPaciente);
			receituario.setDependente(dependenteOriginal);
		} else {
			Cliente clienteOriginal = clienteDAO.searchByKey(Cliente.class, idPaciente);
			receituario.setCliente(clienteOriginal);
		}
		if(atendimentoOriginal != null ) {
			receituario.setAtendimento(atendimentoOriginal);
			try {
//				List<ReceituarioMedicamentos> list = receituario.getMedicamentos();
//				receituario.setMedicamentos(null);
				receituarioDAO.persist(receituario);
//				for(ReceituarioMedicamentos med : list) {
//					med.setIdReceituario(receituario.getId());
//				}
//				receituario.setMedicamentos(list);
//				receituarioDAO.update(receituario);
				receituarioSalvo = true;
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao registrar receituario");
			}
		} else {
			Atendimento novoAtendimento = new Atendimento();
			novoAtendimento.setNmQueixaPaciente("");
			novoAtendimento.setDtAtendimento(Calendar.getInstance().getTime());
			novoAtendimento.setBoFinalizado(new Boolean(false));
			novoAtendimento.setAgendamento(agendamentoOriginal);
			try {
				atendimentoDAO.persist(novoAtendimento);
				receituario.setAtendimento(novoAtendimento);
				try {
					receituarioDAO.persist(receituario);
					receituarioSalvo = true;
				} catch (Exception e) {
					throw new ObjectNotFoundException(
							"Erro ao registrar receituario");
				}
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao registrar o novo atendimento");
			}
		}
		return receituarioSalvo;
	}
	
	public boolean updateReceituario(Receituario receituario){
		boolean receituarioSalvo = false;
		
		Receituario receituarioOriginal = receituarioDAO.searchByKey(Receituario.class, receituario.getId());
		if(receituarioOriginal != null) {
			receituarioOriginal.setNmReceituario(receituario.getNmReceituario());
			receituarioOriginal.setRecomendacoes(receituario.getRecomendacoes());
			List<MedicamentoReceituario> list = null;
			if(receituario.getMedicamentos() != null) {
				list = new ArrayList<MedicamentoReceituario>();
				for(MedicamentoReceituario med : receituario.getMedicamentos()) {
					list.add(recmedicamentoDAO.merge(med));
				}
			}
			receituarioOriginal.updateMedicamentosList(list);
			try {
				receituarioDAO.update(receituarioOriginal);
				 return receituarioSalvo = true;
			} catch (Exception e) {
				throw new ObjectNotFoundException(
						"Erro ao atualizar o receituario");
			}
		}
		return receituarioSalvo;
	}
	
	
	public ReceituarioDTO getReceituario(Long idAgendamento){
		Receituario receituarioOriginal = new Receituario();
		ReceituarioDTO receituarioDTO = new ReceituarioDTO();
		
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(idAgendamento);
		if(atendimentoOriginal != null){
			receituarioOriginal = receituarioDAO.getReceituarioPorAtendimento(atendimentoOriginal.getId());
		}
		if(receituarioOriginal != null){	
			receituarioDTO.setNmReceituario(receituarioOriginal.getNmReceituario());
			receituarioDTO.setId(receituarioOriginal.getId());
			receituarioDTO.setDtReceituario(receituarioOriginal.getDtReceituario());
			receituarioDTO.setMedicamentos(receituarioOriginal.getMedicamentos());
			receituarioDTO.setRecomendacoes(receituarioOriginal.getRecomendacoes());
			return receituarioDTO;
		}else
			return null;
		
	}
	
	public List<Medicamento> searchMedicamentos(String nmMedicamento) {
		return medicamentoDAO.searchMedicamentos(nmMedicamento);
	}
	
	
	
	public ReceituarioOftalmologico getReceituarioOftal(Long idAgendamento){
		
		ReceituarioOftalmologico recOfitalOriginal = new ReceituarioOftalmologico();
		
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(idAgendamento);
		
		if(atendimentoOriginal != null){
			
			recOfitalOriginal = receituarioOftalmologicoDAO.getReceituarioOftalPorAtendimento(atendimentoOriginal.getId());
		}
		
		if(recOfitalOriginal != null){
				
				return formatarReceituarioOftal(recOfitalOriginal);
			}else
				return null;
		
		
	}
	
public boolean saveReceituarioOftal(Long idAgendamento, ReceituarioOftalmologico receituariooftal, String inTipoPaciente, Long idPaciente){
		
		boolean receituarioSalvo = false;
		Agendamento agendamentoOriginal = agendamentoDAO.getAgendamento(idAgendamento);
		
		receituariooftal.setDtReceituarioOftalmologico(new Date());
		
		
		Atendimento atendimentoOriginal = atendimentoDAO.getAtendimentoPorIdAgendamento(agendamentoOriginal.getId());
		
		if(inTipoPaciente.equals("D")){
			Dependente dependenteOriginal = dependenteDAO.searchByKey(Dependente.class, idPaciente);
			receituariooftal.setDependente(dependenteOriginal);
		}else{
			Cliente clienteOriginal = clienteDAO.searchByKey(Cliente.class, idPaciente);
			receituariooftal.setCliente(clienteOriginal);
		}

		if(atendimentoOriginal != null ){
			
			receituariooftal.setAtendimento(atendimentoOriginal);
			try {
				receituarioOftalmologicoDAO.persist(receituariooftal);
				receituarioSalvo = true;
			} catch (Exception e) {
				System.out.println(e);
				throw new ObjectNotFoundException(
						"Erro ao registrar receituario");
			}
		}else{
			Atendimento novoAtendimento = new Atendimento();
			novoAtendimento.setNmQueixaPaciente("");
			novoAtendimento.setDtAtendimento(Calendar.getInstance().getTime());
			novoAtendimento.setBoFinalizado(new Boolean(false));
			
			
			novoAtendimento.setAgendamento(agendamentoOriginal);
			
			try {
				atendimentoDAO.persist(novoAtendimento);
				receituariooftal.setAtendimento(novoAtendimento);
				
						try {
							receituarioOftalmologicoDAO.persist(receituariooftal);
							receituarioSalvo = true;
						} catch (Exception e) {
							System.out.println(e);
							throw new ObjectNotFoundException(
									"Erro ao registrar receituario");
						}
				
				
			} catch (Exception e) {
				System.out.println(e);
				throw new ObjectNotFoundException(
						"Erro ao registrar o novo atendimento");
			}
		}
		
		return receituarioSalvo;
		
	}


public boolean updateReceituarioOftal(ReceituarioOftalmologico receituario){
	
	boolean receituarioSalvo = false;
	
	ReceituarioOftalmologico receituarioOriginal = receituarioOftalmologicoDAO.searchByKey(ReceituarioOftalmologico.class, receituario.getId());
	
	if(receituarioOriginal != null){
		receituarioOriginal.setNmObservacao(receituario.getNmObservacao() != null ? receituario.getNmObservacao() : null);
		receituarioOriginal.setNrLongeOdEsferico(receituario.getNrLongeOdEsferico() != null ? receituario.getNrLongeOdEsferico(): null);
		receituarioOriginal.setNrLongeOdCilindrico(receituario.getNrLongeOdCilindrico() != null ? receituario.getNrLongeOdCilindrico() : null);
		receituarioOriginal.setNrLongeOdEixo(receituario.getNrLongeOdEixo() != null ? receituario.getNrLongeOdEixo() : null);
		receituarioOriginal.setNrLongeOeEsferico(receituario.getNrLongeOeEsferico() != null ? receituario.getNrLongeOeEsferico(): null);
		receituarioOriginal.setNrLongeOeCilindrico(receituario.getNrLongeOeCilindrico() != null ? receituario.getNrLongeOeCilindrico() : null);
		receituarioOriginal.setNrLongeOeEixo(receituario.getNrLongeOeEixo() != null ? receituario.getNrLongeOeEixo() : null);
		receituarioOriginal.setNrPertoOdEsferico(receituario.getNrPertoOdEsferico() != null ? receituario.getNrPertoOdEsferico() : null);
		receituarioOriginal.setNrPertoOdCilindrico(receituario.getNrPertoOdCilindrico() != null ? receituario.getNrPertoOdCilindrico() : null);
		receituarioOriginal.setNrPertoOdEixo(receituario.getNrPertoOdEixo() != null ? receituario.getNrPertoOdEixo() : null);
		receituarioOriginal.setNrPertoOeEsferico(receituario.getNrPertoOeEsferico() != null ? receituario.getNrPertoOeEsferico() : null);
		receituarioOriginal.setNrPertoOeCilindrico(receituario.getNrPertoOeCilindrico() != null ? receituario.getNrPertoOeCilindrico() : null);
		receituarioOriginal.setNrPertoOeEixo(receituario.getNrPertoOeEixo() != null ? receituario.getNrPertoOeEixo() : null);
		try {
			receituarioOftalmologicoDAO.update(receituarioOriginal);
			 return receituarioSalvo = true;
		} catch (Exception e) {
			throw new ObjectNotFoundException(
					"Erro ao atualizar o receituario");
		}
		
	}
	
	return receituarioSalvo;
	
	
}

	private ReceituarioOftalmologico formatarReceituarioOftal (ReceituarioOftalmologico receituario){
		
//		ReceituarioOftalmologico receituarioOriginal = new ReceituarioOftalmologico();
		
		receituario.setNmObservacao(receituario.getNmObservacao() != null ? receituario.getNmObservacao() : "");
		receituario.setNrLongeOdEsferico(receituario.getNrLongeOdEsferico() != null ? receituario.getNrLongeOdEsferico(): "");
		receituario.setNrLongeOdCilindrico(receituario.getNrLongeOdCilindrico() != null ? receituario.getNrLongeOdCilindrico() : "");
		receituario.setNrLongeOdEixo(receituario.getNrLongeOdEixo() != null ? receituario.getNrLongeOdEixo() : "");
		receituario.setNrLongeOeEsferico(receituario.getNrLongeOeEsferico() != null ? receituario.getNrLongeOeEsferico(): "");
		receituario.setNrLongeOeCilindrico(receituario.getNrLongeOeCilindrico() != null ? receituario.getNrLongeOeCilindrico() : "");
		receituario.setNrLongeOeEixo(receituario.getNrLongeOeEixo() != null ? receituario.getNrLongeOeEixo() : "");
		receituario.setNrPertoOdEsferico(receituario.getNrPertoOdEsferico() != null ? receituario.getNrPertoOdEsferico() : "");
		receituario.setNrPertoOdCilindrico(receituario.getNrPertoOdCilindrico() != null ? receituario.getNrPertoOdCilindrico() : "");
		receituario.setNrPertoOdEixo(receituario.getNrPertoOdEixo() != null ? receituario.getNrPertoOdEixo() : "");
		receituario.setNrPertoOeEsferico(receituario.getNrPertoOeEsferico() != null ? receituario.getNrPertoOeEsferico() : "");
		receituario.setNrPertoOeCilindrico(receituario.getNrPertoOeCilindrico() != null ? receituario.getNrPertoOeCilindrico() : "");
		receituario.setNrPertoOeEixo(receituario.getNrPertoOeEixo() != null ? receituario.getNrPertoOeEixo() : "");
		
		
		return receituario;
	}
	
	
	public List<ReceituarioOftalmologico> historicoOftalmologico(Long idPaciente, String inTipoPaciente){
		
		 List<ReceituarioOftalmologico> receituarioOriginal = receituarioOftalmologicoDAO.getReceituarioOftalmologicoPorIdPaciente(idPaciente, inTipoPaciente);
		
		if(receituarioOriginal != null){
			
			return receituarioOriginal;
		}else
			return null;
		
	}
	
	public List<TemplateReceituario> getTemplatesReceituarioForEspecialidadeProfissional(Long idMedico, Long idEspecialidade) {
		return templateReceituarioDAO.getTemplatesReceituarioForEspecialidadeProfissional(idMedico, idEspecialidade);
	}
	
	public TemplateReceituario getTemplateReceituario(Long idTemplate) {
		return templateReceituarioDAO.getTemplateReceituario(idTemplate);
	}
	
	public TemplateReceituario updateTemplateReceituario(Long idTemplate, TemplateReceituario template) {
		TemplateReceituario tpl = templateReceituarioDAO.getTemplateReceituario(idTemplate);
		if(tpl == null) {
			throw new WebApplicationException("Template fornecido não existe", 500);
		}
		tpl.setNmTemplate(template.getNmTemplate());
		// atualizacao de medicamentos
		List<MedicamentoTemplateReceituario> list = null;
		if(tpl.getMedicamentosTemplate() != null) {
			list = new ArrayList<MedicamentoTemplateReceituario>();
			for(MedicamentoTemplateReceituario med : template.getMedicamentosTemplate()) {
				list.add(entityManager.merge(med));
			}
		}
		tpl.getMedicamentosTemplate().clear();
		tpl.getMedicamentosTemplate().addAll(list);
		//---------------------------------------------------
		tpl.setDtAtualizacaoLog(new Date());
		tpl = templateReceituarioDAO.update(tpl);
		return tpl;
	}
	
	public TemplateReceituario deleteTemplateReceituario(Long idTemplate) {
		TemplateReceituario tpl = templateReceituarioDAO.getTemplateReceituario(idTemplate);
		if(tpl == null) {
			throw new WebApplicationException("Template fornecido não existe", 500);
		}
		tpl.setDtExclusao(new Date());
		return templateReceituarioDAO.update(tpl);
	}
	
	public TemplateReceituario saveTemplateReceituario(Long idMedico, Long idEspecialidade, 
			TemplateReceituario newTemplate) {
		EspProfissional espProfissional = especialidadeDAO.getEspProfissional(idMedico, idEspecialidade);
		if(espProfissional == null) {
			throw new WebApplicationException("Profissional não atende com a especialidade fornecidda", 500);
		}
		newTemplate.setEspProfissional(espProfissional);
		newTemplate.setDtInclusaoLog(new Date());
		TemplateReceituario tpl = templateReceituarioDAO.persist(newTemplate);
		return tpl;
	}
	
	public List<TemplateReceituario> getTemplatesReceituarioForAgendamento(Long idAgendamento) {
		Agendamento ag = agendamentoDAO.getAgendamento(idAgendamento);
		if(ag == null) {
			throw new WebApplicationException("Agendamento fornecido não existe", 500);
		}
		return this.getTemplatesReceituarioForEspecialidadeProfissional(ag.getProfissional().getId(), 
				ag.getEspecialidade().getId());
	}
}
