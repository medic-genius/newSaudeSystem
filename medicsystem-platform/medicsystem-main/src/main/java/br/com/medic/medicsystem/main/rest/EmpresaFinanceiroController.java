package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.EmpresaFinanceiroService;
import br.com.medic.medicsystem.persistence.model.EmpresaFinanceiro;

@Path("/empresafinanceiro")
@RequestScoped
public class EmpresaFinanceiroController  extends BaseController  {

	@Inject
	private EmpresaFinanceiroService empresaFinanceiroService;
	

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public EmpresaFinanceiro getEmpresaFinanceiro(@PathParam("id") Long id) {
		final EmpresaFinanceiro empresaFinanceiro = empresaFinanceiroService.getEmpresaFinanceiro(id);

		if (empresaFinanceiro == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return empresaFinanceiro;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaFinanceiro> getEmpresasFinanceiro() {
		final List<EmpresaFinanceiro> empresaFinanceiroList = empresaFinanceiroService.getEmpresasFinanceiro( true );

		if ( empresaFinanceiroList.isEmpty() || empresaFinanceiroList == null) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {
			return empresaFinanceiroList;
		}

	}

	@GET
	@Path("/folha")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaFinanceiro> getEmpresasFinanceiroFolha() {
		final List<EmpresaFinanceiro> empresaFinanceiroList = empresaFinanceiroService.getEmpresasFinanceiro( false );

		if ( empresaFinanceiroList.isEmpty() || empresaFinanceiroList == null) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {
			return empresaFinanceiroList;
		}

	}
	
}
