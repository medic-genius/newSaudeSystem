package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.medic.medicsystem.main.service.CobrancaFinanceiroService;
import br.com.medic.medicsystem.main.service.NegociacaoAutomaticaService;
import br.com.medic.medicsystem.persistence.model.views.NegociacaoAutomaticaView;

@Path("/negociacao")
@RequestScoped
public class NegociacaoController extends BaseController {

	@Inject
	CobrancaFinanceiroService negociacaoService;
	
	@Inject
	NegociacaoAutomaticaService negociacaoAutomaticaService;
	
	@GET
	@Path("/clientescontratos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NegociacaoAutomaticaView> getClientesNegociacaoView(
			 @QueryParam("inFormaPagamento") Integer inFormaPagamento, @QueryParam("qtdMensalidade") Integer qtdMensalidade, 
			 @QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim, @QueryParam("boNegociadoAuto") Boolean boNegociadoAuto, 
			 @QueryParam("inTipoDesconto") Integer inTipoDesconto, @QueryParam("vlDesconto") Float vlDesconto) {
				
		
		List<NegociacaoAutomaticaView> negociacaoAutomaticaView = 
				negociacaoAutomaticaService.getClientesNegociacaoView(inFormaPagamento, qtdMensalidade, dtInicio, dtFim, boNegociadoAuto, 
						inTipoDesconto, vlDesconto);

		if (negociacaoAutomaticaView == null || negociacaoAutomaticaView.size() < 0) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return negociacaoAutomaticaView;
	}
	
}
