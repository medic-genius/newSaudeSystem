package br.com.medic.medicsystem.main.vindi.data.summary;

public class ProductItem {
	private Integer id;
	
	private Product product;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
