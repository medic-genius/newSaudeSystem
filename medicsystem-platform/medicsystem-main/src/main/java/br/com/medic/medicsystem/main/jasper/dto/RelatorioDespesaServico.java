package br.com.medic.medicsystem.main.jasper.dto;

import java.text.DecimalFormat;

import br.com.medic.medicsystem.persistence.model.Servico;

public class RelatorioDespesaServico {

	private String qtServico;

	private String nmServico;

	private String vlServico;

	private String vlTotal;
	
	private String nmPreparatorio;

	public RelatorioDespesaServico(Servico servico, Integer qtServico,
	        Double vlServico) {
		DecimalFormat formatador = new DecimalFormat();
		formatador.setMinimumFractionDigits(2);

		this.qtServico = qtServico.toString();
		this.nmServico = servico.getNmServico();
		this.vlServico = formatador.format(vlServico);
		this.vlTotal = formatador.format(qtServico * vlServico);
		this.nmPreparatorio = servico.getNmPreparatorio();
	}

	public String getNmServico() {

		return nmServico;
	}

	public void setNmServico(String nmServico) {

		this.nmServico = nmServico;
	}

	public String getQtServico() {

		return qtServico;
	}

	public void setQtServico(String qtServico) {

		this.qtServico = qtServico;
	}

	public String getVlServico() {

		return vlServico;
	}

	public void setVlServico(String vlServico) {

		this.vlServico = vlServico;
	}

	public String getVlTotal() {

		return vlTotal;
	}

	public void setVlTotal(String vlTotal) {

		this.vlTotal = vlTotal;
	}

	public String getNmPreparatorio() {
		return nmPreparatorio;
	}

	public void setNmPreparatorio(String nmPreparatorio) {
		this.nmPreparatorio = nmPreparatorio;
	}
	
	

}
