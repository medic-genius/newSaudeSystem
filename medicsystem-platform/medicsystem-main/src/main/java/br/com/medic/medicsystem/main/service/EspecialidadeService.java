package br.com.medic.medicsystem.main.service;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.dao.EspecialidadeDAO;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.SubEspecialidade;

@Stateless
public class EspecialidadeService {

	@Inject
	private Logger logger;

	@Inject
	@Named("especialidade-dao")
	private EspecialidadeDAO especialidadeDAO;

	public List<Especialidade> getEspecialidades() {
		logger.debug("Obtendo especialidades...");

		return especialidadeDAO.getEspecialidades();
	}

	public List<SubEspecialidade> getSubEspecialidades(Long idEspecialidade) {
		logger.debug("Obtendo subEspecialidades...");

		return especialidadeDAO.getSubEspecialidades(idEspecialidade);
	}
	
	
	public Collection<Especialidade> getEspecialidadesAtivas() {

		return especialidadeDAO.getEspecialidadesAtivas();
	}

}
