package br.com.medic.medicsystem.main.jasper;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import br.com.medic.medicsystem.persistence.model.BoletoBancario;

public class JRDataSourceBoletoBancarioRenovacao  implements JRDataSource {

	private DateFormat date = new SimpleDateFormat( "dd/MM/yyyy" );
	private DecimalFormat formatador = new DecimalFormat();

	private Iterator< BoletoBancario > itr;
	private Object valorAtual;
	private boolean irParaProximo = true;

	public JRDataSourceBoletoBancarioRenovacao(List<BoletoBancario> boletos) {

		formatador.setMinimumFractionDigits(2);
		formatador.setMaximumFractionDigits(2);

		this.itr = boletos.iterator();
	}

	public boolean next() throws JRException {

		valorAtual = itr.hasNext() ? itr.next() : null;
		irParaProximo = ( valorAtual != null );

		return irParaProximo;
	}

	public Object getFieldValue( JRField campo ) throws JRException {

		Object valor = null;

		BoletoBancario boleto = (BoletoBancario) valorAtual;

		if ("nrBoleto".equals(campo.getName())) {
			valor = boleto.getNrBoleto().toString();
		} else if ("dtVencimento".equals(campo.getName())) {
			valor = date.format(boleto.getMensalidade().getDataVencimento());
		} else if ("nrAgCedente".equals(campo.getName())) {
			if (boleto.getMensalidade().getContrato().getEmpresaGrupo().getId() == 2) { // Odontomed
				valor = "1300/041258-9";
			} else if (boleto.getMensalidade().getContrato().getEmpresaGrupo()
					.getId() == 9) { // Real Saude
				valor = "1300/247479-4";
			}
		} else if ("nrNossoNumero".equals(campo.getName())) {
			valor = boleto.getNrNossoNumero();
		} else if ("vlDocumento".equals(campo.getName())) {
			valor = formatador.format(boleto.getMensalidade()
					.getValorMensalidade());
		} else if ("nmCliente".equals(campo.getName())) {
			valor = boleto.getCliente().getNmCliente() + " - CPF:"
					+ boleto.getCliente().getNrCPF();
		} else if ("dtEmissao".equals(campo.getName())) {
			valor = date.format(new Date());
		} else if ("dtProcessamento".equals(campo.getName())) {
			valor = date.format(new Date());
		} else if ("nmEndereco".equals(campo.getName())) {
			valor = (boleto.getCliente().getNmLogradouro() != null ? boleto
					.getCliente().getNmLogradouro() : "")
					+ (boleto.getCliente().getNrNumero() != null ? ", N� "
							+ boleto.getCliente().getNrNumero() : "")
					+ (boleto.getCliente().getNmComplemento() != null ? " - "
							+ boleto.getCliente().getNmComplemento() : "")
					+ (boleto.getCliente().getBairro() != null ? " - "
							+ boleto.getCliente().getBairro().getNmBairro()
							: "");
		} else if ("nmComplemento".equals(campo.getName())) {
			valor = (boleto.getCliente().getNrCEP() != null ? boleto
					.getCliente().getNrCEP() : "")
					+ (boleto.getCliente().getCidade() != null ? " - "
							+ boleto.getCliente().getCidade().getNmCidade()
							+ " - " + boleto.getCliente().getCidade().getNmUF()
							: "");
		} else if ("nrBarra".equals(campo.getName())) {
			valor = boleto.getNrBarra();
		} else if ("nrCodigoBarra".equals(campo.getName())) {
			valor = boleto.getCdBarra();
		} else if ("nmFantasia".equals(campo.getName())) {
			valor = boleto.getMensalidade().getContrato().getEmpresaGrupo()
					.getNmFantasia()
					+ " - CNPJ:"
					+ boleto.getMensalidade().getContrato().getEmpresaGrupo()
							.getNrCnpj();
		} else if ("nmRazaoSocial".equals(campo.getName())) {
			valor = boleto.getMensalidade().getContrato().getEmpresaGrupo()
					.getNmRazaoSocial()
					+ " - CNPJ:"
					+ boleto.getMensalidade().getContrato().getEmpresaGrupo()
							.getNrCnpj();
		} else if ("nrContrato".equals(campo.getName())) {
			valor = boleto.getMensalidade().getContrato().getNrContrato();
		}

		return valor;
	}
}
