package br.com.medic.medicsystem.main.webservice.pjbank;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.vindi.util.BaseHttpService;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Mensalidade;
import br.com.medic.medicsystem.persistence.model.Parcela;
import br.com.medic.medicsystem.persistence.model.pjbank.BoletoPagar;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WebServiceBoleto extends BaseHttpService{
	
	private final String request = "/api/v2";
			
	//Teste
//	public static final String CREDENCIAL_KEY = "9551ee0828a18f9f79fea3973e58c8eb3694db1d";
//	public static final String CHAVE_KEY = "ba5e1a4d601e76c7b5e1d990bd415918e5502681";
	
	//Producao
	public static final String CREDENCIAL_KEY = "1d20f733e5200e9ae1a8b683db23536c5276b4f4";
	public static final String CHAVE_KEY = "b022d59057270b8990d274847d86eaf022c44527";
	
	protected static final String GATEWAY_HOST = "pjbank.com.br";
					
	public JSONObject criarEditarBoleto( Object documento, Object cliente) throws IOException {
		
		String url = request + "/pagar";
		
		BoletoPagar boletoPagar = new BoletoPagar();
		HttpPost post = new HttpPost();		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		JSONObject jsonReturn;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		DecimalFormat df = new DecimalFormat("0.##");
		Cliente clienteBoleto = null;
		EmpresaCliente empresaBoleto = null;
		
		Mensalidade mensalidade = null;
		Parcela parcela = null;		

		Double TxJuros = new Double(0.14);
		Double TxMulta = new Double(2);
		
		if(documento instanceof Mensalidade){
			mensalidade = (Mensalidade) documento;
					
			boletoPagar.setVencimento(sdf.format(mensalidade.getDataVencimento()));
			boletoPagar.setValor( Float.valueOf( df.format(mensalidade.getValorMensalidade()).replace(",", ".") ) );
			boletoPagar.setPedido_numero("1" + mensalidade.getId().toString()); //Mensalidade = 1
			boletoPagar.setTexto("Contrato: " + mensalidade.getContrato().getNrContrato());
		}
		else if(documento instanceof Parcela){
			parcela = (Parcela) documento;
						
			boletoPagar.setVencimento(sdf.format(parcela.getDtVencimento()));
			boletoPagar.setValor( Float.valueOf( df.format(parcela.getVlParcela()).replace(",", ".") ) );
			boletoPagar.setPedido_numero("0" + parcela.getId().toString()); //Parcela = 0
			boletoPagar.setTexto("Contrato: " + parcela.getDespesa().getContratoCliente().getContrato().getNrContrato());
		}
						
		boletoPagar.setCredencial(CREDENCIAL_KEY);	
		
		boletoPagar.setJuros(df.format(TxJuros).replace(",", "."));  // 4,20% a.m ou 0,14 dia
		boletoPagar.setMulta(df.format(TxMulta).replace(",", "."));  // 2%
		boletoPagar.setDesconto("");
		
		if(cliente instanceof Cliente){
			clienteBoleto = (Cliente) cliente;
			
			String nmCliente = Normalizer.normalize(clienteBoleto.getNmCliente(), Normalizer.Form.NFD);
			nmCliente = nmCliente.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			nmCliente = nmCliente.replaceAll("[^\\w\\s]", "");
			nmCliente = nmCliente.trim();
			nmCliente = nmCliente.substring(0, nmCliente.length() > 80 ? 80 : nmCliente.length()).trim();
			
			boletoPagar.setNome_cliente(nmCliente);
			boletoPagar.setCpf_cliente(clienteBoleto.getNrCPF());
			
			String nmEndereco = null;
			String numeroEndereco = null;
			if(clienteBoleto.getNmLogradouro() == null) {
				nmEndereco = "AV. JOAQUIM NABUCO";
				numeroEndereco = "2213";
			} else {
				nmEndereco = Normalizer.normalize(clienteBoleto.getNmLogradouro(), Normalizer.Form.NFD);
				nmEndereco = nmEndereco.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmEndereco = nmEndereco.replaceAll("[^\\w\\s]", "");
				nmEndereco = nmEndereco.trim();
				
				numeroEndereco = clienteBoleto.getNrNumero().trim().replaceAll("[^0-9]", "");
			}
			boletoPagar.setEndereco_cliente(nmEndereco);			
			boletoPagar.setNumero_cliente(numeroEndereco);
			
			String numeroChar = numeroEndereco;
			boletoPagar.setComplemento_cliente(numeroChar.length() > 0 ? "Numero "+ clienteBoleto.getNrNumero() + " " + clienteBoleto.getNmComplemento() : clienteBoleto.getNmComplemento());
			
			String nmBairro = "CENTRO";
			String nmCidade = "MANAUS";
			String nmUF = "AM";
			Integer nrCep = new Integer(69020031);
			
			if(clienteBoleto.getBairro() != null && clienteBoleto.getCidade() != null && clienteBoleto.getNrCEP() != null) {
				nmBairro = Normalizer.normalize(clienteBoleto.getBairro().getNmBairro(), Normalizer.Form.NFD);
				nmBairro = nmBairro.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmBairro = nmBairro.replaceAll("[^\\w\\s]", "");
				nmBairro = nmBairro.trim();
				
				nmCidade = Normalizer.normalize(clienteBoleto.getCidade().getNmCidade(), Normalizer.Form.NFD);
				nmCidade = nmCidade.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmCidade = nmCidade.replaceAll("[^\\w\\s]", "");
				nmCidade = nmCidade.trim();
				
				nmUF = Normalizer.normalize(clienteBoleto.getCidade().getNmUF(), Normalizer.Form.NFD);
				nmUF = nmUF.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmUF = nmUF.replaceAll("[^\\w\\s]", "");
				nmUF = nmUF.trim();
				
				nrCep = Integer.parseInt(clienteBoleto.getNrCEP().replace("\\D", ""));
			}
						
			boletoPagar.setBairro_cliente(nmBairro);						
			boletoPagar.setCidade_cliente(nmCidade);						
			boletoPagar.setEstado_cliente(nmUF);
			boletoPagar.setCep_cliente(nrCep);
		}
		else if(cliente instanceof EmpresaCliente) {
			empresaBoleto = (EmpresaCliente) cliente;
			
			String nmEmpresa = Normalizer.normalize(empresaBoleto.getNmRazaoSocial(), Normalizer.Form.NFD);
			nmEmpresa = nmEmpresa.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			nmEmpresa = nmEmpresa.replaceAll("[^\\w\\s]", "");
			nmEmpresa = nmEmpresa.trim();
			nmEmpresa = nmEmpresa.substring(0, nmEmpresa.length() > 80 ? 80 : nmEmpresa.length()).trim();
					
			boletoPagar.setNome_cliente(nmEmpresa);			
			boletoPagar.setCpf_cliente(empresaBoleto.getNrCnpj());
			
			String nmLogradouro = Normalizer.normalize(empresaBoleto.getNmLogradouro(), Normalizer.Form.NFD);
			nmLogradouro = nmLogradouro.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			nmLogradouro = nmLogradouro.replaceAll("[^\\w\\s]", "");
			nmLogradouro = nmLogradouro.trim();
			
			boletoPagar.setEndereco_cliente(nmLogradouro);			
			boletoPagar.setNumero_cliente(empresaBoleto.getNrNumero().replaceAll("[^0-9]", ""));
			
			String numeroChar = empresaBoleto.getNrNumero().replaceAll("[^0-9]", "");
			boletoPagar.setComplemento_cliente(numeroChar.length() > 0 ? "Numero "+ empresaBoleto.getNrNumero() + " " + empresaBoleto.getNmComplemento() : empresaBoleto.getNmComplemento());
			
			String nmBairro = "";
			String nmCidade = "";
			String nmUF = "";
			Integer nrCep = new Integer(0);
			
			if(empresaBoleto.getBairro() != null && empresaBoleto.getCidade() != null && empresaBoleto.getNrCep() != null) {
				nmBairro = Normalizer.normalize(empresaBoleto.getBairro().getNmBairro(), Normalizer.Form.NFD);
				nmBairro = nmBairro.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmBairro = nmBairro.replaceAll("[^\\w\\s]", "");
				nmBairro = nmBairro.trim();
				
				nmCidade = Normalizer.normalize(empresaBoleto.getCidade().getNmCidade(), Normalizer.Form.NFD);
				nmCidade = nmCidade.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmCidade = nmCidade.replaceAll("[^\\w\\s]", "");
				nmCidade = nmCidade.trim();
				
				nmUF = Normalizer.normalize(empresaBoleto.getCidade().getNmUF(), Normalizer.Form.NFD);
				nmUF = nmUF.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
				nmUF = nmUF.replaceAll("[^\\w\\s]", "");
				nmUF = nmUF.trim();
				
				nrCep = Integer.parseInt(empresaBoleto.getNrCep().replaceAll("\\D", ""));
			}
			
			boletoPagar.setBairro_cliente(nmBairro);						
			boletoPagar.setCidade_cliente(nmCidade);			
			boletoPagar.setEstado_cliente(nmUF);
			boletoPagar.setCep_cliente(nrCep);
		}
		
		
		boletoPagar.setLogo_url("");		
		boletoPagar.setGrupo("");
				
		try {
			setPjbank(GATEWAY_HOST);

			post.setHeader("Content-Type", "application/json");
						
			jsonStr = mapper.writeValueAsString(boletoPagar);
			post.setEntity(new StringEntity(jsonStr, "UTF-8"));
			
			String response = sendRequest(url, post);				
			System.out.println(response);
			
			jsonReturn = new JSONObject(response);		
			System.out.println(jsonReturn);
			
			return jsonReturn;
						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
				
	}
		
	public String imprimirBoleto(List<?> documentos) {
		
		String url = request + "/imprimir?formato=carne&nunca_atualizar_boleto=1";
				
		JSONObject json = new JSONObject();
		JSONArray pedidoNumero = new JSONArray();
		HttpPost post = new HttpPost();
		JSONObject jsonReturn;
				
		try {
			setPjbank(GATEWAY_HOST);
						
			post.setHeader("Content-Type", "application/json");			
			
			for(Object documento : documentos)
				if(documento instanceof Mensalidade)					
					pedidoNumero.put("1" + ((Mensalidade) documento).getId().toString() );
				else if(documento instanceof Parcela)
					pedidoNumero.put("0" + ((Parcela) documento).getId().toString() );						
									
			json.put("credencial", CREDENCIAL_KEY);
			json.put("chave", CHAVE_KEY);
			json.put("pedido_numero", pedidoNumero);

			StringEntity strEntity = new StringEntity(json.toString());			
			post.setEntity(strEntity);
			
			String response = sendRequest(url, post);
			
			jsonReturn = new JSONObject(response);		
			System.out.println(jsonReturn);
			
			if(jsonReturn.getInt("status") == 200)
				return jsonReturn.getString("linkBoleto");
			else	
				return null;			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
		
	}
	
	public String imprimirBoletoNormal(List<?> documentos) {
		
		String url = request + "/imprimir?nunca_atualizar_boleto=1";
				
		JSONObject json = new JSONObject();
		JSONArray pedidoNumero = new JSONArray();
		HttpPost post = new HttpPost();
		JSONObject jsonReturn;
				
		try {
			
			setPjbank(GATEWAY_HOST);			
						
			post.setHeader("Content-Type", "application/json");			
			
			for(Object documento : documentos)
				if(documento instanceof Mensalidade)					
					pedidoNumero.put("1" + ((Mensalidade) documento).getId().toString() );
				else if(documento instanceof Parcela)
					pedidoNumero.put("0" + ((Parcela) documento).getId().toString() );						
									
			json.put("credencial", CREDENCIAL_KEY);
			json.put("chave", CHAVE_KEY);
			json.put("pedido_numero", pedidoNumero);			

			StringEntity strEntity = new StringEntity(json.toString());			
			post.setEntity(strEntity);
			
			String response = sendRequest(url, post);
			
			jsonReturn = new JSONObject(response);		
			System.out.println(jsonReturn);
			
			if(jsonReturn.getInt("status") == 200)
				return jsonReturn.getString("linkBoleto");
			else	
				return null;			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
		
	}
	
	public JSONArray extratoBoleto( String dtInicio, String dtFim, Integer inTipoPago, Integer numPagina ) {
		
		//inTipoPago = null //boletos nao pagos
		//inTipoPago = 1 //boletos pagos
		
		String url = request + "/extrato";
		
		HttpGet get = new HttpGet();
		List<NameValuePair> parames = new LinkedList<NameValuePair>();
		JSONArray jsonListReturn;
							
		try {
			
			setPjbank(GATEWAY_HOST);
									
			get.setHeader("Content-Type", "application/json");
						
			parames.add(new BasicNameValuePair("credencial", CREDENCIAL_KEY));
			parames.add(new BasicNameValuePair("chave", CHAVE_KEY));
			parames.add(new BasicNameValuePair("pago", String.valueOf(inTipoPago)));
			parames.add(new BasicNameValuePair("data_inicio", String.valueOf(dtInicio)));
			parames.add(new BasicNameValuePair("data_fim", String.valueOf(dtFim)));
			parames.add(new BasicNameValuePair("pagina", String.valueOf(numPagina.intValue())));
			
			String paramString = URLEncodedUtils.format(parames, "utf-8");
			url += "?" + paramString;
			
			String response = sendRequest(url, get);
			
			jsonListReturn = new JSONArray(response);		
			//System.out.println(jsonListReturn);
			
			get.releaseConnection();
			
			return jsonListReturn;			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public JSONArray extratoBoleto( String dtInicio, String dtFim, Integer inTipoPago ) {
		
		return this.extratoBoleto(dtInicio, dtFim, inTipoPago, 1);
		
	}	
	
}
