package br.com.medic.medicsystem.main.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.medic.medicsystem.main.service.VendasService;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteDataDTO;
import br.com.medic.medicsystem.persistence.dto.AssinaturaRecorrenteResultDTO;
import br.com.medic.medicsystem.persistence.dto.PagamentoAgendamentoDTO;
import br.com.medic.medicsystem.persistence.model.LogVendedor;

@Path("/vendas")
@RequestScoped
public class VendasController extends BaseController {
	
	@Inject
	private VendasService vendasService;
	
	@POST
	@Path("/log")
	@Produces(MediaType.APPLICATION_JSON)
	public LogVendedor insertInLog(LogVendedor logObj) {
		if(logObj != null) {
			return vendasService.insertIntoLog(logObj);
		}
		return null;
	}
	
	@GET
	@Path("/clientes/{card:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public AssinaturaRecorrenteResultDTO checkClienteForCard(
			@PathParam("card") String cardNumber) {
		if(cardNumber != null) {
			LogVendedor log = vendasService.checkClienteCard(cardNumber);
			if(log != null && log.getVindiSubscriptionId() != null) {
				AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
				result.setStatus("success");
				result.setSubscriptionId(log.getVindiCustomerId());
				result.setMessage("Usuario ja cadastrado");
				return result;
			}
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus("fail");
		result.setMessage("Usuario nao encontrado");
		return result;
	}
	
	@POST
	@Path("/assinatura")
	@Produces(MediaType.APPLICATION_JSON)
	public AssinaturaRecorrenteResultDTO createAssinatura(
			AssinaturaRecorrenteDataDTO data) {
		if(data != null && data.getCartaoNomePortador() != null && 
				data.getCartaoNumero() != null && data.getCartaoCvv() != null && 
				data.getCartaoValidade() != null && data.getDiaVencimentoInicial() != null &&
				data.getIdEmpresaGrupo() != null && data.getPlanoIdRecorrencia() != null &&
				data.getQuantidade() != null && data.getValor() != null) {
			return vendasService.criarAssinaturaRecorrente(data);
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus("fail");
		result.setMessage("Dados não fornecidos ou ausentes");
		return result;
	}
	
	@POST
	@Path("/pagamento_agendamento")
	@Produces(MediaType.APPLICATION_JSON)
	public AssinaturaRecorrenteResultDTO createPagamentoAvulso(
			@QueryParam("source") String source,
			PagamentoAgendamentoDTO data) {
		if(data != null && data.getCartaoNomePortador() != null && 
				data.getCartaoNumero() != null && data.getCartaoCvv() != null && 
				data.getCartaoValidade() != null && data.getValor() != null && data.getIdAgendamento() != null) {
			return vendasService.pagamentoAgendamentoCartao(data, source);
		}
		AssinaturaRecorrenteResultDTO result = new AssinaturaRecorrenteResultDTO();
		result.setStatus("fail");
		result.setMessage("Dados não fornecidos ou ausentes");
		return result;
	}
}
