package br.com.medic.medicsystem.main.vindi.data.subscription;

public class SubscriptionResponse {
	private Subscription subscription;
	private Bill bill;
	
	public Subscription getSubscription() {
		return subscription;
	}
	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}
	public Bill getBill() {
		return bill;
	}
	public void setBill(Bill bill) {
		this.bill = bill;
	}
}
