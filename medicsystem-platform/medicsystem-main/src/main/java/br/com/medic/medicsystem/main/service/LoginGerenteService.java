package br.com.medic.medicsystem.main.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.medic.medicsystem.persistence.security.KeycloakHelper;
import br.com.medic.medicsystem.persistence.security.dto.KeycloakTokenDTO;
import br.com.medic.medicsystem.persistence.security.dto.LoginDTO;

@Stateless
public class LoginGerenteService {

	@Inject
	private Logger logger;

	@Inject
	private KeycloakHelper khelper;

	public boolean login(LoginDTO loginDTO) {

		logger.info("Autorizando com gerente...");

		khelper.initialize("keycloak-secondauth.json");

		KeycloakTokenDTO token = khelper.login(loginDTO.getUsername(),
		        loginDTO.getPassword());

		if (token == null) {
			return false;
		}

		String userID = khelper.getUserID(loginDTO.getUsername(),
		        token.getAccess_token());

		if (userID == null) {
			return false;
		}

		if (loginDTO.getPerfilNecessario() != null) {
			return khelper.hasRoles(userID, token.getAccess_token(),
			        loginDTO.getPerfilNecessario(), "administrador");
		} else {
			return khelper.hasRoles(userID, token.getAccess_token(), "gerenteunidade", "gerenteatendimentocliente",
			        "administrador");
		}

	}

}
