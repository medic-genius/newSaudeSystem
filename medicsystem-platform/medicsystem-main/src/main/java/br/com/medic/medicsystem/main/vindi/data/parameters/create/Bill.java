package br.com.medic.medicsystem.main.vindi.data.parameters.create;

import java.util.List;
import java.util.Map;

import br.com.medic.medicsystem.main.vindi.data.parameters.PaymentProfile;

public class Bill {
	private Integer customer_id;
	
	private String code;
	
	private Integer installments;
	
	private String payment_method_code;
	
	private String billing_at;
	
	private String due_at;
	
	private List<BillItem>bill_items;
	
	private Map<String, String> metadata;
	
	private PaymentProfile payment_profile;

	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public String getPayment_method_code() {
		return payment_method_code;
	}

	public void setPayment_method_code(String payment_method_code) {
		this.payment_method_code = payment_method_code;
	}

	public String getBilling_at() {
		return billing_at;
	}

	public void setBilling_at(String billing_at) {
		this.billing_at = billing_at;
	}

	public String getDue_at() {
		return due_at;
	}

	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}

	public List<BillItem> getBill_items() {
		return bill_items;
	}

	public void setBill_items(List<BillItem> bill_items) {
		this.bill_items = bill_items;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	public PaymentProfile getPayment_profile() {
		return payment_profile;
	}

	public void setPayment_profile(PaymentProfile payment_profile) {
		this.payment_profile = payment_profile;
	}
}
