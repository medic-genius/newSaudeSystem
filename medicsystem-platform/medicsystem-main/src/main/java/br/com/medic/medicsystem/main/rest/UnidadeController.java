package br.com.medic.medicsystem.main.rest;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.medic.medicsystem.main.service.UnidadeService;
import br.com.medic.medicsystem.persistence.model.Especialidade;
import br.com.medic.medicsystem.persistence.model.Unidade;

@Path("/unidades")
@RequestScoped
public class UnidadeController extends BaseController {

	// @Inject
	// private Logger logger;

	@Inject
	private UnidadeService unidadeService;

	@Context
	protected UriInfo info;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Unidade> getUnidades() {

		List<Unidade> unidades = unidadeService.getUnidades();
		if (unidades.isEmpty() || unidades == null) {

			throw new WebApplicationException(Response.Status.NO_CONTENT);
		} else {

			return unidades;
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/especialidades")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Especialidade> getEspecialidades(@PathParam("id") Long idUnidade) {
		
		if (idUnidade != null){
			
			return unidadeService.getEspecialidades(idUnidade);
		} else {
			
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Unidade getGuia(@PathParam("id") Long id) {
		return unidadeService.getUnidade(id);
	}
}