package br.com.medic.medicsystem.main.jasper;

import java.util.Iterator;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.medic.medicsystem.main.jasper.dto.RelatorioDespesaCliente;

public class JRDataSourceDespesaCliente implements JRDataSource {

	private Iterator<RelatorioDespesaCliente> it;
	private RelatorioDespesaCliente current;
	private boolean gotToNext = true;

	public JRDataSourceDespesaCliente(List<RelatorioDespesaCliente> list) {

		this.it = list.iterator();
	}

	public boolean next() throws JRException {
		current = it.hasNext() ? it.next() : null;
		gotToNext = (current != null);

		return gotToNext;
	}

	public Object getFieldValue(JRField field) throws JRException {
		Object valor = null;

		if ("nrMatricula".equals(field.getName())) {
			valor = current.getNrMatricula();

		} else if ("nmCliente".equals(field.getName())) {

			valor = current.getNmCliente();
		} else if ("nmOrgao".equals(field.getName())) {

			valor = current.getNmOrgao();

		} else if ("nmPlano".equals(field.getName())) {

			valor = current.getNmPlano();

		} else if ("vlMensalidade".equals(field.getName())) {

			valor = current.getVlMensalidade();

		} else if ("nmUsuario".equals(field.getName())) {

			valor = current.getNmUsuario();

		} else if ("nrDespesa".equals(field.getName())) {

			valor = current.getNrDespesa();

		} else if ("idDespesa".equals(field.getName())) {

			valor = current.getIdDespesa();

		} else if ("dtDespesa".equals(field.getName())) {

			valor = current.getDtDespesa();

		} else if ("vlDespesa".equals(field.getName())) {

			valor = current.getVlDespesa();

		} else if ("nmObservacao".equals(field.getName())) {

			valor = (current.getNmObsevacao() != null ? current
			        .getNmObsevacao().toUpperCase() : "");

		} else if ("servicosDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(current.getServicosDespesa());

		} else if ("parcelasDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(current.getParcelasDespesa());

		} else if ("encaminhamentosDespesa".equals(field.getName())) {

			valor = new JRBeanCollectionDataSource(
			        current.getEncaminhamentosDespesa());
		}

		return valor;
	}
}
