package br.com.medic.medicsystem.main.service;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.medic.medicsystem.persistence.dao.EmpresaGrupoDAO;
import br.com.medic.medicsystem.persistence.dao.FuncionarioDAO;
import br.com.medic.medicsystem.persistence.dao.PlanoDAO;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupo;
import br.com.medic.medicsystem.persistence.model.Funcionario;
import br.com.medic.medicsystem.persistence.model.Plano;

@Stateless
public class EmpresaService {

	@Inject
	@Named("empresagrupo-dao")
	private EmpresaGrupoDAO empresaGrupoDAO;
	
	@Inject
	@Named("funcionario-dao")
	private FuncionarioDAO funcionarioDAO;
	
	@Inject
	@Named("plano-dao")
	private PlanoDAO planoDAO;

	public Collection<EmpresaGrupo> getEmpresaGrupos() {

		return empresaGrupoDAO.getEmpresaGrupos();
	}

	public List<Funcionario> getVendedores(Long idEmpresa) {
	   return funcionarioDAO.getVendedoresPorEmpresaGrupo(idEmpresa);
    }

	public List<Plano> getPlanos(Long idEmpresa) {
	   return planoDAO.gePlanosPorEmpresaGrupo(idEmpresa);
    }
	
	public List<Plano> getPlanosByEmpresa(Long idEmpresa) {
	   return planoDAO.gePlanosByEmpresaGrupo(idEmpresa);
    }
		
}
