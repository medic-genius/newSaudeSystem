package br.com.medic.dashboard.main.rest.wrapper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import br.com.medic.dashboard.persistence.model.ClientePlano;

import com.opencsv.CSVWriter;


@Produces("text/csv")
public class ClientePlanoCSVWrapper implements
        MessageBodyWriter<List<ClientePlano>> {

	@Override
	public boolean isWriteable(Class<?> type, Type genericType,
	        Annotation[] annotations, MediaType mediaType) {
		ParameterizedType paramType = (ParameterizedType) genericType;

		if (paramType.getRawType().equals(List.class)) {
			if (paramType.getActualTypeArguments()[0].equals(ClientePlano.class)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public long getSize(List<ClientePlano> t, Class<?> type, Type genericType,
	        Annotation[] annotations, MediaType mediaType) {
		return -1;
	}

	@Override
	public void writeTo(List<ClientePlano> t, Class<?> type, Type genericType,
	        Annotation[] annotations, MediaType mediaType,
	        MultivaluedMap<String, Object> httpHeaders,
	        OutputStream entityStream) throws IOException,
	        WebApplicationException {
		
		httpHeaders.add("content-disposition","attachment; filename = clientePorPlano.csv");
		
		Writer osWriter = new OutputStreamWriter(entityStream, "UTF-8");
	    CSVWriter writer = new CSVWriter(osWriter, ',', '"', "\r\n");
	    if (t.size() > 0) {
	    	
	      writer.writeNext(new String[]{
	    		  "numerocontrato",
	    		  "situacao",
	    		  "formapagamento",
	    		  "nomecliente",
	    		  "vidas",
	    		  "custoVida",
	    		  "plano",
	    		  "cobrado",
	    		  "telefone",
	    		  "celular",
	    		  "endereco",
	    		  "numeroendereco",
	    		  });
	      //Write the data
	      
	      for (ClientePlano row: t) {
	        writer.writeNext(new String[]{
	        		row.getNumerocontrato(), 
	        		row.getSituacao(),
	        		row.getFormapagamento(),
	        		row.getNomecliente(),
	        		String.valueOf(row.getVidas()),
	        		row.getCustoVidaFormatado(),
	        		row.getPlanoFormatado(),
	        		row.getCobradoFormatado(),
	        		row.getTelefone(),
	        		row.getCelular(),
	        		row.getEndereco(),
	        		row.getNumeroendereco()
	        		});
	      }
	    }
	    writer.flush();
		
	}

}
