package br.com.medic.medicsystem.main.rest;

import java.net.URI;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.service.DespesaService;
import br.com.medic.medicsystem.persistence.dto.DespesaDTO;
import br.com.medic.medicsystem.persistence.dto.DespesaViewDTO;
import br.com.medic.medicsystem.persistence.model.Despesa;

@Path("/despesas")
@RequestScoped
public class DespesaController extends BaseController {

	@Inject
	private DespesaService despesaService;

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public DespesaViewDTO getDespesa(@PathParam("id") Long id) {
		return despesaService.getDespesaDTO(id);

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createDespesa(@Valid Despesa despesa) {
		
		try {
			Despesa salvo = despesaService.saveDespesa(despesa);
						
			Response resp = Response.created(URI.create("/despesas/" + salvo.getId()))
			        .build();
						
			return resp;
			
		} catch (Exception e) {
			// TODO: handle exception			
			throw new WebApplicationException(e.getMessage(), Response.Status.PRECONDITION_FAILED);			
		}
				
	}

	@GET
	@Path("/{id:[0-9][0-9]*}/pdf")
	@Produces("application/pdf")
	public Response getDespesaPDF(@PathParam("id") Long idDespesa) {

		if (idDespesa != null) {

			byte[] pdf = despesaService.generateDespesaPDF(idDespesa);

			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();

		} else {
			
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@POST
	@Path("/orcamento/pdf")
	@Produces("application/pdf")
	public Response getOrcamentoPDF(Despesa despesa) {

		byte[] pdf = despesaService.generateOrcamentoPDF(despesa);

		if (pdf == null) {
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}
 
		ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

		return response.build();

	}

	@GET
	@Path("/{id:[0-9][0-9]*}/encaminhamento/pdf")
	@Produces("application/pdf")
	public Response getEncaminhamentoPDF(@PathParam("id") Long idDespesa,
	        @QueryParam("path") String path) {

		if (idDespesa != null) {

			byte[] pdf = despesaService.generateEncaminhamentoDespesaPDF(
			        idDespesa, path);

			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(Base64.encodeBase64(pdf));

			return response.build();

		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

	}

	@POST
	@Path("/novoLancamento")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveNovoLancamentoDespesa(@Valid DespesaDTO despesaDTO) {

		Boolean autorizacaoGerenteRequired = despesaService
		        .isAutorizacaoGerenteRequired(despesaDTO);
		if (autorizacaoGerenteRequired) {

			despesaDTO.setBoAutorizadoGerente(autorizacaoGerenteRequired);
			return Response.accepted(despesaDTO).build();
		} else {

			Despesa salvo = despesaService.saveLancamentoDespesa(despesaDTO);
			return Response.created(URI.create("/despesas/" + salvo.getId()))
			        .build();
		}

	}

	@POST
	@Path("/novoLancamentoAutorizado")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveNovoLancamentoDespesaAutorizado(
	        @Valid DespesaDTO despesaDTO) {

		Despesa salvo = despesaService.saveLancamentoDespesa(despesaDTO);
		return Response.created(URI.create("/despesas/" + salvo.getId()))
		        .build();

	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeDespesa(@PathParam("id") Long id,
	        @QueryParam("motivo") String motivo) {
		despesaService.removeDespesa(id, motivo);
		return Response.ok().build();
	}

	@GET
	@Path("/search_single/guia_servico/{idGuiaServico:[0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getDespesaByGuiaServico(@PathParam("idGuiaServico") Long idGuiaServico) {
		Despesa despesa = despesaService.getDespesaByGuiaServico(idGuiaServico);
		if(null != despesa) {
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(despesa).build();
		} else {
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	@POST
	@Path("/gerafaturaconvenio")
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response geraFaturaEmpresaCliente(
			@QueryParam("idEmpresaCliente") Long idEmpresaCliente,
			@QueryParam("periodo") String periodo,
			@QueryParam("vlDesconto") Double vlDesconto,
			@QueryParam("obsFatura") String obsFatura){
		
		if(idEmpresaCliente != null){
			String link = despesaService.gerarFaturaEmpresaCliente(idEmpresaCliente, periodo, vlDesconto, obsFatura);
			JSONObject json = new JSONObject();
			json.put("link", link);
			return Response.status(200).type(MediaType.TEXT_PLAIN).entity(json).build();
		}
		else
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
	}
	
	@PUT
	@Path("/reabrirfaturaconvenio")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response geraFaturaEmpresaCliente(
			@QueryParam("idEmpresaCliente") Long idEmpresaCliente,
			@QueryParam("periodo") String periodo){
		
		if(idEmpresaCliente != null && periodo != null){
			Boolean reaberto = despesaService.reabrirFaturaEmpresaCliente(idEmpresaCliente, periodo);
			if(reaberto)
				return Response.status(200).type(MediaType.APPLICATION_JSON).build();
			else
				return Response.status(500).type(MediaType.APPLICATION_JSON).build();
		}
		else
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();		
	
	}
}