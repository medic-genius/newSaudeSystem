package br.com.medic.medicsystem.main.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.medic.medicsystem.main.service.EmpresaClienteService;
import br.com.medic.medicsystem.main.service.GuiaServicoService;
import br.com.medic.medicsystem.persistence.dto.ClienteConvDTO;
import br.com.medic.medicsystem.persistence.dto.GenericPaginateDTO;
import br.com.medic.medicsystem.persistence.model.Cliente;
import br.com.medic.medicsystem.persistence.model.CoberturaPlano;
import br.com.medic.medicsystem.persistence.model.Contrato;
import br.com.medic.medicsystem.persistence.model.ContratoCliente;
import br.com.medic.medicsystem.persistence.model.ContratoDependente;
import br.com.medic.medicsystem.persistence.model.Dependente;
import br.com.medic.medicsystem.persistence.model.EmpresaCliente;
import br.com.medic.medicsystem.persistence.model.Guia;
import br.com.medic.medicsystem.persistence.model.GuiaServico;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.views.EmpresaClienteContratoClienteView;


@Path("/empresasClientes")
@RequestScoped
public class EmpresaClienteController extends BaseController {
	
	@Inject
	private EmpresaClienteService empresaClienteService;
	
	@Inject
	private GuiaServicoService guiaServicoService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createEmpresaCliente(@Valid EmpresaCliente empresaCliente,
			@QueryParam("tipoCobranca") Integer tipoCobranca,
			@QueryParam("vlFixo") Double vlFixo) {
		EmpresaCliente newConveniada = empresaClienteService.saveEmpresaCliente(empresaCliente, tipoCobranca,vlFixo);
		if(null == newConveniada)
		{
			return Response.status(500).type(MediaType.APPLICATION_JSON).build();
		}
		return Response.created(URI.create("/empresasClientes/"+ newConveniada.getId())).build();
	}
	
	@POST
	@Path("saveCliente")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCliente(Cliente cliente) {
		
		Cliente salvo = empresaClienteService.saveCliente(cliente);
		
		return Response.created(URI.create("/saveCliente/"+ salvo.getId())).build();
		
	}
	/* add 24-04-2018*/
	@GET
	@Path("/cnpj/{cnpj}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaCliente> getEmpresaClienteByCnpj(@PathParam("cnpj") String cnpj){
		List<EmpresaCliente> empresas = empresaClienteService.getEmpresaClienteByCnpj(cnpj);
		return empresas;
	}
	/*end*/
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateEmpresaCliente(@Valid EmpresaCliente empresaCliente,
			@QueryParam("idContrato") Long idContrato, 
			@QueryParam("vlTotal") Double vlTotal) {

		if (empresaCliente != null) {
			EmpresaCliente empresaClienteAtualizada = empresaClienteService.
					updateEmpresaCliente(empresaCliente,idContrato,vlTotal);

			if (empresaClienteAtualizada != null) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				throw new WebApplicationException(
				        Response.Status.INTERNAL_SERVER_ERROR);
			}

		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@PUT
	@Path("/inativar/{id:[0-9][0-9]*}")
	public Response inativarEmpresaCliente(@PathParam("id") Long id){
		
		empresaClienteService.inativarEmpresaCliente(id);
		return Response.ok().build();
		
	}
	
	@PUT
	@Path("/ativar/{id:[0-9][0-9]*}")
	public Response ativarEmpresaCliente(@PathParam("id") Long id){
		
		empresaClienteService.ativarEmpresaCliente(id);
		return Response.ok().build();
		
	}
	
	@GET
	@Path("/ativas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaCliente> getListaEmpresasAtivas() {
		return empresaClienteService.getListaEmpresasAtivas();
	}
	
	@GET
	@Path("/inativas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaCliente> getListaEmpresasInativas() {
		return empresaClienteService.getListaEmpresasInativas();
	}
	
	@GET
	@Path("/clientecontratoview/{id: [0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmpresaClienteContratoClienteView> getDadosCliente(@PathParam("id") Long idConveniada) {
		return empresaClienteService.getDadosCliente(idConveniada);
	}
	
	@GET
	@Path("/clientesconveniada")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClienteConvDTO> getDadosClienteByLoginConveniada(
			@QueryParam("in_situacao") Integer inSituacao) {
		return empresaClienteService.getDadosClienteByLoginConveniada(inSituacao);
	}
	
	@POST
	@Path("/addcliente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoCliente addClienteOnConveniada(@Valid Cliente cliente) {
		return empresaClienteService.addClienteOnConveniada(cliente);
	}
	
	@PUT
	@Path("/inativarcontrato/{id: [0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Contrato updateInativarContratoEmpresaCliente(@PathParam("id")Long idContrato) throws Exception {
		if(idContrato != null){
			return empresaClienteService.inactiveAndResolveContrato(idContrato);
		} else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/reativarcontrato/{id:[0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Contrato updateReativarContratoEmpresaCliente(@PathParam("id") Long idContratoCliente) throws Exception {
		if(idContratoCliente != null){
			return empresaClienteService.reativarContratoEmpresaCliente(idContratoCliente);
		} else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
	}
	
	@PUT
	@Path("/{iddependente: [0-9][0-9]*}/inativarcontratodependente/{id: [0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoDependente updateInativarContratoEmpresaDependente(
			@PathParam("id")Long idCliente,
			@PathParam("iddependente")Long idDependente) {
		if(idCliente != null){
			return empresaClienteService.updateInativarContratoEmpresaDependente(idCliente, idDependente);
		} else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
		
	}
	
	@POST
	@Path("/adddependente/{id: [0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoDependente addContratodependente(
			@Valid Dependente dependente,
			@PathParam("id") Long idCliente) {
		
		if(idCliente != null){
			return empresaClienteService.addContratodependente(idCliente, dependente);
		}else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
		
	}
	
	@GET
	@Path("/dependentes/{id: [0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dependente> getDependentesByClientesEmpresaConveniada(
			@PathParam("id") Long idCliente) {
		return empresaClienteService.getDependentesByClientesEmpresaConveniada(idCliente);
	}

	@GET
	@Path("/servicos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Servico> getServicosByConveniada(){
		return empresaClienteService.getServicos(getQueryParam("name", String.class));
	}

	@GET
	@Path("/guias")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list(
			@QueryParam("offset") @DefaultValue("1") int offset, 
			@QueryParam("limit") @DefaultValue("10") int limit,
			@QueryParam("id") String idString,
			@QueryParam("nm_cliente") String nmCliente,
			@QueryParam("nm_beneficiario") String nameBeneficiary,
			@QueryParam("status") String statusString) {
		Long id = null;
		Integer status = null;
		try{
			if(idString != null) {
				id = Long.valueOf(idString).longValue();
			}
		} catch(NumberFormatException e) {
			id = (long) 0;
		}
		try{
			if(statusString != null) {
				status = Integer.valueOf(statusString).intValue();
			}
		} catch(NumberFormatException e) {
			status = 0;
		}
		GenericPaginateDTO paginator = empresaClienteService.getGuias(offset, limit, id, nmCliente, nameBeneficiary, status);
		if(paginator.getTotal() > 0)
		{
			return Response.status(200).type(MediaType.APPLICATION_JSON).entity(paginator).build();
		} else
		{
			return Response.status(204).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	@GET
	@Path("/guias/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGuia(@PathParam("id") Long id) {
		try {
			Guia guia = empresaClienteService.getGuia(id);
			if(guia != null)
			{
				return Response.status(200).type(MediaType.APPLICATION_JSON).entity(guia).build();
			} else {
				return Response.status(404).type(MediaType.APPLICATION_JSON).build();
			}
		} catch (WebApplicationException wae) {
			System.out.println("It does not happen!");
			return null;
		} catch (Exception e) {
			String http403 = "javax.ejb.EJBException: javax.ws.rs.WebApplicationException: HTTP 403 Forbidden";
			if(http403.equals(e.toString()))
			{
				return Response.status(403).type(MediaType.APPLICATION_JSON).build();
			}
			return null;
		}
	}
	
	@DELETE
	@Path("/guias/guia_servico/{id:[0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteGuiaService(@PathParam("id") Long id) {
		try {
			GuiaServico guiaServico = guiaServicoService.deleteGuiaServico(id);
			if(guiaServico != null)
			{
				return Response.status(200).type(MediaType.APPLICATION_JSON).entity(guiaServico).build();
			} else {
				return Response.status(404).type(MediaType.APPLICATION_JSON).build();
			}
		} catch (WebApplicationException wae) {
			System.out.println("It does not happen!");
			return null;
		} catch (Exception e) {
			String http403 = "javax.ejb.EJBException: javax.ws.rs.WebApplicationException: HTTP 403 Forbidden";
			if(http403.equals(e.toString()))
			{
				return Response.status(403).type(MediaType.APPLICATION_JSON).build();
			}
			return null;
		}
	}
	
	@GET
	@Path("/coberturaplano")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CoberturaPlano> getCoberturaPlano(
		@QueryParam("id") Long idEmpresaCliente) {
		return empresaClienteService.getCoberturaPlano(idEmpresaCliente);
	}
	

	@GET
	@Path("/dependentes/by_contrato/{id: [0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dependente> getDependentesByContrato(
			@PathParam("id") Long idContrato) {
		return empresaClienteService.getDependentesByContrato(idContrato);
	}

	@POST
	@Path("adddependente/by_contrato/{id: [0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoDependente addDependenteByContrato(
			@Valid Dependente dependente,
			@PathParam("id") Long idContrato) {
		
		if(idContrato != null){
			return empresaClienteService.addContratoDependenteByContrato(idContrato, dependente);
		}else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
		
	}

	@GET
	@Path("/dependentes/by_contrato/non_exclude/{id: [0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dependente> getDependentesByContratoAndNot(
			@PathParam("id") Long idContrato) {
		return empresaClienteService.getDependentesByContratoNoExclude(idContrato);
	}

	@PUT
	@Path("/inactive_contratodependente/{id: [0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ContratoDependente inactiveContratoDependenteEmpresaDependente(
			@PathParam("id")Long idContratoDependente) {
		if(idContratoDependente != null){
			return empresaClienteService.inactiveContratoDependente(idContratoDependente);
		} else {
			throw new WebApplicationException(
			        Response.Status.BAD_REQUEST);
		}
		
	}
}
