package br.com.medic.medicsystem.main.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.codec.binary.Base64;

import br.com.medic.medicsystem.main.service.OrcamentoExameService;
import br.com.medic.medicsystem.persistence.dto.OrcamentoExameDTO;
import br.com.medic.medicsystem.persistence.model.Servico;
import br.com.medic.medicsystem.persistence.model.views.AtendimentomedicoDespesaOrcamentoExameView;




@Path("/orcamentos")
@RequestScoped
public class OrcamentoExameController {
	
	
	@Inject
	private OrcamentoExameService orcamentoExameService;
	
	
	@POST
	@Path("/{id:[0-9][0-9]*}/save")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveOrcamento(
			@PathParam("id") Long idAgendamento,List<OrcamentoExameDTO> orcamentoExameDTO) {
		
		boolean saveOrcamento = orcamentoExameService.saveOrcamento(idAgendamento, orcamentoExameDTO);
		
		if (saveOrcamento) {
			return Response.ok().build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrcamento(List<OrcamentoExameDTO> orcamentoExameDTO) {
		
		boolean saveOrcamento = orcamentoExameService.updateOrcamento(orcamentoExameDTO);
		
		if (saveOrcamento) {
			return Response.ok().build();
		} else {
			throw new WebApplicationException(
			        Response.Status.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/especialidadeorcamento/{idespecialidade:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrcamentoExameDTO> getOrcamento(
			@PathParam("id") Long idAgendamento, @PathParam("idespecialidade") Long idEspecialidade) {

		if (idAgendamento != null && idEspecialidade != null) {
			return orcamentoExameService.getOrcamento(idAgendamento, idEspecialidade);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/orcamento/cliente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentomedicoDespesaOrcamentoExameView> getOrcamentoPorIdPaciente(
			@PathParam("id") Long idPaciente) {

		if (idPaciente != null) {
			return orcamentoExameService.getOrcamentoPorIdPaciente(idPaciente);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}/orcamento/{idAgendamento:[0-9][0-9]*}/pdf")
	@Produces("application/pdf")
	public Response getServicoOrcamentoPDF(
			@PathParam("id") Long idOrcamentoExame, @PathParam("idAgendamento") Long idAgendamento) {

		if (idOrcamentoExame != null) {
			byte[] pdf =orcamentoExameService.getServicoOrcamentoPDF(idOrcamentoExame,idAgendamento);
			
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(pdf);

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	
	
	@GET
	@Path("/{id:[0-9][0-9]*}/orcamento/servico")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Servico> getServicoPorOrcamento(
			@PathParam("id") Long idOrcamentoExame) {

		if (idOrcamentoExame != null) {
			return orcamentoExameService.getServicoPorOrcamento(idOrcamentoExame);
		} else {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}


}
