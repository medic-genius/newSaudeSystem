package br.com.medic.medicsystem.main.rest.superlogica;

import java.net.URLDecoder;
import java.text.ParseException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.medic.medicsystem.main.rest.BaseController;
import br.com.medic.medicsystem.main.service.CobrancaService;
import br.com.medic.medicsystem.main.service.superlogica.CobrancaServiceSP;
import br.com.medic.medicsystem.persistence.dto.BoletoDTO;
import br.com.medic.medicsystem.persistence.model.EmpresaGrupoCompartilhado;
import br.com.medic.medicsystem.persistence.model.superlogica.Cobranca;

@Path("/superlogica/cobranca")
@RequestScoped
public class CobrancaController extends BaseController{
	
	private static final String  VALIDATION_TOKEN_SL = "a917a6bd508b2e30258b055eb5f17a534a6df8ec";
	
	@Inject
	CobrancaServiceSP cobrancaServiceSP;
	
	@Inject
	CobrancaService cobrancaService;
	
	
	@POST
	@Path("/cadastarcobranca")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> cadastrarCobrancaCliente(List<Cobranca> cobranca){
		return cobrancaServiceSP.cadastrarCobrancaCliente(cobranca);
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/cadastarcobrancaavulsaperiodo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> cadastrarCobrancaClienteAvulsaPeriodo(List<Long> idClientes,
			@QueryParam("dtInicio")String dtInicio,
			@QueryParam("dtFim")String dtFim,
			@QueryParam("tipoValor")Integer tipoValor,
			@QueryParam("vlCobranca")Double vlCobranca){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro( getQueryParam("nrIdEmpresaFinanceiro", Long.class) );

		return cobrancaServiceSP.cadastrarCobrancaClienteAvulsaPeriodo(idClientes, empresa, dtInicio, dtFim, tipoValor, vlCobranca);
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/cadastarcobrancaavulsa")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> cadastrarCobrancaClienteAvulsa(List<Long> idClientes,
			@QueryParam("dtInicio")String dtInicio,
			@QueryParam("dtFim")String dtFim,
			@QueryParam("tipoValor")Integer tipoValor,
			@QueryParam("vlCobranca")Double vlCobranca){
		
		EmpresaGrupoCompartilhado empresa = cobrancaService.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro( getQueryParam("nrIdEmpresaFinanceiro", Long.class) );

		return cobrancaServiceSP.cadastrarCobrancaClienteAvulsa(idClientes, empresa, dtInicio, dtFim, vlCobranca, tipoValor);
	}
	
		
	@SuppressWarnings("unchecked")
	@POST
	@Path("/boletospdf")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/pdf")
	public Response getServicoOrcamentoPDF(
			List<BoletoDTO> boletos) {

		if (boletos != null && boletos.size() > 0) {
			byte[] pdf = cobrancaServiceSP.getBoletosPDF(boletos);
			
			if (pdf == null) {
				throw new WebApplicationException(Response.Status.NO_CONTENT);
			}

			ResponseBuilder response = Response.ok(pdf);

			return response.build();
			
		} else {

			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
	}
	

	@POST
	@Path("/postwebhook/liquidar")	
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject ResponseWebhook(String request) throws JSONException, ParseException{
				
		JSONObject response = new JSONObject();
		Boolean sucesso = false;
		
		request = URLDecoder.decode(request);		
		JSONObject json = new JSONObject(paramJson(request));
		System.out.println(json);
		
		if(json.getString("validationtoken").equals(VALIDATION_TOKEN_SL)){
			
			sucesso = cobrancaServiceSP.liquidarCobrancaSl(json);
			
			if(sucesso)
				response.put("status", 200); //OK
			else 
				response.put("status", 500); //Erro
		}
		else
			response.put("status", 401); //Não autorizado
		
		return response;
	}
	
	public String paramJson(String paramIn) {
	    paramIn = paramIn.replaceAll("=", "\":\"");
	    paramIn = paramIn.replaceAll("&", "\",\"");
	    return "{\"" + paramIn + "\"}";
	}

	@SuppressWarnings("unchecked")
	@POST
	@Path("/createcobrancaavulsacliente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<JSONObject> createCobrancaAvulsaCliente(
			List<Long> idMensalidades, @QueryParam("idCliente") Long idCliente,
			@QueryParam("dtVencimento") String dtVencimento,
			@QueryParam("vlBoleto") Double vlBoleto,
			@QueryParam("nrCelular") String nrCelular,
			@QueryParam("updateCel") Boolean updateCel) throws Exception {
		
		if(idCliente == null) {
			throw new Exception("Cliente para negociação não especificado");
		}
		if(dtVencimento == null) {
			throw new Exception("Data de vencimento do boleto não especificada");
		}
		if(vlBoleto == null) {
			throw new Exception("Valor do boleto não especificado");
		}
		if(nrCelular == null) {
			throw new Exception("Número de celular para envio do boleto não especificado");
		}

		EmpresaGrupoCompartilhado empresa = cobrancaService
				.getEmpresaGrupoCompartilhadoByEmpresaFinanceiro(Long
						.valueOf(17));

		return cobrancaServiceSP.createCobrancaAvulsaCliente(idCliente,
				idMensalidades, dtVencimento, vlBoleto, empresa, nrCelular,
				updateCel);
	}


}
