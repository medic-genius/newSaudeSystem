#!/usr/bin/env bash

set -eu


if (( $# != 1 ))
then
    echo >&2 "usage: add-project project"
    exit 1
fi
PROJECT=$1

fwk_prop_read() {
  local propkey=$1
  value=$(awk -F= "/framework.$propkey/ {print \$2}" /etc/rundeck/framework.properties)
  printf "%s" "${value//[[:space:]]/}"
}

RDECK_URL=$(fwk_prop_read  server.url)
RDECK_USER=$(fwk_prop_read server.username)
RDECK_PASS=$(fwk_prop_read server.password)
RDECK_NAME=$(fwk_prop_read server.name)
RDECK_HOST=$(fwk_prop_read server.hostname)

# Create a directory for the resource model
ETC=/var/rundeck/projects/${PROJECT}/etc
RESOURCES_D=$ETC/resources.d
mkdir -p "$RESOURCES_D"

# Create an example project
# --------------------------

echo "Creating project $PROJECT..."
chown -R rundeck:rundeck /var/rundeck

#
# Create the project

su - rundeck -c "rd-project -a create -p $PROJECT --resources.source.2.type=directory --resources.source.2.config.directory=$RESOURCES_D"

cat > $ETC/resources.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>

<project>    
  <node name="$RDECK_NAME" hostname="$RDECK_HOST" username="rundeck"
      description="Rundeck server node." tags=""
      osFamily="unix" osName="$(uname -s)" osArch="$(uname -m)" osVersion="$(uname -r)"
      >
    <!-- configure bash as the local node executor -->
    <attribute name="script-exec-shell" value="bash -c"/>
    <attribute name="script-exec" value="\${exec.command}"/>
    <attribute name="local-node-executor" value="script-exec"/>
  </node>
</project>
EOF

# Run a local ad-hoc command for sanity checking.
su - rundeck -c "dispatch -p $PROJECT"
# Run a distributed ad-hoc command across all nodes
su - rundeck -c "dispatch -p $PROJECT -f '*' whoami"

# Add jobs, scripts and options
# -----------------------------

exit $?